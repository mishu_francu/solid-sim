# solid-sim

Build with Visual Studio 2017. Find the solution in the 'make' folder.

Command line arguments:
-f[#frames] : sets the number of frames to run the simulation 

Keys:
T: start/stop simulation
space: pause everything
R: restart simulation
W/A/S/D: FPS camera navigation
X: toggle profiler display
Y: toggle step by step
C: toggle debug draw
