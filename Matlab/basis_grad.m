function value = basis_grad(order, node, xi)
%BASIS_GRAD computes the gradient of the basis function at point xi
%   Uses a Lagrange polynomial basis function
%   'order' is the order of the basis polynomial
%   'node' is the local node index inside the element
%   'xi' is the point at which we evaluate the function

    % evaluate gradient of Lagrange polynomial
    delta = 2 / order;
    points = -1:delta:1; % xi coordinates of element nodes
    value = 0;
    % go through all element nodes
    for j=1:order+1        
        if (j ~= node)
            prod = 1 / (points(node) - points(j));
            for i=1:order+1
                if (i ~= node && i ~= j)
                    prod = prod * (xi - points(i)) / (points(node) - points(i));
                end
            end
            value = value + prod;
        end
    end
end
