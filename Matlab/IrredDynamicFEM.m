common;

% the irreducible form
[Kirr, F] = assemble(n, 10, h, order, 1);
M = assemble_mass(n, h, order, density);

% set up the solution vectors
u = zeros(n, numSteps);
v = zeros(n, numSteps);
u(:,1) = ui;
v(:,1) = vi;

figure
for i = 1:numSteps-1
    % take one step
    Fe = Kirr * u(:,i);
    
    % symplectic Euler
    RHS = M * v(:,i) - dt^2 * Fe + dt^2 * F;
    v(:,i+1) = M \ RHS;
    u(:,i+1) = u(:,i) + dt * v(:,i+1);
    
    % the start point is always fixed
    v(1,i+1) = 0;
    u(1,i+1) = 0;
    
    % draw the current displacement
    clf    
    plot(x, u(:,i+1));
    height = g2;
    axis([0,L,-height,height])
    axis manual
    drawnow
    %pause(1)    
end

figure
plot(u(end,:));
