common;

% the mixed form
[J, C] = assemble_mixed(N, 10, dx, order, orderS);
F = assemble_forces(n, h, order, 1);
M = assemble_mass(n, h, order, density);
%KKT = [zeros(n, n) J'; J C]; % the KKT matrix
W = inv(M); % the inverse mass matrix
A = dt^2 * J * W * J' - C; % the Schur complement

% set up the solution vectors
u = zeros(n, numSteps);
v = zeros(n, numSteps);
u(:,1) = ui;
v(:,1) = vi;

figure
for i = 1:numSteps-1
    % compute the Lagrange multipliers
    vFree = v(:,i) + dt * F;
    b = J * (u(:,i) + dt * vFree);
    lambda = A \ b;
    
    Fe = J' * lambda; % the elastic/constraint force
    
    % symplectic Euler integration
    v(:, i+1) = vFree - dt^2 * W * Fe;
    u(:,i+1) = u(:,i) + dt * v(:,i+1);
    
    % the start point is always fixed
    v(1,i+1) = 0;
    u(1,i+1) = 0;
    
    % draw the current displacement
    clf    
    plot(x, u(:,i+1));
    height = g2;
    axis([0,L,-height,height])
    axis manual
    drawnow
    %pause(1)    
end

figure
plot(u(end,:));
