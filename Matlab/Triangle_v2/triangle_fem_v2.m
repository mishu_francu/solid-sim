close all
clear all

global Dm_inv area mu lambda f

% elastic params
E = 1;
nu = 0.4;
mu = E / (1 + nu) / 2;
lambda = E * nu / (1 + nu) / ( 1 - 2 * nu);

% set up the original triangle
x = [1; -1; 1; 1];
xm = x; % back up the undeformed pos

% draw it
figure
% line([0, x(1)], [0, x(2)]);
% line([0, x(3)], [0, x(4)]);
% line([x(1), x(3)], [x(2), x(4)]);

% compute material matrix
Dm = [x(1) x(3); x(2) x(4)];
area = 0.5 * det(Dm);
Dm_inv = inv(Dm);

Ds = [x(1) x(3); x(2) x(4)];
F = Ds * Dm_inv;

% check that the energy is zero
n = 4;
f = zeros(n, 1);
% prescribe compression forces along x
force = 1;
f(1) = force;
f(3) = force;% * 0.9;

% solve for equilibrium
x(1) = 1;
x(3) = 1;

% solve using energy minimization
disp("*fminunc");
options2 = optimoptions(@fminunc, 'CheckGradients', true, 'Display', 'off');
sol2 = fminunc(@total_energy, x, options2);

disp("*pure princ. stretches");
%sol3 = fminunc(@energy_pure_stretches, [1; 1]);
sol3 = fmincon(@energy_pure_stretches, [1; 1], [], [], [], [], [0; 0], []);

% solve using principal stretches
z = [x; 1; 1];
lb = [-Inf; -Inf; -Inf; -Inf; 0; 0];
disp("*fmincon princ. stretches");
options4 = optimoptions('fmincon', 'SpecifyConstraintGradient', true);%, 'CheckGradients', true);
sol4 = fmincon(@total_energy_stretches, z, [], [], [], [], lb, [], @constr4);%, options4);
%sol5 = fsolve(@saddle_stretches, [z; 0; 0]);.

% solve using princ. stretches and angles
disp("*fmincon princ. stretches + angles");
sol6 = fmincon(@total_energy_angles, [z; 0; 0], [], [], [], [], [lb; -Inf; -Inf], [], @constr6);

% ADMM
disp("*ADMM");
sol7 = admm_stretch(x, [1; 1]);

%x = sol4(1:4);
x = sol2;
Ds = [x(1) x(3); x(2) x(4)];
F = Ds * Dm_inv;
s = svd(F);

sol2
sol4

% draw the solution
draw_sol(sol2, 'g')
%draw_sol([sol3(1); -sol3(2); sol3(1); sol3(2)], 'y')
draw_sol(sol4(1:4), 'k')
%draw_sol(sol6(1:4), 'm')
%draw_sol(sol7(1:4), 'r')

function E = total_energy_angles(w)
    reg = 10;
    E = total_energy_stretches(w(1:6)) + reg * (w(7)^2 + w(8)^2);
end

function draw_sol(x, col)
    xlabel('x')
    ylabel('y')
    line([0, x(1)], [0, x(2)], 'Color', col);
    line([0, x(3)], [0, x(4)], 'Color', col);
    line([x(1), x(3)], [x(2), x(4)], 'Color', col);
end
