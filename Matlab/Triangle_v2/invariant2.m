function I2 = invariant2(x)
    global Dm_inv
    Ds = [x(1) x(3); x(2) x(4)];
    F = Ds * Dm_inv;    
    C = F' * F;
    I2 = det(C);
end

