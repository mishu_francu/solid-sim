function [c, ceq, GC, GCeq] = constr4_grad(z)
    x = z(1:4);
    s1 = z(5);
    s2 = z(6);
    c = []; % no inequality constraints
    GC = [];
    global Dm_inv area
    Ds = [x(1) x(3); x(2) x(4)];
    F = Ds * Dm_inv;    
    s = svd(F);
    ceq = [s1 - s(1); s2 - s(2)];
    
    % compute gradients of invariants I1 and I2 of C=F'*F
    H = F * Dm_inv';
    dI1 = 2 * [H(1,1); H(2,1); H(1,2); H(2,2)];
    %dI1v = finite_diff_gradient(@invariant1, x)
    G = [x(4); -x(3); -x(2); x(1)];
    dI2 = det(F) * G / area;
    %norm(finite_diff_gradient(@invariant2, x) - dI2)
    
    % compute gradient of constraints
    if s(1) ~= s(2)
        A = [s(1) * eye(4), s(2) * eye(4);
            s(1) * s(2)^2 * eye(4), s(1)^2 * s(2) * eye(4)];
        dI = [dI1; dI2];
        ds = 0.5 * A \ dI;
        ds1 = ds(1:4) / 4;
        ds2 = ds(5:8) / 4;
    else
        ds1 = dI1 / s(1) / 4;
        %ds1 = dI2 / 4 / s(1)^3;
        ds2 = ds1;
    end
    % TODO: missed a factor 4 in my calculations !?
    ds1v = norm(ds1 - finite_diff_gradient(@sing_val1, x))
    GCeq = [-ds1, -ds2; 1, 0; 0, 1];
end

