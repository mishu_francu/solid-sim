function [c, ceq, GC, GCeq] = constr4(z)
    x = z(1:4);
    s1 = z(5);
    s2 = z(6);
    c = []; % no inequality constraints
    GC = [];
    global Dm_inv
    Ds = [x(1) x(3); x(2) x(4)];
    F = Ds * Dm_inv;    
    [U, S, V] = svd(F);
    s = diag(S);
    if (det(V) < 0)
        % reflect first column
        V(:,1) = -V(:,1);
        % recompute U
        U = F * V * inv(S);        
    end
    sgn1 = 1;
    sgn2 = 1;
    if (det(U) < 0)
        disp("inverted singular values")
        if s(1) < s(2)
            s(1) = -s(1);
            sgn1 = -1;
            U(:,1) = -U(:,1);
        else
            s(2) = -s(2);
            sgn2 = -1;
            U(:,2) = -U(:,2);
        end
    end
    if s1 <= 0 || s2 <= 0
        disp("inverted vars")
    end
    ceq = [s1 - s(1); s2 - s(2)];
    
    % gradients
    W = Dm_inv * V;
    ds1 = [U(1,1) * W(1,1); U(2,1) * W(1,1); U(1,1) * W(2,1); U(2,1) * W(2,1)];
%     ds1fd = finite_diff_gradient(@sing_val1, x);
%     norm(ds1fd - ds1)
    ds2 = [U(1,2) * W(1,2); U(2,2) * W(1,2); U(1,2) * W(2,2); U(2,2) * W(2,2)];
%     ds2fd = finite_diff_gradient(@sing_val2, x);
%     norm(ds2fd - ds2)    

    %GCeq = [-sgn1 * ds1, -sgn2 * ds2; 1, 0; 0, 1];
    
    u1 = U(:,1);
    u2 = U(:,2);
    v1 = V(:,1);
    v2 = V(:,2);
    r1T = Dm_inv(1,:);
    r2T = Dm_inv(2,:);
    M1 = v1' * u1;
    M2 = v2' * u2;
    ds1 = [r1T * M1, r1T * M2];
    ds2 = [r2T * M1, r2T * M2];
    GCeq = [-ds1', -ds2'; 1, 0; 0, 1];
end

