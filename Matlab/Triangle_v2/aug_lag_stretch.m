function [L, grad] = aug_lag_stretch(s)
    global rho x_admm u_admm
    [c, ceq] = constr4([x_admm; s]);
    c1 = ceq + u_admm;
    L = energy_stretches(s) + 0.5 * rho * (c1' * c1);
    
    % gradient
    global mu lambda area
    s1 = s(1);
    s2 = s(2);
    psi1 = mu * s1 - mu / s1 + lambda * s2 * (s1 * s2 - 1);
    psi2 = mu * s2 - mu / s2 + lambda * s1 * (s1 * s2 - 1);
    grad = [area * psi1; area * psi2] + rho * c1;
end

