function s2 = sing_val2(x)
    global Dm_inv
    Ds = [x(1) x(3); x(2) x(4)];
    F = Ds * Dm_inv;    
    s = svd(F);
    s2 = s(2);
end

