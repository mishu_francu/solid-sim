function grad = finite_diff_gradient(fun, x)
    n = size(x,1);
    grad = zeros(n,1);
    E0 = fun(x);
    eps = 1e-6;
    for i=1:n
        basis = zeros(n,1);
        basis(i) = 1;
        grad(i) = (fun(x + eps * basis) - E0) / eps;
    end
end