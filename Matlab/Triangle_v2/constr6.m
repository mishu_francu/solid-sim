function [c, ceq] = constr6(w)
    c = [];
    x = w(1:4);
    s1 = w(5);
    s2 = w(6);
    theta = w(7);
    phi = w(8);
    %disp([theta, phi])
    global Dm_inv
    Ds = [x(1) x(3); x(2) x(4)];
    F = Ds * Dm_inv;
    U = [cos(theta), -sin(theta); sin(theta), cos(theta)];
    V = [cos(phi), -sin(phi); sin(phi), cos(phi)];
    Fhat = U' * F * V;
    ceq = [s1 - Fhat(1,1); s2 - Fhat(2,2); Fhat(1,2); Fhat(2,1)];
end

