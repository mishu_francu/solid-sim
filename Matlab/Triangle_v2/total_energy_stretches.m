function E = total_energy_stretches(z)
    global f area mu lambda
    x = z(1:4);
    s1 = z(5);
    s2 = z(6);
    if s1 <= 0 || s2 <= 0
        disp("inversion")
    end
    J = s1 * s2;
    I1 = s1^2 + s2^2;
    psi = 0.5 * mu * (I1 - 2) - mu * reallog(J) + 0.5 * lambda * (J - 1)^2;
    E = x' * f + area * psi;
end

