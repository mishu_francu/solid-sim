function sol = admm_stretch(x, s)
    global rho x_admm s_admm u_admm
    x_admm = x;
    s_admm = [1; 1];
    u_admm = 0;
    rho = 30;
    options_s_con = optimoptions('fmincon', 'Display', 'off');
    options_s_unc = optimoptions('fminunc', 'Display', 'off');%, 'SpecifyObjectiveGradient', true);% 'CheckGradients', true);
    options_x = optimoptions('fminunc', 'Display', 'off');
    old_energy = total_energy_stretches([x_admm; s_admm]);
    threshold = 1e-8;
    for iter=1:30
        %s_admm = fminunc(@aug_lag_stretch, s_admm, options_s_unc);
        [s_admm, s_iters] = gradient_descent_exp(@grad_stretch, s_admm, 0.009, 400);
        %[s_admm, s_iters] = gradient_descent(@grad_stretch, s_admm, 0.001, 400);
        %[s_admm, s_iters] = proj_gradient_descent(@grad_stretch, s_admm, 0.001, 400);
        %disp(s_iters)
        %s_admm = fmincon(@aug_lag_stretch, s_admm, [], [], [], [], [0; 0], [], [], options_s_con);
        x_admm = fminunc(@aug_lag_pos, x_admm, options_x);
        new_energy = total_energy_stretches([x_admm; s_admm]);
        rel_diff = abs(new_energy - old_energy) / old_energy;
%         if rel_diff < threshold
%             iter
%             break
%         end
        old_energy = new_energy;
        [c, ceq] = constr4([x_admm; s_admm]);
        u_admm = u_admm + ceq;
    end
    sol = [x_admm; s_admm];
end