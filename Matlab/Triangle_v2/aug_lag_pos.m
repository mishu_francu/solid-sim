function L = aug_lag_pos(x)
    global f rho s_admm u_admm
    [c, ceq] = constr4([x; s_admm]);
    c1 = ceq + u_admm;
    L = x' * f + 0.5 * rho * (c1' * c1);
end

