function grad = saddle_stretches(z)
    global f area mu lambda Dm_inv
    x = z(1:4);
    s1 = z(5);
    s2 = z(6);
    eta1 = z(7);
    eta2 = z(8);
    Ds = [x(1) x(3); x(2) x(4)];
    F = Ds * Dm_inv;
    psi1 = mu * s1 - mu / s1 + lambda * s2 * (s1 * s2 - 1);
    psi2 = mu * s2 - mu / s2 + lambda * s1 * (s1 * s2 - 1);
    %eta2 = (psi1 - psi2) / (s2 - s1)
    %eta1 = area * (s1 * psi1 - s2 * psi2) / (s1 - s2)
    y = [Dm_inv(1,1); Dm_inv(1,2); Dm_inv(2,1); Dm_inv(2,2)];
    G = [x(4); -x(3); -x(2); x(1)];
    grad = [f + eta1 * y + 0.5 * eta2 * G / area;
        area * psi1 - eta1 - eta2 * s2;
        area * psi2 - eta1 - eta2 * s1;
        s1 + s2 - trace(F); 
        s1 * s2 - det(F)];
end

