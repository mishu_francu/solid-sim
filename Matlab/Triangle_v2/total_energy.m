% build the objective
function [Pi] = total_energy(x)
% x is a 4 vector containing points 1 and 2 (3 is fixed in zero): x' = (x1' x2')
% f is also a 4 vector containing the forces acting on the points
    global Dm_inv area mu lambda f
    % compute spatial matrix
    Ds = [x(1) x(3); x(2) x(4)];
    % compute deformation gradient
    F = Ds * Dm_inv;
    % compute right Cauchy tensor
    C = F' * F;
    
    % compute first invariant
    I1 = trace(C);
    % compute J and its log
    J = det(F);
    if J == 0
        disp("degenerate case");
    end
    if J < 0
        disp("inverted case");
    end
    logJ = log(J);
    % compute Neo-Hookean energy density
    %psi = 0.5 * mu * (I1 - 2) - mu * logJ + 0.5 * lambda * logJ^2;
    psi = 0.5 * mu * (I1 - 2) - mu * logJ + 0.5 * lambda * (J - 1)^2;
    
    % compute StVK energy density
    %E = 0.5 * (C - eye(2)); % Green strain
    %psi = mu * norm(E) + 0.5 * lambda * trace(E)^2;
    
    % total energy
    Pi = area * psi + x' * f;

    %grad = total_gradient(x);
end

