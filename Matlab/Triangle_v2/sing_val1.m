function s1 = sing_val1(x)
    global Dm_inv
    Ds = [x(1) x(3); x(2) x(4)];
    F = Ds * Dm_inv;    
    s = svd(F);
    s1 = s(1);
end

