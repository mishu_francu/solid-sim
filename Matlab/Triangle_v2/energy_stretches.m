function E = energy_stretches(s)
    global area mu lambda
    s1 = s(1);
    s2 = s(2);
    if s1 <= 0 || s2 <= 0
        disp("inversion")
    end
    J = s1 * s2;
    I1 = s1^2 + s2^2;
    psi = 0.5 * mu * (I1 - 2) - mu * reallog(J) + 0.5 * lambda * (J - 1)^2;
    E = area * psi;
end

