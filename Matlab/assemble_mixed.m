function [J, C] = assemble_mixed(N, E, h, mu, ms)
%ASSEMBLE_MIXED assemble a Jacobian and compliance matrix
%   displacements use a linear shape function
%   stresses also use a linear shape function
%   N is the number of elements
%   E is Young's modulus
%   h is the length of an element
%   ms is the order of shape functions for stress
    global xi w p
    nu = mu * N + 1; % the number of displacement nodes
    ns = ms * N + 1; % the number of stress nodes
    if (ms == 0)
        ns = N;
    end
    J = zeros(ns, nu); % Jacobian matrix
    C = zeros(ns, ns); % compliance matrix
    % iterate through all elements
    for e=1:N
        % we have ms+1 stress nodes per element
        % and mu+1 displacement nodes per element
        Jlocal = zeros(ms+1, mu+1);
        Clocal = zeros(ms+1, ms+1);
        % Gaussian quadrature of Clocal
        for q=1:p
            % construct shape matrices at the current quadrature point
            Ns = zeros(1, ms+1);
            % go through all stress element nodes
            for j=1:ms+1
                Ns(j) = basis(ms, j, xi(q));
            end
            
            Nugrad = zeros(1, mu+1);
            % go through all displacement element nodes
            for j=1:mu+1
                Nugrad(j) = basis_grad(mu, j, xi(q));
            end
            
            Clocal = Clocal - mu * h / 2 / E * (Ns' * Ns) * w(q);
            
            Jlocal = Jlocal + (Ns' * Nugrad) * w(q);
        end
        
        % compute the global indices of the local stress nodes
        start = (e-1) * ms + 1;
        if (ms == 0)
            start = e;
        end
        globalIdx = start : start+ms;

        % compute the global indices of the local displacement nodes
        start = (e-1) * mu + 1;
        globalIdxU = start : start+mu;

        for i=1:ms+1
            idxI = globalIdx(i);
            for j=1:ms+1
                idxJ = globalIdx(j);
                C(idxI, idxJ) = C(idxI, idxJ) + Clocal(i, j);
            end
            
            idxI = globalIdx(i);
            for j=1:mu+1
                idxJ = globalIdxU(j);
                J(idxI, idxJ) = J(idxI, idxJ) + Jlocal(i, j);
            end
        end        
    end    
end
