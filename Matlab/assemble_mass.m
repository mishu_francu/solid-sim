function [M] = assemble_mass(n, h, m, density)
%ASSEMBLE_MASS builds a mass marix M
% n is the number of points
% h is the length of an element
% m is the order of the shape functions
    
    global p xi w;
    % assemble the stiffness matrix and the forcing vector
    M = zeros(n, n); % mass matrix
    % iterate over all elements
    numElem = (n-1) / m; % the number of elements of order m
    for e=1:numElem
        % there are m+1 nodes per element
        Mlocal = zeros(m+1, m+1);
        
        % compute the global x coordinate of the nodes
        start = (e-1) * m + 1;
        globalIdx = start:start+m;
        
        % Gaussian quadrature of Mlocal
        for q=1:p      
            % construct shape matrix at the current quadrature point
            N = zeros(1, m+1);
            for j=1:m+1
                N(j) = basis(m, j, xi(q));
            end
            
            % local mass matrix
            Mlocal = Mlocal + w(q) * density * h / 2 * (N' * N); % this is constant as long as h and density are constant
        end    

        % add the contribution to the global matrix
        for k=1:m+1
            idxK = globalIdx(k);           
            for l=1:m+1
                idxL = globalIdx(l);
                M(idxK, idxL) = M(idxK, idxL) + Mlocal(k, l);
            end
        end        
    end
end