common;

% TODO: use sparse matrices

% the mixed form
[J, C] = assemble_mixed(N, E, dx, order, orderS);
%[J, C] = assemble_mixed11(n, E, h);
%[J, C] = assemble_mixed22(N, E, h);
F = assemble_forces(n, h, order, fbar);
if (problem == 2)
    % the Dirichlet-Neumann problem - broken
    J2 = J(:, 2:n); % remove the first row    
    F = zeros(2 * (n-1), 1); % n-1 displacements and forces

    % solve using KKT matrix
    K = [ zeros(n-1, n-1) J2' ; J2 C]; % the KKT matrix
    F(n-1, 1) = t;
    z = K\F;    

    % solve using Schur complement
    Ktan = -J2' * inv(C) * J2;
    F1 = zeros(n-1, 1);
    F1(n-1) = t;
    u = Ktan\F1;
else
    % the pure Dirichlet problem
    %if (n - 2 < ns) % the solvability requirement
        J2 = J(:, 2:n-1); % remove the first and last columns

        % solve using the KKT matrix
        RHS = [F(2:n-1); -g1 * J(:,1) - g2 * J(:,n)];
        K = [zeros(n-2, n-2) J2'; J2 C]; % the KKT matrix
        z = K\RHS;
        uMix = [g1; z(1:n-2); g2];
        sigma = z(n-1:end);

        % compute the Schur complement
        Ktan = -J' * inv(C) * J;

        % solve the pure Dirichlet problem
        KtanDD = Ktan(2:n-1, 2:n-1); % eliminate the first and last rows/columns
        FDD = F(2:n-1) - g1 * Ktan(2:n-1, 1) - g2 * Ktan(2:n-1, n);
        uDD = KtanDD\FDD;
        uMix2 = [g1; uDD; g2];
   %end
end

% the irreducible form
[Ktan2, F2] = assemble(n, E, h, order, fbar);

if (problem == 2) 
    % solve the Dirichlet-Neumann problem
    KtanDN = Ktan2(2:n, 2:n); % eliminate the first row and column from K
    FDN = F2(2:n); % eliminate the first row from F
    FDN(n-1) = FDN(n-1) + t; % add the endpoint traction term to the RHS
    uDN = KtanDN\FDN; % solve the linear system
    uIrr = [g1 uDN];
else
    % solve the pure Dirichlet problem
    KtanDD = Ktan2(2:n-1, 2:n-1); % eliminate the first and last rows/columns
    FDD = F2(2:n-1) - g1 * Ktan2(2:n-1, 1) - g2 * Ktan2(2:n-1, n);
    uDD = KtanDD\FDD;
    uIrr = [g1; uDD; g2];
end

% reconstruct the stress from displacements
sigmaIrr = zeros(n-1, 1);
for i=1:n-1    
    sigmaIrr(i) = E * (uIrr(i+1) - uIrr(i)) / dx; % finite difference approximation    
end

% compute the exact solution
x = 0:dx:L;
a = -fbar / 6 / E;
b = g2 / L - a * L^2;
uExact = a * x.^3 + b * x;
sigmaExact = E * (3 * a * x.^2 + b);

% plot the results
figure('Name', 'Displacement');
plot(x, uMix, x, uIrr, x, uExact);
legend({'mixed', 'irred', 'exact'}, 'Location', 'northwest');

figure('Name', 'Stress');
xMid = ((1:n-1) - 0.5) * dx;
if (orderS == 0)
    xs = ((1:ns) - 0.5) * h;
else
    xs = ((1:ns) - 1) * h/orderS;
end
plot(x, sigmaExact, xs, sigma, xMid, sigmaIrr);
legend({'exact', 'mixed', 'irred'}, 'Location', 'west');

function [J, C] = assemble_mixed10(n, E, h)
%ASSEMBLE_MIXED10 assemble a Jacobian and compliance matrix
%   displacements use a linear shape function (order 1)
%   stresses are piecewise continuous (order 0)
%   n is the number of points
%   E is Young's modulus
%   h is the length of an element
    J = zeros(n-1, n); % bi-diagonal matrix
    C = zeros(n-1, n-1); % diagonal matrix
    % iterate through all elements
    for i=1:n-1
        J_local = [-1 1]; % Jacobian matrix
        C_local = -h / E; % compliance matrix
        
        J(i, i) = J(i, i) + J_local(1);
        J(i, i+1) = J(i, i+1) + J_local(2);     
        C(i, i) = C_local;
    end    
end

function [J, C] = assemble_mixed11(n, E, h)
%ASSEMBLE_MIXED10 assemble a Jacobian and compliance matrix
%   displacements use a linear shape function (order 1)
%   stresses are also linear (order 1)
%   n is the number of points
%   E is Young's modulus
%   h is the length of an element
    J = zeros(n, n);
    C = zeros(n, n);
    % iterate through all elements
    for e=1:n-1
        J_local = 0.5 * [-1 1; -1 1]; % Jacobian matrix
        C_local = -h / 6 / E * [2 1; 1 2]; % compliance matrix
        
        J(e, e) = J(e, e) + J_local(1, 1);
        J(e, e+1) = J(e, e+1) + J_local(1, 2); 
        J(e+1, e) = J(e+1, e) + J_local(2, 1);
        J(e+1, e+1) = J(e+1, e+1) + J_local(2, 2);

        C(e, e) = C(e, e) + C_local(1, 1);
        C(e, e+1) = C(e, e+1) + C_local(1, 2);
        C(e+1, e) = C(e+1, e) + C_local(2, 1);
        C(e+1, e+1) = C(e+1, e+1) + C_local(2, 2);
    end    
end

function [J, C] = assemble_mixed21(N, E, h)
%ASSEMBLE_MIXED10 assemble a Jacobian and compliance matrix
%   displacements use a quadratic shape function (order 2)
%   stresses are linear (order 1)
%   N is the number of elements
%   E is Young's modulus
%   h is the length of an element
    ns = N + 1;
    nu = 2 * N + 1;
    J = zeros(ns, nu);
    C = zeros(ns, ns);
    % iterate through all elements
    for e=1:N
        J_local = 1 / 6 * [-5 4 1; -1 -4 5]; % Jacobian matrix
        C_local = -h / 6 / E * [2 1; 1 2]; % compliance matrix
        
        C(e, e) = C(e, e) + C_local(1, 1);
        C(e, e+1) = C(e, e+1) + C_local(1, 2);
        C(e+1, e) = C(e+1, e) + C_local(2, 1);
        C(e+1, e+1) = C(e+1, e+1) + C_local(2, 2);

        start = (e-1) * 2 + 1;
        J(e, start) = J(e, start) + J_local(1, 1);
        J(e, start+1) = J(e, start+1) + J_local(1, 2);
        J(e, start+2) = J(e, start+2) + J_local(1, 3);
        J(e+1, start) = J(e+1, start) + J_local(2, 1);
        J(e+1, start+1) = J(e+1, start+1) + J_local(2, 2);
        J(e+1, start+2) = J(e+1, start+2) + J_local(2, 3);
    end    
end

function [J, C] = assemble_mixed22(N, E, h)
%ASSEMBLE_MIXED10 assemble a Jacobian and compliance matrix
%   displacements use a quadratic shape function (order 2)
%   stresses are also quadratic (order 2)
%   N is the number of elements
%   E is Young's modulus
%   h is the length of an element
    ns = 2 * N + 1;
    nu = 2 * N + 1;
    J = zeros(ns, nu);
    C = zeros(ns, ns);
    % iterate through all elements
    for e=1:N
        J_local = 1 / 2 * [-1 4/3 -1/3; -4/3 0 4/3; 1/3 -4/3 1]; % Jacobian matrix
        C_local = -h / 15 / E * [2 1 -1/2; 1 8 1; -1/2 1 2]; % compliance matrix

        start = (e-1) * 2 + 1;
        i = start;
        C(i, i) = C(i, i) + C_local(1, 1);
        C(i, i+1) = C(i, i+1) + C_local(1, 2);
        C(i, i+2) = C(i, i+2) + C_local(1, 3);
        C(i+1, i) = C(i+1, i) + C_local(2, 1);
        C(i+1, i+1) = C(i+1, i+1) + C_local(2, 2);
        C(i+1, i+2) = C(i+1, i+2) + C_local(2, 3);
        C(i+2, i) = C(i+2, i) + C_local(3, 1);
        C(i+2, i+1) = C(i+2, i+1) + C_local(3, 2);
        C(i+2, i+2) = C(i+2, i+2) + C_local(3, 3);
        
        J(i, start) = J(i, start) + J_local(1, 1);
        J(i, start+1) = J(i, start+1) + J_local(1, 2);
        J(i, start+2) = J(i, start+2) + J_local(1, 3);
        J(i+1, start) = J(i+1, start) + J_local(2, 1);
        J(i+1, start+1) = J(i+1, start+1) + J_local(2, 2);
        J(i+1, start+2) = J(i+1, start+2) + J_local(2, 3);
        J(i+2, start) = J(i+2, start) + J_local(3, 1);
        J(i+2, start+1) = J(i+2, start+1) + J_local(3, 2);
        J(i+2, start+2) = J(i+2, start+2) + J_local(3, 3);
    end    
end
