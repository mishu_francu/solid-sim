% gradient descent
function [x, iter] = gradient_descent_exp(grad, x0, alpha, max_iter)
    x = x0;
    res = norm(x);
    for iter=1:max_iter
        dx = - alpha * grad(x);
        x = x * exp(dx);
        new_res = norm(x);
        rel_res = (new_res - res) / res;
        if abs(rel_res) < 1e-6
            break        
        end
        res = new_res;
    end
end