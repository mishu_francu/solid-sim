function J = deformation_jacobian(F)
    J = det(F);
    if J == 0
        disp("degenerate case");
    end
    if J < 0
        disp("inverted case");
    end    
end

