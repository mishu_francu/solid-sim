close all

p1 = [0; 0];
p2 = [1; 1];
p3 = [1; -1];
Dm = zeros(2,2);
Dm(:,1) = p2 - p1;
Dm(:,2) = p3 - p1;

figure
line([0, p2(1)], [0, p2(2)]);
line([0, p3(1)], [0, p3(2)]);
line([p2(1), p3(1)], [p2(2), p3(2)]);

n = 100; % number of frames
l1 = zeros(1,n);
l2 = zeros(1,n);
theta = zeros(1,n);
phi = zeros(1,n);

for i=1:n
    p2 = p2 + 0.001 * rand(2,1);
    p3 = p3 + 0.001 * rand(2,1);

    line([0, p2(1)], [0, p2(2)], 'Color', 'r');
    line([0, p3(1)], [0, p3(2)], 'Color', 'r');
    line([p2(1), p3(1)], [p2(2), p3(2)], 'Color', 'r');

    Ds = zeros(2,2);
    Ds(:,1) = p2 - p1;
    Ds(:,2) = p3 - p1;

    F = Ds * inv(Dm);

    [U, S, V] = svd(F);

    l1(i) = S(1,1);
    l2(i) = S(2,2);
    theta(i) = U(2,1);% atan2(U(2,1), U(1,1));
    phi(i) = atan2(V(2,1), V(1,1));
    a1 = theta * 180 / pi;
    a2 = phi * 180 / pi;
end

figure
plot(l1)
title('lambda1')

figure
plot(l2)
title('lambda2')

figure
plot(theta)
title('theta')

figure
plot(phi)
title('phi')

