%finite difference directional derivative
function df = finite_diff_dir_deriv(fun, x, dx)
    eps = 1e-3;
    f0 = fun(x);
    f1 = fun(x + eps * dx);
    df = (f1 - f0) / eps;
end

