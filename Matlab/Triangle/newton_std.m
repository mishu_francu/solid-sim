function sol = newton_std(x)
    sol = x;
    for iter=1:10
        r = total_gradient(sol);
        K = finite_diff_hessian(@total_gradient, sol);
        dx = -K\r;
        sol = sol + dx;
    end
end