% build the objective
function [Pi] = total_energy_area(x)
% x is a 4 vector containing points 1 and 2 (3 is fixed in zero): x' = (x1' x2')
% y=x(5) is the deformed area of the triangle
% f is also a 4 vector containing the forces acting on the points
    global Dm_inv area mu lambda f
    y = x(5);
    % compute spatial matrix
    Ds = [x(1) x(3); x(2) x(4)];
    % compute deformation gradient
    F = Ds * Dm_inv;
    % compute right Cauchy tensor
    C = F' * F;
    
    % compute first invariant
    I1 = trace(C);
    % compute J and its log
%     J = det(F);
%     if J == 0
%         disp("degenerate case");
%     end
%     if J < 0
%         disp("inverted case");
%     end
%     logJ = log(J);
    % compute Neo-Hookean energy density
    %psi = 0.5 * mu * (I1 - 2) - mu * logJ;
    % Mooney energy
    psi = 0.5 * mu * (I1 - 2);
    
    % volumetric error function
    J = y / area;
    
    % total energy (deviatoric, volumetric and external)
    Pi = area * psi - area * mu * log(J) + area * 0.5 * lambda * (J - 1)^2 + x(1:4)' * f;
end

