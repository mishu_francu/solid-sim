% deformation gradient
function F = deformation_gradient(x)
    global Dm_inv area mu lambda f
    % compute spatial matrix
    Ds = [x(1) x(3); x(2) x(4)];
    % compute deformation gradient
    F = Ds * Dm_inv;
end
