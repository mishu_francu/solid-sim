function H = hessian_area_exp(z)
    global lambda area
    y = z(5);
    Kd = finite_diff_hessian(@total_gradient_dist, z(1:4));
    k = lambda / area;
    G = 0.5 * [z(4); -z(3); -z(2); z(1)];
    Ha = [ 0 0 0 1;
        0 0 -1 0;
        0 -1 0 0;
        1 0 0 0];
    Kg = Ha * k * (z(5) - area);
    H = [Kd + Kg k * y * G; G' -y];
end