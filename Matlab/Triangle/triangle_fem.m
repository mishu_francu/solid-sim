close all
clear all

global Dm_inv area mu lambda f

% elastic params
E = 1;
nu = 0.49;
mu = E / (1 + nu) / 2;
lambda = E * nu / (1 + nu) / ( 1 - 2 * nu);

% set up the original triangle
x = [1; -1; 1; 1];
xm = x; % back up the undeformed pos

% draw it
figure
line([0, x(1)], [0, x(2)]);
line([0, x(3)], [0, x(4)]);
line([x(1), x(3)], [x(2), x(4)]);

% compute material matrix
Dm = [x(1) x(3); x(2) x(4)];
area = 0.5 * det(Dm);
Dm_inv = inv(Dm);

% check that the energy is zero
n = 4;
f = zeros(n, 1);
%[Pi, grad] = total_energy(x);

% % prescribe a compression displacmeent along x
% delta = 0.1;
% x(1) = x(1) - delta;
% x(3) = x(3) - delta;
% 
% % draw the compressed triangle
% line([0, x(1)], [0, x(2)]);
% line([0, x(3)], [0, x(4)]);
% line([x(1), x(3)], [x(2), x(4)]);
% 
% % check that the energy is non-zero
% [Pi, grad] = totalEnergy(x)
 
% prescribe compression forces along x
force = 50;
f(1) = force;
f(3) = force;

% solve for equilibrium
x(1) = 1;
x(3) = 1;

% solve using energy minimization
%sol1 = fminsearch(@totalEnergy, x)
disp("*fminunc");
options2 = optimoptions(@fminunc, 'CheckGradients', true, 'Display', 'off');
sol2 = fminunc(@total_energy, x, options2);
% solve using force equation
disp("*fsolve");
options3 = optimoptions('fsolve','SpecifyObjectiveGradient',true, 'Display', 'off');
sol3 = fsolve(@saddle_std, x, options3);

% compute area once again
Ds = [x(1) x(3); x(2) x(4)];
newArea = 0.5 * det(Ds);

% solve using area var
z = [x; newArea];
lb = [-Inf; -Inf; -Inf; -Inf; 0];
disp("*fmincon area");
options4 = optimoptions('fmincon', 'Display', 'off');
sol4 = fmincon(@total_energy_area, z, [], [], [], [], [], [], @constr, options4);
options5 = optimoptions('fsolve','SpecifyObjectiveGradient',true, 'Display', 'off');
disp("*fsolve area");
sol5 = fsolve(@saddle_area, z);

% draw the solution
draw_sol(sol2, 'r')
%draw_sol(sol4(1:4), 'g')
draw_sol(sol5(1:4), 'k')

% update the configuration
%x = sol2;

% Newton (standard FEM)
disp("*Newton std");
sol6 = newton_std(x);
%draw_sol(sol6, 'y')

% Newton (mixed pos-area FEM)
% disp("*Newton pos-area exp");
% sol7 = newton_area_exp(z);
% draw_sol(sol7(1:4), 'c')

% compute area once again
Ds = [x(1) x(3); x(2) x(4)];
newArea = 0.5 * det(Ds);

function [c, ceq] = constr(z)
    x = z(1:4);
    y = z(5);
    c = []; % no inequality constraints
    Ds = [x(1) x(3); x(2) x(4)];
    a = 0.5 * det(Ds);
    ceq = y - a;
end

function draw_sol(x, col)
    line([0, x(1)], [0, x(2)], 'Color', col);
    line([0, x(3)], [0, x(4)], 'Color', col);
    line([x(1), x(3)], [x(2), x(4)], 'Color', col);
end

function L = lagrangian(x)
    global lambda area
    y = x(5);
    Ds = [x(1) x(3); x(2) x(4)];
    a = 0.5 * det(Ds);
    multiplier = lambda * (y - area) / area;
    L = total_energy_area(x) - multiplier * (y - a);
end

function [f, J] = saddle_std(x)
    f = total_gradient(x);
    J = finite_diff_hessian(@total_gradient, x);
end