% elastic and external gradient (force)
function grad = total_gradient(x)
    global Dm_inv area mu lambda f
    F = deformation_gradient(x);
    
    % compute J and its log
    J = deformation_jacobian(F);

    % compute first Piola-Kirchoff tensor
    % Neo-Hookean
    Finv = inv(F);
    %P = mu * (F - Finv') + lambda * log(J) * Finv';
    P = mu * (F - Finv') + lambda * (J - 1) * J * Finv';
    
    % internal forces
    fint = area * P * Dm_inv';
    
    % gradient
    grad = [fint(1,1) fint(2,1) fint(1,2) fint(2,2)]' + f;
    
    % verify
    %grad_verify = finite_diff_gradient(@total_energy, x)
end

