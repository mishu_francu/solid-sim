% distortional elastic and external gradient (force)
function grad = total_gradient_dist(x)
    global Dm_inv area mu f
    F = deformation_gradient(x);
%     if (det(F) <= 0)
%         disp("inverted case (disp)");
%     end
    
    % compute first Piola-Kirchoff tensor
    % Neo-Hookean
    %Finv = inv(F);
    %P = mu * (F - Finv');
    % Mooney
    P = mu * F;
    
    % internal forces
    fint = area * P * Dm_inv';
    
    % gradient
    grad = [fint(1,1) fint(2,1) fint(1,2) fint(2,2)]' + f;
end

