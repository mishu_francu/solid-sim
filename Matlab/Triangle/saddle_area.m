function [f, J] = saddle_area(z)
    global lambda mu area
    x = z(1:4);
    y = z(5);
    G = 0.5 * [x(4); -x(3); -x(2); x(1)];
    grad = total_gradient_dist(x);
    %grad_verify = finite_diff_gradient(@total_energy_dist, x)
    f = zeros(5,1);
    J = y / area;
    gradU = lambda * (J - 1) - mu / J;
    f(1:4) = grad + gradU * G;
    Ds = [x(1) x(3); x(2) x(4)];
    a = 0.5 * det(Ds);    
    f(5) = a - y;
    
    J = hessian_area(z);
end

