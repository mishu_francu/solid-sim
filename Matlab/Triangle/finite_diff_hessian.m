% compute Hessian using finite differences
function H = finite_diff_hessian(grad, x)
    H = zeros(4, 4);
    for i = 1:4
        dir = zeros(4,1);
        dir(i) = 1;
        H(:,i) = finite_diff_dir_deriv(grad, x, dir);
    end
end

