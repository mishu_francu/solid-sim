% force differential
function df = force_differential(x, dx)
    global Dm_inv area mu lambda f
    F = deformation_gradient(x);
    % compute diff spatial matrix
    dDs = [dx(1) dx(3); dx(2) dx(4)];
    % compute diff F
    dF = dDs * Dm_inv;    
    % compute J and its log
    J = deformation_jacobian(F);
    logJ = log(J);
    % Neo-Hookean
    Finv = inv(F);
    dP = mu * dF + (mu - lambda * logJ) * Finv' * dF' * Finv' + ...
        lambda * trace(Finv * dF) * Finv';
    M = -area * dP * Dm_inv;
    df = reshape(M, [4, 1]);
end

