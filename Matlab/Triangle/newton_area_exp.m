function z = newton_area_exp(z0)
    z = z0;
    for iter=1:200
        r = saddle_area(z);
        H = hessian_area(z);
        dz = -H\r;
        z(1:4) = z(1:4) + dz(1:4);
        z(5) = z(5) * exp(dz(5));
    end
end