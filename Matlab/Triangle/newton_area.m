function z = newton_area(z0)
    z = z0;
    for iter=1:100
        r = saddle_area(z);
        H = hessian_area(z);
        dz = -H\r;
        z = z + dz;
    end
end