function H = hessian_area(z)
    % FIXME
    global mu lambda area
    Kd = finite_diff_hessian(@total_gradient_dist, z(1:4));
    k = lambda / area;
    G = 0.5 * [z(4); -z(3); -z(2); z(1)];
    Ha = [ 0 0 0 1;
        0 0 -1 0;
        0 -1 0 0;
        1 0 0 0];
    y = z(5);
    J = y / area;
    gradU = lambda * (J - 1) - mu / J;    
    Kg = Ha * gradU;    
    hessU = k - mu * area / y^2; 
    H = [Kd + Kg hessU * G; G' -1];
end