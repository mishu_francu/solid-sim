close all;

nu = [0.499
0.49
0.48
0.45
0.4
0.3
0.2
0.1
0];

mixed = [0.042396
0.044802
0.048382
0.054841
0.060221
0.059797
0.056602
0.052375
0.04765];

std = [0.004557
0.023466
0.031545
0.045729
0.056831
0.065123
0.067425
0.066941
0.064427];

mixedHiLin = [0.082128
0.086736
0.088484
0.089272
0.087748
0.083419
0.079071
0.074851
0.070671];

ri = 0.2;
ro = 0.266;
r = 0.236;
p = 10000;
E = 66000;

c1 = p * ri^2 * (1 + nu) .* (2 * nu - 1) / E / (ri^2 - ro^2);
c2 = -p * ri^2 * ro^2 * (1 + nu) / E / (ri^2 - ro^2);
u = c1 * r + c2 / r;

figure;
plot(nu, 100 * mixed, nu, 100 * std);
xlabel('Poisson ratio');
ylabel('Average radius increase [cm]');
legend('mixed', 'standard');
grid on

figure; % linear
plot(nu, 100 * mixedHiLin, nu, 100 * u);
xlabel('Poisson ratio');
ylabel('Average radius increase [cm]');
legend('mixed', 'analytic');
grid on
