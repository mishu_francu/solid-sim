clc;
clear;

% pathtodata = '../../make/out/x64/Measurements/';
pathtodata = '../../results/';

% testcasename = 'pot_energy_hanging_spine_low_gravity';
% testcasename = 'hanging cantilever'
testcasename = 'comprBlock_fixedD_20_499'
% testcasename = 'e2'
pathtoworkingfolder = strcat(pathtodata, testcasename, '/')

%% Energy over time

root_content = dir(pathtoworkingfolder);
% I am assuming that the first two entries always are '.' and '..'
root_content(1) = [];
root_content(1) = [];
no_plots = size(root_content, 1);

for i=1:no_plots
    plotdir = strcat(pathtoworkingfolder, root_content(i).name, '/');
    
    legends = dir(plotdir);
    % I am assuming that the first two entries always are '.' and '..'
    legends(1) = [];
    legends(1) = [];
    
    no_legends = size(legends, 1);
    figure
    for j=1:no_legends
        
        legenddir = strcat(plotdir, legends(j).name, '/');
        if exist(legenddir) == 7
            fileID_dt = fopen(strcat(legenddir, 'dt.txt'));
            fileID_gvol = fopen(strcat(legenddir, 'energy0.txt'));

            dts = fscanf(fileID_dt, '%f'); fclose(fileID_dt);
            energy = fscanf(fileID_gvol, '%f'); fclose(fileID_gvol);
    %         norm_gvol = gvol ./ gvol(1,1)

            plot(dts, energy, 'DisplayName', legends(j).name);
            hold on
        end
    end
    xlabel('Normalized Time');
    ylabel('Energy');
    title(['Energy over time' testcasename ' ' root_content(i).name])
    legend show
end

%% Energy over deflection

root_content = dir(pathtoworkingfolder);
% I am assuming that the first two entries always are '.' and '..'
root_content(1) = [];
root_content(1) = [];
no_plots = size(root_content, 1);

for i=1:no_plots
    plotdir = strcat(pathtoworkingfolder, root_content(i).name, '/');
    
    legends = dir(plotdir);
    % I am assuming that the first two entries always are '.' and '..'
    legends(1) = [];
    legends(1) = [];
    
    no_legends = size(legends, 1);
    figure
    for j=1:no_legends
        
        % // level 0 -> elastic
        % // level 1 -> gravitational
        % // level 2 -> vol
        % // level 3 -> kinetic

        
        legenddir = strcat(plotdir, legends(j).name, '/');
        if exist(legenddir) == 7
            fileID_dt = fopen(strcat(legenddir, 'dt.txt'));
            fileID_energy0 = fopen(strcat(legenddir, 'energy0.txt'));
            fileID_energy1 = fopen(strcat(legenddir, 'energy1.txt'));
            fileID_energy2 = fopen(strcat(legenddir, 'energy2.txt'));
            fileID_energy3 = fopen(strcat(legenddir, 'energy3.txt'));

            dts = fscanf(fileID_dt, '%f'); fclose(fileID_dt);
            energy0 = fscanf(fileID_energy0, '%f'); fclose(fileID_energy0);
            energy1 = fscanf(fileID_energy1, '%f'); fclose(fileID_energy1);
            energy2 = fscanf(fileID_energy2, '%f'); fclose(fileID_energy2);
            energy3 = fscanf(fileID_energy3, '%f'); fclose(fileID_energy3);

            fileID_point = fopen(strcat(legenddir, 'deflectionpoint.txt'));        

            point = fscanf(fileID_point, '%f'); fclose(fileID_point);
            point = reshape(point, 3, [])';

            energy = energy0-energy1+energy2;
            ys = abs(point(:,2) - point(1,2));
            endpoint_x = ys(end)
            endpoint_y = energy(end)
            plot(ys, energy, 'DisplayName', legends(j).name);
            hold on
            plot(endpoint_x, endpoint_y, 'g+','HandleVisibility','off')
            hold on
        end
    end
    grid on
    xlabel('y deflection of endpoint');
    ylabel('Potential energy');
%     title(['Energy over deflection ' testcasename ' ' root_content(i).name])
    legend show
    legend('Location','northwest')
end


%% Deflection - point

root_content = dir(pathtoworkingfolder);
% I am assuming that the first two entries always are '.' and '..'
root_content(1) = [];
root_content(1) = [];
no_plots = size(root_content, 1);

for i=1:no_plots
    plotdir = strcat(pathtoworkingfolder, root_content(i).name, '/');
    
    legends = dir(plotdir);
    % I am assuming that the first two entries always are '.' and '..'
    legends(1) = [];
    legends(1) = [];
    
    no_legends = size(legends, 1);
    figure
    for j=1:no_legends
        
        legenddir = strcat(plotdir, legends(j).name, '/');
        if exist(legenddir) == 7
            fileID_point = fopen(strcat(legenddir, 'deflectionpoint.txt'));        

            point = fscanf(fileID_point, '%f'); fclose(fileID_point);
            point = reshape(point, 3, [])'

    %         scatter3(point(:,1)*0, point(:,2), point(:,3), 'DisplayName', legends(j).name);
            plot(point(:,3), point(:,2), 'DisplayName', legends(j).name);
            hold on
        end
    end
    set(gca, 'XDir','reverse');
%     set(gca, 'YDir','reverse');
    xlabel('z');
    ylabel('y');
    title(['Deflection of endpoint ' testcasename ' ' root_content(i).name])
    legend show
end

%% Deflection - curve

root_content = dir(pathtoworkingfolder);
% I am assuming that the first two entries always are '.' and '..'
root_content(1) = [];
root_content(1) = [];
no_plots = size(root_content, 1);

for i=2:2%no_plots
    plotdir = strcat(pathtoworkingfolder, root_content(i).name, '/');
    
    legends = dir(plotdir);
    % I am assuming that the first two entries always are '.' and '..'
    legends(1) = [];
    legends(1) = [];
    
    no_legends = size(legends, 1);
    figure
    for j=1:no_legends
        
        legenddir = strcat(plotdir, legends(j).name, '/');
        if exist(legenddir) == 7
            all_curves = load(strcat(legenddir, 'deflectioncurve.txt'));
            last_curve = all_curves(size(all_curves,1),:);
%             last_curve = all_curves(1,:);
            last_curve = reshape(last_curve, 2, [])';

            plot(last_curve(:,1), last_curve(:,2), 'DisplayName', legends(j).name);
            hold on
        end
    end
    grid on
    xlim([last_curve(1,1) last_curve(end,1)])
%     title(['Deflection ' testcasename ' ' root_content(i).name])
    xlabel('x')
    ylabel('y')
    legend show
end



%% Global volume
pathtoglobalvolume = strcat(pathtodata, testcasename, '/');

root_content = dir(pathtoglobalvolume);
% I am assuming that the first two entries always are '.' and '..'
root_content(1) = [];
root_content(1) = [];
no_plots = size(root_content, 1);

for i=1:no_plots
    plotdir = strcat(pathtoglobalvolume, root_content(i).name, '/');
    
    legends = dir(plotdir);
    % I am assuming that the first two entries always are '.' and '..'
    legends(1) = [];
    legends(1) = [];
    
    no_legends = size(legends, 1);
    figure
    for j=1:no_legends
        
        legenddir = strcat(plotdir, legends(j).name, '/');
        if exist(legenddir) == 7
            fileID_dt = fopen(strcat(legenddir, 'dt.txt'));
            fileID_gvol = fopen(strcat(legenddir, 'gvolume.txt'));

            dts = fscanf(fileID_dt, '%f'); fclose(fileID_dt);
            gvol = fscanf(fileID_gvol, '%f'); fclose(fileID_gvol);
            norm_gvol = gvol ./ gvol(1,1);

            plot(dts, norm_gvol, 'DisplayName', legends(j).name);
            hold on
        end
    end
    xlabel('Normalized Time');
    ylabel('Normalized Volume');
%     title(['vol ' testcasename ' ' root_content(i).name])
    legend show
end





%% Z width
pathtoglobalvolume = strcat(pathtodata, testcasename, '/');

root_content = dir(pathtoglobalvolume);
% I am assuming that the first two entries always are '.' and '..'
root_content(1) = [];
root_content(1) = [];
no_plots = size(root_content, 1);

for i=1:no_plots
    plotdir = strcat(pathtoglobalvolume, root_content(i).name, '/');
    
    legends = dir(plotdir);
    % I am assuming that the first two entries always are '.' and '..'
    legends(1) = [];
    legends(1) = [];
    
    no_legends = size(legends, 1);
    figure
    for j=1:no_legends
        
        legenddir = strcat(plotdir, legends(j).name, '/');
        fileID_dt = fopen(strcat(legenddir, 'dt.txt'));
        fileID_gvol = fopen(strcat(legenddir, 'widthZ.txt'));
        
        dts = fscanf(fileID_dt, '%f'); fclose(fileID_dt);
        gvol = fscanf(fileID_gvol, '%f'); fclose(fileID_gvol);
        
        
        plot(dts, gvol, 'DisplayName', legends(j).name);
        hold on
    end
    
    title(['WidthZ ' testcasename ' ' root_content(i).name])
    legend show
end


pathtoglobalvolume = strcat(pathtodata, testcasename, '/');

root_content = dir(pathtoglobalvolume);
% I am assuming that the first two entries always are '.' and '..'
root_content(1) = [];
root_content(1) = [];
no_plots = size(root_content, 1);

for i=1:no_plots
    plotdir = strcat(pathtoglobalvolume, root_content(i).name, '/');
    
    legends = dir(plotdir);
    % I am assuming that the first two entries always are '.' and '..'
    legends(1) = [];
    legends(1) = [];
    
    no_legends = size(legends, 1);
    figure
    for j=1:no_legends
        
        legenddir = strcat(plotdir, legends(j).name, '/');
        fileID_dt = fopen(strcat(legenddir, 'dt.txt'));
        fileID_gvol = fopen(strcat(legenddir, 'widthX.txt'));
        
        dts = fscanf(fileID_dt, '%f'); fclose(fileID_dt);
        gvol = fscanf(fileID_gvol, '%f'); fclose(fileID_gvol);
        
        
        plot(dts, gvol, 'DisplayName', legends(j).name);
        hold on
    end
    
    title(['WidthX ' testcasename ' ' root_content(i).name])
    legend show
end

pathtoglobalvolume = strcat(pathtodata, testcasename, '/');

root_content = dir(pathtoglobalvolume);
% I am assuming that the first two entries always are '.' and '..'
root_content(1) = [];
root_content(1) = [];
no_plots = size(root_content, 1);

for i=1:no_plots
    plotdir = strcat(pathtoglobalvolume, root_content(i).name, '/');
    
    legends = dir(plotdir);
    % I am assuming that the first two entries always are '.' and '..'
    legends(1) = [];
    legends(1) = [];
    
    no_legends = size(legends, 1);
    figure
    for j=1:no_legends
        
        legenddir = strcat(plotdir, legends(j).name, '/');
        fileID_dt = fopen(strcat(legenddir, 'dt.txt'));
        fileID_gvol = fopen(strcat(legenddir, 'widthY.txt'));
        
        dts = fscanf(fileID_dt, '%f'); fclose(fileID_dt);
        gvol = fscanf(fileID_gvol, '%f'); fclose(fileID_gvol);
        
        
        plot(dts, gvol, 'DisplayName', legends(j).name);
        hold on
    end
    
    title(['WidthY ' testcasename ' ' root_content(i).name])
    legend show
end



