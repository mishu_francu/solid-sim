close all

volDual = [
10.000000
3.129686
0.881702
10.374727
2.832768
1.377814
1.146557
0.853007
9.733133
2.024009
1.327366
1.154346
0.938370
9.463547
1.687450
1.332075
1.096664
0.999598
9.231071
1.446391
1.325616
1.034176
1.008949
0.842141
9.083727
2.176992
1.111352
1.126476
0.875780
8.697457
1.286150
1.343599
0.987893
8.799874
2.068873
1.305403
1.251582
1.047531
1.070840
0.894104
8.367920
1.253611
1.267747
1.011743
1.020268
0.899747
8.412061
1.861018
1.183162
1.150322
1.000730
1.038276
0.883665
];

volNewton = [
10.00
0.83
10.50
0.75
10.07
0.65
9.67
0.56
9.32
0.49
9.01
0.44
8.74
0.39
8.50
0.35
8.28
0.32
8.08
0.29
];

figure
hold on
grid on
box on
xlabel('Quasi-static dual steps');
ylabel('Relative volume error (%)');
xlim([1 44]);
ylim([0 11]);
plot(1:3, volDual(1:3), 'g', 'LineWidth', 2)
line([3 3], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(3:7, volDual(4:8), 'g', 'LineWidth', 2)
line([7 7], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(7:11, volDual(9:13), 'g', 'LineWidth', 2)
line([11 11], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(11:15, volDual(14:18), 'g', 'LineWidth', 2)
line([15 15], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(15:20, volDual(19:24), 'g', 'LineWidth', 2)
line([20 20], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(20:24, volDual(25:29), 'g', 'LineWidth', 2)
line([24 24], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(24:27, volDual(30:33), 'g', 'LineWidth', 2)
line([27 27], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(27:33, volDual(34:40), 'g', 'LineWidth', 2)
line([33 33], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(33:38, volDual(41:46), 'g', 'LineWidth', 2)
line([38 38], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(38:44, volDual(47:53), 'g', 'LineWidth', 2)

figure
hold on
grid on
box on
xlabel('Quasi-static dual steps');
ylabel('Relative volume error (%)');
xlim([1 11]);
ylim([0 11]);
plot(1:2, volNewton(1:2), 'r', 'LineWidth', 2)
line([2 2], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(2:3, volNewton(3:4), 'r', 'LineWidth', 2)
line([3 3], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(3:4, volNewton(5:6), 'r', 'LineWidth', 2)
line([4 4], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(4:5, volNewton(7:8), 'r', 'LineWidth', 2)
line([5 5], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(5:6, volNewton(9:10), 'r', 'LineWidth', 2)
line([6 6], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(6:7, volNewton(11:12), 'r', 'LineWidth', 2)
line([7 7], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(7:8, volNewton(13:14), 'r', 'LineWidth', 2)
line([8 8], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(8:9, volNewton(15:16), 'r', 'LineWidth', 2)
line([9 9], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(9:10, volNewton(17:18), 'r', 'LineWidth', 2)
line([10 10], [0 11], 'LineStyle', '--', 'Color', 'k');
plot(10:11, volNewton(19:20), 'r', 'LineWidth', 2)

