function [K, F] = assemble(n, E, h, m, fbar)
%ASSEMBLE builds a tangent stiffness matrix K and a forcing vector F
% n is the number of points
% E is Young's modulus
% h is the length of an element
% m is the order of the shape functions
% fbar is the slope of the forcing function
    
    global p xi w;
    % assemble the stiffness matrix and the forcing vector
    K = zeros(n, n); % tangent stiffness matrix
    F = zeros(n, 1); % forcing vector
    % iterate over all elements
    numElem = (n-1) / m; % the number of elements of order m
    for e=1:numElem
        % there are m+1 nodes per element
        Klocal = zeros(m+1, m+1);
        Flocal = zeros(m+1, 1);
        
        % compute the global x coordinate of the nodes
        start = (e-1) * m + 1;
        globalIdx = start:start+m;
        globalPos = (globalIdx' - 1) * h / m; 
        
        % Gaussian quadrature of Klocal and Flocal
        for q=1:p      
            % construct current shape and gradient matrices at the current
            % quadrature point
            N = zeros(1, m+1);
            Ngrad = zeros(1, m+1);
            for j=1:m+1
                N(j) = basis(m, j, xi(q));
                Ngrad(j) = basis_grad(m, j, xi(q));
            end
            
            % local stiffness matrix
            Klocal = Klocal + w(q) * 2 * E / h * (Ngrad' * Ngrad); % this is constant as long as h is constant

            % compute x at this quadrature point
            x = N * globalPos;

            % forcing is f(x) = x * fbar        
            f = fbar * x;

            % local forcing vector            
            Flocal = Flocal + w(q) * h * 0.5 * N' * f;
        end    

        % add the contribution to the global matrix/vector
        for k=1:m+1
            idxK = globalIdx(k);           
            for l=1:m+1
                idxL = globalIdx(l);
                K(idxK, idxL) = K(idxK, idxL) + Klocal(k, l);
            end
            F(idxK, 1) = F(idxK, 1) + Flocal(k);
        end        
    end
end