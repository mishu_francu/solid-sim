function [F] = assemble_forces(n, h, m, fbar)
%ASSEMBLE builds a forcing vector F
%   n is the number of points
%   h is the length of an element
%   m is the order of the shape functions
%   fbar is the slope of the forcing function
    
    global p xi w;
    % assemble the stiffness matrix and the forcing vector
    F = zeros(n, 1); % forcing vector
    % iterate over all elements
    numElem = (n-1) / m; % the number of elements of order m
    for e=1:numElem
        % there are m+1 nodes per element
        Flocal = zeros(m+1, 1);
        
        % compute the global x coordinate of the nodes
        start = (e-1) * m + 1;
        globalIdx = start:start+m;        
        globalPos = (globalIdx' - 1) * h / m; 
        
        % Gaussian quadrature of Klocal and Flocal
        for q=1:p      
            % construct shape gradient matrices at the current point
            N = zeros(1, m+1);
            for j=1:m+1
                N(j) = basis(m, j, xi(q));
            end
            
            % compute x at this quadrature point
            x = N * globalPos;

            % forcing is f(x) = x * fbar        
            f = fbar * x;

            % local forcing vector            
            Flocal = Flocal + w(q) * h * 0.5 * N' * f;
        end    

        % add the contribution to the global vector
        for k=1:m+1
            idxK = globalIdx(k);           
            F(idxK, 1) = F(idxK, 1) + Flocal(k);
        end        
    end
end