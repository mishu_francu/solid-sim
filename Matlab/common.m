% global constants
clear variables global;
close all;
N = 10; % number of elements
order = 3; % order of basis functions for displacements
orderS = 2; % order of basis functions for stresses
n = order * N + 1; % number of displacement nodes
ns = max(orderS * N + 1, N); % number of stress nodes
E = 1e11; % Young's modulus [Pa]
L = 0.1; % total length [m]
h = L / N; % length of each element
dx = L / (n-1); % distance between nodes
t = 1e11; % endpoint traction B.C. [Pa]
problem = 1; % 1 - pure Dirichlet, 2 - Dirichlet-Neumann
g1 = 0; % start point Dirichlet B.C. [m]
g2 = 0.001; % end point Dirichlet B.C. [m]
fbar = 1e11; % slope of forcing function
global p xi w;
p = 5; % the quadrature rule order

% quadrature points and weights up to order 3
if (p == 1)
    w = 2;
    xi = 0;
else
    if (p == 2)
        w = [1 1];
        xi = [-1/sqrt(3) 1/sqrt(3)];
    else
        if (p == 3)
            xi = [-sqrt(3/5) 0 sqrt(3/5)];
            w = [5/9 8/9 5/9];
        else
            if (p == 4)
                xi = [-0.861136 -0.339981 0.339981 0.861136];
                w = [0.347855 0.652145 0.652145 0.347855];
            else
                xi = [-0.90618 -0.538469 0 0.538469 0.90618];
                w = [0.236927 0.478629 0.568889 0.478629 0.236927];
            end            
        end        
    end            
end

% for the dynamics case:
% data
density = 1; % constant density
dt = 0.001; % the integration time-step
numSteps = 500; % the number of integration steps
x = 0:dx:L;

% initial conditions
ui = zeros(n, 1); % the displacement field
vi = zeros(n, 1); % the velocity field
ui(end) = g2;

