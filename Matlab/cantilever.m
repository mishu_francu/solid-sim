close all

lenL = [0.1, 0.05, 0.0333, 0.025, 0.0125, 0.01];
lenQ = [0.1, 0.05, 0.0333, 0.025, 0.0125];
lenC = [0.1, 0.05, 0.0333, 0.025, 0.0125, 0.01];
lenS = [0.1, 0.05, 0.0333, 0.025, 0.02, 0.0125, 0.01, 0.009, 0.008333, 0.0077, 0.00714, 0.00666, 0.00625, 0.00588, 0.00555];

%(points)
nodesL = [44, 189, 496, 1025, 6561, 12221];
nodesQ = [189, 1025, 2989, 6561, 46529];
nodesC = [496, 2989, 9100, 20449, 160625, 289261];
nodesS = [44, 189, 496, 1025, 1836, 6561, 12221, 15984, 20449, 25676, 31725, 38656, 46529, 55404, 65341];

%(cells) 
elemsL = [60, 480, 1620, 3840, 30720, 60000];
elemsQ = [60, 480, 1620, 3840, 30720];
elemsC = [60, 480, 1620, 3840, 30720, 60000];
elemsS = [60, 480, 1620, 3840, 7500, 30720, 60000, 79860, 103680, 131820, 164640, 202500, 245760, 294780, 349920];

linear = [
-0.485953
-0.652178
-0.733363
-0.755482
-0.786175
-0.790559
];

quadratic = [
-0.754854    
-0.790744
-0.796405
-0.798268
-0.800058
];

cubic = [
-0.778928
-0.795289
-0.798117
-0.799124
-0.800178
-0.800346
];

mixed = [
-0.519939
-0.66522
-0.738439
-0.760334
-0.772438
-0.78642
-0.793196
-0.796066
-0.795553
-0.796556
-0.797014
-0.794469
-0.794756
-0.795174
-0.800001
];

figure
plot(-log(lenS(1:end-4)), mixed(1:end-4), '-o', -log(lenC), cubic, '-o', -log(lenQ), quadratic, '-o', 'LineWidth', 1.5)
grid on
xlabel('log(h)')
ylabel('deflection')
legend('ours', 'cubic', 'quadratic')

figure
plot(log10(4 * nodesS(1:end-4)), 100 * mixed(1:end-4), '-o', log10(3 * nodesC), 100 * cubic, '-o', log10(3 * nodesQ), 100 * quadratic, '-o', 'LineWidth', 1.5)
grid on
xlabel('log(#dofs)')
ylabel('deflection (cm)')
legend('ours', 'cubic', 'quadratic')

%figure
%plot(log10(elemsL), linear)

figure
plot(log10(elemsS(1:end-4)), 100 * mixed(1:end-4), '-o', log10(elemsC), 100 * cubic, '-o', log10(elemsQ), 100 * quadratic, '-o', 'LineWidth', 1.5)
grid on
xlabel('log(#elems)')
ylabel('deflection (cm)')
legend('ours', 'cubic', 'quadratic')
