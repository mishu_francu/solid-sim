function D = shapeMatrix3D(pos, mesh3D, idx)
    i1 = mesh3D.Tets(idx,1) - 1;
    i2 = mesh3D.Tets(idx,2) - 1;
    i3 = mesh3D.Tets(idx,3) - 1;
    i4 = mesh3D.Tets(idx,4) - 1;
    x1 = pos(i1 * 3 + 1);
    y1 = pos(i1 * 3 + 2);
    z1 = pos(i1 * 3 + 3);
    x2 = pos(i2 * 3 + 1);
    y2 = pos(i2 * 3 + 2);
    z2 = pos(i2 * 3 + 3);
    x3 = pos(i3 * 3 + 1);
    y3 = pos(i3 * 3 + 2);
    z3 = pos(i3 * 3 + 3);
    x4 = pos(i4 * 3 + 1);
    y4 = pos(i4 * 3 + 2);
    z4 = pos(i4 * 3 + 3);
    D = [x2 - x1, x3 - x1, x4 - x1; 
        y2 - y1, y3 - y1, y4 - y1; 
        z2 - z1, z3 - z1, z4 - z1];
end

