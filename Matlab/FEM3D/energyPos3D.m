function E = energyPos3D(pos, mesh3D)
    E = 0;
    inverted = false;
    for i = 1:size(mesh3D.Tets,1)
        Ds = shapeMatrix3D(pos, mesh3D, i);
        DmInv = mesh3D.DmInv(:,:,i);
        F = Ds * DmInv;
        J = det(F);
        if J <= 0
            inverted = true;
        end
        logJ = log(J);
        C = F' * F;
        I1 = trace(C);
        localE = 0.5 * mesh3D.Mu * (I1 - 3) - mesh3D.Mu * logJ + 0.5 * mesh3D.Lambda * (J - 1)^2;
        E = E + mesh3D.Volume(i) * localE;
    end
    if inverted
        disp('inverted')
    end
end