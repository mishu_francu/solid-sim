function E = energyStretches3D(s, mesh)
% elastic energy in principal stretches
    E = 0;
    for i = 1:size(mesh.Tets,1)
        s1 = s((i-1) * 3 + 1);
        s2 = s((i-1) * 3 + 2);
        s3 = s((i-1) * 3 + 3);
        J = s1 * s2 * s3;
        I1 = s1^2 + s2^2 + s3^2;
        psi = 0.5 * mesh.Mu * (I1 - 3) - mesh.Mu * log(J) + 0.5 * mesh.Lambda * (J - 1)^2;
        E = E + mesh.Volume(i) * psi;
    end
end
    
