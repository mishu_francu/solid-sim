function fe = gradStd3D(x, mesh)
    freeIndices = mesh.FreeIndices;
    pos = mesh.RefPos;
    pos(freeIndices) = x;

    fe = zeros(size(pos));
    for i = 1:size(mesh.Tets,1)
        Ds = shapeMatrix3D(pos, mesh, i);
        DmInv = mesh.DmInv(:,:,i);
        F = Ds * DmInv;
        J = det(F);
        
        Finv = inv(F);
        P = mesh.Mu * (F - Finv') + mesh.Lambda * (J - 1) * J * Finv'; % PK1 tensor
        localF = -mesh.Volume(i) * P * DmInv';
        f2 = localF(:,1);
        f3 = localF(:,2);
        f4 = localF(:,3);
        f1 = -f2 - f3 - f4;
        i1 = mesh.Tets(i, 1) - 1;
        i2 = mesh.Tets(i, 2) - 1;
        i3 = mesh.Tets(i, 3) - 1;
        i4 = mesh.Tets(i, 4) - 1;
        fe(i1 * 3 + 1) = fe(i1 * 3 + 1) + f1(1);
        fe(i1 * 3 + 2) = fe(i1 * 3 + 2) + f1(2);
        fe(i1 * 3 + 3) = fe(i1 * 3 + 3) + f1(3);
        fe(i2 * 3 + 1) = fe(i2 * 3 + 1) + f2(1);
        fe(i2 * 3 + 2) = fe(i2 * 3 + 2) + f2(2);
        fe(i2 * 3 + 3) = fe(i2 * 3 + 3) + f2(3);
        fe(i3 * 3 + 1) = fe(i3 * 3 + 1) + f3(1);
        fe(i3 * 3 + 2) = fe(i3 * 3 + 2) + f3(2);
        fe(i3 * 3 + 3) = fe(i3 * 3 + 3) + f3(3);
        fe(i4 * 3 + 1) = fe(i4 * 3 + 1) + f4(1);
        fe(i4 * 3 + 2) = fe(i4 * 3 + 2) + f4(2);
        fe(i4 * 3 + 3) = fe(i4 * 3 + 3) + f4(3);
    end
end
