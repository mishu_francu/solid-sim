function innerVol = innerVol3D(pos, mesh)
    innerVol = 0;
    for i=1:size(mesh.InnerTris,1)
        i2 = mesh.InnerTris(i,1) - 1;
        i3 = mesh.InnerTris(i,2) - 1;
        i4 = mesh.InnerTris(i,3) - 1;
        x1 = 0;
        y1 = 0;
        z1 = 0;
        x2 = pos(i2 * 3 + 1);
        y2 = pos(i2 * 3 + 2);
        z2 = pos(i2 * 3 + 3);
        x3 = pos(i3 * 3 + 1);
        y3 = pos(i3 * 3 + 2);
        z3 = pos(i3 * 3 + 3);
        x4 = pos(i4 * 3 + 1);
        y4 = pos(i4 * 3 + 2);
        z4 = pos(i4 * 3 + 3);
        D = [x2 - x1, x3 - x1, x4 - x1; 
            y2 - y1, y3 - y1, y4 - y1; 
            z2 - z1, z3 - z1, z4 - z1];
        innerVol = innerVol - det(D) / 6;
    end
end