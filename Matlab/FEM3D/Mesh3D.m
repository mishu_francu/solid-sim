classdef Mesh3D
    properties
        Tets
        DmInv
        RefPos
        FreeIndices
        Loads
        Volume
        Mu
        Lambda
        InnerTris
        InnerVol
    end
    methods
        function mesh = Mesh3D(tets, pos)
            mesh.Tets = tets;
            mesh.RefPos = pos;
            m = size(mesh.Tets,1);
            mesh.Volume = zeros(m, 1);
            for i = 1:m
                Dm = shapeMatrix3D(pos, mesh, i);
                mesh.DmInv(:,:,i) = inv(Dm);
                mesh.Volume(i) = det(Dm) / 6;
            end
        end
    end
end