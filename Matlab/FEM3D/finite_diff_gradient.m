function grad = finite_diff_gradient(energy, x)
    n = size(x,1);
    grad = zeros(n,1);
    E0 = energy(x);
    eps = 1e-3;
    for i=1:n
        basis = zeros(n,1);
        basis(i) = 1;
        grad(i) = (energy(x + eps * basis) - E0) / eps;
    end
end