function [E, grad] = energy3D(x, mesh3D)
    freeIndices = mesh3D.FreeIndices;
    pos = mesh3D.RefPos;
    pos(freeIndices) = x;

    % elastic energy
    E = energyPos3D(pos, mesh3D);
    
    % potential energy of loads
    %p = 1e6;
    U = -pos' * mesh3D.Loads;
    %U = U + p * innerVol3D(pos, mesh3D);

    % compute gradient
    fe = gradStd3D(x, mesh3D);
    fp = zeros(size(pos));
    
    % compute gradient due to pressure
%     for i=1:size(mesh3D.InnerTris,1)
%         i1 = mesh3D.InnerTris(i,1) - 1;
%         i2 = mesh3D.InnerTris(i,2) - 1;
%         i3 = mesh3D.InnerTris(i,3) - 1;
%         p1 = pos(i1 * 3 + 1:i1 * 3 + 3);
%         p2 = pos(i2 * 3 + 1:i2 * 3 + 3);
%         p3 = pos(i3 * 3 + 1:i3 * 3 + 3);
%         a = cross(p2 - p1, p3 - p1) / 2;
%         fp(i1 * 3 + 1: i1 * 3 + 3) = fp(i1 * 3 + 1: i1 * 3 + 3) + p * a / 3;
%         fp(i2 * 3 + 1: i2 * 3 + 3) = fp(i2 * 3 + 1: i2 * 3 + 3) + p * a / 3;
%         fp(i3 * 3 + 1: i3 * 3 + 3) = fp(i3 * 3 + 1: i3 * 3 + 3) + p * a / 3;
%     end    
%     for i=1:size(pos,1)/3
%         posi = pos((i-1)*3+1:(i-1)*3+3);
%         normi = norm(posi);
%         posi = posi / normi;
%         U = U - p * normi;
%         fp((i-1)*3+1:(i-1)*3+3) = posi * p;
%     end
    %U = U - pos' * fp;
    
    %fp = p * finite_diff_gradient(@(pos)innerVol3D(pos, mesh3D), pos);
    
    E = E + U;
    grad = -mesh3D.Loads(freeIndices) - fe(freeIndices) - fp(freeIndices);
end


