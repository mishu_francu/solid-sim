clear variables
close all

nx = 3;
ny = 3;
nz = 3;
n = nx * ny * nz;
segm = 10;

loadTube = false;

% construct the grid, tetrahedralize it and display the tet mesh
xs = segm * (0:nx-1);
ys = segm * (0:ny-1);
zs = segm * (0:nz-1);
[X, Y, Z] = meshgrid(xs,ys,zs);
xlin = reshape(X, [n, 1]);
ylin = reshape(Y, [n, 1]);
zlin = reshape(Z, [n, 1]);
P = [xlin, ylin, zlin];
T = delaunay(xlin, ylin, zlin);

% load the tube mesh
if loadTube
    load("tube")
    T = tube_elems;
    P = tube_points;
    n = size(P,1);

    figure
    trimesh(tube_inner, P(:,1), P(:,2), P(:,3))
    axis vis3d
end

figure
tetramesh(T, P, 'FaceColor', 'r')
xlabel('x')
ylabel('y')
zlabel('z')
axis vis3d

% construct the state vector (x1, y1, z1), (x2, y2, z2), ...
pos = reshape(P', [3 * n, 1]);
defPos = pos;

% boundary conditions
loads = zeros(3 * n, 1);
if loadTube
    numBCs = 46;
    p = 1e-6;
    % innerIndices = unique(tube_inner);
    for i=1:size(P,1)
        %i = innerIndices(j);
        posi = P(i,:);
        posi(3) = 0;
        posi = posi / norm(posi);
        loads((i-1)*3+1:(i-1)*3+3) = posi * p;
    end
else
    numBCs = nx * ny;
    loads(3:3:end) = 40000;
end
freeIndices = 3 * numBCs + 1:3 * n;

% Psub = P(innerIndices,:)
% K = convhull(Psub);
% 
% figure
% trimesh(K, Psub(:,1), Psub(:,2), Psub(:,3));

% construct the mesh object
mesh3D = Mesh3D(T, pos);
mesh3D.FreeIndices = freeIndices;
mesh3D.Loads = loads;
%mesh3D.InnerTris = tube_inner;

% compute inner volume
%mesh3D.InnerVol = innerVol3D(pos, mesh3D);

% material properties
nu = 0.3;
Y = 3000;
mu = 0.5 * Y / (1 + nu);
lambda = nu * Y / (1 + nu) / (1 - 2 * nu);
mesh3D.Mu = mu;
mesh3D.Lambda = lambda;

% solve using standard formulation
x0 = pos(freeIndices);
if true
    disp('### standard')    
    options1 = optimoptions(@fminunc, 'SpecifyObjectiveGradient', false);
    tic
    fun1 = @(x)energy3D(x, mesh3D);
    x = fminunc(fun1, x0, options1);
    toc    
    defPos(freeIndices) = x;
    defP = reshape(defPos, [3, n]);
    figure
    tetramesh(T, defP')
    xlabel('x')
    ylabel('y')
    zlabel('z')
    axis vis3d
end

% solve using principal stretches
numElems = size(T,1);
numFreeDofs = size(x0, 1);
z0 = [x0; ones(numElems * 3,1)]; % 3 singular values per element
lb = zeros(size(z0));
%lb = ones(size(z0)) * 1e-10;
lb(1:size(x0,1)) = -Inf;
if true
    disp('### principal stretches')
    options = optimoptions(@fmincon, 'Algorithm', 'interior-point', ...
        'MaxFunctionEvaluations', 30000, 'SpecifyConstraintGradient', false, ...
        'SpecifyObjectiveGradient', false);%, 'HessianFcn', @hessianfcn);
    tic
    fun3obj = @(z)energyPS3D(z, mesh3D);
    fun3con = @(z)constrPS3D(z, mesh3D);
    z = fmincon(fun3obj, z0, [], [], [], [], lb, [], fun3con, options);
    toc
    defPos(freeIndices) = z(1:numFreeDofs);
    defP = reshape(defPos, [3, n]);
    figure
    tetramesh(T, defP')
    xlabel('x')
    ylabel('y')
    zlabel('z')
    axis vis3d
end

