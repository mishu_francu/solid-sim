function value = basis(order, node, xi)
% BASIS computes the basis function at point xi
%   Uses a Lagrange polynomial basis function
%   'order' is the order of the basis polynomial
%   'node' is the local node index inside the element
%   'xi' is the point at which we evaluate the function

    if (order == 0)
        value = 1; % "zero order" shape function is just 1
    else
        % evaluate Lagrange polynomial
        delta = 2 / order;
        points = -1:delta:1; % xi coordinates of element nodes
        value = 1;
        % go through all element nodes
        for i=1:order+1
            if (i ~= node)
                value = value * (xi - points(i)) / (points(node) - points(i)); 
            end
        end
    end
end

