function E = energy_stretches(s, mesh)
% elastic energy in principal stretches
    E = 0;
    for i = 1:size(mesh.Triangles,1)
        s1 = s((i-1) * 2 + 1);
        s2 = s((i-1) * 2 + 2);
        J = s1 * s2;
        I1 = s1^2 + s2^2;
        psi = 0.5 * mesh.Mu * (I1 - 2) - mesh.Mu * log(J) + 0.5 * mesh.Lambda * (J - 1)^2;
        E = E + mesh.Area(i) * psi;
    end
end
    
