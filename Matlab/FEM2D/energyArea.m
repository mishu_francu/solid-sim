function E = energyArea(y)
    global numFreeNodes bulk area0
    x = y(1:numFreeNodes * 2);
    E = energy(x); % distortional energy + external work
    
    % volumetric energy
    a = y(numFreeNodes * 2 + 1:end);
    U = 0;
    for i=1:size(a,1)
        J = a(i) / area0(i);
        U = U + area0(i) * 0.5 * bulk * (J - 1)^2;
    end
    
    E = E + U;
end

