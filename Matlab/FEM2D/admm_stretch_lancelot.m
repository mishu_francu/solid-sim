function sol = admm_stretch_lancelot(x, s, mesh, options)
    numElems = size(mesh.Triangles,1);
    x_admm = x;
    s_admm = s;
    u_admm = zeros(numElems * 2, 1);
    rho = options.Penalty;
    options_s_con = optimoptions('fmincon', 'Display', 'off', 'SpecifyObjectiveGradient', true);
    options_x = optimoptions('fminunc', 'Display', 'off', 'StepTolerance', 1e-5, 'SpecifyObjectiveGradient', true, 'CheckGradients', false);
    old_merit = 0;
    rel_merit = 1;
    lb = zeros(numElems * 2, 1);
    for iter=1:options.MaxIters
        funS = @(s)aug_lag_stretch(s, x_admm, u_admm, rho, mesh);
        s_admm = fmincon(funS, s_admm, [], [], [], [], lb, [], [], options_s_con);
        funX = @(x)aug_lag_pos(x, s_admm, u_admm, rho, mesh);
        x_admm = fminunc(funX, x_admm, options_x);       
        [~, ceq, ~, ~] = constrPS([x_admm; s_admm], mesh);
        norm1C = vecnorm(ceq, 1);
        norm2C = vecnorm(ceq);
        u_admm = u_admm + ceq;
        maxU = vecnorm(u_admm, Inf);
        new_energy = energyPS([x_admm; s_admm], mesh);
        new_merit = new_energy + rho * maxU * norm1C;   
        if old_merit ~= 0
            rel_merit = abs((new_merit - old_merit) / old_merit);
        end
        old_merit = new_merit;
        if rel_merit < 1e-7 && norm2C < 1e-4
            break
        end
    end        
    iter
    norm2C
    rel_merit
    sol = [x_admm; s_admm];
end