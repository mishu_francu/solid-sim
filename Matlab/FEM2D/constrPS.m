function [c, ceq, GC, GCeq] = constrPS(z, mesh)
    c = [];
    GC = [];
    % extract vars
    freeIndices = mesh.FreeIndices;
    numFreeDofs = size(freeIndices,2);
    x = z(1:numFreeDofs);
    pos = mesh.RefPos;
    pos(freeIndices) = x;
    s = z(numFreeDofs + 1:end);    
    m = size(mesh.Triangles,1);
    ceq = zeros(m * 2, 1);    
    
    % compute the SVDs
    r = zeros(3, 2);
    gradS = zeros(size(pos,1), m * 2);
    for i = 1:m
        Ds = shapeMatrix(pos, i, mesh);
        DmInv = mesh.DmInv(:,:,i);
        F = Ds * DmInv;
        [U, S, V] = signedSVD(F);
        sigma = diag(S);       
        idx = (i-1) * 2;
        ceq(idx + 1) = s(idx + 1) - sigma(1);
        ceq(idx + 2) = s(idx + 2) - sigma(2);
        
        % compute the gradient w.r.t. x (with a minus sign)
        r(2,:) = DmInv(1,:);
        r(3,:) = DmInv(2,:);
        r(1,:) = -r(2,:) - r(3,:);
        for j=1:3
            ds1 = -r(j,:) * V(:,1) * U(:,1);
            ds2 = -r(j,:) * V(:,2) * U(:,2);
            ds = [ds1, ds2];
            gidx = (mesh.Triangles(i,j) - 1) * 2;
            gradS(gidx + 1:gidx + 2, idx + 1:idx + 2) = ds;
        end
    end
    
    GCeq = [gradS(freeIndices,:); eye(m *2)];
    
    % finite diff approx
%     grad = zeros(size(z,1), m * 2);
%     for i=1:size(z,1)
%         pert = zeros(size(z));
%         pert(i) = 1e-3;
%         z1 = z + pert;
%         c1 = constrPSsimple(z1);
%         grad(i,:) = (c1' - ceq') / pert(i);
%     end
%     GCeq = grad;
end

