function grad = grad_stretch(s, mesh, rho, x, u)
    [~, ceq] = constrPS([x; s], mesh);
    c1 = ceq + u;
    %L = energy_stretches(s) + 0.5 * rho * (c1' * c1);

    % gradient
    grad = rho * c1;
    % duplicate code of gradU
    for i=1:size(mesh.Triangles,1)
        idx = (i-1) * 2;
        s1 = s(idx + 1);
        s2 = s(idx + 2);
        psi1 = mesh.Mu * s1 - mesh.Mu / s1 + mesh.Lambda * s2 * (s1 * s2 - 1);
        psi2 = mesh.Mu * s2 - mesh.Mu / s2 + mesh.Lambda * s1 * (s1 * s2 - 1);
        grad(idx + 1) = grad(idx + 1) + mesh.Area(i) * psi1;
        grad(idx + 2) = grad(idx + 2) + mesh.Area(i) * psi2;
    end
end