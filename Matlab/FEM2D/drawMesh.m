function drawMesh(pos, mesh, color, style, marker)
    plot(pos(1:2:end), pos(2:2:end), marker)
    %trimesh(mesh.Triangles, pos(1:2:end), pos(2:2:end));
%     labels = "" + (1:size(pos,1) / 2);
%     text(pos(1:2:end), pos(2:2:end), labels)
    for i = 1:size(mesh.Triangles,1)
        i1 = mesh.Triangles(i,1) - 1;
        i2 = mesh.Triangles(i,2) - 1;
        i3 = mesh.Triangles(i,3) - 1;
        x1 = pos(i1 * 2 + 1);
        y1 = pos(i1 * 2 + 2);
        x2 = pos(i2 * 2 + 1);
        y2 = pos(i2 * 2 + 2);
        x3 = pos(i3 * 2 + 1);
        y3 = pos(i3 * 2 + 2);
        line([x1 x2 x3 x1], [y1 y2 y3 y1], 'Color', color, 'LineStyle', style)
    end
end
