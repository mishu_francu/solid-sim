function L = aug_lag_area(a)
    global rho x_admm u_admm
    [c, ceq] = constrArea([x_admm; a]);
    c1 = ceq + u_admm;
    L = energy_areas(a) + 0.5 * rho * (c1' * c1);
end