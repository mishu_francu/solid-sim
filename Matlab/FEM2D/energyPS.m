function [E, grad] = energyPS(z, mesh)
    freeIndices = mesh.FreeIndices;
    numFreeDofs = size(freeIndices,2);
    % extract vars
    x = z(1:numFreeDofs);
    pos = mesh.RefPos;
    pos(freeIndices) = x;
    s = z(numFreeDofs + 1:end);
    
    E = energy_stretches(s, mesh);
    
    % potential energy of loads
    U = -pos' * mesh.Loads;
    E = E + U;
    
    % gradient
    grad = zeros(size(z));
    grad(1:numFreeDofs) = -mesh.Loads(freeIndices);
    grad(numFreeDofs+1:end) = gradU(s, mesh);   
end

