function ceq = constrPSsimple(z)
    % only used for finite differences
    global numFreeNodes numBCs refPos triangles area0
    % extract vars
    x = z(1:numFreeNodes * 2);
    pos = [refPos(1:numBCs * 2); x];
    s = z(numFreeNodes * 2 + 1:end);    
    m = size(triangles,1);
    ceq = zeros(m * 2, 1);    
    
    % compute the SVDs
    for i = 1:m
        Dm = shapeMatrix(refPos, i);
        area0(i) = det(Dm) / 2;
        Ds = shapeMatrix(pos, i);
        DmInv = inv(Dm);
        F = Ds * DmInv;
        [U, S, V] = svd(F);
        %[U, S, V] = signedSVD(F);
        sigma = diag(S);
        idx = (i-1) * 2;
        ceq(idx + 1) = s(idx + 1) - sigma(1);
        ceq(idx + 2) = s(idx + 2) - sigma(2);        
    end    
end

