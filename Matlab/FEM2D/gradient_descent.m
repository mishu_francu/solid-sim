function [x, iter] = gradient_descent(grad, x0, alpha, max_iter)
%GRADIENT_DESCENT Performs gradient descent optimization
%   Performs optimization given the gradient function and not the objective function
%   x0 - initial guess
%   alpha - the step length
%   max_iter - the maximum number of iterations
    x = x0;
    res = norm(x);
    for iter=1:max_iter
        dx = - alpha * grad(x);
        x = x + dx;
        new_res = norm(x);
        rel_res = (new_res - res) / res;
        if abs(rel_res) < 1e-7
            break        
        end
        res = new_res;
    end
end