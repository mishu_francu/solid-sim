function sol = admm_area(x, a, stiffness, maxIters)
    global rho x_admm a_admm u_admm numElems
    x_admm = x;
    a_admm = a;
    u_admm = zeros(size(a));
    rho = stiffness;
    options_s_con = optimoptions('fmincon', 'Display', 'off');%, 'SpecifyObjectiveGradient', true);
    options_s_unc = optimoptions('fminunc', 'Display', 'off');%, 'SpecifyObjectiveGradient', true);% 'CheckGradients', true);
    options_x = optimoptions('fminunc', 'Display', 'off', 'MaxFunctionEvaluations', 1e4);
    old_energy = energyArea([x_admm; a_admm]);
    old_merit = 0;
    rel_merit = 1;
    threshold = 1e-8;
    lb = zeros(numElems, 1);
    for iter=1:maxIters
        x_admm = fminunc(@aug_lag_pos_area, x_admm, options_x);
        a_admm = fmincon(@aug_lag_area, a_admm, [], [], [], [], lb, [], []);
        %a_admm = fminunc(@aug_lag_area, a_admm);
        %[s_admm, s_iters] = proj_gradient_descent(@grad_stretch, s_admm, 1e-10, 400);
        %[s_admm, s_iters] = gradient_descent_exp(@grad_stretch, s_admm, 1e-10, 400);
        %disp(s_iters)        
        new_energy = energyArea([x_admm; a_admm]);
        rel_diff = abs(new_energy - old_energy) / old_energy;
        %disp(rel_diff)
%         if rel_diff < threshold
%             iter
%             break
%         end
        old_energy = new_energy;
        [c, ceq] = constrArea([x_admm; a_admm]);        
        norm1C = vecnorm(ceq, 1);
        norm2C = vecnorm(ceq);
        u_admm = u_admm + ceq;
        maxU = vecnorm(u_admm, Inf);
        new_merit = new_energy + rho * maxU * norm1C
        if old_merit ~= 0
            rel_merit = abs(new_merit - old_merit) / old_merit;
        end
        old_merit = new_merit;
%         if rel_merit < 1e-6
%             iter
%             break
%         end
    end    
    sol = [x_admm; a_admm];
end