function [f, J] = saddle_ps(y, mesh) 
    freeIndices = mesh.FreeIndices;
    [f, gradS] = grad_lag_ps(y, mesh);
    
    % compute the Jacobian (i.e. Hessian of Lagrangian)
    n = size(y,1);
    numElems = size(mesh.Triangles,1);
    numDofs = size(freeIndices,2);
    numPS = numElems * 2;
    numVars = numDofs + numPS;
    
    % assemble the sparse Jacobian
    J = zeros(n);
    J(numVars+1:end, 1:numDofs) = gradS';
    J(1:numDofs, numVars+1:end) = gradS;
    J(numVars+1:end, numDofs+1:numVars) = eye(numPS);
    J(numDofs+1:numVars, numVars+1:end) = eye(numPS);
    J(1:numVars,1:numVars) = hessian_lag(y(1:numVars), y(numVars+1:end), mesh);
end

