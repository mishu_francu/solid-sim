clear variables
close all

% experiments
% 1 compressed block
% 2 compressed block (fixed Y)
% 3 vertical load
% 4 both ends fixed, vertical load
% 5 inflating balloon
experiment = 2;

% algorithms
% 1 - standard unconstrained
% 2 - standard + constraints (positive PS)
% 3 - principal stretches fmincon
% 4 - principal stretches ALM
% 5 - principal stretches ADMM
% 6 - principal stretches fmicon + exp
% 7 - principle stretches KKT
% 8 - principle stretches reduced KKT
algs = [1, 5];

offsetX = 0;
offsetY = 30;
numPointsX = 6;
numPointsY = 3;
segmSize = 30;
inner = 100;

numNodes = numPointsX * numPointsY;
pos = zeros(numNodes * 2, 1);

nu = 0.4;
Y = 90000;

mu = 0.5 * Y / (1 + nu);
lambda = nu * Y / (1 + nu) / (1 - 2 * nu);
bulk = Y / (1 - 2 * nu) / 3;
loads = zeros(numNodes * 2, 1);

if experiment <= 3
    freeIndices = numPointsY * 2 + 1:size(pos,1);
    if experiment == 2
        endpointsy = size(pos,1) - numPointsY * 2 + 2:2:size(pos,1);
        freeIndices = setdiff(freeIndices, endpointsy);
    end
end
if experiment == 4
    freeIndices = numPointsY * 2 + 1:size(pos,1) - numPointsY * 2;
end
if experiment == 5
    endpoints = size(pos,1) - numPointsY * 2 + 1:2:size(pos,1);
    startpoints = 2:2:numPointsY * 2;
    freeIndices = 1:size(pos,1);
    freeIndices = setdiff(freeIndices, endpoints);
    freeIndices = setdiff(freeIndices, startpoints);
end
fixedIndices = setdiff(1:size(pos,1), freeIndices);
numFreeDofs = size(freeIndices,2);

% grid points
for i = 1:numPointsX
    for j = 1:numPointsY
        idx = (i-1) * numPointsY + j - 1;
        if experiment <= 4
            pos(idx * 2 + 1) = offsetX + i * segmSize;
            pos(idx * 2 + 2) = offsetY + j * segmSize;
            if experiment <= 2% && i == numPointsX
                loads(idx * 2 + 1) = -100000; % horizontal load
            end
            if experiment >= 3 && j == 1
                loads(idx * 2 + 2) = -1000000; % vertical load
            end
        else
            r = inner + j * segmSize;
            theta = (i - 1) / (numPointsX - 1) * pi / 2;
            pos(idx * 2 + 1) = r * cos(theta);
            pos(idx * 2 + 2) = r * sin(theta);
            l = 1000;
            if j == 1
                loads(idx * 2 + 1) = l * cos(theta);
                loads(idx * 2 + 2) = l * sin(theta);
            end
        end
    end
end

mesh = Mesh(numPointsX, numPointsY, mu, lambda, pos);
mesh.Loads = loads;
mesh.FreeIndices = freeIndices;
numElems = size(mesh.Triangles,1);

figure
axis equal
hold on
% draw the mesh
drawMesh(pos, mesh, 'k', ':', 'k.');

% solve using standard formulation
x0 = pos(freeIndices);
defPos = mesh.RefPos;
if ismember(1, algs)
    disp('### standard')    
    options1 = optimoptions(@fminunc, 'SpecifyObjectiveGradient', true, 'Algorithm', 'trust-region');
    tic
    fun1 = @(x)energy(x, mesh);
    x = fminunc(fun1, x0, options1);
    toc    
    defPos(freeIndices) = x;
    drawMesh(defPos, mesh, 'r', '-', 'ro');
end
 
% solve using standard formulation + area constraints
if ismember(2, algs)
    disp('### std + constr');
    options2 = optimoptions(@fmincon, 'Algorithm', 'sqp', 'MaxFunctionEvaluations', 20000, 'SpecifyObjectiveGradient', true);
    fun2obj = @(x)energy(x, mesh);
    fun2con = @(x)constrAreaPos(x, mesh);
    x = fmincon(fun2obj, x0, [], [], [], [], [], [], fun2con, options2);
    defPos(freeIndices) = x;
    drawMesh(defPos, mesh, 'g', '-', 'go');
end

% solve using principal stretches
z0 = [x0; ones(numElems * 2,1)]; % 2 singular values per element
lb = zeros(size(z0));
%lb = ones(size(z0)) * 1e-10;
lb(1:size(x0,1)) = -Inf;
if ismember(3, algs)
    disp('### principal stretches')
    options = optimoptions(@fmincon, 'Algorithm', 'interior-point', ...
        'MaxFunctionEvaluations', 20000, 'SpecifyConstraintGradient', true, ...
        'SpecifyObjectiveGradient', true);%, 'HessianFcn', @hessianfcn);
    tic
    fun3obj = @(z)energyPS(z, mesh);
    fun3con = @(z)constrPS(z, mesh);
    [z, val, flag, out, mult, grad, hess] = fmincon(fun3obj, z0, [], [], [], [], lb, [], fun3con, options);
    toc
    defPos(freeIndices) = z(1:numFreeDofs);
    drawMesh(defPos, mesh, 'm', '--', 'm+');
end

% solve using principal stretches + exponential map
z0 = [x0; zeros(numElems * 2,1)]; % 2 singular values per element
if ismember(6, algs)
    disp('### principal stretches + exp')
    options6 = optimoptions(@fmincon, 'Algorithm', 'interior-point', ...
        'MaxFunctionEvaluations', 20000, 'SpecifyObjectiveGradient', true,...
        'SpecifyConstraintGradient', true);%, 'HessianFcn', @hessianexp);
    tic
    fun6obj = @(z)energyPSexp(z, mesh);
    fun6con = @(z)constrPSexp(z, mesh);
    z = fmincon(fun6obj, z0, [], [], [], [], [], [], fun6con, options6);
    toc
    defPos(freeIndices) = z(1:numFreeDofs);
    drawMesh(defPos, mesh, 'g', '--', 'g+');
end

% solve using principal stretches and ALM
if ismember(4, algs)
    disp('### principal stretches ALM')
    z0 = [x0; ones(numElems * 2,1)];
    zALM = alm_stretch(z0, mesh);
    defPos(freeIndices) = zALM(1:numFreeDofs);
    drawMesh(defPos, mesh, 'c', '--', 'c+');
end

% solve using principal stretches and ADMM
if ismember(5, algs)
    disp('### ADMM')
    options5 = ADMMOptions(1e8, 300);
    tic
    z = admm_stretch_gd_mex(x0, ones(numElems * 2,1), mesh, options5);
    toc
    defPos(freeIndices) = z(1:numFreeDofs);
    drawMesh(defPos, mesh, 'b', '--', 'b+');
end

if ismember(7, algs)
    disp('### saddle ps')
    options7 = optimoptions(@fsolve, 'Algorithm', 'trust-region-dogleg', ...
        'SpecifyObjectiveGradient', true, 'MaxIterations', 700);%, 'PlotFcn', 'optimplotfirstorderopt');
    %s0 = 0.5 + rand(numElems * 2,1) * 0.5;
    s0 = 0.9 + rand(numElems * 2,1) * 0.2;
    y0 = [x0; s0; zeros(numElems * 2,1)];
    tic
    fun7 = @(y)saddle_ps(y, mesh);
    y = fsolve(fun7, y0, options7);   
    toc
    defPos(freeIndices) = y(1:numFreeDofs);
    drawMesh(defPos, mesh, 'c', '--', 'c+');
end

%     disp('### saddle ps + exp')
%     options7 = optimoptions(@fsolve, 'Algorithm', 'trust-region-dogleg', 'Display', 'iter-detailed');
%     y0 = [x0; 1e-6 * rand(numElems * 2,1); zeros(numElems * 2,1)];
%     y = fsolve(@saddle_ps_exp, y0);
%     defPos(freeIndices) = y(1:numFreeDofs);
%     drawMesh(defPos, 'b', '--', 'b+');

if ismember(8, algs)
    disp('### reduced saddle ps')
    s0 = 0.9 + rand(numElems * 2,1) * 0.2;
    options8 = optimoptions(@fsolve, 'SpecifyObjectiveGradient', true);
    fun8 = @(z)reduced_saddle_ps(z, mesh);
    z = fsolve(fun8, [x0; s0]);
    defPos(freeIndices) = z(1:numFreeDofs);
    drawMesh(defPos, mesh, 'b', '--', 'b+');
end

% Newton/SQP
% s0 = 0.9 + rand(numElems * 2,1) * 0.2;
% y0 = [x0; s0; 0.1 * ones(numElems * 2,1)];
% y = y0;
% numVars = numFreeDofs + numElems * 2;
% J = zeros(numVars);
% for iter=1:20
%     [f, Jfull] = saddle_ps(y);
%     E = energyPS(y(1:numVars));
%     rho = 1e8; %vecnorm(y(numVars+1:end), Inf)
%     merit = E + rho * vecnorm(f(numVars+1:end), 1)
%     % solve for the step delta
%     %dyf = - Jfull \ f;
%     % solve the Schur complement
%     G = Jfull(1:numFreeDofs,numVars+1:end);
%     B = Jfull(numFreeDofs+1:numVars,numFreeDofs+1:numVars);
%     J(numFreeDofs+1:end,1:numFreeDofs) = G';
%     J(numFreeDofs+1:end,numFreeDofs+1:end) = eye(numElems * 2);
%     J(1:numFreeDofs,1:numFreeDofs) = Jfull(1:numFreeDofs,1:numFreeDofs);
%     J(1:numFreeDofs,numFreeDofs+1:end) = -G * B;
%     Ls = f(numFreeDofs+1:numVars);
%     dz = - J \ [f(1:numFreeDofs) - G * Ls; f(numVars+1:end)];
%     deta = Ls - B * dz(numFreeDofs+1:end);
%     dy = [dz; deta];
%     
%     step = 1;
%     oldY = y;
%     while step > 1e-3
%         y = oldY + step * dy;
%         E1 = energyPS(y(1:numVars));
%         [~,ceq] = constrPS(y(1:numVars));
%         merit1 = E1 + rho * vecnorm(ceq, 1);
%         if merit1 < merit
%             break;
%         else
%             step = step * 0.9;
%         end
%     end
% end
% defPos(freeIndices) = y(1:numFreeDofs);
% drawMesh(defPos, 'b', '--', 'y+');

% solve using area and ADMM
% disp('### area-pos ADMM');
% lambda = 0; % force the material to distortional (leave it last) (FIXME)
% y = admm_area(x0, area0, 9e4, 100);
% defPos = [refPos(1:numBCs * 2); y(1:numFreeNodes * 2)];
% drawMesh(defPos, 'y', '--', 'y+');

% solve using area-pos formulation
% disp('### area-pos');
% y0 = [x0; area0];
% lambda = 0; % force the material to distortional (leave it last) (FIXME)
% lb = zeros(size(y0));
% lb(1:size(x0,1)) = -Inf;
% options = optimoptions(@fmincon, 'Algorithm', 'sqp');
% y = fmincon(@energyArea, y0, [], [], [], [], lb, [], @constrArea, options);
% defPos = [refPos(1:numBCs * 2); y(1:numFreeNodes * 2)];
% drawMesh(defPos, 'c', '--', 'c+');


% function grad = grad_pos_ps(z)
%     global loads freeIndices
%     numFreeDofs = size(freeIndices,2);
%     % extract vars
%     s = z(numFreeDofs + 1:end);
%     
%     % gradient
%     grad = zeros(size(z));
%     grad(1:numFreeDofs) = -loads(freeIndices);
%     grad(numFreeDofs+1:end) = gradU(s);       
% end

function H = hessianfcn(z, lambda)
    H = hessian_lag(z, lambda.eqnonlin);
end

function H = hessianexp(z, lambda)
    global freeIndices
    numDofs = size(freeIndices,2);
    
    % replace the lower right corner
    x = z(1:numDofs);
    sigma = z(numDofs+1:end);
    s = exp(sigma);
    H0 = hessian_lag([x; s], lambda.eqnonlin);
    hessU0 = H0(numDofs+1:end, numDofs+1:end);
    H = H0;
    Sigma = diag(s);
    H(numDofs+1:end, numDofs+1:end) = diag(Sigma * gradU(s)) + Sigma * hessU0 * Sigma;
end

function f = saddle_ps_exp(y)
    global loads freeIndices triangles
    numFreeDofs = size(freeIndices,2);
    x = y(1:numFreeDofs);
    sigma = y(numFreeDofs + 1:numFreeDofs + size(triangles,1) * 2);
    s = exp(sigma);
    eta = y(numFreeDofs + size(triangles,1) * 2 + 1:end);
    
    [~, ceq, ~, GCeq] = constrPS([x; s]);
    gradS = GCeq(1:numFreeDofs, :);

    f1 = -loads(freeIndices) + gradS * eta;
    f2 = gradU(s) + eta;
    f = [f1; f2; ceq];
end

