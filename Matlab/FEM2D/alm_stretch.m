function zALM = alm_stretch(z0, mesh)
    numElems = size(mesh.Triangles,1);
    rho = 1e8;
    zALM = z0;
    lALM = zeros(numElems * 2, 1); % Lagrange multipliers
    %optALM = optimoptions('fmincon', 'MaxFunctionEvaluations', 3e4);
    for iter=1:10
        fun = @(z)aug_lag(z, mesh, rho, lALM);
        zALM = fminunc(fun, zALM);
        %zALM = fmincon(@aug_lag, zALM, [], [], [], [], lb, [], [], optALM);
        [~, ceq] = constrPS(zALM, mesh);
        E = energyPS(zALM, mesh);
        nc = vecnorm(ceq, 1);
        lALM = lALM + rho * ceq;
        maxL = vecnorm(lALM, Inf);
        merit = E + maxL * nc
    end
end