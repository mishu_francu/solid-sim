function L = aug_lag_pos_area(x)
    global rho a_admm u_admm numBCs loads refPos
    [c, ceq] = constrArea([x; a_admm]);
    c1 = ceq + u_admm;
    pos = [refPos(1:numBCs * 2); x];
    L = -pos' * loads + energy(x) + 0.5 * rho * (c1' * c1);
end