function H = hessian_lag(z, eta, mesh)
    freeIndices = mesh.FreeIndices;
    numElems = size(mesh.Triangles,1);
    numDofs = size(freeIndices,2);
    numPS = numElems * 2;
    numVars = numDofs + numPS;
    x = z(1:numDofs);
    s = z(numDofs + 1:numVars);

    % compute the upper left corner using finite differences
    Jfd = zeros(numDofs);
    eps = 1e-6;
    gradS = dsdx(x, mesh);
    f0 = gradS * eta;
    for i=1:numDofs
        x1 = x;
        x1(i) = x1(i) + eps;
        gradS1 = dsdx(x1, mesh);
        f1 = gradS1 * eta;        
        Jfd(i,:) = (f1 - f0) / eps;
    end
    
    H = zeros(numVars);
    H(1:numDofs, 1:numDofs) = Jfd;    
    H(numDofs+1:numVars, numDofs+1:numVars) = hessU(s, mesh);
end

