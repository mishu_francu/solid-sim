function L = aug_lag(z, mesh, rho, lALM)
    E = energyPS(z, mesh);
    [~, ceq] = constrPS(z, mesh);
    L = E + lALM' * ceq + 0.5 * rho * (ceq' * ceq);
end
