function grad = gradU(s, mesh)
    grad = zeros(size(s));
    for i=1:size(mesh.Triangles,1)
        idx = (i-1) * 2;
        s1 = s(idx + 1);
        s2 = s(idx + 2);
        if s1 < 0 || s2 < 0
            disp('inverted s')
        end
        psi1 = mesh.Mu * s1 - mesh.Mu / s1 + mesh.Lambda * s2 * (s1 * s2 - 1);
        psi2 = mesh.Mu * s2 - mesh.Mu / s2 + mesh.Lambda * s1 * (s1 * s2 - 1);
        % TODO: remove the += as there is no overlap between elements
        grad(idx + 1) = grad(idx + 1) + mesh.Area(i) * psi1;
        grad(idx + 2) = grad(idx + 2) + mesh.Area(i) * psi2;
    end
end
