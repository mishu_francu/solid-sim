function sol = admm_stretch(x, s, stiffness, maxIters)
    global rho x_admm s_admm u_admm numElems
    x_admm = x;
    s_admm = s;
    u_admm = zeros(numElems * 2, 1);
    rho = stiffness;
    options_s_con = optimoptions('fmincon', 'Display', 'off', 'SpecifyObjectiveGradient', true);
    options_s_unc = optimoptions('fminunc', 'Display', 'off');%, 'SpecifyObjectiveGradient', true);% 'CheckGradients', true);
    options_x = optimoptions('fminunc', 'Display', 'off', 'StepTolerance', 1e-5);
    old_energy = energyPS([x_admm; s_admm]);
    old_merit = 0;
    rel_merit = 1;
    threshold = 1e-8;
    lb = zeros(numElems * 2, 1);
    for iter=1:maxIters        
        %s_admm = fmincon(@aug_lag_stretch, s_admm, [], [], [], [], lb, [], [], options_s_con);
        %s_admm = fminunc(@aug_lag_stretch, s_admm);
        [s_admm, s_iters] = proj_gradient_descent(@grad_stretch, s_admm, 1e-10, 400);
        %[s_admm, s_iters] = gradient_descent_exp(@grad_stretch, s_admm, 1e-10, 400);
        disp(s_iters)
        x_admm = fminunc(@aug_lag_pos, x_admm, options_x);
        new_energy = energyPS([x_admm; s_admm]);
        rel_diff = abs(new_energy - old_energy) / old_energy;
        %disp(rel_diff)
%         if rel_diff < threshold
%             iter
%             break
%         end
        old_energy = new_energy;
        [c, ceq] = constrPS([x_admm; s_admm]);        
        norm1C = vecnorm(ceq, 1);
        norm2C = vecnorm(ceq);
        u_admm = u_admm + ceq;
        maxU = vecnorm(u_admm, Inf);
        new_merit = new_energy + rho * maxU * norm1C;
        if old_merit ~= 0
            rel_merit = abs(new_merit - old_merit) / old_merit;
        end
        old_merit = new_merit;
        if rel_merit < 1e-6
            iter
            break
        end
    end    
    sol = [x_admm; s_admm];
end