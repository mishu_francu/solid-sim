function [f, J] = reduced_saddle_ps(z, mesh)
    freeIndices = mesh.FreeIndices;
    numFreeDofs = size(freeIndices,2);
    x = z(1:numFreeDofs);
    s = z(numFreeDofs+1:end);
    [~, ceq, ~, GCeq] = constrPS(z, mesh);
    gradS = GCeq(1:numFreeDofs, :);
    GU = gradU(s, mesh);
    fe1 = gradS * GU;
    f1 = -mesh.Loads(freeIndices) - fe1;
    f = [f1; ceq];
        
    % compute the upper left corner using finite differences
%     Jfd = zeros(numFreeDofs);
%     eps = 1e-6;
%     gradS = dsdx(x);
%     f0 = -gradS * GU;
%     for i=1:numFreeDofs
%         x1 = x;
%         x1(i) = x1(i) + eps;
%         gradS1 = dsdx(x1);
%         f1 = -gradS1 * GU;        
%         Jfd(i,:) = (f1 - f0) / eps;
%     end
%     
%     % the Jacobian
%     J = zeros(size(z,1));
%     J(numFreeDofs+1:end,1:numFreeDofs) = gradS';
%     J(numFreeDofs+1:end,numFreeDofs+1:end) = eye(size(s,1));
%     J(1:numFreeDofs,1:numFreeDofs) = Jfd;
%     J(1:numFreeDofs,numFreeDofs+1:end) = -gradS * hessU(s);
end