function fe = grad_std(x, mesh)
    freeIndices = mesh.FreeIndices;
    pos = mesh.RefPos;
    pos(freeIndices) = x;

    fe = zeros(size(pos));
    for i = 1:size(mesh.Triangles,1)
        Ds = shapeMatrix(pos, i, mesh);
        DmInv = mesh.DmInv(:,:,i);
        F = Ds * DmInv;
        J = det(F);
        
        Finv = inv(F);
        P = mesh.Mu * (F - Finv') + mesh.Lambda * (J - 1) * J * Finv'; % PK1 tensor
        localF = -mesh.Area(i) * P * DmInv';
        f2 = localF(:,1);
        f3 = localF(:,2);
        f1 = -f2 - f3;
        i1 = mesh.Triangles(i, 1) - 1;
        i2 = mesh.Triangles(i, 2) - 1;
        i3 = mesh.Triangles(i, 3) - 1;
        fe(i1 * 2 + 1) = fe(i1 * 2 + 1) + f1(1);
        fe(i1 * 2 + 2) = fe(i1 * 2 + 2) + f1(2);
        fe(i2 * 2 + 1) = fe(i2 * 2 + 1) + f2(1);
        fe(i2 * 2 + 2) = fe(i2 * 2 + 2) + f2(2);
        fe(i3 * 2 + 1) = fe(i3 * 2 + 1) + f3(1);
        fe(i3 * 2 + 2) = fe(i3 * 2 + 2) + f3(2);        
    end
end
