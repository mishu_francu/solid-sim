function [U, S, V] = signedSVD(F)
    [U, S, V] = svd(F);
    s = diag(S);
    Sinv = [1 / s(1), 0; 0 1 / s(2)];
    if (det(V) < 0)
        % reflect first column
        V(:,1) = -V(:,1);
        % recompute U
        U = F * V * Sinv;
    end
    sgn1 = 1;
    sgn2 = 1;
    if (det(U) < 0)
        %disp("inverted singular values")
        if s(1) < s(2)
            s(1) = -s(1);
            sgn1 = -1;
            U(:,1) = -U(:,1);
        else
            s(2) = -s(2);
            sgn2 = -1;
            U(:,2) = -U(:,2);
        end
    end
    S = diag(s);
end

