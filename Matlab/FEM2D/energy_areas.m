function U = energy_areas(a)
    global area0 bulk
    U = 0;
    for i=1:size(a,1)
        J = a(i) / area0(i);
        U = U + area0(i) * 0.5 * bulk * (J - 1)^2;
    end
end