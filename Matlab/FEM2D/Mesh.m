classdef Mesh
    properties
        NumPointsX
        NumPointsY
        Triangles
        Mu
        Lambda
        Area
        RefPos
        Loads
        FreeIndices
        DmInv
    end
    methods
        function obj = Mesh(nx, ny, mu, lambda, pos)
            % init vars
            obj.NumPointsX = nx;
            obj.NumPointsY = ny;
            obj.Mu = mu;
            obj.Lambda = lambda;
            obj.Triangles = [];
            obj.RefPos = pos;
            % build triangles
            %obj.Triangles = delaunay(pos(1:2:end), pos(2:2:end));
            for i = 1:obj.NumPointsX-1
                baseX = (i-1) * obj.NumPointsY;
                for j = 1:obj.NumPointsY-1
                    baseY = baseX + j;
                    if mod(i+j,2) == 1
                        obj.Triangles = [obj.Triangles; baseY, baseY + obj.NumPointsY, baseY + 1];
                        obj.Triangles = [obj.Triangles; baseY + 1, baseY + obj.NumPointsY, baseY + obj.NumPointsY + 1];
                    else
                        obj.Triangles = [obj.Triangles; baseY, baseY + obj.NumPointsY + 1, baseY + 1];
                        obj.Triangles = [obj.Triangles; baseY, baseY + obj.NumPointsY, baseY + obj.NumPointsY + 1];            
                    end       
                end
            end
            % compute areas
            obj.Area = zeros(size(obj.Triangles,1),1);
            for i = 1:size(obj.Triangles,1)
                Dm = shapeMatrix(pos, i, obj);
                obj.DmInv(:,:,i) = inv(Dm);
                obj.Area(i) = det(Dm) / 2;
            end           
        end
    end
end