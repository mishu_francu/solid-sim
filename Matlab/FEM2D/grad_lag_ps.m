function [f, gradS] = grad_lag_ps(y, mesh)
    freeIndices = mesh.FreeIndices;
    numFreeDofs = size(freeIndices,2);
    x = y(1:numFreeDofs);
    s = y(numFreeDofs + 1:numFreeDofs + size(mesh.Triangles,1) * 2);
    eta = y(numFreeDofs + size(mesh.Triangles,1) * 2 + 1:end);

    [~, ceq, ~, GCeq] = constrPS([x; s], mesh);
    gradS = GCeq(1:size(x,1), :);

    f1 = -mesh.Loads(freeIndices) + gradS * eta;
    f2 = gradU(s, mesh) + eta;
    f = [f1; f2; ceq];
end

