function [c, ceq] = constrAreaPos(x, mesh)
    ceq = [];
    n = size(mesh.Triangles,1);
    c = zeros(n*2,1);    
    pos = mesh.RefPos;
    pos(mesh.FreeIndices) = x;
    for i = 1:n
        Ds = shapeMatrix(pos, i, mesh);
        area = det(Ds) / 2;
        c(i) = -area;
    end
end

