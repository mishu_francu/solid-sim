function [E, grad] = energy(x, mesh)
    freeIndices = mesh.FreeIndices;
    pos = mesh.RefPos;
    pos(freeIndices) = x;

    % elastic energy
    E = energy_pos(pos, mesh);
    
    % potential energy of loads
    U = -pos' * mesh.Loads;
    E = E + U;

    % compute gradient
    fe = grad_std(x, mesh);
    
    %fd_grad = finite_diff_gradient(@energy_pos, pos)
    %fe
    
    grad = -mesh.Loads(freeIndices) - fe(freeIndices);
end

