function H = hessU(s, mesh)
    H = zeros(size(s,1));
    for i=1:size(mesh.Triangles,1)
        idx = (i-1) * 2;
        s1 = s(idx + 1);
        s2 = s(idx + 2);
        psi11 = mesh.Mu + mesh.Mu / s1^2 + mesh.Lambda * s2^2;
        psi22 = mesh.Mu + mesh.Mu / s2^2 + mesh.Lambda * s1^2;
        psi12 = mesh.Lambda * (2 * s1 * s2 - 1);
        H(idx + 1, idx + 1) = mesh.Area(i) * psi11;
        H(idx + 2, idx + 2) = mesh.Area(i) * psi22;
        H(idx + 1, idx + 2) = mesh.Area(i) * psi12;
        H(idx + 2, idx + 1) = mesh.Area(i) * psi12;        
    end
end

