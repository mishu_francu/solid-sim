function grad = grad_pos(x, mesh, rho, s, u)
    freeIndices = mesh.FreeIndices;
    [~, ceq, ~, GCeq] = constrPS([x; s], mesh);
    c1 = ceq + u;
    
    % gradient part
    gradS = GCeq(1:size(x,1), :);
    grad = -mesh.Loads(freeIndices) + rho * gradS * c1;
end