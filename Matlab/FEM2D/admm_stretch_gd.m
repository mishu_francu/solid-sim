function sol = admm_stretch_gd(x, s, mesh, options)
    numElems = size(mesh.Triangles, 1);
    x_admm = x;
    s_admm = s;
    u_admm = zeros(numElems * 2, 1);
    rho = options.Penalty;
    old_merit = 0;
    rel_merit = 1;
    s_avg_iters = 0;       
    for iter=1:options.MaxIters
        funS = @(s)grad_stretch(s, mesh, rho, x_admm, u_admm);
        [s_admm, s_iters] = proj_gradient_descent(funS, s_admm, 1e-10, 400);
        %[s_admm, s_iters] = gradient_descent_exp(@grad_stretch, s_admm, 1e-10, 400);
        s_avg_iters = s_avg_iters + s_iters;
        funX = @(x)grad_pos(x, mesh, rho, s_admm, u_admm);
        [x_admm, x_iters] = gradient_descent(funX, x_admm, 9e-7, 800);
        disp(x_iters)
        [~, ceq, ~, ~] = constrPS([x_admm; s_admm], mesh);
        norm1C = vecnorm(ceq, 1);
        norm2C = vecnorm(ceq);
        u_admm = u_admm + ceq;
        maxU = vecnorm(u_admm, Inf);
        new_energy = energyPS([x_admm; s_admm], mesh); 
        new_merit = new_energy + rho * maxU * norm1C;        
        if old_merit ~= 0
            rel_merit = abs((new_merit - old_merit) / old_merit);
        end
        old_merit = new_merit;
        if rel_merit < 1e-7 && norm2C < 1e-4
            break
        end
    end        
%     iter
%     norm2C
%     rel_merit
%     s_avg_iters = s_avg_iters / iter
    sol = [x_admm; s_admm];
end