function [E, grad] = energyPSexp(z, mesh)
    freeIndices = mesh.FreeIndices;
    numFreeDofs = size(freeIndices,2);
    % extract vars
    x = z(1:numFreeDofs);
    pos = mesh.RefPos;
    pos(freeIndices) = x;
    s = exp(z(numFreeDofs + 1:end));
    
    E = energy_stretches(s, mesh);
    
    % potential energy of loads
    U = -pos' * mesh.Loads;
    E = E + U;
    
    % gradient
    grad = zeros(size(z));
    grad(1:numFreeDofs) = -mesh.Loads(freeIndices);
    grad(numFreeDofs+1:end) = s .* gradU(s, mesh);
end

