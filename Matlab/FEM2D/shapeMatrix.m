function D = shapeMatrix(pos, tri, mesh)
    i1 = mesh.Triangles(tri,1) - 1;
    i2 = mesh.Triangles(tri,2) - 1;
    i3 = mesh.Triangles(tri,3) - 1;
    x1 = pos(i1 * 2 + 1);
    y1 = pos(i1 * 2 + 2);
    x2 = pos(i2 * 2 + 1);
    y2 = pos(i2 * 2 + 2);
    x3 = pos(i3 * 2 + 1);
    y3 = pos(i3 * 2 + 2);
    D = [x2 - x1, x3 - x1; y2 - y1, y3 - y1];
end

