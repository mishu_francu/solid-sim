function [L, grad] = aug_lag_pos(x, s, u, rho, mesh)
    freeIndices = mesh.FreeIndices;
    [~, ceq, ~, GCeq] = constrPS([x; s], mesh);
    c1 = ceq + u;
    pos = mesh.RefPos;
    pos(freeIndices) = x;
    L = -pos' * mesh.Loads + 0.5 * rho * (c1' * c1);
    
    % gradient part
    gradS = GCeq(1:size(x,1), :);
    grad = -mesh.Loads(freeIndices) + rho * gradS * c1;
end