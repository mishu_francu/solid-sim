function [c, ceq] = constrArea(y)
    global triangles numFreeNodes numBCs refPos
    c = [];
    n = size(triangles,1);
    ceq = zeros(n,1);
    x = y(1:numFreeNodes * 2);
    pos = [refPos(1:numBCs * 2); x];
    a = y(numFreeNodes * 2 + 1:end);
    for i = 1:n
        Ds = shapeMatrix(pos, i);
        area = det(Ds) / 2;      
        ceq(i) = -area + a(i);        
    end    
end

