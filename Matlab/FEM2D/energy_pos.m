function E = energy_pos(pos, mesh)
    E = 0;
    inverted = false;
    for i = 1:size(mesh.Triangles,1)
        Ds = shapeMatrix(pos, i, mesh);
        DmInv = mesh.DmInv(:,:,i);
        F = Ds * DmInv;
        J = det(F);
        if J <= 0
            inverted = true;
        end
        logJ = log(J);
        C = F' * F;
        I1 = trace(C);
        localE = 0.5 * mesh.Mu * (I1 - 2) - mesh.Mu * logJ + 0.5 * mesh.Lambda * (J - 1)^2;
        E = E + mesh.Area(i) * localE;
    end
    if inverted
        disp('inverted')
    end
end