classdef ADMMOptions
    properties
        Penalty
        MaxIters
        SStepLen
        SMaxIters
        XStepLen
        XMaxIters
    end
    methods
        function obj = ADMMOptions(rho, maxIters)
            obj.Penalty = rho;
            obj.MaxIters = maxIters;
        end
    end
end