import vtk
import sys
import vtk.util.numpy_support as np_sup
import numpy

path = sys.argv[1]
extension = path[-3:]

# read the file
geometryMapper = None
reader = None
if extension == "vtk":
    reader = vtk.vtkUnstructuredGridReader()    
    reader.SetFileName(path)
    reader.Update()
    
    # analyze points
    vtkPoints = reader.GetOutput().GetPoints().GetData()
    npPoints = np_sup.vtk_to_numpy(vtkPoints)
    miny = 1000
    for p in npPoints:
        if p[1] < miny:
            miny = p[1]
    print(miny)
    
    # map the geometry
    geometryMapper = vtk.vtkDataSetMapper()
    geometryMapper.SetInputConnection(reader.GetOutputPort())
    geometryMapper.SetScalarModeToUseCellData()
    geometryMapper.SetScalarRange(0, 11)
elif extension == "stl":
    reader = vtk.vtkSTLReader()
    reader.SetFileName(path)    
    
    geometryMapper = vtk.vtkPolyDataMapper()
    geometryMapper.SetInputConnection(reader.GetOutputPort())
    

# create geometry actor
geometryActor = vtk.vtkActor()
geometryActor.SetMapper(geometryMapper)
#geometryActor.GetProperty().SetLineWidth(3)
#geometryActor.GetProperty().EdgeVisibilityOn()
#geometryActor.GetProperty().SetEdgeColor(0, 0, 0)

# Create the Renderer
renderer = vtk.vtkRenderer()
renderer.AddActor(geometryActor)
renderer.SetBackground(0.4, 0.4, 0.4)  # Set background to white
#renderer.SetBackground(colors.GetColor3d("Wheat"))

aCamera = vtk.vtkCamera()
#aCamera.Azimuth(-40.0)
aCamera.SetPosition(-0.025, -0.065, 0.4)
aCamera.SetFocalPoint(-0.025, -0.065, 0)
aCamera.Elevation(0.0)

renderer.SetActiveCamera(aCamera)
#renderer.ResetCamera()

# Create the RendererWindow
renderer_window = vtk.vtkRenderWindow()
renderer_window.AddRenderer(renderer)
renderer_window.SetSize(640, 480)

# screenshot code:
w2if = vtk.vtkWindowToImageFilter()
w2if.SetInput(renderer_window)
w2if.SetInputBufferTypeToRGB()
w2if.ReadFrontBufferOff()
w2if.Update()

writer = vtk.vtkPNGWriter()
writer.SetFileName(path[0:-4] + ".png")
writer.SetInputConnection(w2if.GetOutputPort())
writer.Write()

# Create the RendererWindowInteractor and display the vtk_file
interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(renderer_window)
interactor.Initialize()
#interactor.Start()