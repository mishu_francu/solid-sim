#version 400
layout(location = 0) out vec4 out_color;

//uniform sampler2D map_color;

uniform vec3 eye_position;
uniform vec3 light_position;
uniform vec3 material_kd;

in vec3 position;
in vec3 normal;
in vec2 texcoord;

void main(){

    vec3 L = normalize(light_position - position);
    vec3 N = normalize(normal);
    vec3 V = normalize(eye_position - position);
    vec3 H = normalize(L + V);
   
    float light = max(dot(L, N), 0) + pow(max(dot(N, H), 0), 12);
    //vec3 color = texture(map_color, texcoord).xyz;
	vec3 color = material_kd;

    out_color = vec4(color * light, 1);
}