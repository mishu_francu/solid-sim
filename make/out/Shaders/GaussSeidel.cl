#include "Common.cl"

__kernel void gaussSeidel(__global const Edge* edges, __global float4* pos, __global float* invMass, __global float4* normals, __global float4* points)
{
	int i = get_global_id(0);
	int i1 = edges[i].i1;
	int i2 = edges[i].i2;
	if (edges[i].type == LINK)
	{
		float4 delta = pos[i1] - pos[i2];
		float len = length(delta);
		float len0 = edges[i].len;
		float lambda = (len - len0) / (invMass[i1] + invMass[i2]);
		float4 disp = (lambda / len) * delta;
		pos[i1] -= invMass[i1] * disp;
		pos[i2] += invMass[i2] * disp;
	}
	else if (edges[i].type == CONTACT)
	{
		float lambda = dot(pos[i1] - points[i2], normals[i2]) - edges[i].len;
		if (lambda > 0)
			return;
		pos[i1] -= lambda * normals[i2];
	}
}

