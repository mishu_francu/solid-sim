#version 330

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec3 in_color;
layout(location = 3) in vec2 in_uv;

uniform mat4 model_matrix, view_proj_matrix;
uniform mat4 vp_shadow_matrix;

out vec3 position;
flat out vec3 normal;
out vec3 color;
out vec2 texcoord;
out vec4 ShadowCoord;

out VS_OUT
{
	vec3 n;
} vs_out; 

void main(){

	// compute vectors in WORLD SPACE
	position = (model_matrix * vec4(in_position,1)).xyz;
	normal = normalize(mat3(model_matrix) * in_normal);
	color = in_color;
	texcoord = in_uv;

	mat4 MVP = view_proj_matrix * model_matrix;
	gl_Position = MVP * vec4(in_position, 1); 

	ShadowCoord = vp_shadow_matrix * model_matrix * vec4(in_position, 1);	
	
	vs_out.n = normal;
}
