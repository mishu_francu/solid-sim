typedef struct
{
	float4 p, n;
	uint idx1, idx2;
	int idx;
} PrimitivePair;

typedef struct
{
	float u, v, w;
} BarycentricCoords;

bool PointInAabb3D(float4 min, float4 max, float4 point)
{
	return !(point.x < min.x || point.x > max.x
		|| point.y < min.y || point.y > max.y
		|| point.z < min.z || point.z > max.z);
}

float4 ClosestPtPointTriangle(float4 p, float4 a, float4 b, float4 c, BarycentricCoords* bar)
{
	bar->u = bar->v = bar->w = 0.f;

	// Check if P in vertex region outside A
	float4 ab = b - a;
	float4 ac = c - a;
	float4 ap = p - a;
	float d1 = dot(ab, ap);
	float d2 = dot(ac, ap);
	if (d1 <= 0.f && d2 <= 0.f)
	{
		bar->u = 1.f;
		return a;
	}

	// Check if P in vertex region outside B
	float4 bp = p - b;
	float d3 = dot(ab, bp);
	float d4 = dot(ac, bp);
	if (d3 >= 0.f && d4 <= d3)
	{
		bar->v = 1.f;
		return b;
	}

	// Check if P in edge region of AB, if so return projection
	float vc = d1 * d4 - d3 * d2;
	if (vc <= 0.f && d1 >= 0.f && d3 <= 0.f)
	{
		bar->v = d1 / (d1 - d3);
		bar->u = 1 - bar->v;
		return a + bar->v * ab;
	}

	// Check if P in vertex region outside C
	float4 cp = p - c;
	float d5 = dot(ab, cp);
	float d6 = dot(ac, cp);
	if (d6 >= 0.f && d5 <= d6)
	{
		bar->w = 1.f;
		return c;
	}

	// Check if P in edge region of AC
	float vb = d5 * d2 - d1 * d6;
	if (vb <= 0.f && d2 >= 0.f && d6 <= 0.f)
	{
		bar->w = d2 / (d2 - d6);
		bar->u = 1 - bar->w;
		return a + bar->w * ac;
	}

	// Check if P in edge region of BC
	float va = d3 * d6 - d5 * d4;
	float d43 = d4 - d3;
	float d56 = d5 - d6;
	if (va <= 0.f && d43 >= 0.f && d56 >= 0.f)
	{
		bar->w = d43 / (d43 + d56);
		bar->v = 1 - bar->w;
		return b + bar->w * (c - b);
	}

	// P inside face region
	float denom = 1.f / (va + vb + vc);
	bar->v = vb * denom;
	bar->w = vc * denom;
	bar->u = 1.f - bar->v - bar->w;
	return a + ab * bar->v + ac * bar->w;
}

bool IntersectSphereTriangle(float4 v, float radTol, float4 v1, float4 v2, float4 v3, 
							float4* normal, float4* pos, float* dist, BarycentricCoords* coords)
{
	float4 p = ClosestPtPointTriangle(v, v1, v2, v3, coords);
	float4 delta = v - p;
	float dSqr = dot(delta, delta);
	if (dSqr > radTol * radTol)
		return false;
	if (dSqr == 0)
		return false; // TODO: choose a normal
	*pos = p;
	delta = normalize(delta);
	*normal = delta;
	*dist = dSqr;
	return true;
}

__kernel void PointVsTriangle(__global PrimitivePair* pairs, 
							  __global const float4* prev,
							  __global const uint* indices,
							  __global const float4* vertices,
							  float radTol)
{
	int i = get_global_id(0);
	if (pairs[i].idx == 0)
		return;
	int k = pairs[i].idx2;
	int idx = pairs[i].idx1;

	float4 v = prev[idx];
	uint i1 = indices[k];
	uint i2 = indices[k + 1];
	uint i3 = indices[k + 2];
	float4 v1 = vertices[i1];
	float4 v2 = vertices[i2];
	float4 v3 = vertices[i3];

	float4 extrude = (float4)(radTol);
	float4 minV = min(min(v1, v2), v3) - extrude;
	float4 maxV = max(max(v1, v2), v3) + extrude;
	if (!PointInAabb3D(minV, maxV, v)) return;

	float4 n1 = cross(v2 - v1, v3 - v1);
	n1 = normalize(n1);
	// if coming from inside skip
	if (dot(n1, v - v1) < 0)
		return;
	BarycentricCoords coords;
	float4 n, p;
	float d;
	bool intersect = IntersectSphereTriangle(v, radTol, v1, v2, v3, &n, &p, &d, &coords);
	if (intersect)
	{
		//AddContact(idx, p, n);
		pairs[i].idx = idx;
		pairs[i].p = p;
		pairs[i].n = n;
	}
}
