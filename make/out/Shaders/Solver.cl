typedef struct
{
	int i1, i2;//, type;
	float len, stiff;
} Edge;

#define MAX_INCIDENCE 16

typedef struct
{
	int size;
	float4 disp[MAX_INCIDENCE];
} Accumulator;

typedef struct
{
	int info[MAX_INCIDENCE];
} Incidence;

typedef struct
{
	float4 n, p;
	int idx;
	float lambda;
} Contact;

int GetIndexAndSign(int info, int* sgn)
{
	int idx;
	if (info > 0)
	{
		idx = info - 1;
		*sgn = 1;
	}
	else
	{
		idx = -info - 1;
		*sgn = -1;
	}
	return idx;
}

__kernel void conjResEdges(__global const Edge* edges, __global float4* pos, __global float* invMass, __global float4* disp, float alpha, float beta)
{
	int i = get_global_id(0);
	int i1 = edges[i].i1 & 0xffffff;
	int i2 = edges[i].i2 & 0xffffff;
	float len0 = edges[i].len;

	float4 delta = pos[i1] - pos[i2];
	float len = length(delta);
	if (len == 0)
		return;	
	float lambda = (len - len0) * alpha / (invMass[i1] + invMass[i2]);
	disp[i] = edges[i].stiff * ((lambda / len) * delta + beta * disp[i]);
}

__kernel void minResParticles(__global float4* pos, __global const float* invMass, __global const Incidence* incidence, __global const float4* disp)
{
	int i = get_global_id(0);
	float4 acc = (float4)(0);
	float im = invMass[i];
	for(int j = 0; j < 16; j++)
	{
		int info = incidence[i].info[j];
		if (info == 0)
			break;
		int sgn = 1;
		int idx = GetIndexAndSign(info, &sgn);
		acc += sgn * im * disp[idx];
	}
	pos[i] += acc;
}

__kernel void conjResEdges1(__global const Edge* edges, __global float4* pos, __global float* invMass, __global float4* disp, float alpha, float beta, __global Accumulator* acc)
{
	int i = get_global_id(0);

	int val1 = edges[i].i1;
	int val2 = edges[i].i2;
	int i1 = val1 & 0xffffff;
	int i2 = val2 & 0xffffff;
	float len0 = edges[i].len;

	float4 delta = pos[i1] - pos[i2];
	float len = length(delta);
	if (len == 0)
		return;	
	int cur1 = val1 >> 24;
	int cur2 = val2 >> 24;
	float lambda = (len - len0) * alpha / (invMass[i1] + invMass[i2]);
	float4 imp0 = disp[i];
	float4 imp = ((lambda / len) * delta + beta * imp0); // TODO: previous lambda instead of disp
	disp[i] = imp;

	acc[i1].disp[cur1] = -edges[i].stiff * imp;
	acc[i2].disp[cur2] = edges[i].stiff * imp;
}

__kernel void conjResContacts(__global Contact* contacts,
							  __global float4* pos, __global float* invMass, __global float4* prev, float len0, float mu)
							 // __global float4* disp, float beta) //__global Accumulator* acc)
{
	int i = get_global_id(0);

	int idx = contacts[i].idx & 0xffffff;
	float4 point = contacts[i].p;
	if (dot(point, point) == 0)
		return;
	float4 delta = pos[idx] - point;
	float4 normal = contacts[i].n;
	float len = dot(delta, normal);
	if (len > len0) 
		return;
	float depth = len - len0;
	float4 imp = depth * normal;
	pos[idx] -= invMass[idx] * imp;
	//int cur = indices[i] >> 24;
	//acc[idx].disp[cur] = -0.1f * imp;
	
	if (mu > 0.f)
	{
		float4 v12 = pos[idx] - prev[idx];
		float vnrel = dot(v12, normal);
		float4 vt = v12 - vnrel * normal;
		float vtrel = length(vt);

		float lambda = max(0.f, vnrel);
		float limit = mu * lambda;

		//float lambda = vnrel + depth;
		//contacts[i].lambda += lambda;
		//float limit = mu * contacts[i].lambda;

		if (vtrel < limit)
			pos[idx] -= vt;
		else //if (vtrel > 0.001f)
			//pos[idx] -= (limit / vtrel) * vt;
			pos[idx] = prev[idx] + vnrel * normal;

		//if (vtrel > 0.001f)
		//{
		//	float dLambda = vtrel;
		//	float lambda0 = contact.lambdaF;
		//	contact.lambdaF = lambda0 + dLambda;
		//	if (contact.lambdaF >= limit)
		//		contact.lambdaF = limit;
		//	dLambda = contact.lambdaF - lambda0;

		//	vt.Scale(1.f / vtrel); // normalize
		//	Vector3 p = dLambda * vt;
		//	p1.pos += p;
		//}
	}
}

typedef struct
{
	uint i1, i2, i3, i4;
	float4 normal;
	float w1, w2, w3;
} SelfContact;

__kernel void conjResSelfTris(__global const SelfContact* selfTris, __global float4* pos, __global const float* invMass, float len0, __global Accumulator* acc)
{
	int i = get_global_id(0);
	float4 n = selfTris[i].normal;
	if (dot(n, n) == 0)
		return;
	SelfContact contact = selfTris[i];
	float4 p = contact.w1 * pos[contact.i1] + contact.w2 * pos[contact.i2] + contact.w3 * pos[contact.i3];
	float len = dot(n, pos[contact.i4] - p);
	if (len > len0)
		return;
	float im = invMass[contact.i1] + invMass[contact.i2] + invMass[contact.i3] + invMass[contact.i4];
	float s = 1.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / im;
	float dLambda = s * (len - len0);
	float4 disp = dLambda * n;
	pos[contact.i1] += disp * (contact.w1 * invMass[contact.i1]);
	pos[contact.i2] += disp * (contact.w2 * invMass[contact.i2]);
	pos[contact.i3] += disp * (contact.w3 * invMass[contact.i3]);
	pos[contact.i4] -= disp * invMass[contact.i4];
}

__kernel void minResParticles1red(__global float4* pos, __global const float* invMass, __global Accumulator* acc)
{
#define STEPS 1

	__local float4 buf[256]; // TODO: exact local size
	int i = get_global_id(0) >> 4; // particle idx
	int lid = get_local_id(0);
	int j = lid & 15; // neighbour idx

	// load into local mem
	buf[lid] = acc[i].disp[j];
	acc[i].disp[j] = 0;
	barrier(CLK_LOCAL_MEM_FENCE);

	//for (int k = 0; k < STEPS; k++)
	//{
	//	int mask = (1 << (k + 1)) - 1;
	//	if ((lid & mask) == 0)
	//	{
	//		buf[lid] = buf[lid] + buf[lid + (1 << k)];
	//	}
	//	barrier(CLK_LOCAL_MEM_FENCE);
	//}

	if (j < 8)
		buf[lid] = buf[lid] + buf[lid + 8];
	barrier(CLK_LOCAL_MEM_FENCE);

	if (j != 0)
		return;
	float4 disp = (float4)(0);
	for (int k = 0; k < 8; k++)
		disp += buf[lid + k];
	pos[i] += invMass[i] * disp;
}
