#version 400
layout(location = 0) out vec4 out_color;

uniform sampler2D map_color;

uniform vec3 cam_position;
uniform vec3 light_position;

in vec3 position;
in vec3 normal;
in vec2 texcoord;

float orennayar(float roughness){
    vec3 L = normalize(light_position - position);
    vec3 N = normalize(normal);
    vec3 V = normalize(cam_position - position);

    float acosVN = acos(dot(V, N));
    float acosLN = acos(dot(L, N));
    float alpha = max(acosVN, acosLN);
    float beta = min(acosVN, acosLN);
    float gamma = dot(V - N * dot(V, N), L - N * dot(L, N));
    float rough_sq = roughness * roughness;

    float C1 = 1.0f - 0.5f * (rough_sq / (rough_sq + 0.33f));
    float C2 = 0.45f * (rough_sq / (rough_sq + 0.09));
    if (gamma >= 0)
    {
        C2 *= sin(alpha);
    }
    else
    {
        C2 *= (sin(alpha) - pow((2 * beta) / 3.14167, 3));
    }

    float C3 = (1.0f / 8.0f);
    C3 *= (rough_sq / (rough_sq + 0.09f));
    C3 *= pow((4.0f * alpha * beta) / (3.14167 * 3.14167), 2);

    float A = gamma * C2 * tan(beta);
    float B = (1 - abs(gamma)) * C3 * tan((alpha + beta) / 2.0f);

    return min(max(0.0f, dot(N, L)) * (C1 + A + B),0.99f);
}

float phong(){
    vec3 L = normalize(light_position - position);
    vec3 N = normalize(normal);
    vec3 V = normalize(cam_position - position);
    vec3 H = normalize(L + V);
    return max(dot(L, N), 0) + pow(max(dot(N, H), 0), 8)*0.3;
}



void main(){

    //vec3 albedo = texture(map_color, texcoord).xyz;

    //out_color = albedo * orennayar(1.4);
	out_color = vec4(1,0,0,1);
}