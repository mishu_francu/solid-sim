#include "Common.cl"

__kernel void jacobiEdges(__global const Edge* edges, __global float4* pos, __global float* invMass, __global float4* disp, __global float4* normals, __global float4* points)
{
	int i = get_global_id(0);
	int i1 = edges[i].i1;
	int i2 = edges[i].i2;
	float len0 = edges[i].len;
	if (edges[i].type != CONTACT)
	{
		float4 delta = pos[i1] - pos[i2];
		float len = length(delta);		
		if (edges[i].type == COLL_PAIR && len > len0)
			disp[i] = (float4)(0);
		else
		{
			float lambda = (len - len0) / (invMass[i1] + invMass[i2]);
			disp[i] = edges[i].stiff * (lambda / len) * delta; // TODO: relaxation factor as input
		}
	}
	else
	{
		float4 n = normals[i2];
		float len = dot(n, pos[i1] - points[i2]);
		if (len > len0)
			disp[i] = (float4)(0);
		else
			disp[i] = ((len - len0) / invMass[i1]) * n;
	}
}

__kernel void jacobiParticles(__global float4* pos, __global const float* invMass, __global const Incidence* incidence, __global float4* disp)
{
	int i = get_global_id(0);
	float4 acc = (float4)(0);
	for(int j = 0; j < MAX_INCIDENCE; j++)
	{
		int info = incidence[i].info[j];
		if (info == 0)
			break;
		int sgn;
		int idx = GetIndexAndSign(info, &sgn);
		acc += 0.25f * sgn * invMass[i] * disp[idx];
	}
	pos[i] += acc;
}
