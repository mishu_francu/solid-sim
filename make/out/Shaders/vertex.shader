//#version 140

layout(location = 0) 
in vec3 vertexPosition_modelspace; // TODO: rename
layout(location = 1) 
in vec3 normalIn;
//layout(location = 2) 
in vec2 vertexUV;

 
// Values that stay constant for the whole mesh.
uniform mat4 MVP;
uniform mat4 V;
uniform mat4 M;
uniform vec3 l; // TODO: rename
uniform vec3 diffColor;
uniform vec3 specColor;
uniform vec3 ambientColor;
uniform vec3 lightColor;
uniform float gloss;

// Output data ; will be interpolated for each fragment.
out vec3 fragmentColor;
//out vec2 UV;
out vec3 normal;
out vec3 halfVec;

// TODO: varying was deprecated
//varying vec3 normal;
out vec3 light;
out vec3 EyeDirection_cameraspace;

void Light()
{
	// Position of the vertex, in worldspace : M * position
	vec3 Position_worldspace = vertexPosition_modelspace; //(M * vec4(vertexPosition_modelspace,1)).xyz;
	 
	// Vector that goes from the vertex to the camera, in camera space.
	// In camera space, the camera is at the origin (0,0,0).
	vec3 vertexPosition_cameraspace = ( V * vec4(vertexPosition_modelspace,1)).xyz;
	EyeDirection_cameraspace = vec3(0,0,0) - vertexPosition_cameraspace;
	 
	vec3 LightPosition_worldspace = l;
	// Vector that goes from the vertex to the light, in camera space. M is ommited because it's identity.
	vec3 LightPosition_cameraspace = ( V * vec4(LightPosition_worldspace,1)).xyz;
	vec3 LightDirection_cameraspace = LightPosition_cameraspace + EyeDirection_cameraspace;
	 
	// Normal of the the vertex, in camera space
	vec3 Normal_cameraspace = ( V * vec4(normalIn,0)).xyz; // Only correct if ModelMatrix does not scale the model ! Use its inverse transpose if not.
	
	normal = normalize(Normal_cameraspace);
	light = normalize(LightDirection_cameraspace);
}

// TODO: common include
#define PI 3.1415926535897932384626433832795
//#define VERTEX_SHADING

vec3 ShadeLambertVertex()
{
	// TODO: world space specular
	vec3 posVS = (V * M * vec4(vertexPosition_modelspace,1)).xyz;
	vec3 eyeDirVS = normalize(-posVS);
	vec3 lightDirVS = normalize((V * vec4(l, 0)).xyz);
	
	vec3 lightPower = 30 * lightColor; // TODO: so big?	
	vec3 n = normalIn; // TODO: rename to normal if possible; check if normalized?
	vec3 lightDir = normalize(l);
	float cosi = clamp(dot(lightDir, n), 0, 1);
	vec3 h = normalize(eyeDirVS + lightDirVS);
	vec3 nVS = normalize((V * vec4(n, 0)).xyz);
	float cosh = clamp(dot(h, nVS), 0, 1);	
	
	return ambientColor + (diffColor + (gloss + 8) * pow(cosh, gloss) * specColor / 8) * lightPower * cosi / PI;	
}

void ShadeLambertPixel()
{
	vec3 posVS = (V * vec4(vertexPosition_modelspace,1)).xyz;
	vec3 eyeDirVS = normalize(-posVS);
	vec3 lightDirVS = normalize((V * vec4(l, 0)).xyz);
	
	vec3 n = normalIn; // TODO: check if normalized?
	vec3 lightDir = normalize(l);

	// output half vector and normal
	halfVec = normalize(eyeDirVS + lightDirVS);	
	normal = normalize((V * vec4(n, 0)).xyz);
}

void main()
{
	// Output position of the vertex, in clip space : MVP * position
	gl_Position = MVP * vec4(vertexPosition_modelspace, 1);
	
	//UV = vertexUV;
	
#ifdef VERTEX_SHADING
	// per vertex lambert shading
	fragmentColor = ShadeLambertVertex();
#else
	// per pixel lambert shading
	//ShadeLambertPixel();
#endif
}

