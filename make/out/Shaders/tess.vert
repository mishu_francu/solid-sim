#version 400

layout(location = 0) in vec3 in_position;		
layout(location = 1) in vec3 in_normal;	
layout(location = 2) in vec2 in_texcoord;

uniform mat4 model_matrix;

out vec3 v_position;
out vec3 v_normal;
out vec2 v_texcoord;

void main(){
	
    v_position = (model_matrix * vec4(in_position.xyz, 1)).xyz;
	v_normal = normalize(mat3(model_matrix)*normalize(in_normal));
	v_texcoord = in_texcoord;
}
