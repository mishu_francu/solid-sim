#include "Common.cl"

typedef struct 
{
	float4 disp;
	float len0, stiffness;
	int idx;
} IncidenceInfo;

typedef struct
{
	IncidenceInfo info[MAX_INCIDENCE];
	int num;
} IncidenceEx;

__kernel void particlesProject(__global const float* invMass,
							   __global const float4* posIn,
							   __global float4* posOut,
							   __global IncidenceEx* inc,
							   float alpha, float beta)
{
	int i = get_global_id(0);
	int size = get_local_size(0);
	int group = get_group_id(0);

	local float4 scratch[256];
	int ii = get_local_id(0);
	scratch[ii] = posIn[i];
	barrier(CLK_LOCAL_MEM_FENCE);

	float4 acc = (float4)(0);
	float im = invMass[i];
	float4 p = posIn[i];

	float4 delta;
	int base = group * size;
	for (int j = 0; j < inc[i].num; j++)
	{
		int idx = inc[i].info[j].idx;
		int g = idx / size;
		if (g == group)
			delta = p - scratch[idx - base];
		else
			delta = p - posIn[idx];
		float len = length(delta);
		float len0 = inc[i].info[j].len0;
		float lambda = alpha * (len - len0) / len / (im + invMass[idx]);
		float4 disp = inc[i].info[j].stiffness * (lambda * delta + beta * inc[i].info[j].disp);
		inc[i].info[j].disp = disp;
		acc += disp;
	}
	posOut[i] = p - im * acc;
}

// good for CPU
__kernel void particlesProject0(__constant float* invMass,
							   __global const float4* posIn,
							   __global float4* posOut,
							   __global IncidenceEx* inc,
							   float alpha, float beta)
{
	int i = get_global_id(0);

	float4 acc = (float4)(0);
	float im = invMass[i];
	float4 p = posIn[i];

	for (int j = 0; j < inc[i].num; j++)
	{
		int idx = inc[i].info[j].idx;
		float4 delta = p - posIn[idx];
		float len = length(delta);
		float len0 = inc[i].info[j].len0;
		float lambda = alpha * (len - len0) / len / (im + invMass[idx]);
		float4 disp = inc[i].info[j].stiffness * (lambda * delta + beta * inc[i].info[j].disp);
		inc[i].info[j].disp = disp;
		acc += disp;
	}
	posOut[i] = p - im * acc;
}