SolidFEM.exe ../Models/spine0.040.feb -Mmixed -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.035.feb -Mmixed -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.030.feb -Mmixed -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.025.feb -Mmixed -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.020.feb -Mmixed -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.015.feb -Mmixed -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.010.feb -Mmixed -N40 -G-9.8 -R0.01 -nols