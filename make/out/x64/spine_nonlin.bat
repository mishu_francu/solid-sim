SolidFEM.exe ../Models/spine0.040.feb -Mnonlinear -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.035.feb -Mnonlinear -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.030.feb -Mnonlinear -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.025.feb -Mnonlinear -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.020.feb -Mnonlinear -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.015.feb -Mnonlinear -N40 -G-9.8 -R0.01 -nols
SolidFEM.exe ../Models/spine0.010.feb -Mnonlinear -N40 -G-9.8 -R0.01 -nols