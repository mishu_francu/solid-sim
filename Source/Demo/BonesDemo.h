#include "Rig.h"
#include <Box2D.h>

class Engine;

class Box: public Object
{
	public:
		
		float x, y, w, h, density;

		b2Body* body;

	public:

		Box(): Object(TYPE_BOX)
		{
		}
};

// TODO: base demo class
class BonesDemo
{
	public:

		Rig rig;

		Chain chain;

		Vector2 target;

		BoneTree* sel;

		vector<Object*> objects;
		
		int mode;

		b2World* world;

		b2Body* character;

		Vector2 camera;

	public:
		
		BonesDemo();

		~BonesDemo();

		void Step(Engine* engine);

		b2Body* CreateBox(float x, float y, float hw, float hh, float density, float friction = 0.3f);

};