#include <Graphics2D/Graphics.h>

#include "Rig.h"

#include <stack>
using namespace std;

void Chain::AddBone(float length, float angle)
{
	Bone bone;
	bone.length = length;
	bone.angle = angle;
	if (bones.size() != 0)
	{
		const Bone& parent = bones[bones.size() - 1];
		bone.worldAngle = parent.worldAngle + angle;
		bone.b = parent.e;
		float xe = bone.b.GetX() + length * cos(RADIAN(bone.worldAngle));
		float ye = bone.b.GetY() + length * sin(RADIAN(bone.worldAngle));		
		bone.e.Set(xe, ye);
	}
	else
	{
		bone.worldAngle = angle;
		bone.b.Set((float)x, (float)y); // TODO: set accesors for (x, y)
		float xe = x + length * cos(RADIAN(angle));
		float ye = y + length * sin(RADIAN(angle));
		bone.e.Set(xe, ye); 
	}
	bones.push_back(bone);
}

void Chain::Draw(Graphics2D& g)
{
	float x0 = (float)x;
	float y0 = (float)y;
	float angle = 0;
	for (size_t i = 0; i < bones.size(); i++)
	{
		if (i == 0)
		{
			g.DrawRect(x0 - 3, y0 - 3, 6.0f, 6.0f);
		}
		angle += bones[i].angle;
		float x1 = x0 + bones[i].length * cos(RADIAN(angle));
		float y1 = y0 + bones[i].length * sin(RADIAN(angle));
		g.DrawLine(x0, y0, x1, y1);
		g.DrawRect(x1 - 2, y1 - 2, 4.0f, 4.0f);
		x0 = x1;
		y0 = y1;
	}
}

void Chain::CcdIk(const Vector2& target, int effectorIndex, int numIterations)
{
	if (effectorIndex == -1)
	{
		effectorIndex = bones.size() - 1;
	}
	Vector2 effector = bones[effectorIndex].e;
	for ( int k = 0; k < numIterations; k++)
	{
		for ( int i = effectorIndex; i >= 0; i--)
		{
			Vector2 PE = effector;
			PE.Subtract(bones[i].b);
			PE.Normalize();

			Vector2 PT = target;
			PT.Subtract(bones[i].b);
			if (PT.GetX() != 0 && PT.GetY() != 0)
			{
				PT.Normalize();

				float c = PE.Dot(PT);
				float s = PE.Cross(PT);
				float phi = atan(s / c);
				if (c < 0) phi = PI - phi;

				bones[i].angle += DEGREE(phi);
				float x0 = bones[i].b.GetX();
				float y0 = bones[i].b.GetY();
				for (size_t j = i; j < bones.size(); ++j)
				{
					bones[j].worldAngle += DEGREE(phi);
					float xe = x0 + bones[j].length * cos(RADIAN(bones[j].worldAngle));
					float ye = y0 + bones[j].length * sin(RADIAN(bones[j].worldAngle));
					bones[j].b.Set(x0, y0);
					bones[j].e.Set(xe, ye);
					x0 = xe; y0 = ye;
				}

				effector = bones[effectorIndex].e;
			}
		}
	}
}

int Chain::GetBoneAt(int mx, int my)
{
	const float r = 3.5f;
	for (size_t i = 0; i < bones.size(); i++)
	{
		float x1 = bones[i].e.GetX();
		float y1 = bones[i].e.GetY();

		float dx = x1 - mx;
		float dy = y1 - my;
		if ( dx * dx + dy * dy < r * r )
		{
			return i;
		}
	}
	return -1;
}

BoneTree& BoneTree::AddChild(float length, float angle)
{
	BoneTree child;
	child.length = length;
	child.angle = angle;
	child.parent = this;
	child.worldAngle = worldAngle + angle;
	child.b = e;
	child.e.Set(e.GetX() + length * cos(RADIAN(child.worldAngle)), e.GetY() + length * sin(RADIAN(child.worldAngle)));
	children.push_back(child);

	return children[children.size() - 1];
}

void BoneTree::SetAngle(float newAngle)
{
	angle = newAngle;

	stack<BoneTree*> stack;
	stack.push(this);
	while (stack.size() != 0)
	{
		BoneTree* node = stack.top();
		stack.pop();
		if (node->parent != NULL)
		{
			node->b = node->parent->e;
			node->worldAngle = node->angle + node->parent->worldAngle;
		}
		else
		{
			node->worldAngle = node->angle;
		}
		node->e.Set(node->b.GetX() + node->length * cos(RADIAN(node->worldAngle)), 
			node->b.GetY() + node->length * sin(RADIAN(node->worldAngle)));
		for (int i = node->children.size() - 1; i >= 0; --i)
		{
			stack.push(&node->children[i]);
		}
	}
}

void BoneTree::Draw(Graphics2D& g, float rootAngle)
{
	g.PushMatrix();
	float newAngle = (rootAngle + angle);

	if ( length != 0 )
	{
		float x = length * cos(RADIAN(newAngle));
		float y = length * sin(RADIAN(newAngle));
		g.DrawLine(0, 0, x, y);
		g.DrawRect(x - 2, y - 2, 4.0f, 4.0f);
		g.Translate(x, y);
	}
	for (size_t i = 0; i < children.size(); i++)
	{
		children[i].Draw(g, newAngle);
	}
	g.PopMatrix();
}

void Rig::SetRoot(int x, int y)
{
	this->x = x;
	this->y = y;
	root.e.Set((float)x, (float)y);
}

// TODO: iterative version
BoneTree* BoneTree::GetBoneAt(int mx, int my, float r)
{
	for (size_t i = 0; i < children.size(); i++)
	{
		float x1 = children[i].e.GetX();
		float y1 = children[i].e.GetY();
		float dx = x1 - mx;
		float dy = y1 - my;
		if ( dx * dx + dy * dy < r * r )
		{
			return &children[i];
		}
		BoneTree* ret = children[i].GetBoneAt(mx, my);
		if (ret != NULL)
		{
			return ret;
		}
	}
	return NULL;
}

void Rig::Draw(Graphics2D& g)
{
	g.DrawRect(x - 3.0f, y - 3.0f, 6.0f, 6.0f);
	g.PushMatrix();
	g.Translate((float)x, (float)y);

	root.Draw(g, 0);

	g.PopMatrix();
}

void Rig::CcdIk(const Vector2& target, BoneTree* bone, int numIterations)
{
	Vector2 effector = bone->e;
	for ( int k = 0; k < numIterations; k++)
	{
		BoneTree* current = bone;
		BoneTree* parent = bone->parent;
		while ( parent != NULL )
		{
			Vector2 PE = effector;
			PE.Subtract(current->b);
			PE.Normalize();

			Vector2 PT = target;
			PT.Subtract(current->b);
			if (PT.GetX() != 0 && PT.GetY() != 0)
			{
				PT.Normalize();

				float c = PE.Dot(PT);
				float s = PE.Cross(PT);
				float phi = atan(s / c);
				if (c < 0) phi = PI - phi;

				current->SetAngle(current->angle + DEGREE(phi));

				effector = bone->e;
			}

			current = parent;
			parent = current->parent;
		}
	}
}

void Rig::Save(Stream& stream)
{
	stream.WriteUInt(x);
	stream.WriteUInt(y);
	stack<BoneTree*> stack;
	stack.push(&root);
	while (stack.size() != 0)
	{
		BoneTree* current = stack.top();		
		stack.pop();
		stream.WriteFloat(current->length);
		stream.WriteFloat(current->angle);
		int n = current->children.size();
		stream.WriteByte(n);		
		for (int i = n - 1; i >= 0; --i)
		{
			stack.push(&current->children[i]);
		}
	}
}

void Rig::Load(Stream& stream)
{
	x = stream.ReadUInt();
	y = stream.ReadUInt();
	root.e.Set((float)x, (float)y);
	root.children.clear();
	stack<BoneTree*> stack;
	stack.push(&root);
	while (stack.size() != 0)
	{
		BoneTree* current = stack.top();
		stack.pop();
		current->length = stream.ReadFloat();
		current->angle = stream.ReadFloat();
		if (current->parent != NULL)
		{
			current->b = current->parent->e;
			current->worldAngle = current->parent->worldAngle + current->angle;
			current->e.Set(current->b.GetX() + current->length * cos(RADIAN(current->worldAngle)), 
				current->b.GetY() + current->length * sin(RADIAN(current->worldAngle)));
		}
		int n = stream.ReadByte();
		for (int i = n - 1; i >= 0; --i)
		{
			BoneTree child;
			child.parent = current;
			current->children.push_back(child);
			stack.push(&current->children[i]);
		}
	}
}