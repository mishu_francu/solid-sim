#ifndef USE_GDIPLUS
#	define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif
// Windows Header Files:
#include <windows.h>
#include <tchar.h>

#include "SpriteTest.h"

// application entry point
int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	RUN_ENGINE(SpriteTest, hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}

