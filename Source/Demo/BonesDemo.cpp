//#include <Engine/Base.h>
#include <Engine/Engine.h>
#include "BonesDemo.h"
#include <Streams/FileStream.h>

b2Body* BonesDemo::CreateBox(float x, float y, float hw, float hh, float density, float friction)
{
	// Define the dynamic body. We set its position and call the body factory.
	b2BodyDef bodyDef;
	bodyDef.position.Set(x / 10.0f, -y / 10.0f);
	b2Body* body = world->CreateBody(&bodyDef);

	// Define another box shape for our dynamic body.
	b2PolygonDef shapeDef;
	shapeDef.SetAsBox(hw / 10.0f, hh / 10.0f);

	// Set the box density to be non-zero, so it will be dynamic.
	shapeDef.density = density;

	// Override the default friction.
	shapeDef.friction = friction;

	// Add the shape to the body.
	body->CreateShape(&shapeDef);

	// Now tell the dynamic body to compute it's mass properties base
	// on its shape.
	body->SetMassFromShapes();

	return body;
}

BonesDemo::BonesDemo(): sel(NULL)
{
	camera.Set(0, 0);
	// create physics world
	b2AABB worldAABB;
	worldAABB.lowerBound.Set(-100.0f, -100.0f);
	worldAABB.upperBound.Set(200.0f, 100.0f);

	// Define the gravity vector.
	b2Vec2 gravity(0.0f, -10.0f);

	// Do we want to let bodies sleep?
	bool doSleep = true;

	// Construct a world object, which will hold and simulate the rigid bodies.
	world = new b2World(worldAABB, gravity, doSleep);

	character = CreateBox(100, 20, 10, 20, 0.5f, 0);

	b2MassData inertia;
	inertia.center = character->GetLocalCenter();
	inertia.mass = character->GetMass();
	inertia.I = 0;

	character->SetMass(&inertia);
	character->AllowSleeping(false);

	FileStream file;
	file.Open("res/world.bin", Stream::STREAM_MODE_READ);
	while (!file.IsEOF())
	{
		uint8 type = file.ReadByte();
		if (type == TYPE_RIG)
		{
			rig.Load(file);
			objects.push_back(&rig);
		}
		else if (type == TYPE_BOX)
		{
			Box* box = new Box;
			box->dynamic = true;
			box->x = (float)file.ReadUShort();
			box->y = (float)file.ReadUShort();
			box->w = (float)file.ReadUShort();
			box->h = (float)file.ReadUShort();
			box->density = (float)file.ReadUShort();
			box->body = CreateBox(box->x + box->w / 2, box->y + box->h / 2, box->w / 2, box->h / 2, box->density );
			objects.push_back(box);
		}
	}
	file.Close();
}

BonesDemo::~BonesDemo()
{
	// free objects
	for (size_t i = 0; i < objects.size(); ++i)
	{
		if (objects[i]->dynamic)
		{
			delete objects[i];
		}
	}

	delete world;
}

#define SELECT 0
#define IK 1
#define ADD 2
#define BOX 3

void BonesDemo::Step(Engine* engine)
{
	// Prepare for simulation. Typically we use a time step of 1/60 of a
	// second (60Hz) and 10 iterations. This provides a high quality simulation
	// in most game scenarios.
	float32 timeStep = 1.0f / 60.0f;
	int32 iterations = 10;

	world->Step(timeStep, iterations);

	// draw physics world
	b2Vec2 position;
	float32 angle;

	position = character->GetPosition();
	angle = character->GetAngle();

	//if (position.x * 10 - camera.x > (3 * SCREEN_WIDTH / 4))
	//{
	//	camera.x += 100;
	//}

	engine->graphics->PushMatrix();
	engine->graphics->Translate(-camera.GetX(), -camera.GetY());

	engine->graphics->PushMatrix();
	engine->graphics->Translate(position.x * 10, -position.y * 10);
	engine->graphics->Rotate(-DEGREE(angle));
	engine->graphics->FillRect(-10, -20, 20, 40);
	engine->graphics->PopMatrix();

	for (size_t i = 0; i < objects.size(); ++i)
	{
		if (objects[i]->type == TYPE_BOX)
		{
			Box* box = reinterpret_cast<Box*>(objects[i]);
			position = box->body->GetPosition();
			angle = box->body->GetAngle();
			engine->graphics->PushMatrix();
			engine->graphics->Translate(position.x * 10, -position.y * 10);
			engine->graphics->Rotate(-DEGREE(angle));
			engine->graphics->DrawRect(-box->w / 2, -box->h / 2, box->w, box->h);
			engine->graphics->PopMatrix();
		}
		if (objects[i]->type == TYPE_RIG)
		{
			Rig* rig = reinterpret_cast<Rig*>(objects[i]);
			rig->Draw(*(engine->graphics));
		}
	}
	if (sel != NULL)
	{
		engine->graphics->DrawRect(sel->e.GetX() - 3, sel->e.GetY() - 3, 6.0f, 6.0f);
	}
	if (engine->isKeyPressed('I'))
	{
		mode = IK;
	}
	if (engine->isKeyPressed('S'))
	{
		mode = SELECT;
	}
	if (engine->isKeyPressed('A'))
	{
		mode = ADD;
	}
	if (engine->isKeyPressed('B'))
	{
		mode = BOX;
	}

	if (mode == SELECT)
	{
		if (engine->isKeyPressed(VK_LBUTTON))
		{
			POINT point;
			engine->GetMousePosition(point);
			sel = rig.root.GetBoneAt(point.x, point.y, 4);
		}
	}
	else if (mode == ADD)
	{
		if (sel != NULL && engine->isKeyPressed(VK_LBUTTON))
		{
			POINT point;
			engine->GetMousePosition(point);
			Vector2 link;
			link.Set((float)point.x, (float)point.y);
			link.Subtract(sel->e);
			float len = link.Length();
			if (len != 0)
			{
				link.Normalize();
				Vector2 parent = sel->e;
				parent.Subtract(sel->b);
				parent.Normalize();
				float c = parent.Dot(link);
				float s = parent.Cross(link);
				float phi = atan(s / c);
				if (c < 0) phi = PI - phi;
				
				sel->AddChild(len, phi);
			}
		}
	}
	else if (mode == IK)
	{
		if (engine->isKeyPressed(VK_LBUTTON))
		{
			POINT point;
			engine->GetMousePosition(point);
			if (sel != NULL)
			{
				target.Set((float)point.x, (float)point.y);
				rig.CcdIk(target, sel);
			}
		}
		engine->graphics->DrawRect(target.GetX() - 2, target.GetY() - 2, 4.0f, 4.0f);
	}

	// TODO: find out if the character is resting on the ground
	b2Vec2 vel = character->GetLinearVelocity();
//	if (fabs(vel.y) < 1.0f)
	{
		// keyframed motion
		if (engine->isKeyPressed(VK_UP))
		{		
			b2Vec2 imp;
			imp.x = 0.0f;
			if (engine->isKeyDown(VK_RIGHT))
			{
				imp.x = 10.0f;
			}
			if (engine->isKeyDown(VK_LEFT))
			{
				imp.x = -10.0f;
			}
			imp.y = 50.0f;
			character->ApplyImpulse(imp, character->GetWorldCenter());
			//camera.y -= 10;
		}
		if (engine->wasKeyReleased(VK_RIGHT) || engine->wasKeyReleased(VK_LEFT))
		{
			vel.x = 0;
			character->SetLinearVelocity( vel );
		}		
		if ( engine->isKeyDown(VK_RIGHT) )
		{
			vel.x = 10.0f;
			character->SetLinearVelocity( vel );
			camera.Set(camera.GetX() + 1, camera.GetY());
		}
		if (engine->isKeyDown(VK_LEFT))
		{
			vel.x = -10.0f;
			character->SetLinearVelocity( vel );
			camera.Set(camera.GetX() - 1, camera.GetY());
		}
	}

	engine->graphics->PopMatrix();
}