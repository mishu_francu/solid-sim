#include "FluidsDemo.h"

#include <Graphics2D/Graphics2D.h>

const float dx = 25.f;

FluidsDemo::FluidsDemo() //: fluid((int)(getScreenWidth() / dx), (int)(getScreenHeight() / dx), dx)
	: fluid(15, 15, dx)
{
}

void FluidsDemo::OnDraw()
{
	float h = fluid.GetNumCellsY() * fluid.GetCellSize();
	float w = fluid.GetNumCellsX() * fluid.GetCellSize();
	
	// draw grid
	//for (int i = 0; i <= fluid.GetNumCellsX(); i++)
	//{
	//	float x = i * fluid.GetCellSize();
	//	graphics->DrawLine(x, 0, x, h);
	//}
	//for (int i = 0; i <= fluid.GetNumCellsY(); i++)
	//{
	//	float y = i * fluid.GetCellSize();
	//	graphics->DrawLine(0, y, w, y);
	//}

	for (int i = 0; i <= fluid.GetNumCellsX(); i++)
	{
		for (int j = 0; j <= fluid.GetNumCellsY(); j++)
		{
			//graphics->SetColor(0xff0000ff);
			//graphics->FillRect(i * dx + 1, j * dx + 1, dx - 2, dx - 2);

			graphics->SetColor(0xffffffff);

			if (i < fluid.GetNumCellsX() && j < fluid.GetNumCellsY())
			{
				Vector2 p((i + 0.5f) * fluid.GetCellSize(), (j + 0.5f) * fluid.GetCellSize());
				Vector2 vel = fluid.GetVelocity(i, j);
				graphics->DrawLine(p, p + vel);
				graphics->DrawCircle(p.GetX(), p.GetY(), 2);
				//graphics->DrawFormattedString(p.GetX(), p.GetY(), "%.2f", fluid.GetPressure(i, j));
			}
			//if (i < fluid.GetNumCellsX())
			//{
			//	Vector2 p((i + 0.5f) * fluid.GetCellSize(), j * fluid.GetCellSize());
			//	Vector2 vel(0, fluid.GetV(i, j));

			//	graphics->DrawLine(p, p + vel * 5.f);
			//	//graphics->DrawCircle(p.x, p.y, 2);
			//}
			//if (j < fluid.GetNumCellsY())
			//{
			//	Vector2 p(i * fluid.GetCellSize(), (j + 0.5f) * fluid.GetCellSize());
			//	Vector2 vel(fluid.GetU(i, j), 0);

			//	graphics->DrawLine(p, p + vel * 5.f);
			//	//graphics->DrawCircle(p.x, p.y, 2);
			//}
		}
	}

	//if (isKeyPressed('T'))
		fluid.Step(0.016f);	
}