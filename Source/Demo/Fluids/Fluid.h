#ifndef FLUID_H
#define FLUID_H

#include <vector>

#include <Math/Vector2.h>

// Eulerian fluid - Bridson
class Fluid
{
private:
	int nx, ny; // number of divisions

	float dx; // size of the grid

	float **u, **v, **unew, **vnew; // velocity field

	std::vector<float> p; // pressure field

private:
	void AdvectVelocity(float dt);
	
	void AdvectAdditional(float** q, float dt);

	void Project();

public:
	Fluid(int sx, int sy, float size);

	int GetNumCellsX() const { return nx; }
	int GetNumCellsY() const { return ny; }
	float GetCellSize() const { return dx; }

	float GetU(int i, int j) { return u[i][j]; }
	float GetV(int i, int j) { return v[i][j]; }
	Vector2 GetAvgVelocity(int i, int j) { return Vector2(0.5f * (u[i][j] + u[i + 1][j]), 0.5f * (v[i][j] + v[i][j + 1])); }

	float GetPressure(int i, int j) { return p[i + j * nx]; }

	void Step(float dt);
};

class FluidStam
{
public:
	FluidStam(int sx, int sy, float size);
	int GetNumCellsX() const { return nx + 2; }
	int GetNumCellsY() const { return ny + 2; }
	float GetCellSize() const { return dx; }
	size_t GetIndex(int i, int j) const { return i + (nx + 2) * j; }
	size_t IX(int i, int j) const { return i + (nx + 2) * j; }
	Vector2 GetVelocity(int i, int j) const { return Vector2(u[GetIndex(i, j)], v[GetIndex(i, j)]); }
	float GetPressure(int i, int j) const { return p[GetIndex(i, j)]; }

	void Step(float dt);

private:
	void Advect(float dt, std::vector<float>& dst, const std::vector<float>& src);
	void SetBounds(int b, std::vector<float>& x);
	void SetBoundsV();
	void SetBoundsS(std::vector<float>& x);
	void Project();

private:
	std::vector<float> p; // pressure field
	std::vector<float> u; // x velocity field
	std::vector<float> v; // y velocity field
	int nx, ny; // x, y divisions
	float dx; // grid granularity
};

#endif // FLUID_H