#include "Fluid.h"

#include <string.h>
#include <Engine/Base.h>
#include <Math/Matrix.h>

Fluid::Fluid(int sx, int sy, float size)
	: nx(sx)
	, ny(sy)
	, dx(size)
{
	ASSERT(size != 0);
	u = new float*[nx + 1];
	v = new float*[nx];
	unew = new float*[nx + 1];
	vnew = new float*[nx];
	for (int i = 0; i <= nx; i++)
	{
		if (i < nx)
		{
			v[i] = new float[ny + 1];
			memset(v[i], 0, (ny + 1) * sizeof(float));
			vnew[i] = new float[ny + 1];
			memset(vnew[i], 0, (ny + 1) * sizeof(float));
		}
		u[i] = new float[ny];
		memset(u[i], 0, ny * sizeof(float));
		unew[i] = new float[ny];
		memset(unew[i], 0, ny * sizeof(float));
	}

	// random values
	//for (int j = 0; j <= ny; j++)
	//{
	//	for (int i = 0; i <= nx; i++)
	//	{
	//		if (j < ny)
	//		{
	//			//u[i][j] = 10.f - 0.1f * (rand() % 200);
	//			u[i][j] = i == 0 ? 10.f : 1.f;
	//		}
	//		if (i < nx)
	//		{
	//			//v[i][j] = 10.f - 0.1f * (rand() % 200);
	//			v[i][j] = j == 0 ? 12.f : 1.f;
	//		}
	//	}
	//}

	p.resize(nx * ny);
}

void Fluid::Step(float dt)
{
	AdvectVelocity(dt);

	// apply gravity (or body forces)
	for (int i = 0; i < nx; i++)
	{
		for (int j = 0; j <= ny; j++)
		{
			v[i][j] += dt * 9.8f; // TODO: length scale
		}
	}

	Project();
}

void Fluid::AdvectAdditional(float** q, float dt)
{
	float** qnew = new float*[nx]; // TODO: permananent buffer
	for (int i = 0; i < nx; i++)
	{
		qnew[i] = new float[ny];
		for (int j = 0; j < ny; j++)
		{
			Vector2 xg(i * dx, j * dx);
			Vector2 vel = GetAvgVelocity(i, j);
			Vector2 xp = xg - Scalar(dt) * vel;

			float ipf = xp.GetX() / dx;
			float jpf = xp.GetY() / dx;
			int ip = (int)ipf;
			int jp = (int)jpf;
			// TODO: check if in grid
			float alpha = ipf - ip;
			float beta = jpf - jp;
			float omAlpha = 1.f - alpha;
			float omBeta = 1.f - beta;

			// bilinear interpolation
			qnew[i][j] = alpha * beta * q[ip][jp] + omAlpha * beta * q[ip + 1][jp] + omBeta * alpha * q[ip][jp + 1] + omAlpha * omBeta * q[ip + 1][jp + 1];
		}
	}
	// copy back the new data
	for (int i = 0; i < nx; i++)
	{
		for (int j = 0; j < ny; j++)
		{
			q[i][j] = qnew[i][j];
		}
		delete[] qnew[i];
	}
	delete[] qnew;
}

void Fluid::AdvectVelocity(float dt)
{
	// advect u
	for (int i = 0; i <= nx; i++)
	{
		for (int j = 0; j < ny; j++)
		{
			Vector2 xg(i * dx, (j + 0.5f) * dx);
			float v1 = i < nx ? v[i][j] : 0.f;
			float v2 = i < nx ? v[i][j + 1] : 0.f;
			float v3 = i < nx - 1 ? v[i + 1][j] : 0.f; // boundary condition
			float v4 = i < nx - 1 ? v[i + 1][j + 1] : 0.f; // boundary condition
			Vector2 vel(u[i][j], 0.25f * (v1 + v2 + v3 + v4));
			Vector2 xp = xg - Scalar(dt) * vel;

			float ipf = xp.GetX() / dx;
			float jpf = xp.GetY() / dx - 0.5f;
			int ip = floor(ipf);
			int jp = floor(jpf);
			float alpha = ipf - ip;
			float beta = jpf - jp;

			float u1 = ip >= 0 && jp >= 0? u[ip][jp] : 0.f; 
			float u2 = ip < nx && jp >= 0 ? u[ip + 1][jp] : 0.f;
			float u3 = jp < ny - 1 && ip >= 0 ? u[ip][jp + 1] : 0.f;
			float u4 = ip < nx && jp < ny - 1 ? u[ip + 1][jp + 1] : 0.f;

			if (jp < 0)
				beta = 1.f;
			else if (jp >= ny - 1)
				beta = 0.f;
			if (ip < 0)
				alpha = 1.f;
			else if (ip >= nx)
				alpha = 0.f;
			float omAlpha = 1.f - alpha;
			float omBeta = 1.f - beta;
			unew[i][j] = omBeta * (omAlpha * u1 + alpha * u2) + beta * (omAlpha * u3 + alpha * u4);
		}
	}
	// advect v
	for (int i = 0; i < nx; i++)
	{
		for (int j = 0; j <= ny; j++)
		{
			Vector2 xg((i + 0.5f) * dx, j * dx);
			float u1 = j < ny ? u[i][j] : 0.f;
			float u2 = j < ny - 1 ? u[i][j + 1] : 0.f;
			float u3 = j < ny ? u[i + 1][j] : 0.f; // boundary condition
			float u4 = j < ny - 1 ? u[i + 1][j + 1] : 0.f; // boundary condition
			Vector2 vel(0.25f * (u1 + u2 + u3 + u4), v[i][j]);
			Vector2 xp = xg - Scalar(dt) * vel;

			float ipf = xp.GetX() / dx - 0.5f;
			float jpf = xp.GetY() / dx;
			int ip = floor(ipf);
			int jp = floor(jpf);
			float alpha = ipf - ip;
			float beta = jpf - jp;

			float v1 = ip >= 0 && jp >= 0 ? v[ip][jp] : 0.f;
			float v2 = ip < nx - 1 && jp >= 0 ? v[ip + 1][jp] : 0.f;
			float v3 = jp < ny && ip >= 0 ? v[ip][jp + 1] : 0.f;
			float v4 = ip < nx - 1 && jp < ny ? v[ip + 1][jp + 1] : 0.f;

			if (jp < 0)
				beta = 1.f;
			else if (jp >= ny)
				beta = 0.f;
			if (ip < 0)
				alpha = 1.f;
			else if (ip >= nx - 1)
				alpha = 0.f;
			float omAlpha = 1.f - alpha;
			float omBeta = 1.f - beta;
			vnew[i][j] = omBeta * (omAlpha * v1 + alpha * v2) + beta * (omAlpha * v3 + alpha * v4);
		}
	}
	// copy back the new data
	for (int i = 0; i <= nx; i++)
	{
		for (int j = 0; j <= ny; j++)
		{
			if (j < ny)
				u[i][j] = unew[i][j];
			if (i < nx)
				v[i][j] = vnew[i][j];
		}
	}
}

void Fluid::Project()
{
	// build matrix and rhs
	int n = nx * ny;
	MatrixSqr<float> mat(n); // TODO: int matrix?
	mat.SetZero();

	std::vector<float> rhs(n);
	for (int i = 0; i < nx; i++)
	{
		for (int j = 0; j < ny; j++)
		{
			int k = i + j * nx;
			int c = 0;
			rhs[k] = 0.f;
			if (j - 1 >= 0)
			{
				mat.Get(k, k - nx) = -1.f;
				rhs[k] += v[i][j];
				c++;
			}
			if (i - 1 >= 0)
			{
				mat.Get(k, k - 1) = -1.f;
				rhs[k] += u[i][j];
				c++;
			}
			if (i + 1 < nx)
			{
				mat.Get(k, k + 1) = -1.f;
				rhs[k] -= u[i + 1][j];
				c++;
			}
			if (j + 1 < ny)
			{
				mat.Get(k, k + nx) = -1.f;
				rhs[k] -= v[i][j + 1];
				c++;
			}
			mat.Get(k, k) = c;
		}
	}

	// print the matrix
	Printf("Matrix A\n");
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			Printf("%2.0f ", mat.Get(i, j));
			//if (mat.Get(i, j) != 0.f)
			//	Printf("* ");
			//else
			//	Printf("  ");
		}
		Printf(" = %e\n", rhs[i]);
	}

	// LU decomposition
	MatrixSqr<float> L(n), U(n);
	mat.DecomposeLU(L, U);
	//std::vector<float> p(n);
	SolveLU(L, U, rhs, p);

	// update velocities from pressure
	for (int i = 0; i < nx; i++)
	{
		for (int j = 0; j < ny; j++)
		{
			int k = i + j * nx;
			u[i][j] -= p[k];
			u[i + 1][j] += p[k];
			v[i][j] -= p[k];
			v[i][j + 1] += p[k];
			Printf("%f ", p[k]);
		}
		Printf("\n");
	}
}


// ****** collocated grid **************

FluidStam::FluidStam(int sx, int sy, float size)
{
	nx = sx;
	ny = sy;
	dx = size;
	size_t n = (sx + 2) * (sy + 2);
	p.resize(n);
	u.resize(n);
	v.resize(n);

	// random vel field
	const float maxVel = 20.f;
	for (int i = 0; i <= nx + 1; i++)
	{
		for (int j = 0; j <= ny + 1; j++)
		{
			int idx = GetIndex(i, j);
			u[idx] = GetRandomReal11() * maxVel + 10;
			//u[idx] = -maxVel * (i - 0.5f * nx) / nx;
			v[idx] = GetRandomReal11() * maxVel + 10;
			//v[idx] = -maxVel * (j - 0.5f * ny) / nx;
		}
	}
	SetBoundsV();
}

void FluidStam::SetBoundsV()
{
	//SetBounds(1, u);
	//SetBounds(2, v);

	float k = 0.f;
	for (int i = 0; i <= nx + 1; i++)
	{
		v[IX(i, 0)] = k * fabs(v[IX(i, 1)]);
		v[IX(i, ny + 1)] = -k * fabs(v[IX(i, ny)]);
		u[IX(i, 0)] = u[IX(i, 1)];
		u[IX(i, ny + 1)] = u[IX(i, ny)];
	}
	for (int j = 0; j <= ny + 1; j++)
	{
		u[IX(0, j)] = k * fabs(u[IX(1, j)]);
		u[IX(nx + 1, j)] = -k * fabs(u[IX(nx, j)]);
		v[IX(0, j)] = v[IX(1, j)];
		v[IX(nx + 1, j)] = v[IX(nx, j)];
	}
}

void FluidStam::SetBoundsS(std::vector<float>& x)
{
	// top and bottom
	for (int i = 0; i <= nx + 1; i++)
	{
		x[IX(i, 0)] = x[IX(i, 1)];
		x[IX(i, ny + 1)] = x[IX(i, ny)];
	}
	// left and right
	for (int j = 0; j <= ny + 1; j++)
	{
		x[IX(0, j)] = x[IX(1, j)];
		x[IX(nx + 1, j)] = x[IX(nx, j)];
	}
}

void FluidStam::Advect(float dt, std::vector<float>& dst, const std::vector<float>& src)
{
	for (int i = 1; i <= nx; i++)
	{
		for (int j = 1; j <= ny; j++)
		{
			int idx = GetIndex(i, j);
			float x = i - dt * u[idx];
			float y = j - dt * v[idx];
			// bilinear interpolation
			if (x < 0.5f) x = 0.5f;
			if (x > nx + 0.5f) x = nx + 0.5f; 
			int i0 = (int)x; 
			int i1 = i0 + 1;
			if (y < 0.5f) y = 0.5f;
			if (y > ny + 0.5f) y = ny + 0.5f;
			int j0 = (int)y; 
			int j1 = j0 + 1;
			float s1 = x - i0;
			float s0 = 1.f - s1;
			float t1 = y - j0;
			float t0 = 1.f - t1;
			dst[idx] = s0 * (t0 * src[GetIndex(i0, j0)] + t1 *src[GetIndex(i0, j1)])+
				s1 * (t0 * src[GetIndex(i1, j0)] + t1 * src[GetIndex(i1, j1)]);
		}
	}
}

void FluidStam::Step(float dt)
{
	std::vector<float> uNew(u.size());
	std::vector<float> vNew(v.size());
	Advect(dt, uNew, u);
	Advect(dt, vNew, v);
	u.swap(uNew);
	v.swap(vNew);
	SetBoundsV();

	Project();
}

void FluidStam::SetBounds(int b, std::vector<float>& x)
{
	int N = nx;
	for (int i=1 ; i<=N ; i++ ) {
		x[GetIndex(0 ,i)] = b==1 ? -x[GetIndex(1,i)] : x[GetIndex(1,i)];
		x[GetIndex(N+1,i)] = b==1 ? -x[GetIndex(N,i)] : x[GetIndex(N,i)];
		x[GetIndex(i,0 )] = b==2 ? -x[GetIndex(i,1)] : x[GetIndex(i,1)];
		x[GetIndex(i,N+1)] = b==2 ? -x[GetIndex(i,N)] : x[GetIndex(i,N)];
	}
	x[GetIndex(0 ,0 )] = 0.5*(x[GetIndex(1,0 )]+x[GetIndex(0 ,1)]);
	x[GetIndex(0 ,N+1)] = 0.5*(x[GetIndex(1,N+1)]+x[GetIndex(0 ,N )]);
	x[GetIndex(N+1,0 )] = 0.5*(x[GetIndex(N,0 )]+x[GetIndex(N+1,1)]);
	x[GetIndex(N+1,N+1)] = 0.5*(x[GetIndex(N,N+1)]+x[GetIndex(N+1,N )]);
}

void FluidStam::Project()
{
	std::vector<float> div(p.size());
	float h = 1.f / nx;
	for (int i=1 ; i<=nx ; i++ ) {
		for (int j=1 ; j<=ny ; j++ ) {
			div[GetIndex(i,j)] = -0.5*h*(u[GetIndex(i+1,j)]-u[GetIndex(i-1,j)]+
				v[GetIndex(i,j+1)]-v[GetIndex(i,j-1)]);
			p[GetIndex(i,j)] = 0;
		}
	}
	SetBounds(0, div);
	SetBounds(0, p);
	div[IX(nx/2, ny/2)] += 10;
	for (int k=0 ; k<20 ; k++ ) {
		for (int i=1 ; i<=nx ; i++ ) {
			for (int j=1 ; j<=ny ; j++ ) {
				p[IX(i,j)] = (div[IX(i,j)]+p[IX(i-1,j)]+p[IX(i+1,j)]+
					p[IX(i,j-1)]+p[IX(i,j+1)])/4;
			}
		}
		SetBounds(0, p);
	}
	for (int i=1 ; i<=nx ; i++ ) {
		for (int j=1 ; j<=ny ; j++ ) {
			u[IX(i,j)] -= 0.5*(p[IX(i+1,j)]-p[IX(i-1,j)])/h;
			v[IX(i,j)] -= 0.5*(p[IX(i,j+1)]-p[IX(i,j-1)])/h;
		}
	}
	SetBoundsV();
	//SetBounds(1, u);
	//SetBounds(2, v);
}