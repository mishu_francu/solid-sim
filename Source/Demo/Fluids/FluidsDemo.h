#ifndef FLUIDS_DEMO_H
#define FLUIDS_DEMO_H

#include <Engine/Engine.h>
#include "Fluid.h"

class FluidsDemo : public Engine
{
private:
	FluidStam fluid;

	void OnDraw();

public:
	FluidsDemo();
};

#endif // FLUIDS_DEMO_H