#pragma once

#include "Engine/Engine.h"
#include "Geometry/Mesh.h"
#include "Geometry/MeshIntersector.h"

#include <vector>

using namespace Geometry; // careful with this one

class CollisionDemo : public Engine
{
public:
	enum DrawMode
	{
		INVISIBLE = 0,
		SHADED,
		SHADED_WITH_EDGES,
		WIREFRAME
	};

public:
	CollisionDemo();

	void OnCreate() override;
	void OnDraw() override;
	void OnDraw3D() override;

private:
	struct Triangle
	{
		uint32 i1, i2, i3;
		float area;
		float du1, du2, dv1, dv2, invDet, dv12, du12;
		float lu2, lv2;

		uint32 GetOppositeVertex(uint32 a, uint32 b)
		{
			if (i1 != a && i1 != b)
				return i1;
			if (i2 != a && i2 != b)
				return i2;
			return i3;
		}
	};

	struct IntersectionSegment
	{
		uint32 i1, i2;
	};

	enum DemoType
	{
		DEMO_TWO_MESHES,
		DEMO_TRIANGLE,
		DEMO_MESH,
		DEMO_TWO_TRIANGLES,
		DEMO_EDGE_TRIANGLES,
	};

	enum DemoTwoMeshesType
	{
		DTM_BUNNY_AND_DRAGON,
		DTM_TWO_SPHERES,
		DTM_PREVIOUS_MESHES,
		DTM_MAYA_SPHERES,
		DTM_CUBE_SPHERE,
	};

	struct BendConstraint
	{
		uint32 i1, i2, i3, i4;
		float theta0; // the initial angle
		float l1, l2;
	};

private:
	void Reset();
	void InitMesh(Mesh& mesh, std::vector<Triangle>& triangles, std::vector<float>& edgeLens, std::vector<BendConstraint>& bends);

	void Project(float step, const Vector3& dir, Mesh& mesh, const std::vector<int>& selection);

	void AddBendConstraint(const Mesh& mesh, std::vector<BendConstraint>& bends, const Mesh::Edge& e);

	void ProjectLengths(float factor, Mesh& mesh, const std::vector<float>& lengths);
	void ProjectBending(float factor, Mesh& mesh, const std::vector<BendConstraint>& bends);
	void ProjectTriangles(Mesh& mesh, const std::vector<Triangle>& triangles);
	void ProjectArea(float factor, Mesh& mesh, const std::vector<Triangle>& triangles);

	void Smooth(Mesh& mesh, const std::vector<Triangle>& triangles, const std::vector<BendConstraint>& bends, int iterations = 100);

	void DrawIndex(int i, const Mesh& mesh, bool backFaceCulling = true);
	void DrawTriIndex(size_t i, const Mesh& mesh, bool backFaceCulling = true);
	void DrawEdgeIndex(size_t i, const Mesh& mesh, bool backFaceCulling = true);

	Vector3 ComputeDirection(const Mesh& mesh, const std::vector<MeshIntersector::ProxyInfo>& vertexInfos);

	void DrawUI();
	void DrawVertexInfos(int meshIdx, const std::vector<MeshIntersector::ProxyInfo>& vertexInfos);

private:
	DemoType mDemoType = DEMO_TWO_MESHES;

	Mesh mMesh[2];

	MeshIntersector mMeshIntersector;

	std::vector<Triangle> mTriangles1;
	std::vector<Triangle> mTriangles2;

	std::vector<float> mEdgeLens1;
	std::vector<float> mEdgeLens2;

	std::vector<BendConstraint> mBends1;
	std::vector<BendConstraint> mBends2;

	Vector3 mDir1, mDir2;

	// state
	bool mDrawIndices = false;
	bool mDrawTriIndices = false;
	bool mDrawEdgeIndices = false;
	int mFilterIndex = -1;
	int mDrawMesh[2];
	bool mDrawNormals[2];
	bool mDrawIntPoints = true;
	bool mDrawContour = true;
	bool mDrawEdge = false;
	bool mDrawTriangle = false;
	bool mDrawInfo1 = true;
	bool mDrawInfo2 = true;
	bool mDrawIntMesh1 = false;
	bool mDrawIntMesh2 = false;

	float mStep = 0.5f;
	float mPointSize = 0.05f;
	float mCameraSpeed = 0.1f;

	Vector3 mPos1;
	Matrix4 mTransform1;
	Matrix4 mTransform2;

	int mDtmType = DTM_CUBE_SPHERE;

	std::vector<Vector3> mProjections1;
	std::vector<Vector3> mProjections2;

	std::vector<int> mSelection[2];

	int mCurrMesh = 0;

	// Laplacian smoothing
	int mSmoothIters = 2;
	float mSmoothStep = 0.5f;
	bool mSmoothTaubin = false;
	bool mSmoothNormals = false;
};

