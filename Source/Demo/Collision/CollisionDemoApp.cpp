#include "CollisionDemoApp.h"

#include "Graphics3D/Graphics3D.h"
#include "Geometry/Assets.h"
#include "Geometry/SmoothMesh.h"
#include "Graphics2D/Graphics2D.h"

using namespace Math;

CollisionDemo::CollisionDemo()
{
	is3D = true;
	canResize = true;
	drawFps = true;
	mDrawMesh[0] = mDrawMesh[1] = DrawMode::SHADED;
	mDrawNormals[0] = mDrawNormals[1] = false;
}

void CollisionDemo::Smooth(Mesh& mesh, const std::vector<Triangle>& triangles, const std::vector<BendConstraint>& bends, int iterations)
{
	for (int iter = 0; iter < iterations; iter++)
	{
		//ProjectLengths(0.1f, mMesh1, mEdgeLens1);
		ProjectArea(0.7f, mesh, triangles);
		ProjectBending(0.1f, mesh, bends);
	}
}

Vector3 CollisionDemo::ComputeDirection(const Mesh& mesh, const std::vector<MeshIntersector::ProxyInfo>& vertexInfos)
{
	Vector3 dir;
	for (int i = 0; i < vertexInfos.size(); i++)
	{
		if (vertexInfos[i].side == MeshIntersector::SideType::INTERIOR)
		{
			dir += mesh.normals[i];
		}
	}
	dir.Normalize();
	return dir;
}


void CollisionDemo::DrawIndex(int i, const Mesh& mesh, bool backFaceCulling)
{
	if (backFaceCulling)
	{
		Vector3 viewDir = graphics3D->camera.GetViewDir();
		Vector3 normalDir = mesh.normals[i];
		if (viewDir.Dot(normalDir) <= 0)
			return;
	}

	graphics->SetColor(0xff000000);

	float sx, sy;
	graphics3D->GetScreenCoords(mesh.vertices[i], sx, sy);
	graphics->DrawFormattedString(sx, sy, "%d", i);
}

void CollisionDemo::DrawTriIndex(size_t i, const Mesh& mesh, bool backFaceCulling)
{
	const Vector3& a = mesh.vertices[mesh.indices[i * 3]];
	const Vector3& b = mesh.vertices[mesh.indices[i * 3 + 1]];
	const Vector3& c = mesh.vertices[mesh.indices[i * 3 + 2]];

	if (backFaceCulling)
	{
		Vector3 viewDir = graphics3D->camera.GetViewDir();
		Vector3 normalDir = (b - a).Cross(c - a);
		if (viewDir.Dot(normalDir) <= 0)
			return;
	}

	graphics->SetColor(0xff000000);

	float ax, ay;
	graphics3D->GetScreenCoords(a, ax, ay);
	float bx, by;
	graphics3D->GetScreenCoords(b, bx, by);
	float cx, cy;
	graphics3D->GetScreenCoords(c, cx, cy);
	float sx = 0.33f * (ax + bx + cx);
	float sy = 0.33f * (ay + by + cy);
	graphics->DrawFormattedString(sx, sy, "%d", i);
}

void CollisionDemo::DrawEdgeIndex(size_t i, const Mesh& mesh, bool backFaceCulling)
{
	const Mesh::Edge& edge = mesh.edges[i];
	const Vector3& p = mesh.vertices[edge.i1];
	const Vector3& q = mesh.vertices[edge.i2];

	if (backFaceCulling)
	{
		Vector3 viewDir = graphics3D->camera.GetViewDir();
		Vector3 normalDir = 0.5f * (mesh.normals[edge.i1] + mesh.normals[edge.i2]);
		if (viewDir.Dot(normalDir) <= 0)
			return;
	}

	graphics->SetColor(0xff000000);

	float ax, ay;
	graphics3D->GetScreenCoords(p, ax, ay);
	float bx, by;
	graphics3D->GetScreenCoords(q, bx, by);
	float sx = 0.5f * (ax + bx);
	float sy = 0.5f * (ay + by);
	graphics->DrawFormattedString(sx, sy, "%d", i);
}

void CollisionDemo::InitMesh(Mesh& mesh, std::vector<Triangle>& triangles, std::vector<float>& edgeLens, std::vector<BendConstraint>& bends)
{
	Printf("The mesh has %d vertices and %d triangles.\n", mesh.vertices.size(), mesh.GetNumTriangles());

	size_t count = mesh.RemoveDuplicatedVertices();
	if (count > 0)
	{
		Printf("Removed %d duplicated vertices.\n", count);
	}
	//if (count > 0 || mesh.normals.empty())
	{
		mesh.ComputeNormals();
	}
	mesh.ConstructEdges();
	mesh.ConstructOneRings();

	triangles.clear();
	triangles.resize(mesh.GetNumTriangles());
	for (size_t i = 0; i < mesh.GetNumTriangles(); i++)
	{
		triangles[i].i1 = mesh.indices[i * 3];
		triangles[i].i2 = mesh.indices[i * 3 + 1];
		triangles[i].i3 = mesh.indices[i * 3 + 2];
		const Vector3& x1 = mesh.vertices[triangles[i].i1];
		const Vector3& x2 = mesh.vertices[triangles[i].i2];
		const Vector3& x3 = mesh.vertices[triangles[i].i3];
		Vector3 n = cross(x2 - x1, x3 - x1);
		triangles[i].area = 0.5f * n.Length();
	}

	edgeLens.resize(mesh.edges.size());
	bends.clear();
	for (int i = 0; i < mesh.edges.size(); i++)
	{
		int i1 = mesh.edges[i].i1;
		int i2 = mesh.edges[i].i2;
		edgeLens[i] = (mesh.vertices[i1] - mesh.vertices[i2]).Length();
		AddBendConstraint(mesh, bends, mesh.edges[i]);
	}
}

void CollisionDemo::AddBendConstraint(const Mesh& mesh, std::vector<BendConstraint>& bends, const Mesh::Edge& e)
{
	// check for border edge
	if (e.t2 < 0)
		return;

	Triangle tri1;
	tri1.i1 = mesh.indices[e.t1 * 3];
	tri1.i2 = mesh.indices[e.t1 * 3 + 1];
	tri1.i3 = mesh.indices[e.t1 * 3 + 2];

	Triangle tri2;
	tri2.i1 = mesh.indices[e.t2 * 3];
	tri2.i2 = mesh.indices[e.t2 * 3 + 1];
	tri2.i3 = mesh.indices[e.t2 * 3 + 2];

	Vector3 n1 = cross(mesh.vertices[tri1.i2] - mesh.vertices[tri1.i1],	mesh.vertices[tri1.i3] - mesh.vertices[tri1.i1]);
	float l1 = n1.Length();
	n1.Normalize();
	Vector3 n2 = cross(mesh.vertices[tri2.i2] - mesh.vertices[tri2.i1], mesh.vertices[tri2.i3] - mesh.vertices[tri2.i1]);
	float l2 = n2.Length();
	n2.Normalize();

	BendConstraint bc;
	bc.i1 = e.i1;
	bc.i2 = e.i2;
	bc.i3 = tri1.GetOppositeVertex(e.i1, e.i2);
	bc.i4 = tri2.GetOppositeVertex(e.i1, e.i2);
	bc.theta0 = acosf(n1 * n2);
	bc.l1 = l1;
	bc.l2 = l2;
	bends.push_back(bc);
}

// TODO: make it a special case of project on mesh
void CollisionDemo::Project(float step, const Vector3& dir, Mesh& mesh, const std::vector<int>& selection)
{
	// project mesh along vertex normals
	for (int i = 0; i < selection.size(); i++)
	{
		int j = selection[i];
		mesh.vertices[j] -= step * mesh.normals[j];
	}
	mesh.ComputeNormals();
}

void CollisionDemo::ProjectLengths(float factor, Mesh& mesh, const std::vector<float>& lengths)
{
	for (int i = 0; i < mesh.edges.size(); i++)
	{
		const Mesh::Edge& edge = mesh.edges[i];
		int i1 = edge.i1;
		int i2 = edge.i2;
		Vector3 delta = mesh.vertices[i1] - mesh.vertices[i2];
		float len = delta.Length();
		delta *= 1.0f / len;
		float lambda = factor * 0.5f * (len - lengths[i]);
		mesh.vertices[i1] -= lambda * delta;
		mesh.vertices[i2] += lambda * delta;
	}
}

void CollisionDemo::ProjectBending(float factor, Mesh& mesh, const std::vector<BendConstraint>& bends)
{
	for (size_t i = 0; i < bends.size(); i++)
	{
		const BendConstraint& bc = bends[i];

		Vector3& x1 = mesh.vertices[bc.i1];
		Vector3& x2 = mesh.vertices[bc.i2];
		Vector3& x3 = mesh.vertices[bc.i3];
		Vector3& x4 = mesh.vertices[bc.i4];

		Vector3 x12 = x2 - x1;
		Vector3 x13 = x3 - x1;
		Vector3 n1 = cross(x12, x13);
		float l1 = bc.l1;// n1.Length();
		Vector3 x14 = x4 - x1;
		Vector3 n2 = cross(x12, x14);
		float l2 = bc.l2; //n2.Length();

		float c1 = 1.f / (l1 * l2);
		float d = n1 * n2 * c1; // cosine of angle
		d = min(1.f, max(-1.f, d));
		ASSERT(!isnan(c1));
		Vector3 q3 = -c1 * cross(x12, n2);
		Vector3 q4 = -c1 * cross(x12, n1);
		Vector3 q2 = c1 * (cross(x13, n2) + cross(x14, n1));
		Vector3 q1 = -q2 - q3 - q4;

		float diag = q1.LengthSquared() +
			q2.LengthSquared() +
			q3.LengthSquared() +
			q4.LengthSquared();
		float d2 = d * d;
		float sine = sqrt(1 - d * d);
		float arccos = sine; //PI - acos(d); // TODO: sign!
		float lambda = -factor * sine * arccos / diag;
		x1 += lambda * q1;
		x2 += lambda * q2;
		x3 += lambda * q3;
		x4 += lambda * q4;
	}
}

void CollisionDemo::ProjectTriangles(Mesh& mesh, const std::vector<Triangle>& triangles)
{
	for (size_t k = 0; k < triangles.size(); k++)
	{
		const Triangle& tri = triangles[k];
		Vector3& p1 = mesh.vertices[tri.i1];
		Vector3& p2 = mesh.vertices[tri.i2];
		Vector3& p3 = mesh.vertices[tri.i3];
		Vector3 dx1 = p2 - p1;
		Vector3 dx2 = p3 - p1;

		Vector3 wu = tri.invDet * (tri.dv2 * dx1 - tri.dv1 * dx2);
		Vector3 wv = tri.invDet * (-tri.du2 * dx1 + tri.du1 * dx2);

		float err, diag, lambda;
		Vector3 q1, q2, q3;

		// warp
		q2 = (tri.invDet * tri.dv2) * wu;
		q3 = (-tri.invDet * tri.dv1) * wu;
		q1 = -q2 - q3;
		diag = (q1 * q1 + q2 * q2 + q3 * q3);
		err = 0.5f * (wu * wu - tri.lu2);
		lambda = -err / diag;
		p1 += lambda * q1;
		p2 += lambda * q2;
		p3 += lambda * q3;

		// weft
		q2 = (-tri.invDet * tri.du2) * wv;
		q3 = (tri.invDet * tri.du1) * wv;
		q1 = -q2 - q3;
		diag = (q1 * q1 + q2 * q2 + q3 * q3);
		err = 0.5f * (wv * wv - tri.lv2);
		lambda = -err / diag;
		p1 += lambda * q1;
		p2 += lambda * q2;
		p3 += lambda * q3;

		// shearing
		q2 = tri.dv2 * wv - tri.du2 * wu;
		q3 = -tri.dv1 * wv + tri.du1 * wu;
		q1 = -q2 - q3;

		diag = tri.invDet * (q1 * q1 + q2 * q2 + q3 * q3);
		err = wu * wv;
		lambda = -err / diag;
		p1 += lambda * q1;
		p2 += lambda * q2;
		p3 += lambda * q3;
	}
}

void CollisionDemo::ProjectArea(float factor, Mesh& mesh, const std::vector<Triangle>& triangles)
{
	for (int i = 0; i < triangles.size(); i++)
	{
		Vector3& x1 = mesh.vertices[triangles[i].i1];
		Vector3& x2 = mesh.vertices[triangles[i].i2];
		Vector3& x3 = mesh.vertices[triangles[i].i3];
		// compute area
		Vector3 n = cross(x2 - x1, x3 - x1);
		float area = 0.5f * n.Length();
		float err = area - triangles[i].area;
		// compute Jacobian
		n.Normalize();
		Vector3 j1 = cross(x3 - x2, n);
		Vector3 j2 = cross(x1 - x3, n);
		Vector3 j3 = cross(x2 - x1, n);
		float diag = j1.LengthSquared() + j2.LengthSquared() + j3.LengthSquared();
		// apply correction
		float lambda = factor * err / diag;
		x1 += lambda * j1;
		x2 += lambda * j2;
		x3 += lambda * j3;
	}
}


void CollisionDemo::OnCreate()
{
	graphics3D->camera.SetPosition(Vector3(0, 30, 70));
	if (mDemoType == DEMO_TRIANGLE)
		graphics3D->SetLightPos(Vector3(0, 30, 70));
	graphics->SetBgColor(0xff5c5c5c);
	
	Reset();
}

void CollisionDemo::Reset()
{
	mTransform1 = Matrix4::Identity();
	mTransform2 = Matrix4::Identity();

	if (mDemoType == DEMO_TWO_MESHES)
	{
		mMeshIntersector.ClearIntersection();
		mProjections1.clear();
		mProjections2.clear();

		if (mDtmType == DTM_BUNNY_AND_DRAGON)
		{
			LoadMeshFromObj("../Models/bunny.obj", mMesh[0]);
			LoadMeshFromObj("../Models/dragon_new.obj", mMesh[1]);
		}
		else if (mDtmType == DTM_TWO_SPHERES)
		{
			if (!LoadMeshFromObj("../Models/sphere_new.obj", mMesh[0]))
				return;
			mPos1.Set(1, -15, 0);
			mTransform1 = Matrix4::Translation(1, -15, 0) * Matrix4::Scale(10, 10, 10);

			if (!LoadMeshFromObj("../Models/sphere_new.obj", mMesh[1]))
				return;
			mTransform2 = Matrix4::Scale(10, 10, 10);
		}
		else if (mDtmType == DTM_PREVIOUS_MESHES)
		{
			LoadMeshFromObj("../Models/mesh1.obj", mMesh[0]);
			LoadMeshFromObj("../Models/mesh2.obj", mMesh[1]);
		}
		else if (mDtmType == DTM_MAYA_SPHERES)
		{
			LoadMeshFromObj("../Models/pSphere1.obj", mMesh[0]);
			LoadMeshFromObj("../Models/pSphere2.obj", mMesh[1]);
			mTransform1 = Matrix4::Scale(10, 10, 10);
			mTransform2 = Matrix4::Scale(10, 10, 10);
		}
		else if (mDtmType == DTM_CUBE_SPHERE)
		{
			LoadMeshFromObj("../Models/sfera.obj", mMesh[0]);
			LoadMeshFromObj("../Models/cub.obj", mMesh[1]);
			mTransform1 = Matrix4::Scale(10, 10, 10);
			mTransform2 = Matrix4::Scale(10, 10, 10);
		}

		InitMesh(mMesh[0], mTriangles1, mEdgeLens1, mBends1);
		InitMesh(mMesh[1], mTriangles2, mEdgeLens2, mBends2);
		mMeshIntersector.InitMeshes(&mMesh[0], &mMesh[1]);
		mMeshIntersector.SetTransforms(mTransform1, mTransform2);
	}
	else if (mDemoType == DEMO_MESH)
	{
		bool ret = LoadMeshFromObj("../Models/dragon_new.obj", mMesh[0]);
		if (!ret)
			return;

		Printf("The mesh has %d vertices and %d triangles.\n", mMesh[0].vertices.size(), mMesh[0].GetNumTriangles());

		mMesh[0].ComputeNormals();
		mMesh[0].ConstructEdges();
	}
}

void CollisionDemo::OnDraw()
{
	if (mDrawIndices)
	{
		//auto vertexInfos1 = mMeshIntersector.GetVertexInfos1();
		//auto vertexInfos2 = mMeshIntersector.GetVertexInfos2();
		if (mFilterIndex < 0)
		{
			if (mDrawMesh[0] != DrawMode::INVISIBLE)
			{
				for (int i = 0; i < mMesh[0].vertices.size(); i++)
				{
					DrawIndex(i, mMesh[0]);
				}
			}

			if (mDrawMesh[0] != DrawMode::INVISIBLE)
			{
				for (int i = 0; i < mMesh[1].vertices.size(); i++)
				{
					DrawIndex(i, mMesh[1]);
				}
			}

			auto intersections = mMeshIntersector.GetIntersections();
			for (int i = 0; i < intersections.size(); i++)
			{
				float sx, sy;
				graphics3D->GetScreenCoords(intersections[i].pt, sx, sy);
				graphics->DrawFormattedString(sx, sy, "%d", i);
			}
		}
		else
		{
			if (mFilterIndex < mMesh[0].vertices.size())
				DrawIndex(mFilterIndex, mMesh[0], false);
			if (mFilterIndex < mMesh[1].vertices.size())
				DrawIndex(mFilterIndex, mMesh[1], false);
		}
	}

	if (mDrawTriIndices)
	{
		if (mFilterIndex < 0)
		{
			for (size_t i = 0; i < mMesh[0].GetNumTriangles() && mDrawMesh[0]; i++)
			{
				DrawTriIndex(i, mMesh[0]);
			}

			for (size_t i = 0; i < mMesh[1].GetNumTriangles() && mDrawMesh[1]; i++)
			{
				DrawTriIndex(i, mMesh[1]);
			}
		}
		else
		{
			if (mFilterIndex < mMesh[0].GetNumTriangles())
				DrawTriIndex(mFilterIndex, mMesh[0], false);
			if (mFilterIndex < mMesh[1].GetNumTriangles())
				DrawTriIndex(mFilterIndex, mMesh[1], false);
		}
	}

	if (mDrawEdgeIndices)
	{
		if (mFilterIndex < 0)
		{
			for (size_t i = 0; i < mMesh[0].edges.size() && mDrawMesh[0]; i++)
			{
				DrawEdgeIndex(i, mMesh[0]);
			}

			for (size_t i = 0; i < mMesh[1].edges.size() && mDrawMesh[1]; i++)
			{
				DrawEdgeIndex(i, mMesh[1]);
			}
		}
		else
		{
			if (mFilterIndex < mMesh[0].edges.size())
				DrawEdgeIndex(mFilterIndex, mMesh[0], false);
			if (mFilterIndex < mMesh[1].edges.size())
				DrawEdgeIndex(mFilterIndex, mMesh[1], false);
		}
	}
}

void CollisionDemo::DrawUI()
{
#ifdef USE_IMGUI
	if (ImGui::CollapsingHeader("Rendering", ImGuiTreeNodeFlags_DefaultOpen))
	{
		Vector3 light = graphics3D->GetLightPos();
		if (ImGui::InputFloat3("Light", light.v))
		{
			graphics3D->SetLightPos(light);
		}
	}
	
	if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen))
	{
		ImGui::SliderFloat("Speed", &mCameraSpeed, 0.01f, 0.1f);
	}
	
	if (ImGui::CollapsingHeader("Drawing", ImGuiTreeNodeFlags_DefaultOpen))
	{
		if (ImGui::Button("Reset"))
		{
			Reset();
		}
		const char* drawModes[] = { "Hide", "Shaded", "Shaded with edges", "Wireframe" };
		const char* meshes[] = { "Mesh 1", "Mesh 2" };
		ImGui::Checkbox("Draw vertex indices", &mDrawIndices);
		ImGui::Checkbox("Draw triangle indices", &mDrawTriIndices);
		ImGui::Checkbox("Draw edge indices", &mDrawEdgeIndices);
		ImGui::InputInt("Filter index", &mFilterIndex);
		ImGui::Combo("Current mesh", &mCurrMesh, meshes, 2);
		ImGui::Checkbox("Draw normals", &mDrawNormals[mCurrMesh]);
		ImGui::Combo("Draw Mode", &mDrawMesh[mCurrMesh], drawModes, 4);
		if (mDemoType == DEMO_TWO_MESHES)
		{
			//ImGui::Combo("Mesh 2", &mDrawMesh[1], drawModes, 4);
			ImGui::Checkbox("Draw intersection points", &mDrawIntPoints);
			ImGui::Checkbox("Draw intersection contour", &mDrawContour);
			ImGui::Checkbox("Draw edge", &mDrawEdge);
			ImGui::Checkbox("Draw triangle", &mDrawTriangle);
			ImGui::Checkbox("Draw info 1", &mDrawInfo1);
			ImGui::Checkbox("Draw info 2", &mDrawInfo2);
			ImGui::Checkbox("Draw intersection mesh 1", &mDrawIntMesh1);
			ImGui::Checkbox("Draw intersection mesh 2", &mDrawIntMesh2);
			if (ImGui::Button("Save mesh 1"))
			{
				SaveMeshToObj("../Models/mesh1.obj", mMesh[0]);
			}
			if (ImGui::Button("Save mesh 2"))
			{
				SaveMeshToObj("../Models/mesh2.obj", mMesh[1]);
			}
		}
		ImGui::SliderFloat("Point size", &mPointSize, 0.005f, 0.08f);
	}

	if (mDemoType == DEMO_TWO_MESHES && ImGui::CollapsingHeader("Algorithm", ImGuiTreeNodeFlags_DefaultOpen))
	{
		const char* demos[] = { "Bunny and dragon", "Two sphere", "Previous meshes", "Maya spheres", "Cube sphere" };
		ImGui::Combo("Demo", &mDtmType, demos, 5);
		if (ImGui::Button("Intersect slow"))
		{
			mMeshIntersector.IntersectSlow();
		}
		if (ImGui::Button("Intersect fast"))
		{
			mMeshIntersector.IntersectFast();
			mSelection[0] = mMeshIntersector.GetSelection1();
			mSelection[1] = mMeshIntersector.GetSelection2();
		}

		if (ImGui::Button("Raycast 1"))
		{
			ProjectVerticesOnMesh(mMesh[0], mTransform1, mMeshIntersector.GetSelection1(), mMeshIntersector.GetIntersectingMesh2(), mProjections1, mStep);
			mMeshIntersector.UpdateMesh1();
		}
		ImGui::SameLine();
		if (ImGui::Button("Raycast 2"))
		{
			ProjectVerticesOnMesh(mMesh[1], mTransform2, mMeshIntersector.GetSelection2(), mMeshIntersector.GetIntersectingMesh1(), mProjections2, mStep);
			mMeshIntersector.UpdateMesh2();
		}

		if (ImGui::Button("Project 1"))
		{
			mDir1 = ComputeDirection(mMesh[0], mMeshIntersector.GetVertexInfos1());
			Project(mStep, mDir1, mMesh[0], mMeshIntersector.GetSelection1());
			//Smooth(mMesh1, mTriangles1, mBends1);
			mMeshIntersector.UpdateMesh1();
			mMeshIntersector.IntersectFast();
		}
		ImGui::SameLine();
		if (ImGui::Button("Project 2"))
		{
			mDir2 = ComputeDirection(mMesh[1], mMeshIntersector.GetVertexInfos2());
			Project(mStep, mDir2, mMesh[1], mMeshIntersector.GetSelection2());
			//Smooth(mMesh2, mTriangles2, mBends2);
			mMeshIntersector.UpdateMesh2();
			mMeshIntersector.IntersectFast();
		}
		ImGui::InputFloat("Gradient step", &mStep);
		if (ImGui::Button("Project lengths"))
		{
			ProjectLengths(1.0f, mMesh[0], mEdgeLens1);
			mMesh[0].ComputeNormals();
		}
		if (ImGui::Button("Project bending"))
		{
			ProjectBending(1.0f, mMesh[0], mBends1);
			mMesh[0].ComputeNormals();
		}
		if (ImGui::Button("Project area"))
		{
			ProjectArea(1.0f, mMesh[0], mTriangles1);
			mMesh[0].ComputeNormals();
		}
		if (ImGui::Button("Smooth 1"))
		{
			Smooth(mMesh[0], mTriangles1, mBends1);
		}
		ImGui::SameLine();
		if (ImGui::Button("Smooth 2"))
		{
			Smooth(mMesh[1], mTriangles2, mBends2);
		}
	}

	if (ImGui::CollapsingHeader("Smooth", ImGuiTreeNodeFlags_DefaultOpen))
	{
		ImGui::InputInt("Iterations", &mSmoothIters);
		ImGui::InputFloat("Step", &mSmoothStep);
		ImGui::Checkbox("Taubin smoothing", &mSmoothTaubin);
		ImGui::Checkbox("Smooth normals", &mSmoothNormals);
		if (ImGui::Button("Laplacian smoothing"))
		{
			for (int k = 0; k < mSmoothIters; k++)
			{
				SmoothVertices(mMesh[mCurrMesh], &mSelection[mCurrMesh], mSmoothStep, mSmoothNormals);
				if (mSmoothTaubin)
					SmoothVertices(mMesh[mCurrMesh], nullptr, -mSmoothStep - 0.1f, mSmoothNormals);
			}
			if (!mSmoothNormals)
				mMesh[mCurrMesh].ComputeNormals();
			if (mCurrMesh == 0)
				mMeshIntersector.UpdateMesh1();
			else
				mMeshIntersector.UpdateMesh2();
		}
	}
#endif
}

void DrawMesh(int drawMode, Graphics3D* graphics3D, const Mesh& mesh, const Matrix4& transform, bool drawNormals)
{
	if (drawMode != CollisionDemo::INVISIBLE)
	{
		if (drawMode == CollisionDemo::WIREFRAME)
		{
			graphics3D->SetRenderMode(RenderMode::RM_WIREFRAME);
			graphics3D->SetColor(0.0f, 0.0f, 0.0f);
		}
		else if (drawMode == CollisionDemo::SHADED_WITH_EDGES)
		{
			graphics3D->SetRenderMode(RenderMode::RM_WIREFRAME_ON_SHADED);
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
		}
		else if (drawMode == CollisionDemo::SHADED)
		{
			graphics3D->SetRenderMode(RenderMode::RM_SHADED);
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
		}

		graphics3D->DrawMesh(mesh.vertices, mesh.normals, mesh.indices, &transform);

		graphics3D->SetRenderMode(RenderMode::RM_SHADED);

		// debug draw normals
		for (size_t i = 0; i < mesh.normals.size() && drawNormals; i++)
		{
			Vector3 pt = mesh.vertices[i];
			pt = transform.Transform(pt);
			Vector3 n = mesh.normals[i];
			n = transform.TransformRay(n);
			graphics3D->DrawLine(pt, pt + n * 0.1f);
			// TODO: create line list buffer
		}
	}
}

void CollisionDemo::DrawVertexInfos(int meshIdx, const std::vector<MeshIntersector::ProxyInfo>& vertexInfos)
{
	for (int i = 0; i < vertexInfos.size(); i++)
	{
		if (vertexInfos[i].side != MeshIntersector::SideType::INVALID)
		{
			if (vertexInfos[i].side == MeshIntersector::SideType::EXTERIOR)
			{
				graphics3D->SetColor(1.0f, 0.0f, 0.0f);
			}
			else if (vertexInfos[i].side == MeshIntersector::SideType::INTERIOR)
			{
				graphics3D->SetColor(0.0f, 1.0f, 0.0f);
			}
			Vector3 pt = mTransform1.Transform(mMesh[meshIdx].vertices[i]);
			graphics3D->DrawSphere(pt, mPointSize);
		}
	}
}

void CollisionDemo::OnDraw3D()
{
	graphics3D->SetLightPos(10.f * graphics3D->camera.GetPosition());

	DrawUI();

	// camera control
	float dz = 0;
	float dx = 0;
	if (isKeyDown('W'))
		dz = mCameraSpeed;
	else if (isKeyDown('S'))
		dz = -mCameraSpeed;
	if (isKeyDown('A'))
		dx = -mCameraSpeed;
	else if (isKeyDown('D'))
		dx = mCameraSpeed;
	if (dx || dz)
		graphics3D->camera.Translate(dz, dx);

	if (mDemoType == DEMO_MESH)
	{
		graphics3D->SetColor(1.0f, 1.0f, 1.0f);
		DrawMesh(mDrawMesh[0], graphics3D, mMesh[0], mTransform1, mDrawNormals[0]);
	}
	if (mDemoType == DEMO_TWO_MESHES)
	{
		if (isKeyDown('O'))
		{
			mPos1.x -= 0.1f;
			mTransform1 = Matrix4::Translation(mPos1.x, mPos1.y, mPos1.z) * Matrix4::Scale(10, 10, 10);
			mMeshIntersector.SetTransforms(mTransform1, mTransform2);
		}
		if (isKeyDown('P'))
		{
			mPos1.x += 0.1f;
			mTransform1 = Matrix4::Translation(mPos1.x, mPos1.y, mPos1.z) * Matrix4::Scale(10, 10, 10);
			mMeshIntersector.SetTransforms(mTransform1, mTransform2);
		}

		DrawMesh(mDrawMesh[0], graphics3D, mMesh[0], mTransform1, mDrawNormals[0]);
		DrawMesh(mDrawMesh[1], graphics3D, mMesh[1], mTransform2, mDrawNormals[1]);

		// draw intersection points
		if (mDrawContour)
		{
			graphics3D->SetFlags(MONOCHROME);
			graphics3D->DrawLines(mMeshIntersector.GetPolyLine());
			graphics3D->SetFlags(0);
		}

		if (mDrawIntPoints)
		{
			for (int i = 0; i < mProjections2.size(); i++)
			{
				graphics3D->SetColor(0, 0, 1);
				graphics3D->DrawSphere(mProjections2[i], mPointSize);

				// TODO: line from original mesh point
				//graphics3D->SetColor(1, 1, 1);
				//Vector3 pt = mMesh2.vertices[mMeshIntersector.GetSelection2()[i]];
				//pt = mTransform2.Transform(pt);
				//graphics3D->DrawLine(mProjections2[i], pt);
			}
			for (int i = 0; i < mProjections1.size(); i++)
			{
				graphics3D->DrawSphere(mProjections1[i], mPointSize);
			}
			auto intersections = mMeshIntersector.GetIntersections();
			for (int i = 0; i < intersections.size(); i++)
			{
				if (intersections[i].type & MeshIntersector::END_PT)
					graphics3D->SetColor(1.f, 0.64f, 1.0f);
				else if (intersections[i].type & MeshIntersector::EDGE_PT)
					graphics3D->SetColor(0.1f, 0.54f, 0.1f);
				else
					graphics3D->SetColor(0.0f, 1.0f, 1.0f);
				graphics3D->DrawSphere(intersections[i].pt, mPointSize);

				graphics3D->SetColor(0.0f, 1.0f, 1.0f);
				if (mDrawEdge)
				{
					uint32 ei = intersections[i].edge;
					const Mesh::Edge& edge = intersections[i].meshEdge->edges[ei];
					const Vector3& p = intersections[i].meshEdge->vertices[edge.i1];
					const Vector3& q = intersections[i].meshEdge->vertices[edge.i2];
					graphics3D->DrawLine(p, q);
				}

				// draw the other edge
				//if (mIntersections[i].type & EDGE_PT)
				//{
				//	uint32 ej = mIntersections[i].edgeOnTri;
				//	const Mesh::Edge& edge = mIntersections[i].mesh2->edges[ej];
				//	const Vector3& p = mIntersections[i].mesh2->vertices[edge.i1];
				//	const Vector3& q = mIntersections[i].mesh2->vertices[edge.i2];
				//	graphics3D->DrawLine(p, q);
				//}

				if (mDrawTriangle)
				{
					uint32 ti = intersections[i].tri;
					int i0 = intersections[i].meshTri->indices[ti * 3];
					int i1 = intersections[i].meshTri->indices[ti * 3 + 1];
					int i2 = intersections[i].meshTri->indices[ti * 3 + 2];
					const Vector3& x0 = intersections[i].meshTri->vertices[i0];
					const Vector3& x1 = intersections[i].meshTri->vertices[i1];
					const Vector3& x2 = intersections[i].meshTri->vertices[i2];
					graphics3D->SetColor(0.9f, 0.5f, 0.1f);
					graphics3D->DrawTriangle(x0, x1, x2, true);
					graphics3D->DrawTriangle(x0, x2, x1, true);
					graphics3D->SetColor(0.0f, 0.0f, 0.0f);
					graphics3D->DrawTriangle(x0, x1, x2, false);
					graphics3D->DrawTriangle(x0, x2, x1, false);
				}
			}

			//for (int i = 0; i < mIntTriangles1.size(); i++)
			//{
			//	int tri = mIntTriangles1[i];
			//	int i0 = mMesh1.indices[tri * 3];
			//	int i1 = mMesh1.indices[tri * 3 + 1];
			//	int i2 = mMesh1.indices[tri * 3 + 2];
			//	const Vector3& x0 = mMesh1.vertices[i0];
			//	const Vector3& x1 = mMesh1.vertices[i1];
			//	const Vector3& x2 = mMesh1.vertices[i2];
			//	graphics3D->DrawTriangle(x0, x1, x2, true);
			//	graphics3D->DrawTriangle(x0, x2, x1, true);
			//}

			//for (int i = 0; i < mIntTriangles2.size(); i++)
			//{
			//	int tri = mIntTriangles2[i];
			//	int i0 = mMesh2.indices[tri * 3];
			//	int i1 = mMesh2.indices[tri * 3 + 1];
			//	int i2 = mMesh2.indices[tri * 3 + 2];
			//	const Vector3& x0 = mMesh2.vertices[i0];
			//	const Vector3& x1 = mMesh2.vertices[i1];
			//	const Vector3& x2 = mMesh2.vertices[i2];
			//	graphics3D->DrawTriangle(x0, x1, x2, true);
			//	graphics3D->DrawTriangle(x0, x2, x1, true);
			//}
		}

		// draw intersecting meshes
		graphics3D->SetRenderMode(RM_WIREFRAME_ON_SHADED);
		if (mDrawIntMesh1)
		{
			const Mesh& intMesh1 = mMeshIntersector.GetIntersectingMesh1();
			graphics3D->DrawMesh(intMesh1.vertices, intMesh1.normals, intMesh1.indices);
		}
		graphics3D->SetColor(1.0f, 1.0f, 0.0f);
		if (mDrawIntMesh2)
		{
			const Mesh& intMesh2 = mMeshIntersector.GetIntersectingMesh2();
			graphics3D->DrawMesh(intMesh2.vertices, intMesh2.normals, intMesh2.indices);
		}
		graphics3D->SetRenderMode(RM_SHADED);

		// draw vertex info
		if (mDrawInfo1)
			DrawVertexInfos(0, mMeshIntersector.GetVertexInfos1());
		if (mDrawInfo2)
			DrawVertexInfos(1, mMeshIntersector.GetVertexInfos2());
	}
	else if (mDemoType == DEMO_TRIANGLE)
	{
		graphics3D->SetColor(1.0f, 0.f, 0.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(10, 0, 0));
		graphics3D->SetColor(0.0f, 1.f, 0.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(0, 10, 0));
		graphics3D->SetColor(0.0f, 0.f, 1.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(0, 0, 10));

		Vector3 a(0, 0, 0);
		Vector3 b(10, -20, 30);
		Vector3 c(30, -10, 20);

		//Vector3 a(8.88987005f, 6.56872988f, 7.84044981f);
		//Vector3 b(8.28193009f, 6.21143997f, 8.28193009f);
		//Vector3 c(8.28193009f, 6.90159976f, 8.28193009f);

		graphics3D->SetColor(0.9f, 0.5f, 0.1f);
		graphics3D->DrawTriangle(a, b, c, true);

		graphics3D->SetColor(0.f, 0.f, 0.0f);
		graphics3D->DrawTriangle(a, b, c, false);

		Vector3 p(5, -10, 10);
		Vector3 q(20, 10, 30);

		//Vector3 p(7.21156001f, 6.56872988f, 8.66370022f);
		//Vector3 q(8.88987005f, 6.56872988f, 7.84044981f);
		
		graphics3D->DrawLine(p, q);

		BarycentricCoords coords;
		float t;
		Vector3 pt;
		if (IntersectSegmentTriangle(p, q, a, b, c, coords, t, pt))
		{
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
			graphics3D->DrawSphere(pt, mPointSize);
		}

		{
			Vector3 pt(5, -5, 5);
			graphics3D->DrawSphere(pt, 0.1f);
			Vector3 closestPt = ClosestPtPointTriangle(pt, a, b, c, coords);
			graphics3D->DrawSphere(closestPt, 0.1f);
			graphics3D->DrawLine(pt, closestPt);
		}

		{
			Vector3 pt(5, 15, 25);
			graphics3D->DrawSphere(pt, 0.1f);
			Vector3 closestPt = ClosestPtPointTriangle(pt, a, b, c, coords);
			graphics3D->DrawSphere(closestPt, 0.1f);
			graphics3D->DrawLine(pt, closestPt);
		}

		{
			Vector3 pt(30, 25, 35);
			graphics3D->DrawSphere(pt, 0.1f);
			Vector3 closestPt = ClosestPtPointTriangle(pt, a, b, c, coords);
			graphics3D->DrawSphere(closestPt, 0.1f);
			graphics3D->DrawLine(pt, closestPt);
		}
	}
	else if (mDemoType == DEMO_TWO_TRIANGLES)
	{
		graphics3D->SetColor(1.0f, 0.f, 0.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(10, 0, 0));
		graphics3D->SetColor(0.0f, 1.f, 0.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(0, 10, 0));
		graphics3D->SetColor(0.0f, 0.f, 1.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(0, 0, 10));

		Vector3 a(0, 0, 0);
		Vector3 b(10, -20, 30);
		Vector3 c(30, -10, 20);

		graphics3D->SetColor(0.9f, 0.5f, 0.1f);
		graphics3D->DrawTriangle(a, b, c, true);

		graphics3D->SetColor(0.f, 0.f, 0.0f);
		graphics3D->DrawTriangle(a, b, c, false);

		Vector3 d(30, 5, -1);
		Vector3 e(0, 20, 0);
		Vector3 f(5, -20, 20);

		graphics3D->SetColor(0.9f, 0.0f, 0.3f);
		graphics3D->DrawTriangle(d, e, f, true);

		graphics3D->SetColor(0.f, 0.f, 0.0f);
		graphics3D->DrawTriangle(d, e, f, false);

		BarycentricCoords coords;
		float t;
		std::vector<Vector3> pts;
		Vector3 pt;
		// check edges of triangle 2 against triangle 1
		if (IntersectSegmentTriangle(d, e, a, b, c, coords, t, pt))
		{
			pts.push_back(pt);
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
			graphics3D->DrawSphere(pt, 0.1f);
		}
		if (IntersectSegmentTriangle(e, f, a, b, c, coords, t, pt))
		{
			pts.push_back(pt);
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
			graphics3D->DrawSphere(pt, 0.1f);
		}
		if (IntersectSegmentTriangle(f, d, a, b, c, coords, t, pt))
		{
			pts.push_back(pt);
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
			graphics3D->DrawSphere(pt, 0.1f);
		}
		// check edges of triangle 1 against triangle 2
		if (IntersectSegmentTriangle(a, b, d, e, f, coords, t, pt))
		{
			pts.push_back(pt);
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
			graphics3D->DrawSphere(pt, 0.1f);
		}
		if (IntersectSegmentTriangle(b, c, d, e, f, coords, t, pt))
		{
			pts.push_back(pt);
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
			graphics3D->DrawSphere(pt, 0.1f);
		}
		if (IntersectSegmentTriangle(c, a, d, e, f, coords, t, pt))
		{
			Vector3 pt2 = (1 - t) * c + t * a;
			pts.push_back(pt2);
			graphics3D->SetColor(1.0f, 1.0f, 1.0f);
			graphics3D->DrawSphere(pt2, 0.1f);
		}
		
		if (pts.size() >= 2)
		{
			graphics3D->SetColor(0, 1, 0);
			graphics3D->DrawLine(pts[0], pts[1]);
		}
	}
	else if (mDemoType == DemoType::DEMO_EDGE_TRIANGLES)
	{
		graphics3D->SetColor(1.0f, 0.f, 0.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(10, 0, 0));
		graphics3D->SetColor(0.0f, 1.f, 0.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(0, 10, 0));
		graphics3D->SetColor(0.0f, 0.f, 1.f);
		graphics3D->DrawLine(Vector3(0, 0, 0), Vector3(0, 0, 10));

		float scale = 20.f;
		Vector3 p(0, 0.453990996f, -0.515398979f);
		Vector3 q(0, 0.495719999f, -0.330271006f);
		Vector3 a(0, 0.470200986f, -0.277009010f);
		Vector3 b(0.0483409986f, 0.482511997f, -0.425787002f);
		Vector3 c(0, 0.482511997f, -0.433443993f);
		Vector3 d(-0.0483409986f, 0.482511997f, -0.425787002f);
		p *= scale;
		q *= scale;
		a *= scale;
		b *= scale;
		c *= scale;
		d *= scale;

		graphics3D->SetColor(1.f, 1.f, 1.f);
		graphics3D->DrawLine(p, q);
		graphics3D->DrawTriangle(c, b, a, false);
		graphics3D->DrawTriangle(d, c, a, false);
		graphics3D->DrawTriangle(b, c, a, false);
		graphics3D->DrawTriangle(c, d, a, false);
		graphics3D->SetColor(0.9f, 0.0f, 0.3f);
		graphics3D->DrawTriangle(c, b, a, true);
		graphics3D->DrawTriangle(d, c, a, true);
		graphics3D->DrawTriangle(b, c, a, true);
		graphics3D->DrawTriangle(c, d, a, true);

		BarycentricCoords coords;
		float t;
		Vector3 pt;
		if (IntersectSegmentTriangle(p, q, c, b, a, coords, t, pt))
		{
			graphics3D->SetColor(0.0f, 0.f, 1.f);
			graphics3D->DrawSphere(pt, mPointSize);
		}
		if (IntersectSegmentTriangle(p, q, d, c, a, coords, t, pt))
		{
			graphics3D->SetColor(0.0f, 0.f, 1.f);
			graphics3D->DrawSphere(pt, mPointSize);
		}
	}
}
// application entry point
int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	RUN_ENGINE(CollisionDemo, hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}
