#ifndef RIG_H
#define RIG_H

#include <Graphics2D/Graphics2D.h>
#include <Engine/Vector2.h>
#include <Streams/Stream.h>

#include <vector>
using namespace std;

// TODO: limits
class Bone
{
public:
	float length;

	float angle;

	Vector2 b, e;

	float worldAngle;

	Bone():	length(0),
		angle(0),
		worldAngle(0)
	{
	}
};

class Chain
{
public:

	vector<Bone> bones;

	// TODO: should be float?
	int x, y;

	void AddBone(float length, float angle);

	void Draw(Graphics2D& g);

	void CcdIk(const Vector2& target, int effectorIndex = -1, int numIterations = 10 );

	int GetBoneAt(int x, int y);
};

class BoneTree: public Bone
{
	public:

		BoneTree* parent;

		vector<BoneTree> children;

		BoneTree(): parent(NULL)
		{
		}

		BoneTree& AddChild(float length, float angle);

		void SetAngle(float newAngle);

		BoneTree* GetBoneAt(int x, int y, float r = 3.5f);

		void Draw(Graphics2D& g, float rootAngle);
};

#define TYPE_BOX 1
#define TYPE_RIG 2

class Object
{
	public:

		uint8 type; // for serialization primarily and RTTI

		uint8 dynamic; // for memory management

		Object(uint8 aType): type(aType)
		{
		}
};

class Rig: public Object
{
	public:

		BoneTree root;

		int x, y; // TODO: should be float?

	public:

		Rig(): Object(TYPE_RIG)
		{
		}

		void SetRoot(int x, int y);		

		void CcdIk(const Vector2& target, BoneTree* bone, int numIterations = 10 );

		void Draw(Graphics2D& g);

		void Save(Stream& stream);

		void Load(Stream& stream);
};

#endif // RIG_H