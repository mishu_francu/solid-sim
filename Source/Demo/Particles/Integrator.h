#ifndef INTEGRATOR_H
#define INTEGRATOR_H

namespace Integrators
{
	enum IntegratorType
	{
		EULER,
		MIDPOINT, // same as RK2
		TRAPEZOID, // same as Modified Euler?
		RK4,
		// TODO: implicit Euler and Midpoint
		IMPLICIT_EULER,
		SE1,
		SE2,
		LEAPFROG,
		VERLET
		// TODO: Hairer velocity forms of Stormer-Verlet
	};
}

template <int Method, typename Vector, typename Force, typename Real>
struct Integrator
{
};

template <typename Vector, typename Force, typename Real>
struct Integrator<Integrators::EULER, Vector, Force, Real>
{
	void Initialize(const Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h) { }
	void operator ()(Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		Vector f = force->Evaluate(pos, vel);
		pos += h * vel;
		vel += h * f;
	}

	void Step(Vector& pos, Vector& vel, const Force* derivative, Real h)
	{
		Vector f = derivative->EvaluateForce(pos, vel);
		pos += h * derivative->EvaluateVelocity(pos, vel);
		vel += h * f;
	}
};

template <typename Vector, typename Force, typename Real>
struct Integrator<Integrators::SE1, Vector, Force, Real>
{
	void Initialize(const Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h) { }
	void operator ()(Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		vel += h * force->Evaluate(pos, vel);
		posPrev = pos;
		pos += h * vel;
	}

	void StepVel(const Vector& pos, Vector& vel, const Force* force, Real h)
	{
		vel += h * force->Evaluate(pos, vel);
	}

	void StepPos(Vector& pos, const Vector& vel, Real h)
	{
		pos += h * vel;
	}

	void Step(Vector& pos, Vector& vel, const Force* derivative, Real h)
	{
		Vector f = derivative->EvaluateForce(pos, vel);
		vel += h * f;
		pos += h * derivative->EvaluateVelocity(pos, vel);
	}
};

template <typename Vector, typename Force, typename Real>
struct Integrator<Integrators::LEAPFROG, Vector, Force, Real>
{
	void Initialize(const Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		vel += Real(0.5) * h * force->Evaluate(pos, vel);
	}

	void operator ()(Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		Vector f = force->Evaluate(pos, vel);
		vel += h * f;
		pos += h * vel;
	}

	void Step(Vector& pos, Vector& vel, const Force* derivative, Real h)
	{
		Vector f = derivative->EvaluateForce(pos, vel);
		vel += h * f;
		pos += h * derivative->EvaluateVelocity(pos, vel);
	}
};

template <typename Vector, typename Force, typename Real>
struct Integrator<Integrators::SE2, Vector, Force, Real>
{
	void Initialize(const Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h) { }
	void operator ()(Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		pos += h * vel;
		vel += h * force->Evaluate(pos, vel);
	}

	void Step(Vector& pos, Vector& vel, const Force* derivative, Real h)
	{
		pos += h * derivative->EvaluateVelocity(pos, vel);
		vel += h * derivative->EvaluateForce(pos, vel);
	}
};

template <typename Vector, typename Force, typename Real>
struct Integrator<Integrators::VERLET, Vector, Force, Real>
{
	void Initialize(const Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h) 
	{ 
		posPrev = pos - vel * h;
	}

	void operator ()(Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		Vector posNew = Real(2) * pos - posPrev + h * h * force->Evaluate(pos, vel);
		vel = Real(0.5 / h) * (posNew - posPrev);
		posPrev = pos;
		pos = posNew;
	}
};

template <typename Vector, typename Force, typename Real>
struct Integrator<Integrators::MIDPOINT, Vector, Force, Real>
{
	void Initialize(const Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h) { }
	void operator ()(Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		const Real hh(0.5 * h);
		Vector velMid = vel + hh * force->Evaluate(pos, vel);
		Vector posMid = pos + hh * vel;
		vel += h * force->Evaluate(posMid, velMid);
		pos += h * velMid;
	}

	void Step(Vector& pos, Vector& vel, const Force* derivative, Real h)
	{
		const Real hh(0.5 * h);
		Vector velMid = vel + hh * derivative->EvaluateForce(pos, vel);
		Vector posMid = pos + hh * derivative->EvaluateVelocity(pos, vel);
		vel += h * derivative->EvaluateForce(posMid, velMid);
		pos += h * derivative->EvaluateVelocity(posMid, velMid);
	}
};

template <typename Vector, typename Force, typename Real>
struct Integrator<Integrators::TRAPEZOID, Vector, Force, Real>
{
	void Initialize(const Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h) { }
	void operator ()(Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		Vector vel1 = vel + h * force->Evaluate(pos, vel);
		Vector pos1 = pos + h * vel;
		vel = Real(0.5) * (vel + vel1 + h * force->Evaluate(pos1, vel1));
		pos = Real(0.5) * (pos + pos1 + h * vel1);
	}

	// FIXME
	void Step(Vector& pos, Vector& vel, const Force* derivative, Real h)
	{
		Vector vel1 = vel + h * derivative->EvaluateForce(pos, vel);
		Vector pos1 = pos + h * derivative->EvaluateVelocity(pos, vel);
		vel += Real(0.5) * (vel + vel1 + h * derivative->EvaluateForce(pos1, vel1));
		pos += Real(0.5) * (pos + pos1 + h * derivative->EvaluateVelocity(pos1, vel1));
	}
};

template <typename Vector, typename Force, typename Real>
struct Integrator<Integrators::RK4, Vector, Force, Real>
{
	void Initialize(const Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h) { }
	void operator ()(Vector& pos, Vector& posPrev, Vector& vel, const Force* force, Real h)
	{
		Real hh(0.5 * h);
		Vector p2 = pos + hh * vel;
		Vector a1 = force->Evaluate(pos, vel);
		Vector v2 = vel + hh * a1;
		Vector a2 = force->Evaluate(p2, v2);
		Vector p3 = p2 + hh * hh * a1;
		Vector v3 = vel + hh * a2;
		Vector a3 = force->Evaluate(p3, v3);
		Vector pe = pos + h * vel;
		Vector p4 = pe + hh * h * a2;
		Vector v4 = vel + h * a3;
		Vector a4 = force->Evaluate(p4, v4);

		Real sixth = Real(1) / Real(6);
		pos = pe + sixth * h * h * (a1 + a2 + a3);
		vel += sixth * h * (a1 + Real(2) * a2 + Real(2) * a3 + a4);
	}

	// FIXME
	void Step(Vector& pos, Vector& vel, const Force* derivative, Real h)
	{
		Real hh(0.5 * h);
		Vector v1 = derivative->EvaluateVelocity(pos, vel);
		Vector p2 = pos + hh * v1;
		Vector a1 = derivative->EvaluateForce(pos, vel);
		Vector v2 = vel + hh * a1;
		Vector a2 = derivative->EvaluateForce(p2, v2);
		Vector p3 = p2 + hh * hh * a1;
		Vector v3 = vel + hh * a2;
		Vector a3 = derivative->EvaluateForce(p3, v3);
		Vector pe = pos + h * v1;
		Vector p4 = pe + hh * h * a2;
		Vector v4 = vel + h * a3;
		Vector a4 = derivative->EvaluateForce(p4, v4);

		Real sixth = Real(1) / Real(6);
		pos = pe + sixth * h * h * (a1 + a2 + a3);
		vel += sixth * h * (a1 + Real(2) * a2 + Real(2) * a3 + a4);
	}
};

#endif // INTEGRATOR_H
