#ifndef IMP_JAC_H
#define IMP_JAC_H

#include <cuda_runtime.h>
#include <vector>
#include <Math/Vector4.h>
#include "Constraint.h"
#include "Particle.h"

template <class VectorType, class ParticleType>
class ProjectorCUDA
{
protected:
	int numIterations;
	float4* positions;
	float4* disp;
	Vector4* posPtr;
	Edge* edges;
	float* invMass;
	Incidence* incEdges;
	size_t nParticles, nLinks;

public:
	ProjectorCUDA() 
		: numIterations(20)
		, positions(NULL)
		, posPtr(NULL)
	{
	}

	void ReadBuffers(std::vector<ParticleType>& particles);
	void SetNumIterations(int val) { numIterations = val; }
	void Init() { } // TODO: anything in here?
	void PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links);
	void CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links);
	void ProjectPositions();
};

template <class VectorType, class ParticleType>
inline void ProjectorCUDA<VectorType, ParticleType>::ReadBuffers(std::vector<ParticleType>& particles)
{
	// read back positions
	cudaMemcpy(posPtr, positions, particles.size() * sizeof(Vector4), cudaMemcpyDeviceToHost);
	for (size_t i = 0; i < particles.size(); i++)
	{
		particles[i].pos = posPtr[i];
	}
}

#endif
