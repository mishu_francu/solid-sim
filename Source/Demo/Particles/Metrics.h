#ifndef METRICS_H
#define METRICS_H

#include <vector>
#include <map>
#include <string>

template <typename Vector>
struct MetricsParticle
{
	Vector pos, vel;
};

struct MetricsLink
{
	float residual, len;
};

template <typename Vector>
struct MetricsFrame
{
	std::vector<MetricsParticle<Vector> > particles;
	std::vector<MetricsLink> links; // TODO: rename to constraints
};

typedef std::vector<float> MetricsValues;

template <typename Vector>
class Metrics
{
public:
	Metrics() : kineticEnergy(-1) { }

	void Record(MetricsFrame<Vector>& metricsFrame);

	void Log(float val, const char* name);

	void SaveToFile(const char* path);

private:
	std::vector<MetricsFrame<Vector> > frames;
	float kineticEnergy;

	std::map<std::string, MetricsValues> mValues;
};

#endif