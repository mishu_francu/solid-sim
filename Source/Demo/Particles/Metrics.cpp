#include <Demo/Particles/Metrics.h>
#include <Math/Vector2.h>
#include <Math/Vector3.h>
#include <Engine/Platform.h>
#include <Engine/Base.h>
#include <Engine/Engine.h>
#include <algorithm>
#include <string>

using namespace Math;

template <typename Vector>
void Metrics<Vector>::Record(MetricsFrame<Vector>& metricsFrame)
{
	// write metrics
	frames.push_back(metricsFrame);
	// compute kinetic energy
	kineticEnergy = 0;
	float maxSpeed = 0;
	for (size_t i = 0; i < metricsFrame.particles.size(); i++)
	{
		Vector vel = metricsFrame.particles[i].vel;
		kineticEnergy += 0.5f * vel.LengthSquared();
		float speed = vel.Length();
		if (speed > maxSpeed)
			maxSpeed = speed;
	}

	// L1, L2 and Linf norm of the residual
	float normL1 = 0;
	float normL2 = 0;
	float normLinf = 0;
	float wave = 0;
	float maxRel = 0;
	for (size_t i = 0; i < metricsFrame.links.size(); i++)
	{
		float r = metricsFrame.links[i].residual;
		wave += r;
		float ar = fabs(r);
		float rel = ar / metricsFrame.links[i].len;
		normL1 += ar;
		normL2 += r * r;
		normLinf = max(normLinf, ar);
		maxRel = max(maxRel, rel);
	}
	float avg = normL1 / metricsFrame.particles.size();		
}

template <typename Vector>
void Metrics<Vector>::Log(float val, const char* name)
{
	mValues[name].push_back(val);
}

// save in CVS format
template <typename Vector>
void Metrics<Vector>::SaveToFile(const char* path)
{
	// global metrics
	FILE* metricsFile;
	int ret = fopen_s(&metricsFile, path, "wt");
	if (ret != 0 || metricsFile == NULL)
		return;

	// write header
	std::string header;
	for (auto it = mValues.begin(); it != mValues.end(); it++)
	{
		header += it->first + "; ";
	}
	fprintf(metricsFile, "%s\n", header.c_str());

	// write rows
	size_t index = 0;
	while (true)
	{
		std::string row;
		bool noMoreData = true;
		for (auto it = mValues.begin(); it != mValues.end(); it++)
		{
			const MetricsValues& values = it->second;
			if (index < values.size())
			{
				row += std::to_string(values[index]);
				noMoreData = false;
			}
			row += "; ";
		}
		if (noMoreData)
			break;
		fprintf(metricsFile, "%s\n", row.c_str());
		index++;
	}

	fclose(metricsFile);
}

template class Metrics<Vector2>;
template class Metrics<Vector3>;
