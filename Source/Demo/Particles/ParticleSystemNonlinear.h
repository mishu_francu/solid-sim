#ifndef PARTICLE_SYSTEM_NL_H
#define PARTICLE_SYSTEM_NL_H

#include "ParticleSystemVelocity.h"

class ParticleSystemNonlinear : public ParticleSystemVelocity
{
public:
	void MicroStep() override;
	bool SupportsSolver(SolverType aSolver) override;

	void MicroStep1();
	void MicroStep2();
	void MicroStep3();
};

#endif // PARTICLE_SYSTEM_NL_H
