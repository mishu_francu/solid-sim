#include "Soft2DDemo.h"
#include <Graphics2D/Graphics2D.h>
#include <Physics/Multibody/MultibodySystem.h>

using namespace MBD;

Soft2DDemo::Soft2DDemo() : mUseFEM(true), mBodyType(BT_GRID)
{
}

Soft2DDemo::~Soft2DDemo()
{
}

void Soft2DDemo::Update()
{
	mMultibodySystem->Step();
}

void Soft2DDemo::Init(float width, float height)
{
	mMultibodySystem.reset(new MultibodySystem(mUseFEM));

	const float offsetX = 200;
	const float offsetY = 40;
	int numPointsX = 5;
	int numPointsY = 3;
	int segmSize = 60;

	if (mBodyType == BT_TRIANGLE || mBodyType == BT_QUAD)
	{
		//triangle/quad
		mMultibodySystem->AddParticle(0, Vector2(offsetX, offsetY), Vector2());
		mMultibodySystem->AddParticle(0, Vector2(100 + offsetX, offsetY), Vector2());
		mMultibodySystem->AddParticle(1, Vector2(offsetX, offsetY + 100), Vector2());
		if (mBodyType == BT_QUAD)
			mMultibodySystem->AddParticle(1, Vector2(offsetX + 100, offsetY + 100), Vector2());
	}
	else
	{
		// grid
		for (int i = 0; i < numPointsX; i++)
		{
			for (int j = 0; j < numPointsY; j++)
			{
				mMultibodySystem->AddParticle(i == 0 ? 0 : 1, Vector2(offsetX + i * segmSize, offsetY + j * segmSize), Vector2());
			}
		}
	}

	if (!mUseFEM)
	{
		// realization of the system using distance constraints

		if (mBodyType == BT_TRIANGLE || mBodyType == BT_QUAD)
		{
			// triangle
			mMultibodySystem->AddDistanceConstraint(0, 1);
			mMultibodySystem->AddDistanceConstraint(1, 2);
			mMultibodySystem->AddDistanceConstraint(2, 0);

			// quad
			if (mBodyType == BT_QUAD)
			{
				mMultibodySystem->AddDistanceConstraint(3, 2);
				mMultibodySystem->AddDistanceConstraint(3, 1);
			}
		}
		else
		{
			for (int i = 0; i < numPointsX; i++)
			{
				int baseX = i * numPointsY;
				for (int j = 0; j < numPointsY; j++)
				{
					int baseY = baseX + j;
					// vertical links
					if (j + 1 < numPointsY)
					{
						mMultibodySystem->AddDistanceConstraint(baseY, baseY + 1);
					}
					// horizontal links
					if (i + 1 < numPointsX)
					{
						mMultibodySystem->AddDistanceConstraint(baseY, baseY + numPointsY);
					}
					// diagonals
					if (j + 1 < numPointsY && i + 1 < numPointsX)
					{
						mMultibodySystem->AddDistanceConstraint(baseY + 1, baseY + numPointsY);
						mMultibodySystem->AddDistanceConstraint(baseY, baseY + numPointsY + 1);
					}
				}
			}
		}

		mMultibodySystem->SetWidthAndHeight(width, height);
	}
	else
	{
		// realization of the system using triangle elements

		if (mBodyType == BT_TRIANGLE || mBodyType == BT_QUAD)
		{
			mMultibodySystem->AddTriangleElement(0, 1, 2);
			if (mBodyType == BT_QUAD)
				mMultibodySystem->AddTriangleElement(1, 3, 2);
		}
		else
		{
			// grid
			for (int i = 0; i < numPointsX - 1; i++)
			{
				int baseX = i * numPointsY;
				for (int j = 0; j < numPointsY - 1; j++)
				{
					int baseY = baseX + j;
					if ((i + j) % 2 == 1)
					{
						mMultibodySystem->AddTriangleElement(baseY, baseY + numPointsY, baseY + 1);
						mMultibodySystem->AddTriangleElement(baseY + 1, baseY + numPointsY, baseY + numPointsY + 1);
					}
					else
					{
						mMultibodySystem->AddTriangleElement(baseY, baseY + numPointsY + 1, baseY + 1);
						mMultibodySystem->AddTriangleElement(baseY, baseY + numPointsY, baseY + numPointsY + 1);
					}
				}
			}
		}

		mMultibodySystem->SetMaterial(MMT_NEO_HOOKEAN_OGDEN);
	}

	mMultibodySystem->Compile();
}

void Soft2DDemo::Draw(Graphics2D* graphics, int color)
{
	for (uint32 i = 0; i < mMultibodySystem->GetParticlesCount(); i++)
	{
		const MultibodySystem::Particle& p = mMultibodySystem->GetParticle(i);
		graphics->DrawCircle(p.pos, 5);
	}

	for (uint32 i = 0; i < mMultibodySystem->GetDistanceConstraintsCount(); i++)
	{
		const MultibodySystem::DistanceConstraint& dc = mMultibodySystem->GetDistanceConstraint(i);
		const MultibodySystem::Particle& p1 = mMultibodySystem->GetParticle(dc.idx1);
		const MultibodySystem::Particle& p2 = mMultibodySystem->GetParticle(dc.idx2);
		graphics->DrawLine(p1.pos, p2.pos);
	}

	for (uint32 i = 0; i < mMultibodySystem->GetTriangleElementsCount(); i++)
	{
		const auto& te = mMultibodySystem->GetTriangleElement(i);
		const MultibodySystem::Particle& p1 = mMultibodySystem->GetParticle(te.idx1);
		const MultibodySystem::Particle& p2 = mMultibodySystem->GetParticle(te.idx2);
		const MultibodySystem::Particle& p3 = mMultibodySystem->GetParticle(te.idx3);
		graphics->DrawLine(p1.pos, p2.pos);
		graphics->DrawLine(p2.pos, p3.pos);
		graphics->DrawLine(p3.pos, p1.pos);
	}
}