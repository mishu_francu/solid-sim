#include "HelloIntegrator.h"

namespace HelloIntegrator
{

	void HelloIntegratorDemo::InitLinks(real width, real height)
	{
		// configuration
		bool horizontal = false;
		bool middle = true;
		bool fixedEnd = false;
		bool noGravity = false;
		const real initialVel = 20.0f;

		const int n = 3; // number of particles
		const real startY = middle ? height * 0.5f : 0;
		const real stopY = height * 0.9f;
		SetNumParticles(n);
		real inc = (stopY - startY) / (n - 1);
		invMass[0] = 0.f;
		if (fixedEnd)
			invMass[n - 1] = 0.f;
		for (int i = 0; i < n; i++)
		{
			if (horizontal)
				pos[i].Set(width * 0.4f + startY + inc * i, 0); // drop
			else
				pos[i].Set(width * 0.5f, startY + inc * i);
			vel[i] = vec2(0, 0);
			if (i > 0)
			{
				AddLink(i - 1, i);
			}
		}
		if (!horizontal)
			vel[n - 1] = vec2(initialVel, 0);
	}

	void HelloIntegratorDemo::AccumulateForces()
	{
		const real stiffness = real(100);
		const real damping = real(0.1);

		for (size_t i = 0; pos.size(); i++)
		{
			forces[i].SetZero();
		}

		for (size_t i = 0; i < edges.size(); i++)
		{
			const Edge& link = edges[i];
			int i0 = link.idx[0];
			int i1 = link.idx[1];
			vec2 delta = pos[i0] - pos[i1];
			real len = delta.Length();
			delta *= real(1.) / len;
			real dLen = len - restLengths[i];
			vec2 force = delta;
			vec2 v = vel[i0] - vel[i1];
			real vrel = v.Dot(delta);
			force *= stiffness * dLen + damping * vrel;
			forces[i0] -= force;
			forces[i1] += force;
		}
	}

} // namespace HelloIntegrator