#include "ParticleSystemRattle.h"

void ParticleSystemRattle::MicroStep()
{
	if (!mImplicit)
		StoreNormalsForShake();

	// integrate half a step
	Scalar h(timeStep);
	Scalar f(0.5f * timeStep * lengthScale);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += f * gravity;
		particles[i].prevPos = particles[i].pos;
		particles[i].pos += h * particles[i].velocity;
	}

	Collide();

	if (mImplicit)
	{
		// this is a PBD/implicit projection
		for (int k = 0; k < numIterations; ++k)
			ProjectPositions();
	}
	else
	{
		// Shake position projection step
		if (solver == EXACT)
			ProjectNewton();
		else if (solver == GAUSS_SEIDEL)
			ProjectExplicit();
		else
			assert(false);
	}

	// update velocities
	Scalar invH(1.f / timeStep);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity = invH * (particles[i].pos - particles[i].prevPos) + f * gravity;
	}

	ProjectVelocities();

	// compute position and velocity error 2-norm
	float posNorm = 0;
	float velNorm = 0;
	float pe = 0, ve = 0;
	for (size_t i = 0; i < links.size(); i++)
	{
		Vector2 delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
		float err = delta.Length() - links[i].len;
		posNorm += err * err;
		pe += err;
		delta.Normalize();
		Vector2 v12 = timeStep * (particles[links[i].i2].GetVelocity() - particles[links[i].i1].GetVelocity());
		float vn = delta.Dot(v12);
		velNorm += vn * vn;
		ve += vn;
	}
	posNorm = sqrt(posNorm);
	velNorm = sqrt(velNorm);
	pe *= 1.f / links.size();
	ve *= 1.f / links.size();
}

bool ParticleSystemRattle::SupportsSolver(SolverType aSolver)
{
	return aSolver == EXACT || aSolver == GAUSS_SEIDEL;
}