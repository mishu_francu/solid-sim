#ifndef PARTICLE_SYSTEM_SPOOK_H
#define PARTICLE_SYSTEM_SPOOK_H

#include "ParticleSystemVelocity.h"

class ParticleSystemSpook : public ParticleSystemVelocity
{
public:
	void MicroStep() override;
	bool SupportsSolver(SolverType aSolver) override { return true; }
};

#endif // PARTICLE_SYSTEM_SPOOK_H