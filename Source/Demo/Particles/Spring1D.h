#ifndef SPRING1D_H
#define SPRING1D_H

#include <Engine/Utils.h>

class Spring1D
{
private:
	float pos, vel;
	float omega;
	float amp;
	float phase;
	float time;

public:
	Spring1D(float w, float x0, float v0);

	void Step(float dt)
	{
		//StepAnalytic(dt);
		//StepExplicitEuler(dt);
		//StepImplicitEuler(dt);
		StepSymplecticEuler(dt);
		//StepConstraintVelocity(dt);
	}

	float GetPos() const { return pos; }
	float GetVel() const { return vel; }

private:
	void StepAnalytic(float dt);
	void StepExplicitEuler(float dt);
	void StepImplicitEuler(float dt);
	void StepSymplecticEuler(float dt);
	void StepConstraintVelocity(float dt);
	void StepConstraintPosition(float dt);
};

class SpringDamped1D
{
private:
	float m, k, c;
	float x, v;

public:
	SpringDamped1D(float mass, float stiffness, float damping) 
		: m(mass)
		, k(stiffness)
		, c(damping)
	{
	}

	void SetInitial(float x0, float v0)
	{
		x = x0;
		v = v0;
	}

	void Step(float dt)
	{
		StepImplicitEuler(dt);
	}

	float GetPos() const { return x; }
	float GetVel() const { return v; }

private:
	void StepExplicitEuler(float h);
	void StepSymplecticEuler(float h);
	void StepImplicitEuler(float h);
};

class TwoSprings1D
{
private:
	float k1, k2;
	float m1, m2;
	float l1, l2;
	float x1, x2;
	float v1, v2;
};

#endif // SPRING1D_H