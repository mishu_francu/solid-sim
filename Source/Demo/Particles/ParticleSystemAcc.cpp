#include "ParticleSystemAcc.h"

void ParticleSystemAcc::MicroStep()
{
	if (diag.empty())	
	{
		constraints = links;
		BuildIncidenceMatrix();
	}

	// build matrix
	const size_t nl = links.size();
	std::vector<Vector2> n(nl);
	for (size_t i = 0; i < nl; i++)
	{
		n[i] = (particles[links[i].i2].pos - particles[links[i].i1].pos);
		n[i].Normalize();
		mat.Get(i, i) = diag[i];
	}

	// update dots between normals
	for (size_t i = 0; i < sparseMat.size(); i++)
	{
		sparseMat[i].dot = n[sparseMat[i].i].Dot(n[sparseMat[i].j]);
		const float val = sparseMat[i].a * sparseMat[i].dot;
		mat.Get(sparseMat[i].i, sparseMat[i].j) = val;
		mat.Get(sparseMat[i].j, sparseMat[i].i) = val;
	}

	//PrintMatrix(mat);

	std::vector<float> lambda(nl), rhs(nl);
	std::vector<Vector2> normals(nl);
	for (size_t i = 0; i < links.size(); i++)
	{
		Vector2 delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
		float len = delta.Length();
		float invLen = 1.f / len;
		Vector2 n = invLen * delta;
		normals[i] = n;
		float f = n.Dot(gravity * lengthScale);
		Vector2 v12 = particles[links[i].i2].velocity - particles[links[i].i1].velocity;
		float vn = n.Dot(v12);
		Vector2 vt = v12 - n * vn;
		float jDot = vt.LengthSquared() * invLen;
		rhs[i] = -f - jDot;
		lambda[i] = 0;
	}

	std::vector<float> a(nl), b(nl), c(nl);
	mat.GetTridiagonal(a, b, c); // TODO: build tridiagonal matrix directly, use symmetry (a <=> c)
	SolveTridiagonal(a, b, c, rhs, lambda);

	for (size_t i = 0 ; i < nl; i++)
	{
		Vector2 impulse = timeStep * lambda[i] * normals[i];
		particles[links[i].i2].velocity += impulse;
		particles[links[i].i1].velocity -= impulse;
	}

	Vector2 g = gravity * lengthScale * timeStep;
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += g;
		particles[i].pos += timeStep * particles[i].velocity;
	}
}