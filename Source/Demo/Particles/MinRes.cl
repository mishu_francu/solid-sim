#include "Common.cl"

__kernel void minResEdges(__global const Edge* edges, __global float4* pos, __global float* invMass, __global float4* disp, __global float4* normals, __global float4* points, float alpha)
{
	int i = get_global_id(0);
	int i1 = edges[i].i1;
	int i2 = edges[i].i2;
	float len0 = edges[i].len;
	if (edges[i].type != CONTACT)
	{
		float4 delta = pos[i1] - pos[i2];
		float len = length(delta);		
		if (edges[i].type == COLL_PAIR && len > len0)
			disp[i] = (float4)(0);
		else
		{
			float lambda = (len - len0) * alpha;
			disp[i] = edges[i].stiff * (lambda / len) * delta;
		}
	}
	else
	{
		float4 n = normals[i2];
		float len = dot(n, pos[i1] - points[i2]);
		len0 = 5;
		if (len > len0)
			disp[i] = (float4)(0);
		else
			disp[i] = alpha * (len - len0) * n;
	}
}

__kernel void conjResEdges(__global const Edge* edges, __global float4* pos, __global float* invMass, __global float4* disp, float alpha, float beta)
{
	int i = get_global_id(0);
	int i1 = edges[i].i1 & 0xffffff;
	int i2 = edges[i].i2 & 0xffffff;
	float len0 = edges[i].len;

	float4 delta = pos[i1] - pos[i2];
	float len = length(delta);
	if (len == 0)
		return;	
	float lambda = (len - len0) * alpha;
	disp[i] = edges[i].stiff * ((lambda / len) * delta + beta * disp[i]);
}

__kernel void minResParticles(__global float4* pos, __global const float* invMass, __global const Incidence* incidence, __global const float4* disp)
{
	int i = get_global_id(0);
	float4 acc = (float4)(0);
	float im = invMass[i];
	for(int j = 0; j < 16; j++)
	{
		int info = incidence[i].info[j];
		if (info == 0)
			break;
		int sgn = 1;
		int idx = GetIndexAndSign(info, &sgn);
		acc += sgn * im * disp[idx];
	}
	pos[i] += acc;
}

__kernel void conjResEdges2(__global Edge* edges)//, __global float4* pos, __global float* invMass, __global float4* disp, float alpha, float beta)
{
	int i = get_global_id(0);
	edges[i].type = 0; // flag
}

__kernel void minResParticles2(__global float4* pos, __global const float* invMass, __global const Incidence* incidence, __global float4* disp, 
							   __global Edge* edges, float alpha, float beta, int iter)
{
	int i = get_global_id(0);
	float4 acc = (float4)(0);
	float im = invMass[i];
	for(int j = 0; j < 16; j++)
	{
		int info = incidence[i].info[j];
		if (info == 0)
			break;
		int sgn = 1;
		int idx = GetIndexAndSign(info, &sgn);
		if (edges[idx].type <= iter)
		{
			int i1 = edges[idx].i1 & 0xffffff;
			int i2 = edges[idx].i2 & 0xffffff;
			float len0 = edges[idx].len;

			float4 delta = pos[i1] - pos[i2];
			float len = length(delta);
			float lambda = (len - len0) * alpha;
			disp[idx] = edges[idx].stiff * ((lambda / len) * delta + beta * disp[idx]);
			edges[idx].type++;
		}
		acc += sgn * im * disp[idx];
	}
	pos[i] += acc;
}

__kernel void minResParticlesGPU(__global float4* pos, __global const float* invMass, __global const Incidence* incidence, __global const float4* disp)
{
	int i = get_global_id(0);

	int ii = get_local_id(0);
	int base = ii * 16;
	__local int lInc[32 * 16];
	__local float4 lDisp[32 * 16];
	__local int count;

	//if (ii == 0)
	//	count = 0;
	for(int j = 0; j < 16; j++)
	{
		int info = incidence[i].info[j];
		lInc[base + j] = info;
		if (info == 0)
		{
			//lInc[base + j] = 0;
			continue;
		}
		int sgn = +1 | (info >> (sizeof(int) * 8 - 1));  // extract sign
		int idx = sgn * info - 1;
		//int old = atomic_inc(&count);
		lDisp[base + j] = disp[idx];
		//lInc[base + j] = sgn * (old + 1);
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	float4 acc = (float4)(0);
	float im = invMass[i];
	for(int j = 0; j < 16; j++)
	{
		int info = lInc[base + j];
		if (info == 0)
			break;
		int sgn = +1 | (info >> (sizeof(int) * 8 - 1));  // if v < 0 then -1, else +1
		int idx = sgn * info - 1;

		acc += sgn * im * disp[idx];//lDisp[base + j];
	}
	pos[i] += acc;
}

inline void AtomicAdd(volatile __local float *source, const float operand) {
    union {
        unsigned int intVal;
        float floatVal;
    } newVal;
    union {
        unsigned int intVal;
        float floatVal;
    } prevVal;
    do {
        prevVal.floatVal = *source;
        newVal.floatVal = prevVal.floatVal + operand;
    } while (atomic_cmpxchg((volatile __local unsigned int *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}

inline void AtomicAdd3(volatile __local float4* source, const float4 operand)
{
	__local float* ptr = (__local float*)source;
	AtomicAdd(ptr, operand.x);
	ptr++;
	AtomicAdd(ptr, operand.y);
	ptr++;
	AtomicAdd(ptr, operand.z);
	ptr++;
}

__kernel void conjResEdges1(__global const Edge* edges, __global float4* pos, __global float* invMass, __global float4* disp, float alpha, float beta, __global Accumulator* acc)
{
	int i = get_global_id(0);

	int val1 = edges[i].i1;
	int val2 = edges[i].i2;
	int i1 = val1 & 0xffffff;
	int i2 = val2 & 0xffffff;
	float len0 = edges[i].len;

	float4 delta = pos[i1] - pos[i2];
	float len = length(delta);
	if (len == 0)
		return;	
	int cur1 = val1 >> 24;
	int cur2 = val2 >> 24;
	float lambda = (len - len0) * alpha; // TODO: divide by im1 + im2
	float4 imp0 = disp[i];
	float4 imp = edges[i].stiff * ((lambda / len) * delta + beta * imp0); // TODO: previous lambda instead of disp
	disp[i] = imp;

	acc[i1].disp[cur1] = -imp;
	acc[i2].disp[cur2] = imp;
}

__kernel void minResParticles1(__global float4* pos, __global const float* invMass, __global Accumulator* acc)
{
	int i = get_global_id(0);
	float4 disp = (float4)(0);
	for (int k = 0; k < MAX_INCIDENCE; k++)
	{
		disp += acc[i].disp[k];
	}
	float im = invMass[i];
	pos[i] += im * disp;
}

__kernel void minResParticles1red(__global float4* pos, __global const float* invMass, __global Accumulator* acc)
{
#define STEPS 1

	__local float4 buf[256]; // TODO: exact local size
	int i = get_global_id(0) >> 4; // particle idx
	int lid = get_local_id(0);
	int j = lid & 15; // neighbour idx

	// load into local mem
	buf[lid] = acc[i].disp[j];
	barrier(CLK_LOCAL_MEM_FENCE);

	//for (int k = 0; k < STEPS; k++)
	//{
	//	int mask = (1 << (k + 1)) - 1;
	//	if ((lid & mask) == 0)
	//	{
	//		buf[lid] = buf[lid] + buf[lid + (1 << k)];
	//	}
	//	barrier(CLK_LOCAL_MEM_FENCE);
	//}

	if (j < 8)
		buf[lid] = buf[lid] + buf[lid + 8];
	barrier(CLK_LOCAL_MEM_FENCE);

	if (j != 0)
		return;
	float4 disp = (float4)(0);
	for (int k = 0; k < 8; k++)
		disp += buf[lid + k];
	pos[i] += invMass[i] * disp;
}
