#ifndef DISTANCE_MAP_H
#define DISTANCE_MAP_H

class DistanceMap
{
public:
	DistanceMap() : m_map(NULL) { }
	~DistanceMap() 
	{ 
		if (m_map) {
			delete[] m_map;
			m_map = NULL;
		}
	}
	bool Create(const char* fileName);
	bool Load(const char* fileName);
	bool IsActive() { return m_map != NULL; }
	float GetDist(int x, int y) 
	{
		if (!m_map)
			return 0;
		int idx = x + y * m_width;
		if (x >= 0 && x < m_width && y >= 0 &&y < m_height)
			return m_map[idx]; // TODO: checks
		return 1e37f;
	}
public:
	int m_width, m_height;
	float* m_map;
};

#endif // DISTANCE_MAP_H