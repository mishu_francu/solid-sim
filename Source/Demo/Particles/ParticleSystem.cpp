#include <Engine/Base.h>

#define FLOAT_SCALING_OPERATORS

#include <Demo/Particles/ParticleSystem.h>
#include <Engine/Engine.h>

#include <stdio.h>
#include <map>

template <typename Particle, typename Broadphase>
ParticleSystem<Particle, Broadphase>::ParticleSystem() :
	radius(1.f),
	restitution(0.f),
	saveMetrics(true)
{
	nFrames = 0;
}

template <typename Particle, typename Broadphase>
ParticleSystem<Particle, Broadphase>::~ParticleSystem()
{
	newMetrics.SaveToFile("metrics.csv");
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::Init(float aWidth, float aHeight, float aRadius)
{
	width = aWidth;
	height = aHeight;
	radius = aRadius;
	
	broadphase.Init(width, height, 2 * radius);

	frames.clear();

	// clean up the particles and constraints
	particles.clear();
	aabbs.clear();
	links.clear();
	constraints.clear();
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::SetNumParticles(int n) 
{
	particles.resize(n);
	aabbs.resize(n);

	// prepare the mesh
	mesh.vertices = (char*)&particles[0].pos;
	mesh.stride = sizeof(Particle);
	mesh.numVerts = particles.size();
}

template <typename Particle, typename Broadphase>
int ParticleSystem<Particle, Broadphase>::GetParticleAt(float x, float y)
{
	Vector2 pos(x, y);
	float minLen = 1e37f;
	int minIdx = -1;
	for (size_t i = 0; i < particles.size(); i++)
	{
		Vector2 delta = particles[i].pos;
		delta.Subtract(pos);
		float len = delta.Length();
		if (len < minLen)
		{
			minLen = len;
			minIdx = (int)i;
		}
	}
	if (minLen > radius)
		return -1;
	return minIdx;
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::WallCollisions() // TODO: rename to static collisions
{
	// TODO: use the grid
	static Vector2 up(0.f, -1.f);
	static Vector2 down(0.f, 1.f);
	static Vector2 left (-1.f, 0.f);
	static Vector2 right (1.f, 0.f);

	const float r = radius + tolerance;
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;

		const Particle& particle = particles[i];
		if (particle.pos.GetY() + r >= height) // down
			AddContact(i, Vector2(particle.pos.GetX(), height), up);
		else if (particle.pos.GetY() - r <= 0) // up
			AddContact(i, Vector2(particle.pos.GetX(), 0.f), down);
		if (particle.pos.GetX() + r >= width) // right
			AddContact(i, Vector2(width, particle.pos.GetY()), left);
		else if (particle.pos.GetX() - r <= 0) // left
			AddContact(i, Vector2(0.f, particle.pos.GetY()), right);
	}

	// collision with boxes
	Vector2 p, n;
	std::vector<int> ids;
	for (size_t j = 0; j < collBox.size(); j++)
	{
		// get AABB
		Aabb aabb;
		// TODO: virtual or CRTP?
		if (collBox[j]->type == Primitive::OBB)
			aabb = ((Obb*)collBox[j].get())->GetAabb();
		else if (collBox[j]->type == Primitive::CIRCLE)
			aabb = ((Circle*)collBox[j].get())->GetAabb();
		else if (collBox[j]->type == Primitive::SEGMENT)
			aabb = ((Segment*)collBox[j].get())->GetAabb();
		aabb.Expand(r);

		broadphase.TestObject(aabb, ids);
		for (size_t i = 0; i < ids.size(); i++)
		{
			int idx = ids[i];
			if (IntersectPrimitive(particles[idx].pos, r, collBox[j].get(), p, n))
				AddContact(idx, p, n);
		}
	}

#ifdef COLLIDE_LINKS
	// collide links
	float t;
	for (size_t i = 0; i < links.size(); i++)
	{
		if (collBox[0]->type == Primitive::CIRCLE)
		{
			Circle* circle = (Circle*)collBox[0].get();
			Vector2 p = ClosestPtSegm(circle->pos, particles[links[i].i1].pos, particles[links[i].i2].pos, t);
			Vector2 n = p - circle->pos;
			if (n.Length() < r + circle->radius)
			{
				n.Normalize();
				Constraint contact;
				contact.type = Constraint::EDGE;
				contact.i1 = links[i].i1;
				contact.i2 = links[i].i2;
				contact.point = circle->pos + circle->radius * n;
				contact.normal = n;
				contact.w1 = 1.f - t;
				contact.w2 = t;
				constraints.push_back(contact);
				//AddContact(links[i].i1, circle->pos + circle->radius * n, n);
				//AddContact(links[i].i2, circle->pos + circle->radius * n, n);
			}
		}
	}
#endif
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::PairwiseBroadphase()
{
	const int n = (int)GetNumParticles();
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = i + 1; j < n; j++)
		{
			if (AabbOverlap(aabbs[i], aabbs[j]))
				AddCollisionPair(i, j);
		}
	}
}

template <typename Particle, typename Broadphase>
inline void ParticleSystem<Particle, Broadphase>::Collide()
{
	constraints.clear();
	if (detectCollision)
	{
		PROFILE_SCOPE("Collide");
		UpdateAabbs();
		ConstraintVector candidates;
		broadphase.Update(aabbs, candidates);
		// TODO: pairwise broadphase
		float radTol = radius + tolerance;
		float radSqr = 4 * radTol * radTol;
		for (size_t i = 0; i < candidates.size(); i++)
		{
			if ((particles[candidates[i].i1].pos - particles[candidates[i].i2].pos).LengthSquared() <= radSqr)
				constraints.push_back(candidates[i]);
		}

		WallCollisions();
	}	
	constraints.insert(constraints.end(), links.begin(), links.end());
	
	//if (solver == PhysicsSystem::MIN_RES)
	//	BuildIncidenceMatrix();
}

template <typename Particle, typename Broadphase>
inline void ParticleSystem<Particle, Broadphase>::UpdateAabbs()
{
	float tol = radius + tolerance;
	Vector2 rad(tol, tol);
	for (size_t i = 0; i < GetNumParticles(); i++)
		particles[i].GetAabb(aabbs[i].min, aabbs[i].max, rad);
}

// ******** PROJECTIONS ****************************

// Gauss-Seidel PBD position projection (contacts, links and collision pairs)
template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectPositions(int maxIter)
{
	static Particle ground; ground.invMass = 0;

	PROFILE_SCOPE("ProjectPositions");
	int n = maxIter == 0 ? numIterations : maxIter;	
	for (int k = 0; k < n; ++k)
	{
		//Collide();
		for (size_t i = 0; i < constraints.size(); i++)
		{
			//if (constraints[i].type == Constraint::EDGE)
			//{
			//	Constraint& contact = constraints[i];
			//	Vector2 p = contact.w1 * particles[contact.i1].pos + contact.w2 * particles[contact.i2].pos;
			//	float len0 = constraints[i].len;
			//	float len = contact.normal.Dot(p - contact.point);
			//	if (len > len0)
			//		continue;
			//	float s = 1.f / (contact.w1 * contact.w1 + contact.w2 * contact.w2);
			//	Vector2 disp = s * (len - len0) * contact.normal;
			//	particles[contact.i1].pos -= disp * (contact.w1 * particles[contact.i1].invMass);
			//	particles[contact.i2].pos -= disp * (contact.w2 * particles[contact.i2].invMass);
			//	continue;
			//}
			Particle* p1 = &particles[constraints[i].i1];
			Vector2 delta = p1->pos;
			Particle* p2 = NULL;
			if (constraints[i].type == Constraint::CONTACT)
			{
				p2 = &ground;
				p2->pos = constraints[i].point;
			}
			else
				p2 = &particles[constraints[i].i2];
			delta.Subtract(p2->pos);
			float len0 = constraints[i].len;
			float len = constraints[i].type == Constraint::CONTACT ? delta.Dot(constraints[i].normal) : delta.Length();
			if (constraints[i].type != Constraint::LINK && len > len0)
				continue;
			if (constraints[i].type == Constraint::CONTACT)
				delta = constraints[i].normal;
			else
			{
				//if (k == 0)
				//{
					delta.Scale(1 / len);
				//	constraints[i].normal = delta;
				//}
				//else
				//	delta = constraints[i].normal;
			}
			delta.Scale(constraints[i].stiffness * (len - len0) / (p1->invMass + p2->invMass));
			p1->pos.Subtract(delta * p1->invMass);
			p2->pos.Add(delta * p2->invMass);
		}
	}
}
	
template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectPositionsJacobi(float omega, int maxIter)
{
	if (constraints.size() == 0)
		return;
	static Particle ground; ground.invMass = 0;

	PROFILE_SCOPE("ProjectPositionsJacobi");
	int n = maxIter == 0 ? numIterations : maxIter;	
	for (int k = 0; k < n; ++k)
	{
		// TODO: buffer positions
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Particle* p1 = &particles[constraints[i].i1];
			Vector2 delta = p1->pos;
			Particle* p2 = NULL;
			if (constraints[i].type == Constraint::CONTACT)
			{
				p2 = &ground;
				p2->pos = constraints[i].point;
			}
			else
				p2 = &particles[constraints[i].i2];
			delta.Subtract(p2->pos);
			float len0 = constraints[i].len;
			float len = constraints[i].type == Constraint::CONTACT ? delta.Dot(constraints[i].normal) : delta.Length();
			if (constraints[i].type != Constraint::LINK && len > len0)
			{
				constraints[i].lambda = 0;
				constraints[i].disp.SetZero();
				continue;
			}
			if (constraints[i].type != Constraint::CONTACT)
			{
				delta.Scale(1 / len);
				constraints[i].normal = delta;
			}
			float lambda = (len - len0) / (p1->invMass + p2->invMass);
			constraints[i].disp = omega * lambda * constraints[i].normal;
		}
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Vector2 delta = constraints[i].disp;
			particles[constraints[i].i1].pos.Subtract(delta * particles[constraints[i].i1].invMass);
			if (constraints[i].type != Constraint::CONTACT)
				particles[constraints[i].i2].pos.Add(delta * particles[constraints[i].i2].invMass);
		}
	}
}

// ******** PROJECTIONS (obsolete) ****************************

// solves a pair of collding particles by the Jakobsen method inside a Projected Gauss-Seidel loop
template <typename Particle, typename Broadphase>
INLINE void ParticleSystem<Particle, Broadphase>::SolvePairPgs(Constraint& pair, const Scalar& r, const Scalar& diamSqr)
{
	static const Scalar quart(0.25f);
	const Scalar half(0.5f);
	Particle& p1 = particles[pair.i1];
	Particle& p2 = particles[pair.i2];
	Vector2 delta = p1.pos;
	delta.Subtract(p2.pos);
	const Scalar len2 = delta.LengthSquared();
	if (len2 >= diamSqr)
		return;
	const Scalar len = r + (len2 * quart) / r;
	delta.Scale(r / len - half);
	p1.pos.Add(delta);
	p2.pos.Subtract(delta);
}

// TODO: maybe rename to Catto and templatize (is it useful anyway?)
// solvers a pair of colliding particles by the Catto method (LCP) and computes lambda
template <typename Particle, typename Broadphase>
INLINE void ParticleSystem<Particle, Broadphase>::SolvePairPgsLambda(Constraint& pair, const Scalar& r, const Scalar& diamSqr)
{
	Particle& p1 = particles[pair.i1];
	Particle& p2 = particles[pair.i2];
	Vector2 delta = p1.pos;
	delta.Subtract(p2.pos);
	Scalar len = delta.Length();
	if (len < Scalar(0.001f))
		return;
	Scalar dLambda = r - Scalar(0.5f) * len;
	Scalar lambda0(pair.lambda);
	pair.lambda = lambda0 + dLambda;
	const Scalar zero(0.f);
	if (pair.lambda < zero)
		pair.lambda = zero; // TODO: branchless
	dLambda = Scalar(pair.lambda) - lambda0;
	delta.Scale(dLambda / len);
	p1.pos.Add(delta);
	p2.pos.Subtract(delta);
}

// solves a particle colliding a wall inside a Projected Gauss-Seidel loop
template <typename Particle, typename Broadphase>
INLINE void ParticleSystem<Particle, Broadphase>::SolveContactPgs(Constraint& pair, const Scalar& r)
{
	Particle& p1 = particles[pair.i1];
	Vector2 delta = p1.pos;
	delta.Subtract(pair.point);
	const Scalar len = delta.Dot(pair.normal);
	if (len >= r)
		return;
	const Scalar depth = r - len;
	Vector2 disp = pair.normal;
	disp.Scale(depth);
	p1.pos.Add(disp);
}

template <typename Particle, typename Broadphase>
INLINE void ParticleSystem<Particle, Broadphase>::SolveContactJacobi(Constraint& pair, float r)
{
	Particle& p1 = particles[pair.i1];
	Vector2 delta = p1.pos;
	delta.Subtract(pair.point);
	const float len = delta.Dot(pair.normal);
	if (len >= r)
		pair.disp.SetZero();
	else 
	{
		pair.disp = pair.normal;
		pair.disp.Scale(Scalar(r - len));
	}
}

template <typename Particle, typename Broadphase>
INLINE void ParticleSystem<Particle, Broadphase>::SolveLinkJakobsen(Constraint& link)
{
	const Scalar half(0.5f);
	Particle& p1 = particles[link.i1];
	Particle& p2 = particles[link.i2];
	Vector2 delta = p1.pos;
	delta.Subtract(p2.pos);
	Scalar len = delta.Length();
	Scalar len0 = Scalar(link.len);
	delta.Normalize();
	Scalar dLen = (len - len0) * half;
	Scalar sum(1.f / (p1.invMass + p2.invMass));
	delta.Scale(dLen * sum);
	link.disp.Add(delta);
	p1.pos.Subtract(delta * p1.invMass);
	p2.pos.Add(delta * p2.invMass);
}

template <typename Particle, typename Broadphase>
INLINE void ParticleSystem<Particle, Broadphase>::SolveLinkShakeExact(Constraint& link)
{
	Particle& p1 = particles[link.i1];
	Particle& p2 = particles[link.i2];
	Vector2 delta = p1.pos;
	delta.Subtract(p2.pos);
	Vector2 delta0 = p1.prevPos;
	delta0.Subtract(p2.prevPos);

	Scalar dot = delta.Dot(delta0);
	Scalar lenSqr = delta0.LengthSquared();
	Scalar len(link.len);
	Scalar C = (delta.LengthSquared() - len * len);
	Scalar lambda = (-dot + sqrt(dot * dot - lenSqr * C)) / lenSqr;
	delta0.Scale(lambda);
	link.disp.Add(delta0);
	p1.pos.Add(delta0);
	p2.pos.Subtract(delta0);
}

template <typename Particle, typename Broadphase>
INLINE void ParticleSystem<Particle, Broadphase>::SolveLinkShake1(Constraint& link)
{
	Particle& p1 = particles[link.i1];
	Particle& p2 = particles[link.i2];
	Vector2 delta = p1.pos;
	delta.Subtract(p2.pos);
	Vector2 delta0 = p1.prevPos;
	delta0.Subtract(p2.prevPos);

	Scalar len(link.len);
	Scalar lambda = Scalar(-0.5f) * (delta.LengthSquared() - len * len) / delta.Dot(delta0);
	delta0.Scale(lambda);
	link.disp.Add(delta0);
	p1.pos.Add(delta0);
	p2.pos.Subtract(delta0);
}

template <typename Particle, typename Broadphase>
inline void ParticleSystem<Particle, Broadphase>::SolvePositionsJacobi(Constraint& pair)
{
	const float r = radius;
	const float diamSqr = 4 * r * r;
	Particle& p1 = particles[pair.i1];
	Particle& p2 = particles[pair.i2];
	Vector2 delta = p1.pos;
	delta.Subtract(p2.pos);
	const float len2 = delta.LengthSquared();
	if (len2 >= diamSqr) 
	{
		pair.disp.SetZero();
		return;
	}
	const float len = r + (len2 * 0.25f) / r;
	delta.Scale(Scalar(r / len - 0.5f));
	pair.disp = delta;
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::RecordMetrics()	
{
	if (!saveMetrics)
		return;

	// new metrics
	MetricsFrame<Vector2> frame;
	frame.particles.resize(particles.size());
	for (size_t i = 0; i < particles.size(); i++)
	{
		frame.particles[i].vel = (1.f / timeStep) * (particles[i].pos - particles[i].prevPos);
	}
	frame.links.resize(constraints.size());
	for (size_t i = 0; i < constraints.size(); i++)
	{
		if (constraints[i].type == Constraint::CONTACT || constraints[i].stiffness < 1)
			continue;
		Vector2 delta = particles[constraints[i].i1].pos - particles[constraints[i].i2].pos;
		float r = delta.Length() - constraints[i].len;
		frame.links[i].residual = r;
		frame.links[i].len = constraints[i].len;
	}	
	
	// analyze data; TODO: move to a function called at the end
	newMetrics.Record(frame);
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::SaveMetrics()
{
//#ifndef ANDROID_NDK
//	// global metrics
//	FILE* metricsFile;
//	int ret = fopen_s(&metricsFile, "metrics.csv", "wt");
//	if (!ret && metricsFile) {
//		fprintf(metricsFile, "Kinetic Energy, Potential Enerygy, Penetration, VelRel, lambda, NumPairs\n");
//		for (size_t i = 0; i < metrics.size(); i++)
//		{
//			size_t n = metrics[i].constraints.size();
//			float depth = n ? metrics[i].penetration / n : 0.f;
//			float vrel = n ? metrics[i].vrel / n : 0.f;
//			float lambda = n ? metrics[i].lambda / n : 0.f;
//			fprintf(metricsFile, "%f,%f,%f,%f,%f,%d\n", metrics[i].energyK, metrics[i].energyP, depth, vrel, lambda, n);
//		}
//		fclose(metricsFile);
//	}
//#endif
}

// TODO: rename to ProjectPositionsDense (using the whole matrix); also Newton
template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectPositionsExact()
{
#ifndef ANDROID_NDK
	const size_t nl = links.size();
	std::vector<Vector2> n(nl);

	BuildMatrix();

	//AnalyzeEigenValues();
	//return;

	// print the matrix
	//Printf("Matrix A\n");
	//PrintMatrix(mat);

#define LU_DECOMP
#define IS_ROPE
#ifdef LU_DECOMP
	// LU decomposition
	MatrixSqr<float> l(nl), u(nl);
	mat.DecomposeLU(l, u);
#endif

	// Newton raphson iterations
	std::vector<float> r(nl), lambda(nl), lambdaExact(nl);
	const int numIters = numIterations; // Newton iterations

	// smoothing vars
	std::vector<float> resSmooth(nl), lambdaSmooth(nl);
	std::vector<Vector2> posSmooth(particles.size());
	for (size_t i = 0; i < particles.size(); i++)
	{
		posSmooth[i] = particles[i].pos;
	}

	for (int iter = 0; iter < numIters; iter++)
	{
		// compute normals and reset lambda
		float resL1 = 0;
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 delta;
			delta = (particles[links[i].i2].pos - particles[links[i].i1].pos);
			r[i] = delta.Length() - links[i].len;
			resL1 += (float)fabs(r[i]);
			
			delta.Normalize();
			n[i] = delta;

			lambda[i] = 0.f; // TODO: better warm starting
		}
		// update dots between normals
		for (size_t i = 0; i < sparseMat.size(); i++)
		{
			sparseMat[i].dot = n[sparseMat[i].i].Dot(n[sparseMat[i].j]);
			const float val = sparseMat[i].a * sparseMat[i].dot;
			mat.Get(sparseMat[i].i, sparseMat[i].j) = val;
			mat.Get(sparseMat[i].j, sparseMat[i].i) = val;
		}

#ifdef IS_ROPE
		// TODO: attenuate solution/rhs -> try to reduce Newton iterations
		std::vector<float> a(nl), b(nl), c(nl);
		mat.GetTridiagonal(a, b, c); // TODO: build tridiagonal matrix directly, use symmetry (a <=> c)
		SolveTridiagonal(a, b, c, r, lambda);
#elif defined(LU_DECOMP)
		SolveLU(l, u, r, lambda);
#else
//#	ifdef _DEBUG
//		MatrixSqr<float> l(nl);
//		if (!mat.DecomposeCholesky(l))
//			Printf("Not PD\n");
//#	endif

		SolveGaussSeidel(mat, r, lambda, 10);
		//SolveJacobi(mat, r, lambda, 1/*numIterations*/);

		// Conjugate Gradient family
		//SolveSteepestDescent(mat, r, lambda, 10);
		//SolveConjugateGradient(mat, r, lambda, 10);
		//SolvePreconditionedConjugateGradient(mat, r, lambda, Preconditioner<float>::Diagonal, numIterations);
		//SolveBiconjugateGradient(mat, r, lambda, numIterations);
		//SolveMinimalResidual(mat, r, lambda, 10);
		//SolvePreconditionedMinimalResidual(mat, r, lambda, Preconditioner<float>::Diagonal, numIterations);
#endif
		// aplly lambdas to positions
		float errL1 = 0;
		for (size_t i = 0; i < nl; i++)
		{
			// error - norm 1
			float err = (float)(lambda[i] - lambdaExact[i]);
			errL1 += fabs(err);

			const Constraint& link = links[i];
			float l = (float)lambda[i]; // !!!
			Vector2 disp = l * n[i];
			particles[link.i1].pos += particles[link.i1].invMass * disp;
			particles[link.i2].pos -= particles[link.i2].invMass * disp;
		}
	}
#endif
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::SolveNonlinearConjugateGradient()
{
	const size_t n = links.size();
	std::vector<float> r(n);
	std::vector<Vector2> normals(n);
	for (size_t i = 0; i < n; i++)
	{
		Vector2 delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
		r[i] = (float)delta.Length() - links[i].len;
		delta.Normalize(); // FIXME
		normals[i] = delta;
	}
	// update dots between normals and compute alpha
	for (size_t k = 0; k < sparseMat.size(); k++)
	{
		sparseMat[k].dot = normals[sparseMat[k].i].Dot(normals[sparseMat[k].j]);
	}

	std::vector<float> d(r);
	std::vector<float> p(n);
	MultiplyBySparseMatrix(r, sparseMat, diag, p);
	float dNew = r * p;
	const int numItersNR = 1;
	std::vector<float> q(n);
	std::vector<float> s(n);
	for (int iter = 0; iter < numIterations; iter++)
	{
		for (int iterNR = 0; iterNR < numItersNR; iterNR++)
		{
			MultiplyBySparseMatrix(d, sparseMat, diag, q);
			//float alpha = (r * d) / (d * q);
			float alpha = (r * q) / (q * q); // min res
			// aplly lambdas to positions
			for (size_t i = 0; i < n; i++)
			{
				const Constraint& link = links[i];
				Vector2 disp = alpha * d[i] * normals[i];
				particles[link.i1].pos += particles[link.i1].invMass * disp;
				particles[link.i2].pos -= particles[link.i2].invMass * disp;
			}
			// compute new residual
			for (size_t i = 0; i < n; i++)
			{
				Vector2 delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
				float newR = (float)delta.Length() - links[i].len;
				s[i] = newR - r[i];
				r[i] = newR;
				delta.Normalize(); // FIXME
				normals[i] = delta;
			}
			for (size_t k = 0; k < sparseMat.size(); k++)
			{
				sparseMat[k].dot = normals[sparseMat[k].i].Dot(normals[sparseMat[k].j]);
			}
		}
		float dOld = dNew;
		MultiplyBySparseMatrix(r, sparseMat, diag, p); // for min res
		dNew = r * p;
		float beta = dNew / dOld; // Fletcher-Reeves
		//float beta = max(0.f, (p * s) / dOld); // Polak-Ribiere (unstable)
		d = r + beta * d;
	}
}

//#define SD_UPDATE_NORMALS

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectPositionsSd()
{
	// TODO: check if matrix is PD

	size_t m = links.size();
	std::vector<float> r(m);
	std::vector<Vector2> normals(m);
	float nom = 0.f;
	for (size_t i = 0; i < m; i++)
	{
		Vector2 delta = (particles[links[i].i2].pos - particles[links[i].i1].pos);
		r[i] = (float)delta.Length() - links[i].len;
		nom += r[i] * r[i];
		delta.Normalize();
		normals[i] = delta;
	}

	size_t n = GetNumParticles();
	std::vector<Vector2> y(n);
	std::vector<float> q(m);
	for (int iter = 0; iter < numIterations; iter++)
	{
		for (size_t i = 0; i < n; i++)
			y[i].SetZero();
		for (size_t i = 0; i < m; i++)
		{
			float sum = 1.f;// / (particles[links[i].i1].invMass + particles[links[i].i2].invMass);
#ifdef SD_UPDATE_NORMALS
			Vector2 delta = (particles[links[i].i2].pos - particles[links[i].i1].pos);
			delta.Normalize();
			normals[i] = delta;
#endif
			if (particles[links[i].i1].invMass == 0 || particles[links[i].i2].invMass == 0)
				sum = 2.f;
			Vector2 dy = normals[i] * (r[i] * sum);
			y[links[i].i1] -= dy * particles[links[i].i1].invMass;
			y[links[i].i2] += dy * particles[links[i].i2].invMass;
		}
		float den = 0.f;
		for (size_t i = 0; i < m; i++)
		{
			Vector2 dy = y[links[i].i2] - y[links[i].i1];
			q[i] = normals[i].Dot(dy);
			den += r[i] * q[i];
		}
		float alpha = nom / den;
		nom = 0.f;
		for (size_t i = 0; i < m; i++)
		{
			float sum = 1.f;// / (particles[links[i].i1].invMass + particles[links[i].i2].invMass); // TODO: is it really like this?
			Vector2 disp = (alpha * r[i] * sum) * normals[i];
			particles[links[i].i1].pos += disp * particles[links[i].i1].invMass;
			particles[links[i].i2].pos -= disp * particles[links[i].i2].invMass;
			r[i] -= alpha * q[i];
			nom += r[i] * r[i];
		}
	}
}

void MultiplyBySparseMatrix(const std::vector<float>& x, const std::vector<SparseElement>& a, const std::vector<float>& diag, std::vector<float>& y)
{
	for (size_t k = 0; k < x.size(); k++)
	{
		y[k] = diag[k] * x[k];
	}
	for (size_t k = 0; k < a.size(); k++)
	{
		const float val = a[k].a * a[k].dot;
		y[a[k].i] += val * x[a[k].j];
		y[a[k].j] += val * x[a[k].i];
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::BuildMatrix()
{
	if (diag.empty())
	{
		BuildIncidenceMatrix();
	}

	// build matrix
	const size_t nl = links.size();
	std::vector<Vector2> n(nl);
	for (size_t i = 0; i < nl; i++)
	{
		n[i] = (particles[links[i].i2].pos - particles[links[i].i1].pos);
		n[i].Normalize();
		mat.Get(i, i) = diag[i];
	}

	// update dots between normals
	for (size_t i = 0; i < sparseMat.size(); i++)
	{
		sparseMat[i].dot = n[sparseMat[i].i].Dot(n[sparseMat[i].j]);
		const float val = sparseMat[i].a * sparseMat[i].dot;
		mat.Get(sparseMat[i].i, sparseMat[i].j) = val;
		mat.Get(sparseMat[i].j, sparseMat[i].i) = val;
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::BuildIncidenceMatrix()
{
	if (constraints.empty())
		return;

	sparseMat.clear();

	std::vector<std::vector<size_t> > vertexIn(GetNumParticles() + 1);
	diag.resize(constraints.size());
	//float avg = 0.f;
	for (size_t i = 0; i < constraints.size(); i++)
	{
		const size_t i1 = constraints[i].i1;
		const size_t i2 = constraints[i].i2;
		vertexIn[i1].push_back(i);
		if (i2 != 0xffff)
		{
			vertexIn[i2].push_back(i);
			diag[i] = particles[i1].invMass + particles[i2].invMass;
		}
		else
		{
			diag[i] = particles[i1].invMass;
			vertexIn[vertexIn.size() - 1].push_back(i);
		}
		//avg += diag[i];
	}

	for (size_t i = 0; i < vertexIn.size(); i++)
	{
		size_t num = vertexIn[i].size();
		if (num == 0)
			continue;
		for (size_t j = 0; j < num - 1; j++)
		{
			for (size_t k = j + 1; k < num; k++)
			{
				SparseElement elem;
				const size_t c1 = vertexIn[i][j];
				const size_t c2 = vertexIn[i][k];
				elem.i = c1;
				elem.j = c2;
				float invMass = -1.f;
				// TODO: what if sorted? only 3 cases?
				if (constraints[c1].i1 == constraints[c2].i1)
				{
					invMass = particles[constraints[c1].i1].invMass;
				}
				else if (constraints[c1].i2 == constraints[c2].i2)
				{
					invMass = constraints[c1].i2 != 0xffff ? particles[constraints[c1].i2].invMass : 0;
				}
				else if (constraints[c1].i1 == constraints[c2].i2)
				{
					invMass = -particles[constraints[c1].i1].invMass;
				}
				else if (constraints[c1].i2 == constraints[c2].i1)
				{
					invMass = constraints[c1].i2 != 0xffff ? -particles[constraints[c1].i2].invMass : 0;
				}
				elem.a = invMass;
				sparseMat.push_back(elem);
			}
		}
	}

	// init dense matrix too
	mat.SetSize(constraints.size());
	mat.SetZero();
}

#define MINRES_UPDATE_RHS

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectPositionsMinRes(bool gsWarmStart)
{
	PROFILE_SCOPE("Project MinRes");

	const size_t n = constraints.size();
	if (n == 0)
		return;
	int numIters = numIterations;
	if (gsWarmStart)
	{
		//ProjectPositions(1);
		ProjectPositionsJacobi(1);
		if (--numIters == 0)
			return;
	}

	if (r.empty())
	{
		BuildIncidenceMatrix();
		r.resize(n);
		z.resize(n);
		d.resize(n);
		normals.resize(n);
	}

	float rSqr = 0;
	float ySqr = 0;
	for (size_t i = 0; i < n; i++)
	{
		const int i1 = constraints[i].i1;
		Vector2 delta;
		//if (constraints[i].type == Constraint::CONTACT)
		//{
		//	delta = particles[i1].pos - constraints[i].point;
		//	normals[i] = constraints[i].normal; normals[i].Flip();
		//	r[i] = delta.Length() - radius;
		//}
		//else
		{
			const int i2 = constraints[i].i2;
			delta = particles[i2].pos - particles[i1].pos;
			r[i] = delta.Length() - constraints[i].len;		
			delta.Normalize(); // TODO: return length
			normals[i] = delta;
		}
		z[i] = diag[i] * r[i];
		rSqr += r[i] * z[i];
		ySqr += z[i] * z[i];
	}

	if (rSqr == 0 || ySqr == 0)
		return;

	// update dots between normals and compute alpha
	for (size_t k = 0; k < sparseMat.size(); k++)
	{
		sparseMat[k].dot = normals[sparseMat[k].i].Dot(normals[sparseMat[k].j]); // TODO: prune near zero elements
		const float val = sparseMat[k].a * sparseMat[k].dot;
		sparseMat[k].val = val;
		const size_t i = sparseMat[k].i;
		const size_t j = sparseMat[k].j;
		const float di = val * r[j];
		const float dj = val * r[i];
		ySqr += di * di + dj * dj + 2 * (di * z[i] + dj * z[j]);
		rSqr += r[i] * di + r[j] * dj;
		z[i] += di;
		z[j] += dj;
	}

	// the actual min res alorithm
	float alpha = rSqr / ySqr;
	for (size_t i = 0; i < n; i++)
	{
		const Constraint& link = constraints[i];
		//if (link.type != Constraint::LINK && r[i] > 0)
		//	continue;
		Vector2 disp = alpha * r[i] * normals[i];
		particles[link.i1].pos += particles[link.i1].invMass * disp;
		//if (link.type != Constraint::CONTACT)
			particles[link.i2].pos -= particles[link.i2].invMass * disp;
		d[i] = r[i];
	}
	for (int iter = 1; iter < numIters; iter++)
	{		
		float rSqr1 = 0;
#ifdef MINRES_UPDATE_RHS
		for (size_t i = 0; i < n; i++)
		{
			const int i1 = constraints[i].i1;
			Vector2 delta;
			//if (constraints[i].type == Constraint::CONTACT)
			//{
			//	delta = constraints[i].point - particles[i1].pos;
			//	r[i] = delta.Length() - radius;
			//}
			//else
			{
				const int i2 = constraints[i].i2;
				delta = particles[i2].pos - particles[i1].pos;
				r[i] = delta.Length() - constraints[i].len;
				// just added
				delta.Normalize();
				normals[i] = delta;
			}
			rSqr1 += r[i] * r[i] * diag[i];
		}
		// why was this needed?
		if (rSqr1 == 0) // TODO: threshold, better termination condition
			break;
#else
		for (size_t k = 0; k < n; k++)
		{
			r[k] -= alpha * z[k];
			rSqr1 += r[k] * r[k] * diag[k];
		}
#endif
		for (size_t k = 0; k < sparseMat.size(); k++)
		{
			const float val = sparseMat[k].val;
			const size_t i = sparseMat[k].i;
			const size_t j = sparseMat[k].j;
			const float di = val * r[j];
			const float dj = val * r[i];
			rSqr1 += r[i] * di + r[j] * dj;
		}
		
		float beta = rSqr1 / rSqr;
		
		rSqr = rSqr1;
		float delta = 0;
		float zSqr = 0;
		for (size_t i = 0; i < n; i++)
		{
			d[i] = r[i] + beta * d[i];
			z[i] = diag[i] * d[i];
		}
		for (size_t k = 0; k < sparseMat.size(); k++)
		{
			const float val = sparseMat[k].val;
			const size_t i = sparseMat[k].i;
			const size_t j = sparseMat[k].j;
			const float di = val * d[j];
			const float dj = val * d[i];
			z[i] += di;
			z[j] += dj;
		}
		for (size_t i = 0; i < n; i++)
		{
			zSqr += z[i] * z[i];
			delta += r[i] * z[i];
		}

		alpha = delta / zSqr;

		// aplly lambdas to positions
		for (size_t i = 0; i < n; i++)
		{
			const Constraint& link = constraints[i];
			//if (link.type != Constraint::LINK && r[i] > 0)
				//continue;
			Vector2 disp = alpha * d[i] * normals[i];
			particles[link.i1].pos += particles[link.i1].invMass * disp;
			//if (link.type != Constraint::CONTACT)
				particles[link.i2].pos -= particles[link.i2].invMass * disp;
		}
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectPositionsMinRes1()
{
	std::vector<Vector2> normals(links.size());
	std::vector<float> lambda(links.size());
	std::vector<float> r(links.size());
	std::vector<float> p(links.size());
	std::vector<float> y(links.size());
	std::vector<float> z(links.size());
	std::vector<float> rhs(links.size());
	for (int iter = 0; iter < numIterations / 2 + 1; iter++)
	{
		for (size_t i = 0; i < links.size(); i++)
		{
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			Vector2 delta = particles[i2].pos - particles[i1].pos;
			rhs[i] = (float)delta.Length() - links[i].len;
			delta.Normalize();
			normals[i] = delta;
			lambda[i] = 0.f;
		}
		for (size_t i = 0; i < sparseMat.size(); i++)
		{
			sparseMat[i].dot = normals[sparseMat[i].i].Dot(normals[sparseMat[i].j]);
		}
		
		std::vector<float> r(links.size());
		// warm start with one GS iteration
		for (size_t i = 0; i < links.size(); i++)
		{
			const Constraint& link = links[i];
			Particle& p1 = particles[link.i1];
			Particle& p2 = particles[link.i2];
			Vector2 delta = p1.pos;
			delta.Subtract(p2.pos);
			float len = delta.Length();
			float dLen = (len - link.len) * 0.5f;
			float sum = 1.f / (p1.invMass + p2.invMass);
			lambda[i] = dLen * sum;
		}
		MultiplyBySparseMatrix(lambda, sparseMat, diag, r); // r = a * x
		for (size_t i = 0; i < links.size(); i++)
		{
			r[i] = rhs[i] - r[i];
		}

		// first MinRes iteration
		MultiplyBySparseMatrix(r, sparseMat, diag, z); // z = a * r
		float rSqr = r * z;
		float alpha = rSqr / (z * z);
		lambda = lambda + alpha * r;

#ifdef SECOND_ITERATION
		p = r;
		r = r - alpha * z;
		MultiplyBySparseMatrix(r, sparseMat, diag, y); //y = a * r;
		float rSqr1 = r * y;
		float beta = rSqr1 / rSqr;
		rSqr = rSqr1;
		p = r + beta * p;
		MultiplyBySparseMatrix(p, sparseMat, diag, z); //z = a * p;
		alpha = rSqr / (z * z);
		lambda = lambda + alpha * p;
#endif

		// aplly lambdas to positions
		for (size_t i = 0; i < links.size(); i++)
		{
			const Constraint& link = links[i];
			Vector2 disp = lambda[i] * normals[i];
			particles[link.i1].pos += particles[link.i1].invMass * disp;
			particles[link.i2].pos -= particles[link.i2].invMass * disp;
		}
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::IntegrateVerlet()
{
	PROFILE_SCOPE("IntegrateVerlet");
	//AddDampingForces();

	const float h2 = timeStep * timeStep;
	Vector2 force = h2 * lengthScale * gravity;
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;

		Vector2 prev = particles[i].pos;

		//const float vx = particles[i].pos.GetX() - particles[i].prevPos.GetX();
		//const float vy = particles[i].pos.GetY() - particles[i].prevPos.GetY();

		Vector2 velocity = particles[i].pos - particles[i].prevPos;
		//velocity.Set(vx, vy);
		particles[i].pos.Add(velocity);

		particles[i].pos.Add(force);

		particles[i].prevPos = prev;
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::StoreNormalsForShake()
{
	// compute and store initial normals (i.e. constraint directions)
	for (size_t i = 0; i < links.size(); i++)
	{
		Constraint& link = links[i];
		const ParticleVerlet& p1 = particles[link.i1];
		const ParticleVerlet& p2 = particles[link.i2];

		link.normal = p1.pos - p2.pos;
		link.depth = link.normal.Length() - link.len;
		link.normal.Normalize();
	}
}

// explcit constraint directions, nonlinear Gauss-Seidel solver
template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectExplicit()
{
	// Shake constraint solver - Gauss Seidel
	for (int iter = 0; iter < numIterations; iter++)
	{
		// go through all links
		for (size_t i = 0; i < links.size(); i++)
		{
			const Constraint& link = links[i];
			ParticleVerlet& p1 = particles[link.i1];
			ParticleVerlet& p2 = particles[link.i2];

			// compute constraint error and current normal
			Vector2 normal = p1.pos - p2.pos;
			float err = normal.Length() - link.len;
			normal.Normalize();

			// compute lambda (with h^2 absorbed)
			float dot = normal.Dot(link.normal);
			float mu = p1.invMass + p2.invMass;
			float lambda = err / mu / dot;

			// update positions
			p1.pos -= p1.invMass * lambda * link.normal;
			p2.pos += p2.invMass * lambda * link.normal;
		}
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::BuildMatrixShake()
{
	if (diag.empty())
	{
		BuildIncidenceMatrix();
	}

	// build matrix
	const size_t nl = links.size();
	std::vector<Vector2> n(nl); // current normals
	for (size_t i = 0; i < nl; i++)
	{
		n[i] = (particles[links[i].i2].pos - particles[links[i].i1].pos);
		n[i].Normalize();
		mat.Get(i, i) = n[i].Dot(links[i].normal) * diag[i];
		// the value diag[i] is not correct as it should be scaled by the above dot product
	}

	// update dots between normals
	for (size_t k = 0; k < sparseMat.size(); k++)
	{
		int i = sparseMat[k].i;
		int j = sparseMat[k].j;
		sparseMat[k].dot = n[i].Dot(links[j].normal);
		float val = sparseMat[k].a * sparseMat[k].dot;
		mat.Get(i, j) = val;
		mat.Get(j, i) = val;
	}
}

// position projection, Shake/explicit - Newton solver
template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNewton()
{
	const size_t numLinks = links.size();

	std::vector<float> residual(numLinks); // the position residual
	std::vector<float> a(numLinks), b(numLinks), c(numLinks); // the 3 diagonal bands
	std::vector<float> lambda(numLinks); // the unknown Lagrange multipliers

	for (int iter = 0; iter < numIterations; iter++)
	{
		// update the matrix
		BuildMatrixShake();

		// compute the residual
		for (size_t i = 0; i < numLinks; i++)
		{
			const Constraint& link = links[i];
			ParticleVerlet& p1 = particles[link.i1];
			ParticleVerlet& p2 = particles[link.i2];

			// compute constraint error
			Vector2 normal = p1.pos - p2.pos;
			residual[i] = normal.Length() - link.len;
		}

		// tridiagonal linear system solver - for ropes only!!!
		mat.GetTridiagonal(a, b, c);
		SolveTridiagonal(a, b, c, residual, lambda);

		// update positions - apply lambdas
		for (size_t i = 0; i < numLinks; i++)
		{
			Constraint& link = links[i];
			Vector2 impulse = lambda[i] * links[i].normal; // explicit constraint direction
			particles[link.i1].pos += particles[link.i1].invMass * impulse;
			particles[link.i2].pos -= particles[link.i2].invMass * impulse;
		}
	}
}

#include <Demo/Particles/ParticleSystemIterative.hpp>

template class ParticleSystem<ParticleVerlet>;
template class ParticleSystem<ParticleVelocity>;
template class ParticleSystem<ParticleAcceleration>;
template class ParticleSystem<ParticleFluid>;
