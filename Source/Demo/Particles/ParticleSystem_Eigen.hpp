#include <Eigen/Dense>

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::AnalyzeEigenValues()
{
	// TODO: sparse matrix
	size_t n = constraints.size();
	Eigen::MatrixXf A(n, n);
	// fill in the matrix
	for (size_t i = 0; i < n; i++)
	{
		for (size_t j = 0; j < n; j++)
		{
			A(i, j) = (float)mat.Get(i, j);
		}
	}
	// eigen decomposition
	Eigen::SelfAdjointEigenSolver<Eigen::MatrixXf> eigensolver(A);

	if (eigensolver.info() != Eigen::Success)
	{
		Printf("Eigen solver failed\n");
		return;
	}
	Eigen::VectorXf eigenVal = eigensolver.eigenvalues();
	bool posDef = true;
	bool posSemiDef = true;
	for (size_t i = 0; i < n; i++)
	{
		if (eigenVal(i) <= 0)
		{
			posDef = false;
			if (eigenVal(i) < 0)
			{
				posSemiDef = false;
			}
		}
		//Printf("%f ", eigenVal(i));
	}
	//Printf("\n");
	//if (posDef)
	//	Printf("Matrix is PD\n");
	//else if (posSemiDef)
	//	Printf("Matrix is PSD\n");
	//else
	//	Printf("Matrix is indefinite\n");
	//Printf("Condition number is %f\n", eigenVal(n - 1) / eigenVal(0));
	Printf("Spectral radius is %f\n", eigenVal(n - 1));
}