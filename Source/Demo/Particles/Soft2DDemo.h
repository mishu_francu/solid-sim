#ifndef SOFT2D_DEMO_H
#define SOFT2D_DEMO_H

#include <memory>

class Graphics2D;
namespace MBD
{
	class MultibodySystem;
}

class Soft2DDemo
{
public:
	enum BodyType
	{
		BT_TRIANGLE,
		BT_QUAD,
		BT_GRID,
	};

public:
	Soft2DDemo();
	~Soft2DDemo();
	void Create() {}
	void Init(float width, float height);
	void Update();
	void Draw(Graphics2D* graphics, int color);

private:
	std::unique_ptr<MBD::MultibodySystem> mMultibodySystem;
	bool mUseFEM;
	int mBodyType;
};

#endif // SOFT2D_DEMO_H
