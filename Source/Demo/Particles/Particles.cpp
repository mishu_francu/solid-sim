#include <Demo/Particles/Particles.h>
#include <Graphics2D/Graphics2D.h>
#include <Graphics3D/Graphics3D.h>
#include <Engine/Profiler.h>
#include <Engine/xml.h>
#include <Geometry/Assets.h>

#include <Demo/Particles/ParticleSystemVerlet.h>
#include <Demo/Particles/ParticleSystemPbd.h>
#include <Demo/Particles/ParticleSystemSeqImp.h>
#include <Demo/Particles/ParticleSystemSeqPos.h>
#include <Demo/Particles/ParticleSystemPenalty.h>
#include <Demo/Particles/ParticleSystemShake.h>
#include <Demo/Particles/ParticleSystemRattle.h>
#include <Demo/Particles/ParticleSystemSpook.h>
#include <Demo/Particles/ParticleSystemAcc.h>
#include <Demo/Particles/ParticleSystemSph.h>
#include <Demo/Particles/ParticleSystemGravity.h>
#include <Demo/Particles/ParticleSystemNonlinear.h>
#include <Demo/Particles/ParticleSystemImpVel.h>
#include <Demo/Particles/Rigid.h>
#include <Demo/Particles/3D/FemSystem.h>
#include <Physics/RigidSystem.h>

#include <Demo/Particles/3D/ClothDemo.h>

#include <iterator>

bool ParticlesDemo::noDraw = false;
int ParticlesDemo::maxFrames = -1;
bool ParticlesDemo::bAbcExport = false;
bool ParticlesDemo::bAutoStart = false;

#if (RENDERER == OPENGLES)
#	define NUM_PARTICLES 200
#	define RADIUS 10.f
#else
const float RADIUS = 29.f;
const int NUM_PARTICLES = 100;
#endif

ParticlesDemo::ParticlesDemo() : 
	replayMode(false),
	currFrame(0),
	drawVel(false),
	pausePhysics(!bAutoStart),
	mouseLinkId(-1),
	numParticles(NUM_PARTICLES),
	radius(2),
	divisions(10),
	scene(NULL),
	stepByStep(false),
	nFrames(0),
	drip(false),
	mShowDebug(false),
	wireframe(false),
	mDebugDrawFlags(0),
	mClothDemo(new ClothDemo())
{
	initFn = NULL;

	drawFps = true;
	graphics->SetBgColor(0xff9f9f9f);
	canResize = true;
	//maximized = true;

	drawProfiler = bAutoStart;

	// read first if we are in 3D mode
	XML* config = new XML("../Res/particles.xml");
	if(config->IntegrityTest() || config->ParseStatus() == 0)
	{
		XMLElement* root = config->GetRootElement();
		XMLVariable* x3d = root->FindVariableZ("3d");
		if (x3d)
		{
			is3D = x3d->GetValueInt() != 0;
		}
	}
	delete config;
}

ParticlesDemo::~ParticlesDemo() 
{
}

void ParticlesDemo::SetIterations(int val)
{
	if (is3D)
		phys3D.SetNumIterations(val);
}

void ParticlesDemo::SetDemoFromString(const char* str)
{
	if (strcmp(str, "cloth+particles") == 0)
	{
		type = DEMO_CLOTH_PARTICLES;
		initFn = &ParticlesDemo::InitClothParticles3D;
		// defaults
		divisions = 20;
		numParticles = 100;
		phys3D.SetNumIterations(30);
		phys3D.SetSolver(PhysicsSystem::GAUSS_SEIDEL);
	}
	else if (strnicmp(str, "cloth", 5) == 0)
	{
		type = DEMO_CLOTH;
		initFn = is3D ? &ParticlesDemo::InitCloth3D : &ParticlesDemo::InitParticles;

		if (is3D)
		{
			if (strcmp(str, "Cloth sphere") == 0)
			{
				mClothDemo->Create(ClothDemo::CLOTH_DEMO_SPHERE);
			}
			else if (strcmp(str, "Cloth on mesh") == 0)
			{
				mClothDemo->Create(ClothDemo::CLOTH_DEMO_MESH);
			}
			else if (strcmp(str, "Cloth self") == 0)
			{
				mClothDemo->Create(ClothDemo::CLOTH_DEMO_SELF);
			}
			else
			{
				mClothDemo->Create(ClothDemo::CLOTH_DEMO_DEFAULT);
			}
		}
		else
		{
			mParticles2DDemo.Create(Particles2DDemo::DEMO_CLOTH);
		}
	}
	else if (strcmp(str, "particles") == 0)
	{
		type = DEMO_PARTICLES;
		initFn = is3D ? &ParticlesDemo::InitParticles3D : &ParticlesDemo::InitParticles;		
		if (is3D)
		{
			// defaults
			radius = 15;
			divisions = -1;
			numParticles = 1000;
			phys3D.SetNumIterations(10);
			phys3D.SetMethod(PhysicsSystem3D::METHOD_SEQ_IMP);
			phys3D.SetSolver(PhysicsSystem::GAUSS_SEIDEL);
		}
		else
		{
			mParticles2DDemo.Create(Particles2DDemo::DEMO_PARTICLES);
		}
	}
	else if (strcmp(str, "hourglass") == 0)
	{
		type = DEMO_HOURGLASS;
		initFn = &ParticlesDemo::InitParticles3D;
		divisions = 12;
		phys3D.SetWallBottom(0);

		if (LoadMesh("../Models/hourglass_interior.obj", *phys3D.GetMesh(), Vector3()))
		{
			phys3D.InitMeshGrid();
		}
	}
	else if (strcmp(str, "links") == 0)
	{
		type = DEMO_LINKS;
		initFn = is3D ? &ParticlesDemo::InitLinks3D : &ParticlesDemo::InitParticles;
		if (!is3D)
		{
			mParticles2DDemo.Create(Particles2DDemo::DEMO_LINKS);
		}
	}
	else if (strcmp(str, "lattice") == 0 && !is3D)
	{
		type = DEMO_LATTICE;
		initFn = &ParticlesDemo::InitParticles;
		mParticles2DDemo.Create(Particles2DDemo::DEMO_LATTICE);
	}
	else if (strcmp(str, "fem") == 0)
	{
		type = DEMO_FEM;
		if (is3D)
			initFn = &ParticlesDemo::InitFem3D;
		else
		{
			mSoft2DDemo.Create();
			initFn = &ParticlesDemo::InitSoft2D;
		}
	}
	else if (strcmp(str, "rigid") == 0 || strcmp(str, "Rigid") == 0)
	{
		type = DEMO_RIGID;
		if (is3D)
			mRigid3DDemo.Create();
		initFn = is3D ? &ParticlesDemo::InitRigid3D : &ParticlesDemo::InitRigid;
	}
	else if (strcmp(str, "unified") == 0 && is3D)
	{
		type = DEMO_UNIFIED;
		initFn = &ParticlesDemo::InitUnified;
		// defaults
		numParticles = 10000;
		radius = 1.5f;
	}
	else if (strcmp(str, "pendulum") == 0)
	{
		type = DEMO_PENDULUM;
		initFn = &ParticlesDemo::InitPendulum;
	}
}

void ParticlesDemo::LoadConfig()
{
	XML* config = new XML("../Res/particles.xml");
	if(config->IntegrityTest() || config->ParseStatus() == 0)
	{
		char str[32];
		XMLElement* root = config->GetRootElement();
		
		XMLVariable* xType = root->FindVariableZ("type");
		if (xType)
		{
			xType->GetValue(str);
			SetDemoFromString(str);
			if (type == DEMO_CLOTH)
			{
				XMLElement* xCloth = root->FindElementZ("Cloth");
				if (xCloth != NULL)
				{
					if (is3D)
					{
						if (!mClothDemo->LoadFromXml(xCloth))
							Engine::getInstance()->Quit();
						root = NULL; // skip the rest
					}
					else
					{
						mParticles2DDemo.LoadFromXml(xCloth);
					}
				}
			}
			else if (type == DEMO_CLOTH_PARTICLES)
			{
				XMLElement* xCloth = root->FindElementZ("ClothParticles");
				if (xCloth)
				{
					XMLVariable* xNum = xCloth->FindVariableZ("divisions");
					if (xNum)
					{
						divisions = xNum->GetValueInt();
					}
					xNum = xCloth->FindVariableZ("num");
					if (xNum)
					{
						numParticles = xNum->GetValueInt();
					}
				}
				root = xCloth; //!
			}
			else if (type == DEMO_PARTICLES) 
			{
				XMLElement* xParticles = root->FindElementZ("Particles");
				if (xParticles)
				{
					if (!is3D)
					{
						mParticles2DDemo.LoadFromXml(xParticles);
					}
					else
					{
						XMLVariable* xNum = xParticles->FindVariableZ("num");
						if (xNum)
						{
							numParticles = xNum->GetValueInt();
						}
						XMLVariable* xDrip = xParticles->FindVariableZ("drip");
						if (xDrip)
						{
							drip = xDrip->GetValueInt() != 0;
						}
						XMLVariable* xMesh = xParticles->FindVariableZ("mesh");
						if (xMesh)
						{
							xMesh->GetValue(str);
							LoadMesh(str, *phys3D.GetMesh(), Vector3());
							phys3D.InitMeshGrid();
						}
						XMLVariable* xOffset = xParticles->FindVariableZ("offset");
						if (xOffset)
						{
							xOffset->GetValue(str);
							float offset = (float)atof(str);
							phys3D.SetMeshOffset(offset);
						}
						XMLVariable* xDiv = xParticles->FindVariableZ("divisions");
						if (xDiv)
						{
							divisions = xDiv->GetValueInt();
						}
						root = xParticles; //!
					}
				}
			}
			else if (type == DEMO_HOURGLASS) 
			{				
				XMLElement* xParticles = root->FindElementZ("Hourglass");
				if (xParticles)
				{
					XMLVariable* xNum = xParticles->FindVariableZ("num");
					if (xNum)
					{
						numParticles = xNum->GetValueInt();
					}
					XMLVariable* xDrip = xParticles->FindVariableZ("drip");
					if (xDrip)
					{
						drip = xDrip->GetValueInt() != 0;
					}
				}
				root = xParticles; //!
			}
			else if (type == DEMO_LINKS)
			{				
				XMLElement* xLinks = root->FindElementZ("Links");
				if (xLinks)
				{
					mParticles2DDemo.LoadFromXml(xLinks);
				}
			}
			else if (type == DEMO_LATTICE)
			{
				XMLElement* xLattice = root->FindElementZ("Lattice");
				if (xLattice)
					mParticles2DDemo.LoadFromXml(xLattice);
			}
			else if (type == DEMO_UNIFIED)
			{
				XMLElement* xUnified = root->FindElementZ("Unified");
				if (xUnified)
				{
					mUnified.LoadFromConfig(xUnified);
				}
			}
		}

		if (root != NULL)
		{		
			XMLVariable* xRadius = root->FindVariableZ("radius");
			if (xRadius)
			{
				xRadius->GetValue(str);
				SetRadius((float)atof(str));
			}

			XMLVariable* xMethod = root->FindVariableZ("method");
			if (xMethod)
			{
				xMethod->GetValue(str);
				if (strcmp(str, "pbd") == 0)
				{
					phys3D.SetMethod(PhysicsSystem3D::METHOD_PBD);
				}
				else if (strcmp(str, "si") == 0)
				{
					phys3D.SetMethod(PhysicsSystem3D::METHOD_SEQ_IMP);
				}
				else if (strcmp(str, "sp") == 0)
				{
					phys3D.SetMethod(PhysicsSystem3D::METHOD_SEQ_POS);
				}
				else if (strcmp(str, "si-ps") == 0)
				{
					phys3D.SetMethod(PhysicsSystem3D::METHOD_SEQ_IMP_POST);
				}
				else if (strcmp(str, "ivp") == 0)
				{
					phys3D.SetMethod(PhysicsSystem3D::METHOD_IVP);
				}
				else
				{
					Printf("Unknown simulation method\n");
					Engine::getInstance()->Quit();
					return;
				}
			}

			XMLVariable* xNum = root->FindVariableZ("iterations");
			if (xNum)
			{
				SetIterations(xNum->GetValueInt());
			}

			xNum = root->FindVariableZ("iterations2");
			if (xNum && is3D)
			{
				phys3D.SetNumIterations2(xNum->GetValueInt());
			}

			XMLVariable* xSolver = root->FindVariableZ("solver");
			if (xSolver)
			{
				xSolver->GetValue(str);
				if (strcmp(str, "gs") == 0)
				{
					phys3D.SetSolver(PhysicsSystem::GAUSS_SEIDEL);
				}
				if (strcmp(str, "jacobi") == 0)
				{
					phys3D.SetSolver(PhysicsSystem::JACOBI);
				}
				else if (strcmp(str, "mr") == 0)
				{
					phys3D.SetSolver(PhysicsSystem::MIN_RES);
				}
				else if (strcmp(str, "cr") == 0)
				{
					phys3D.SetSolver(PhysicsSystem::CONJ_RES);
				}
				else if (strcmp(str, "exact") == 0)
				{
					phys3D.SetSolver(PhysicsSystem::EXACT);
				}
				else if (strcmp(str, "jcr") == 0)
				{
					phys3D.SetSolver(PhysicsSystem::JACOBI_CR);
				}
				else if (strcmp(str, "soft") == 0)
				{
					phys3D.SetSolver(PhysicsSystem::SOFT_CONSTR);
				}
			}

			XMLVariable* xSteps = root->FindVariableZ("steps");
			if (xSteps)
			{
				int steps = xSteps->GetValueInt();
				phys3D.SetNumSteps(steps);
			}
			XMLVariable* xMu = root->FindVariableZ("mu");
			if (xMu)
			{
				xMu->GetValue(str);
				float mu = (float)atof(str);
				phys3D.SetFriction(mu);
			}
			XMLVariable* xTol = root->FindVariableZ("tolerance");
			if (xTol)
			{
				xTol->GetValue(str);
				float tol = (float)atof(str);
				phys3D.SetTolerance(tol);
			}
			XMLVariable* xBeta = root->FindVariableZ("beta");
			if (xBeta)
			{
				xBeta->GetValue(str);
				float beta = (float)atof(str);
				phys3D.SetBaumgarte(beta);
			}
		}
	}
	delete config;

	if (initFn == NULL)
	{
		Printf("No init function set, defaulting to particles.\n");
		type = DEMO_PARTICLES;
		initFn = is3D ? &ParticlesDemo::InitParticles3D : &ParticlesDemo::InitParticles;
	}
}

void ParticlesDemo::OnCreate()
{
	//testCuda();
	//LoadMeshFromOgreXml("avatar_anim.mesh.xml", bunny);
	
	//SkelUtils::LoadFromXml("avatar_anim.skeleton.xml", skel);

	mParticles2DDemo.Test();

	LoadConfig();
	if (is3D)
	{		
		graphics3D->camera.SetPosition(Vector3(0, 30, 70));
		graphics3D->SetLightPos(Vector3(400, 800, 0));
	}
	else
	{
		mParticles2DDemo.SetMaxFrames(maxFrames);
	}
	Reset();
}

template<typename ContactVector>
void ParticlesDemo::DrawWallNormals(const ContactVector& walls) const
{
	// wall normals debug draw
	graphics->SetColor(0xffff0000);
	for (size_t i = 0; i < walls.size(); i++) {
		graphics->DrawLine(walls[i].point.GetX(), walls[i].point.GetY(), 
			walls[i].point.GetX() + walls[i].normal.GetX() * 10, 
			walls[i].point.GetY() + walls[i].normal.GetY() * 10);
	}
}

void ParticlesDemo::InitParticles()
{
	mParticles2DDemo.Init(getScreenWidth(), getScreenHeight());
}

void ParticlesDemo::InitSoft2D()
{
	mSoft2DDemo.Init(getScreenWidth(), getScreenHeight());
}

void ParticlesDemo::InitPendulum()
{

}

void ParticlesDemo::Reset()
{
	pausePhysics = true;
	if (initFn)
		(this->*initFn)();
}

void ParticlesDemo::OnUpdate(float dt)
{
	if (isKeyPressed('X'))
		drawProfiler = !drawProfiler;
	if (isKeyPressed('R'))
	{
		Reset();
	}
	if (isKeyPressed('T'))
		pausePhysics = !pausePhysics;
	if (isKeyPressed('Y'))
		stepByStep= !stepByStep;
#ifndef ANDROID_NDK
	if (type == DEMO_HOURGLASS && isKeyPressed('U'))
		phys3D.SetWallBottom(PhysicsSystem3D::WALL_BOTTOM);
#endif
	if (isKeyPressed('L'))
	{
		wireframe = !wireframe;
	}

	if (type == DEMO_UNIFIED)
	{
		if (!pausePhysics)
		{
			mUnified.Step();
			pausePhysics = stepByStep;
		}
		return;
	}

	if (type == DEMO_FEM)
	{
		if (!pausePhysics)
		{
			if (is3D)
				mFEMDemo.Update(0.016f);
			else
				mSoft2DDemo.Update();
			pausePhysics = stepByStep;
		}
		return;
	}
}

//#define ONE_RIGID

void ParticlesDemo::InitRigid()
{
	rigidSys.reset(new RigidSystem);
#ifdef ONE_RIGID
	Rigid rigid;
	rigid.pos.Set(getScreenWidth() * 0.5f, getScreenHeight() * 0.5f);
	rigid.theta = 0.1f;
	rigid.omega = 1.f;
	rigid.vel.Set(20.f, 0.f);
	rigidSys->AddRigid(rigid);
#else
	float x = 140.f;
	float y = 150.f;
	for (size_t i = 0; i < 40; i++)
	{
		Rigid rigid;
		if (i & 1)
			rigid.shape = std::make_shared<Capsule>(Capsule(30, 20));
		else
			rigid.shape = std::make_shared<Sphere>(Sphere((float)(20 + rand() % 10)));
		//rigid.pos.Set(getScreenWidth() * clamp(GetRandomReal01(), 0.2f, 0.8f), getScreenHeight() * clamp(GetRandomReal01(), 0.2f, 0.8f));
		if (x > getScreenWidth() - 150.f)
		{
			y += 65.f;
			x = 140.f;
		}
		rigid.pos.Set(x, y);
		x += 100.f;
		//rigid.theta = GetRandomReal01() * 0.7f;
		//rigid.omega = GetRandomReal01();
		rigid.vel.Set(10.f * GetRandomReal01(), GetRandomReal01());
		rigidSys->AddRigid(rigid);
	}
#endif
}

void ParticlesDemo::DrawRigid()
{
	graphics->SetColor(0xff00007f);

	for (size_t i = 0; i < rigidSys->GetNumRigids(); i++)
	{
		const Vector2& pos = rigidSys->GetRigid(i).pos;
		graphics->Translate(pos.GetX(), pos.GetY());
		//graphics->DrawFormattedString(0, 0, "%d", i);
		graphics->Rotate(DEGREE(rigidSys->GetRigid(i).theta));

		const Shape* shape = (const Shape*)rigidSys->GetRigid(i).shape.get();
		if (shape->type == Shape::CAPSULE)
		{
			// capsule
			const Capsule* cap = (const Capsule*)shape;
			const float r = cap->r;
			const float hh = cap->hh;
			//graphics->DrawCircle(-hh, 0, r);
			//graphics->DrawCircle(hh, 0, r);
			//graphics->DrawLine(-hh, r, hh, r);
			//graphics->DrawLine(-hh, -r, hh, -r);
			graphics->FillCircle(-hh, 0, r);
			graphics->FillCircle(hh, 0, r);
			graphics->FillRect(-hh, -r, 2 * hh, 2 * r);
		}
		else if (shape->type == Shape::SPHERE)
		{
			// circle
			const Sphere* sph = (const Sphere*)shape;
			graphics->DrawCircle(0, 0, sph->r);
			graphics->DrawLine(0, 0, sph->r, 0);
			graphics->DrawLine(0, 0, 0, sph->r);
		}

		// box
		//Vector2 extent(radius, radius);
		//Vector2 p = -1.f * extent; // TODO: minus operator
		//Vector2 size = 2.f * extent;
		//graphics->DrawRect(p.GetX(), p.GetY(), size.GetX(), size.GetY());

		graphics->ResetTransform();
	}

	if (!pausePhysics)
	{
		rigidSys->Step(0.016f);
		pausePhysics = stepByStep;
	}

	if ((mDebugDrawFlags & DDF_CONTACTS) && rigidSys->GetNumContacts() != 0)
	{
		for (size_t i = 0; i < rigidSys->GetNumContacts(); i++)
		{
			const RigidSystem::Contact& contact = rigidSys->GetContact(i);
			Vector2 q = contact.q;
			int j = contact.idx1;
			q.Rotate(rigidSys->GetRigid(j).theta);
			q += rigidSys->GetRigid(j).pos;

			graphics->SetColor(0xff00ff00);
			graphics->DrawLine(q, q + contact.n * 10);

			if (contact.idx2 >= 0)
			{
				Vector2 p = contact.p;
				j = contact.idx2;
				p.Rotate(rigidSys->GetRigid(j).theta);
				p += rigidSys->GetRigid(j).pos;

				graphics->SetColor(0xffff0000);
				graphics->DrawLine(p, p - contact.n * 10);

				graphics->SetColor(0xffffffff);
				graphics->DrawLine(p, q);

				//pausePhysics = true;
			}
			//pausePhysics = true;
		}
	}
}

void ParticlesDemo::OnDraw()
{
	//PROFILE_SCOPE("Draw2D");
	
	if (is3D)
		return;
	if (rigidSys)
	{
		DrawRigid();
		return;
	}
	if (type == DEMO_FEM)
	{
		mSoft2DDemo.Draw(graphics, 0xffffffff);
		return;
	}
	
	if (type != DEMO_PENDULUM)
	{
		if (isKeyPressed('P'))
			replayMode = !replayMode;
		//if ((isKeyDown(VK_OEM_4) || isKeyPressed('U')) && currFrame > 0 && replayMode)
			//currFrame--;
		//if ((isKeyDown(VK_OEM_6) || isKeyPressed('I')) && currFrame < particleSys->frames.size() - 1 && replayMode)
		//	currFrame++;
		if (isKeyPressed('O'))
			drawVel = !drawVel;
	
		// paint particle system
		mParticles2DDemo.Draw(graphics, 0xffffffff);
	}
	else
	{
		mPendulumDemo.Draw(graphics);
	}

	if (!replayMode && !pausePhysics) 
	{
		BEGIN_PROFILE("Physics");
		if (type != DEMO_PENDULUM)
			mParticles2DDemo.Update();
		else
			mPendulumDemo.Update();
		END_PROFILE();

		pausePhysics = stepByStep;
	}
}

Vector2 InterpolateHermite(float u, const Vector2& p0, const Vector2& p1, const Vector2& t0, const Vector2& t1)
{
	float u2 = u * u;
	float u3 = u2 * u;
	float f1 = 2 * u3 - 3 * u2 + 1;
	float f2 = - 2 * u3 + 3 * u2;
	float f3 = u3 - 2 * u2 + u;
	float f4 = u3 - u2;
	return f1 * p0 + f2 * p1 + f3 * t0 + f4 * t1;
}

#ifdef DISABLED_WIN32
// application entry point
int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	std::wstring cmdLine(lpCmdLine);
	std::wistringstream iss(lpCmdLine);
	std::vector<std::wstring> tokens;
	std::copy(
		std::istream_iterator<std::wstring, wchar_t, std::char_traits<wchar_t> >(iss),
		std::istream_iterator<std::wstring, wchar_t, std::char_traits<wchar_t> >(),
		std::back_inserter(tokens));
	for (size_t i = 0; i < tokens.size(); i++)
	{
		if (tokens[i][0] != '-')
			continue;
		if (tokens[i].substr(1, 6).compare(L"nodraw") == 0)
		{
			ParticlesDemo::noDraw = true;
		}
		if (tokens[i][1] == 'f')
		{
			ParticlesDemo::maxFrames = std::stoi(tokens[i].substr(2));
		}
		if (tokens[i].substr(1, 3).compare(L"abc") == 0)
		{
			ParticlesDemo::bAbcExport = true;
		}
		if (tokens[i].substr(1, 5).compare(L"start") == 0)
		{
			ParticlesDemo::bAutoStart = true;
		}
	}

	RUN_ENGINE(ParticlesDemo, hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}

#endif // DISABLED_WIN32

#ifdef PARTICLES_EXPORTS
#include "ParticlesDll.h"

void RunParticles()
{
	ParticlesDemo engine;
	engine.Main(NULL, NULL, NULL, SW_SHOWDEFAULT);
}

static ParticlesDemo demo;

void RunParticlesAsChild(/*HINSTANCE hInstance, */HWND hParent)
{
	// TODO: leak!!!
	//ParticlesDemo* engine = new ParticlesDemo;
	//engine->CreateAsChild(NULL, hParent, 0, 0, 100, 100);
	demo.CreateAsChild(NULL, hParent, 0, 0, 500, 500);
}

void DrawParticles()
{
	demo.Draw();
}

#endif

