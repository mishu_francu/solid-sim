#include <Engine/Platform.h>
#include <tchar.h>
#include <Engine/Engine.h>
#include "Particles.h"
#include <Physics/OpenCL.h>

#ifndef DISABLED_WIN32
#include <CommCtrl.h>
#include <windowsx.h>

#pragma comment(linker, \
	"\"/manifestdependency:type='Win32' "\
	"name='Microsoft.Windows.Common-Controls' "\
	"version='6.0.0.0' "\
	"processorArchitecture='*' "\
	"publicKeyToken='6595b64144ccf1df' "\
	"language='*'\"")

#pragma comment(lib, "ComCtl32.lib")

#define IDC_RESET_BUTTON	101
#define IDC_PLAY_BUTTON		102
#define IDC_DIV_SPIN		103
#define IDC_ITER_SPIN		104
#define IDC_DEMO_COMBO		105
#define IDC_PLATFORM_COMBO	106
#define IDC_DIV_EDIT		107
#define IDC_PLATFORMCL_COMBO 108
#define IDC_PARTICLES_EDIT	109
#define IDC_PARTICLES_SPIN	110
#define IDC_RADIUS_EDIT		111
#define IDC_DEVICE_COMBO	112
#define IDC_METHOD_COMBO	113
#define IDC_SOLVER_COMBO	114
#define IDC_BAUMGARTE_EDIT	115
#define IDC_SHOW_CHECK		116
#define IDC_ITER_EDIT		117
#define IDC_BEND_EDIT		118
#define IDC_SHEAR_EDIT		119
#define IDC_DIHEDRAL_CHECK	120
#define IDC_STIFF_EDIT		121
#define IDC_MU_EDIT			122
#define IDC_FEM_CHECK		123
#define IDC_CUBIC_CHECK		124
#define IDC_YOUNG_EDIT		125
#define IDC_POISSON_EDIT	126
#define IDC_MODEL_COMBO		127
#define IDC_PARTICLES_CHECK	128
#define IDC_AREA_EDIT		129
#define IDC_UNIT_EDIT		130
#define IDC_ALPHA_EDIT		131

//#define USE_DIALOG

const int strLen = 64;
const TCHAR platforms[][strLen] =  
{
	TEXT("CPU ST"), TEXT("OpenCL")
};
TCHAR demos[][strLen] =  
{
	TEXT("Cloth"), TEXT("Cloth sphere"), TEXT("Cloth on mesh"), TEXT("Cloth self"), TEXT("rigid"), TEXT("particles"), TEXT("fem"), //TEXT("hourglass")
};
const TCHAR models[][strLen] =  
{
	TEXT("Default"), TEXT("Patch"), TEXT("Katja"), TEXT("MD")
};
TCHAR methods[][strLen] =  
{
	TEXT("PBD"), TEXT("Springs"), TEXT("Soft constraints"), TEXT("Newmark projection"), TEXT("OpenCloth"), TEXT("Sequential Impulses"), //TEXT("Sequential Positions"), 
};
TCHAR solvers[][strLen] =  
{
	TEXT("Jacobi"), TEXT("Gauss Seidel"), TEXT("Improved Jacobi"), TEXT("Implicit"), TEXT("Explicit")
};

void ErrorExit(LPTSTR lpszFunction);

#ifndef USE_DIALOG

// TODO: class
static ParticlesDemo demo;
HINSTANCE hInst;
HWND hMainWnd;
HFONT hFont;

bool creating = true;

struct Control
{
	HWND hWnd;
	int x, y, w, h; // TODO: GetWindowRect
	bool isSpin;

	Control(HWND ahWnd, bool spin) : hWnd(ahWnd), x(0), y(0), w(0), h(0), isSpin(spin) { }
	Control(HWND ahWnd, int ax, int ay, int aw, int ah) : hWnd(ahWnd), x(ax), y(ay), w(aw), h(ah), isSpin(false) { }
	void Resize(int leftX, int topY)
	{
		if (isSpin)
		{
			HWND hBuddy = (HWND)SendMessage(hWnd, UDM_GETBUDDY, 0, 0);
			SendMessage(hWnd, UDM_SETBUDDY, (WPARAM)hBuddy, 0);
		}
		else
			SetWindowPos(hWnd, NULL, leftX + x, topY + y, w, h, 0);	
	}
};

std::vector<Control> controls;

inline HWND CreateStatic(const TCHAR* text, int x, int y, int w, int h, HWND hWnd)
{
	HWND hControl = CreateWindowEx(0, L"STATIC", text, WS_CHILD | WS_VISIBLE, x, y, w, h, hWnd, NULL, hInst, NULL);
	SendMessage(hControl, WM_SETFONT, (WPARAM)hFont, 0);
	controls.push_back(Control(hControl, x, y, w, h));
	return hControl;
}

inline HWND CreateButton(int id, const TCHAR* text, int x, int y, int w, int h, HWND hWnd)
{
	HWND hControl = CreateWindowEx(NULL, 
		L"BUTTON",
		text,
		WS_TABSTOP|WS_VISIBLE|WS_CHILD,
		x,
		y,
		w,
		h,
		hWnd,
		(HMENU)id,
		GetModuleHandle(NULL), // TODO: hInst?
		NULL);
	SendMessage(hControl, WM_SETFONT, (WPARAM)hFont, 0);
	controls.push_back(Control(hControl, x, y, w, h));
	return hControl;
}

inline HWND CreateEdit(int id, int x, int y, int w, int h, HWND hWnd, int style = 0)
{
	HWND hControl = CreateWindowEx(WS_EX_LEFT | WS_EX_CLIENTEDGE | WS_EX_CONTEXTHELP,    //Extended window styles.
		WC_EDIT,
		NULL,
		WS_CHILDWINDOW | WS_VISIBLE | WS_BORDER    // Window styles.
		| style | ES_LEFT,                     // Edit control styles.
		x, y, w, h,
		hWnd,
		(HMENU)id,
		hInst,
		NULL);
	SendMessage(hControl, WM_SETFONT, (WPARAM)hFont, 0);
	controls.push_back(Control(hControl, x, y, w, h));
	return hControl;
}

inline HWND CreateCombo(int id, int x, int y, int w, int h, HWND hWnd)
{
	HWND hControl = CreateWindow(WC_COMBOBOX, TEXT(""), 
		CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE | WS_VSCROLL,
		x, y, w, h, hWnd, (HMENU)id, hInst, NULL);
	SendMessage(hControl, WM_SETFONT, (WPARAM)hFont, 0);
	controls.push_back(Control(hControl, x, y, w, h));
	return hControl;
}

inline HWND CreateSpin(int id, HWND hWnd)
{
	HWND hControl = CreateWindowEx(WS_EX_LEFT | WS_EX_LTRREADING,
		UPDOWN_CLASS,
		NULL,
		WS_CHILDWINDOW | WS_VISIBLE
		| UDS_AUTOBUDDY | UDS_SETBUDDYINT | UDS_ALIGNRIGHT | UDS_ARROWKEYS | UDS_HOTTRACK,
		0, 0,
		0, 0,         // Set to zero to automatically size to fit the buddy window.
		hWnd,
		(HMENU)id,
		hInst,
		NULL);
	controls.push_back(Control(hControl, true));
	return hControl;
}

inline HWND CreateCheck(int id, const TCHAR* text, int x, int y, int w, int h, HWND hWnd)
{
	HWND hControl = CreateWindow(L"BUTTON", text,
                 WS_VISIBLE | WS_CHILD | BS_AUTOCHECKBOX,
                 x, y, w, h,
                 hWnd, (HMENU)id, hInst, NULL);
	SendMessage(hControl, WM_SETFONT, (WPARAM)hFont, 0);
	controls.push_back(Control(hControl, x, y, w, h));
	return hControl;
}

HWND CreateGroupBox(HWND hwndParent, int x, int y, int w, int h)
{
    HWND hControl = CreateWindowEx(WS_EX_LEFT | WS_EX_CONTROLPARENT | WS_EX_LTRREADING,
                              WC_BUTTON,
                              NULL,
                              WS_CHILDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE | WS_GROUP | BS_GROUPBOX,
							  x, y, w, h,
                              hwndParent,
                              NULL,
                              hInst,
                              NULL);

    return (hControl);
}

void PopulateCombo(HWND hCombo, const TCHAR names[][strLen], int n)
{
	// load the combobox with item list.  
	// Send a CB_ADDSTRING message to load each item
	for (int k = 0; k < n; k++)
	{
		// Add string to combobox.
		SendMessage(hCombo, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)names[k]);
	}

	// Send the CB_SETCURSEL message to display an initial item 
	//  in the selection field  
	SendMessage(hCombo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
}

void PopulateCombo(HWND hCombo, const std::vector<std::string>& names)
{
	ComboBox_ResetContent(hCombo);
	// load the combobox with item list.  
	// Send a CB_ADDSTRING message to load each item
	for (size_t k = 0; k < names.size(); k++)
	{
		// Add string to combobox.
		SendMessageA(hCombo, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)names[k].c_str());
	}

	// Send the CB_SETCURSEL message to display an initial item 
	//  in the selection field  
	SendMessage(hCombo, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
}

void Refresh(HWND hWnd)
{
	wchar_t wstr[16];
	HWND hControl;
	hControl = GetDlgItem(hWnd, IDC_ITER_SPIN);
	SendMessage(hControl, UDM_SETPOS, 0, (LPARAM)demo.GetIterations());
	hControl = GetDlgItem(hWnd, IDC_METHOD_COMBO);
	SendMessage(hControl, CB_SETCURSEL, (WPARAM)demo.GetMethod(), (LPARAM)0);
	hControl = GetDlgItem(hWnd, IDC_SOLVER_COMBO);
	SendMessage(hControl, CB_SETCURSEL, (WPARAM)demo.GetSolver(), (LPARAM)0);

	hControl = GetDlgItem(hWnd, IDC_RADIUS_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetRadius());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	hControl = GetDlgItem(hWnd, IDC_PARTICLES_SPIN);
	SendMessage(hControl, UDM_SETPOS, 0, (LPARAM)demo.GetNumParticles());

	hControl = GetDlgItem(hWnd, IDC_DIV_SPIN);
	SendMessage(hControl, UDM_SETPOS, 0, (LPARAM)demo.GetDivisions());

	hControl = GetDlgItem(hWnd, IDC_DEVICE_COMBO);
	PopulateCombo(hControl, OpenCL::GetInstance()->GetDeviceNames());

	hControl = GetDlgItem(hWnd, IDC_BEND_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetBending());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	hControl = GetDlgItem(hWnd, IDC_SHEAR_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetShearing());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	hControl = GetDlgItem(hWnd, IDC_STIFF_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetCloth().GetModel().GetStiffness());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	hControl = GetDlgItem(hWnd, IDC_MU_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetCloth().GetModel().GetFriction());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	hControl = GetDlgItem(hWnd, IDC_AREA_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetCloth().GetModel().GetArea());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	hControl = GetDlgItem(hWnd, IDC_UNIT_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetCloth().GetModel().GetUnit());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	hControl = GetDlgItem(hWnd, IDC_ALPHA_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetCloth().GetModel().GetNewmarkAlpha());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	hControl = GetDlgItem(hWnd, IDC_POISSON_EDIT);
	swprintf_s(wstr, 16, L"%g", demo.GetCloth().GetModel().GetPoisson());
	SendMessage(hControl, WM_SETTEXT, 0, (LPARAM)wstr);

	CheckDlgButton(hWnd, IDC_DIHEDRAL_CHECK, demo.GetCloth().GetModel().GetDihedral() ? BST_CHECKED : BST_UNCHECKED);
	CheckDlgButton(hWnd, IDC_FEM_CHECK, demo.GetCloth().GetModel().GetFEM() ? BST_CHECKED : BST_UNCHECKED);
	CheckDlgButton(hWnd, IDC_CUBIC_CHECK, demo.GetCloth().GetBicubic() ? BST_CHECKED : BST_UNCHECKED);
}

void OnCreate(HWND hWnd)
{
	// set the correct font
	NONCLIENTMETRICS ncm;
	ncm.cbSize = sizeof(NONCLIENTMETRICS);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0);
	hFont = CreateFontIndirect(&ncm.lfMessageFont);
 
	RECT rect;
	GetClientRect(hWnd, &rect);
	int w = rect.right - rect.left;
	int h = rect.bottom - rect.top;
	int demoW = w - 300;
	demo.CreateAsChild(hInst, hWnd, 0, 0, demoW, h);

	const int topX = 10;
	int topY = 10;
	const int rowH = 30;
	// demo type
	HWND hStaticDemo = CreateStatic(L"Demo Type", topX, topY, 124, 25, hWnd);
	HWND hComboDemo = CreateCombo(IDC_DEMO_COMBO, topX + 130, topY, 150, 200, hWnd);
	PopulateCombo(hComboDemo, demos, sizeof(demos) / sizeof(demos[0]));

	// platform
	topY += rowH;
	HWND hStaticPlatform = CreateStatic(L"Platform", 10, topY, 124, 25, hWnd);
	HWND hPlatformCombo = CreateCombo(IDC_PLATFORM_COMBO, topX + 130, topY, 150, 200, hWnd);
	PopulateCombo(hPlatformCombo, platforms, sizeof(platforms) / sizeof(platforms[0]));

	// OpenCL platform
	topY += rowH;
	HWND hStaticPlatformCL = CreateStatic(L"OpenCL Platform", topX, topY, 124, 25, hWnd);
	HWND hComboPlatformCL = CreateCombo(IDC_PLATFORMCL_COMBO, topX + 130, topY, 150, 200, hWnd);
	PopulateCombo(hComboPlatformCL, OpenCL::GetInstance()->GetPlatformNames());

	// OpenCL device
	topY += rowH;
	CreateStatic(L"OpenCL Device", topX, topY, 124, 25, hWnd);
	HWND hComboDevice = CreateCombo(IDC_DEVICE_COMBO, topX + 130, topY, 150, 200, hWnd);
	PopulateCombo(hComboDevice, OpenCL::GetInstance()->GetDeviceNames());

	// buttons
	topY += rowH;
	CreateButton(IDC_RESET_BUTTON, L"Reset", topX, topY, 100, 24, hWnd);
	CreateButton(IDC_PLAY_BUTTON, L"Play/Pause", topX + 110, topY, 100, 24, hWnd);

	// cloth model
	topY += rowH;
	CreateStatic(L"Cloth model", topX, topY, 124, 25, hWnd);
	HWND hComboModel = CreateCombo(IDC_MODEL_COMBO, topX + 130, topY, 150, 200, hWnd);
	PopulateCombo(hComboModel, models, sizeof(models) / sizeof(models[0]));

	// divisions
	topY += rowH;
	HWND hWndStatic1 = CreateStatic(L"Divisions", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_DIV_EDIT, topX + 130, topY, 52, 23, hWnd, ES_NUMBER);
	HWND hSpinDiv = CreateSpin(IDC_DIV_SPIN, hWnd);

	SendMessage(hSpinDiv, UDM_SETRANGE, 0, MAKELPARAM(150, 2));    // Sets the controls direction and range.

	// iterations
	topY += rowH;
	CreateStatic(L"Iterations", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_ITER_EDIT, topX + 130, topY, 52, 23, hWnd, ES_NUMBER);
	HWND hSpinIter = CreateSpin(IDC_ITER_SPIN, hWnd);

	SendMessage(hSpinIter, UDM_SETRANGE, 0, MAKELPARAM(10000, 1));    // Sets the controls direction and range.

	// num particles
	//topY += rowH;
	//HWND hStaticParticles = CreateStatic(L"Particles", topX, topY, 124, 25, hWnd);
	//HWND hEditParticles = CreateEdit(IDC_PARTICLES_EDIT, topX + 130, topY, 72, 23, hWnd, ES_NUMBER);
	//HWND hSpinParticles = CreateSpin(IDC_PARTICLES_SPIN, hWnd);

	//SendMessage(hSpinParticles, UDM_SETRANGE, 0, MAKELPARAM(10000, 1));    // Sets the controls direction and range.

	// unit
	topY += rowH;
	CreateStatic(L"Unit", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_UNIT_EDIT, topX + 130, topY, 52, 23, hWnd);

	// radius
	topY += rowH;
	CreateStatic(L"Radius", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_RADIUS_EDIT, topX + 130, topY, 52, 23, hWnd);

	// method
	topY += rowH;
	CreateStatic(L"Method", topX, topY, 124, 25, hWnd);
	HWND hComboMethod = CreateCombo(IDC_METHOD_COMBO, topX + 130, topY, 150, 200, hWnd);
	PopulateCombo(hComboMethod, methods, sizeof(methods) / sizeof(methods[0]));

	// solver
	topY += rowH;
	CreateStatic(L"Solver", topX, topY, 124, 25, hWnd);
	HWND hComboSolver = CreateCombo(IDC_SOLVER_COMBO, topX + 130, topY, 150, 200, hWnd);
	PopulateCombo(hComboSolver, solvers, sizeof(solvers) / sizeof(solvers[0]));

	// Baumgarte
	//topY += rowH;
	//CreateStatic(L"Baumgarte", topX, topY, 124, 25, hWnd);
	//CreateEdit(IDC_BAUMGARTE_EDIT, topX + 130, topY, 52, 23, hWnd);

	// bending
	topY += rowH;
	CreateStatic(L"Bending (N/m^2)", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_BEND_EDIT, topX + 130, topY, 52, 23, hWnd);

	// shearing
	topY += rowH;
	CreateStatic(L"Shearing (N/m^2)", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_SHEAR_EDIT, topX + 130, topY, 52, 23, hWnd);

	// stiffness
	topY += rowH;
	CreateStatic(L"Stiffness (N/m^2)", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_STIFF_EDIT, topX + 130, topY, 52, 23, hWnd);

	// friction
	topY += rowH;
	CreateStatic(L"Friction", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_MU_EDIT, topX + 130, topY, 52, 23, hWnd);

	// spring area
	topY += rowH;
	CreateStatic(L"Spring area (units)", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_AREA_EDIT, topX + 130, topY, 52, 23, hWnd);

	// Young's modulus
	//topY += rowH;
	//CreateStatic(L"Young's modulus", topX, topY, 124, 25, hWnd);
	//CreateEdit(IDC_YOUNG_EDIT, topX + 130, topY, 52, 23, hWnd);

	// Poisson ratio
	topY += rowH;
	CreateStatic(L"Poisson ratio", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_POISSON_EDIT, topX + 130, topY, 52, 23, hWnd);

	// Newmark alpha
	topY += rowH;
	CreateStatic(L"Newmark alpha", topX, topY, 124, 25, hWnd);
	CreateEdit(IDC_ALPHA_EDIT, topX + 130, topY, 52, 23, hWnd);

	// dihedral bending
	topY += rowH;
	CreateCheck(IDC_DIHEDRAL_CHECK, L"Dihedral bending", topX, topY, 124, 25, hWnd);

	// FEM
	CreateCheck(IDC_FEM_CHECK, L"FEM", topX + 130, topY, 124, 25, hWnd);

	// show debug
	topY += rowH;
	CreateCheck(IDC_SHOW_CHECK, L"Show debug info", topX, topY, 124, 25, hWnd);

	// bicubic
	CreateCheck(IDC_CUBIC_CHECK, L"Bicubic smooth", topX + 130, topY, 124, 25, hWnd);

	// show particles
	topY += rowH;
	CreateCheck(IDC_PARTICLES_CHECK, L"Show particles", topX, topY, 124, 25, hWnd);

	Refresh(hWnd);
	creating = false;
}

void OnCommand(WORD code, WORD id, HWND hWnd)
{
	char str[256];
	if(code == CBN_SELCHANGE)
	// If the user makes a selection from the list:
	//   Send CB_GETCURSEL message to get the index of the selected list item.
	//   Send CB_GETLBTEXT message to get the item.
	//   Display the item in a message box.
	{ 
		int ItemIndex = SendMessage(hWnd, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
		SendMessageA(hWnd, (UINT)CB_GETLBTEXT, (WPARAM)ItemIndex, (LPARAM)str); // ANSI version (easier)
		switch(id)
		{
		case IDC_DEMO_COMBO:
			demo.SetDemoFromString(str);
			demo.Reset();
			Refresh(hMainWnd);
			break;
		case IDC_PLATFORM_COMBO:
			demo.SetPlatformFromString(str);
			demo.Reset();
			break;
		case IDC_PLATFORMCL_COMBO:
			OpenCL::GetInstance()->SetPlatform(ItemIndex);
			demo.Reset();
			Refresh(hMainWnd);
			break;
		case IDC_DEVICE_COMBO:
			OpenCL::GetInstance()->SetDevice(ItemIndex);
			demo.Reset();
			break;
		case IDC_METHOD_COMBO:
			demo.SetMethod(ItemIndex);
			demo.Reset();
			break;
		case IDC_SOLVER_COMBO:
			demo.SetSolver(ItemIndex);
			demo.Reset();
			break;
		case IDC_MODEL_COMBO:
			demo.SetClothModel(ItemIndex);
			demo.Reset();
			break;
		}
	}
	else if (code == EN_CHANGE && !creating)
	{
		SendMessageA(hWnd, WM_GETTEXT, 256, (LPARAM)str);
		switch (id)
		{
		case IDC_DIV_EDIT:
			demo.SetDivisions(max(2, atoi(str)));
			break;
		case IDC_RADIUS_EDIT:
			demo.SetRadius(max(0.1f, (float)atof(str)));
			break;
		case IDC_BEND_EDIT:
			demo.SetBending((float)atof(str));
			break;
		case IDC_SHEAR_EDIT:
			demo.SetShearing((float)atof(str));
			break;
		case IDC_STIFF_EDIT:
			demo.GetCloth().GetModel().SetStiffness((float)atof(str));
			break;
		case IDC_MU_EDIT:
			demo.GetCloth().GetModel().SetFriction((float)atof(str));
			break;
		case IDC_AREA_EDIT:
			demo.GetCloth().GetModel().SetArea((float)atof(str));
			break;
		case IDC_UNIT_EDIT:
			demo.GetCloth().GetModel().SetUnit((float)atof(str));
			break;
		case IDC_ALPHA_EDIT:
			demo.GetCloth().GetModel().SetNewmarkAlpha((float)atof(str));
			break;
		case IDC_PARTICLES_EDIT:
			demo.SetNumParticles(atoi(str));
			break;
		//case IDC_BAUMGARTE_EDIT:
		//	demo.GetPhysics3D().SetBaumgarte(atof(str));
		//	break;
		case IDC_ITER_EDIT:
			demo.SetIterations(atoi(str));
			break;
		}
	}
	else switch (id)
	{
	case IDC_RESET_BUTTON:
		{
			demo.Reset();
			Refresh(hMainWnd);
		}
		break;
	case IDC_PLAY_BUTTON:
		{
			demo.PausePhysics();
		}
		break;
	case IDC_SHOW_CHECK:
		{
			UINT checked = IsDlgButtonChecked(hMainWnd, IDC_SHOW_CHECK);
			demo.SetShowDebug(checked != 0);
		}
 		break;
	case IDC_DIHEDRAL_CHECK:
		{
			UINT checked = IsDlgButtonChecked(hMainWnd, IDC_DIHEDRAL_CHECK);
			demo.GetCloth().GetModel().SetDihedral(checked != 0);
		}
		break;
	case IDC_FEM_CHECK:
		{
			UINT checked = IsDlgButtonChecked(hMainWnd, IDC_FEM_CHECK);
			demo.GetCloth().GetModel().SetFEM(checked != 0);
		}
		break;
	case IDC_PARTICLES_CHECK:
		{
			UINT checked = IsDlgButtonChecked(hMainWnd, IDC_PARTICLES_CHECK);
			int flags = demo.GetDebugDrawFlags();
			if ((flags & ParticlesDemo::DDF_PARTICLES) == 0)
				demo.SetDebugDrawFlags(flags | ParticlesDemo::DDF_PARTICLES);
			else
				demo.SetDebugDrawFlags(flags & ~ParticlesDemo::DDF_PARTICLES);
		}
		break;
	case IDC_CUBIC_CHECK:
		{
			UINT checked = IsDlgButtonChecked(hMainWnd, IDC_CUBIC_CHECK);
			demo.GetCloth().SetBicubic(checked != 0);
			if (checked == 0)
				demo.Reset();
		}
		break;
	}
}

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_SIZE:
		{
			RECT rect;
			GetClientRect(hWnd, &rect);
			int w = rect.right - rect.left;
			int h = rect.bottom - rect.top;
			demo.ResizeWindow(w - 300, h);
			const int topX = w - 300;
			int topY = 0;
			for (size_t i = 0; i < controls.size(); i++)
				controls[i].Resize(topX, topY);
		}
		break;
	case WM_CREATE:
		OnCreate(hWnd);
		break;
	case WM_COMMAND:
		OnCommand(HIWORD(wParam), LOWORD(wParam), (HWND)lParam);
		break;
	case WM_NOTIFY:
		{
			LPNMHDR nmhdr = (LPNMHDR)lParam;
			UINT nCode = nmhdr->code;

			switch (nCode)
			{
			case UDN_DELTAPOS:
				LPNMUPDOWN lpnmud  = (LPNMUPDOWN)lParam;
				switch (nmhdr->idFrom)
				{
				case IDC_DIV_SPIN:
					demo.SetDivisions(lpnmud->iPos);
					break;
				case IDC_ITER_SPIN:
					demo.SetIterations(lpnmud->iPos);
					break;
				}
				break;
			}
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	InitCommonControls();

	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW ;
	wcex.lpfnWndProc	= &MainWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= 0;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName	= 0; 
	wcex.lpszClassName	= L"MainWindow";
	wcex.hIconSm		= 0;

	if (RegisterClassEx(&wcex) == 0)
		return 1;

	hInst = hInstance; // Store instance handle in our global variable

	hMainWnd = CreateWindow(L"MainWindow", L"Particles GUI", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hMainWnd)
	{
		return 1;
	}

	ShowWindow(hMainWnd, nCmdShow);
	UpdateWindow(hMainWnd);

	// Main message loop:
	MSG msg;
	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				break;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			// update and draw the frame
			demo.UpdateAndDraw();
		}		
	}

	return (int) msg.wParam;
}
#else

#include <Windows.h>
#include <CommCtrl.h>
#include <tchar.h>
#include "resource.h"

INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDCANCEL:
			SendMessage(hDlg, WM_CLOSE, 0, 0);
			return TRUE;
		}
		break;

	case WM_CLOSE:
		if(MessageBox(hDlg, TEXT("Close the program?"), TEXT("Close"),
			MB_ICONQUESTION | MB_YESNO) == IDYES)
		{
			DestroyWindow(hDlg);
		}
		return TRUE;

	case WM_DESTROY:
		PostQuitMessage(0);
		return TRUE;
	}

	return FALSE;
}

int WINAPI _tWinMain(HINSTANCE hInst, HINSTANCE h0, LPTSTR lpCmdLine, int nCmdShow)
{
	HWND hDlg;
	MSG msg;
	BOOL ret;

	//InitCommonControls();
	hDlg = CreateDialogParam(hInst, MAKEINTRESOURCE(IDD_DIALOG1), 0, DialogProc, 0);
	ShowWindow(hDlg, nCmdShow);

	while((ret = GetMessage(&msg, 0, 0, 0)) != 0) {
		if(ret == -1)
			return -1;

		if(!IsDialogMessage(hDlg, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return 0;
}
#endif // DISABLED_WIN32
#endif