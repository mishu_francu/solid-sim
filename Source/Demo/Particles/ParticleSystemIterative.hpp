// TODO: SOR-Newton

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNewtonJacobi(int maxIter)
{
	if (links.size() == 0)
		return;
	int n = maxIter == 0 ? numIterations : maxIter;	
	for (int k = 0; k < n; ++k)
	{
		for (size_t i = 0; i < links.size(); i++)
		{
			Particle* p1 = &particles[links[i].i1];
			Vector2 delta = p1->pos;
			Particle* p2 = NULL;
			p2 = &particles[links[i].i2];
			delta.Subtract(p2->pos);
			float len0 = links[i].len;
			float len = delta.Length();
			delta.Scale(1 / len);
			links[i].normal = delta;
			links[i].lambda = (len - len0) / (p1->invMass + p2->invMass);
		}
		for (size_t i = 0; i < links.size(); i++)
		{
			Vector2 delta = links[i].lambda * links[i].normal;
			particles[links[i].i1].pos.Subtract(delta * particles[links[i].i1].invMass);
			particles[links[i].i2].pos.Add(delta * particles[links[i].i2].invMass);
		}
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNewtonSteepestDescent(int maxIters, bool minRes) // TODO: make minres a template arg
{
	size_t nl = links.size();
	if (nl == 0)
		return;
	if (r.empty())
		r.resize(nl);

	const float tolerance = 0.001f;
	// TODO: insert inside main loop
	for (size_t i = 0; i < nl; i++)
	{
		Vector2 delta;
		delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
		r[i] = -(links[i].len - delta.Length());
		delta.Normalize();
		links[i].normal = delta;
		links[i].lambda = 0;
	}

	size_t np = particles.size();
	std::vector<Vector2> y(np);
	std::vector<float> q(nl);
	int subIter = 0;
	const int skip = 1;
	float alpha = 0.25f;
	// TODO: keep only min res version with no skip
	int numIters = min(numIterations, maxIters);
	if (numIters == 0) numIters = numIterations;
	for (int iter = 0; iter < numIters; iter++)
	{
		// decide wheter to do the next Newton step
		subIter++;
		bool reset = subIter >= skip;
		if (reset) subIter = 0;

		float den = 0;
		float dot = 0;

		for (size_t i = 0; i < nl; i++)
		{
			// y = J^T * r
			Vector2 add = links[i].normal * r[i];
			y[links[i].i1] -= add;
			y[links[i].i2] += add;

			// dot = r^T * r
			if (!minRes)
				dot += r[i] * r[i];
		}
		if (!reset || minRes)
		{
			// q = J * W  * y
			for (size_t i = 0; i < nl; i++)
			{
				// TODO: increment as you go
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);

				if (minRes)
				{
					// den = q^T * q
					den += q[i] * q[i];
					// dot = r^T * q
					dot += r[i] * q[i];
				}
			}
		}
		for (size_t i = 0; i < np; i++)
		{
			if (!minRes)
			{
				// den = y^T * W * y (can be computed before q)
				den += particles[i].invMass * y[i].LengthSquared();
			}
			y[i].SetZero(); // TODO: memset
		}
		// compute alpha
		//if (dot < tolerance || den < tolerance)
			//return;		
		alpha = dot / den;
		//Printf("alpha%d=%f\n", iter, alpha);
		for (size_t i = 0; i < nl; i++)
		{
			// x = x + alpha * r
			float dLambda = alpha * r[i];
			links[i].lambda += dLambda; // useless

			// aplly lambda
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			Vector2 disp = dLambda * links[i].normal;
			particles[i1].pos += particles[i1].invMass * disp;
			particles[i2].pos -= particles[i2].invMass * disp;
		}
		for (size_t i = 0; i < nl; i++)
		{
			if (reset)
			{
				// recompute residual and normal
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				Vector2 delta;
				delta = particles[i2].pos - particles[i1].pos;			
				r[i] = -(links[i].len - delta.Length());
				delta.Normalize();
				links[i].normal = delta;
			}
			else
			{
				// r = r - alpha * q
				// TODO: move it in previous loop
				r[i] -= alpha * q[i]; // this is where we need q
			}
		}
	}	
}

// FIXME
template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNewtonConjugateGradient()
{
	size_t nl = links.size();
	if (nl == 0)
		return;
	if (r.empty())
	{
		r.resize(nl);
		d.resize(nl);
	}

	size_t np = particles.size();
	std::vector<Vector2> y(np);
	std::vector<float> q(nl);
	const int cgIters = 2;
	int numNewtIter = numIterations / cgIters;
	if (numIterations % cgIters) numNewtIter++;

	for (int newtIter = 0; newtIter < numNewtIter; newtIter++)
	{
		float dot1 = 0;
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 delta;
			delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
			r[i] = -(links[i].len - delta.Length());
			d[i] = r[i];
			dot1 += r[i] * r[i];
		
			delta.Normalize();
			links[i].normal = delta;
			links[i].lambda = 0;
		}

		for (int iter = 0; iter < cgIters; iter++)
		{
			// y = J^T * d
			for (size_t i = 0; i < nl; i++)
			{
				Vector2 add = links[i].normal * d[i];
				y[links[i].i1] -= add;
				y[links[i].i2] += add;
			}
			// q = J * W  * y
			for (size_t i = 0; i < nl; i++)
			{
				// TODO: increment as you go
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);
			}
			// den = y^T * W * y (can be computed before q)
			float den = 0;
			for (size_t i = 0; i < np; i++)
			{
				den += particles[i].invMass * y[i].LengthSquared();
				y[i].SetZero(); // TODO: memset
			}
			// compute alpha and the new solution
			float alpha = dot1 / den;		
			float dot0 = dot1;
			dot1 = 0;
			for (size_t i = 0; i < nl; i++)
			{
				// x = x + alpha * d
				float dLambda = alpha * d[i];
				links[i].lambda += dLambda; // useless

				// aplly lambda
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				Vector2 disp = dLambda * links[i].normal;
				particles[i1].pos += particles[i1].invMass * disp;
				particles[i2].pos -= particles[i2].invMass * disp;
			
				// r = r - alpha * q
				r[i] -= alpha * q[i]; // this is where we need q
				dot1 += r[i] * r[i];
			}
			// compute beta - TODO: move at beginning of loop
			float beta = dot1 / dot0;
			for (size_t i = 0; i < nl; i++)
			{
				d[i] = r[i] + beta * d[i];
			}
		}	
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNewtonMinimumResidual()
{
	size_t nl = links.size();
	if (nl == 0)
		return;
	if (r.empty())
	{
		r.resize(nl);
		d.resize(nl);
	}

	size_t np = particles.size();
	std::vector<Vector2> y(np);
	std::vector<float> q(nl), z(nl);
	const int cgIters = 2; // 1 is SDMR
	int numNewtIter = numIterations / cgIters;
	if (numIterations % cgIters) numNewtIter++;

	for (int newtIter = 0; newtIter < numNewtIter; newtIter++)
	{
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 delta;
			delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
			r[i] = -(links[i].len - delta.Length());
			d[i] = r[i];
		
			delta.Normalize();
			links[i].normal = delta;
			links[i].lambda = 0;
		}

		float dot0, dot1;
		for (int iter = 0; iter < cgIters; iter++)
		{
			// y = J^T * r
			memset(&y[0], 0, np * sizeof(Vector2));
			for (size_t i = 0; i < nl; i++)
			{
				Vector2 add = links[i].normal * r[i];
				y[links[i].i1] -= add;
				y[links[i].i2] += add;
			}
			// q = J * W  * y
			for (size_t i = 0; i < nl; i++)
			{
				// TODO: increment as you go
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);
				if (iter == 0)
					z[i] = q[i];
			}
			// d1 = y^T * W * y (can be computed before q)
			dot1 = 0;
			for (size_t i = 0; i < np; i++)
			{
				dot1 += particles[i].invMass * y[i].LengthSquared();
				//y[i].SetZero(); // TODO: memset
			}
			if (dot1 == 0) // TODO: tolerance
				break;
			// compute beta from the 2nd iteration on
			float den = 0;
			if (iter > 0)
			{
				float beta = dot1 / dot0;
				for (size_t i = 0; i < nl; i++)
				{
					d[i] = r[i] + beta * d[i];
					z[i] = q[i] + beta * z[i];
					den += z[i] * z[i];
				}
			}
			else
			{
				for (size_t i = 0; i < nl; i++)
				{
					den += z[i] * z[i];
				}
			}
			if (den == 0) // TODO: tolerance
				break;
			dot0 = dot1;
			// compute alpha and the new solution
			float alpha = dot1 / den;
			for (size_t i = 0; i < nl; i++)
			{
				// x = x + alpha * d
				float dLambda = alpha * d[i];
				links[i].lambda += dLambda; // useless

				// aplly lambda
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				Vector2 disp = dLambda * links[i].normal;
				particles[i1].pos += particles[i1].invMass * disp;
				particles[i2].pos -= particles[i2].invMass * disp;
			
				// r = r - alpha * q
				r[i] -= alpha * z[i]; // this is where we need q
			}
		}	
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNewtonMinimumResidual1()
{
	size_t nl = links.size();
	if (nl == 0)
		return;
	if (r.empty())
	{
		r.resize(nl);
		d.resize(nl);
	}

	size_t np = particles.size();
	std::vector<Vector2> y(np);
	std::vector<float> q(nl), z(nl);
	const int cgIters = 2; // 1 is SDMR
	int numNewtIter = numIterations / cgIters;
	if (numIterations % cgIters) numNewtIter++;

	for (int newtIter = 0; newtIter < numNewtIter; newtIter++)
	{
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 delta;
			delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
			r[i] = -(links[i].len - delta.Length());
			d[i] = r[i];
		
			delta.Normalize();
			links[i].normal = delta;
			links[i].lambda = 0;
		}

		float dot0, dot1;
		for (int iter = 0; iter < cgIters; iter++)
		{
			// y = J^T * r
			memset(&y[0], 0, np * sizeof(Vector2));
			for (size_t i = 0; i < nl; i++)
			{
				Vector2 add = links[i].normal * r[i];
				y[links[i].i1] -= add;
				y[links[i].i2] += add;
			}
			// q = J * W  * y
			for (size_t i = 0; i < nl; i++)
			{
				// TODO: increment as you go
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);
				if (iter == 0)
					z[i] = q[i];
			}
			// d1 = y^T * W * y (can be computed before q)
			dot1 = 0;
			for (size_t i = 0; i < np; i++)
			{
				dot1 += particles[i].invMass * y[i].LengthSquared();
				//y[i].SetZero(); // TODO: memset
			}
			if (dot1 == 0) // TODO: tolerance
				break;
			// compute beta from the 2nd iteration on
			float den = 0;
			if (iter > 0)
			{
				float beta = dot1 / dot0;
				for (size_t i = 0; i < nl; i++)
				{
					d[i] = r[i] + beta * d[i];
					z[i] = q[i] + beta * z[i];
					den += z[i] * z[i];
				}
			}
			else
			{
				for (size_t i = 0; i < nl; i++)
				{
					den += z[i] * z[i];
				}
			}
			if (den == 0) // TODO: tolerance
				break;
			dot0 = dot1;
			// compute alpha and the new solution
			float alpha = dot1 / den;
			for (size_t i = 0; i < nl; i++)
			{
				// x = x + alpha * d
				float dLambda = alpha * d[i];
				links[i].lambda += dLambda; // useless

				// aplly lambda
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				Vector2 disp = dLambda * links[i].normal;
				particles[i1].pos += particles[i1].invMass * disp;
				particles[i2].pos -= particles[i2].invMass * disp;
			
				// r = r - alpha * q
				r[i] -= alpha * z[i]; // this is where we need q
			}
		}	
	}
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNonlinearConjugateGradient()
{
	size_t nl = links.size();
	if (nl == 0)
		return;
	if (r.empty())
	{
		r.resize(nl);
		d.resize(nl);
	}

	size_t np = particles.size();
	std::vector<Vector2> y(np);
	std::vector<float> q(nl);
	std::vector<float> s(nl); // PR

	float resL1 = 0;
	// compute r0, d0 and normals
	float dot1 = 0;
	for (size_t i = 0; i < nl; i++)
	{
		Vector2 delta;
		delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
		r[i] = -(links[i].len - delta.Length());
		d[i] = r[i];
		dot1 += r[i] * r[i];
		
		delta.Normalize();
		links[i].normal = delta;
		links[i].lambda = 0; // useless

		resL1 += fabs(r[i]);
	}
	//Printf("%f\n", resL1);

	const int newtIters = 1;
	int ncgIters = numIterations / newtIters;
	if (numIterations % newtIters) ncgIters++;

	for (int iter = 0; iter < ncgIters; iter++)
	{
		for (size_t i = 0; i < nl; i++)
		{
			s[i] = r[i]; //PR
		}
		for (int newtIter = 0; newtIter < newtIters; newtIter++)
		{
			// y = J^T * d and gamma = r^T * d
			float gamma = 0;
			for (size_t i = 0; i < nl; i++)
			{
				Vector2 add = links[i].normal * d[i];
				y[links[i].i1] -= add;
				y[links[i].i2] += add;

				gamma += r[i] * d[i];		
			}
			// den = y^T * W * y
			float den = 0;
			for (size_t i = 0; i < np; i++)
			{
				den += particles[i].invMass * y[i].LengthSquared();
				y[i].SetZero(); // TODO: memset
			}
			// compute alpha and the new solution
			float alpha = gamma / den;
			for (size_t i = 0; i < nl; i++)
			{
				// x = x + alpha * d
				float dLambda = alpha * d[i];
				links[i].lambda += dLambda; // useless

				// aplly lambda
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				Vector2 disp = dLambda * links[i].normal;
				particles[i1].pos += particles[i1].invMass * disp;
				particles[i2].pos -= particles[i2].invMass * disp;
			}
			// recompute residual and normal
			resL1 = 0;
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				Vector2 delta;
				delta = particles[i2].pos - particles[i1].pos;			
				r[i] = -(links[i].len - delta.Length());
				delta.Normalize();
				links[i].normal = delta;

				resL1 += fabs(r[i]);
			}
			//Printf("%f\n", resL1);
		}
		// compute beta and new search direction
		float dot0 = dot1;
		dot1 = 0;
		for (size_t i = 0; i < nl; i++)
		{
			//dot1 += r[i] * r[i]; // FR
			// PR
			s[i] = r[i] - s[i];
			dot1 += s[i] * r[i];
		}
		float beta = dot1 / dot0;
		for (size_t i = 0; i < nl; i++)
		{
			d[i] = r[i] + beta * d[i];
		}
	}	
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNonlinearMinimumResidual()
{
	size_t nl = links.size();
	if (nl == 0)
		return;
	if (z.empty())
	{
		r.resize(nl);
		d.resize(nl);
		z.resize(nl);
	}

	size_t np = particles.size();
	std::vector<Vector2> y(np);
	std::vector<float> q(nl);
	std::vector<float> rPrev(nl);

	const float att = 1.f;

	// compute r0, d0 and normals
	float resL1 = 0;
	for (size_t i = 0; i < nl; i++)
	{
		Vector2 delta;
		delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
		r[i] = -(links[i].len - delta.Length()) * att;
		d[i] = r[i];
		
		delta.Normalize();
		links[i].normal = delta;
		links[i].lambda = 0; // useless

		resL1 += fabs(r[i]);
	}
	//Printf("%f\n", resL1);

	const int newtIters = 1;
	int ncgIters = numIterations / newtIters;
	if (numIterations % newtIters) ncgIters++;

	float dot1 = 0;
	for (int iter = 0; iter < ncgIters; iter++)
	{
		// y = J^T * r
		memset(&y[0], 0, np * sizeof(Vector2));
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 add = links[i].normal * r[i];
			y[links[i].i1] -= add;
			y[links[i].i2] += add;
		}
		// q = J * W  * y
		for (size_t i = 0; i < nl; i++)
		{
			// TODO: increment as you go
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);
		}
		
		// dot1 = r^T * q
		float dot0 = dot1;
		dot1 = 0;
		float dotPR = 0;
		for (size_t i = 0; i < nl; i++)
		{
			dot1 += r[i] * q[i];
			if (iter == 0)
			{
				z[i] = q[i];
				rPrev[i] = 0;
			}
			dotPR += z[i] * (r[i] - rPrev[i]);
		}
		if (iter > 0)
		{
			float betaFR = dot1 / dot0; // Fletcher-Reeves
			float betaPR = -dotPR / dot0; //Polak-Ribiere // TODO: drop it
			float beta = betaFR;
			for (size_t i = 0; i < nl; i++)
			{
				d[i] = r[i] + beta * d[i];
				z[i] = q[i] + beta * z[i];
			}
		}
		// the line search
		float den = 0;
		float gamma = 0;
		for (size_t i = 0; i < nl; i++)
		{
			den += z[i] * z[i];
			gamma += r[i] * z[i];
		}
		//for (int newtIter = 0; newtIter < newtIters; newtIter++)
		{
			// y = J^T * d
			memset(&y[0], 0, np * sizeof(Vector2));
			for (size_t i = 0; i < nl; i++)
			{
				Vector2 add = links[i].normal * d[i];
				y[links[i].i1] -= add;
				y[links[i].i2] += add;
			}
			// z = J * W  * y
			//float den = 0;
			//float gamma = 0;
			//for (size_t i = 0; i < nl; i++)
			//{
			//	// TODO: increment as you go
			//	const int i1 = links[i].i1;
			//	const int i2 = links[i].i2;
			//	z[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);
			//	den += z[i] * z[i];
			//	gamma += r[i] * z[i];
			//}

			float alpha = gamma / den;

			for (size_t i = 0; i < nl; i++)
			{
				// x = x + alpha * d
				float dLambda = alpha * d[i];
				links[i].lambda += dLambda; // useless

				// apply lambda
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				Vector2 disp = dLambda * links[i].normal;
				particles[i1].pos += particles[i1].invMass * disp;
				particles[i2].pos -= particles[i2].invMass * disp;
			}
			// recompute residual and normal
			resL1 = 0;
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				Vector2 delta;
				delta = particles[i2].pos - particles[i1].pos;
				rPrev[i] = r[i]; // PR
				r[i] = -(links[i].len - delta.Length()) * att;
				delta.Normalize();
				links[i].normal = delta;

				resL1 += fabs(r[i]);
			}
			//Printf("%f\n", resL1);
		}
	}
}

template <typename Particle, typename Broadphase>
float ParticleSystem<Particle, Broadphase>::SpectralRadius(std::vector<float>& x, int iters)
{
	PROFILE_SCOPE("Spectral radius");
	// power method for computing the largest eigenvalue (absolute value) and its eigenvector
	//MEASURE_TIME("Spectral Radius");
	size_t nl = links.size();
	if (nl == 0)
		return 0;
	
	// initial guess for eigenvector
	//std::vector<float> x(nl, 0);
	//x[0] = 1;

	// compute normals
	for (size_t i = 0; i < nl; i++)
	{
		const int i1 = links[i].i1;
		const int i2 = links[i].i2;
		Vector2 delta;
		delta = particles[i2].pos - particles[i1].pos;			
		delta.Normalize();
		links[i].normal = delta;
	}

	size_t np = particles.size();
	std::vector<Vector2> y(np);
	std::vector<float> q(nl);
	float sr = 0;
	for (int iter = 0; iter < iters; iter++)
	{
		// y = J^T * r
		memset(&y[0], 0, np * sizeof(Vector2));
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 add = links[i].normal * x[i];
			y[links[i].i1] -= add;
			y[links[i].i2] += add;
		}
		// q = J * W  * y
		float len2 = 0;
		for (size_t i = 0; i < nl; i++)
		{
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);
			len2 += q[i] * q[i];
		}
		if (len2 == 0)
			break;
		if (iter == iters - 1)
		{
			// lambda = x^T * q
			for (size_t i = 0; i < nl; i++)
			{
				sr += x[i] * q[i];
			}
		}
		// x = normalize(q)
		float invLen = 1.f / sqrt(len2);
		for (size_t i = 0; i < nl; i++)
		{
			x[i] = q[i] * invLen;
		}
	}
	return sr;
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectMinRes(float alpha)
{
//#define SKIP_ALPHA
	PROFILE_SCOPE("ProjectMinRes");
	size_t nl = links.size();
	if (nl == 0)
		return;
	if (r.empty())
		r.resize(nl);

	std::vector<float> c(nl);
	size_t np = particles.size();
	std::vector<Vector2> y(np);
	std::vector<float> q(nl);
	//float alpha = 0.2f;
	for (int iter = 0; iter < numIterations; iter++)
	{
		// compute error and normal
		for (size_t i = 0; i < nl; i++)
		{
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			Vector2 delta;
			delta = particles[i2].pos - particles[i1].pos;			
			c[i] = links[i].len - delta.Length();
			r[i] = c[i];// - alpha * q[i];
			delta.Normalize();
			links[i].normal = delta;
		}

#ifndef SKIP_ALPHA
		float den = 0;
		float dot = 0;

		// y = J^T * r
		memset(&y[0], 0, np * sizeof(Vector2));
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 add = links[i].normal * r[i];
			y[links[i].i1] -= add;
			y[links[i].i2] += add;
		}
		// q = J * W  * y
		// den = q^T * q
		// dot = c^T * q
		for (size_t i = 0; i < nl; i++)
		{
			// TODO: increment as you go
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);

			den += q[i] * q[i];
			dot += c[i] * q[i];
		}

		alpha = dot / den;
#endif

		// apply dlambda = alpha * r
		for (size_t i = 0; i < nl; i++)
		{
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			Vector2 disp = alpha * r[i] * links[i].normal;
			particles[i1].pos -= particles[i1].invMass * disp;
			particles[i2].pos += particles[i2].invMass * disp;
		}
	}	
}

template <typename Particle, typename Broadphase>
void ParticleSystem<Particle, Broadphase>::ProjectNCR()
{
	PROFILE_SCOPE("CR");
	size_t nl = links.size();
	if (nl == 0)
		return;
	if (r.empty())
		r.resize(nl);

	// compute initial error
	float rSqrOld = 0;
	std::vector<float> c(nl), d(nl);
	for (size_t i = 0; i < nl; i++)
	{
		Vector2 delta;
		delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
		c[i] = (links[i].len - delta.Length()) * links[i].stiffness;
		r[i] = c[i];
		d[i] = r[i];
		delta.Normalize();
		links[i].normal = delta;
		links[i].lambda = 0;
	}

	// y = J^T * r
	size_t np = particles.size();
	std::vector<Vector2> y(np);
	memset(&y[0], 0, np * sizeof(Vector2));
	for (size_t i = 0; i < nl; i++)
	{
		Vector2 add = links[i].normal * r[i];
		y[links[i].i1] -= add;
		y[links[i].i2] += add;
	}
	// r^2 = r A r = y W y
	for (size_t i = 0; i < np; i++)
	{
		rSqrOld += y[i].LengthSquared() * particles[i].invMass;
	}

	std::vector<float> q(nl);
	for (int iter = 0; iter < numIterations; iter++)
	{
		float den = 0;
		float dot = 0;
		float rSqrNew = 0;

		// y = J^T * d
		memset(&y[0], 0, np * sizeof(Vector2));
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 add = links[i].normal * d[i];
			y[links[i].i1] -= add;
			y[links[i].i2] += add;
 		}
		// q = J * W  * y = A * d
		// den = q^T * q
		// dot = c^T * q
		for (size_t i = 0; i < nl; i++)
		{
			// TODO: increment as you go
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);

			den += q[i] * q[i];
			dot += c[i] * q[i];
		}

		float alpha = -dot / den;
		//Printf("alpha%d: %f\n", iter, alpha);

		// apply dlambda = alpha * d
		for (size_t i = 0; i < nl; i++)
		{
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			Vector2 disp = alpha * d[i] * links[i].normal;
			particles[i1].pos += particles[i1].invMass * disp;
			particles[i2].pos -= particles[i2].invMass * disp;
		}
		// recompute error and normal
		for (size_t i = 0; i < nl; i++)
		{
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			Vector2 delta;
			delta = particles[i2].pos - particles[i1].pos;			
			c[i] = (links[i].len - delta.Length()) * links[i].stiffness;
			delta.Normalize();
			links[i].normal = delta;

			r[i] = c[i] + alpha * q[i];
		}

		// y = J^T * r
		memset(&y[0], 0, np * sizeof(Vector2));
		for (size_t i = 0; i < nl; i++)
		{
			Vector2 add = links[i].normal * r[i];
			y[links[i].i1] -= add;
			y[links[i].i2] += add;
		}
		// r^2 = r A r
		for (size_t i = 0; i < np; i++)
		{
			rSqrNew += y[i].LengthSquared() * particles[i].invMass;
		}

		// compute new search direction
		float beta = rSqrNew / rSqrOld;
		//Printf("beta%d: %f\n", iter, beta);
		rSqrOld = rSqrNew;
		for (size_t i = 0; i < nl; i++)
		{
			d[i] = r[i] + beta * d[i];
		}
	}	
}

