#ifndef PHYSICS_SYSTEM_H
#define PHYSICS_SYSTEM_H

#include <Demo/Particles/Constraint.h>
#include <Demo/Particles/Particle.h>
#include <Graphics2D/Graphics2D.h> // TODO: remove
#include <memory>

// TODO: remove the inline thing
#ifdef ANDROID_NDK
//#	define CCD
#	define INLINE inline
	const float refFrameLen = 0.02f;
#else
#	define INLINE __forceinline
	const float refFrameLen = 0.016f;
//#	include <Physics/OpenCL.h>
#endif

class PhysicsSystem
{
public:
	enum SolverType
	{
		JACOBI,
		GAUSS_SEIDEL,
		JACOBI_CR,
		MIN_RES,
		CONJ_RES,
		NESTEROV,
		SOFT_CONSTR,
		EXACT,
	};

protected:
	int numIterations;
	int numIterationsPost;
	float lengthScale;
	int numMicroSteps;
	bool sse;
	SolverType solver;
	ConstraintVector links;
	ConstraintVector constraints;
	ConstraintVector springs;

	// TODO: move
	std::vector<std::auto_ptr<Primitive> > collBox;

	float timeStep;
	Vector2 gravity;

	LineMesh mesh; // TODO: pointer

	float mu; // friction coefficient
	float tolerance; // radius collision tolerance
	float beta; // Baumgarte factor

	bool detectCollision;
	int nFrames;
	int mMaxFrames;

public:
	PhysicsSystem();
	virtual ~PhysicsSystem() { }

	virtual void Init(float w, float h, float r) = 0;
	virtual void SetNumParticles(int n) = 0;
	virtual size_t GetNumParticles() const = 0;
	virtual ParticleBase& GetParticle(size_t i) = 0;
	virtual void SetParticleVelocity(size_t i, const Vector2& v) = 0;
	virtual Vector2 GetParticleVelocity(size_t i) = 0;
	virtual Constraint* AddLink(ParticleIdx parIdx1, ParticleIdx parIdx2, float stiffness = 1) = 0;
	virtual void BuildIncidenceMatrix() = 0;
	virtual void MicroStep() = 0;
	virtual bool SupportsSolver(SolverType solver) = 0;

	void Step();
	ConstraintVector& GetLinks() { return links; }
	ConstraintVector& GetConstraints() { return constraints; }

	// TODO: should go somewhere else
	void SetNumBoxes(int n) { collBox.resize(n); }
	size_t GetNumBoxes() const { return collBox.size(); }
	std::auto_ptr<Primitive>& GetBox(int i) { return collBox[i]; }

	void SetNumMicroSteps(int val)
	{
		numMicroSteps = val;
		timeStep = refFrameLen / val;
	}
	float GetTimeStep() const { return timeStep; }
	float GetLengthScale() const { return lengthScale; }
	SolverType GetSolver() const { return solver; }
	Vector2& GetGravity() { return gravity; }

	const LineMesh& GetLinksMesh() { return mesh; }

	void SetNumIterations(int val) { numIterations = val; }
	void SetNumIterationsPost(int val) { numIterationsPost = val; }
	void SetSolver(SolverType val) { solver = val; }
	void SetFriction(float val) { mu = val; }
	void SetTolerance(float val) { tolerance = val; }
	void SetBaumgarte(float val) { beta = val; }
	void SetMaxFrames(int val) { mMaxFrames = val; }
	void SetDetectCollision(bool val) { detectCollision = val; }
};

inline PhysicsSystem::PhysicsSystem()
	: numIterations(10)
	, numIterationsPost(0)
	, gravity(0, 10)
	, lengthScale(500.0f)
	, numMicroSteps(1)
	, sse(false)
	, solver(GAUSS_SEIDEL)
	, timeStep(refFrameLen / numMicroSteps)
	, mu(0.1f)
	, tolerance(0)
	, beta(0)
	, detectCollision(true)
	, mMaxFrames(-1)
{
}

#endif // PHYSICS_SYSTEM_H