//#include <Engine/Base.h>

#include "SweepAndPrune.h"
#include <Engine/Engine.h>

#include <vector>
using namespace std;

void SweepAndPrune::Init(const AabbVector& aabbs)
{
	// init endpoints
	const size_t numParticles = aabbs.size();
	epx.resize(2 * numParticles);
	epy.resize(2 * numParticles);
	for (size_t i = 0; i < numParticles; i++)
	{
		Aabb bounds = aabbs[i];

		epx[i * 2].SetPos(bounds.min.GetX());
		epx[i * 2].SetType(BEGIN);
		epx[i * 2].SetIndex((int)i);

		epx[i * 2 + 1].SetPos(bounds.max.GetX());
		epx[i * 2 + 1].SetType(END);
		epx[i * 2 + 1].SetIndex((int)i);

		epy[i * 2].SetPos(bounds.min.GetY());
		epy[i * 2].SetType(BEGIN);
		epy[i * 2].SetIndex((int)i);

		epy[i * 2 + 1].SetPos(bounds.max.GetY());
		epy[i * 2 + 1].SetType(END);
		epy[i * 2 + 1].SetIndex((int)i);
	}

	UpdateAxisSweep(epx);
	UpdateAxisSweep(epy);
}

void SweepAndPrune::Update(const AabbVector& aabbs, ConstraintVector& pairs)
{
	// update endpoints - X axis
	for (size_t i = 0; i < epx.size(); i++)
	{
		const Aabb& bounds = aabbs[epx[i].GetIndex()];
		if (epx[i].GetType() == BEGIN)
		{
			epx[i].SetPos(bounds.min.GetX());
		}
		else
		{
			epx[i].SetPos(bounds.max.GetX());
		}
	}
	// Y axis
	for (size_t i = 0; i < epy.size(); i++)
	{
		const Aabb& bounds = aabbs[epy[i].GetIndex()];
		if (epy[i].GetType() == BEGIN)
		{
			epy[i].SetPos(bounds.min.GetY());
		}
		else
		{
			epy[i].SetPos(bounds.max.GetY());
		}
	}

	UpdateAxis(epx, aabbs);
	UpdateAxis(epy, aabbs);

	pairs.clear();
	//hash_set<uint32>::iterator it;
	for (auto it = pairsSet.begin(); it != pairsSet.end(); ++it )
	{
		uint32 fusedPair = *it;
		int a = fusedPair >> 16;
		int b = fusedPair & 0xffff;
		pairs.push_back(Constraint(Constraint::COLL_PAIR, a, b, 0));
	}
}

void SweepAndPrune::UpdateAxis(vector<Endpoint>& endpoints, const AabbVector& aabbs)
{
	//sort endpoints by insertion sort
	for (size_t i = 1; i < endpoints.size(); i++)
	{
		for (int j = (int)i; j > 0 && endpoints[j].GetPos() < endpoints[j - 1].GetPos(); j--)
		{
			// swap
			//if (endpoints[j].type != endpoints[j - 1].type)
			{
				if (endpoints[j].GetType() == BEGIN && endpoints[j - 1].GetType() == END)
				{
					AddPair(endpoints[j].GetIndex(), endpoints[j - 1].GetIndex(), aabbs);
				}
				else if (endpoints[j].GetType() == END && endpoints[j - 1].GetType() == BEGIN)
				{
					RemovePair(endpoints[j].GetIndex(), endpoints[j - 1].GetIndex());
				}
			}
			Endpoint temp = endpoints[j];
			endpoints[j] = endpoints[j - 1];
			endpoints[j - 1] = temp;
		}
	}
}

void SweepAndPrune::UpdateAxisSweep(vector<Endpoint>& endpoints)
{
	//sort endpoints by insertion sort
	for (size_t i = 1; i < endpoints.size(); i++)
	{
		for (int j = (int)i; j > 0 && endpoints[j].GetPos() < endpoints[j - 1].GetPos(); j--)
		{
			Endpoint temp = endpoints[j];
			endpoints[j] = endpoints[j - 1];
			endpoints[j - 1] = temp;
		}
	}

	{
		std::vector<uint8> set(endpoints.size() / 2);
		for (uint32 i = 0; i < set.size(); i++)
		{
			set[i] = 0;
		}
		for (size_t i = 0; i < endpoints.size(); i++)
		{
			if (endpoints[i].GetType() == BEGIN)
			{
				for (uint32 j = 0; j < set.size(); j++)
				{
					if (set[j] == 1)
					{
						int index1 = j;
						int index2 = endpoints[i].GetIndex();
						int pi, pj;
						if (index1 < index2)
						{
							pi = index1;
							pj = index2;
						}
						else
						{
							pj = index1;
							pi = index2;
						}
						uint32 pair = (pi << 16) | pj;
						pairsSet.insert(pair);
					}
				}
				set[endpoints[i].GetIndex()] = 1;
			}
			else
			{
				set[endpoints[i].GetIndex()] = 0;
			}
		}
	}
}
