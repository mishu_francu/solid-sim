#include <Engine/Base.h>
#include <Engine/Engine.h>
#include <Engine/Profiler.h>
#include "GridBroadphase.h"
#include "Constraint.h"
#include "Collision.h"
#include <vector>
#include <algorithm>

static int profile;

void GridBroadphase::Init(float w, float h, float size) 
{
	cellSize = size;
	int columns = (int)(w / size + 1);
	int rows = (int)(h / size + 1);
	width = columns;
	height = rows;
	cells = new int[width * height];
	memset(cells, 0xff, width * height * sizeof(int));
}

inline void GridBroadphase::TestCell(const AabbVector& handles, ConstraintVector& pairs, int count, int start, int x, int y)
{
	int id = x + y * width;
	if (cells[id] <= 0)
		return;
	int countAdj = (cells[id] >> 24) & 0xff;
	int startAdj = cells[id] & 0xffffff;
	for (int i = 0; i < count; i++)
		for (int j = 0; j < countAdj; j++)
			if (AabbOverlap(handles[particles[start + i].particleId], handles[particles[startAdj + j].particleId]))
				pairs.push_back(Constraint(Constraint::COLL_PAIR, particles[start + i].particleId, particles[startAdj + j].particleId, cellSize));
}

void GridBroadphase::Update(const AabbVector& handles, ConstraintVector& pairs)
{
	PROFILE_SCOPE("GridBroadphase");
	// sample particles into grid cells
	if (particles.empty())
		particles.resize(handles.size());
	// TODO: bounding spheres
	size_t num = 0;
	for (size_t i = 0; i < handles.size(); i++)
	{
		// use center
		Vector2 center = handles[i].min;
		center.Add(handles[i].max);
		center.Scale(0.5f);
		int x = (int)(center.GetX() / cellSize);
		int y = (int)(center.GetY() / cellSize);
		if (x < 0 || x >= width || y < 0 || y >= height)
			continue;
		int cellId = x + y * width;		
		particles[num++] = ParticleInCell((int)i, cellId);
	}
	// sort the particles by cell id
	std::sort(particles.begin(), particles.begin() + num);	
	// set the cell information: cell[i] = <start[i], count[i]>
	// TODO: sparse array
	memset(cells, 0xff, width * height * sizeof(int));
	int lastId = -1;
	int count = 0;
	for (size_t i = 0; i < num; i++)
	{
		if (lastId != particles[i].cellId)
		{			
			if (lastId >= 0)
			{
				ASSERT(count < 256);
				cells[lastId] |= (count & 0xff) << 24;
			}
			lastId = particles[i].cellId;
			cells[lastId] = i & 0xffffff;
			count = 0;	
		}
		count++;
	}
	ASSERT(count < 256);
	cells[lastId] |= (count & 0xff) << 24;
	// go through all the cells and query the neighbours
	//pairs.clear();
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int id = x + y * width;
			if (cells[id] < 0) continue;
			// inside pairs
			int count = (cells[id] >> 24) & 0xff;
			int start = cells[id] & 0xffffff;
			for (int i = 0; i < count - 1; i++)
				for (int j = i + 1; j < count; j++)
					if (AabbOverlap(handles[particles[start + i].particleId], handles[particles[start + j].particleId]))
						pairs.push_back(Constraint(Constraint::COLL_PAIR, particles[start + i].particleId, particles[start + j].particleId, cellSize));
			// right adjacent pairs
			if (x + 1 < width)
			{
				TestCell(handles, pairs, count, start, x + 1, y);
			}
			// bottom left adjacent pairs
			if (y + 1 < height && x - 1 >= 0)
			{
				TestCell(handles, pairs, count, start, x - 1, y + 1);
			}
			// bottom adjacent pairs
			if (y + 1 < height)
			{
				TestCell(handles, pairs, count, start, x, y + 1);
			}
			// diagonal adjacent pairs
			if (y + 1 < height && x + 1 < width)
			{
				TestCell(handles, pairs, count, start, x + 1, y + 1);
			}
		}
	}
}

void GridBroadphase::TestObject(const Aabb& aabb, std::vector<int>& ids)
{
	int iMin = max(0, min(width - 1, (int)(aabb.min.GetX() / cellSize)));
	int iMax = max(0, min(width - 1, (int)(aabb.max.GetX() / cellSize)));
	int jMin = max(0, min(height - 1, (int)(aabb.min.GetY() / cellSize)));
	int jMax = max(0, min(height - 1, (int)(aabb.max.GetY() / cellSize)));
	ids.clear();
	for (int x = iMin; x <= iMax; x++)
	{
		for (int y = jMin; y <= jMax; y++)
		{
			int id = x + y * width;
			if (cells[id] < 0) continue;
			// inside pairs
			int count = (cells[id] >> 24) & 0xff;
			int start = cells[id] & 0xffffff;
			for (int i = 0; i < count; i++)
				ids.push_back(particles[start + i].particleId);
		}
	}
}