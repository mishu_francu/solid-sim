#include "Pendulum.h"

#include <limits>
#include <algorithm>

using namespace std;

// Jacobi elliptic functions taken from Numerical Recipes
// mc = kc ^ 2 = 1 - k^2
template<typename Real>
void JacobiEllipticInverse(Real u, Real mc, Real& sn, Real& cn, Real& dn)
{
	const Real ca = Real(1.0e-8); // accuracy (square of ca)
	const Real h(0.5);
	if (mc != 0.f)
	{
		bool negative = mc < 0;
		Real d;
		if (negative)
		{
			d = 1 - mc;
			mc *= d;
			d = sqrt(d);
			u *= d;
		}
		Real a = 1;
		dn = 1;
		Real em[13], en[13]; // why 13?
		Real emc = mc;
		Real c;
		int l;
		for (int i = 0; i < 13; i++)
		{
			l = i;
			em[i] = a;
			emc = sqrt(emc);
			en[i] = emc;
			c = h * (a + emc);
			if (fabs(a - emc) <= ca * a)
				break;
			emc *= a;
			a = c;
		}
		u *= c;
		sn = sin(u);
		cn = cos(u);
		if (sn != 0)
		{
			a = cn / sn;
			c *= a;
			for (int i = l; i >= 0; i--)
			{
				Real b = em[i];
				a *= c;
				c *= dn;
				dn = (en[i] + a) / (b + a);
				a = c / b;
			}
			a = 1.f / sqrt(c * c + 1);
			sn = sn >= 0 ? a : -a;
			cn = c * sn;
		}
		if (negative)
		{
			a = dn;
			dn = cn;
			cn = a;
			sn /= d;
		}
	}
	else
	{
		cn = 1.f / cosh(u); // cos hyperbolic
		dn = cn;
		sn = tanh(u); // tan hyperbolic
	}
}

template void JacobiEllipticInverse<float>(float u, float mc, float& sn, float& cn, float& dn);
template void JacobiEllipticInverse<double>(double u, double mc, double& sn, double& cn, double& dn);

// Carlson elliptic integral of first kind
/*Real rf(const Real x, const Real y, const Real z)
{
	const Real errTol = 0.08f; // 0.0025f for double
	const Real third = 1.f / 3.f;
	const Real c1 = 1.f / 24.f;
	const Real c2 = 0.1f;
	const Real c3 = 3.f / 44.f;
	const Real c4 = 1.f / 14.f;
	const Real tiny = 5.f * numeric_limits<Real>::min();
	const Real big = 0.2f * numeric_limits<Real>::min();
	if (min(min(x, y), z) < 0.f || min(min(x + y, x + z), y + z) < tiny || max(max(x, y), z) > big)
	{
		ASSERT(false);
		return 0.f;
	}
	Real xt = x;
	Real yt = y;
	Real zt = z;
	Real ave, delx, dely, delz;
	do {
		Real sqrtx = sqrt(xt);
		Real sqrty = sqrt(yt);
		Real sqrtz = sqrt(zt);
		Real alamb = sqrtx * (sqrty + sqrtz) + sqrty * sqrtz;
		xt = 0.25f * (xt + alamb);
		yt = 0.25f * (yt + alamb);
		zt = 0.25f * (zt + alamb);
		ave = third * (xt + yt + zt);
		delx = (ave - xt) / ave;
		dely = (ave - yt) / ave;
		delz = (ave - zt) / ave;
	} while (max(max(fabs(delx), fabs(dely)), fabs(delz)) > errTol);
	Real e2 = delx * dely - delz * delz;
	Real e3 = delx * dely * delz;
	return (1.f + (c1 * e2 - c2 - c3 * e3) * e2 + c4 * e3) / sqrt(ave);
}

// Legender elliptic integral of the first kind F(phi, k)
Real EllipticIntegral1(const Real phi, const Real k)
{
	Real s = sin(phi);
	Real c = cos(phi);
	return s * rf(c * c, (1.f - s * k) * (1.f + s * k), 1.f);
}*/
