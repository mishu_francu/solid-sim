#ifndef PARTICLE_SYSTEM_ACC_H
#define PARTICLE_SYSTEM_ACC_H

#include "ParticleSystem.h"

class ParticleSystemAcc : public ParticleSystem<ParticleVelocity>
{
public:
	void MicroStep() override;
	bool SupportsSolver(SolverType aSolver) override { return true; }
};

#endif // PARTICLE_SYSTEM_ACC_H
