#ifdef USE_OPENCL
#include <Demo/Particles/ParticleSystemVerlet.h>

//#define TEST_CL

#ifdef EMULATE_CL
/*class GaussSeidelSolver : public CLKernelFunctor
{
public:
	void Run(size_t* size)
	{
		// prepare arguments
		ParticleSystemVerlet::Edge* edges = (ParticleSystemVerlet::Edge*)args[0].buf->Get();
		Vector2* pos = (Vector2*)args[1].buf->Get();
		float* im = (float*)args[2].buf->Get();

		for (globalId = 0; globalId < size[0]; globalId++)
		{
			GaussSeidel(edges, pos, im);
		}
	}
private:
	void GaussSeidel(ParticleSystemVerlet::Edge* edges, Vector2* pos, const float* invMass)
	{
		int i = globalId;
		int i1 = edges[i].i1;
		int i2 = edges[i].i2;
		Vector2 delta = pos[i1] - pos[i2];
		float len = delta.Length();
		float len0 = edges[i].len;
		float lambda = (len - len0) / (invMass[i1] + invMass[i2]);
		Vector2 disp = (lambda / len) * delta;
		pos[i1] -= invMass[i1] * disp;
		pos[i2] += invMass[i2] * disp;
	}
};*/

class VerletIntegrator : public CLKernelFunctor
{
public:
	void Run(size_t* size)
	{
		Vector2* pos0 = (Vector2*)args[0].buf->Get();
		Vector2* pos = (Vector2*)args[1].buf->Get();
		float* im = (float*)args[2].buf->Get();
		Vector2* pos1 = (Vector2*)args[3].buf->Get();
		Vector2 force(args[3].vec[0], args[4].vec[1]);

		for (globalId = 0; globalId < size[0]; globalId++)
		{
			Integrate(pos0, pos, im, pos1, force);
		}
	}
private:
	void Integrate(const Vector2* prevPos, const Vector2* pos, const float* invMass, Vector2* newPos, const Vector2& force)
	{
		int i = globalId;
		Vector2 velocity = pos[i] - prevPos[i];
		newPos[i] = pos[i] + min(1.f, invMass[i]) * (velocity + force);
	}
};

class ConstraintErrorsComputer : public CLKernelFunctor
{
public:
	void Run(size_t* size)
	{
		ParticleSystemVerlet::Edge* edges = (ParticleSystemVerlet::Edge*)args[0].buf->Get();
		Vector2* normals = (Vector2*)args[1].buf->Get();
		float* c = (float*)args[2].buf->Get();
		Vector2* pos = (Vector2*)args[3].buf->Get();

		for (globalId = 0; globalId < size[0]; globalId++)
		{
			computeConstraintErrors(edges, normals, c, pos);
		}
	}
private:
	void computeConstraintErrors(const ParticleSystemVerlet::Edge* edges, Vector2* normals, float* c, Vector2* pos)
	{
		int i = globalId;
		Vector2 delta = pos[edges[i].i2] - pos[edges[i].i1];
		float len = delta.Length();
		float res = edges[i].len - len;
		c[i] = res;
		normals[i] = (1.f / len) * delta;
	}
};

class ParticleErrorsComputer : public CLKernelFunctor
{
public:
	void Run(size_t* size)
	{
		Vector2* normals = (Vector2*)args[0].buf->Get();
		float* c = (float*)args[1].buf->Get();
		ParticleSystemVerlet::Incidence* incidence = (ParticleSystemVerlet::Incidence*)args[2].buf->Get();
		Vector2* err = (Vector2*)args[3].buf->Get();

		for (globalId = 0; globalId < size[0]; globalId++)
		{
			computeParticleErrors(normals, c, incidence, err);
		}
	}
private:
	void computeParticleErrors(const Vector2* normals, const float* c, const ParticleSystemVerlet::Incidence* incidence, Vector2* err)
	{
		int i = globalId;
		Vector2 acc; // careful, only the float version inits to zero
		for(int j = 0; j < 4; j++)
		{
			int info = incidence[i].info[j];
			if (info == 0)
				break;
			int idx;
			float sgn;
			if (info > 0)
			{
				idx = info - 1;
				sgn = 1;
			}
			else
			{
				idx = -info - 1;
				sgn = -1;
			}
			acc += sgn * c[idx] * normals[idx];
		}
		err[i] = acc;
	}
};

class RelativeErrorsComputer: public CLKernelFunctor
{
public:
	void Run(size_t* size)
	{
		ParticleSystemVerlet::Edge* edges = (ParticleSystemVerlet::Edge*)args[0].buf->Get();
		Vector2* normals = (Vector2*)args[1].buf->Get();
		Vector2* err = (Vector2*)args[2].buf->Get();
		const float* invMass = (const float*)args[3].buf->Get();
		float* q = (float*)args[4].buf->Get();

		for (globalId = 0; globalId < size[0]; globalId++)
		{
			computeRelativeErrors(edges, normals, err, invMass, q);
		}
	}
private:
	void computeRelativeErrors(
		const ParticleSystemVerlet::Edge* edges, 
		const Vector2* normals, 
		const Vector2* err, 
		const float* invMass,
		float* q)
	{
		int i = globalId;
		int i1 = edges[i].i1;
		int i2 = edges[i].i2;
		Vector2 diff = invMass[i2] * err[i2] - invMass[i1] * err[i1];
		q[i] = normals[i].Dot(diff);
	}
};

class UpdatePositionsKernel : public CLKernelFunctor
{
public:
	void Run(size_t* size)
	{
		Vector2* pos = (Vector2*)args[0].buf->Get();
		const float* invMass = (const float*)args[1].buf->Get();
		Vector2* err = (Vector2*)args[2].buf->Get();
		float alpha = args[3].f;

		for (globalId = 0; globalId < size[0]; globalId++)
		{
			updatePositions(pos, invMass, err, alpha);
		}
	}
private:
	void updatePositions(Vector2* pos, const float* invMass, const Vector2* err, float alpha)
	{
		int i = globalId;
		pos[i] += alpha * invMass[i] * err[i];
	}
};

// TODO: namespace
VerletIntegrator integrator;
GaussSeidelSolver gsSolver;
ConstraintErrorsComputer constrErr;
ParticleErrorsComputer partErr;
RelativeErrorsComputer relErrKer;
UpdatePositionsKernel updatePosKer;
#endif

void ParticleSystemVerlet::InitKernels()
{
	program = compute.Build("../Shaders/Physics_Kernels.cl");
	// Create kernel object
	if (program)
	{
#ifdef EMULATE_CL
		integrateKernel.Create(&integrator);
		projectKernel.Create(&gsSolver);
		compConstrErr.Create(&constrErr);
		compPartErr.Create(&partErr);
		compRelErr.Create(&relErrKer);
		updatePos.Create(&updatePosKer);
#else
		integrateKernel.Create(compute, program, "integrate");
		projectKernel.Create(compute, program, "gaussSeidel");
		jacEdgesKernel.Create(compute, program, "jacobiEdges");
		jacParticlesKernel.Create(compute, program, "jacobiParticles");

		compConstrErr.Create(compute, program, "computeConstraintErrors");
		compPartErr.Create(compute, program, "computeParticleErrors");
		compRelErr.Create(compute, program, "computeRelativeErrors");
		updatePos.Create(compute, program, "updatePositions");
		reduceAlpha.Create(compute, program, "reduceAlpha");
		reduceFinal.Create(compute, program, "reduceFinal");

		compute.GetKernelWorkGroupInfo(reduceAlpha.Get());
#endif
		//sampleKernel = clCreateKernel(compute.GetProgram(), "sample", NULL);		
	}
}

void ParticleSystemVerlet::IntegrateVerletCL()
{
	//PROFILE_SCOPE("IntegrateCL");

	float h2 = lengthScale * timeStep * timeStep;
	Vector2 force = gravity;
	force.Scale(h2);
	const float cellSize = 2 * radius;
	int columns = (int)(width / cellSize + 1);
	int rows = (int)(height / cellSize + 1);
	ASSERT(columns * rows - 1 <= 0xffff);
	size_t n = GetNumParticles();

	bool createBuffers = prevNum != n;
	prevNum = n;
	
	int i1 = currBuff;
	int i2 = (currBuff + 1) % 3;
	int i3 = (currBuff + 2) % 3;
	currBuff = i2;

	// allocate the buffer memory objects
	if (createBuffers)
	{
		// TODO: release old ones
		for (int i = 0; i < 3; i++)
			buffer[i].Create(compute, CL_MEM_ALLOC_HOST_PTR, sizeof(Vector2) * n);
		//pic = clCreateBuffer(compute.GetContext(), CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, sizeof(int) * n, NULL, NULL);

		std::vector<std::vector<int> > incidence(n);
		std::vector<Edge> edgesTemp(links.size());
		std::vector<Edge> buckets[4];
		for (size_t i = 0; i < links.size(); i++)
		{
			int info = i + 1;
			incidence[links[i].i1].push_back(-info);
			incidence[links[i].i2].push_back(info);

			Edge edge(links[i]);
			edgesTemp[i] = edge;

			// for Gauss-Seidel
			ASSERT(links[i].batch >= 0 && links[i].batch < 4);
			buckets[links[i].batch].push_back(edge);
		}
		for (int i = 0; i < 4; i++)
		{
			batchSizes[i] = buckets[i].size();
			batches[i].Create(compute, CL_MEM_COPY_HOST_PTR, buckets[i].size() * sizeof(Edge), &(buckets[i])[0]);
		}

		edges.Create(compute, CL_MEM_COPY_HOST_PTR, sizeof(Edge) * links.size(), &edgesTemp[0]);
		disp.Create(compute, CL_MEM_ALLOC_HOST_PTR, sizeof(Vector2) * links.size());
		
		normals.Create(compute, CL_MEM_ALLOC_HOST_PTR, sizeof(Vector2) * links.size());
		constr.Create(compute, CL_MEM_ALLOC_HOST_PTR, sizeof(float) * links.size());
		relErr.Create(compute, CL_MEM_ALLOC_HOST_PTR, sizeof(float) * links.size());		
		err.Create(compute, CL_MEM_ALLOC_HOST_PTR, sizeof(Vector2) * n);

		std::vector<Incidence> incPacked(n);
		std::vector<float> im(n); // TODO: write as positions
		Vector2* prevPosPtr = (Vector2*)compute.EnqueueMap(buffer[i1], CL_FALSE, CL_MAP_WRITE);//_INVALIDATE_REGION);
		Vector2* posPtr = (Vector2*)compute.EnqueueMap(buffer[i2], CL_FALSE, CL_MAP_WRITE);//_INVALIDATE_REGION);
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			prevPosPtr[i] = particles[i].prevPos;
			posPtr[i] = particles[i].pos;
			im[i] = particles[i].invMass;

			// for Jacobi
			for (size_t j = 0; j < 4; j++)
				incPacked[i].info[j] = j < incidence[i].size() ? incidence[i][j] : 0;
		}
		compute.EnqueueUnmap(buffer[i1], prevPosPtr);
		compute.EnqueueUnmap(buffer[i2], posPtr);
		invMass.Create(compute, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float) * n, &im[0]);
		incEdges.Create(compute, CL_MEM_COPY_HOST_PTR, sizeof(Incidence) * n, &incPacked[0]);
	}

	// Sets integrate kernel arguments
	integrateKernel.SetArgument(0, &buffer[i1]); // prev pos
	integrateKernel.SetArgument(1, &buffer[i2]); // pos
	integrateKernel.SetArgument(2, &invMass);
	integrateKernel.SetArgument(3, &buffer[i3]); // new pos
	integrateKernel.SetArgument(4, &force);

	// Running the kernel.
	size_t global_work_size[1] = { n };
	compute.EnqueueKernel(integrateKernel, global_work_size);

	// the grid sampling kernel
	//status = clSetKernelArg(sampleKernel, 0, sizeof(cl_mem), &buffer[i3]); // new pos
	//status = clSetKernelArg(sampleKernel, 1, sizeof(cl_mem), &pic); // new pos
	//status = clSetKernelArg(sampleKernel, 2, sizeof(float), &cellSize);
	//status = clSetKernelArg(sampleKernel, 3, sizeof(int), &columns);
	//status = clSetKernelArg(sampleKernel, 4, sizeof(int), &rows);
	////size_t global_work_size[1] = { n };
	//status = clEnqueueNDRangeKernel(compute.GetCommandQueue(), sampleKernel, 1, NULL, global_work_size, NULL, 0, NULL, NULL);

	// read back particles in cell
	//int* picPtr = (int*)clEnqueueMapBuffer(compute.GetCommandQueue(), pic, CL_FALSE, CL_MAP_READ, 0, n * sizeof(int), 0, NULL, NULL, NULL );
	//Printf("Sample grid\n");
	//for (size_t i = 0; i < n; i++)
	//{
	//	uint16 particle = picPtr[i] >> 16;
	//	uint16 cell = picPtr[i] & 0xffff;
	//	Printf("%d - %d\n", particle, cell);
	//}
	//clEnqueueUnmapMemObject(compute.GetCommandQueue(), buffer[i3], ptr, 0, NULL, NULL);
}

void ParticleSystemVerlet::ProjectPositionsJacobiCL()
{
	PROFILE_SCOPE("ProjectJacobiCL");
	const int i3 = (currBuff + 1) % 3;
	const size_t n = GetNumParticles();

	jacEdgesKernel.SetArgument(0, &edges);
	jacEdgesKernel.SetArgument(1, &buffer[i3]); // TODO: this the only one that changes
	jacEdgesKernel.SetArgument(2, &invMass);
	jacEdgesKernel.SetArgument(3, &disp);
	
	jacParticlesKernel.SetArgument(0, &buffer[i3]);
	jacParticlesKernel.SetArgument(1, &invMass);
	jacParticlesKernel.SetArgument(2, &incEdges);
	jacParticlesKernel.SetArgument(3, &disp);

	size_t edges_work_size[1] = { links.size() };
	size_t global_work_size[1] = { n };
	for (int k = 0; k < numIterations; ++k)
	{
		compute.EnqueueKernel(jacEdgesKernel, edges_work_size);
		compute.EnqueueKernel(jacParticlesKernel, global_work_size);
	}

	// TODO: deferred reading
	// read back positions
	Vector2* ptr = (Vector2*)compute.EnqueueMap(buffer[i3], true, CL_MAP_READ);
	// TODO: memcpy
	for (size_t i = 0; i < n; i++)
	{
		particles[i].prevPos = particles[i].pos;
		particles[i].pos = ptr[i];
	}
	compute.EnqueueUnmap(buffer[i3], ptr);
}

void ParticleSystemVerlet::ProjectPositionsGS_CL()
{
	PROFILE_SCOPE("ProjectGS CL");

	const int i3 = (currBuff + 1) % 3;
	const size_t n = GetNumParticles();
	projectKernel.SetArgument(1, &buffer[i3]);
	projectKernel.SetArgument(2, &invMass);

	for (int k = 0; k < numIterations; ++k)
	{
		for (int i = 0; i < 4; i++)
		{			
			//PROFILE_SCOPE("Batches");
			projectKernel.SetArgument(0, &batches[i]);
			size_t edges_work_size[1] = { batchSizes[i] }; // TODO: include it in CLBuffer?
			compute.EnqueueKernel(projectKernel, edges_work_size);
		}
	}

	// read back positions
	Vector2* ptr = (Vector2*)compute.EnqueueMap(buffer[i3], true, CL_MAP_READ); 
	for (size_t i = 0; i < n; i++)
	{
		particles[i].prevPos = particles[i].pos;
		particles[i].pos = ptr[i];
	}
	compute.EnqueueUnmap(buffer[i3], ptr);
}

void ParticleSystemVerlet::ProjectPositionsNMR_CL()
{
	PROFILE_SCOPE("ProjectNMR CL");
	const int i3 = (currBuff + 1) % 3;
	const size_t n = GetNumParticles();

	compConstrErr.SetArgument(0, &edges);
	compConstrErr.SetArgument(1, &normals);
	compConstrErr.SetArgument(2, &constr);
	compConstrErr.SetArgument(3, &buffer[i3]);

	compPartErr.SetArgument(0, &normals);
	compPartErr.SetArgument(1, &constr);
	compPartErr.SetArgument(2, &incEdges);
	compPartErr.SetArgument(3, &err);

	compRelErr.SetArgument(0, &edges);
	compRelErr.SetArgument(1, &normals);
	compRelErr.SetArgument(2, &err);
	compRelErr.SetArgument(3, &invMass);
	compRelErr.SetArgument(4, &relErr);

	size_t edges_work_size[1] = { links.size() };
	size_t localSize = 8;//links.size();
	//Printf("mod: %d\n", links.size() % localSize);
	size_t numGroups = links.size() / localSize;
	reduction.Create(compute, CL_MEM_ALLOC_HOST_PTR, sizeof(Vector2) * numGroups);		

	reduceAlpha.SetArgument(0, &relErr);
	reduceAlpha.SetArgument(1, &constr);
	reduceAlpha.SetArgument(2, edges_work_size);
	reduceAlpha.SetArgument(3, localSize * sizeof(float));
	reduceAlpha.SetArgument(4, &reduction);

	reduceFinal.SetArgument(0, &reduction);

	updatePos.SetArgument(0, &buffer[i3]);
	updatePos.SetArgument(1, &invMass);
	updatePos.SetArgument(2, &err);
	updatePos.SetArgument(3, &reduction);

	size_t particles_work_size[1] = { GetNumParticles() };
	for (int k = 0; k < numIterations; ++k)
	{
		compute.EnqueueKernel(compConstrErr, edges_work_size);
		compute.EnqueueKernel(compPartErr, particles_work_size);
		//compute.EnqueueKernel(compRelErr, edges_work_size);
		//compute.EnqueueKernel(reduceAlpha, edges_work_size, &localSize);
		//compute.EnqueueKernel(reduceFinal, &numGroups);
		compute.EnqueueKernel(updatePos, particles_work_size);
	}

	// read back positions
	Vector2* ptr = (Vector2*)compute.EnqueueMap(buffer[i3], true, CL_MAP_READ);
	for (size_t i = 0; i < n; i++)
	{
		particles[i].prevPos = particles[i].pos;
		particles[i].pos = ptr[i];
	}
	compute.EnqueueUnmap(buffer[i3], ptr);
}

void ParticleSystemVerlet::ProjectParticles()
{
	PROFILE_SCOPE("ProjectParticles");
	NeighbourArray* particleLinks = new NeighbourArray[GetNumParticles()];
	for (size_t i = 0; i < neighbours.size(); i++)
	{
		for (size_t j = 0; j < neighbours[i].size(); j++)
		{
			particleLinks[i].array[j] = neighbours[i][j];
		}
	}

	for (int k = 0; k < numIterations; ++k)
	{
		// compute lambdas
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			int j = 0;
			while (particleLinks[i].array[j].idx != -1)
			{
				int adj = particleLinks[i].array[j].idx;
				Vector2 delta = particles[i].pos - particles[adj].pos;
				float len = delta.Length();
				float len0 = particleLinks[i].array[j].len0;
				const float lambda = (len - len0) / (particles[i].invMass + particles[adj].invMass);
				particleLinks[i].array[j].disp = (lambda / len) * delta;
				j++;
			}
		}
		// apply lambdas
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			int j = 0;
			while (particleLinks[i].array[j].idx != -1)
			{
				int adj = particleLinks[i].array[j].idx;
				const Vector2& delta = particleLinks[i].array[j].disp;
				particles[i].pos.Subtract(delta * particles[i].invMass);
				particles[adj].pos.Add(delta * particles[adj].invMass);
				j++;
			}
		}
	}
}

#endif // USE_OPENCL