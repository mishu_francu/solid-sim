#include "Particles2DDemo.h"
#include "Engine/xml.h"
#include "PhysicsSystem.h"
#include <Graphics2D/Graphics2D.h>
#include <Demo/Particles/ParticleSystemVerlet.h>
#include <Demo/Particles/ParticleSystemPbd.h>
#include <Demo/Particles/ParticleSystemSeqImp.h>
#include <Demo/Particles/ParticleSystemSeqPos.h>
#include <Demo/Particles/ParticleSystemPenalty.h>
#include <Demo/Particles/ParticleSystemShake.h>
#include <Demo/Particles/ParticleSystemRattle.h>
#include <Demo/Particles/ParticleSystemSpook.h>
#include <Demo/Particles/ParticleSystemAcc.h>
#include <Demo/Particles/ParticleSystemSph.h>
#include <Demo/Particles/ParticleSystemGravity.h>
#include <Demo/Particles/ParticleSystemNonlinear.h>
#include <Demo/Particles/ParticleSystemImpVel.h>

Particles2DDemo::Particles2DDemo()
	: mRadius(10)
	, mDivisions(5)
	, mNumParticles(100)
{
}

bool Particles2DDemo::LoadFromXml(XMLElement* xRoot)
{
	XMLVariable* xNum = xRoot->FindVariableZ("num");
	if (xNum)
	{
		mNumParticles = xNum->GetValueInt();
	}

	XMLVariable* xDiv = xRoot->FindVariableZ("divisions");
	if (xDiv)
	{
		mDivisions = xDiv->GetValueInt();
	}

	char str[32];
	XMLVariable* xRadius = xRoot->FindVariableZ("radius");
	if (xRadius)
	{
		xRadius->GetValue(str);
		mRadius = (float)atof(str);
	}

	XMLVariable* xMethod = xRoot->FindVariableZ("method");
	if (xMethod && mParticleSys.get() == nullptr)
	{
		xMethod->GetValue(str);
		if (strcmp(str, "pbd") == 0)
		{
			mParticleSys.reset(new ParticleSystemPbd<>());
		}
		else if (strcmp(str, "verlet") == 0)
		{
			mParticleSys.reset(new ParticleSystemVerlet());
		}
		else if (strcmp(str, "si") == 0)
		{
			mParticleSys.reset(new ParticleSystemSeqImp());
		}
		else if (strcmp(str, "sp") == 0)
		{
			mParticleSys.reset(new ParticleSystemSeqPos());
		}
		else if (strcmp(str, "sph") == 0)
		{
			mParticleSys.reset(new ParticleSystemSph());
		}
		else if (strcmp(str, "acc") == 0)
		{
			mParticleSys.reset(new ParticleSystemAcc());
		}
		else if (strcmp(str, "shake") == 0)
		{
			mParticleSys.reset(new ParticleSystemShake());
		}
		else if (strcmp(str, "rattle") == 0)
		{
			mParticleSys.reset(new ParticleSystemRattle());
		}
		else if (strcmp(str, "spook") == 0)
		{
			mParticleSys.reset(new ParticleSystemSpook());
		}
		else if (strcmp(str, "nonlinear") == 0)
		{
			mParticleSys.reset(new ParticleSystemNonlinear());
		}
		else if (strcmp(str, "ivp") == 0)
		{
			mParticleSys.reset(new ParticleSystemImpVel());
		}
		else
		{
			Printf("Unknown simulation method\n");
			return false;
		}
		
		XMLVariable* xNum = xRoot->FindVariableZ("iterations");
		if (xNum)
		{
			mParticleSys->SetNumIterations(xNum->GetValueInt());
		}

		xNum = xRoot->FindVariableZ("iterations2");
		if (xNum)
		{
			mParticleSys->SetNumIterationsPost(xNum->GetValueInt());
		}

		XMLVariable* xSolver = xRoot->FindVariableZ("solver");
		if (xSolver)
		{
			xSolver->GetValue(str);
			if (strcmp(str, "gs") == 0)
			{
				mParticleSys->SetSolver(PhysicsSystem::GAUSS_SEIDEL);
			}
			if (strcmp(str, "jacobi") == 0)
			{
				mParticleSys->SetSolver(PhysicsSystem::JACOBI);
			}
			else if (strcmp(str, "mr") == 0)
			{
				mParticleSys->SetSolver(PhysicsSystem::MIN_RES);
			}
			else if (strcmp(str, "cr") == 0)
			{
				mParticleSys->SetSolver(PhysicsSystem::CONJ_RES);
			}
			else if (strcmp(str, "exact") == 0)
			{
				mParticleSys->SetSolver(PhysicsSystem::EXACT);
			}
			else if (strcmp(str, "jcr") == 0)
			{
				mParticleSys->SetSolver(PhysicsSystem::JACOBI_CR);
			}
			else if (strcmp(str, "soft") == 0)
			{
				mParticleSys->SetSolver(PhysicsSystem::SOFT_CONSTR);
			}
			else if (strcmp(str, "nesterov") == 0)
			{
				mParticleSys->SetSolver(PhysicsSystem::NESTEROV);
			}
		}

		XMLVariable* xSteps = xRoot->FindVariableZ("steps");
		if (xSteps)
		{
			int steps = xSteps->GetValueInt();
			mParticleSys->SetNumMicroSteps(steps);
		}

		XMLVariable* xMu = xRoot->FindVariableZ("mu");
		if (xMu)
		{
			xMu->GetValue(str);
			float mu = (float)atof(str);
			mParticleSys->SetFriction(mu);
		}

		XMLVariable* xTol = xRoot->FindVariableZ("tolerance");
		if (xTol)
		{
			xTol->GetValue(str);
			float tol = (float)atof(str);
			mParticleSys->SetTolerance(tol);
		}

		XMLVariable* xBeta = xRoot->FindVariableZ("beta");
		if (xBeta)
		{
			xBeta->GetValue(str);
			float beta = (float)atof(str);
			mParticleSys->SetBaumgarte(beta);
		}

		XMLVariable* xCollide = xRoot->FindVariableZ("collide");
		if (xCollide)
		{
			mParticleSys->SetDetectCollision(xCollide->GetValueInt() != 0);
		}
	}

	return true;
}

void Particles2DDemo::SetMaxFrames(int val)
{
	if (mParticleSys)
		mParticleSys->SetMaxFrames(val);
}

void Particles2DDemo::DrawParticles(Graphics2D* graphics)
{
	if (!mParticleSys)
		return;
	const float rr = 2 * mRadius;
	Vector2 b = mParticleSys->GetParticle(0).pos;
	size_t numParticles = mParticleSys->GetNumParticles();
	Vector2 e = mParticleSys->GetParticle(numParticles - 1).pos;
	Vector2 p0 = b;
	std::vector<float> u(numParticles);
	for (size_t i = 0; i < numParticles; i++)
	{
		const ParticleBase& particle = mParticleSys->GetParticle(i);
		int color = 0;
		if (particle.invMass > 0)
			color = (int)(255 * log(0.01f / particle.invMass));
		graphics->SetColor(0xff007f00 | (color & 0xff));
		graphics->DrawCircle(particle.pos.GetX(), particle.pos.GetY(), mRadius);
	}
}

void Particles2DDemo::Draw(Graphics2D* graphics, int color)
{
	const float r = mRadius * 0.7f;
	const float rr = 2 * r;
	//if (!replayMode) 
	{
		if (mDemoType == DEMO_PARTICLES || mDemoType == DEMO_LINKS || mDemoType == DEMO_LATTICE || mDemoType == DEMO_CLOTH)
			DrawParticles(graphics);
		
		// draw links
		if (mDemoType == DEMO_CLOTH)
		{
			graphics->SetColor(color);
			graphics->DrawLineMesh(mParticleSys->GetLinksMesh());
		}

#ifndef FIXED_PIPELINE
		if (mDemoType == DEMO_LINKS || mDemoType == DEMO_LATTICE)
#endif
		{
			for (size_t i = 0; mParticleSys && i < mParticleSys->GetLinks().size(); i++)
			{
				const Constraint& link = mParticleSys->GetLinks()[i];
				if (link.type != Constraint::CONTACT)
				{
					graphics->SetColor(color);
					if (link.batch == 0)
						graphics->SetColor(0xffff0000);
					else if (link.batch == 1)
						graphics->SetColor(0xff00ff00);
					else if (link.batch == 2)
						graphics->SetColor(0xff0000ff);
					else if (link.batch == 3)
						graphics->SetColor(0xffffff00);
					graphics->DrawLine(mParticleSys->GetParticle(link.i1).pos, mParticleSys->GetParticle(link.i2).pos);
				}
			}
		}
	}

	// draw boxes (colliders)
	graphics->SetColor(0xff00ffff);
	for (size_t i = 0; mParticleSys && i < mParticleSys->GetNumBoxes(); i++)
	{
		Primitive* prim = mParticleSys->GetBox(i).get();
		if (prim->type == Primitive::OBB)
		{
			const Obb& obb = *(const Obb*)prim;
			float deg = DEGREE(obb.angle);
			const Vector2& c = obb.pos;
			graphics->Rotate(deg);
			graphics->Translate(c.GetX(), c.GetY());
			Vector2 p = -1.f * obb.extent; // TODO: minus operator
			Vector2 size = 2.f * obb.extent;
			graphics->DrawRect(p.GetX(), p.GetY(), size.GetX(), size.GetY());
			graphics->ResetTransform();
		}
		else if (prim->type == Primitive::CIRCLE)
		{
			const Circle* circ = (const Circle*)prim;
			graphics->DrawCircle(circ->pos.GetX(), circ->pos.GetY(), circ->radius);
		}
		else if (prim->type == Primitive::SEGMENT)
		{
			const Segment* segm = (const Segment*)prim;
			graphics->DrawLine(segm->a, segm->b);
		}
	}
}

void Particles2DDemo::InitBoxes(int numBoxes, float width, float height)
{
	const float boxSize = 40.f;
	mParticleSys->SetNumBoxes(numBoxes);
	for (int i = 0; i < numBoxes; i++) 
	{
		bool overlap = true;
		while (overlap) 
		{
			float extentX = boxSize + rand() % 60;
			float extentY = boxSize + rand() % 60;
			float x = GetRandomReal01() * (width - extentX);
			float y = GetRandomReal01() * (height - extentY);
			if ((rand() & 1) == 0)
			{
				Obb* obb = new Obb();
				obb->extent.Set(extentX, extentY);
				obb->pos.Set(x, y);
				obb->angle = GetRandomReal01() * 2 * PI;
				mParticleSys->GetBox(i).reset(obb);
			}
			else if ((rand() & 1) == 0)
			{
				Circle* circ = new Circle();
				circ->radius = extentX;
				circ->pos.Set(x, y);
				mParticleSys->GetBox(i).reset(circ);
			}
			else
			{
				Segment* segm = new Segment();
				segm->a.Set(GetRandomReal01() * width, GetRandomReal01() * height);
				segm->b.Set(GetRandomReal01() * width, GetRandomReal01() * height);
				mParticleSys->GetBox(i).reset(segm);
			}
			overlap = false;
		}
	}
}

bool Particles2DDemo::OverlapsBoxes(const Vector2& p)
{
	Vector2 rad(mRadius, mRadius);
	bool overlap = false;
	Vector2 intersect, normal;
	for (size_t j = 0; j < mParticleSys->GetNumBoxes(); j++)
	{
		if (IntersectPrimitive(p, mRadius, mParticleSys->GetBox(j).get(), intersect, normal))
		{
			overlap = true;
			break;
		}
	}
	return overlap;
}

void Particles2DDemo::InitParticles(float width, float height)
{
	mParticleSys->Init(width, height, mRadius);
	mParticleSys->SetNumParticles(mNumParticles);

	// initialize the collision boxes (randomly across the screen)
	InitBoxes(0, width, height);

	// intialize particles
	const float spacing = 0.2f * mRadius;
	float x = mRadius + spacing;
	float y = mRadius + spacing;
	const float maxVel = 0.1f * mParticleSys->GetLengthScale();
	size_t i = 0;
	while (i < mParticleSys->GetNumParticles())
	{
		// set position
		Vector2 p(x, y);
		mParticleSys->GetParticle(i).pos = p;
		
		// increment to next position
		x += 2 * mRadius + spacing;
		if (x > width - mRadius - spacing)
		{
			y += 2 * mRadius + spacing;
			x = mRadius + spacing;
		}
		
		// check for intersection with boxes		
		if (OverlapsBoxes(p))
			continue;
		
		// set velocity
		Vector2 dr;
		dr.Set(-maxVel * GetRandomReal01(), maxVel * GetRandomReal11());
		mParticleSys->SetParticleVelocity(i, dr);
		
		mParticleSys->GetParticle(i).SetMass(1);

		i++;
	}
}

void Particles2DDemo::InitCloth(float width, float height)
{
	mParticleSys->Init(width, height, mRadius);
	const int n = mDivisions; // number of links
	const float startY = mRadius;
	const float stopY = height * 0.9f;
	mParticleSys->SetNumParticles(n * n);
	float inc = (stopY - startY) / n;
	float x = 0.5f * (width - (n - 1) * inc);
	float h = mParticleSys->GetTimeStep();
	const float shear = 20 * h * h;

	for (int j = 0, base = 0; j < n; j++, base += n)
	{
		mParticleSys->GetParticle(base).pos.Set(x, startY);
		mParticleSys->SetParticleVelocity(base, Vector2(0.f, 0.f));
		int batchH = 2 + (j & 1);
		if (j == 0 || j == n - 1)
			mParticleSys->GetParticle(base).invMass = 0.f;
		if (base >= n)
		{
			Constraint* link = mParticleSys->AddLink(base - n, base);
			link->batch = batchH;
			mParticleSys->AddLink(base - n + 1, base, shear);
		}
		for (int i = 1; i < n; i++)
		{
			int idx = base + i;
			mParticleSys->GetParticle(idx).pos.Set(x, startY + inc * i);
			mParticleSys->GetParticle(idx).invMass = 1;
			if (idx > base)
			{
				Constraint* linkV = mParticleSys->AddLink(idx - 1, idx);
				linkV->batch = i & 1;
				if (idx >= n)
				{
					Constraint* linkH = mParticleSys->AddLink(idx - n, idx);
					linkH->batch = batchH;
					if (i < n - 1)
						mParticleSys->AddLink(idx - n + 1, idx, shear);
				}
				if (idx > n)
					mParticleSys->AddLink(idx - n - 1, idx, shear);
			}
		}
		x += inc;
	}

	if (mParticleSys->GetSolver() == PhysicsSystem::MIN_RES)
		mParticleSys->BuildIncidenceMatrix();
}

void Particles2DDemo::InitLinks(float width, float height)
{
	if (!mParticleSys)
		return;

	// configuration
	bool horizontal = false;
	bool middle = false;
	bool fixedEnd = false;
	bool noGravity = false;
	const float initialVel = 20.0f;

	mParticleSys->Init(width, height, mRadius);
	if (noGravity)
		mParticleSys->GetGravity().SetZero();
	const int n = mDivisions; // number of particles
	const float startY = middle ? height * 0.5f : mRadius;
	const float stopY = height * 0.9f;
	mParticleSys->SetNumParticles(n);
	float inc = (stopY - startY) / (n - 1);
	mParticleSys->GetParticle(0).invMass = 0.f;
	if (fixedEnd)
		mParticleSys->GetParticle(n - 1).invMass = 0.f;
	for (int i = 0; i < n; i++)
	{
		if (horizontal)
			mParticleSys->GetParticle(i).pos.Set(width * 0.4f + startY + inc * i, 0); // drop
		else
			mParticleSys->GetParticle(i).pos.Set(width * 0.5f, startY + inc * i);
		mParticleSys->SetParticleVelocity(i, Vector2(0, 0));
		if (i > 0)
		{
			mParticleSys->AddLink(i - 1, i);
		}
	}
	if (!horizontal)
		mParticleSys->SetParticleVelocity(n - 1, Vector2(initialVel * mParticleSys->GetLengthScale(), 0));
}

void Particles2DDemo::InitLattice(float width, float height)
{
	if (!mParticleSys)
		return;

	mParticleSys->Init(width, height, mRadius);
	mParticleSys->SetNumParticles(4);

	const float a = 200;
	const float b = 100;
	mParticleSys->GetParticle(0).pos.Set(a, b);
	mParticleSys->GetParticle(1).pos.Set(a + 100, b);
	mParticleSys->GetParticle(2).pos.Set(a + 100, b + 100);
	mParticleSys->GetParticle(3).pos.Set(a, b + 100);

	mParticleSys->GetParticle(0).invMass = 0.f;
	mParticleSys->GetParticle(3).invMass = 0.f;

	mParticleSys->AddLink(0, 1);
	mParticleSys->AddLink(1, 2);
	mParticleSys->AddLink(2, 3);
	mParticleSys->AddLink(3, 0);
	mParticleSys->AddLink(0, 2);
	mParticleSys->AddLink(1, 3);
}

void Particles2DDemo::InitCradle(float width, float height) 
{
	const float radius = 20.f;
	mParticleSys->Init(width, height, radius);
	const int n = 3; // number of pendulums
	mParticleSys->SetNumParticles(n);
	const float stride = 2 * radius + 1.f;
	const float length = 80.f;
	float startX = (width - n * stride) * 0.5f;
	for (int i = 0; i < n; i++)
	{
		mParticleSys->GetParticle(i).pos.Set(startX, length);
		mParticleSys->SetParticleVelocity(i, Vector2(0, 0));
		startX += stride;
	}
	mParticleSys->SetParticleVelocity(0, Vector2(-10.f * mParticleSys->GetLengthScale(), 0.f));
}

void Particles2DDemo::Init(float width, float height)
{
	if (mDemoType == DEMO_PARTICLES)
		InitParticles(width, height);
	else if (mDemoType == DEMO_CLOTH)
		InitCloth(width, height);
	else if (mDemoType == DEMO_LINKS)
		InitLinks(width, height);
	else if (mDemoType == DEMO_LATTICE)
		InitLattice(width, height);
	else
		assert(true);
}

void Particles2DDemo::Update()
{
	if (mParticleSys)
		mParticleSys->Step();
}

void Particles2DDemo::TestMethod()
{
	const float width = 1000;
	const float height = 1000;
	for (int demo = DEMO_PARTICLES; demo < DEMO_LATTICE; demo++)
	{
		mDemoType = (DemoType)demo;
		Init(width, height);
		
		for (int solver = PhysicsSystem::JACOBI; solver <= PhysicsSystem::JACOBI_CR; solver++)
		{
			if (!mParticleSys->SupportsSolver((PhysicsSystem::SolverType)solver))
				continue;
			mParticleSys->SetSolver((PhysicsSystem::SolverType)solver);
			for (int frame = 0; frame < 10; frame++)
				Update();
		}
	}
}

void Particles2DDemo::Test()
{
	mDivisions = 5;
	mNumParticles = 100;

	mParticleSys.reset(new ParticleSystemPbd<>());
	TestMethod();

	mParticleSys.reset(new ParticleSystemVerlet());
	TestMethod();

	mParticleSys.reset(new ParticleSystemSeqImp());
	TestMethod();

	mParticleSys.reset(new ParticleSystemSeqPos());
	TestMethod();

	mParticleSys.reset(new ParticleSystemSph());
	TestMethod();

	mParticleSys.reset(nullptr);
}