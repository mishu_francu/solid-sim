#ifndef PARTICLE_SYSTEM_VERLET_H
#define PARTICLE_SYSTEM_VERLET_H

#include "ParticleSystem.h"
#include <Demo/Particles/Projector.h>
#ifndef ANDROID_NDK
//#	include "ProjectorCL.h"
#endif

// TODO: move to another file
#if USE_SSE == 1
struct FourVectors
{
	__m128 x, y;

	FourVectors() { }

	FourVectors(const Vector2& v)
	{
		// TODO: shuffle
		x = _mm_set1_ps(v.GetX());
		y = _mm_set1_ps(v.GetY());
	}

	FourVectors& operator +=(const FourVectors& v)
	{
		x = _mm_add_ps(x, v.x);
		y = _mm_add_ps(y, v.y);
		return *this;
	}

	vector4 Length()
	{
		vector4 x2 = _mm_mul_ps(x, x);
		vector4 y2 = _mm_mul_ps(y, y);
		vector4 l2 = _mm_add_ps(x2, y2);
		return _mm_sqrt_ps(l2);
	}

	vector4 Normalize()
	{
		vector4 x2 = _mm_mul_ps(x, x);
		vector4 y2 = _mm_mul_ps(y, y);
		vector4 l2 = _mm_add_ps(x2, y2);
		//vector4 lrcp = _mm_rsqrt_ps(l2);
		//x = _mm_mul_ps(x, lrcp);
		//y = _mm_mul_ps(y, lrcp);
		vector4 l = _mm_sqrt_ps(l2);
		x = _mm_div_ps(x, l);
		y = _mm_div_ps(y, l);
		return l;
	}
};

inline FourVectors operator +(const FourVectors& a, const FourVectors& b)
{
	FourVectors c;
	c.x = _mm_add_ps(a.x, b.x);
	c.y = _mm_add_ps(a.y, b.y);
	return c;
}

inline FourVectors operator -(const FourVectors& a, const FourVectors& b)
{
	FourVectors c;
	c.x = _mm_sub_ps(a.x, b.x);
	c.y = _mm_sub_ps(a.y, b.y);
	return c;
}

inline FourVectors operator *(vector4 a, const FourVectors& b)
{
	FourVectors c;
	c.x = _mm_mul_ps(a, b.x);
	c.y = _mm_mul_ps(a, b.y);
	return c;
}

struct FourLinks
{
	FourVectors pos1, pos2, normal;
	vector4 len, sim, lambda;
	// for min res
	//vector4 r;
};

#endif

// PBD using the Verlet integrator
class ParticleSystemVerlet : public ParticleSystem<ParticleVerlet>
{
public:
	struct Edge
	{		
		int i1, i2;
		float len;

		Edge() : i1(-1), i2(-1), len(0) { }
		Edge(const Constraint& c) : i1(c.i1), i2(c.i2), len(c.len) { }
	};

	struct Incidence
	{
		int info[4];
	};

private:
	// TODO: deprecate
	struct Neighbour
	{
		int idx;
		float len0;
		Vector2 disp;
		Neighbour(int i, float l) : idx(i), len0(l){ }
		Neighbour() : idx(-1), len0(0) { }
	};

	struct NeighbourArray
	{
		enum { NUM = 4 };
		Neighbour array[NUM];		
	};

private:
	std::vector<std::vector<Neighbour> > neighbours;

#if USE_SSE == 1
	std::vector<FourLinks> links4;
#endif

	size_t prevNum;
	int currBuff;
#ifdef USE_OPENCL
	CLKernel integrateKernel, projectKernel;
	CLKernel jacEdgesKernel, jacParticlesKernel;
	CLKernel compConstrErr, compPartErr, compRelErr, updatePos, reduceAlpha, reduceFinal;
	
	cl_program program;
	CLBuffer edges, disp, incEdges;
	CLBuffer buffer[3], invMass, batches[4];
	CLBuffer constr, err, normals, relErr, reduction;
	size_t batchSizes[4];

	GSProjectorCL<Vector2, ParticleVerlet> projCL;
#endif

	Projector<ParticleVerlet> proj;

	std::vector<Vector2> forceAcc;

	std::vector<float> ev; // initial guess for eigevenvector 
	float rho; // spectral radius

public:
	ParticleSystemVerlet();
	void MicroStep() override;
	bool SupportsSolver(SolverType solver) override;

private:
	void ProjectPairVelocities(const Constraint& pair, float damping);
	
	//void InitKernels();
	void IntegrateVerletCL();
	void ProjectPositionsGS_CL();
	void ProjectPositionsNMR_CL();
	void ProjectPositionsJacobiCL();
	void ProjectParticles();

	void IntegrateVerletSSE();
	void ProjectLinksJacobi();
	void ProjectLinksGS();
	void ProjectLinksJacobiSSE();
	void ProjectLinksGaussSeidelSSE();
	void ProjectPositionsMinResSSE(bool gsWarmStart);

	void FirstTimeInit();

	void AddDampingForces();
};

#endif // PARTICLE_SYSTEM_VERLET_H
