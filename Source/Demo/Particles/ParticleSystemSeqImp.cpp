#include <Engine/Base.h>
#include "ParticleSystemSeqImp.h"

void ParticleSystemSeqImp::MicroStep()
{
	Collide();

	// integrate velocities
	Vector2 g = gravity;
	g.Scale(lengthScale * timeStep);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		particles[i].velocity.Add(g);
		particles[i].prevVel = particles[i].velocity;
		particles[i].prevPos = particles[i].pos;
	}

	ProjectVelocities();

	// update positions
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		particles[i].pos = particles[i].prevPos + timeStep * particles[i].velocity;
	}

    // post-stabilization
    if (0)
    {
        ProjectPositions(100);
        // update velocities
        for (size_t i = 0; i < GetNumParticles(); i++)
        {
            if (particles[i].invMass == 0.f)
                continue;
            particles[i].velocity = (1.f / timeStep) * (particles[i].pos - particles[i].prevPos);
        }
    }

	LogMetrics();
}

bool ParticleSystemSeqImp::SupportsSolver(SolverType aSolver)
{
	return aSolver == EXACT || aSolver == GAUSS_SEIDEL || aSolver == JACOBI || aSolver == MIN_RES || aSolver == CONJ_RES || aSolver == NESTEROV;
}
