#ifndef PARTICLE_SYSTEM_SI_H
#define PARTICLE_SYSTEM_SI_H

#include "ParticleSystemVelocity.h"

// Sequential impulses - velocity time stepping using a relaxation solver (GS or Jacobi)

class ParticleSystemSeqImp : public ParticleSystemVelocity
{
public:
	void MicroStep() override;
	bool SupportsSolver(SolverType solver) override;
};

#endif // PARTICLE_SYSTEM_SI_H
