#include "ParticleSystemShake.h"
#include <Math/Matrix.h>

void ParticleSystemShake::MicroStep()
{
	StoreNormalsForShake();

	// integrate candidate position
	IntegrateVerlet();

	// copy links over to constraints
	constraints.insert(constraints.end(), links.begin(), links.end());

	// position projection step
	if (solver == EXACT)
		ProjectNewton();
	else if (solver == GAUSS_SEIDEL)
		ProjectExplicit();
	else
		assert(false);
}

bool ParticleSystemShake::SupportsSolver(SolverType aSolver)
{
	return aSolver == EXACT || aSolver == GAUSS_SEIDEL;
}

