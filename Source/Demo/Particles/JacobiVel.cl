#include "Common.cl"

__kernel void jacobiEdges(__global Edge* edges, __global float4* vel, __global float* invMass, __global float4* disp, __global float4* normals, float beta)
{
	int i = get_global_id(0);
	int i1 = edges[i].i1;
	int i2 = edges[i].i2;

	float4 v12;
	// TODO: get rid of the branch
	if (edges[i].type == COLL_PAIR)
	{			
		v12 = vel[i2] - vel[i1];
	}
	else if (edges[i].type == CONTACT)
	{
		v12 = -vel[i1];
	}

	float vnrel = dot(normals[i], v12);
	float err = vnrel + edges[i].len;

	float alpha = 0.5f / (invMass[i1] + invMass[i2]);
	float dLambda = alpha * err + beta * dot(disp[i], normals[i]); // TODO: store prev dlambda?

	float lambda0 = edges[i].stiff;
	edges[i].stiff = max(0.f, lambda0 + dLambda);
	dLambda = edges[i].stiff - lambda0;

	disp[i] = dLambda * normals[i];
}

__kernel void jacobiParticles(__global float4* vel, __global const float* invMass, __global const Incidence* incidence, __global float4* disp)
{
	int i = get_global_id(0);
	float4 acc = (float4)(0);
	for(int j = 0; j < MAX_INCIDENCE; j++)
	{
		int info = incidence[i].info[j];
		if (info == 0)
			break;
		int sgn;
		int idx = GetIndexAndSign(info, &sgn);
		acc += sgn * invMass[i] * disp[idx];
	}
	vel[i] -= acc;	
}
