#ifndef JOBQUEUE_H
#define JOBQUEUE_H

#ifdef ANDROID_NDK
#	include <pthread.h>
#else
#	include <Windows.h>
#endif

struct Job
{
	virtual void DoWork(int thread = 0) const = 0;
};

class JobQueue
{
private:
	static const Job* currJob;
#ifdef ANDROID_NDK
	pthread_t m_thread;
	friend void* JobQueueThreadProc(void* ptr);
#else
	HANDLE m_thread;
	static HANDLE hEvent, waitEvent;
	friend DWORD WINAPI JobQueueThreadProc(__in  LPVOID lpParameter);
#endif
public:
	JobQueue();
	~JobQueue();
	void Start();
	void StopAndWait();
	void StartJob(const Job& job);
	void WaitForJobs();
};

#ifndef ANDROID_NDK
inline void JobQueue::StartJob(const Job& job)
{
	currJob = &job; // TODO: job queue
	SetEvent(hEvent);
}

inline void JobQueue::WaitForJobs()
{
	WaitForSingleObject(waitEvent, INFINITE);
}
#endif // !ANDROID_NDK

#endif // JOBQUEUE_H