#include "Collision.h"

bool IntersectAabb(const Vector2& center, float radius, const Vector2& min, const Vector2& max, 
	Vector2& intersPoint, Vector2& normal)
{
	Vector2 v[4] = { min, Vector2(min.GetX(), max.GetY()), max, Vector2(max.GetX(), min.GetY()) };
	Vector2 n[4] = { Vector2(-1, 0), Vector2(0, 1), Vector2(1, 0), Vector2(0, -1) };
	float minDist = 1e37f;
	int minIdx = 0;
	bool isInside = PointInAabb(min, max, center);
	bool isEdge = false;
	for (int i = 0; i < 4; i++) {
		Vector2 delta = v[i]; delta.Subtract(center);
		float dVert = float(delta.LengthSquared());
		if (dVert < minDist) {
			minDist = dVert;
			minIdx = i;
			isEdge = false;
		}
		int next = (i + 1) & 3;
		Vector2 edge = v[next]; edge.Subtract(v[i]);
		float t = -float(delta.Dot(edge) / edge.LengthSquared());
		float dEdge = -float(delta.Dot(n[i]));
		if (t > 0.f && t < 1.f) { // TODO: check the sign only
			if (!isInside && dEdge > 0.f) {
				if (dEdge > radius)
					return false;
				intersPoint = edge;
				intersPoint.Scale(t);
				intersPoint.Add(v[i]);
				normal = n[i];
				return true;
			}
			else {
				if (-dEdge < minDist) {
					minDist = -dEdge;
					isEdge = true;
					intersPoint = edge;
					intersPoint.Scale(t);
					intersPoint.Add(v[i]);
					normal = n[i];
				}
			}
		}
	}
	if (!isInside && minDist > radius * radius)
		return false;
	if (!isEdge) {
		intersPoint = v[minIdx];
		normal = center;
		normal.Subtract(intersPoint);
		normal.Normalize();
		if (isInside)
			normal.Flip();
	}
	return true;
}

/*bool IntersectAabb(const Vector2& center, float radius, const Vector2& min, const Vector2& max, 
	Vector2& intersPoint, Vector2& normal)
{
	int intersBits = 0;
	const float radiusSqr =  radius * radius;
	float delta = min.y - center.y;
	float discr = radiusSqr - delta * delta;
	float x1 = min.x;
	float x2 = max.x;
	if (discr >= 0) {
		x1 = center.x + sqrt(discr);
		x2 = center.x - sqrt(discr);
		int bInters = (x1 >= min.x && x1 <= max.x) || (x2 >= min.x && x2 <= max.x);
		intersBits |= bInters; // UP
	}
	delta = max.y - center.y;
	discr = radiusSqr - delta * delta;
	if (discr >= 0) {
		x1 = center.x + sqrt(discr);
		x2 = center.x - sqrt(discr);
		int bInters = (x1 >= min.x && x1 <= max.x) || (x2 >= min.x && x2 <= max.x);
		intersBits |= bInters << 1; // DOWN
	}
	delta = min.x - center.x;
	discr = radiusSqr - delta * delta;
	float y1 = min.y, y2 = max.y;
	if (discr >= 0) {
		y1 = center.y + sqrt(discr);
		y2 = center.y - sqrt(discr);
		int bInters = (y1 >= min.y && y1 <= max.y) || (y2 >= min.y && y2 <= max.y);
		intersBits |= bInters << 2; // LEFT
	}
	delta = max.x - center.x;
	discr = radiusSqr - delta * delta;
	if (discr >= 0) {
		y1 = center.y + sqrt(discr);
		y2 = center.y - sqrt(discr);
		int bInters = (y1 >= min.y && y1 <= max.y) || (y2 >= min.y && y2 <= max.y);
		intersBits |= bInters << 3; // RIGHT
	}

	if (!intersBits)
		return false;

	enum
	{
		UP = 1,
		DOWN = 2,
		LEFT = 4,
		RIGHT = 8
	};
	normal.SetZero();
	if (intersBits & LEFT)
		normal.x = -1.f;
	else if (intersBits & RIGHT)
		normal.x = 1.f;
	if (intersBits & UP)
		normal.y = -1.f;
	else if (intersBits & DOWN)
		normal.y = 1.f;
	intersPoint.x = (intersBits & LEFT) ? min.x : (intersBits & RIGHT) ? max.x : (max.x + min.x) * 0.5f;
	intersPoint.y = (intersBits & UP) ? min.y : (intersBits & DOWN) ? max.y : (max.y + min.y) * 0.5f;
	if (normal.x != 0 && normal.y != 0) {
		normal = center;
		normal.Subtract(intersPoint);
		normal.Normalize();
	}	
	return true;
}*/
