#include "ParticleSystemVelocity.h"
#include <Graphics2D/Color.h>
#include <Math/Matrix.h>

void ParticleSystemVelocity::SolveConstraints()
{
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity* p1 = &particles[pair.i1];
		ParticleVelocity dummy; dummy.invMass = 0.f;
		ParticleVelocity* p2 = &dummy;
		Vector2 v12;
		if (pair.type == Constraint::COLL_PAIR || pair.type == Constraint::LINK)
		{			
			p2 = &particles[pair.i2];
			v12 = p2->velocity - p1->velocity;
		}
		else if (pair.type == Constraint::CONTACT)
		{
			v12 = p1->velocity; v12.Flip();
		}

		float vnrel = pair.normal.Dot(v12);
		float dLambda = (vnrel + beta * pair.depth / timeStep) / (p1->invMass + p2->invMass);

		float lambda0 = pair.lambda;
		pair.lambda = lambda0 + dLambda;
		if (pair.type != Constraint::LINK && pair.lambda < 0.f)
			pair.lambda = 0.f;
		dLambda = pair.lambda - lambda0;

		Vector2 disp = pair.stiffness * dLambda * pair.normal;
		p1->velocity += p1->invMass * disp;
		p2->velocity -= p2->invMass * disp;
		
		// friction
		if (pair.type != Constraint::LINK && mu > 0.f)
		{
			Vector2 vt = v12 - pair.normal * vnrel;
			float vtrel = vt.Length();
			if (vtrel != 0.0f)
			{
				float dLambdaF = vtrel / (p1->invMass + p2->invMass);
				float lambdaF0 = pair.lambdaF1;
				pair.lambdaF1 = lambdaF0 + dLambdaF;
				const float limit = mu * pair.lambda;
				pair.lambdaF1 = clamp(pair.lambdaF1, -limit, limit);
				dLambdaF = pair.lambdaF1 - lambdaF0;
			
				vt.Scale(dLambdaF / vtrel);
				p1->velocity.Add(vt * p1->invMass);
				p2->velocity.Subtract(vt * p2->invMass);
			}
		}
	}
}

void ParticleSystemVelocity::SolveJacobi(float omega, bool minRes, float gamma)
{
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity* p1 = &particles[pair.i1];
		ParticleVelocity dummy; dummy.invMass = 0.f;
		ParticleVelocity* p2 = &dummy;
		float vnrel = 0.f;
		Vector2 v12;
		if (pair.type == Constraint::COLL_PAIR)
		{			
			p2 = &particles[pair.i2];
			v12 = p2->velocity - p1->velocity;
		}
		else if (pair.type == Constraint::CONTACT)
		{
			v12 = p1->velocity; v12.Flip();
		}

		vnrel = pair.normal.Dot(v12);
		float alpha = omega;
		if (!minRes)
		{
			if (p1->invMass == 0 && p2->invMass == 0)
				continue;
			alpha = omega / (p1->invMass + p2->invMass);
		}
		pair.dLambda = alpha * (vnrel + beta * pair.depth / timeStep) + gamma * pair.dLambda;

		float lambda0 = pair.lambda;
		pair.lambda = lambda0 + pair.dLambda;
		if (pair.lambda < 0.f)
			pair.lambda = 0.f;
		pair.dLambda = pair.lambda - lambda0;

		pair.disp = pair.dLambda * pair.normal;
	
		// friction
		if (mu > 0.f)
		{
			Vector2 vt = v12 - pair.normal * vnrel;
			float vtrel = vt.Length();
			if (vtrel != 0.0f)
			{
				float dLambdaF = vtrel / (p1->invMass + p2->invMass);
				float lambdaF0 = pair.lambdaF1;
				pair.lambdaF1 = lambdaF0 + dLambdaF;
				const float limit = mu * pair.lambda;
				// TODO: clamp
				if (pair.lambdaF1 < -limit)
					pair.lambdaF1 = -limit;
				else if (pair.lambdaF1 > limit)
					pair.lambdaF1 = limit;
				dLambdaF = pair.lambdaF1 - lambdaF0;
			
				vt.Scale(dLambdaF / vtrel);
				pair.disp += omega * vt;
			}
		}
	}
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity& p1 = particles[pair.i1];
		p1.velocity += p1.invMass * pair.disp;
		if (pair.type == Constraint::COLL_PAIR)
		{
			ParticleVelocity& p2 = particles[pair.i2];
			p2.velocity -= p2.invMass * pair.disp;
		}
	}
}

#define SKIP_ALPHA
void ParticleSystemVelocity::SolveMinRes(float alpha)
{
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity* p1 = &particles[pair.i1];
		ParticleVelocity dummy; dummy.invMass = 0.f;
		ParticleVelocity* p2 = &dummy;
		Vector2 v12;
		if (pair.type == Constraint::COLL_PAIR)
		{			
			p2 = &particles[pair.i2];
			v12 = p2->velocity - p1->velocity;
		}
		else if (pair.type == Constraint::CONTACT)
		{
			v12 = p1->velocity; v12.Flip();
		}

		float vnrel = pair.normal.Dot(v12);
		pair.err = vnrel + beta * pair.depth / timeStep;
	}
#ifndef SKIP_ALPHA
	// compute dot products for alpha
	float den = 0;
	float dot = 0;

	// y = J^T * r
	std::vector<Vector2> y(particles.size()); // TODO: preallocate
	//memset(&y[0], 0, np * sizeof(VectorType)); // TODO: fill?
	size_t nl = constraints.size();
	for (size_t i = 0; i < nl; i++)
	{
		Vector2 add = constraints[i].normal * constraints[i].err;
		y[constraints[i].i1] -= add;
		if (constraints[i].type != Constraint::CONTACT)
			y[constraints[i].i2] += add;
	}
	// q = J * W  * y
	// den = q^T * q
	// dot = c^T * q
	std::vector<float> q(nl); // TODO: preallocate
	for (size_t i = 0; i < nl; i++)
	{
		const int i1 = constraints[i].i1;
		const int i2 = constraints[i].i2;
		Vector2 diff;
		if (constraints[i].type != Constraint::CONTACT)
			diff = y[i2] * particles[i2].invMass;
		diff -= y[i1] * particles[i1].invMass;
		q[i] = constraints[i].normal.Dot(diff);

		den += q[i] * q[i];
		dot += constraints[i].err * q[i];
	}

	if (den == 0)
		return;
	alpha = dot / den; // TODO: clamp up?
#endif	
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		float dLambda = alpha * pair.err;

		float lambda0 = pair.lambda;
		pair.lambda = lambda0 + dLambda;
		if (pair.lambda < 0.f)
			pair.lambda = 0.f;
		dLambda = pair.lambda - lambda0;

		pair.dLambda = dLambda;
		pair.disp = dLambda * pair.normal;	

		ParticleVelocity& p1 = particles[pair.i1];
		p1.velocity += p1.invMass * pair.disp;
		if (pair.type == Constraint::COLL_PAIR)
		{
			ParticleVelocity& p2 = particles[pair.i2];
			p2.velocity -= p2.invMass * pair.disp;
		}
	}
}

#define SKIP_BETA
void ParticleSystemVelocity::SolveConjRes(float omega, bool minRes)
{
	ParticleVelocity dummy; dummy.invMass = 0.f;
#ifndef SKIP_BETA
	float rSqrOld;
#endif
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint& pair = constraints[i];
			ParticleVelocity* p1 = &particles[pair.i1];
			ParticleVelocity* p2 = &dummy;
			Vector2 v12;
			if (pair.type == Constraint::COLL_PAIR)
			{			
				p2 = &particles[pair.i2];
				v12 = p2->velocity - p1->velocity;
			}
			else if (pair.type == Constraint::CONTACT)
			{
				v12 = p1->velocity; v12.Flip();
			}

			float vnrel = pair.normal.Dot(v12);
			pair.err = vnrel + beta * pair.depth / timeStep;		
		}
#ifndef SKIP_BETA
		// A-norm Fletcher-Reeves
		// y = J^T * r
		size_t np = particles.size();
		std::vector<Vector2> y(np);
		memset(&y[0], 0, np * sizeof(Vector2));
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Vector2 add = constraints[i].normal * constraints[i].err;
			y[constraints[i].i1] += add;
			if (constraints[i].type != Constraint::CONTACT)
				y[constraints[i].i2] -= add;
		}
		// r^2 = r A r
		float rSqrNew = 0;
		for (size_t i = 0; i < np; i++)
		{
			rSqrNew += y[i].LengthSquared() * particles[i].invMass;
		}

		float gamma = 0.f;
		if (k > 0)
		{
			gamma = rSqrNew / rSqrOld; // FR
			gamma = min(1.f, gamma);
		}
		rSqrOld = rSqrNew;
#else
		float gamma = 3.f * (float)k / (float)(numIterations - 1);
		if (gamma > 0) gamma = pow(gamma, 0.1f);
		gamma = min(1.f, gamma);
#endif	
		//Printf("%f\n", gamma);
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint& pair = constraints[i];
			ParticleVelocity* p1 = &particles[pair.i1];
			ParticleVelocity* p2 = pair.type == Constraint::COLL_PAIR ? &particles[pair.i2] : &dummy;

			float alpha = omega;
			if (!minRes)
			{
				alpha = omega / (p1->invMass + p2->invMass);
			}
			pair.dLambda = alpha * pair.err + gamma * pair.dLambda;

			float lambda0 = pair.lambda;
			pair.lambda = lambda0 + pair.dLambda;
			if (pair.lambda < 0.f)
				pair.lambda = 0.f;
			pair.dLambda = pair.lambda - lambda0;

			pair.disp = pair.dLambda * pair.normal;

			p1->velocity += p1->invMass * pair.disp;
			p2->velocity -= p2->invMass * pair.disp;
		}
	}
}

void ParticleSystemVelocity::SolveNesterov(float omega, float beta)
{
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity* p1 = &particles[pair.i1];
		ParticleVelocity dummy; dummy.invMass = 0.f;
		ParticleVelocity* p2 = &dummy;
		float vnrel = 0.f;
		Vector2 v12;
		if (pair.type == Constraint::COLL_PAIR)
		{			
			p2 = &particles[pair.i2];
			v12 = p2->velocity - p1->velocity;
		}
		else if (pair.type == Constraint::CONTACT)
		{
			v12 = p1->velocity; v12.Flip();
		}

		vnrel = pair.normal.Dot(v12);
		float alpha = omega;
		pair.dLambda = alpha * (vnrel + beta * pair.depth / timeStep);

		// clamp lambda
		float lambda0 = pair.y;
		pair.lambda = lambda0 + pair.dLambda;
		if (pair.lambda < 0.f)
			pair.lambda = 0.f;
		pair.dLambda = pair.lambda - lambda0;

		// update y and impulse		
		pair.y = pair.lambda + beta * pair.dLambda;
		//pair.disp += beta * pair.dLambda * pair.normal;
	}
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity& p1 = particles[pair.i1];
		Vector2 imp = beta * pair.dLambda * pair.normal;
		p1.velocity += p1.invMass * imp;
		if (pair.type == Constraint::COLL_PAIR)
		{
			ParticleVelocity& p2 = particles[pair.i2];
			p2.velocity -= p2.invMass * imp;
		}
	}
}


// TODO: remove (obsolete)

// specializations for SolvePairs
template<>
void ParticleSystemVelocity::SolvePairs<ParticleSystemVelocity::JAKOBSEN>(TConstraintInfos& infos, int iteration)
{
	for (size_t i = 0; i < constraints.size(); i++)
	{
		//if (infos[i].len == 0)
		//	continue;
		const Constraint& pair = constraints[i];
		if (pair.type != Constraint::COLL_PAIR)
			continue;
		ParticleVelocity& p1 = particles[pair.i1];
		ParticleVelocity& p2 = particles[pair.i2];
		Vector2 v12 = p2.velocity;
		v12.Subtract(p1.velocity);
		Vector2 n = infos[i].normal;
		Scalar vrel = n.Dot(v12);
		if (vrel <= 0)
			continue;
		vrel += Scalar(beta * infos[i].len);// + restitution * vrel; // TODO: premultiply beta
		const Scalar d = vrel * Scalar(0.5f);
		Vector2 disp = n;
		disp.Scale(d); // TODO: over-relaxation
		p1.velocity.Add(disp);
		p2.velocity.Subtract(disp);
	}
}

template<>
void ParticleSystemVelocity::SolvePairs<ParticleSystemVelocity::CATTO>(TConstraintInfos& infos, int iteration)
{
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		if (pair.type != Constraint::COLL_PAIR)
			continue;
		ParticleVelocity& p1 = particles[pair.i1];
		ParticleVelocity& p2 = particles[pair.i2];
		Vector2 v12 = p2.velocity;
		v12.Subtract(p1.velocity);
		Vector2 n = infos[i].normal;
		Scalar vrel = n.Dot(v12);
		vrel += Scalar(beta * infos[i].len);// + restitution * vrel;
		Scalar dLambda = vrel * Scalar(0.5f);
		Scalar lambda0(pair.lambda);
		pair.lambda = lambda0 + dLambda;
		const Scalar zero(0.f);
		if (pair.lambda < zero)
			pair.lambda = zero; // TODO: branchless
		dLambda = (pair.lambda - lambda0);
		Vector2 disp = n;
		disp.Scale(dLambda); // TODO: over-relaxation
		p1.velocity.Add(disp);
		p2.velocity.Subtract(disp);
		
		//if (warmStarting && iteration == numItersVel - 1)
		//{
		//	uint32 id = (pair.i1 << 16) | pair.i2;
		//	lambdas[id] = pair.lambda;
		//}
	}
}

template<>
void ParticleSystemVelocity::SolvePairs<ParticleSystemVelocity::CATTO_FRICTION>(TConstraintInfos& infos, int iteration)
{
	const float mu = 0.5f;
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		if (pair.type != Constraint::COLL_PAIR)
			continue;
		ParticleVelocity& p1 = particles[pair.i1];
		ParticleVelocity& p2 = particles[pair.i2];
		Vector2 v12 = p2.velocity;
		v12.Subtract(p1.velocity);
		Vector2 n = infos[i].normal;
		// normal contact
		Scalar vnrel = n.Dot(v12);
		Scalar vrel = vnrel + Scalar(beta * infos[i].len);// + restitution * vnrel;
		Scalar dLambda = vrel * Scalar(0.5f);
		Scalar lambda0 = pair.lambda;
		pair.lambda = lambda0 + dLambda;
		const Scalar zero(0.f);
		if (pair.lambda < zero)
			pair.lambda = zero;
		dLambda = (pair.lambda - lambda0);
		Vector2 disp = n;
		disp.Scale(dLambda); // TODO: over-relaxation
		p1.velocity.Add(disp);
		p2.velocity.Subtract(disp);

		// friction
		Vector2 vt = v12 - n * vnrel;
		Scalar vtrel = vt.Length();
		if (vtrel == zero)
			continue;
		dLambda = vtrel * Scalar(0.5f);
		lambda0 = infos[i].lambdaF;
		infos[i].lambdaF = lambda0 + dLambda;
		const float push = lengthScale * fabs(gravity.Dot(infos[i].normal));
		const Scalar limit(timeStep * mu * push);
		if (infos[i].lambdaF < -limit)
			infos[i].lambdaF = -limit;
		else if (infos[i].lambdaF > limit)
			infos[i].lambdaF = limit;
		vt.Scale(infos[i].lambdaF / vtrel);
		p1.velocity.Add(vt);
		p2.velocity.Subtract(vt);
	}
}

template<>
void ParticleSystemVelocity::SolveContacts<ParticleSystemVelocity::CATTO>(TConstraintInfos& infos, int iteration)
{
	for (size_t i = 0; i < constraints.size(); i++) {
		Constraint& pair = constraints[i];
		if (pair.type != Constraint::CONTACT)
			continue;
		ParticleVelocity& p1 = particles[pair.i1];
		const Vector2& v12 = p1.velocity; 
		const Vector2& n = pair.normal;
		Scalar dLambda = -n.Dot(v12); // TODO: projection function (more accurate?)

		Vector2 delta = p1.pos;
		delta.Subtract(pair.point);
		const float len = delta.Dot(pair.normal);
		const float depth = radius - len;
		
		dLambda += Scalar(beta * depth);// + restitution * dLambda;
		Scalar lambda0(pair.lambda);
		pair.lambda = lambda0 + dLambda;
		if (pair.lambda < 0.f)
			pair.lambda = 0.f;
		dLambda = Scalar(pair.lambda) - lambda0;
		
		Vector2 disp = n;
		disp.Scale(dLambda); // TODO: over-relaxation
		p1.velocity.Add(disp);
	}
}

template<>
void ParticleSystemVelocity::SolveContacts<ParticleSystemVelocity::JAKOBSEN>(TConstraintInfos& infos, int iteration)
{
	for (size_t i = 0; i < constraints.size(); i++) {
		Constraint& pair = constraints[i];
		if (pair.type != Constraint::CONTACT)
			continue;
		ParticleVelocity& p1 = particles[pair.i1];
		const Vector2& v12 = p1.velocity; 
		const Vector2& n = pair.normal;
		Scalar vrel = n.Dot(v12); // TODO: projection function (more accurate?)
		if (vrel >= 0)
			continue;
		Vector2 delta = p1.pos;
		delta.Subtract(pair.point);
		const float len = delta.Dot(pair.normal);
		const float depth = radius - len;
		//if (depth < 0)
		//	continue;
		vrel += Scalar(beta * -depth);// + restitution * vrel;
		Vector2 disp = n;
		disp.Scale(vrel); // TODO: over-relaxation
		p1.velocity.Subtract(disp);
	}
}

template<>
void ParticleSystemVelocity::SolveContacts<ParticleSystemVelocity::JAKOBSEN_FRICTION>(TConstraintInfos& infos, int iteration)
{
	const Scalar sRad(radius);
	const Scalar sBeta(beta);
	const Scalar sRest(restitution);
	for (size_t i = 0; i < constraints.size(); i++) {
		Constraint& pair = constraints[i];
		if (pair.type != Constraint::CONTACT)
			continue;
		ParticleVelocity& p1 = particles[pair.i1];
		const Vector2& v12 = p1.velocity; 
		const Vector2& n = pair.normal;
		Scalar vrel = n.Dot(v12); // TODO: projection function (more accurate?)
		if (vrel >= 0)
			continue;
		Vector2 delta = p1.pos;
		delta.Subtract(pair.point);
		const Scalar len = delta.Dot(pair.normal);
		const Scalar depth = sRad - len;
		vrel += sBeta * -depth + sRest * vrel;
		Vector2 disp = n;
		disp.Scale(vrel); // TODO: over-relaxation
		p1.velocity.Subtract(disp);

		// friction
		//if (wallMu == 0.f || iteration > 0)
		//	continue;
		//// TODO: real normal push
		//float push = lengthScale * fabs(gravity.Dot(n));
		//Vector2 vt = v12 - vrel * pair.normal;
		//float friction = timeStep * wallMu * push;
		//float vtrel = vt.Length();
		//if (vtrel > friction)
		//{
		//	vt.Scale(Scalar(friction / vtrel));
		//	p1.velocity.Subtract(vt);
		//}
		//else
		//{
		//	p1.velocity.Subtract(vt);
		//}
	}
}

void ParticleSystemVelocity::ProjectVelocitiesExact()
{
	// this method works for links only!

	// build system matrix
	BuildMatrix();

	// build matrix - now the diagonal part
	const size_t numLinks = links.size();
	std::vector<Vector2> normals(numLinks); // normals (i.e. constraint directions)
	// also compute the residual (LHS term)
	std::vector<float> residual(numLinks); // the velocity residual
	for (size_t i = 0; i < numLinks; i++)
	{
		const Constraint& link = links[i];
		// check the the system is a rope
		assert(link.i2 == link.i1 + 1);		
		const ParticleVelocity& p1 = particles[link.i1];
		const ParticleVelocity& p2 = particles[link.i2];

		// compute the link length and direction
		normals[i] = p2.pos - p1.pos;
		float len = normals[i].Length();
		normals[i].Normalize();

		// the constraint error (relative to the initial length)
		float err = link.len - len;

		// the relative velocity (along the normal)
		Vector2 v12 = p1.velocity - p2.velocity;
		float vnrel = normals[i].Dot(v12);

		// the residual term (normal relative velocity + stabilization term)
		residual[i] = -(vnrel + beta * err / timeStep);
	}

	// tridiagonal linear system solver - for ropes only!!!
	std::vector<float> a(numLinks), b(numLinks), c(numLinks); // the 3 diagonal bands
	mat.GetTridiagonal(a, b, c);
	std::vector<float> lambda(numLinks); // the unknown Lagrange multipliers
	SolveTridiagonal(a, b, c, residual, lambda);

	// update velocities - apply lambdas
	for (size_t i = 0; i < numLinks; i++)
	{
		const Constraint& link = links[i];
		Vector2 impulse = lambda[i] * normals[i];
		particles[link.i1].velocity += particles[link.i1].invMass * impulse;
		particles[link.i2].velocity -= particles[link.i2].invMass * impulse;
	}
}

// Velocity projection (contacts, link, collision pairs)
void ParticleSystemVelocity::ProjectVelocities()
{
	PROFILE_SCOPE("ProjectVelocities");
	if (solver == EXACT)
	{
		// TODO: general linear system solver
		ProjectVelocitiesExact();
		return;
	}

	// precompute length and normal
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity& p1 = particles[pair.i1];
		if (pair.type == Constraint::COLL_PAIR || pair.type == Constraint::LINK)
		{			
			const ParticleVelocity& p2 = particles[pair.i2];
			Vector2 n = p1.pos - p2.pos;
			float len = n.Length();
			pair.depth = pair.len - len;
			n.Normalize();
			pair.normal = n;
		}
		else
		{
			pair.depth = pair.len - pair.normal.Dot(p1.pos - pair.point);
		}
		pair.lambda = 0.f;
		pair.lambdaF1 = 0.f;
		pair.disp.SetZero();
		pair.dLambda = 0.f;
		pair.y = 0.f;
		if (warmStarting)
		{
			uint32 id = (pair.i1 << 16) | pair.i2;
			pair.lambda = lambdas[id];
			pair.dLambda = lambdas[id];

			Vector2 impulse = pair.lambda * pair.normal;
			p1.velocity += p1.invMass * impulse;
			if (pair.type == Constraint::COLL_PAIR)
			{
				ParticleVelocity& p2 = particles[pair.i2];
				p2.velocity -= p2.invMass * impulse;
			}
		}
	}
	float theta = 1.f; // for Nesterov
	for (int k = 0; k < numIterations; ++k)
	{
		// TODO: move iterations loop inside
		if (solver == GAUSS_SEIDEL)
			SolveConstraints();
		else if (solver == JACOBI) 
			SolveJacobi(0.5f, false, 0.f);
		else if (solver == MIN_RES)
		{
			//SolveJacobi(0.25f, true, 0.f);
			SolveMinRes(0.25f);
		}
		else if (solver == CONJ_RES)
		{			
			//float b = (float)(k + 1) / (float)(numIterations);
			//if (b > 0)
			//	b = pow(b, 0.6f);
			
			float theta0 = theta;
			theta = 0.5f * (-theta * theta + theta * sqrt(theta * theta +4));
			float beta = theta0 * (1 - theta0) / (theta0 * theta0 + theta);

			//Printf("%f,%f\n", beta, b);
			//SolveJacobi(0.5f, true, b);
			SolveJacobi(.9f, false, beta); // LSJ
		}
		else if (solver == NESTEROV)
		{
			float theta0 = theta;
			theta = 0.5f * (-theta * theta + theta * sqrt(theta * theta +4));
			float beta = theta0 * (1 - theta0) / (theta0 * theta0 + theta);
			//Printf("%f\n", beta);
			if (k == numIterations - 1)
				 beta = 0;
			SolveNesterov(0.25f, beta);
		}
	}

	//SolveConjRes(0.45f, true);
	//SolveConjRes(1.f, false); // Jacobi

	if (warmStarting)
	{
		// store lambda
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint& pair = constraints[i];
			uint32 id = (pair.i1 << 16) | pair.i2;
			lambdas[id] = 0.3f * pair.lambda;
		}
	}
}

// TODO: remove - obsolete
void ParticleSystemVelocity::PrintMetrics()
{
	// compute and print constraint error
	float err1 = 0; // unilateral
	float err2 = 0; // bilateral
	float vel = 0;
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity* p1 = &particles[pair.i1];
		if (pair.type == Constraint::COLL_PAIR)
		{			
			ParticleVelocity* p2 = &particles[pair.i2];
			Vector2 n = p1->pos - p2->pos;
			float len = n.Length();
			err1 += max(0.f, pair.len - len);
			n.Normalize();
			vel += max(0.f, n.Dot(p2->velocity - p1->velocity));
		}
		else if (pair.type == Constraint::LINK)
		{			
			ParticleVelocity* p2 = &particles[pair.i2];
			Vector2 n = p1->pos - p2->pos;
			float len = n.Length();
			err2 += fabs(pair.len - len);
			n.Normalize();
			vel += fabs(n.Dot(p2->velocity - p1->velocity));
		}
		else if (pair.type == Constraint::CONTACT)
		{
			err1 += max(0.f, pair.len - pair.normal.Dot(p1->pos - pair.point));
			vel += max(0.f, pair.normal.Dot(p1->velocity));
		}
	}
	Printf("%f\n", err1 + err2);
}

void ParticleSystemVelocity::LogMetrics()
{
	// compute and print constraint position and velocity error (norm 1)
	float err = 0;
	float vel = 0;
	float err2 = 0;
	float vel2 = 0;
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity* p1 = &particles[pair.i1];
		if (pair.type == Constraint::COLL_PAIR)
		{
			ParticleVelocity* p2 = &particles[pair.i2];
			Vector2 n = p1->pos - p2->pos;
			float len = n.Length();
			float posError = max(0.f, pair.len - len);
			err += posError;
			err2 += posError * posError;

			n.Normalize();
			float velError = max(0.f, n.Dot(p2->velocity - p1->velocity));
			vel += velError;
			vel2 += velError * velError;
		}
		else if (pair.type == Constraint::LINK)
		{
			ParticleVelocity* p2 = &particles[pair.i2];
			Vector2 n = p1->pos - p2->pos;
			float len = n.Length();
			float posError = fabs(pair.len - len);
			err += posError;
			err2 += posError * posError;

			n.Normalize();
			float velError = fabs(n.Dot(p2->velocity - p1->velocity));
			vel += velError;
			vel2 += velError * velError;
		}
		else if (pair.type == Constraint::CONTACT)
		{
			float posError = max(0.f, pair.len - pair.normal.Dot(p1->pos - pair.point));
			err += posError;
			err2 += posError * posError;

			float velError = max(0.f, pair.normal.Dot(p1->velocity));
			vel += velError;
			vel2 += velError * velError;
		}
	}
	err2 = sqrt(err2);
	vel2 = sqrt(vel2);

	newMetrics.Log(err, "Constraint error L1");
	newMetrics.Log(vel, "Constraint velocity error L1");
	newMetrics.Log(err2, "Constraint error L2");
	newMetrics.Log(vel2, "Constraint velocity error L2");

	// compute and print kinetic energy
	float kin = 0;
	float pot = 0;
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		kin += particles[i].velocity.LengthSquared();
		pot += Engine::getInstance()->getScreenHeight() - particles[i].pos.GetY();
	}
	kin *= 0.5f;
	pot *= lengthScale * 10;

	newMetrics.Log(kin, "Kinetic energy");
	newMetrics.Log(pot, "Potential energy");
}