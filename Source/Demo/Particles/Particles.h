#ifndef PARTICLES_H
#define PARTICLES_H

#include <Engine/Engine.h>
#include <Graphics2D/Texture.h>
#include <Graphics2D/Color.h>
#include <Demo/Particles/ParticleSystem.h> // why is this needed and not a forward declaration?
#include <Demo/Particles/3D/PhysicsSystem3D.h>
#include <Demo/Particles/3D/FemSystem.h>
#include <Physics/ClothPatch.h>
#ifndef ANDROID_NDK
#include <Demo/Particles/Spring1D.h>
#endif
#include <Geometry/Skeleton.h>
#include <Demo/Particles/3D/UnifiedView.h>
#include <Demo/Particles/3D/Demo3DCommon.h>
#include <Demo/Particles/3D/RigidSys3DDemo.h>
#include <Demo/Particles/3D/FEMDemo.h>
#include <Demo/Particles/Particles2DDemo.h>
#include <Demo/Particles/Soft2DDemo.h>
#include <Demo/Particles/PendulumDemo.h>
#include <Demo/Particles/HelloIntegrator.h>

#define DISABLED_WIN32

class RigidSystem;
class ClothDemo;

class ParticlesDemo: public Engine
{
public:
	enum DemoType
	{
		DEMO_CLOTH,
		DEMO_PARTICLES,
		DEMO_CLOTH_PARTICLES,
		DEMO_HOURGLASS,
		DEMO_LINKS,
		DEMO_LATTICE,
		DEMO_RIGID,
		DEMO_FEM,
		DEMO_UNIFIED,
		DEMO_PENDULUM
	};

public:
	ParticlesDemo();
	~ParticlesDemo();
	void PausePhysics() { pausePhysics = !pausePhysics; }
	void SetDemoFromString(const char* str);
	//void SetPlatformFromString(const char* str);
	void Reset();
	void SetDivisions(int val) { divisions = val; }
	int GetDivisions() const { return divisions; }
	int GetNumParticles() const { return numParticles; }
	void SetNumParticles(int val) { numParticles = val; }
	float GetRadius() const { return radius; }
	void SetRadius(float val) { radius = val; }
	void SetIterations(int val);
	PhysicsSystem3D& GetPhysics3D() { return phys3D; }

	virtual void OnCreate();
	virtual void OnUpdate(float dt);
	virtual void OnDraw();
	virtual void OnDraw3D();
	void OnMouseMove(int x, int y) override;
	void OnMouseDown(int x, int y, MouseButton mb) override;
	void OnMouseUp(int x, int y, MouseButton mb) override;

private:
	template<typename ContactVector> void DrawWallNormals(const ContactVector& walls) const;
	void InitParticles();
	void InitRigid();
	
	void InitParticles3D();
	void InitLinks3D();
	void InitCloth3D();
	void InitClothParticles3D();
	void InitFem3D();
	void InitRigid3D();
	void InitUnified();
	void InitSoft2D();
	void InitPendulum();

	void LoadConfig();
	void DrawRigid();
	void DrawRigid3D();
	void DrawFem3D();

	void DrawCloth();
	void TessellateCloth();

private:
#ifndef ANDROID_NDK
	PhysicsSystem3D phys3D;
#endif

	Texture background;
	int mouseX, mouseY;
	float distVal;
	bool replayMode;
	unsigned int currFrame;
	bool drawVel;
	void (ParticlesDemo::*initFn)();
	bool pausePhysics;
	bool stepByStep;
	int mouseLinkId;
	
	int numParticles;
	float radius;

	// cloth
	int divisions;

	const struct aiScene* scene;

	DemoType type;
	
	std::vector<GRBf> colors3D;

	int nFrames;
	bool drip;

	std::unique_ptr<RigidSystem> rigidSys; // the 2D rigid body simulator
	
	bool mShowDebug;
	bool wireframe;
	int mDebugDrawFlags;

	UnifiedView mUnified;

	std::unique_ptr<ClothDemo> mClothDemo;
	RigidSys3DDemo mRigid3DDemo;
	Particles2DDemo mParticles2DDemo;
	Soft2DDemo mSoft2DDemo;
	FEMDemo mFEMDemo;
	PendulumDemo mPendulumDemo;

public:
	static bool noDraw;
	static int maxFrames;
	static bool bAbcExport;
	static bool bAutoStart;
};

#endif // PARTICLES_H