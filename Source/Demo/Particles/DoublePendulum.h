#ifndef DOUBLE_PENDULUM_H
#define DOUBLE_PENDULUM_H

#ifndef ANDROID_NDK

#include "Math/Vector2Pair.h"

template<typename Real, int Method>
class DoublePendulum
{
public:
	DoublePendulum(Real len, Real t10, Real v10, Real t20, Real v20, Real h)
		: l(len)
		, theta(t10, t20)
		, vel(v10, v20)
		, g(9.8f * 100)
	{
		integrator.Initialize(theta, thetaPrev, vel, this, h);
	}

	void StepHamilton(Real h)
	{
		const Real g = 9.8f * 100;
		const Real d = t1 - t2;
		const Real sinD = sin(d);
		const Real cosD = cos(d);
		const Real s = 1 + sinD * sinD;
		const Real f = l * l * s;
		const Real c1 = v1 * v2 * sinD / f;
		const Real c2 = (v1 * v1 + 2 * v2 * v2 - v1 * v2 * cosD) * sin(2 * d) / (2 * l * l * s * s);
		v1 += h * (-2 * g * l * sin(t1) - c1 + c2);
		v2 += h * (-g * l * sin(t2) + c1 - c2);
		t1 += h * (v1 - v2 * cosD) / f;
		t2 += h * (2 * v2 - v1 * cosD) / f;
		
		integrator.Step(theta, vel, this, h);
	}

	void Step(Real h)
	{
		// TODO: compare to Hamilton
		integrator(theta, thetaPrev, vel, this, h);

		// energy
		Real t1 = theta.GetX();
		Real t2 = theta.GetY();
		Real v = g * l * (cos(t1) + cos(t2));
		Real l2 = l * l;
		Real v1 = vel.GetX();
		Real v2 = vel.GetY();
		Real t = l2 * (v1 * v1 + 0.5 * v2 * v2 + v1 * v2 * cos(t1 - t2));
		//Printf("Energy (reduced): %f\n", v - t);
	}

	Vector2Tpl<Real> Evaluate(const Vector2Tpl<Real>& theta, const Vector2Tpl<Real>& vel) const
	{
		const Real omegaSqr = g / l;
		const Real t1 = theta.GetX();
		const Real t2 = theta.GetY();
		const Real d = t1 - t2;
		const Real v2 = vel.GetY();
		const Real v2Sqr = v2 * v2;
		const Real sin2d = sin(2 * d);
		const Real num = -1.5 + 0.5 * cos(2 * d);
		const Real a1 = (omegaSqr * sin(t1) * 1.5 + 0.5 * omegaSqr * sin(t1 - 2 * t2) + v2Sqr * 0.5 * sin2d + v2Sqr * sin(d)) / num;
		const Real a2 = (omegaSqr * sin(t2) - omegaSqr * sin(2 * t1 - t2) - 2 * v2Sqr * sin(d) - 0.5 * v2Sqr * sin2d) / num;
		return Vector2Tpl<Real>(a1, a2);
	}

	Vector2Tpl<Real> EvaluateForce(const Vector2Tpl<Real>& theta, const Vector2Tpl<Real>& vel) const
	{
		const Real d = theta.GetX() - theta.GetY();
		const Real sinD = sin(d);
		const Real cosD = cos(d);
		const Real s = 1 + sinD * sinD;
		const Real f = l * l * s;
		const Real v1 = vel.GetX();
		const Real v2 = vel.GetY();
		const Real c1 = v1 * v2 * sinD / f;
		const Real c2 = (v1 * v1 + 2 * v2 * v2 - v1 * v2 * cosD) * sin(2 * d) / (2 * l * l * s * s);
		return Vector2Tpl<Real>(-2 * g * l * sin(theta.GetX()) - c1 + c2, -g * l * sin(theta.GetY()) + c1 - c2);
	}

	Vector2Tpl<Real> EvaluateVelocity(const Vector2Tpl<Real>& theta, const Vector2Tpl<Real>& vel) const
	{
		const Real d = theta.GetX() - theta.GetY();
		const Real sinD = sin(d);
		const Real cosD = cos(d);
		const Real s = 1 + sinD * sinD;
		const Real f = l * l * s;
		const Real v1 = vel.GetX();
		const Real v2 = vel.GetY();
		return Vector2Tpl<Real>((v1 - v2 * cosD) / f, (2 * v2 - v1 * cosD) / f);
	}

	Vector2 GetPosition1() { const Real t1 = theta.GetX(); return Vector2(l * sin(t1), l * cos(t1)); }
	Vector2 GetPosition2() { const Real t2 = theta.GetY(); return Vector2(l * sin(t2), l * cos(t2)); }

private:
	Real l, g;

	Vector2Tpl<Real> theta, thetaPrev, vel;
	Integrator<Method, Vector2Tpl<Real>, DoublePendulum, Real> integrator;
};

template <typename Real, int Method>
class DblPendulumMax
{
public:
	DblPendulumMax(Real len, Real t10, Real v10, Real t20, Real v20, Real h) : l(len), g(9.8f * 100)
	{
		Vector2 p1(len * sin(t10), len * cos(t10));
		Vector2 p2(len * sin(t20), len * cos(t20));
		p2.Add(p1);
		pos.Set(p1, p2);
		Vector2 v1(pos.GetVector1().GetY(), -pos.GetVector1().GetX()); v1.Scale(Scalar(v10));
		Vector2 v2(pos.GetVector2().GetY(), -pos.GetVector2().GetX()); v2.Scale(Scalar(v20));
		vel.Set(v1, v2);

		posPrev = pos;
	}

	Vector2 GetPosition1() { return pos.GetVector1(); }
	Vector2 GetPosition2() { return pos.GetVector2() - pos.GetVector1(); }

protected:
	void ProjectPositionsGaussSeidel(int iterations, Real h)
	{
		for (int i = 0; i < iterations; i++)
		{
			Vector2 p1 = pos.GetVector1();
			p1.Scale(l / p1.Length());
			Vector2 p2 = pos.GetVector2();
			Vector2 delta = p2 - p1;
			Scalar len = delta.Length();
			delta.Scale(0.5f * (l - len) / len);
			p2 += delta;
			p1 -= delta;
			pos.Set(p1, p2);
		}
		// update velocities
		if (Method != Integrators::VERLET)
		{
			vel = (1.f / h) * (pos - posPrev);
			posPrev = pos;
		}
	}

	void ProjectPositionsNewtonFree(int iterations, Real h)
	{
		for (int i = 0; i < iterations; i++)
		{
			Scalar len(l);
			Vector2 p1 = pos.GetVector1();
			Scalar b1 = p1.Length() - len;
			Vector2 delta = pos.GetVector2() - p1;
			Scalar b2 = delta.Length() - len;
			p1.Normalize();
			delta.Normalize();
			Scalar dot = p1.Dot(delta);
			Scalar det = 2.f - dot * dot;
			Scalar mu1 = -(2.f * b1 + dot * b2) / det;
			Scalar mu2 = -(dot * b1 + b2) / det;
			pos.Set(pos.GetVector1() + mu1 * p1 - delta * mu2, pos.GetVector2() + delta * mu2);
		}
		// update velocities
		if (Method != Integrators::VERLET)
		{
			vel = (1.f / h) * (pos - posPrev);
			posPrev = pos;
		}
	}

	void ProjectPositionsShake(int iterations, Real h)
	{
		Vector2 p10 = posPrev.GetVector1();
		Vector2 delta0 = posPrev.GetVector2() - p10;
		p10.Normalize();
		delta0.Normalize();
		for (int i = 0; i < iterations; i++)
		{
			Scalar len(l);
			Vector2 p1 = pos.GetVector1();
			Scalar b1 = p1.Length() - len;
			Vector2 delta = pos.GetVector2() - p1;
			Scalar b2 = delta.Length() - len;
			p1.Normalize();
			delta.Normalize();

			Scalar dot1 = p1.Dot(p10);
			Scalar dot2 = delta.Dot(delta0);
			Scalar dot12 = p1.Dot(delta0);
			Scalar dot21 = p10.Dot(delta);
			Scalar det = Scalar(2.f) * dot1 * dot2 - dot12 * dot21;
			Scalar mu1 = -(Scalar(2.f) * dot2 * b1 + dot12 * b2) / det;
			Scalar mu2 = -(dot21 * b1 + dot1 * b2) / det;
			pos.Set(pos.GetVector1() + mu1 * p10 - delta0 * mu2, pos.GetVector2() + delta0 * mu2);
		}
		// update velocities
		if (Method != Integrators::VERLET)
		{
			vel = (1.f / h) * (pos - posPrev);
			posPrev = pos;
		}
	}

	void ProjectPositionsNewtonExplicit(int iterations, Real h) // Goldenthal
	{
		for (int i = 0; i < iterations; i++)
		{
			Vector2 p10 = posPrev.GetVector1();
			Vector2 delta0 = posPrev.GetVector2() - p10;
			p10.Normalize();
			delta0.Normalize();
			Scalar len(l);
			Vector2 p1 = pos.GetVector1();
			Scalar b1 = p1.Length() - len;
			Vector2 delta = pos.GetVector2() - p1;
			Scalar b2 = delta.Length() - len;
			p1.Normalize();
			delta.Normalize();

			Scalar dot1 = p1.Dot(p10);
			Scalar dot2 = delta.Dot(delta0);
			Scalar dot12 = p1.Dot(delta0);
			Scalar dot21 = p10.Dot(delta);
			Scalar det = 2.f * dot1 * dot2 - dot12 * dot21;
			Scalar mu1 = -(2.f * dot2 * b1 + dot12 * b2) / det;
			Scalar mu2 = -(dot21 * b1 + dot1 * b2) / det;
			pos.Set(pos.GetVector1() + mu1 * p10 - delta0 * mu2, pos.GetVector2() + delta0 * mu2);
		}
		// update velocities
		if (Method != Integrators::VERLET)
		{
			vel = (1.f / h) * (pos - posPrev);
			posPrev = pos;
		}
	}

	void ProjectVelocitiesGaussSeidel(int iterations)
	{
		Vector2 vel1 = vel.GetVector1();
		Vector2 vel2 = vel.GetVector2();
		Vector2 n1 = pos.GetVector1();
		n1.Normalize();
		Vector2 n2 = pos.GetVector2() - pos.GetVector1();
		n2.Normalize();
		for (int i = 0; i < iterations; i++)
		{
			Scalar vt = vel1.Dot(n1);
			vel1.Subtract(vt * n1);
			vt = Scalar(0.5f) * vel2.Dot(n2);
			vel1.Add(vt * n2);
			vel2.Subtract(vt * n2);
		}
		vel.Set(vel1, vel2);
	}

	void ProjectVelocitiesGsBaumgarte(int iterations, Real h)
	{
		// Gauss-Seidel (with Baumgarte position correction, beta = 1 / h)
		Scalar len(l);
		Scalar hh(h);
		Vector2 vel1 = vel.GetVector1();
		Vector2 vel2 = vel.GetVector2();
		Vector2 n1 = pos.GetVector1();
		Scalar C1 = (n1.Length() - len) / hh;
		n1.Normalize();
		Vector2 n2 = pos.GetVector2() - pos.GetVector1();
		Scalar C2 = (n2.Length() - len) / hh;
		n2.Normalize();
		for (int i = 0; i < 10; i++)
		{
			Scalar vt = vel1.Dot(n1) + C1;
			vel1.Subtract(vt * n1);
			vt = Scalar(0.5f) * (vel2.Dot(n2) + C2);
			vel1.Add(vt * n2);
			vel2.Subtract(vt * n2);
		}
		vel.Set(vel1, vel2);
	}

	void ProjectVelocitiesExact()
	{
		Vector2 p1 = pos.GetVector1();
		Vector2 delta = pos.GetVector2() - p1;
		p1.Normalize();
		delta.Normalize();
		Vector2 v1 = vel.GetVector1();
		Vector2 v2 = vel.GetVector2();
		Scalar dot = p1.Dot(delta);
		Scalar det = 2.f - dot * dot;
		Scalar dot1 = p1.Dot(v1);
		Scalar dot2 = delta.Dot(v2 - v1);
		Scalar mu1 = -(2.f * dot1 + dot * dot2) / det;
		Scalar mu2 = -(dot * dot1 + dot2) / det;
		v1 += mu1 * p1 - delta * mu2;
		v2 += delta * mu2;
		vel.Set(v1, v2);
	}

	void ProjectVelocitiesExactBaumgarte(Real h)
	{
		Scalar len(l);
		Scalar hh(h);
		Vector2 p1 = pos.GetVector1();
		Vector2 delta = pos.GetVector2() - p1;
		Scalar C1 = (p1.Length() - len) / hh;
		Scalar C2 = (delta.Length() - len) / hh;
		p1.Normalize();
		delta.Normalize();
		Vector2 v1 = vel.GetVector1();
		Vector2 v2 = vel.GetVector2();
		Scalar dot = p1.Dot(delta);
		Scalar det = 2.f - dot * dot;
		Scalar dot1 = p1.Dot(v1) + C1;
		Scalar dot2 = delta.Dot(v2 - v1) + C2;
		Scalar mu1 = -(2.f * dot1 + dot * dot2) / det;
		Scalar mu2 = -(dot * dot1 + dot2) / det;
		v1 += mu1 * p1 - delta * mu2;
		v2 += delta * mu2;
		vel.Set(v1, v2);
	}

protected:
	Real l;
	Vector2Pair pos, posPrev, vel;
	Real g;
};

template <typename Real, int Method>
class DblPendulumProj : public DblPendulumMax<Real, Method>
{
public:
	DblPendulumProj(Real len, Real t10, Real v10, Real t20, Real v20, Real h) : DblPendulumMax(len, t10, v10, t20, v20, h)
	{
		integrator.Initialize(pos, posPrev, vel, this, h);
	}

	void Step(Real h)
	{

		integrator(pos, posPrev, vel, this, h);

		ProjectPositionsShake(10, h);

		// exact explicit version

		// Rattle
		//Vector2 g(0, 9.8f * 100);
		//Vector2 pos1 = pos.GetVector1();
		//Vector2 vel1 = vel.GetVector1();
		//Vector2 pos2 = pos.GetVector2();
		//Vector2 vel2 = vel.GetVector2();
		//Vector2 posNew1 = pos1 + h * vel1 + 0.5f * h * h * g;
		//Vector2 posNew2 = pos2 + h * vel2 + 0.5f * h * h * g;

		//float dot1 = posNew1.Dot(pos1);
		//float lenSqr1 = pos1.LengthSquared();
		//float lambda1 = (-dot1 + sqrt(dot1 * dot1 - lenSqr1 * (posNew1.LengthSquared() - l * l))) / lenSqr1;
		//vel1.Add(0.5f * h * g + (lambda1 / h) * pos1);
		//pos1 = posNew1 + lambda1 * pos1;
		//
		//Vector2 u = posNew2 - posNew1; // or prev1
		//Vector2 v = posNew2 - pos1;
		//float dot2 = u.Dot(v);
		//float lenSqr2 = u.LengthSquared();
		//float lambda2 = (-dot2 + sqrt(dot2 * dot2 - lenSqr2 * (v.LengthSquared() - l * l))) / lenSqr2;
		//vel2.Add(0.5f * h * g + (lambda2 / h) * u);
		//pos2 = posNew2 + lambda2 * u;

		////for (int i = 0; i < 10; i++)
		////{
		////	posNew1.Scale(l / posNew1.Length());
		////	Vector2 delta = posNew2 - posNew1;
		////	Scalar len = delta.Length();
		////	delta.Scale(0.5f * (l - len) / len);
		////	posNew2 += delta;
		////	posNew1 -= delta;
		////}

		////vel1 = (1.f / h) * (posNew1 - pos1);
		////vel2 = (1.f / h) * (posNew2 - pos2);
		////pos1 = posNew1;
		////pos2 = posNew2;

		//vel1.Add(0.5f * h * g);
		//float mu = pos1.Dot(vel1) / pos1.LengthSquared();
		//vel1.Subtract(mu * pos1);

		//vel2.Add(0.5f * h * g);
		//Vector2 delta = pos2 - pos1;
		//mu = delta.Dot(vel2) / delta.LengthSquared();
		//vel2.Subtract(mu * delta);

		//pos.Set(pos1, pos2);
		//vel.Set(vel1, vel2);
	}

	Vector2Pair Evaluate(const Vector2Pair&, const Vector2Pair&) const
	{
		Vector2 g(0.f, 9.8f * 100);
		return Vector2Pair(g, g);
	}

	Vector2 GetPosition1() { return pos.GetVector1(); }
	Vector2 GetPosition2() { return pos.GetVector2() - pos.GetVector1(); }

private:
	Integrator<Method, Vector2Pair, DblPendulumProj, Real> integrator;
};

template <typename Real, int Method>
class DblPendulumVel : public DblPendulumMax<Real, Method>
{
public:
	DblPendulumVel(Real len, Real t10, Real v10, Real t20, Real v20, Real h) : DblPendulumMax(len, t10, v10, t20, v20, h) 
	{
		integrator.Initialize(pos, posPrev, vel, this, h);
	}

	void Step(Real h)
	{
		integrator.StepVel(pos, vel, this, h);
		// TODO: compare iterative convergence to exact solution (SOR, Conjugate Gradient)
		ProjectVelocitiesExactBaumgarte(h);		
		integrator.StepPos(pos, vel, h);

		//for (int i = 0; i < 10; i++)
		//{
		//	Vector2 p1 = pos.GetVector1();
		//	p1.Scale(len / p1.Length());
		//	Vector2 p2 = pos.GetVector2();
		//	Vector2 delta = p2 - p1;
		//	Scalar lenD = delta.Length();
		//	delta.Scale(Scalar(0.5f) * (len - lenD) / lenD);
		//	p2 += delta;
		//	p1 -= delta;
		//	pos.Set(p1, p2);
		//}

		// energy
		Scalar e = -Scalar(0.5f) * (vel.GetVector1().LengthSquared() + vel.GetVector2().LengthSquared()) + g * Scalar(pos.GetVector1().GetY() + pos.GetVector2().GetY());
		Printf("%f\n", (float)e);
	}

	Vector2Pair Evaluate(const Vector2Pair&, const Vector2Pair&) const
	{
		Vector2 vg(0, g);
		return Vector2Pair(vg, vg);
	}

private:
	Integrator<Method, Vector2Pair, DblPendulumVel, Real> integrator;
};

template <typename Real, int Method>
class DblPendulumForce : public DblPendulumMax<Real, Method>
{
public:
	DblPendulumForce(Real len, Real t10, Real v10, Real t20, Real v20, Real h) : DblPendulumMax(len, t10, v10, t20, v20, h) 
	{
		integrator.Initialize(pos, posPrev, vel, this, h);
	}

	void Step(Real h)
	{
		// TODO: try other integrators too
		// with velocity projection:
		integrator.StepVel(pos, vel, this, h);
		ProjectVelocitiesExactBaumgarte(h);
		integrator.StepPos(pos, vel, h);
		
		//integrator(pos, posPrev, vel, this, h);
		//ProjectPositionsGaussSeidel(10, h);
	}

	Vector2Pair Evaluate(const Vector2Pair&, const Vector2Pair&) const
	{
		// exact
		Vector2 p1 = pos.GetVector1();
		Scalar p1Sqr = p1.LengthSquared();
		Vector2 p2 = pos.GetVector2();
		Vector2 delta = p2 - p1;
		Scalar deltaSqr = delta.LengthSquared();
		Scalar dotG = Scalar(p1.GetY()) * g;
		Vector2 v1 = vel.GetVector1();
		Scalar v1Sqr = v1.LengthSquared();
		Vector2 v2 = vel.GetVector2();
		Scalar vSqr = (v2 - v1).LengthSquared();
		Scalar dot = p1.Dot(delta);
		Scalar det = Scalar(2.f) * p1Sqr * deltaSqr - dot * dot;
		Scalar b = v1Sqr + dotG;
		Scalar lambda1 = -(Scalar(2.f) * deltaSqr * b + dot * vSqr) / det;
		Scalar lambda2 = -(dot * b + p1Sqr * vSqr) / det;
		Vector2 vg(0, g);
		Vector2 f1 = vg + lambda1 * p1 - lambda2 * delta;
		Vector2 f2 = vg + lambda2 * delta;

		// FIXME/TODO: iterative solver
		// Gauss-Seidel (doesn't work)
		//Vector2 f1(0, g);
		//Vector2 f2(0, g);
		//Vector2 n1 = pos.GetVector1();
		//n1.Normalize();
		//Vector2 n2 = pos.GetVector2() - pos.GetVector1();
		//n2.Normalize();
		//for (int i = 0; i < 10; i++)
		//{
		//	Scalar ft = f1.Dot(n1);
		//	f1.Subtract(ft * n1);
		//	ft = Scalar(0.5f) * f2.Dot(n2);
		//	f1.Add(ft * n2);
		//	f2.Subtract(ft * n2);
		//}

		// TODO: Baumgarte

		return Vector2Pair(f1, f2);
	}

private:
	Integrator<Method, Vector2Pair, DblPendulumForce, Real> integrator;
};

#endif // ANDROID_NDK

#endif
