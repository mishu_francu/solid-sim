#include "ProjectorCL.h"

#include <vector>
#include <Engine/Profiler.h>
//#include <Cloth/GridBroadphase3D.h> // FIXME

#ifdef EMULATE_CL
void JacobiEdges::Run(size_t* size)
{
	// prepare arguments
	Edge* edges = (Edge*)args[0].buf->Get();
	Vector4* pos = (Vector4*)args[1].buf->Get();
	float* im = (float*)args[2].buf->Get();
	Vector4* disp = (Vector4*)args[3].buf->Get();
	Vector4* normals = (Vector4*)args[4].buf->Get();
	Vector4* points = (Vector4*)args[5].buf->Get();

	for (globalId = 0; globalId < size[0]; globalId++)
	{
		Kernel(edges, pos, im, disp, normals, points);
	}
}

void JacobiEdges::Kernel(const Edge* edges, Vector4* pos, float* invMass, Vector4* disp, Vector4* normals, Vector4* points)
{
	int i = globalId;
	int i1 = edges[i].i1;
	int i2 = edges[i].i2;
	float len0 = edges[i].len;
	if (edges[i].type != Constraint::CONTACT)
	{
		Vector4 delta = pos[i1] - pos[i2];
		float len = length(delta);
		if (edges[i].type == Constraint::COLL_PAIR && len > len0)
			disp[i] = (Vector4)(0);
		else
		{
			float lambda = (len - len0) / (invMass[i1] + invMass[i2]);
			disp[i] = (lambda / len) * delta; // TODO: relaxation factor as input
		}
	}
	else
	{
		Vector4 n = normals[i2];
		float len = dot(n, pos[i1] - points[i2]);
		if (len > len0)
			disp[i] = (Vector4)(0);
		else
			disp[i] = (len - len0) * n;		
	}
}

void JacobiParticles::Run(size_t* size)
{
	// prepare arguments
	Vector4* pos = (Vector4*)args[0].buf->Get();
	float* im = (float*)args[1].buf->Get();
	Incidence* inc = (Incidence*)args[2].buf->Get(); 
	Vector4* disp = (Vector4*)args[3].buf->Get();

	for (globalId = 0; globalId < size[0]; globalId++)
	{
		Kernel(pos, im, inc, disp);
	}
}

int GetIndexAndSign(int info, int* sgn)
{
	int idx;
	if (info > 0)
	{
		idx = info - 1;
		*sgn = 1;
	}
	else
	{
		idx = -info - 1;
		*sgn = -1;
	}
	return idx;
}

void JacobiParticles::Kernel(Vector4* pos, const float* invMass, const Incidence* incidence, Vector4* disp)
{
	int i = globalId;
	Vector4 acc = (Vector4)(0);
	for(int j = 0; j < MAX_INCIDENCE; j++)
	{
		int info = incidence[i].info[j];
		if (info == 0)
			break;
		int sgn;
		int idx = GetIndexAndSign(info, &sgn);
		acc += 0.4f * sgn * invMass[i] * disp[idx];
	}
	pos[i] += acc;
}

void GaussSeidelSolver::Run(size_t* size)
{
	// prepare arguments
	Edge* edges = (Edge*)args[0].buf->Get();
	Vector4* pos = (Vector4*)args[1].buf->Get();
	float* im = (float*)args[2].buf->Get();

	for (globalId = 0; globalId < size[0]; globalId++)
	{
		GaussSeidel(edges, pos, im);
	}
}

void GaussSeidelSolver::GaussSeidel(Edge* edges, Vector4* pos, const float* invMass)
{
	int i = globalId;
	if (edges[i].type != Constraint::LINK)
		return;
	int i1 = edges[i].i1;
	int i2 = edges[i].i2;
	Vector4 delta = pos[i1] - pos[i2];
	float len = delta.Length();
	float len0 = edges[i].len;
	float lambda = (len - len0) / (invMass[i1] + invMass[i2]);
	Vector4 disp = (lambda / len) * delta;
	pos[i1] -= invMass[i1] * disp;
	pos[i2] += invMass[i2] * disp;
}

#endif

template <class VectorType, class ParticleType>
void JacobiProjectorCL<VectorType, ParticleType>::Init()
{	
	program = OpenCL::GetInstance()->Build("../Shaders/Jacobi.cl", "-I ./");
	// Create kernel object
	if (program)
	{
#ifndef EMULATE_CL
		jacEdgesKernel.Create(program, "jacobiEdges");
		jacParticlesKernel.Create(program, "jacobiParticles");
#else
		jacEdgesKernel.Create(&jacEdges);
		jacParticlesKernel.Create(&jacParticles);
#endif
	}
}

#define WAVE_SIZE 256
#define WAVE_SIZE1 WAVE_SIZE

template <class VectorType, class ParticleType>
void JacobiProjectorCL<VectorType, ParticleType>::PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links)
{
	size_t n = particles.size();
	nParticlesReal = n;
	nParticles = NextMultiple(n, WAVE_SIZE);
	nLinksReal = links.size();
	nLinks = NextMultiple(links.size(), WAVE_SIZE1);

	// release old ones
	positions.Release();
	edges.Release();
	disp.Release();
	invMass.Release();
	incEdges.Release();
	acc.Release();

	if (!positions.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * nParticles))
		Printf("Failed creating buffer\n");
	acc.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Accumulator) * nParticles);

	size_t maxContacts = 6 * n;
	size_t maxSize = nLinks;
	bool ret = edges.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(Edge) * maxSize, NULL);
	disp.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(Vector4) * maxSize);
		
	std::vector<float> im(nParticles);
	for (size_t i = 0; i < n; i++)
	{
		im[i] = particles[i].invMass;
	}
	invMass.Create(CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float) * nParticles, &im[0]);
	incEdges.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Incidence) * nParticles, NULL);

	OpenCL* compute = OpenCL::GetInstance();
	if (!compute)
		return;

	// copy edges
	Edge* edgePtr = (Edge*)compute->EnqueueMap(edges, true, CL_MAP_WRITE);
	memset(edgePtr, 0, nLinks * sizeof(Edge));
	//Vector4* normalPtr = (Vector4*)compute->EnqueueMap(normals, true, CL_MAP_WRITE);
	//Vector4* pointPtr = (Vector4*)compute->EnqueueMap(points, true, CL_MAP_WRITE);
	Incidence* incPtr = (Incidence*)compute->EnqueueMap(incEdges, true, CL_MAP_WRITE);
	// TODO: keep allocated
	std::vector<int> count(n);
	memset(incPtr, 0, sizeof(Incidence) * nParticles);
	//memset(normalPtr, 0, sizeof(Vector4) * n);
	//memset(pointPtr, 0, sizeof(Vector4) * n);
	int nContacts = 0;
	ASSERT(nLinks < 10 * n); // FIXME
	if (edgePtr == NULL || incPtr == NULL)
		Printf("NULL!!!\n");
	for (size_t i = 0; i < links.size(); i++)
	{
		Edge edge;
		//if (i >= links.size())
		//{
		//	edgePtr[i] = edge;
		//	continue;
		//}

		edge.len = links[i].len;
		edge.i1 = links[i].i1;
		edge.type = 0;//links[i].type;
		edge.stiff = links[i].stiffness;

		int info = i + 1;

		int i1 = links[i].i1;
		int& n1 = count[i1];
		edge.i1 |= n1 << 24;
		ASSERT(n1 < MAX_INCIDENCE);
		//if (n1 >= MAX_INCIDENCE) continue;
		incPtr[i1].info[n1++] = -info;

		int i2 = links[i].i2;
		if (links[i].type != Constraint::CONTACT)
		{
			int& n2 = count[i2];
			ASSERT(n2 < MAX_INCIDENCE);
			//if (n2 >= MAX_INCIDENCE) continue;
			edge.i2 = i2 | (n2 << 24);
			incPtr[i2].info[n2++] = info;
		}
		//else
		//{
		//	normalPtr[nContacts] = links[i].normal;
		//	pointPtr[nContacts] = links[i].point;
		//	edge.i2 = nContacts;
		//	nContacts++;
		//}

		edgePtr[i] = edge;
	}
	ASSERT(nContacts <= 2 * (int)n); // FIXME
	compute->EnqueueUnmap(edges, edgePtr);
	compute->EnqueueUnmap(incEdges, incPtr);
	//compute->EnqueueUnmap(normals, normalPtr);
	//compute->EnqueueUnmap(points, pointPtr);

	Accumulator* accPtr = (Accumulator*)compute->EnqueueMap(acc, true, CL_MAP_WRITE);
	memset(accPtr, 0, nParticles * sizeof(Accumulator));
	compute->EnqueueUnmap(acc, accPtr);

	Vector4* dispPtr = (Vector4*)compute->EnqueueMap(disp, true, CL_MAP_WRITE);
	memset(dispPtr, 0, nLinks * sizeof(Vector4));
	compute->EnqueueUnmap(disp, dispPtr);
}

template <class VectorType, class ParticleType>
void JacobiProjectorCL<VectorType, ParticleType>::CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links, bool map)
{
	PROFILE_SCOPE("CopyBuffers");
	size_t n = particles.size();
	OpenCL* compute = OpenCL::GetInstance();
	if (!compute)
		return;

	// copy positions
	// TODO: strided read/write
	Vector4* posPtr = (Vector4*)compute->EnqueueMap(positions, true, CL_MAP_WRITE);
	for (size_t i = 0; i < n; i++)
	{
		posPtr[i] = particles[i].pos;
	}
	compute->EnqueueUnmap(positions, posPtr);
	posPtr = NULL;
}

template <class VectorType, class ParticleType>
void JacobiProjectorCL<VectorType, ParticleType>::ProjectPositions()
{
	if (nParticles == 0 || nLinks == 0)
		return;

	PROFILE_SCOPE("ProjectJacobiCL");
	jacEdgesKernel.SetArgument(0, &edges);
	jacEdgesKernel.SetArgument(1, &positions); // TODO: this the only one that changes
	jacEdgesKernel.SetArgument(2, &invMass);
	jacEdgesKernel.SetArgument(3, &disp);
	jacEdgesKernel.SetArgument(4, &normals);
	jacEdgesKernel.SetArgument(5, &points);
	
	jacParticlesKernel.SetArgument(0, &positions);
	jacParticlesKernel.SetArgument(1, &invMass);
	jacParticlesKernel.SetArgument(2, &incEdges);
	jacParticlesKernel.SetArgument(3, &disp);

	OpenCL* compute = OpenCL::GetInstance();
	for (int k = 0; k < numIterations; ++k)
	{
		compute->EnqueueKernel(jacEdgesKernel, &nLinks);
		compute->EnqueueKernel(jacParticlesKernel, &nParticles);
	}
	clFinish(compute->GetCommandQueue());
}

template class JacobiProjectorCL<Vector2, ParticleVerlet>;
template class JacobiProjectorCL<Vector2, ParticleVelocity>;
template class JacobiProjectorCL<Vector3, ParticleVelocityT<Vector3> >;

template <class VectorType, class ParticleType>
void GSProjectorCL<VectorType, ParticleType>::ProjectPositions(size_t nParticles)
{
	PROFILE_SCOPE("ProjectGS CL");

	projectKernel.SetArgument(1, &positions);
	projectKernel.SetArgument(2, &invMass);
	projectKernel.SetArgument(3, &normals);
	projectKernel.SetArgument(4, &points);

	OpenCL* compute = OpenCL::GetInstance();
	for (int k = 0; k < numIterations; ++k)
	{
		for (int i = 0; i < MAX_BATCHES && batchSizes[i]; i++)
		{			
			projectKernel.SetArgument(0, &batches[i]);
			size_t edges_work_size[1] = { batchSizes[i] }; // TODO: include it in CLBuffer?
			compute->EnqueueKernel(projectKernel, edges_work_size);
		}
	}
	clFinish(compute->GetCommandQueue());
}

template <class VectorType, class ParticleType>
void GSProjectorCL<VectorType, ParticleType>::Init()
{
	OpenCL* compute = OpenCL::GetInstance();
	program = compute->Build("../Shaders/GaussSeidel.cl", "-I ./");
	// Create kernel object
	if (program)
	{
#ifndef EMULATE_CL
		projectKernel.Create(program, "gaussSeidel");
#else
		projectKernel.Create(&gsSolver);
#endif
	}
}

template <class VectorType, class ParticleType>
void GSProjectorCL<VectorType, ParticleType>::PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links)
{
	size_t n = particles.size();
	if (!positions.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * n))
		Printf("Failed creating buffer\n");

	std::vector<Edge> buckets[LINK_BATCHES];
	for (size_t i = 0; i < links.size(); i++)
	{
		Edge edge;
		edge.i1 = links[i].i1;
		edge.i2 = links[i].i2;
		edge.len = links[i].len;
		edge.type = links[i].type;
		edge.stiff = links[i].stiffness;
		ASSERT(links[i].batch >= 0 && links[i].batch < MAX_BATCHES);
		buckets[links[i].batch].push_back(edge);
	}
	for (int i = 0; i < LINK_BATCHES; i++)
	{
		batchSizes[i] = buckets[i].size();
		batches[i].Create(CL_MEM_COPY_HOST_PTR, buckets[i].size() * sizeof(Edge), &(buckets[i])[0]);
	}

	// contacts
	size_t maxContacts = n;
	for (size_t i = 0; i < COLL_BATCHES; i++)
		batches[LINK_BATCHES + i].Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Edge) * n);
	normals.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * maxContacts); // or more
	points.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * maxContacts); // or more

	std::vector<float> im(n);
	for (size_t i = 0; i < n; i++)
	{
		im[i] = particles[i].invMass;
	}
	invMass.Create(CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float) * n, &im[0]);
}

template <class VectorType, class ParticleType>
void GSProjectorCL<VectorType, ParticleType>::CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links, bool map)
{
	PROFILE_SCOPE("CopyBuffers");
	size_t n = particles.size();
	OpenCL* compute = OpenCL::GetInstance();

	size_t nContacts = 0;
	if (links.size())
	{
		Edge* edgePtr[COLL_BATCHES];
		for (size_t i = 0; i < COLL_BATCHES; i++)
		{
			edgePtr[i] = (Edge*)compute->EnqueueMap(batches[LINK_BATCHES + i], true, CL_MAP_WRITE);
			batchSizes[LINK_BATCHES + i] = 0;
		}
		Vector4* normalPtr = (Vector4*)compute->EnqueueMap(normals, true, CL_MAP_WRITE);
		Vector4* pointPtr = (Vector4*)compute->EnqueueMap(points, true, CL_MAP_WRITE);
		for (size_t i = 0; i < links.size(); i++)
		{
			if (links[i].type != Constraint::CONTACT)
				continue;
			Edge edge;
			edge.i1 = links[i].i1;
			edge.i2 = nContacts;
			edge.len = links[i].len;
			edge.type = links[i].type;
			edge.stiff = links[i].stiffness;
			ASSERT(links[i].batch >= 0 && links[i].batch < COLL_BATCHES);
			size_t& count = batchSizes[LINK_BATCHES + links[i].batch];
			edgePtr[links[i].batch][count++] = edge;

			normalPtr[nContacts] = links[i].normal;
			pointPtr[nContacts] = links[i].point;
			nContacts++;
		}
		for (size_t i = 0; i < COLL_BATCHES; i++)
			compute->EnqueueUnmap(batches[LINK_BATCHES + i], edgePtr[i]);
		compute->EnqueueUnmap(normals, normalPtr);
		compute->EnqueueUnmap(points, pointPtr);
	}	

	// copy positions
	Vector4* posPtr = (Vector4*)compute->EnqueueMap(positions, true, CL_MAP_WRITE);
	for (size_t i = 0; i < n; i++)
	{
		posPtr[i] = particles[i].pos;
	}
	compute->EnqueueUnmap(positions, posPtr);
	posPtr = NULL;
}

template class GSProjectorCL<Vector2, ParticleVerlet>;
template class GSProjectorCL<Vector2, ParticleVelocity>;
template class GSProjectorCL<Vector3, ParticleVelocityT<Vector3> >;

template <class VectorType, class ParticleType>
void MinResProjectorCL<VectorType, ParticleType>::Init()
{
	OpenCL* ocl = OpenCL::GetInstance();
	if (!ocl)
		return;
	program = ocl->Build("../Shaders/MinRes.cl", "-I ./");
	// Create kernel object
	if (program)
	{
#ifndef EMULATE_CL
		jacEdgesKernel.Create(program, "minResEdges");
		jacParticlesKernel.Create(program, "minResParticles");
#else
		jacEdgesKernel.Create(&jacEdges);
		jacParticlesKernel.Create(&jacParticles);
#endif
	}
}

template <class VectorType, class ParticleType>
void MinResProjectorCL<VectorType, ParticleType>::ProjectPositions()
{
	if (nParticles == 0 || nLinks == 0)
		return;

	PROFILE_SCOPE("ProjectMinResCL");
	jacEdgesKernel.SetArgument(0, &edges);
	jacEdgesKernel.SetArgument(1, &positions); // TODO: this the only one that changes
	jacEdgesKernel.SetArgument(2, &invMass);
	jacEdgesKernel.SetArgument(3, &disp);
	jacEdgesKernel.SetArgument(4, &normals);
	jacEdgesKernel.SetArgument(5, &points);
	float alpha = 0.25f;
	jacEdgesKernel.SetArgument(6, &alpha);
	
	jacParticlesKernel.SetArgument(0, &positions);
	jacParticlesKernel.SetArgument(1, &invMass);
	jacParticlesKernel.SetArgument(2, &incEdges);
	jacParticlesKernel.SetArgument(3, &disp);

	OpenCL* compute = OpenCL::GetInstance();
	for (int k = 0; k < numIterations; ++k)
	{
		compute->EnqueueKernel(jacEdgesKernel, &nLinks);
		compute->EnqueueKernel(jacParticlesKernel, &nParticles);
	}
	clFinish(compute->GetCommandQueue());
}

template class MinResProjectorCL<Vector2, ParticleVerlet>;
template class MinResProjectorCL<Vector2, ParticleVelocity>;
template class MinResProjectorCL<Vector3, ParticleVelocityT<Vector3> >;

template <class VectorType, class ParticleType>
void ConjResProjectorCL<VectorType, ParticleType>::Init()
{
	OpenCL* ocl = OpenCL::GetInstance();
	if (!ocl)
		return;
	program = ocl->Build("../Shaders/MinRes.cl", "-I ./");
	// Create kernel object
	if (program)
	{
#ifndef EMULATE_CL
		jacEdgesKernel.Create(program, "conjResEdges");
		jacParticlesKernel.Create(program, "minResParticles");
		OpenCL::GetInstance()->GetKernelWorkGroupInfo(jacEdgesKernel.Get());
#else
		jacEdgesKernel.Create(&jacEdges);
		jacParticlesKernel.Create(&jacParticles);
#endif
	}
	else
		Engine::getInstance()->Quit();
}

template <class VectorType, class ParticleType>
void ConjResProjectorCL<VectorType, ParticleType>::ProjectPositions()
{
	if (nParticles == 0 || nLinks == 0)
		return;

	float alpha = 0.25f;

	PROFILE_SCOPE("ProjectConjResCL");
	jacEdgesKernel.SetArgument(0, &edges);
	jacEdgesKernel.SetArgument(1, &positions); // TODO: this the only one that changes
	jacEdgesKernel.SetArgument(2, &invMass);
	jacEdgesKernel.SetArgument(3, &disp);
	jacEdgesKernel.SetArgument(4, &alpha);

	jacParticlesKernel.SetArgument(0, &positions);
	jacParticlesKernel.SetArgument(1, &invMass);
	jacParticlesKernel.SetArgument(2, &incEdges);
	jacParticlesKernel.SetArgument(3, &disp);

	OpenCL* compute = OpenCL::GetInstance();
	size_t localSize = WAVE_SIZE;
	size_t localSize1 = WAVE_SIZE1;
	for (int k = 0; k < numIterations; ++k)
	{
		float beta = (float)k / (float)(numIterations - 1);
		if (beta > 0)
			beta = pow(beta, 0.6f);
		jacEdgesKernel.SetArgument(5, &beta);
		compute->EnqueueKernel(jacEdgesKernel, &nLinks, &localSize1);
		compute->EnqueueKernel(jacParticlesKernel, &nParticles, &localSize);
	}
	clFlush(compute->GetCommandQueue());
}

template class ConjResProjectorCL<Vector2, ParticleVerlet>;
template class ConjResProjectorCL<Vector2, ParticleVelocity>;
template class ConjResProjectorCL<Vector3, ParticleVelocityT<Vector3> >;

template <class VectorType, class ParticleType>
void ConjResProjectorCL1<VectorType, ParticleType>::Init()
{
	OpenCL* ocl = OpenCL::GetInstance();
	if (!ocl)
		return;
	program = ocl->Build("../Shaders/MinRes.cl", "-I ./");// -g -s \"C:\\Work\\GameEngine\\Source\\Demo\\Particles\"");
	// Create kernel object
	if (program)
	{
		jacEdgesKernel.Create(program, "conjResEdges1");
		jacParticlesKernel.Create(program, "minResParticles1red");
		OpenCL::GetInstance()->GetKernelWorkGroupInfo(jacEdgesKernel.Get());
	}
	//else
		//Engine::getInstance()->Quit();
}

template <class VectorType, class ParticleType>
void ConjResProjectorCL1<VectorType, ParticleType>::ProjectPositions()
{
	if (nParticles == 0 || nLinks == 0)
		return;

	float alpha = 0.2f;

	PROFILE_SCOPE("ProjectConjResCL");
	jacEdgesKernel.SetArgument(0, &edges);
	jacEdgesKernel.SetArgument(1, &positions); // TODO: this the only one that changes
	jacEdgesKernel.SetArgument(2, &invMass);
	jacEdgesKernel.SetArgument(3, &disp);
	jacEdgesKernel.SetArgument(4, &alpha);
	jacEdgesKernel.SetArgument(6, &acc);
	
	jacParticlesKernel.SetArgument(0, &positions);
	jacParticlesKernel.SetArgument(1, &invMass);
	jacParticlesKernel.SetArgument(2, &acc);

	OpenCL* compute = OpenCL::GetInstance();
	size_t localSize = WAVE_SIZE;
	size_t localSize1 = WAVE_SIZE1;
	size_t workSize = nParticles * 16;
	for (int k = 0; k < numIterations; ++k)
	{
		float beta = (float)k / (float)(numIterations - 1);
		if (beta > 0)
			beta = pow(beta, 0.6f);
		jacEdgesKernel.SetArgument(5, &beta);
		compute->EnqueueKernel(jacEdgesKernel, &nLinks, &localSize1);
		compute->EnqueueKernel(jacParticlesKernel, &workSize, &localSize);
	}
	clFlush(compute->GetCommandQueue());
}

template class ConjResProjectorCL1<Vector2, ParticleVerlet>;
template class ConjResProjectorCL1<Vector2, ParticleVelocity>;
template class ConjResProjectorCL1<Vector3, ParticleVelocityT<Vector3> >;

template <class VectorType, class ParticleType>
void ConjResProjectorCL2<VectorType, ParticleType>::Init()
{
	OpenCL* ocl = OpenCL::GetInstance();
	if (!ocl)
		return;
	program = ocl->Build("../Shaders/MinRes.cl", "-I ./");
	// Create kernel object
	if (program)
	{
		jacEdgesKernel.Create(program, "conjResEdges2");
		jacParticlesKernel.Create(program, "minResParticles2");
		OpenCL::GetInstance()->GetKernelWorkGroupInfo(jacEdgesKernel.Get());
	}
	else
		Engine::getInstance()->Quit();
}

template <class VectorType, class ParticleType>
void ConjResProjectorCL2<VectorType, ParticleType>::ProjectPositions()
{
	if (nParticles == 0 || nLinks == 0)
		return;

	float alpha = 0.2f;

	PROFILE_SCOPE("ProjectConjResCL2");
	
	jacParticlesKernel.SetArgument(0, &positions);
	jacParticlesKernel.SetArgument(1, &invMass);
	jacParticlesKernel.SetArgument(2, &incEdges);
	jacParticlesKernel.SetArgument(3, &disp);
	jacParticlesKernel.SetArgument(4, &edges);
	jacParticlesKernel.SetArgument(5, &alpha);

	OpenCL* compute = OpenCL::GetInstance();
	size_t localSize = WAVE_SIZE;
	for (int k = 0; k < numIterations; ++k)
	{
		float beta = (float)k / (float)(numIterations - 1);
		if (beta > 0)
			beta = pow(beta, 0.6f);
		jacParticlesKernel.SetArgument(6, &beta);
		jacParticlesKernel.SetArgument(7, &k);
		compute->EnqueueKernel(jacParticlesKernel, &nParticles, &localSize);
	}
	clFlush(compute->GetCommandQueue());
}

template class ConjResProjectorCL2<Vector2, ParticleVerlet>;
template class ConjResProjectorCL2<Vector2, ParticleVelocity>;
template class ConjResProjectorCL2<Vector3, ParticleVelocityT<Vector3> >;

template <class VectorType, class ParticleType>
void JacobiVelProjCL<VectorType, ParticleType>::Init()
{	
	OpenCL* ocl = OpenCL::GetInstance();
	if (!ocl)
		return;
	program = ocl->Build("../Shaders/JacobiVel.cl", "-I ./");
	// Create kernel object
	if (program)
	{
#ifndef EMULATE_CL
		jacEdgesKernel.Create(program, "jacobiEdges");
		jacParticlesKernel.Create(program, "jacobiParticles");
#else
		jacEdgesKernel.Create(&jacEdges);
		jacParticlesKernel.Create(&jacParticles);
#endif
	}
}

template <class VectorType, class ParticleType>
void JacobiVelProjCL<VectorType, ParticleType>::PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links)
{
	size_t n = particles.size();

	// TODO: release old ones
	if (!vel.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * n))
		Printf("Failed creating buffer\n");

	size_t maxContacts = 10 * n; // TODO: tweak
	size_t maxSize = links.size() + maxContacts;
	edges.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Edge) * maxSize, NULL);
	disp.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * maxSize);	
	normals.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * maxSize);
		
	std::vector<float> im(n);
	for (size_t i = 0; i < n; i++)
	{
		im[i] = particles[i].invMass;
	}
	invMass.Create(CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float) * n, &im[0]);
	incEdges.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Incidence) * n, NULL);
}

template <class VectorType, class ParticleType>
void JacobiVelProjCL<VectorType, ParticleType>::CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links, float beta)
{
	PROFILE_SCOPE("CopyBuffers");
	size_t n = particles.size();
	nLinks = links.size();
	OpenCL* compute = OpenCL::GetInstance();

	// copy edges
	Edge* edgePtr = (Edge*)compute->EnqueueMap(edges, true, CL_MAP_WRITE);
	Vector4* normalPtr = (Vector4*)compute->EnqueueMap(normals, true, CL_MAP_WRITE);
	Incidence* incPtr = (Incidence*)compute->EnqueueMap(incEdges, true, CL_MAP_WRITE);
	// TODO: keep allocated
	std::vector<int> count(n);
	memset(incPtr, 0, sizeof(Incidence) * n);
	memset(normalPtr, 0, sizeof(Vector4) * n);
	ASSERT(nLinks < 10 * n);
	for (size_t i = 0; i < nLinks; i++)
	{
		Edge edge;
		//edge.len = links[i].len;
		edge.i1 = links[i].i1;
		edge.type = links[i].type;
		edge.stiff = 0.f; // lambda

		int info = i + 1;

		int i1 = links[i].i1;
		int& n1 = count[i1];
		ASSERT(n1 < MAX_INCIDENCE);
		edge.i1 |= (n1 << 24);
		incPtr[i1].info[n1++] = -info;

		int i2 = links[i].i2;
		if (links[i].type == Constraint::COLL_PAIR)
		{
			int& n2 = count[i2];
			ASSERT(n2 < MAX_INCIDENCE);
			edge.i2 = (n2 << 24);
			incPtr[i2].info[n2++] = info;
			edge.i2 = i2;

			Vector3 n = particles[i1].pos - particles[i2].pos;
			float len = n.Length();
			edge.len = beta * (links[i].len - len);
			n.Normalize();
			normalPtr[i] = n;
		}
		else if (links[i].type == Constraint::CONTACT)
		{
			edge.len = beta * (links[i].len - links[i].normal.Dot(particles[i1].pos - links[i].point));
			normalPtr[i] = links[i].normal;
		}

		edgePtr[i] = edge;
	}
	compute->EnqueueUnmap(edges, edgePtr);
	compute->EnqueueUnmap(incEdges, incPtr);
	compute->EnqueueUnmap(normals, normalPtr);

	// copy positions
	velPtr = (Vector4*)compute->EnqueueMap(vel, true, CL_MAP_WRITE);
	for (size_t i = 0; i < n; i++)
	{
		velPtr[i] = particles[i].velocity;
	}
	compute->EnqueueUnmap(vel, velPtr);
	velPtr = NULL;
}

template <class VectorType, class ParticleType>
void JacobiVelProjCL<VectorType, ParticleType>::ProjectVelocities(size_t nParticles)
{
	if (nParticles == 0 || nLinks == 0)
		return;

	PROFILE_SCOPE("ProjectJacobiCL");
	jacEdgesKernel.SetArgument(0, &edges);
	jacEdgesKernel.SetArgument(1, &vel);
	jacEdgesKernel.SetArgument(2, &invMass);
	jacEdgesKernel.SetArgument(3, &disp);
	jacEdgesKernel.SetArgument(4, &normals);
	
	jacParticlesKernel.SetArgument(0, &vel);
	jacParticlesKernel.SetArgument(1, &invMass);
	jacParticlesKernel.SetArgument(2, &incEdges);
	jacParticlesKernel.SetArgument(3, &disp);

	OpenCL* compute = OpenCL::GetInstance();
	for (int k = 0; k < numIterations; ++k)
	{
		const float mul = 1.f;
		const float exp = 0.6f;
		float gamma = mul * (float)k / (float)(numIterations - 1);
		if (gamma > 0) gamma = pow(gamma, exp); // sqrt(gamma)
		gamma = min(1.f, gamma);

		jacEdgesKernel.SetArgument(5, &gamma);
		compute->EnqueueKernel(jacEdgesKernel, &nLinks);
		compute->EnqueueKernel(jacParticlesKernel, &nParticles);
	}
	clFinish(compute->GetCommandQueue());
}

template class JacobiVelProjCL<Vector3, ParticleVelocityT<Vector3> >;

template <class VectorType, class ParticleType>
void ParticleProjectorCL<VectorType, ParticleType>::Init()
{	
	cl_program program = OpenCL::GetInstance()->Build("../Shaders/ParticlesProj.cl", "-I ./");
	// Create kernel object
	if (program)
	{
		parProjKernel.Create(program, "particlesProject0");
		OpenCL::GetInstance()->GetKernelWorkGroupInfo(parProjKernel.Get());
	}
}

template <class VectorType, class ParticleType>
void ParticleProjectorCL<VectorType, ParticleType>::PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links)
{
	nParticles = NextMultiple(particles.size(), WAVE_SIZE);
	bool ret = false;
	ret = pos[0].Create(CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, nParticles * sizeof(Vector4));
	ret = pos[1].Create(CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, nParticles * sizeof(Vector4));

	// create incidence data
	//std::vector<IncidenceEx> inc(links.size());
	IncidenceEx* inc = (IncidenceEx*)_aligned_malloc(links.size() * sizeof(IncidenceEx), 4096); // TODO: free
	memset(inc, 0, links.size() * sizeof(IncidenceEx));
	for (size_t i = 0; i < links.size(); i++)
	{
		int i1 = links[i].i1;
		int i2 = links[i].i2;

		int& n1 = inc[i1].num;
		ASSERT(n1 < MAX_INCIDENCE);
		inc[i1].info[n1].idx = i2;
		inc[i1].info[n1].stiffness = links[i].stiffness;
		inc[i1].info[n1].len0 = links[i].len;
		n1++;

		if (links[i].type != Constraint::CONTACT)
		{
			int& n2 = inc[i2].num;
			ASSERT(n2 < MAX_INCIDENCE);
			inc[i2].info[n2].idx = i1;
			inc[i2].info[n2].stiffness = links[i].stiffness;
			inc[i2].info[n2].len0 = links[i].len;
			n2++;
		}
	}
	incidence.Create(CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, links.size() * sizeof(IncidenceEx), &inc[0]);
	// TODO: only disp is writable

	// TODO: move mass to incidence (particle info)
	// inverse masses
	//std::vector<float> im(nParticles);
	float* im = (float*)_aligned_malloc(nParticles * sizeof(float), 4096);
	for (size_t i = 0; i < particles.size(); i++)
		im[i] = particles[i].invMass;
	invMass.Create(CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, nParticles * sizeof(float), &im[0]);
}

template <class VectorType, class ParticleType>
void ParticleProjectorCL<VectorType, ParticleType>::CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links, bool map)
{
	PROFILE_SCOPE("WritePos");
	OpenCL* compute = OpenCL::GetInstance();
	Vector4* posPtr = (Vector4*)compute->EnqueueMap(pos[0], true, CL_MAP_WRITE);
	for (size_t i = 0; i < particles.size(); i++)
		posPtr[i] = particles[i].pos;
	compute->EnqueueUnmap(pos[0], posPtr);
}

template <class VectorType, class ParticleType>
void ParticleProjectorCL<VectorType, ParticleType>::ReadBuffers(std::vector<ParticleType>& particles)
{
	PROFILE_SCOPE("ReadPos");
	OpenCL* compute = OpenCL::GetInstance();
	int buf = numIterations & 1;
	Vector4* posPtr = (Vector4*)compute->EnqueueMap(pos[buf], true, CL_MAP_READ); // TODO: don't wait
	for (size_t i = 0; i < particles.size(); i++)
		particles[i].pos = posPtr[i];
	compute->EnqueueUnmap(pos[buf], posPtr);
}

template <class VectorType, class ParticleType>
void ParticleProjectorCL<VectorType, ParticleType>::ProjectPositions()
{
	if (nParticles == 0)
		return;
	float alpha = 0.8f;

	PROFILE_SCOPE("ProjectParticlesCL");
	parProjKernel.SetArgument(0, &invMass);
	parProjKernel.SetArgument(3, &incidence);
	parProjKernel.SetArgument(4, &alpha);
	
	OpenCL* compute = OpenCL::GetInstance();
	size_t localSize = WAVE_SIZE;
	for (int k = 0; k < numIterations; ++k)
	{
		const float mul = 1.f;
		const float exp = 0.6f;
		float gamma = mul * (float)k / (float)(numIterations - 1);
		if (gamma > 0) gamma = pow(gamma, exp); // sqrt(gamma)
		gamma = min(1.f, gamma);

		int bufIn = k & 1;
		int bufOut = (k + 1) & 1;
		parProjKernel.SetArgument(1, &pos[bufIn]);
		parProjKernel.SetArgument(2, &pos[bufOut]);
		parProjKernel.SetArgument(5, &gamma);
		compute->EnqueueKernel(parProjKernel, &nParticles, &localSize);
	}
	clFlush(compute->GetCommandQueue());
}

template class ParticleProjectorCL<Vector3, ParticleVelocityT<Vector3> >;