#include "ParticleSystemVerlet.h"
#include <Engine/Base.h>

//#define USE_OPENCL

ParticleSystemVerlet::ParticleSystemVerlet() 
	: prevNum(0)
	, currBuff(0)
	, proj(numIterations)
{
	tolerance = radius * 1.f;

#ifdef USE_OPENCL
	//InitKernels();
	projCL.Init();
#endif
}

void ParticleSystemVerlet::FirstTimeInit()
{
	// TODO: remove (this is for ProjectParticles)
	size_t n = GetNumParticles();
	bool createBuffers = prevNum != n;
	prevNum = n;
	if (createBuffers)
	{
		neighbours.resize(n);
		for (size_t i = 0; i < links.size(); i++)
		{
			if (links[i].i1 < links[i].i2)
			{
				neighbours[links[i].i1].push_back(Neighbour(links[i].i2, links[i].len));
			}
			else
			{
				neighbours[links[i].i2].push_back(Neighbour(links[i].i1, links[i].len));
			}
		}
	}
}

void ParticleSystemVerlet::MicroStep()
{
	// first time init
	size_t n = GetNumParticles();
	if (prevNum != n)
	{
		prevNum = n;
#ifdef USE_OPENCL
		projCL.PrepareBuffers(particles, links);
		projCL.CopyBuffers(particles, links);
#endif
	}

	IntegrateVerlet();
	Collide();
	
	//FirstTimeInit();

	// print the matrix
	//if (diag.empty())	
	//{
	//	BuildIncidenceMatrix();

	//	const size_t nl = links.size();
	//	std::vector<Vector2> n(nl);
	//	for (size_t i = 0; i < nl; i++)
	//	{
	//		n[i] = (particles[links[i].i2].pos - particles[links[i].i1].pos);
	//		n[i].Normalize();
	//		mat.Get(i, i) = diag[i];
	//	}

	//	// update dots between normals
	//	for (size_t i = 0; i < sparseMat.size(); i++)
	//	{
	//		sparseMat[i].dot = n[sparseMat[i].i].Dot(n[sparseMat[i].j]);
	//		const float val = sparseMat[i].a * sparseMat[i].dot;
	//		mat.Get(sparseMat[i].i, sparseMat[i].j) = val;
	//		mat.Get(sparseMat[i].j, sparseMat[i].i) = val;
	//	}
	//}
	//Printf("Matrix A\n");
	//PrintMatrix(mat);

#ifdef USE_OPENCL
	projCL.CopyBuffers(particles, links);
	projCL.SetNumIterations(numIterations);
	projCL.ProjectPositions(n);
	projCL.ReadBuffers(particles);
#else
	//if (solver == CONJ_RES || solver == MIN_RES || solver == JACOBI)
	//{
	//	//proj.ComputeSpectralRadius(constraints, particles);
	//	//Printf("spectral radius: %f, alpha: %f\n", rho, 1.f / rho);
	//}

	if (solver == EXACT)
	{
		ProjectPositionsExact();
	}
	else if (solver == MIN_RES)
	{
		// TODO: cleanup all other project functions
		//ProjectMinRes(1.f / rho);

		proj.SetNumIterations(numIterations);
		proj.NewtonMinimumResidual(constraints, particles, 0.25f);
	}
	else if (solver == CONJ_RES)
	{
		//ProjectNCR();
		
		proj.SetNumIterations(numIterations);
		proj.NonlinearConjugateResidual(constraints, particles, 0.25f);
	}
	else if (solver == GAUSS_SEIDEL)
	{
		ProjectPositions();

		//RelaxationProjector<ParticleVerlet> proj(numIterations, 1.2f);
		//proj.NewtonSOR(links, particles);
	}
	else if (solver == JACOBI)
	{
		ProjectPositionsJacobi(0.4f); //2.f / rho);
		//ProjectParticles();
		
		//ProjectLinksJacobiSSE();
		//ProjectLinksGaussSeidelSSE();
		//ProjectLinksGS();		
	}
	else
	{
		assert(false);
	}
#endif
	// compute position and velocity error 2-norm
	//float posNorm = 0;
	//float velNorm = 0;
	//float pe = 0, ve = 0;
	//for (size_t i = 0; i < links.size(); i++)
	////size_t i = links.size() - 1;
	//{
	//	Vector2 delta = particles[links[i].i2].pos - particles[links[i].i1].pos;
	//	float err = delta.Length() - links[i].len;
	//	posNorm += err * err;
	//	pe += err;
	//	delta.Normalize();
	//	Vector2 v12 = /*(1.f / timeStep) * */(particles[links[i].i2].GetVelocity() - particles[links[i].i1].GetVelocity());
	//	float vn = delta.Dot(v12);
	//	velNorm += vn * vn;
	//	ve += vn;
	//}
	//posNorm = sqrt(posNorm);
	//velNorm = sqrt(velNorm);
	//pe *= 1.f / links.size();
	//ve *= 1.f / links.size();
}

bool ParticleSystemVerlet::SupportsSolver(SolverType aSolver)
{
	return aSolver == EXACT || aSolver == MIN_RES || aSolver == CONJ_RES || aSolver == GAUSS_SEIDEL || aSolver == JACOBI;
}

INLINE void ParticleSystemVerlet::ProjectPairVelocities(const Constraint& pair, float damping)
{
	ParticleVerlet& p1 = particles[pair.i1];
	ParticleVerlet& p2 = particles[pair.i2];
	Vector2 delta = p1.pos;
	delta.Subtract(p2.pos);
	// TODO: use Taylor estimate 		
	if (!delta.NormalizeCheck())
		return;
	Vector2 v1 = p1.pos; v1.Subtract(p1.prevPos);
	Vector2 v2 = p2.pos; v2.Subtract(p2.prevPos);
	Vector2 v12 = v1; v12.Subtract(v2);
	Scalar vrel = v12.Dot(delta);
	if (vrel > 0)
	{
		Vector2 vnrel = delta; 
		vnrel.Scale(0.5f * vrel * damping);
		p1.prevPos.Add(vnrel);
		p2.prevPos.Subtract(vnrel);
	}
}

void ParticleSystemVerlet::AddDampingForces()
{
	const float dampingFactor = 0.500f;
	if (forceAcc.empty())
		forceAcc.resize(GetNumParticles());
	std::fill(forceAcc.begin(), forceAcc.end(), Vector2());
	for (size_t i = 0; i < links.size(); i++)
	{
		Vector2 delta = particles[links[i].i1].pos - particles[links[i].i2].pos;
		delta.Normalize();
		float vrel = delta.Dot(particles[links[i].i1].GetVelocity() - particles[links[i].i2].GetVelocity()) / timeStep;
		Vector2 force = dampingFactor * vrel * delta;
		forceAcc[links[i].i1] -= force;
		forceAcc[links[i].i2] += force;
	}
}