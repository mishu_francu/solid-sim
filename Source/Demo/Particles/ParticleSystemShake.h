#pragma once

#include "ParticleSystem.h"

class ParticleSystemShake : public ParticleSystem<ParticleVerlet>
{
public:
	void MicroStep() override;
	bool SupportsSolver(SolverType aSolver) override;
};
