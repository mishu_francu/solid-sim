#ifndef PROJECTOR_H
#define PROJECTOR_H

#include <Engine/Profiler.h>

template <typename ParticleType, typename ConstraintType = Constraint, typename VectorType = Vector2>
class RelaxationProjector
{
private:
	int numIterations;
	float omega; // SOR factor

public:
	RelaxationProjector(int n) : numIterations(n), omega(1) { }
	RelaxationProjector(int n, float sor) : numIterations(n), omega(sor) { }

	void NewtonSOR(std::vector<ConstraintType>& constraints, std::vector<ParticleType>& particles, int maxIter = 0)
	{
		PROFILE_SCOPE("NewtonSOR projector");

		static ParticleType ground; ground.invMass = 0;

		int n = maxIter == 0 ? numIterations : maxIter;	
		int k;
		for (k = 0; k < n; ++k)
		{
			selfBroken = false;
			for (size_t i = 0; i < constraints.size(); i++)
			{
				if (constraints[i].type == ConstraintType::SELF_TRIANGLE)
				{
					ConstraintType& contact = constraints[i];
					Vector3& v1 = particles[contact.i1].pos;
					Vector3& v2 = particles[contact.i2].pos;
					Vector3& v3 = particles[contact.i3].pos;
					Vector3& v4 = particles[contact.i4].pos;
					Vector3 p = contact.w1 * v1 + contact.w2 * v2 + contact.w3 * v3;
					Vector3 n = contact.normal;
					float len0 = constraints[i].len;
					float len = n.Dot(particles[contact.i4].pos - p);
					if (len > len0)
						continue;
					selfBroken = true;
					float invMass = particles[contact.i1].invMass + particles[contact.i2].invMass + particles[contact.i3].invMass + particles[contact.i4].invMass;
					float s = 2.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / invMass;
					float dLambda = s * (len - len0);
					VectorType disp = dLambda * n;
					v1 += disp * (contact.w1 * particles[contact.i1].invMass);
					v2 += disp * (contact.w2 * particles[contact.i2].invMass);
					v3 += disp * (contact.w3 * particles[contact.i3].invMass);
					v4 -= disp * (particles[contact.i4].invMass);
					continue;
				}
				if (constraints[i].type == ConstraintType::SELF_EDGES)
				{
					ConstraintType& contact = constraints[i];
					Vector3& v1 = particles[contact.i1].pos;
					Vector3& v2 = particles[contact.i2].pos;
					Vector3& v3 = particles[contact.i3].pos;
					Vector3& v4 = particles[contact.i4].pos;
					Vector3 p = v1 + contact.w1 * (v2 - v1);
					Vector3 q = v3 + contact.w2 * (v4 - v3);
					//Vector3 n = q - p;
					//float len = n.Length();
					Vector3 n = contact.normal;
					float len = n.Dot(q - p);
					if (len > contact.len)
						continue;
					selfBroken = true;
					float invMass = particles[contact.i1].invMass + particles[contact.i2].invMass + particles[contact.i3].invMass + particles[contact.i4].invMass;
					float omw1 = 1.f - contact.w1;
					float omw2 = 1.f - contact.w2;
					float s = 2.f / (contact.w1 * contact.w1 + omw1 * omw1 + contact.w2 * contact.w2 + omw2 * omw2) / invMass;
					float dLambda = s * (len - contact.len);
					//n.Scale(1.f / len); // normalize
					Vector3 disp = dLambda * n;
					v1 += disp * (omw1 * particles[contact.i1].invMass);
					v2 += disp * (contact.w1 * particles[contact.i2].invMass);
					v3 -= disp * (omw2 * particles[contact.i3].invMass);
					v4 -= disp * (contact.w2 * particles[contact.i4].invMass);
					continue;
				}
				//if (constraints[i].type == ConstraintType::TRIANGLE)
				//{
				//	ConstraintType& contact = constraints[i];
				//	Vector3 p = contact.w1 * particles[contact.i1].pos + 
				//		contact.w2 * particles[contact.i2].pos + 
				//		contact.w3 * particles[contact.i3].pos;
				//	float len0 = constraints[i].len;
				//	float len = contact.normal.Dot(p - contact.point);
				//	if (len > len0)
				//		continue;
				//	//float invMass = 1.f; //particles[contact.i1].invMass + particles[contact.i2].invMass + particles[contact.i3].invMass; //?
				//	//float s = 2.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3);
				//	float s = 1.f / (contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3);
				//	VectorType disp = s * (len - len0) * contact.normal;// / invMass;
				//	particles[contact.i1].pos -= disp * (contact.w1 * particles[contact.i1].invMass);
				//	particles[contact.i2].pos -= disp * (contact.w2 * particles[contact.i2].invMass);
				//	particles[contact.i3].pos -= disp * (contact.w3 * particles[contact.i3].invMass);
				//	continue;
				//}
				ParticleType* p1 = &particles[constraints[i].i1];
				VectorType delta = p1->pos;
				ParticleType* p2 = NULL;
				if (constraints[i].type == ConstraintType::CONTACT)
				{
					p2 = &ground;
					p2->pos = constraints[i].point;
				}
				else
					p2 = &particles[constraints[i].i2];
				delta.Subtract(p2->pos);
				float len0 = constraints[i].len;
				float len = constraints[i].type == ConstraintType::CONTACT ? delta.Dot(constraints[i].normal) : delta.Length();
				if (constraints[i].type != ConstraintType::LINK && len > len0)
					continue;
				if (constraints[i].type == ConstraintType::CONTACT)
					delta = constraints[i].normal;
				else
					delta.Scale(1 / len);
				const float depth = len - len0;
				delta.Scale(constraints[i].stiffness * omega * depth / (p1->invMass + p2->invMass));
				p1->pos.Subtract(delta * p1->invMass);
				p2->pos.Add(delta * p2->invMass);
			}
		}
		//Printf("n=%d\n", k);
	}

	void Jacobi(std::vector<ConstraintType>& constraints, std::vector<ParticleType>& particles, float omega, int maxIter = 0)
	{
		if (constraints.size() == 0)
			return;
		static ParticleType ground; ground.invMass = 0;

		PROFILE_SCOPE("Jacobi projector");
		int n = maxIter == 0 ? numIterations : maxIter;
		for (int k = 0; k < n; ++k)
		{
			for (size_t i = 0; i < constraints.size(); i++)
			{
				ParticleType* p1 = &particles[constraints[i].i1];
				VectorType delta = p1->pos;
				ParticleType* p2 = NULL;
				if (constraints[i].type == ConstraintType::CONTACT)
				{
					p2 = &ground;
					p2->pos = constraints[i].point;
				}
				else
					p2 = &particles[constraints[i].i2];
				delta.Subtract(p2->pos);
				float len0 = constraints[i].len;
				float len = constraints[i].type == ConstraintType::CONTACT ? delta.Dot(constraints[i].normal) : delta.Length();
				if (constraints[i].type != ConstraintType::LINK && len > len0)
				{
					constraints[i].lambda = 0;
					constraints[i].disp.SetZero();
					continue;
				}
				if (constraints[i].type != ConstraintType::CONTACT)
				{
					delta.Scale(1 / len);
					constraints[i].normal = delta;
				}
				float lambda = (len - len0) / (p1->invMass + p2->invMass);
				constraints[i].disp = lambda * constraints[i].normal;
			}
			for (size_t i = 0; i < constraints.size(); i++)
			{
				VectorType delta = omega * constraints[i].disp;
				particles[constraints[i].i1].pos.Subtract(delta * particles[constraints[i].i1].invMass);
				if (constraints[i].type != ConstraintType::CONTACT)
					particles[constraints[i].i2].pos.Add(delta * particles[constraints[i].i2].invMass);
			}
		}
	}
};

template <typename ParticleType, typename ConstraintType = Constraint, typename VectorType = Vector2>
class Projector
{
private:
	int numIterations;
	std::vector<float> r, q, d;
	std::vector<VectorType> y;
	std::vector<float> r0;

	std::vector<float> ev; // initial guess for eigevenvector 
	float rho; // spectral radius

public:
	Projector() : numIterations(10) { }
	Projector(int n) : numIterations(n) { }

	void SetNumIterations(int n) { numIterations = n; }

#define SKIP_ALPHA
#define SKIP_BETA

	void NewtonMinimumResidual(std::vector<ConstraintType>& links, std::vector<ParticleType>& particles, float alpha)
	{
		PROFILE_SCOPE("MinRes projector");

		size_t nl = links.size();
		if (nl == 0)
			return;
		if (r.size() != nl)
		{
			r.resize(nl);
			q.resize(nl);
		}

		size_t np = particles.size();
		if (y.size() != np) y.resize(np);
		for (int iter = 0; iter < numIterations; iter++)
		{
			// compute error and normal
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				VectorType delta;
				// TODO: use ComputeErrorAndNormal
				if (links[i].type == ConstraintType::CONTACT)
				{
					r[i] = min(0.f, links[i].normal.Dot(particles[i1].pos - links[i].point) - links[i].len);
					//if (iter == 0) links[i].lambda = 0;
				}
				else
				{
					delta = particles[i2].pos - particles[i1].pos;
					r[i] = links[i].len - delta.Length();
					if (links[i].type == ConstraintType::COLL_PAIR && r[i] < 0) r[i] = 0;
					delta.Normalize();
					links[i].normal = delta;
				}
			}

#ifndef SKIP_ALPHA
			// compute dot products for alpha
			float den = 0;
			float dot = 0;

			// y = J^T * r
			memset(&y[0], 0, np * sizeof(VectorType)); // TODO: fill?
			for (size_t i = 0; i < nl; i++)
			{
				VectorType add = links[i].normal * r[i];
				y[links[i].i1] -= add;
				if (links[i].type != ConstraintType::CONTACT)
					y[links[i].i2] += add;
			}
			// q = J * W  * y
			// den = q^T * q
			// dot = c^T * q
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				VectorType diff;
				if (links[i].type != ConstraintType::CONTACT)
					diff = y[i2] * particles[i2].invMass;
				diff -= y[i1] * particles[i1].invMass;
				q[i] = links[i].normal.Dot(diff);

				den += q[i] * q[i];
				dot += r[i] * q[i];
			}

			if (den == 0)
				break;	
			alpha = dot / den;
#endif

			// apply dlambda = alpha * r
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				if (links[i].type != ConstraintType::CONTACT)
				{
					VectorType disp = alpha * r[i] * links[i].normal;
					particles[i1].pos -= particles[i1].invMass * disp;
					particles[i2].pos += particles[i2].invMass * disp;
				}
				else
				{
					float dLambda = -0.5f * r[i]; // the same omega as in Jacobi
					//float lambda0 = links[i].lambda;
					//links[i].lambda = max(0.f, lambda0 + dLambda);
					//dLambda = links[i].lambda - lambda0;
					VectorType disp = dLambda * links[i].normal;
					particles[i1].pos += particles[i1].invMass * disp;
				}
			}
		}	
	}

	float ComputeErrorAndNormal(ConstraintType& link, const std::vector<ParticleType>& particles)
	{
		if (link.type == ConstraintType::CONTACT)
		{
			VectorType delta = particles[link.i1].pos - link.point;
			float c = link.normal.Dot(particles[link.i1].pos - link.point) - link.len;
			return c > 0 ? 0 : c;
		}
		else
		{
			VectorType delta;
			delta = particles[link.i2].pos - particles[link.i1].pos;
			float c = (delta.Length() - link.len) * link.stiffness;
			if (link.type == ConstraintType::COLL_PAIR)
			{
				if (c > 0) c = 0;
			}
			delta.Normalize();
			link.normal = delta;
			return c;
		}		
	}

	void NonlinearConjugateResidual(std::vector<ConstraintType>& links, std::vector<ParticleType>& particles, float alpha0)
	{
		PROFILE_SCOPE("Nonlinear Conjugate Residual");

		size_t nl = links.size();
		if (nl == 0)
			return;
		if (r.size() != nl)
		{
			r.resize(nl);
			d.resize(nl);
			q.resize(nl);
			r0.resize(nl);
		}

		float rSqrOld = 0;
		size_t np = particles.size();
		if (y.size() != np) y.resize(np);

		memset(&d[0], 0, nl * sizeof(float));
		float theta = 1;
		for (int iter = 0; iter < numIterations; iter++)
		{
			// recompute error and normal
			r0 = r;
			for (size_t i = 0; i < nl; i++)
			{
				r[i] = -ComputeErrorAndNormal(links[i], particles);
			}

#ifndef SKIP_BETA
			// A-norm Fletcher-Reeves
			// y = J^T * r
			memset(&y[0], 0, np * sizeof(VectorType));
			for (size_t i = 0; i < nl; i++)
			{
				VectorType add = links[i].normal * r[i];
				y[links[i].i1] += add;
				if (links[i].type != ConstraintType::CONTACT)
					y[links[i].i2] -= add;
			}
			// r^2 = r A r
			float rSqrNew = 0;
			for (size_t i = 0; i < np; i++)
			{
				rSqrNew += y[i].LengthSquared() * particles[i].invMass;
			}

			if (iter > 0)
			{
				// compute new search direction
				float beta = rSqrNew / rSqrOld; // FR
				beta = min(1.f, beta);
				//Printf("beta=%f\n", beta);
				for (size_t i = 0; i < nl; i++)
				{
					d[i] = r[i] + beta * d[i];
				}
			}
			else
			{
				d = r;
			}
			rSqrOld = rSqrNew;
#else
			float theta0 = theta;
			theta = 0.5f * (-theta * theta + theta * sqrt(theta * theta +4));
			float beta = theta0 * (1 - theta0) / (theta0 * theta0 + theta);
			//float beta = (float)iter / (float)(numIterations - 1);
			//beta = pow(beta, 0.6f);
			for (size_t i = 0; i < nl; i++)
			{
				d[i] = r[i] + beta * d[i];
			}
#endif

#ifndef SKIP_ALPHA
			float den = 0;
			float dot = 0;
			// y = J^T * d
			memset(&y[0], 0, np * sizeof(VectorType));
			for (size_t i = 0; i < nl; i++)
			{
				VectorType add = links[i].normal * d[i];
				y[links[i].i1] -= add;
				if (links[i].type != ConstraintType::CONTACT)
					y[links[i].i2] += add;
 			}
			// q = J * W  * y = A * d
			// den = q^T * q
			// dot = r^T * q
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				if (links[i].type != ConstraintType::CONTACT)
					q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);
				else
				{
					q[i] = -links[i].normal.Dot(y[i1] * particles[i1].invMass);
				}

				den += q[i] * q[i];
				dot += r[i] * q[i];
			}

			if (den == 0)
				break;
			float alpha = min(dot / den, alpha0);
			//Printf("%f\n", alpha);
#else
			float alpha = alpha0;
#endif

			// apply dlambda = alpha * d
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				if (links[i].type != ConstraintType::CONTACT)
				{
					//alpha = 1.f / (particles[i1].invMass + particles[i2].invMass); // lsj
					float dLambda = alpha * d[i];
					VectorType disp = dLambda * links[i].normal;
					particles[i1].pos -= particles[i1].invMass * disp;
					particles[i2].pos += particles[i2].invMass * disp;
				}
				else
				{
					//alpha = 1.f / particles[i1].invMass; // lsj
					float dLambda = alpha * d[i];
					VectorType disp = dLambda * links[i].normal;
					particles[i1].pos += particles[i1].invMass * disp;
				}
			}
		}
	}

	void ConjugateJacobi(std::vector<ConstraintType>& constraints, std::vector<ParticleType>& particles, float omega)
	{
		PROFILE_SCOPE("Conjugate Jacobi");
		if (constraints.size() == 0)
			return;
		static ParticleType ground; ground.invMass = 0;

#ifndef SKIP_BETA
		size_t np = particles.size();
		if (y.size() != np) y.resize(np);
		float rSqrOld;
#endif

		for (int k = 0; k < numIterations; ++k)
		{
			for (size_t i = 0; i < constraints.size(); i++)
			{
				ParticleType* p1 = &particles[constraints[i].i1];
				VectorType delta = p1->pos;
				ParticleType* p2 = NULL;
				if (constraints[i].type == ConstraintType::CONTACT)
				{
					p2 = &ground;
					p2->pos = constraints[i].point;
				}
				else
					p2 = &particles[constraints[i].i2];
				delta.Subtract(p2->pos);
				float len0 = constraints[i].len;
				float len = constraints[i].type == ConstraintType::CONTACT ? delta.Dot(constraints[i].normal) : delta.Length();
				if (constraints[i].type != ConstraintType::LINK && len > len0)
				{
					constraints[i].lambda = 0;
					constraints[i].disp.SetZero();
					continue;
				}
				if (constraints[i].type != ConstraintType::CONTACT)
				{
					delta.Scale(1 / len);
					constraints[i].normal = delta;
				}
				constraints[i].lambda = len - len0;
			}
#ifdef SKIP_BETA
			float beta = (float)k / (float)(numIterations - 1);
			beta = pow(beta, 0.8f);
#else
			// A-norm Fletcher-Reeves
			// y = J^T * r
			memset(&y[0], 0, np * sizeof(VectorType));
			for (size_t i = 0; i < constraints.size(); i++)
			{
				VectorType add = constraints[i].normal * constraints[i].lambda;
				y[constraints[i].i1] += add;
				if (constraints[i].type != ConstraintType::CONTACT)
					y[constraints[i].i2] -= add;
			}
			// r^2 = r A r
			float rSqrNew = 0;
			for (size_t i = 0; i < np; i++)
			{
				rSqrNew += y[i].LengthSquared() * particles[i].invMass;
			}

			float beta = 0.f;
			if (k > 0)
			{
				beta = rSqrNew / rSqrOld; // FR
				beta = min(1.f, beta);
			}
			rSqrOld = rSqrNew;
#endif
			//Printf("%f\n", beta);
			for (size_t i = 0; i < constraints.size(); i++)
			{
				// Jacobi version
				//float omega = 0.35f;
				float alpha;
				if (constraints[i].type != ConstraintType::CONTACT)
					alpha = omega / (particles[constraints[i].i1].invMass + particles[constraints[i].i2].invMass);
				else
					alpha = omega / particles[constraints[i].i1].invMass;

				constraints[i].disp = constraints[i].stiffness * (alpha * constraints[i].lambda * constraints[i].normal + beta * constraints[i].disp);
				VectorType delta = constraints[i].disp;
				particles[constraints[i].i1].pos.Subtract(delta * particles[constraints[i].i1].invMass);
				if (constraints[i].type != ConstraintType::CONTACT)
					particles[constraints[i].i2].pos.Add(delta * particles[constraints[i].i2].invMass);
			}
		}
	}

	float SpectralRadius(std::vector<ConstraintType>& links, std::vector<ParticleType>& particles, std::vector<float>& x, int iters)
	{
		PROFILE_SCOPE("Spectral radius");
		// power method for computing the largest eigenvalue (absolute value) and its eigenvector
		//MEASURE_TIME("Spectral Radius");
		size_t nl = links.size();
		if (nl == 0)
			return 0;
	
		// compute normals
		for (size_t i = 0; i < nl; i++)
		{
			const int i1 = links[i].i1;
			const int i2 = links[i].i2;
			VectorType delta;
			delta = particles[i2].pos - particles[i1].pos;			
			delta.Normalize();
			links[i].normal = delta;
		}

		size_t np = particles.size();
		if (y.size() != np) y.resize(np);
		if (q.size() != nl) q.resize(nl);
		float sr = 0;
		for (int iter = 0; iter < iters; iter++)
		{
			// y = J^T * r
			memset(&y[0], 0, np * sizeof(VectorType));
			for (size_t i = 0; i < nl; i++)
			{
				VectorType add = links[i].normal * x[i];
				y[links[i].i1] -= add;
				y[links[i].i2] += add;
			}
			// q = J * W  * y
			float len2 = 0;
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = links[i].i1;
				const int i2 = links[i].i2;
				q[i] = links[i].normal.Dot(y[i2] * particles[i2].invMass - y[i1] * particles[i1].invMass);
				len2 += q[i] * q[i];
			}
			if (len2 == 0)
				break;
			if (iter == iters - 1)
			{
				// lambda = x^T * q
				for (size_t i = 0; i < nl; i++)
				{
					sr += x[i] * q[i];
				}
			}
			// x = normalize(q)
			float invLen = 1.f / sqrt(len2);
			for (size_t i = 0; i < nl; i++)
			{
				x[i] = q[i] * invLen;
			}
		}
		return sr;
	}

	float ComputeSpectralRadius(std::vector<ConstraintType>& links, std::vector<ParticleType>& particles)
	{
		if (links.empty())
			return 0.f;
		int eigIters = 1;
		if (ev.empty())
		{
			ev.resize(links.size());
			std::fill(ev.begin(), ev.end(), 0.f);
			ev[0] = 1;
			eigIters = 50;
		}
		if (ev.size() != links.size())
		{
			ev.resize(links.size());
			eigIters = 10;
		}
		rho = SpectralRadius(links, particles, ev, eigIters);
		return rho;
	}
};



#endif