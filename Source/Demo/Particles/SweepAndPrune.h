#ifndef SWEEP_AND_PRUNE_H
#define SWEEP_AND_PRUNE_H

#include <Engine/Types.h>
#include <Demo/Particles/Collision.h>
#include <Demo/Particles/Constraint.h>

//#include <hash_set>
#include <set>

enum EndpointType
{
	BEGIN,
	END
};

class Endpoint
{
private:
	// coordinate along a certain axis
	float pos;

	// index of the AABB (body/particle) and type (BEGIN/END)
	unsigned short data;

public:

	Endpoint() : pos(0), data(0) { }

	int GetIndex()
	{
		return data & 0x7fff;
	}

	int GetType()
	{
		return data >> 15;
	}

	void SetIndex(int index)
	{
		data = (data & 0x8000) | (index & 0x7fff);
	}

	void SetType(int type)
	{
		data = (data & 0x7fff) | ((type & 1) << 15);
	}

	float GetPos()
	{
		return pos;
	}

	void SetPos(float p)
	{
		pos = p;
	}
};

class SweepAndPrune
{
private:
	std::vector<Endpoint> epx;
	std::vector<Endpoint> epy;
	std::set<uint32> pairsSet;

public:
	void Init(const AabbVector& aabbs);
	virtual void Update(const AabbVector& aabbs, ConstraintVector& pairs);

private:
	void UpdateAxis(std::vector<Endpoint>& endpoints, const AabbVector& aabbs);
	void UpdateAxisSweep(std::vector<Endpoint>& endpoints);
	void AddPair(int index1, int index2, const AabbVector& aabbs);
	void RemovePair(int index1, int index2);
};

inline void SweepAndPrune::RemovePair(int index1, int index2)
{
	int i, j;
	if (index1 < index2)
	{
		i = index1;
		j = index2;
	}
	else
	{
		j = index1;
		i = index2;
	}
	uint32 pair = (i << 16) | j;
	pairsSet.erase(pair);
}

inline void SweepAndPrune::AddPair(int index1, int index2, const AabbVector& aabbs)
{
	// TODO: optimize AABB overlap test
	if (AabbOverlap(aabbs[index1], aabbs[index2]))
	{
		int i, j;
		if (index1 < index2)
		{
			i = index1;
			j = index2;
		}
		else
		{
			j = index1;
			i = index2;
		}
		uint32 pair = (i << 16) | j;
		pairsSet.insert(pair);
	}
}

#endif //SWEEP_AND_PRUNE_H