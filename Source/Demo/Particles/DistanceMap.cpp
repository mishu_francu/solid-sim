#include <Engine/Base.h>
#include "DistanceMap.h"

#if !defined(_WIN64) && !defined(USE_GDIPLUS) && (RENDERER != DIRECTX)
#include <stack>

const float scaler = 0.93f;

bool DistanceMap::Create(const char* fileName)
{
	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(fileName, 0);
	if(fif == FIF_UNKNOWN) 
		fif = FreeImage_GetFIFFromFilename(fileName);
	if(fif == FIF_UNKNOWN)
		return false;

	FIBITMAP *dib(0);
	if(FreeImage_FIFSupportsReading(fif))
		dib = FreeImage_Load(fif, fileName);
	if(!dib)
		return false;

	BYTE* bits = FreeImage_GetBits(dib);
	m_width = FreeImage_GetWidth(dib);
	m_height = FreeImage_GetHeight(dib);

	// allocate map and build it
	int size = m_width * m_height;
	m_map = new float[size];
	for (int i = 0; i < size; ++i)
		m_map[i] = 1e37f;
	unsigned int* image = new unsigned int[size];
	int bpp = FreeImage_GetBPP(dib) / 8	;
	for (int y = 0; y < m_height; ++y) {
		for (int x = 0; x < m_width; ++x) {
			int idx = x + y * m_width;
			BYTE red = bits[idx * bpp];
			BYTE green = bits[idx * bpp + 1];
			BYTE blue = bits[idx * bpp + 2];
			image[idx] = red;
			if (image[idx] > 0)
				image[idx] = 0xffffffff;
		}
	}
	for (int y = 0; y < m_height; ++y) {
		for (int x = 0; x < m_width; ++x) {
			if (image[x + y * m_width] == 0) {
				m_map[x + y * m_width] = 0;
				std::stack<int> stack;
				stack.push(x + 1 + y * m_width);
				stack.push(x - 1 + y * m_width);
				stack.push(x + (y + 1) * m_width);
				stack.push(x + (y - 1) * m_width);
				//stack.push(x + 1 + (y + 1) * m_width);
				//stack.push(x - 1 + (y + 1) * m_width);
				//stack.push(x + 1 + (y - 1) * m_width);
				//stack.push(x - 1 + (y - 1) * m_width);
				while (!stack.empty()) {
					int idx = stack.top();
					stack.pop();
					if (idx >= 0 && idx < size && image[idx] != 0) {
						int x1 = idx % m_width;
						int y1 = idx / m_width;
						int dx = x - x1;
						int dy = y - y1;
						//float dist = sqrt((float)(dx * dx + dy * dy));
						unsigned int dist = dx * dx + dy * dy;
						if (dist < image[idx]) {
							image[idx] = dist;
							stack.push(x1 + 1 + y1 * m_width);
							stack.push(x1 - 1 + y1 * m_width);
							stack.push(x1 + (y1 + 1) * m_width);
							stack.push(x1 + (y1 - 1) * m_width);
							//stack.push(x1 + 1 + (y1 + 1) * m_width);
							//stack.push(x1 - 1 + (y1 + 1) * m_width);
							//stack.push(x1 + 1 + (y1 - 1) * m_width);
							//stack.push(x1 - 1 + (y1 - 1) * m_width);
						}
					}
				}
			}
		}
	}
	float maxDist = 0;
	for (int i = 0; i < size; i++) {
		m_map[i] = sqrt(float(image[i]));
		if (m_map[i] > maxDist)
			maxDist = m_map[i];
	}
	Printf("maxDist=%f\n", maxDist);
	for (int y = 0; y < m_height; ++y) {
		for (int x = 0; x < m_width; ++x) {
			int val = (int)(m_map[x + y * m_width] * scaler);// * 255.f / maxDist;
			bits[0] = val;
			bits[1] = val;
			bits[2] = val;
			bits += 3;
		}
	}
	delete[] image;

	FreeImage_Save(fif, dib, "dist_map.bmp");
	FreeImage_Unload(dib);
	return true;
}

bool DistanceMap::Load(const char* fileName)
{
	FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(fileName, 0);
	if(fif == FIF_UNKNOWN) 
		fif = FreeImage_GetFIFFromFilename(fileName);
	if(fif == FIF_UNKNOWN)
		return false;

	FIBITMAP *dib(0);
	if(FreeImage_FIFSupportsReading(fif))
		dib = FreeImage_Load(fif, fileName);
	if(!dib)
		return false;

	BYTE* bits = FreeImage_GetBits(dib);
	m_width = FreeImage_GetWidth(dib);
	m_height = FreeImage_GetHeight(dib);

	// allocate map and build it
	int size = m_width * m_height;
	m_map = new float[size];
	int bpp = FreeImage_GetBPP(dib) / 8	;
	for (int y = 0; y < m_height; ++y) {
		for (int x = 0; x < m_width; ++x) {
			int idx = x + y * m_width;
			BYTE red = bits[idx * bpp];
			BYTE green = bits[idx * bpp + 1];
			BYTE blue = bits[idx * bpp + 2];
			m_map[x + (m_height - y) * m_width] = red / scaler;
		}
	}

	FreeImage_Unload(dib);
	return true;
}
#endif // _WIN64