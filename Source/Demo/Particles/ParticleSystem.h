#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H

#include <Math/Vector2.h>
#include <Engine/Profiler.h>
#include <Graphics2D/Graphics2D.h> // TODO: move to mesh header
#include <Demo/Particles/PhysicsSystem.h>
#include <Demo/Particles/Collision.h>
//#include <Demo/Particles/JobQueue.h>
//#include <Demo/Particles/DistanceMap.h>
#include <Demo/Particles/GridBroadphase.h>
#include <Math/Matrix.h>
#include <Demo/Particles/Metrics.h>
#ifndef ANDROID_NDK
//#	include <Physics/OpenCL.h>
#endif

#include <vector>

namespace OldMetrics
{
	struct SConstraintMetric
	{
		int id;
		float lambda;
		float depth;
		float vrel;
		SConstraintMetric() : id(0), lambda(0.f), depth(0.f), vrel(0.f) { }
	};

	struct Metrics
	{
		float energyK, energyP, penetration, vrel, lambda;
		std::vector<SConstraintMetric> constraints;
		Metrics() : energyK(0), energyP(0), penetration(0), vrel(0), lambda(0) { }
	};
}

template<typename Particle, typename Broadphase = GridBroadphase>
class ParticleSystem : public PhysicsSystem
{
public:
	typedef std::vector<Particle> ParticleVector;

	struct Frame
	{
		ParticleVector particles;
	};

public:
	AabbVector aabbs;
	Broadphase broadphase;
	
	// Configuration
	float radius;
	float restitution; // used?

	// Metrics
	//std::vector<OldMetrics::Metrics> metrics;
	std::vector<Frame> frames;
	bool saveMetrics;

	Metrics<Vector2> newMetrics;

protected:
	float width, height;
	ParticleVector particles;

	// sparse matrix
	std::vector<float> diag;
	std::vector<SparseElement> sparseMat;
	// dense matrix
	MatrixSqr<float> mat;

	std::vector<Vector2> normals;
	std::vector<float> r, z, d;

public:
	ParticleSystem();
	virtual ~ParticleSystem();
	virtual void Init(float w, float h, float r);

	void SetNumParticles(int n);
	size_t GetNumParticles() const { return particles.size(); }
	const ParticleVector& GetParticles() const { return particles; }
	Particle& GetParticle(size_t i) { return particles[i]; }
	int GetParticleAt(float x, float y);
	//virtual size_t GetParticleSize() { return sizeof(Particle); }

	void SetParticleVelocity(size_t i, const Vector2& v)
	{
		particles[i].SetVelocity(v, timeStep);
	}

	Vector2 GetParticleVelocity(size_t i)
	{
		return particles[i].GetVelocity();
	}

	Constraint* AddLink(ParticleIdx parIdx1, ParticleIdx parIdx2, float stiffness = 1);

	void BuildIncidenceMatrix();
	void BuildMatrix();

protected:
	void Collide();
	void ProjectPositions(int maxIter = 0);
	void ProjectPositionsJacobi(float, int maxIter = 0);
	
	void ProjectPositionsExact();

	// obsolete
	void ProjectPositionsSd();
	void ProjectPositionsMinRes(bool gsWarmStart);
	void ProjectPositionsMinRes1();
	void SolveNonlinearConjugateGradient();

	void RecordMetrics();
	void SaveMetrics();

//private:
	void PairwiseBroadphase();
	void AddCollisionPair(int a, int b);
	void AddContact(size_t idx, const Vector2& p, const Vector2& n);
	void UpdateAabbs();
	void WallCollisions();
	// constraint solving functions
	void SolvePairPgs(Constraint& pair, const Scalar& r, const Scalar& diamSqr);
	void SolvePairPgsLambda(Constraint& pair, const Scalar& r, const Scalar& diamSqr); // Catto version
	void SolveContactPgs(Constraint& pair, const Scalar& r);
	void SolveContactJacobi(Constraint& pair, float r);
	void SolvePositionsJacobi(Constraint& pair);
	void SolveLinkJakobsen(Constraint& link);
	void SolveLinkShake1(Constraint& link);
	void SolveLinkShakeExact(Constraint& link);

	// Eigen
	void AnalyzeEigenValues();

	void ProjectNewtonJacobi(int maxIter = 0);

	void ProjectNewtonSteepestDescent(int maxIters, bool minRes);
	void ProjectNewtonConjugateGradient();
	void ProjectNewtonMinimumResidual();
	void ProjectNewtonMinimumResidual1();
	void ProjectNonlinearConjugateGradient();
	void ProjectNonlinearMinimumResidual();

	void ProjectMinRes(float);
	void ProjectNCR();

	float SpectralRadius(std::vector<float>&, int);

	// Verlet & Shake
	void IntegrateVerlet();

	void ProjectExplicit();
	void ProjectNewton();

	void StoreNormalsForShake();
	void BuildMatrixShake();
};

void MultiplyBySparseMatrix(const std::vector<float>& x, const std::vector<SparseElement>& a, const std::vector<float>& diag, std::vector<float>& y);

#include "ParticleSystem.inl"

#ifndef ANDROID_NDK
#	include <Demo/Particles/ParticleSystem_Eigen.hpp>
#endif

#endif // PARTICLE_SYSTEM_H
