#ifndef PARTICLE_SYSTEM_VEL_H
#define PARTICLE_SYSTEM_VEL_H

#include "ParticleSystem.h"
#include <map>

class ParticleSystemVelocity : public ParticleSystem<ParticleVelocity>
{
public:
	enum PairSolverType
	{
		JAKOBSEN,
		JAKOBSEN_FRICTION,
		CATTO,
		CATTO_FRICTION
	};

	enum IntegratorType
	{
		EULER,
		SYMPLECTIC_EULER,
		VELOCITY_VERLET,
		LEAPFROG
	};

protected:
	struct ConstraintInfo
	{
		ConstraintInfo() : len(0) { }
		Vector2 normal;
		float len;
		Scalar lambdaF;
	};

	typedef std::vector<ConstraintInfo> TConstraintInfos;

public:
	ParticleSystemVelocity()
		: warmStarting(false)
	{
	}

protected:
	void ProjectVelocities();
	
	template<PairSolverType PST> 
	void SolvePairs(TConstraintInfos&, int iteration);
	template<PairSolverType PST> 
	void SolveContacts(TConstraintInfos&, int iteration);
	
	void SolveConstraints();
	void SolveJacobi(float omega, bool minRes, float gamma);
	void SolveMinRes(float alpha);
	void SolveConjRes(float omega, bool minRes);
	void SolveNesterov(float omega, float beta);
	
	void PrintMetrics();
	void LogMetrics();
	
	void ProjectVelocitiesExact();

protected:
	//int numItersVel;
	bool warmStarting;
	std::map<uint32, float> lambdas;
};

#endif // PARTICLE_SYSTEM_VEL_H