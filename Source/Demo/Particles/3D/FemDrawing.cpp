#include <Demo/Particles/3D/FemDrawing.h>
using namespace FEM_SYSTEM;

FemDrawing::FemDrawing(std::vector<Tet>& tetrahedra, std::vector<Node>& nodesIn, FEM_SYSTEM::FemPhysicsBase& femPhysics)
	: nodes(nodesIn)
	, tets(tetrahedra)
	, mFemPhysics(femPhysics)
{
};

FemDrawing::~FemDrawing() {};

void FemDrawing::DrawTetrahedra(Graphics3D* graphics3D) const
{
	graphics3D->SetFlags(8);
	//graphics3D->SetColor(1, 1, 1);
	//graphics3D->DrawAxes();
	//graphics3D->DrawWireCube(Vector3(), Vector3(100));
	//graphics3D->DrawWireCube(Vector3(0, 0, 0), Vector3(2 * PhysicsSystem3D::WALL_DIST, 2 * PhysicsSystem3D::WALL_DIST, 2 * PhysicsSystem3D::WALL_DIST));
	for (size_t i = 0; i < tets.size(); i++)
	{
		const FEM_SYSTEM::Tet& tet = tets.at(i);
		graphics3D->DrawTetrahedron(DoubleToFloat(nodes.at(tet.idx[0]).pos * 100.f), 
			DoubleToFloat(nodes.at(tet.idx[1]).pos * 100.f), 
			DoubleToFloat(nodes.at(tet.idx[2]).pos * 100.f), 
			DoubleToFloat(nodes.at(tet.idx[3]).pos * 100.f));
	}
	graphics3D->SetFlags(0);
	CheckGlError();
}

void FemDrawing::DrawBBTetrahedra(Graphics3D* graphics3D) const
{
#ifdef HO_RENDERING
	graphics3D->SetFlags(8);
	// NOTE this is a big NO, edge_pairs is internal knowledge of how TetrahedralMesh is built
	// and it should NEVER be used outside of TetrahedralMesh as it may change at any point.
	// However, as HO_RENDERING is an experimental feature, we do not care..
	std::tuple<int, int> edge_pairs[]{
		std::make_tuple(0, 1),
		std::make_tuple(0, 2),
		std::make_tuple(0, 3),
		std::make_tuple(1, 2),
		std::make_tuple(1, 3),
		std::make_tuple(2, 3)
	};

	size_t nodesperedge = size_t(mFemPhysics.GetTetMesh()->GetNodesPerEdge(mFemPhysics.GetTetMesh()->GetOrder()));
	std::vector<Vector3> edgenodes; edgenodes.resize(nodesperedge);
	Vector3 c1, c2;
	for (int e = 0; e < mFemPhysics.GetTetMesh()->GetNumElements(); e++)
	{
		for (int i = 0; i < 6; i++)
		{
			int e1, e2; std::tie(e1, e2) = edge_pairs[i];

			for (size_t j = 0; j < nodesperedge; j++)
			{
				uint32 idx = mFemPhysics.GetTetMesh()->GetGlobalIndex(e, (uint32)(4 + i * nodesperedge + j));
				Vector3R p = mFemPhysics.GetDeformedPosition(idx);
				edgenodes[j] = DoubleToFloat(p) * 100.0f;
			}
			c1 = DoubleToFloat(mFemPhysics.GetDeformedPosition(mFemPhysics.GetTetMesh()->GetGlobalIndex(e, e1))) * 100.0f;
			c2 = DoubleToFloat(mFemPhysics.GetDeformedPosition(mFemPhysics.GetTetMesh()->GetGlobalIndex(e, e2))) * 100.0f;

			if (nodesperedge > 0)
			{
				graphics3D->DrawTetrahedronEdgeAsBBCurve(c1, c2, nodesperedge, &edgenodes[0]);
			}
			else
			{
				graphics3D->DrawLine(c1, c2);
			}
		}
	}

	graphics3D->SetFlags(0);
	CheckGlError();
#endif
}
