//#include "PCH.h"
#include <Demo/Particles/Particles.h>
#include <Graphics3D/Graphics3D.h>
#include <Geometry/Assets.h>
#include <Geometry/Collision3D.h>
#include <Physics/RigidSystem.h>
#include <Geometry/AabbTree.h>
#include <Demo/Particles/3D/ClothDemo.h>

#include <stack>

using namespace Geometry;

#pragma warning( disable : 4267) // for size_t-uint conversions

//#define DEBUG_LIGHT_POS

//void ParticlesDemo::SetPlatformFromString(const char* str)
//{
//	bool useCL = false;
//	if (strcmp(str, "OpenCL") == 0)
//		useCL = true;
//	phys3D.SetUseOpenCL(useCL);
//	cloth.GetModel().SetUseCL(useCL);
//}

void ParticlesDemo::OnMouseMove(int x, int y)
{
	if (is3D)
		mClothDemo->OnMouseMove(x, y);
}

void ParticlesDemo::OnMouseDown(int x, int y, MouseButton mb)
{
	if (is3D && mb == MouseButton::MOUSE_LEFT)
		mClothDemo->OnMouseDown(x, y);
}

void ParticlesDemo::OnMouseUp(int x, int y, MouseButton mb)
{
	if (is3D)
		mClothDemo->OnMouseUp(x, y);
}

void ParticlesDemo::InitParticles3D()
{
#ifndef ANDROID_NDK
	bool randomVelocities = true;
	bool stack = false;

	phys3D.Init(radius);
	colors3D.resize(numParticles);
	phys3D.SetCollisionFlags(PhysicsSystem3D::COLL_WALLS | PhysicsSystem3D::COLL_MESH | PhysicsSystem3D::COLL_PARTICLES);
	if (drip)
		return;
	const float spacing = 2.5f * radius;
	phys3D.SetNumParticles(numParticles);
	const float maxVel = 0.01f * phys3D.GetLengthScale();

	int side = stack ? 1 : (int)(pow(numParticles, 1.f / 3.f) + 1);
	int k = 0;
	float half = (side - 1) * spacing * 0.5f;
	float maxY = half + 100.f;
	float y = maxY;
	while (k < numParticles)
	{
		float x = -half;
		for (int i = 0; i < side; i++)
		{
			float z = -half;
			for (int j = 0; j < side; j++)
			{
				colors3D[k].r = GetRandomReal01();
				colors3D[k].g = GetRandomReal01();
				colors3D[k].b = GetRandomReal01();
				//phys3D.GetParticle(k).invMass = 0.01f + GetRandomReal01() * 1000;
				//float c = phys3D.GetParticle(k).invMass / 1000.1f;
				//colors3D[k].r = c;
				//colors3D[k].g = c;
				//colors3D[k].b = c;
				phys3D.GetParticle(k).pos = Vector3(x, y, z);

                if (randomVelocities)
				    phys3D.GetParticle(k).SetVelocity(maxVel * Vector3(GetRandomReal11(), GetRandomReal11(), GetRandomReal11()), phys3D.GetTimeStep());

				k++;
				if (k >= numParticles)
					return; //FIXME
				z += spacing;
			}
			x += spacing;
		}
		y -= spacing;
	}
#endif
}

void ParticlesDemo::InitLinks3D()
{
#ifndef ANDROID_NDK
	phys3D.Init(radius);
	const int n = divisions; // number of particles
	float inc = (getScreenHeight() * 0.9f - radius) / (n - 1);
	const float startY = 0.5f * (n - 1) * inc;
	phys3D.SetNumParticles(n);
	phys3D.GetParticle(0).invMass = 0.f;
	for (int i = 0; i < n; i++) 
	{
		phys3D.GetParticle(i).pos.Set(0, startY - inc * i, 0);
		phys3D.GetParticle(i).SetVelocity(Vector3(0, 0, 0), phys3D.GetTimeStep());
		if (i > 0)
		{
			phys3D.AddLink(i - 1, i);
		}
	}
	phys3D.GetParticle(n - 1).SetVelocity(Vector3(-250.f * phys3D.GetLengthScale(), 0.f, 0), phys3D.GetTimeStep());
#endif
}

inline int MortonCode(int x, int y)
{
	int ret = 0;
	for (int i = 0; i < 8; i++) // 8 bits per axis
	{
		int mask = (1 << i);
		int xd = (x & mask) >> i; 

		int yd = (y & mask) >> i; 
		ret |= (xd << (2 * i)) | (yd << (2 * i + 1));
	}
	return ret;
}

inline int TileCode(int x, int y, int width)
{
	const int b = 4;
	int nx = width >> b;
	int tile = (x >> b) + (y >> b) * nx;
	int mask = (1 << b) - 1;
	return tile * (1 << (2 * b)) + (x & mask) + ((y & mask) << b);
}

void ParticlesDemo::InitCloth3D()
{
	MEASURE_TIME("InitCloth3D");
	mClothDemo->Init();
}

static Mesh renderMesh;

Vector3 HermiteSpline(float t, const Vector3& p0, const Vector3& m0, const Vector3& p1, const Vector3& m1)
{
	float t2 = t * t;
	float t3 = t2 * t;
	return (2 * t3 - 3 * t2 + 1) * p0 + (t3 - 2 * t2 + t) * m0 + (-2 * t3 + 3 * t2) * p1 + (t3 - t2) * m1;
}

Vector3 CatmullRom(float s, const Vector3 p[])
{
	Vector3 a = -0.5f * p[0] + 1.5f * p[1] - 1.5f * p[2] + 0.5f * p[3];
	Vector3 b =         p[0] - 2.5f * p[1] +  2.f * p[2] - 0.5f * p[3];
	Vector3 c = -0.5f * p[0] +               0.5f * p[2];
	Vector3 d = p[1];
	float s2 = s * s;
	float s3 = s2 * s;
	return a * s3 + b * s2 + c * s + d;
}

#define BICUBIC_TESSELLATION

void ParticlesDemo::TessellateCloth()
{
	Mesh* mesh = phys3D.GetClothMesh();
	const int n = divisions;

	// estimate normals
	mesh->normals.resize(mesh->vertices.size());
	mesh->tangents.resize(mesh->vertices.size());
	mesh->bitangents.resize(mesh->vertices.size());
	renderMesh.vertices.clear();
	renderMesh.normals.resize(mesh->vertices.size());
	renderMesh.indices.clear();
	float inc = (getScreenHeight() * 0.9f - radius) / (n - 1);
	for (int j = 0, base = 0; j < n; j++, base += n)
	{
		for (int i = 0; i < n; i++) 
		{
			int idx = base + i;
			renderMesh.AddVertex(phys3D.GetParticle(idx).pos);

			// derivatives wrt s
			Vector3 ds = (idx >= n) ? phys3D.GetParticle(idx).pos - phys3D.GetParticle(idx - n).pos : phys3D.GetParticle(idx + n).pos - phys3D.GetParticle(idx).pos;
			//ds.Scale(1.f / inc);
			// derivatives wrt t
			Vector3 dt = (i >= 1) ? phys3D.GetParticle(idx).pos - phys3D.GetParticle(idx - 1).pos : phys3D.GetParticle(idx + 1).pos - phys3D.GetParticle(idx).pos;
			//dt.Scale(1.f / inc);
			// these are tangent vectors
			Vector3 normal = cross(ds, dt);
			normal.Normalize();
			//normal.Flip();
			mesh->normals[idx] = normal;
			renderMesh.normals[idx] = normal;

			//ds.Normalize();
			ds.Scale(1.f / inc);
			mesh->tangents[idx] = ds;

			//dt.Normalize();
			dt.Scale(1.f / inc);
			mesh->bitangents[idx] = dt;
		}
	}

	// retriangulate
	for (int j = 2, base = j * n; j < n - 1; j++, base += n)
	{
		for (int i = 2; i < n - 1; i++) 
		{
			int idx = base + i;

#ifdef BICUBIC_TESSELLATION
			// interpolate the 4 midpoints
			Vector3 vertical0[4] = {phys3D.GetParticle(idx - 2 * n - 2).pos, phys3D.GetParticle(idx - 2 * n - 1).pos, phys3D.GetParticle(idx - 2 * n).pos, phys3D.GetParticle(idx - 2 * n + 1).pos };
			Vector3 c0 = CatmullRom(0.5f, vertical0);
			Vector3 vertical1[4] = {phys3D.GetParticle(idx - n - 2).pos, phys3D.GetParticle(idx - n - 1).pos, phys3D.GetParticle(idx - n).pos, phys3D.GetParticle(idx - n + 1).pos };
			Vector3 c1 = CatmullRom(0.5f, vertical1);
			Vector3 vertical2[4] = {phys3D.GetParticle(idx - 2).pos, phys3D.GetParticle(idx - 1).pos, phys3D.GetParticle(idx).pos, phys3D.GetParticle(idx + 1).pos };
			Vector3 c2 = CatmullRom(0.5f, vertical2);
			Vector3 vertical3[4] = {phys3D.GetParticle(idx + n - 2).pos, phys3D.GetParticle(idx + n - 1).pos, phys3D.GetParticle(idx + n).pos, phys3D.GetParticle(idx + n + 1).pos };
			Vector3 c3 = CatmullRom(0.5f, vertical3);
			Vector3 horizontal[4] = {c0, c1, c2, c3};
			Vector3 c = CatmullRom(0.5f, horizontal);
			int newIdx = renderMesh.AddVertex(c);

			// TODO: better than linear normal interpolation
			Vector3 normal = (mesh->normals[idx] + mesh->normals[idx - 1] + mesh->normals[idx - n - 1] + mesh->normals[idx - n]);
			normal.Normalize();
			renderMesh.normals.push_back(normal);

			renderMesh.AddTriangle(idx, idx - 1, newIdx);
			renderMesh.AddTriangle(idx - 1, idx - n - 1, newIdx);
			renderMesh.AddTriangle(idx - n - 1, idx - n, newIdx);
			renderMesh.AddTriangle(idx - n, idx, newIdx);
#else
			renderMesh.AddTriangle(idx - n - 1, idx - n, idx);
			renderMesh.AddTriangle(idx - n - 1, idx, idx - 1);
#endif
		}
	}
}

void ParticlesDemo::InitClothParticles3D()
{
#ifndef ANDROID_NDK
	phys3D.Init(radius);
	const int n = divisions;
	int m = n * n + numParticles;
	phys3D.SetNumParticles(n * n);
	colors3D.resize(m);
	//phys3D.SetCollisionFlags(/*PhysicsSystem3D::COLL_WALLS | */PhysicsSystem3D::COLL_SELF | PhysicsSystem3D::COLL_PARTICLES);

	// TODO: prevent duplication
	float inc = (getScreenHeight() * 0.9f - radius) / (n - 1);
	//float inc = - 4 * radius;
	float x = -0.5f * divisions * inc;
	const float startY = x;
	const float h = phys3D.GetTimeStep();
	const float shear = 200 * h * h;
	const float bend = 20 * h * h;
	const float level = 100;
	Mesh mesh;
	mesh.vertices.resize(n * n);
	for (int j = 0, base = 0; j < n; j++, base += n)
	{
		phys3D.GetParticle(base).pos.Set(x, level, startY);
		phys3D.GetParticle(base).SetVelocity(Vector3(0.f, 0.f, 0.f), h);
		int batchH = 2 + (j & 1);
		if (base >= n)
		{
			phys3D.AddLink(base - n, base, batchH);
			phys3D.AddLink(base - n + 1, base, -1, shear);
			if (base >= 2 * n)
				phys3D.AddLink(base - 2 * n, base, -1, bend);
		}
		for (int i = 1; i < n; i++) 
		{
			int idx = base + i;
			phys3D.GetParticle(idx).pos.Set(x, level, startY + inc * i);
			phys3D.GetParticle(idx).SetVelocity(Vector3(0.f, 0.f, 0.f), h);
			phys3D.GetParticle(idx).invMass = 1;
			if (idx > base)
			{
				phys3D.AddLink(idx - 1, idx, i & 1);
				if (idx > base + 1)
					phys3D.AddLink(idx - 2, idx, -1, bend);
				if (idx >= n)
				{
					phys3D.AddLink(idx - n, idx, batchH);
					if (i < n - 1)
						phys3D.AddLink(idx - n + 1, idx, -1, shear);
					if (j > 1)
						phys3D.AddLink(idx - 2 * n, idx, -1, bend);
				}
				// shear links
				if (idx > n)
				{
					phys3D.AddLink(idx - n - 1, idx, -1, shear);

					mesh.AddTriangle(idx - n - 1, idx - n, idx);
					mesh.AddTriangle(idx - n - 1, idx, idx - 1);
				}
			}
		}
		if (j == 0 || j == n - 1)
		{
			phys3D.GetParticle(base).invMass = 0.f;
#ifdef FOUR_ATTACHED_POINTS
			phys3D.GetParticle(base + n - 1).invMass = 0.f;
#endif
		}
		x += inc;
	}
	phys3D.SetClothMesh(mesh);
	//phys3D.GetParticle(n - 1).SetVelocity(200 * Vector3(2.f, -1.f, 3.f), phys3D.GetTimeStep());
	phys3D.EndGroup();

#ifdef GRANULAR
	const float spacing = 2.2f * radius;
	phys3D.SetNumParticles(m);
	const float maxVel = 0.1f * phys3D.GetLengthScale();

	int side = (int)(pow(numParticles, 1.f / 3.f) + 1);
	int k = n * n;
	float y = side * spacing * 0.5f + 200.f + level;
	while (k < m)
	{
		float x = -side * spacing * 0.5f;
		for (int i = 0; i < side; i++)
		{
			float z = -side * spacing * 0.5f;
			for (int j = 0; j < side; j++)
			{
				colors3D[k].r = GetRandomReal01();
				colors3D[k].g = GetRandomReal01();
				colors3D[k].b = GetRandomReal01();
				//phys3D.GetParticle(k).invMass = 0.01f;
				phys3D.GetParticle(k).pos = Vector3(x, y, z);
				phys3D.GetParticle(k).SetVelocity(maxVel * Vector3(GetRandomReal11(), GetRandomReal11(), GetRandomReal11()), phys3D.GetTimeStep());
				k++;
				if (k >= m)
					return;
				z += spacing;
			}
			x += spacing;
		}
		y -= spacing;
	}
#endif
#endif
}

void ParticlesDemo::InitFem3D()
{
	mFEMDemo.Init();
}

void ParticlesDemo::InitRigid3D()
{
	mRigid3DDemo.Init();
}

// *********************** DRAWING *********************

void ParticlesDemo::DrawRigid3D()
{
	if (!pausePhysics)
		mRigid3DDemo.Update(0);
	mRigid3DDemo.Draw(graphics3D, mShowDebug, mDebugDrawFlags);
}

void ParticlesDemo::DrawFem3D()
{
#ifndef ANDROID_NDK
	// draw ground
	//graphics3D->SetColor(0, 0, 1);
	//graphics3D->DrawCube(Vector3(0,-100,0), Vector3(500, 10, 500), Matrix3());
  
	mFEMDemo.Draw(graphics3D);	
#endif
}

void ParticlesDemo::DrawCloth()
{
	mClothDemo->Draw(graphics3D, mShowDebug, mDebugDrawFlags);
}

void ParticlesDemo::OnDraw3D()
{
#ifdef USE_IMGUI
	if (ImGui::Button("Simulate"))
	{
		pausePhysics = !pausePhysics;
	}
	ImGui::SameLine();
	if (ImGui::Button("Reset"))
	{
		Reset();
	}
	ImGui::Checkbox("Step by step", &stepByStep);
#endif

	//PROFILE_SCOPE("OnDraw3D");
#if (RENDERER == OPENGL)
	if (!is3D)
		return;

	// camera control
	float dz = 0;
	float dx = 0;
	float speed = .25f;
	if (isKeyDown('W'))
		dz = speed;
	else if (isKeyDown('S'))
		dz = -speed;
	if (isKeyDown('A'))
		dx = -speed;
	else if (isKeyDown('D'))
		dx = speed;
	if (dx || dz)
		graphics3D->camera.Translate(dz, dx);

	// debug draw control
	if (isKeyPressed('C'))
	{
		mShowDebug = !mShowDebug;
	}

	if (isKeyPressed('1'))
	{
		if ((mDebugDrawFlags & DDF_CONTACTS) == 0)
			mDebugDrawFlags |= DDF_CONTACTS;
		else
			mDebugDrawFlags &= ~DDF_CONTACTS;
	}

	if (isKeyPressed('2'))
	{
		if ((mDebugDrawFlags & DDF_TRI_CONTACTS) == 0)
			mDebugDrawFlags |= DDF_TRI_CONTACTS;
		else
			mDebugDrawFlags &= ~DDF_TRI_CONTACTS;
	}

	if (isKeyPressed('3'))
	{
		if ((mDebugDrawFlags & DDF_SELF_TRIS) == 0)
			mDebugDrawFlags |= DDF_SELF_TRIS;
		else
			mDebugDrawFlags &= ~DDF_SELF_TRIS;
	}

	if (isKeyPressed('4'))
	{
		if ((mDebugDrawFlags & DDF_SELF_EDGES) == 0)
			mDebugDrawFlags |= DDF_SELF_EDGES;
		else
			mDebugDrawFlags &= ~DDF_SELF_EDGES;
	}

	if (isKeyPressed('0'))
	{
		if ((mDebugDrawFlags & DDF_PARTICLES) == 0)
			mDebugDrawFlags |= DDF_PARTICLES;
		else
			mDebugDrawFlags &= ~DDF_PARTICLES;
	}

	if (type == DEMO_FEM)
	{
		DrawFem3D();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, wireframe ? GL_LINE : GL_FILL);

#ifdef DEBUG_LIGHT_POS
	Vector3 lightPos = graphics3D->GetLightPos();
	if (!graphics3D->isShadowPass)
	{
		graphics3D->DrawSphere(lightPos, 10);
		graphics3D->SetColor(0, 0, 1);
		Vector3 n(graphics3D->viewShadow.m[2], graphics3D->viewShadow.m[6], graphics3D->viewShadow.m[10]);
		graphics3D->DrawLine(lightPos, lightPos + n * 20);

		graphics3D->SetColor(0, 1, 0);
		Vector3 v(graphics3D->viewShadow.m[1], graphics3D->viewShadow.m[5], graphics3D->viewShadow.m[9]);
		graphics3D->DrawLine(lightPos, lightPos + v * 20);

		graphics3D->SetColor(1, 0, 0);
		Vector3 u(graphics3D->viewShadow.m[0], graphics3D->viewShadow.m[4], graphics3D->viewShadow.m[8]);
		graphics3D->DrawLine(lightPos, lightPos + u * 20);
	}

	lightPos.x -= 0.1f;
	graphics3D->SetLightPos(lightPos);
#endif

	if (type == DEMO_RIGID)
	{
		DrawRigid3D();
		return;
	}

	if (type == DEMO_CLOTH)
	{
		if (!pausePhysics && !replayMode)
		{
			mClothDemo->Update(refFrameLen);
			pausePhysics = stepByStep; // for step by step
		}
		DrawCloth();
		return;
	}

	if (type == DEMO_UNIFIED)
	{
		//if (!pausePhysics)
		//{
		//	mUnified.Step();
		//	pausePhysics = stepByStep;
		//}
		if (noDraw)
			return;
		mUnified.Draw(graphics3D);
		return;
	}

	// step physics at beginning or end of frame?
	if (!pausePhysics && !replayMode)
	{
		if (drip)
		{
			nFrames++;
			if (nFrames > 13)
			{
				const float maxVel = 3.f;
				const float spacing = radius * 2.1f;
				int n = phys3D.GetNumParticles();

				int a = 4;
				int b = 4;
				if (n + a * b <= numParticles)
				{
					phys3D.SetNumParticles(n + a * b);
					for (int i = 0; i < a; i++)
					{
						float x = (-a * 0.5f + i) * spacing;
						for (int j = 0; j < b; j++)
						{
							int k = n + i * b + j;
							colors3D[k].r = GetRandomReal01();
							colors3D[k].g = GetRandomReal01();
							colors3D[k].b = GetRandomReal01();
							float z = (-b * 0.5f + j) * spacing;
							phys3D.GetParticle(k).pos = Vector3(x, 500, z);
							phys3D.GetParticle(k).SetVelocity(maxVel * Vector3(GetRandomReal11(), GetRandomReal11(), GetRandomReal11()), phys3D.GetTimeStep());
						}
					}

					nFrames = 0;
				}
			}
		}

		phys3D.Step();

		//int mod = currFrame % 2;
		//if (mod == 0)
		//{
		//	//Printf("collide\n");
		//	phys3D.IntegrateAndCollide(refFrameLen);
		//}
		//else
		//{
		//	//Printf("solve\n");
		//	phys3D.SolveConstraints(refFrameLen);
		//}
		//currFrame++;

		pausePhysics = stepByStep; // for step by step
	}

	//graphics3D->DrawAxes();
	//graphics3D->GetBodies().Draw(graphics3D);

	graphics3D->SetColor(0, 0.6f, 0);
	if (type == DEMO_PARTICLES || type == DEMO_HOURGLASS)
	{
		Vector3 offset = phys3D.GetMeshOffset();
		glTranslatef(offset.X(), offset.Y(), offset.Z());
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		//glDisable(GL_CULL_FACE);
		graphics3D->DrawMesh(phys3D.GetMesh()->vertices, phys3D.GetMesh()->normals, phys3D.GetMesh()->indices);
		glEnable(GL_CULL_FACE);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glTranslatef(-offset.X(), -offset.Y(), -offset.Z());
	}

	graphics3D->DrawWireCube(Vector3(0, 0, 0), Vector3(2 * PhysicsSystem3D::WALL_DIST, 2 * PhysicsSystem3D::WALL_DIST, 2 * PhysicsSystem3D::WALL_DIST));

	// particle color
	graphics3D->SetColor(0.83f, 0.67f, 0.f);
	// draw particles
	for (size_t i = 0; i < phys3D.GetNumParticles(); i++)
	{
		Vector3 pos = replayMode ? phys3D.GetPosition(i, currFrame) : phys3D.GetParticle(i).pos;
		if ((type == DEMO_CLOTH || type == DEMO_CLOTH_PARTICLES) && phys3D.InGroup(i, 0))
			phys3D.GetClothMesh()->vertices[i] = pos;
		if (!phys3D.InGroup(i, 0))
		{
			//graphics3D->SetColor(colors3D[i].r, colors3D[i].g, colors3D[i].b);
			graphics3D->DrawSphere(pos, radius);
		}
	}
	//TessellateCloth();
	phys3D.GetClothMesh()->ComputeNormals(true);

	// reindex mesh

	if (type == DEMO_CLOTH || type == DEMO_CLOTH_PARTICLES)
	{
		// hack: make 2 sided
		// TODO: double sided in shader
		//glDisable(GL_CULL_FACE);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		graphics3D->SetColor(0, 0, 1);
		graphics3D->DrawMesh(phys3D.GetClothMesh()->vertices, phys3D.GetClothMesh()->normals, phys3D.GetClothMesh()->indices);
		//graphics3D->DrawMesh(renderMesh.vertices, renderMesh.normals, renderMesh.indices);
		glCullFace(GL_FRONT);
		graphics3D->SetColor(0, 1, 1);
		graphics3D->DrawMesh(phys3D.GetClothMesh()->vertices, phys3D.GetClothMesh()->normals, phys3D.GetClothMesh()->indices);
		//graphics3D->DrawMesh(renderMesh.vertices, renderMesh.normals, renderMesh.indices);
		glCullFace(GL_BACK);
		//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		//glEnable(GL_CULL_FACE);
	}
	// draw normals
	graphics3D->SetColor(1, 1, 1);
	//for (size_t i = 0; i < renderMesh.vertices.size(); i++)
	//{
	//	graphics3D->DrawLine(renderMesh.vertices[i], renderMesh.vertices[i] + renderMesh.normals[i] * 10);
	//}
	//for (size_t i = 0; i < phys3D.GetNumParticles(); i++)
	//{
	//	graphics3D->DrawLine(phys3D.GetParticle(i).pos, phys3D.GetParticle(i).pos + phys3D.GetClothMesh()->normals[i] * 10);
	//	//graphics3D->DrawLine(phys3D.GetParticle(i).pos, phys3D.GetParticle(i).pos + phys3D.GetClothMesh()->tangents[i] * 10);
	//}
	//for (size_t j = 0; j < phys3D.GetClothMesh()->indices.size(); j += 3)
	//{
	//	const uint16 i1 = phys3D.GetClothMesh()->indices[j];
	//	const uint16 i2 = phys3D.GetClothMesh()->indices[j + 1];
	//	const uint16 i3 = phys3D.GetClothMesh()->indices[j + 2];
	//	Vector3 v1 = phys3D.GetParticle(i1).pos;
	//	Vector3 v2 = phys3D.GetParticle(i2).pos;
	//	Vector3 v3 = phys3D.GetParticle(i3).pos;
	//	Vector3 n = (v2 - v1).Cross(v3 - v1); // TODO: store normals
	//	n.Normalize();
	//	Vector3 p = 0.333f * (v1 + v2 + v3);
	//	graphics3D->DrawLine(p, p + n * 10);
	//}
	if (mShowDebug)
	{
		// draw constraints
		for (size_t i = 0; i < phys3D.GetNumConstraints(); i++)
		{
			const Constraint3D& link = phys3D.GetConstraint(i);
			//if (link.type == Constraint::CONTACT)
			//{
			//	graphics3D->SetColor(0, 0, 1);
			//	graphics3D->DrawLine(phys3D.GetParticle(link.i1).pos, phys3D.GetParticle(link.i1).pos + link.normal * 30);
			//}
#ifdef FIXED_PIPELINE
			if (link.type == Constraint::LINK)
			{
				if ((currFrame & 1) == 0)
					graphics3D->SetColor(1, 1, 1);
				else
					graphics3D->SetColor(0.8f, 0.3f, 0.2f);
				if (link.stiffness < 1)				
				{
					graphics3D->SetColor(0, 1, 0);
					continue;
				}
				graphics3D->DrawLine(phys3D.GetParticle(link.i1).pos, phys3D.GetParticle(link.i2).pos);
			}
#endif
			//else if (link.type == Constraint::TRIANGLE)
			//{
			//	graphics3D->SetColor(0, 1, 0);
			//	Vector3 p = link.w1 * phys3D.GetParticle(link.i1).pos + 
			//		link.w2 * phys3D.GetParticle(link.i2).pos +
			//		link.w3 * phys3D.GetParticle(link.i3).pos;
			//	graphics3D->DrawLine(p, p + link.normal * 10);
			//}
			if (link.type == Constraint::SELF_TRIANGLE)
			{
				if ((currFrame & 1) == 0)
					graphics3D->SetColor(1, 0, 0);
				else
					graphics3D->SetColor(0, 1, 0);
				const Vector3& v1 = phys3D.GetParticle(link.i1).pos;
				const Vector3& v2 = phys3D.GetParticle(link.i2).pos;
				const Vector3& v3 = phys3D.GetParticle(link.i3).pos;
				const Vector3& v4 = phys3D.GetParticle(link.i4).pos;
				//Vector3 p = link.w1 * v1 + link.w2 * v2 + link.w3 * v3;
				Vector3 n = link.normal;
				//graphics3D->DrawLine(p, p + n * 10);
				//graphics3D->SetColor(1, 0, 1);
				//graphics3D->DrawLine(p, v4);
				graphics3D->DrawLine(v4, v4 + n * 10.f);
				//graphics3D->SetColor(0.83f, 0.67f, 0.f);
				//graphics3D->DrawSphere(v4, 2.f);
				//pausePhysics = true;
			}
			if (link.type == Constraint::SELF_EDGES)
			{		
				graphics3D->SetColor(1, 0, 1);
				const Vector3& v1 = phys3D.GetParticle(link.i1).pos;
				const Vector3& v2 = phys3D.GetParticle(link.i2).pos;
				const Vector3& v3 = phys3D.GetParticle(link.i3).pos;
				const Vector3& v4 = phys3D.GetParticle(link.i4).pos;
				Vector3 p = v1 + link.w1 * (v2 - v1);
				Vector3 q = v3 + link.w2 * (v4 - v3);
				Vector3 n = link.normal;
				//graphics3D->DrawLine(p, q);
				graphics3D->DrawLine(p, p - n * 10.f);
				graphics3D->DrawLine(q, q + n * 10.f);
				//pausePhysics = true;
			}
		}
	}

	if (type == DEMO_CLOTH && (phys3D.GetCollisionFlags() & PhysicsSystem3D::COLL_SPHERE))
	{
		graphics3D->SetColor(1, 1, 1);
		graphics3D->DrawSphere(phys3D.GetSpherePos(), phys3D.GetSphereRadius());
	}

	if (replayMode && !pausePhysics && currFrame < phys3D.GetRecordingLength() - 1)
		currFrame++;
#endif
}

void ParticlesDemo::InitUnified()
{
	mUnified.Init();
	mUnified.SetMaxFrames(maxFrames);
	mUnified.SetAbcExport(bAbcExport);
	//mUnified.InitGranular(radius, numParticles); 
	//mUnified.InitCloth(30, 4, Vector3(0, 0, 25), false, true); 
	mUnified.InitFEM();
	//mUnified.InitStairs(); // FIXME
	//mUnified.InitCantilever();
	//mUnified.InitCoupling();
	//mUnified.InitRigids();
	//mUnified.InitJoints();
	//mUnified.InitStack();
	//mUnified.InitFriction();
	//mUnified.InitClothSphere();
	//mUnified.InitCable();
    //mUnified.InitKinematic();
}
