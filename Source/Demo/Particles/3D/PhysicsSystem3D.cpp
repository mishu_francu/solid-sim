#include <Demo/Particles/3D/PhysicsSystem3D.h>
#include <Engine/Profiler.h>
#include <Geometry/Collision3D.h>
#include <Graphics2D/Color.h>
#include <algorithm>

using namespace Geometry;

//#define CONTACT_REDUCTION
#define CCP

//template<> float Constraint3D::radius;

PhysicsSystem3D::PhysicsSystem3D() 
	: timeStep(0.016f)
	, sphRad(150)
	//, sphPos(0, -WALL_DIST + 50, 0)
	, numIterations(30)
	, numIterations2(0)
	, lengthScale(100)
	, proj(numIterations) 
	, prevNum(-1)
	, mesh(new Mesh)
	, cloth(new Mesh)
	, gravity(0, -9.8f * lengthScale, 0)
	, method(METHOD_PBD)
	, beta(0.3f)
	, mu(0.1f)
	, tolerance(0)
	, collFlags(0)
	, wallDist(WALL_DIST)
	, wallBottom(-WALL_DIST)
	, numSteps(1)
	, nFrames(0)
	, useOpenCL(false)
{
}

PhysicsSystem3D::~PhysicsSystem3D()
{
}

void PhysicsSystem3D::Init(float r)
{
	radius = r; 
	//Constraint3D::radius = r; 
	particles.clear();
	links.clear();
	constraints.clear();
	springs.clear();
	firstRun = true;

	broadphase.Init(Vector3(), 2 * wallDist, 2 * wallDist, 2 * wallDist, 2 * radius); // TODO: what is the best cell size?

	ClearGroups();
}

void PhysicsSystem3D::SetClothMesh(const Mesh& m) 
{
	if (!cloth)
		cloth.reset(new Mesh());
	*cloth.get() = m;
}

void PhysicsSystem3D::InitMeshGrid()
{
	Vector3 meshMin(mesh->vertices[0]);
	Vector3 meshMax(mesh->vertices[0]);
	// compute mesh aabb
	for (size_t i = 1; i < mesh->vertices.size(); i++)
	{
		meshMin = vmin(mesh->vertices[i], meshMin);
		meshMax = vmax(mesh->vertices[i], meshMax);
	}
	Vector3 center = 0.5f * (meshMin + meshMax);
	Vector3 extent = meshMax - meshMin;
	meshGrid.Init(center, extent.X(), extent.Y(), extent.Z(), 40);

	// sample mesh points
	float tol = radius + tolerance;
	Vector3 rad(tol);
	std::vector<Aabb3> handles(mesh->vertices.size()); // TODO: keep allocated
	for (size_t i = 0; i < mesh->vertices.size(); i++)
	{
		handles[i].min = mesh->vertices[i] - rad;
		handles[i].max = mesh->vertices[i] + rad;
	}
	meshGrid.Sample(handles);
}

void PhysicsSystem3D::Step()
{
	PROFILE_SCOPE("Physics3D");	
	float h = timeStep / numSteps;
	for (int i = 0; i < numSteps; i++)
	{
		if (method == METHOD_PBD)
			MicroStep(h);
		else if (method == METHOD_SEQ_IMP || method == METHOD_SEQ_IMP_POST)
			MicroStepSI(h);
		else if (method == METHOD_SEQ_POS)
			MicroStepSP(h);
		else if (method == METHOD_IVP)
			MicroStepIVP(h);
	}
	nFrames++;
}

#define NEW_PBD_SOLVER

void PhysicsSystem3D::MicroStep(float h)
{
	PROFILE_SCOPE("MicroStep");

	IntegrateAndCollide(h);
	SolveConstraints(h);
	
#ifndef NEW_PBD_SOLVER
	// update velocities for PBD (this is no longer needed for the new method which updates velocities on the go)
	const float invH = 1.f / h;
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		// TODO: centered difference
		particles[i].velocity = invH * (particles[i].pos - particles[i].prevPos);
	}
#endif

	if (numIterations2 > 0)
	{
		constraints.clear();
		float oldTol = tolerance;
		tolerance = 0.f;
		Collide(); // TODO: surviving contacts
		tolerance = oldTol;
		ProjectVelocities(h, numIterations2);
		constraints.clear();
	}

	if (collFlags & COLL_SELF)
	{
		SelfCollisionDynamic();
		SolveConstraints(h);
	}
}

void PhysicsSystem3D::MicroStepIVP(float h)
{
	PROFILE_SCOPE("MicroStep");

	IntegrateAndCollide(h);
	SolveConstraintsIVP(h);

	if (numIterations2 > 0)
	{
		SolveConstraints(h, numIterations2);
	}
}

void PhysicsSystem3D::IntegrateAndCollide(float h)
{
	PROFILE_SCOPE("MicroStep");

	IntegrateSE(h);

	constraints.clear();
	Collide();
	constraints.insert(constraints.end(), links.begin(), links.end());
}

void PhysicsSystem3D::SolveConstraints(float h, int iterations)
{
	if (iterations < 0)
		iterations = numIterations;
	HandleSprings();
	// projection
#ifdef ENABLE_CL
	if (useOpenCL)
	{
		PROFILE_SCOPE("OpenCL");
		projCL.CopyBuffers(particles, constraints);
		projCL.SetNumIterations(numIterations);
		projCL.ProjectPositions();
		projCL.ReadBuffers(particles);
	}
	else
#endif
	{
		if (solver == PhysicsSystem::GAUSS_SEIDEL)
		{
#ifdef NEW_PBD_SOLVER
            ProjectPostitionsGS(h, iterations);
#else
			RelaxationProjector<Particle3D, Constraint3D, Vector3> relax(iterations, 1.0f);
			relax.NewtonSOR(constraints, particles);
			//relax.Jacobi(constraints, particles, 0.25f);
#endif
		}
		else
		{
			proj.SetNumIterations(iterations);
			//proj.NewtonMinimumResidual(constraints, particles, 0.25f); //1.f / rho);
			//proj.NonlinearConjugateResidual(constraints, particles, 0.25f); //1.f / rho);
			proj.ConjugateJacobi(constraints, particles, 0.8f);
		}
	}
	//PrintInfo();
}

void PhysicsSystem3D::SolveConstraintsIVP(float h, int iterations)
{
	if (iterations < 0)
		iterations = numIterations;
	// projection
	if (solver == PhysicsSystem::GAUSS_SEIDEL)
	{
		ProjectVelocitiesImplicitGS(h, iterations);
	}
	else
		ASSERT(false); // solver not supported
}

inline void ProjectContactUnary_Positions(Particle3D* p1, const Constraint3D& constraint, float h)
{
    Vector3 delta = p1->pos;
    delta.Subtract(constraint.point);
    float len0 = constraint.len;
    float len = delta.Dot(constraint.normal);
    if (len > len0)
        return; // quit if constraint not active
    // project position
    delta = constraint.normal;
    const float depth = len - len0;
    delta.Scale(depth);
    p1->pos.Subtract(delta);

    // update velocity too
    delta.Scale(1.f / h);
    p1->velocity.Subtract(delta);
}

inline void ProjectContactUnary_Velocities(Particle3D* p1, const Constraint3D& constraint, float h, float beta)
{
    Vector3 delta = p1->pos;
    delta.Subtract(constraint.point);
    float len0 = constraint.len;
    float len = delta.Dot(constraint.normal);
    const float depth = len - len0;
    if (depth < 0) // if the contact is active (at position level)
    {
        delta = constraint.normal;
        float vrel = dot(delta, p1->velocity);
        if (vrel < 0) // if the particle is approaching (active velocity constraint)
        {
            // solve the velocity constraint (zero relative velocity)
            #ifndef GGL
                vrel += beta * depth / h;
            #endif
            delta.Scale(vrel);
            p1->velocity.Subtract(delta);

            // update position too
            delta.Scale(h);
            p1->pos.Subtract(delta);
        }

        #ifdef GGL
            // project position too, but not update the velocity (non-physical force a la GGL)
            delta = constraint.normal;
            delta.Scale(depth);
            p1->pos.Subtract(delta);
        #endif
    }
}

inline void ProjectContactBinary_Positions(Particle3D* p1, Particle3D* p2, const Constraint3D& constraint, float h)
{
    Vector3 delta = p1->pos;
    delta.Subtract(p2->pos);
    const float len0 = constraint.len;
    float len = delta.Length();
    delta.Scale(1 / len);
    const float depth = len - len0;
    if (len > len0)
        return; // quit if constraint not active
    // project positions
    delta.Scale(depth / (p1->invMass + p2->invMass));
    p1->pos.Subtract(delta * p1->invMass);
    p2->pos.Add(delta * p2->invMass);

	// update velocities too
	delta.Scale(1.f / h);
	p1->velocity.Subtract(delta * p1->invMass);
	p2->velocity.Add(delta * p2->invMass);
}

inline void ProjectContactBinary_Velocities(Particle3D* p1, Particle3D* p2, const Constraint3D& constraint, float h, float beta)
{
    Vector3 delta = p1->pos;
    delta.Subtract(p2->pos);
    const float len0 = constraint.len;
    float len = delta.Length();
    delta.Scale(1 / len);
    const float depth = len - len0;
    Vector3 dir = delta;

    if (depth < 0) // if the constraint is active (at position level)
    {
        float vrel = dot(delta, p1->velocity - p2->velocity);
        if (vrel < 0) // if the particle is approaching (active velocity constraint)
        {
            // solve the velocity constraint (zero relative velocity)
            float error = vrel;
            // Optionally we could add a stabilization term
            #ifndef GGL
                error += beta * depth / h;
            #endif
            dir.Scale(error / (p1->invMass + p2->invMass));
            p1->velocity.Subtract(dir * p1->invMass);
            p2->velocity.Add(dir * p2->invMass);

            // update the positions too
            dir.Scale(h);
            p1->pos.Subtract(dir * p1->invMass);
            p2->pos.Add(dir * p2->invMass);
        }
        
        #ifdef GGL
            // project positions too, but not update the velocities (non-physical force a la GGL)
            dir = delta;
            dir.Scale(depth / (p1->invMass + p2->invMass));
            p1->pos.Subtract(dir * p1->invMass);
            p2->pos.Add(dir * p2->invMass);
        #endif
    }
}

inline void ProjectLink_Positions(Particle3D* p1, Particle3D* p2, const Constraint3D& constraint, float h)
{
    // project positions
    Vector3 delta = p1->pos;
    delta.Subtract(p2->pos);
    const float len0 = constraint.len;
    float len = delta.Length();
    delta.Scale(1 / len);
    const float depth = len - len0;
    delta.Scale(depth / (p1->invMass + p2->invMass));
    p1->pos.Subtract(delta * p1->invMass);
    p2->pos.Add(delta * p2->invMass);

    // update velocities too
    delta.Scale(1.f / h);
    p1->velocity.Subtract(delta * p1->invMass);
    p2->velocity.Add(delta * p2->invMass);
}

inline void ProjectLink_Velocities(Particle3D* p1, Particle3D* p2, const Constraint3D& constraint, float h, float beta)
{
	Vector3 delta = p1->pos;
	delta.Subtract(p2->pos);
	const float len0 = constraint.len;
	float len = delta.Length();
	delta.Scale(1 / len);
	const float depth = len - len0;
	Vector3 dir = delta;

	float vrel = dot(delta, p1->velocity - p2->velocity);
	{
		// solve the velocity constraint (zero relative velocity)
		float error = vrel;
		// Optionally we could add a stabilization term
#ifndef GGL
		error += beta * depth / h;
#endif
		dir.Scale(error / (p1->invMass + p2->invMass));
		p1->velocity.Subtract(dir * p1->invMass);
		p2->velocity.Add(dir * p2->invMass);

		// update the positions too
		dir.Scale(h);
		p1->pos.Subtract(dir * p1->invMass);
		p2->pos.Add(dir * p2->invMass);
	}

#ifdef GGL
	// project positions too, but not update the velocities (non-physical force a la GGL)
	dir = delta;
	dir.Scale(depth / (p1->invMass + p2->invMass));
	p1->pos.Subtract(dir * p1->invMass);
	p2->pos.Add(dir * p2->invMass);
#endif
}

void PhysicsSystem3D::ProjectPostitionsGS(float h, int maxIter)
{
    PROFILE_SCOPE("ProjectPostitionsGS");

    static Particle3D ground; ground.invMass = 0;
    const float omega = 1.f;
    const int n = maxIter == 0 ? numIterations : maxIter;
    for (int k = 0; k < n; ++k)
    {
        for (size_t i = 0; i < constraints.size(); i++)
        {
            Particle3D* p1 = &particles[constraints[i].i1];
            if (constraints[i].type == Constraint3D::CONTACT)
            {
	            ProjectContactUnary_Positions(p1, constraints[i], h);
            }
            else if (constraints[i].type == Constraint3D::COLL_PAIR)
            {
                ProjectContactBinary_Positions(p1, &particles[constraints[i].i2], constraints[i], h);
            }
            else
            {
				ProjectLink_Positions(p1, &particles[constraints[i].i2], constraints[i], h);
            }
        }
    }
}

void PhysicsSystem3D::ProjectVelocitiesImplicitGS(float h, int maxIter)
{
	PROFILE_SCOPE("ProjectVelocitiesImplicitGS");

	static Particle3D ground; ground.invMass = 0;
	const float omega = 1.f;
	const int n = maxIter == 0 ? numIterations : maxIter;
	for (int k = 0; k < n; ++k)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Particle3D* p1 = &particles[constraints[i].i1];
			if (constraints[i].type == Constraint3D::CONTACT)
			{
				ProjectContactUnary_Velocities(p1, constraints[i], h, beta);
			}
			else if (constraints[i].type == Constraint3D::COLL_PAIR)
			{
				ProjectContactBinary_Velocities(p1, &particles[constraints[i].i2], constraints[i], h, beta);
			}
			else
			{
				ProjectLink_Velocities(p1, &particles[constraints[i].i2], constraints[i], h, beta);
			}
		}
	}
}

void PhysicsSystem3D::PrintInfo()
{
	// compute and print constraint error
	float err = 0;
	float vel = 0;
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint3D& pair = constraints[i];
		Particle3D* p1 = &particles[pair.i1];
		if (pair.type == Constraint3D::COLL_PAIR)
		{			
			Particle3D* p2 = &particles[pair.i2];
			Vector3 n = p1->pos - p2->pos;
			float len = n.Length();
			err += max(0.f, pair.len - len);
			n.Normalize();
			vel += max(0.f, n.Dot(p2->velocity - p1->velocity));
		}
		else if (pair.type == Constraint3D::LINK)
		{
			Particle3D* p2 = &particles[pair.i2];
			Vector3 n = p1->pos - p2->pos;
			float len = n.Length();
			err += len - pair.len;
			n.Normalize();
			vel += n.Dot(p2->velocity - p1->velocity);
		}
		else if (pair.type == Constraint3D::CONTACT)
		{
			err += max(0.f, pair.len - pair.normal.Dot(p1->pos - pair.point));
			vel += max(0.f, pair.normal.Dot(p1->velocity));
		}
	}
	
	// compute and print kinetic energy
	float kin = 0;
	float pot = 0;
	for (size_t i = 0; i < particles.size(); i++)
	{
		kin += particles[i].velocity.LengthSquared() / particles[i].invMass;
		pot += Engine::getInstance()->getScreenHeight() - particles[i].pos.Y();
	}
	//Printf("%f,%f,%f\n", err, vel, kin);
	Printf("%f\n", err);

	//nFrames++;
	if (nFrames > 2000)
		Engine::getInstance()->Quit();
}

void PhysicsSystem3D::HandleSprings()
{
	// hack - explicit spring forces
	for (size_t i = 0; i < springs.size(); i++)
	{
		Particle3D* p1 = &particles[springs[i].i1];
		Particle3D* p2 = &particles[springs[i].i2];
		Vector3 delta = p1->pos - p2->pos;
		float len0 = springs[i].len;
		float len = delta.Length();
		delta.Scale(1 / len);

		springs[i].disp = springs[i].stiffness * (len - len0) * delta;
	}
	for (size_t i = 0; i < springs.size(); i++)
	{
		Vector3 delta = springs[i].disp;
		particles[springs[i].i1].pos.Subtract(delta * particles[springs[i].i1].invMass);
		particles[springs[i].i2].pos.Add(delta * particles[springs[i].i2].invMass);
	}
	// mouse spring
	if (mouseSpring.type != Constraint3D::DUMMY)
	{
		Vector3 delta = particles[mouseSpring.i1].pos - mouseSpring.point;
		float len = delta.Length();
		delta.Scale(1 / len);
		particles[mouseSpring.i1].pos -= mouseSpring.stiffness * (len - mouseSpring.len) * delta;
	}
}

// integrate Symplectic Euler
void PhysicsSystem3D::IntegrateSE(float h)
{
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += h * gravity;
		particles[i].prevPos = particles[i].pos;
		particles[i].pos += h * particles[i].velocity;
	}
}

void PhysicsSystem3D::PostFriction()
{
	const Vector3 push(0, 1, 0);
	const float mu1 = mu / timeStep; // TODO: replace with h
	if (mu1 > 0)
	{
		for (size_t i = 0; i < constraints.size(); i++) // TODO: contacts only list
		{
			// TODO: self edges
			if (constraints[i].type == Constraint3D::CONTACT || constraints[i].type == Constraint3D::SELF_TRIANGLE)
			{
				int idx = constraints[i].type == Constraint3D::SELF_TRIANGLE ? constraints[i].i4 : constraints[i].i1;
				Vector3 v = particles[idx].velocity;
				const Vector3& n = constraints[i].normal;
				Vector3 vt = v - v.Dot(n) * n;
				const float friction = mu1 * push.Dot(n);
				if (friction <= 0)
					continue;
				if (friction > vt.Length()) // TODO: square
				{
					particles[idx].velocity.SetZero(); // zero out the velocity
				}
				else
				{
					vt.Normalize();
					particles[idx].velocity -= friction * vt;
				}
			}
		}
	}
}

void PhysicsSystem3D::UpdateAabbs()
{
	float tol = radius + tolerance;
	Vector3 rad(tol);
	for (size_t i = 0; i < GetNumParticles(); i++)
		particles[i].GetAabb(aabbs[i].min, aabbs[i].max, rad);
}

void PhysicsSystem3D::Collide()
{
	PROFILE_SCOPE("Collide");

	//constraints.clear();

	if (collFlags & COLL_SPHERE)
		CollideWithSphere();

	UpdateAabbs();
	broadphase.Sample(aabbs);

	if (collFlags & COLL_PARTICLES)
		CollideParticles();
	
	if (collFlags & COLL_MESH)
		CollideWithMesh(mesh.get());
	//CollideWithMesh(cloth.get());

	if (collFlags & COLL_SELF)
		SelfCollisionStatic();

	if (collFlags & COLL_WALLS)
		WallCollisions();

	if (collFlags & COLL_PLANE)
		CollideWithPlane();

	contacts = constraints;
	constraints.insert(constraints.end(), links.begin(), links.end());

	// hack for no collisions
	//if (constraints.size() == 0)
	//	constraints.resize(links.size());
	//memcpy(&constraints[0], &links[0], links.size() * sizeof(Constraint3D)); // TODO: further optimize this memcpy; reduce constraint struct size
}

void PhysicsSystem3D::CollideParticles()
{
	// collisions between particles
	std::vector<Constraint3D> candidates;
	broadphase.Update(aabbs, candidates);
	const float radTol = radius + tolerance;
	const float radSqr = 4 * radTol * radTol;
	for (size_t i = 0; i < candidates.size(); i++)
	{
		if (!groups.empty() && (groups[0].Contains(candidates[i].i1) || groups[0].Contains(candidates[i].i2)))
			continue; // hack
		if ((particles[candidates[i].i1].pos - particles[candidates[i].i2].pos).LengthSquared() <= radSqr)
			constraints.push_back(candidates[i]);
	}
}

void PhysicsSystem3D::CollideWithPlane()
{
	float angle = RADIAN(15);
	Vector3 normal(sin(angle), cos(angle), 0);
	//normal.Normalize();
	Vector3 point;

	const float radTol = radius + tolerance;
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		if (normal.Dot(particles[i].pos - point) < radTol)
		{
			constraints.push_back(Constraint3D((ParticleIdx)i, point, normal, radius));
		}
	}
}

void PhysicsSystem3D::CollideWithSphere()
{
	const float radTol = radius + tolerance;
	Vector3 n, p;
	float dSqr;
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass != 0 && IntersectSphereSphere(sphPos, sphRad, particles[i].pos, radTol, n, p, dSqr))
			constraints.push_back(Constraint3D((ParticleIdx)i, p, n, radius));
		//if (particles[i].invMass != 0 && particles[i].pos.Y() - radTol <= 0)
		//	constraints.push_back(Constraint3D((ParticleIdx)i, Vector3(), Vector3(0, 1, 0)));
	}
	// triangle collisions
	// TODO: fix them for MR/CR
	//float dist;
	//BarycentricCoords bar;
	//const float tol = 0.05f;
	//for (size_t i = 0; i < cloth.indices.size(); i += 3)
	//{
	//	int i1 = cloth.indices[i];
	//	int i2 = cloth.indices[i + 1];
	//	int i3 = cloth.indices[i + 2];
	//	const Vector3& v1 = particles[i1].pos;
	//	const Vector3& v2 = particles[i2].pos;
	//	const Vector3& v3 = particles[i3].pos;
	//	if (IntersectSphereTriangle(sphPos, sphRad + radius * 3.f, v1, v2, v3, n, p, dist, bar))
	//	{
	//		//if (fabs(bar.u - 1.f) < tol || fabs(bar.v - 1.f) < tol || fabs(bar.w - 1.f) < tol)
	//		//{
	//		//	// if one barycentric coordinate is close to 1 then we have a point collision
	//		//	continue;
	//		//}
	//		n.Flip();
	//		Constraint3D contact;
	//		contact.type = Constraint3D::TRIANGLE;
	//		contact.normal = n;
	//		contact.point = sphPos + n * sphRad; //p - n * (dist - sphRad);
	//		contact.i1 = i1;
	//		contact.i2 = i2;
	//		contact.i3 = i3;
	//		contact.w1 = bar.u;
	//		contact.w2 = bar.v;
	//		contact.w3 = bar.w;
	//		contact.len = radius;
	//		constraints.push_back(contact);
	//	}
	//}
}

void PhysicsSystem3D::WallCollisions()
{
	// TODO: use the grid
	const float r = radius + tolerance;
	ASSERT(particles.size() < 0xffff); // TODO: MAX_UINT16	
	for (size_t i = 0; i < particles.size(); i++)
	{
		const Particle3D& particle = particles[i];
		if (particle.pos.Y() - r <= wallBottom)
			AddContact((ParticleIdx)i, Vector3(particle.pos.X(), wallBottom, particle.pos.Z()), Vector3(0, 1, 0));
		//if (particle.pos.Y() + r >= WALL_TOP)
		//	AddContact((ParticleIdx)i, Vector3(particle.pos.X(), WALL_TOP, particle.pos.Z()), Vector3(0, -1, 0));
		if (particle.pos.X() - r <= WALL_LEFT)
			AddContact((ParticleIdx)i, Vector3(WALL_LEFT, particle.pos.Y(), particle.pos.Z()), Vector3(1, 0, 0));
		if (particle.pos.X() + r >= WALL_RIGHT)
			AddContact((ParticleIdx)i, Vector3(WALL_RIGHT, particle.pos.Y(), particle.pos.Z()), Vector3(-1, 0, 0));
		if (particle.pos.Z() - r <= WALL_BACK)
			AddContact((ParticleIdx)i, Vector3(particle.pos.X(), particle.pos.Y(), WALL_BACK), Vector3(0, 0, 1));
		if (particle.pos.Z() + r >= WALL_FRONT)
			AddContact((ParticleIdx)i, Vector3(particle.pos.X(), particle.pos.Y(), WALL_FRONT), Vector3(0, 0, -1));
	}
}

// obsolete - for testing purposes only
void PhysicsSystem3D::PairwiseBroadphase()
{
	const float r = radius + tolerance;
	const float diamSqr = 4 * r * r;
	for (size_t i = 0; i < particles.size() - 1; i++)
	{
		for (size_t j = i + 1; j < particles.size(); j++)
		{
			Vector3 delta = particles[i].pos - particles[j].pos;
			if (delta.LengthSquared() < diamSqr)
				constraints.push_back(Constraint3D(Constraint3D::COLL_PAIR, (ParticleIdx)i, (ParticleIdx)j, 2 * radius)); // TODO: assert
		}
	}
}

// TODO: move to MeshCollisions
void PhysicsSystem3D::CollideWithMesh(const Mesh* mesh)
{
	PROFILE_SCOPE("Mesh collide");

	const float radTol = radius + tolerance;
	const Vector3 extrude(radTol);
	Vector3 n, p;
	float d;
	std::vector<int> ids;
	std::vector<ParticleCollision> infos(particles.size()); // TODO: skip allocation
	// go through all triangles
	for (size_t j = 0; j < mesh->indices.size(); j += 3)
	{
		const uint16 i1 = mesh->indices[j];
		const uint16 i2 = mesh->indices[j + 1];
		const uint16 i3 = mesh->indices[j + 2];
		Vector3 v1 = mesh->vertices[i1] + meshOffset;
		Vector3 v2 = mesh->vertices[i2] + meshOffset;
		Vector3 v3 = mesh->vertices[i3] + meshOffset;

		// build AABB
		Vector3 minV = vmin(vmin(v1, v2), v3) - extrude;
		Vector3 maxV = vmax(vmax(v1, v2), v3) + extrude;

		broadphase.TestObject(Aabb3(minV, maxV), ids);
		
		for (size_t ii = 0; ii < ids.size(); ii++)
		{
			size_t i = ids[ii];
			//if (groups[0].Contains(i))
				//continue; // hack
			if (particles[i].invMass == 0)
				continue;
			Vector3 v = particles[i].pos; // TODO: transform

			if (!PointInAabb3D(minV, maxV, v))
				continue;

			Vector3 n = (v2 - v1).Cross(v3 - v1); // TODO: store normals
			n.Normalize();
			// if coming from inside skip
			if (n.Dot(particles[i].prevPos - v1) < 0)
				continue;
			BarycentricCoords coords;
			Vector3 dir = particles[i].pos - particles[i].prevPos;
			bool intersect;			
			intersect = IntersectSphereTriangle(v, radTol, v1, v2, v3, n, p, d, coords);
			if (!intersect)
				intersect = IntersectSweptSphereTriangle(particles[i].prevPos, radTol, dir, v1, v2, v3, p, n, d, coords); // TODO: use velocity instead of prevPos
			if (intersect)
			{
#ifdef CONTACT_REDUCTION
				// contact reduction
				if (infos[i].active)
				{
					int idx = infos[i].coll;
					if (infos[i].d > d)
					{
						constraints[idx].point = p;
						constraints[idx].normal = n;
					}					
					//constraints[idx].normal += n;
					//constraints[idx].normal.Normalize();
					continue;
				}
#endif
#ifdef USE_OPENCL
				// FIXME: for GS
				if (infos[i].active >= COLL_BATCHES)
					continue;
#endif
				Constraint3D c((ParticleIdx)i, p, n, radius);
				c.batch = infos[i].active;
				constraints.push_back(c);

				infos[i].active++;
				infos[i].d = d;
				infos[i].p = p;
				infos[i].n = n;
				infos[i].tri = j; // x3
				infos[i].coll = constraints.size() - 1;
			}
		}
	}

	// TODO: grid vs grid, triangles in grid
	
	// collide mesh points against cloth triangles
#ifdef CLOTH_TRI
	for (size_t j = 0; j < cloth->indices.size(); j += 3)
	{
		const uint16 i1 = cloth->indices[j];
		const uint16 i2 = cloth->indices[j + 1];
		const uint16 i3 = cloth->indices[j + 2];
		Vector3 v1 = cloth->vertices[i1];
		Vector3 v2 = cloth->vertices[i2];
		Vector3 v3 = cloth->vertices[i3];

		// build AABB
		Vector3 minV = vmin(vmin(v1, v2), v3) - extrude;
		Vector3 maxV = vmax(vmax(v1, v2), v3) + extrude;

		meshGrid.TestObject(Aabb3(minV, maxV), ids);
		
		for (size_t ii = 0; ii < ids.size(); ii++)
		{
			int i = ids[ii];
			Vector3 v = mesh->vertices[i]; // TODO: transform

			if (!PointInAabb3D(minV, maxV, v))
				continue;

			//Vector3 n = (v2 - v1).Cross(v3 - v1); // TODO: store normals
			//n.Normalize();
			//// if coming from inside skip
			//if (n.Dot(particles[i].prevPos - v1) < 0)
			//	continue;
			BarycentricCoords bar;
			//Vector3 dir = particles[i].pos - particles[i].prevPos;
			bool intersect;			
			intersect = IntersectSphereTriangle(v, radTol, v1, v2, v3, n, p, d, bar);
			//if (!intersect)
			//	intersect = IntersectSweptSphereTriangle(particles[i].prevPos, radTol, dir, v1, v2, v3, p, n, d);
			if (intersect)
			{
				n.Flip();
				Constraint3D contact;
				contact.type = Constraint3D::TRIANGLE;
				contact.normal = n;
				contact.point = v;
				contact.i1 = i1;
				contact.i2 = i2;
				contact.i3 = i3;
				contact.w1 = bar.u;
				contact.w2 = bar.v;
				contact.w3 = bar.w;
				contact.len = radius;
				constraints.push_back(contact);
			}
		}
	}
#endif
}

void PhysicsSystem3D::AddMouseSpring(int i, const Vector3& p)
{
	mouseSpring.type = Constraint3D::MOUSE_SPRING;
	mouseSpring.len = float((particles[i].pos - p).Length());
	mouseSpring.stiffness = 20.f;
	mouseSpring.i1 = i;
	mouseSpring.point = p;
}

void PhysicsSystem3D::MicroStepSI(float h)
{
	constraints.clear();
	Collide();

	// integrate velocities
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		particles[i].velocity += h * gravity;
        particles[i].prevPos = particles[i].pos;
		//particles[i].pos += h * particles[i].velocity;
	}

#ifdef USE_OPENCL
	size_t n = GetNumParticles();
	bool firstRun = prevNum != n;
	prevNum = n;

	if (firstRun)
	{
		velProjCL.PrepareBuffers(particles, links);
	}
	velProjCL.CopyBuffers(particles, constraints, beta / h);
	velProjCL.SetNumIterations(numIterations);
	velProjCL.ProjectVelocities(n);
	velProjCL.ReadBuffers(particles);
#else
	ProjectVelocities(h, numIterations);
#endif

	// update positions
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		//particles[i].prevPos = particles[i].pos;
		particles[i].pos = particles[i].prevPos + h * particles[i].velocity;
	}

	// post-stabilization: project positions
	if (numIterations2 > 0 && method == METHOD_SEQ_IMP_POST)
	{
		//SolveConstraints(h, numIterations2);
		SolvePositionsExplicitGS(numIterations2);
	}

	//PrintInfo();
}

// TODO: templated projector for 2D and 3D?
void PhysicsSystem3D::ProjectVelocities(float h, int iterations)
{
	PROFILE_SCOPE("ProjectVelocities");
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint3D& pair = constraints[i];
		const Particle3D& p1 = particles[pair.i1];
		if (pair.type == Constraint3D::COLL_PAIR || pair.type == Constraint3D::LINK)
		{			
			const Particle3D& p2 = particles[pair.i2];
			Vector3 n = p1.pos - p2.pos;
			float len = n.Length();
			pair.depth = pair.len - len;
            if (pair.type == Constraint3D::COLL_PAIR && len > pair.len)
                pair.depth = 0;
			n.Normalize();
			pair.normal = n;
		}
		else
		{
            float len = pair.normal.Dot(p1.pos - pair.point);
			pair.depth = pair.len - len;
            if (len > pair.len)
                pair.depth = 0;
        }

		pair.t1 = pair.normal.Perpendicular();
		pair.t2 = pair.normal.Cross(pair.t1);
		pair.t2.Normalize();

		pair.lambda = 0.f;
		pair.lambdaF1 = 0.f;
		pair.lambdaF2 = 0.f;

		pair.dLambda = 0.f;
		pair.dLambdaF1 = 0.f;
		pair.dLambdaF2 = 0.f;
	}

	if (solver == PhysicsSystem::GAUSS_SEIDEL)
	{
		SolveVelocitiesGS(h, iterations);
	}
	else
	{
		SolveVelocitiesJacobi(h, 0.3f);
	}
}

void PhysicsSystem3D::SolvePositionsExplicitGS(int iterations)
{
	PROFILE_SCOPE("GS pos explicit");
	Particle3D dummy; dummy.invMass = 0.f;
	for (int k = 0; k < iterations; ++k)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint3D& pair = constraints[i];
			Particle3D* p1 = &particles[pair.i1];
			Particle3D* p2 = &dummy;
			if (pair.type == Constraint3D::COLL_PAIR || pair.type == Constraint3D::LINK)
			{
				p2 = &particles[pair.i2];
				Vector3 delta = p1->pos - p2->pos;
				float len = delta.Length(); //fabs(pair.normal.Dot(delta));
				pair.depth = pair.len - len;
			}
			else if (pair.type == Constraint3D::CONTACT)
			{
				pair.depth = pair.len - pair.normal.Dot(p1->pos - pair.point);
			}

			float dLambda = pair.depth / (p1->invMass + p2->invMass);

			if (pair.type != Constraint3D::LINK)
			{
				if (pair.depth < 0) continue;
			}

			Vector3 disp = dLambda * pair.normal;
			p1->pos += p1->invMass * disp;
			p2->pos -= p2->invMass * disp;
		}
	}
}

void PhysicsSystem3D::SolveVelocitiesGS(float h, int iterations)
{
	PROFILE_SCOPE("GS vel");
	Particle3D dummy; dummy.invMass = 0.f;
	float beta1 = method == METHOD_SEQ_IMP ? beta : 0; // disable Baumgarte stabilization when using post-stabilization
	for (int k = 0; k < iterations; ++k)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint3D& pair = constraints[i];
			Particle3D* p1 = &particles[pair.i1];
			Particle3D* p2 = &dummy;
			Vector3 v12;
			if (pair.type == Constraint3D::COLL_PAIR || pair.type == Constraint3D::LINK)
			{			
				p2 = &particles[pair.i2];
				v12 = p2->velocity - p1->velocity;
			}
			else if (pair.type == Constraint3D::CONTACT)
			{
				v12 = p1->velocity; v12.Flip();
			}

			float vnrel = pair.normal.Dot(v12);
			float dLambda = (vnrel + beta1 * pair.depth / h) / (p1->invMass + p2->invMass);

			if (pair.type != Constraint3D::LINK)
			{
				float lambda0 = pair.lambda;
				pair.lambda = lambda0 + dLambda;
				if (pair.lambda < 0.f)
					pair.lambda = 0.f;
				dLambda = pair.lambda - lambda0;
			}

			Vector3 disp = dLambda * pair.normal;
			p1->velocity += p1->invMass * disp;
			p2->velocity -= p2->invMass * disp;
		
            // TODO: only apply friction when depth > 0 (?)
			if (pair.type != Constraint3D::LINK && mu > 0.f)
			{
				float limit = mu * pair.lambda;
#ifdef CCP
				Vector3 vt = v12 - vnrel * pair.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel / (p1->invMass + p2->invMass);
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = lambda0 + dLambda;
					if (pair.lambdaF1 >= limit)
						pair.lambdaF1 = limit;
					dLambda = pair.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 p = dLambda * vt;
					p1->velocity += p1->invMass * p;
					p2->velocity -= p2->invMass * p;
				}
#else
				float vtrel = v12.Dot(pair.t1);
				if (fabs(vtrel) >= 0.0001f)
				{
					float dLambdaF = vtrel / (p1->invMass + p2->invMass);
					float lambdaF0 = pair.lambdaF1;
					pair.lambdaF1 = lambdaF0 + dLambdaF;
					pair.lambdaF1 = clamp(pair.lambdaF1, -limit, limit);
					dLambdaF = pair.lambdaF1 - lambdaF0;
			
					Vector3 vt = dLambdaF * pair.t1;
					p1->velocity.Add(vt * p1->invMass);
					p2->velocity.Subtract(vt * p2->invMass);
				}

				vtrel = v12.Dot(pair.t2);
				if (fabs(vtrel) >= 0.0001f)
				{
					float dLambdaF = vtrel / (p1->invMass + p2->invMass);
					float lambdaF0 = pair.lambdaF2;
					pair.lambdaF2 = lambdaF0 + dLambdaF;
					pair.lambdaF2 = clamp(pair.lambdaF2, -limit, limit);
					dLambdaF = pair.lambdaF2 - lambdaF0;
			
					Vector3 vt = dLambdaF * pair.t2;
					p1->velocity.Add(vt * p1->invMass);
					p2->velocity.Subtract(vt * p2->invMass);
				}
#endif
			}
		}
		//PrintInfo();
	}
}

void PhysicsSystem3D::SolveVelocitiesJacobi(float h, float omega)
{
	PROFILE_SCOPE("Jacobi vel");
	Particle3D dummy; dummy.invMass = 0.f;
	float gamma = 0.f;
	for (int k = 0; k < numIterations; ++k)
	{
		if (solver == PhysicsSystem::CONJ_RES || solver == PhysicsSystem::JACOBI_CR)
		{
			const float mul = 1.f;
			const float exp = 0.6f;
			gamma = mul * (float)k / (float)(numIterations - 1);
			if (gamma > 0) gamma = pow(gamma, exp); // sqrt(gamma)
			gamma = min(1.f, gamma);
		}

		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint3D& pair = constraints[i];
			Particle3D* p1 = &particles[pair.i1];
			Particle3D* p2 = &dummy;
			Vector3 v12;
			if (pair.type == Constraint3D::COLL_PAIR)
			{			
				p2 = &particles[pair.i2];
				v12 = p2->velocity - p1->velocity;
			}
			else if (pair.type == Constraint3D::CONTACT)
			{
				v12 = p1->velocity; v12.Flip();
			}

			float vnrel = pair.normal.Dot(v12);
			pair.err = vnrel + beta * pair.depth / h;		

			float alpha = omega;
			if (solver == PhysicsSystem::JACOBI || solver == PhysicsSystem::JACOBI_CR)
			{
				alpha = omega / (p1->invMass + p2->invMass);
			}
			pair.dLambda = alpha * pair.err + gamma * pair.dLambda;

			float lambda0 = pair.lambda;
			pair.lambda = lambda0 + pair.dLambda;
			if (pair.lambda < 0.f)
				pair.lambda = 0.f;
			pair.dLambda = pair.lambda - lambda0;

			pair.disp = pair.dLambda * pair.normal;

			if (mu > 0.f)
			{
				const float limit = mu * pair.lambda;
#ifdef CCP
				Vector3 vt = v12 - vnrel * pair.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel / (p1->invMass + p2->invMass);
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = lambda0 + dLambda;
					if (pair.lambdaF1 >= limit)
						pair.lambdaF1 = limit;
					dLambda = pair.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 p = dLambda * vt;
					p1->velocity += p1->invMass * p;
					p2->velocity -= p2->invMass * p;
				}
#else
				float vtrel = v12.Dot(pair.t1);
				if (fabs(vtrel) >= 0.0001f)
				{
					pair.dLambdaF1 = vtrel * alpha + gamma * pair.dLambdaF1;
					float lambdaF0 = pair.lambdaF1;
					pair.lambdaF1 = clamp(lambdaF0 + pair.dLambdaF1, -limit, limit);
					pair.dLambdaF1 = pair.lambdaF1 - lambdaF0;
			
					pair.disp += pair.dLambdaF1 * pair.t1;					
				}

				vtrel = v12.Dot(pair.t2);
				if (fabs(vtrel) >= 0.0001f)
				{
					pair.dLambdaF2 = vtrel * alpha + gamma * pair.dLambdaF2;
					float lambdaF0 = pair.lambdaF2;
					pair.lambdaF2 = clamp(pair.lambdaF2 + pair.dLambdaF2, -limit, limit);
					pair.dLambdaF2 = pair.lambdaF2 - lambdaF0;
			
					pair.disp += pair.dLambdaF2 * pair.t2;
				}
#endif
			}
		}

		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint3D& pair = constraints[i];
			Particle3D* p1 = &particles[pair.i1];
			Particle3D* p2 = pair.type == Constraint3D::COLL_PAIR ? &particles[pair.i2] : &dummy;

			p1->velocity += p1->invMass * pair.disp;
			p2->velocity -= p2->invMass * pair.disp;
		}
		//PrintInfo();
	}
}

#define POS_PREVIEW
void PhysicsSystem3D::MicroStepSP(float h)
{
	//Printf("Frame %d\n", nFrames);
#ifndef POS_PREVIEW
	Collide();
#endif
	// integrate velocities
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass != 0.f)
		{
			particles[i].velocity += h * gravity;
			particles[i].prevPos = particles[i].pos;
#ifdef POS_PREVIEW
			particles[i].pos += h * particles[i].velocity;
#endif
		}
	}

#ifdef POS_PREVIEW
	Collide();
#endif

	SolveSP(h);

	//PrintInfo();
}

void PhysicsSystem3D::SolveSP(float h)
{
	PROFILE_SCOPE("SP");
	
	// preamble
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint3D& pair = constraints[i];
		const Particle3D& p1 = particles[pair.i1];
		// TODO: remove
		if (pair.type == Constraint3D::LINK || pair.type == Constraint3D::COLL_PAIR)
		{			
			const Particle3D& p2 = particles[pair.i2];
			Vector3 n = p1.pos - p2.pos;
			float len = n.Length();
			pair.depth = pair.len - len;
			n.Normalize();
			pair.normal = n;
		}
		else if (pair.type == Constraint3D::CONTACT)
		{
			pair.depth = pair.len - pair.normal.Dot(p1.pos - pair.point);
		}

		pair.t1 = pair.normal.Perpendicular();
		pair.t2 = pair.normal.Cross(pair.t1);
		pair.t2.Normalize();

		pair.dLambda = 0.f;

		pair.lambda = 0.f;
		pair.lambdaF1 = 0.f;
		pair.lambdaF2 = 0.f;
	}

	if (solver == PhysicsSystem::GAUSS_SEIDEL)
		SolveSP_GS(h);
	else
		SolveSP_CR(h, 0.5f, solver);
}

void PhysicsSystem3D::SolveSP_GS(float h)
{
	PROFILE_SCOPE("SP GS");

	const float alpha = 0.1f;
	const float beta = 1.0f;
	float b = 0.f;
	Particle3D dummy; dummy.invMass = 0.f;
	for (int k = 0; k < numIterations; ++k)
	{
		//if (solver == PhysicsSystem::CONJ_RES || solver == PhysicsSystem::JACOBI_CR)
		// improved GS
		{
			b = (float)k / (float)(numIterations - 1);
			if (b > 0)
				b = pow(b, 0.6f);
		}

		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint3D& pair = constraints[i];
			if (constraints[i].type == Constraint3D::SELF_TRIANGLE)
			{
				SolveTriangle(h, pair, alpha, beta, k);
				continue;
			}

			Particle3D* p1 = &particles[pair.i1];
			Particle3D* p2 = &dummy;
			Vector3 v12;
			if (pair.type != Constraint3D::CONTACT)
			{
				p2 = &particles[pair.i2];
				Vector3 n = p1->pos - p2->pos;
				float len = n.Length();
				pair.depth = pair.len - len;
				if (pair.type == Constraint3D::COLL_PAIR)
					pair.depth = max(0.f, pair.depth);
				pair.normal = (1.f / len) * n;
				v12 = p2->velocity - p1->velocity;
			}
			else
			{
				pair.depth = max(0.f, pair.len - pair.normal.Dot(p1->pos - pair.point));
				v12 = p1->velocity; v12.Flip();
			}

			float vnrel = pair.normal.Dot(v12);
#ifdef CCP
			Vector3 vt = v12 - vnrel * pair.normal;
			float vtrel = vt.Length();
			vnrel -= mu * vtrel;
#endif
			pair.err = alpha * vnrel + beta * pair.depth / h;
			//pair.dLambda = 0.3f * pair.stiffness * pair.err / (p1->invMass + p2->invMass) + b * pair.dLambda;
			pair.dLambda = pair.stiffness * pair.err / (p1->invMass + p2->invMass);

			if (pair.type != Constraint3D::LINK)
			{
				float lambda0 = pair.lambda;
				pair.lambda = lambda0 + pair.dLambda; // TODO: project lambda on the new direction (* dot(n_old, n_new))
				if (pair.lambda < 0.f)
					pair.lambda = 0.f;
				pair.dLambda = pair.lambda - lambda0;
			}

			Vector3 disp = pair.dLambda * pair.normal;
			p1->velocity += p1->invMass * disp;
			p2->velocity -= p2->invMass * disp;
		
			// friction
			if (mu > 0.f && pair.type != Constraint3D::LINK)
			{
				const float limit = mu * pair.lambda;
#ifdef CCP
				if (vtrel > 0.001f)
				{
					// TODO: Jakobsen friction
					float dLambda = vtrel / (p1->invMass + p2->invMass); // FIXME
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = lambda0 + dLambda;
					if (pair.lambdaF1 >= limit)
						pair.lambdaF1 = limit;
					dLambda = pair.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 p = dLambda * vt;
					p1->velocity += p1->invMass * p;
					p2->velocity -= p2->invMass * p;
				}
#else
				float vtrel = v12.Dot(pair.t1);
				if (fabs(vtrel) >= 0.0001f)
				{
					float dLambdaF = vtrel / (p1->invMass + p2->invMass); // FIXME
					float lambdaF0 = pair.lambdaF1;
					pair.lambdaF1 = lambdaF0 + dLambdaF;
					pair.lambdaF1 = clamp(pair.lambdaF1, -limit, limit);
					dLambdaF = pair.lambdaF1 - lambdaF0;
			
					Vector3 vt = dLambdaF * pair.t1;
					disp += vt;
					p1->velocity.Add(vt * p1->invMass);
					p2->velocity.Subtract(vt * p2->invMass);
				}

				vtrel = v12.Dot(pair.t2);
				if (fabs(vtrel) >= 0.0001f)
				{
					float dLambdaF = vtrel / (p1->invMass + p2->invMass); // FIXME
					float lambdaF0 = pair.lambdaF2;
					pair.lambdaF2 = lambdaF0 + dLambdaF;
					pair.lambdaF2 = clamp(pair.lambdaF2, -limit, limit);
					dLambdaF = pair.lambdaF2 - lambdaF0;
			
					Vector3 vt = dLambdaF * pair.t2;
					disp += vt;
					p1->velocity.Add(vt * p1->invMass);
					p2->velocity.Subtract(vt * p2->invMass);
				}
#endif
			}

#ifndef POS_PREVIEW
			if (k > 0)
#endif
			{
				p1->pos += h * p1->invMass * disp;
				p2->pos -= h * p2->invMass * disp;
			}
		}

#ifndef POS_PREVIEW
		if (k == 0)
		{
			for (size_t i = 0; i < GetNumParticles(); i++)
			{
				particles[i].pos += h * particles[i].invMass * particles[i].velocity;
			}
		}
#endif
		//PrintInfo();
	}
}

void PhysicsSystem3D::SolveSP_CR(float h, float omega, PhysicsSystem::SolverType solver)
{
	PROFILE_SCOPE("SP CR");
	Particle3D dummy; dummy.invMass = 0.f;
	float b = 0;
	float a = 0.f;
	float beta = 1.f;
	for (int k = 0; k < numIterations; k++)
	{
		if (solver == PhysicsSystem::CONJ_RES || solver == PhysicsSystem::JACOBI_CR)
		{
			b = (float)k / (float)(numIterations - 1);
			if (b > 0)
				b = pow(b, 0.6f);
		}
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint3D& pair = constraints[i];
			if (pair.type == Constraint3D::SELF_TRIANGLE)
			{
				SolveTriangleCR(h, pair, omega, a, beta, b, k);
				continue;
			}
			Particle3D* p1 = &particles[pair.i1];
			Particle3D* p2 = &dummy;
			Vector3 v12;
			if (pair.type != Constraint3D::CONTACT)
			{			
				p2 = &particles[pair.i2];
				Vector3 n = p1->pos - p2->pos;
				float len = n.Length();
				pair.depth = pair.len - len; // FIXME
				n.Normalize();
				pair.normal = n;

				v12 = p2->velocity - p1->velocity;
			}
			else //if (pair.type == Constraint3D::CONTACT)
			{
				pair.depth = max(0.f, pair.len - pair.normal.Dot(p1->pos - pair.point));
				v12 = p1->velocity; 
				v12.Flip();
			}

			float vnrel = pair.normal.Dot(v12);
			pair.err = a * vnrel + beta * pair.depth / h;		

			float alpha = omega;
			if (solver == PhysicsSystem::JACOBI || solver == PhysicsSystem::JACOBI_CR)
			{
				alpha = omega / (p1->invMass + p2->invMass);
			}
			pair.dLambda = pair.stiffness * (alpha * pair.err + b * pair.dLambda);

			if (pair.type != Constraint3D::LINK)
			{
				float lambda0 = pair.lambda;
				pair.lambda = lambda0 + pair.dLambda;
				if (pair.lambda < 0.f)
					pair.lambda = 0.f;
				pair.dLambda = pair.lambda - lambda0;
			}

			pair.disp = pair.dLambda * pair.normal;
	
			if (pair.type != Constraint3D::LINK && mu > 0.f)
			{
				float vtrel = v12.Dot(pair.t1);
				const float limit = mu * pair.lambda;
				if (fabs(vtrel) >= 0.0001f)
				{
					pair.dLambdaF1 = vtrel * alpha + b * pair.dLambdaF1;
					float lambdaF0 = pair.lambdaF1;
					pair.lambdaF1 = clamp(lambdaF0 + pair.dLambdaF1, -limit, limit);
					pair.dLambdaF1 = pair.lambdaF1 - lambdaF0;
			
					pair.disp += pair.dLambdaF1 * pair.t1;					
				}

				vtrel = v12.Dot(pair.t2);
				if (fabs(vtrel) >= 0.0001f)
				{
					pair.dLambdaF2 = vtrel * alpha + b * pair.dLambdaF2;
					float lambdaF0 = pair.lambdaF2;
					pair.lambdaF2 = clamp(pair.lambdaF2 + pair.dLambdaF2, -limit, limit);
					pair.dLambdaF2 = pair.lambdaF2 - lambdaF0;
			
					pair.disp += pair.dLambdaF2 * pair.t2;
				}
			}
		}
		for (size_t i = 0; i < constraints.size(); i++)
		{			
			Constraint3D& pair = constraints[i];
			if (pair.type == Constraint3D::SELF_TRIANGLE)
			{
				UpdateTriangleCR(h, pair, k);
				continue;
			}
			Particle3D* p1 = &particles[pair.i1];
			Particle3D* p2 = pair.type == Constraint3D::CONTACT ? &dummy : &particles[pair.i2];
			p1->velocity += p1->invMass * pair.disp;
			p2->velocity -= p2->invMass * pair.disp;

#ifndef POS_PREVIEW
			if (k > 0)
#endif
			{
				p1->pos += timeStep * p1->invMass * pair.disp;
				p2->pos -= timeStep * p2->invMass * pair.disp;
			}
		}
#ifndef POS_PREVIEW
		if (k == 0)
		{
			for (size_t i = 0; i < GetNumParticles(); i++)
			{
				particles[i].pos += timeStep * particles[i].velocity;
			}
		}
#endif
	}
}

void PhysicsSystem3D::SolveTriangle(float h, Constraint3D& contact, float alpha, float beta, int k)
{	
	Vector3& v1 = particles[contact.i1].pos;
	Vector3& v2 = particles[contact.i2].pos;
	Vector3& v3 = particles[contact.i3].pos;
	Vector3 p = contact.w1 * v1 + contact.w2 * v2 + contact.w3 * v3;
	Vector3 n = (v2 - v1).Cross(v3 - v1);
	n.Normalize();
	//contact.normal = n;

	float len0 = contact.len;
	float len = n.Dot(particles[contact.i4].pos - p);
	contact.depth = max(0.f, len0 - len);
	float invMass = particles[contact.i1].invMass + particles[contact.i2].invMass + particles[contact.i3].invMass + particles[contact.i4].invMass;
	float s = 2.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / invMass;
				
	Vector3 vp = contact.w1 * particles[contact.i1].velocity + contact.w2 * particles[contact.i2].velocity + contact.w3 * particles[contact.i3].velocity;
	Vector3 v12 = particles[contact.i4].velocity - vp;
	float vnrel = -contact.normal.Dot(v12);

	contact.dLambda = s * (alpha * vnrel + beta * contact.depth / h);

	float lambda0 = contact.lambda;
	contact.lambda = lambda0 + contact.dLambda;
	if (contact.lambda < 0.f)
		contact.lambda = 0.f;
	contact.dLambda = contact.lambda - lambda0;

	Vector3 disp = -contact.dLambda * n;
#ifndef POS_PREVIEW
	if (k > 0)
#endif
	{
		v1 += disp * (h * contact.w1 * particles[contact.i1].invMass);
		v2 += disp * (h * contact.w2 * particles[contact.i2].invMass);
		v3 += disp * (h * contact.w3 * particles[contact.i3].invMass);
		particles[contact.i4].pos -= disp * (h * particles[contact.i4].invMass);
	}

	particles[contact.i1].velocity += disp * (contact.w1 * particles[contact.i1].invMass);
	particles[contact.i2].velocity += disp * (contact.w2 * particles[contact.i2].invMass);
	particles[contact.i3].velocity += disp * (contact.w3 * particles[contact.i3].invMass);
	particles[contact.i4].velocity -= disp * (particles[contact.i4].invMass);

	// friction
	if (mu > 0.f)
	{
		const float limit = mu * contact.lambda;
#ifdef CCP
		Vector3 vt = v12 - vnrel * contact.normal;
		float vtrel = vt.Length();
		if (vtrel > 0.001f)
		{
			float dLambda = s * vtrel / invMass;
			float lambda0 = contact.lambdaF1;
			contact.lambdaF1 = lambda0 + dLambda;
			if (contact.lambdaF1 >= limit)
				contact.lambdaF1 = limit;
			dLambda = contact.lambdaF1 - lambda0;

			vt.Scale(1.f / vtrel); // normalize
			Vector3 p = dLambda * vt;
			particles[contact.i1].velocity += contact.w1 * particles[contact.i1].invMass * p;
			particles[contact.i2].velocity += contact.w2 * particles[contact.i2].invMass * p;
			particles[contact.i3].velocity += contact.w3 * particles[contact.i3].invMass * p;
			particles[contact.i4].velocity -= particles[contact.i4].invMass * p;
		}
#else
		float vtrel = v12.Dot(contact.t1);
		if (fabs(vtrel) >= 0.0001f)
		{
			float dLambdaF = s * vtrel / invMass;
			float lambdaF0 = contact.lambdaF1;
			contact.lambdaF1 = lambdaF0 + dLambdaF;
			contact.lambdaF1 = clamp(contact.lambdaF1, -limit, limit);
			dLambdaF = contact.lambdaF1 - lambdaF0;
			
			Vector3 p = dLambdaF * contact.t1;
			particles[contact.i1].velocity += contact.w1 * particles[contact.i1].invMass * p;
			particles[contact.i2].velocity += contact.w2 * particles[contact.i2].invMass * p;
			particles[contact.i3].velocity += contact.w3 * particles[contact.i3].invMass * p;
			particles[contact.i4].velocity -= particles[contact.i4].invMass * p;
		}

		vtrel = v12.Dot(contact.t2);
		if (fabs(vtrel) >= 0.0001f)
		{
			float dLambdaF = s * vtrel / invMass;
			float lambdaF0 = contact.lambdaF2;
			contact.lambdaF2 = lambdaF0 + dLambdaF;
			contact.lambdaF2 = clamp(contact.lambdaF2, -limit, limit);
			dLambdaF = contact.lambdaF2 - lambdaF0;
			
			Vector3 p = dLambdaF * contact.t2;
			particles[contact.i1].velocity += contact.w1 * particles[contact.i1].invMass * p;
			particles[contact.i2].velocity += contact.w2 * particles[contact.i2].invMass * p;
			particles[contact.i3].velocity += contact.w3 * particles[contact.i3].invMass * p;
			particles[contact.i4].velocity -= particles[contact.i4].invMass * p;
		}
#endif
	}
}

void PhysicsSystem3D::SolveTriangleCR(float h, Constraint3D& contact, float omega, float alpha, float beta, float b, int k)
{	
	Vector3& v1 = particles[contact.i1].pos;
	Vector3& v2 = particles[contact.i2].pos;
	Vector3& v3 = particles[contact.i3].pos;
	Vector3 p = contact.w1 * v1 + contact.w2 * v2 + contact.w3 * v3;
	Vector3 n = (v2 - v1).Cross(v3 - v1);
	n.Normalize();
	//contact.normal = n;

	float len0 = contact.len;
	float len = n.Dot(particles[contact.i4].pos - p);
	contact.depth = max(0.f, len0 - len);
	float invMass = particles[contact.i1].invMass + particles[contact.i2].invMass + particles[contact.i3].invMass + particles[contact.i4].invMass;
	float s = 2.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3);
				
	Vector3 vp = contact.w1 * particles[contact.i1].velocity + contact.w2 * particles[contact.i2].velocity + contact.w3 * particles[contact.i3].velocity;
	Vector3 v12 = particles[contact.i4].velocity - vp;
	float vnrel = -contact.normal.Dot(v12);

	float a = omega;
	if (solver == PhysicsSystem::JACOBI || solver == PhysicsSystem::JACOBI_CR)
	{
		a = omega / invMass;
	}
	contact.err = alpha * vnrel + beta * contact.depth / h;
	contact.dLambda = s * a * contact.err + b * contact.dLambda;

	float lambda0 = contact.lambda;
	contact.lambda = lambda0 + contact.dLambda;
	if (contact.lambda < 0.f)
		contact.lambda = 0.f;
	contact.dLambda = contact.lambda - lambda0;

	contact.disp = -contact.dLambda * n;

	// friction
	if (mu > 0.f)
	{
		const float limit = mu * contact.lambda;
#ifdef CCP
		// TODO: relaxation term
		Vector3 vt = v12 - vnrel * contact.normal;
		float vtrel = vt.Length();
		if (vtrel > 0.001f)
		{
			contact.dLambdaF1 = s * vtrel * a + b * contact.dLambdaF1;
			float lambda0 = contact.lambdaF1;
			contact.lambdaF1 = lambda0 + contact.dLambdaF1;
			if (contact.lambdaF1 >= limit)
				contact.lambdaF1 = limit;
			contact.dLambdaF1 = contact.lambdaF1 - lambda0;

			vt.Scale(1.f / vtrel); // normalize
			contact.disp += contact.dLambdaF1 * vt;
		}
#else
		float vtrel = v12.Dot(contact.t1);
		if (fabs(vtrel) >= 0.0001f)
		{
			contact.dLambdaF1 = s * vtrel * a + b * contact.dLambdaF1;
			float lambdaF0 = contact.lambdaF1;
			contact.lambdaF1 = lambdaF0 + contact.dLambdaF1;
			contact.lambdaF1 = clamp(contact.lambdaF1, -limit, limit);
			contact.dLambdaF1 = contact.lambdaF1 - lambdaF0;
			
			Vector3 p = contact.dLambdaF1 * contact.t1;
			contact.disp += p;
		}

		vtrel = v12.Dot(contact.t2);
		if (fabs(vtrel) >= 0.0001f)
		{
			contact.dLambdaF2 = s * vtrel * a + b * contact.dLambdaF2;
			float lambdaF0 = contact.lambdaF2;
			contact.lambdaF2 = lambdaF0 + contact.dLambdaF2;
			contact.lambdaF2 = clamp(contact.lambdaF2, -limit, limit);
			contact.dLambdaF2 = contact.lambdaF2 - lambdaF0;
			
			Vector3 p = contact.dLambdaF2 * contact.t2;
			contact.disp += p;
		}
#endif
	}

}

void PhysicsSystem3D::UpdateTriangleCR(float h, Constraint3D& contact, int k)
{
	const Vector3& disp = contact.disp;
#ifndef POS_PREVIEW
	if (k > 0)
#endif
	{
		particles[contact.i1].pos += disp * (h * contact.w1 * particles[contact.i1].invMass);
		particles[contact.i2].pos += disp * (h * contact.w2 * particles[contact.i2].invMass);
		particles[contact.i3].pos += disp * (h * contact.w3 * particles[contact.i3].invMass);
		particles[contact.i4].pos -= disp * (h * particles[contact.i4].invMass);
	}

	particles[contact.i1].velocity += disp * (contact.w1 * particles[contact.i1].invMass);
	particles[contact.i2].velocity += disp * (contact.w2 * particles[contact.i2].invMass);
	particles[contact.i3].velocity += disp * (contact.w3 * particles[contact.i3].invMass);
	particles[contact.i4].velocity -= disp * (particles[contact.i4].invMass);
}

void PhysicsSystem3D::EndGroup()
{
	size_t n = groups.size();
	if (n == 0)
		return;
	groups[n - 1].stop = particles.size();
	Group group;
	group.start = particles.size();
	group.stop = particles.size();
	groups.push_back(group);

#ifdef ENABLE_CL
	projCL.Init();
	velProjCL.Init();
	projCL.PrepareBuffers(particles, links); // hack
#endif
}

void PhysicsSystem3D::MicroStepSpring(float h)
{
	// TODO
}

void PhysicsSystem3D::OptimizeLinks(const std::vector<int>& order)
{
	// remap particles and links
	std::vector<Particle3D> newParticles(particles.size());
	for (size_t i = 0; i < order.size(); i++)
	{
		newParticles[order[i]] = particles[i];
	}
	newParticles.swap(particles);
	for (size_t i = 0; i < links.size(); i++)
	{
		links[i].i1 = order[links[i].i1];
		links[i].i2 = order[links[i].i2];
	}

	// build incidence list
	//std::vector<std::vector<int> > incidence(particles.size());
	//for (size_t i = 0; i < links.size(); i++)
	//{
	//	incidence[links[i].i1].push_back(i);
	//	incidence[links[i].i2].push_back(i);
	//}
	//// stats
	//for (size_t i = 0; i < particles.size(); i++)
	//{
	//	int maxIdx = 0;
	//	int minIdx = links.size();
	//	for (size_t j = 0; j < incidence[i].size(); j++)
	//	{
	//		int idx = incidence[i][j];
	//		minIdx = min(idx, minIdx);
	//		maxIdx = max(idx, maxIdx);
	//	}
	//	Printf("delta %d: %d\n", i, maxIdx - minIdx + 1);
	//}

	// reorder
	// TODO: Morton curve traversal
	//std::vector<int> map(links.size(), -1);
	//std::vector<Constraint3D> newLinks;
	//for (size_t i = 0; i < particles.size(); i++)
	//{
	//	std::sort(incidence[i].begin(), incidence[i].end());
	//	for (size_t j = 0; j < incidence[i].size(); j++)
	//	{
	//		int idx = incidence[i][j];
	//		if (map[idx] != -1)
	//			continue;
	//		map[idx] = newLinks.size();
	//		newLinks.push_back(links[idx]);
	//	}
	//}
	//newLinks.swap(links);

	//// new stats
	//for (size_t i = 0; i < particles.size(); i++)
	//{
	//	int maxIdx = 0;
	//	int minIdx = links.size();
	//	for (size_t j = 0; j < incidence[i].size(); j++)
	//	{
	//		int idx = map[incidence[i][j]];
	//		minIdx = min(idx, minIdx);
	//		maxIdx = max(idx, maxIdx);
	//		//Printf("%d ", idx);
	//	}
	//	Printf("delta %d: %d\n", i, maxIdx - minIdx + 1);
	//}
}

void PhysicsSystem3D::BuildMeshAdjacency()
{
	// build unique edges array
	std::vector<Mesh::Edge> duplicates(cloth->indices.size());
	for (size_t i = 0; i < cloth->indices.size() - 1; i++)
	{
		// construct edge
		int i1 = cloth->indices[i];
		int i2 = cloth->indices[i + 1];
		if ((i + 1) % 3 == 0)
			i2 = cloth->indices[i - 2];

		if (i1 > i2)
			std::swap(i1, i2);
		duplicates[i] = Mesh::Edge(i1, i2);
	}
	// sort it so we can find duplicates
	std::sort(duplicates.begin(), duplicates.end(), CompareEdges);
	
	if (incidence.size() != particles.size())
		incidence.resize(particles.size());
	for (size_t i = 0; i < incidence.size(); i++)
		incidence[i].clear();
	edges.clear();
	edges.push_back(duplicates[0]);
	for (size_t i = 1; i < duplicates.size(); i++)
	{
		if (duplicates[i].i1 == duplicates[i - 1].i1 && duplicates[i].i2 == duplicates[i - 1].i2)
			continue;
		incidence[duplicates[i].i1].push_back(edges.size());
		incidence[duplicates[i].i2].push_back(edges.size());
		edges.push_back(duplicates[i]);
	}

	Printf("unique edges: %d\n", edges.size());
}