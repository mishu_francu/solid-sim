#ifndef UNIFIED_VIEW_H
#define UNIFIED_VIEW_H

#include <Physics/Unified/Granular.h>
#include <Physics/Unified/Cloth.h>
#include <Physics/Unified/SoftFEM.h>
#include <Physics/Unified/Rigids.h>
#include <Physics/Unified/UnifiedSolver.h>
#include <Geometry/Mesh.h>
#include <Graphics3D/Graphics3D.h>
#include <Graphics2D/Texture.h>
#include <Engine/Profiler.h>

// couplers
#include <Physics/Unified/ClothGranularCoupler.h>
#include <Physics/Unified/ClothRigidCoupler.h>
#include <Physics/Unified/ClothSoftCoupler.h>
#include <Physics/Unified/SoftCoupler.h>

namespace Physics 
{
	//class RigidSoftCoupler;

	enum DebugDrawFlags
	{
		DDF_CONTACTS = 1,
		DDF_WARP_WEFT = 2,
		DDF_PARTICLES = 4,
		DDF_STRAIN = 8,
		DDF_LINKS = 16,
		DDF_SELF_TRIS = 32,
		DDF_SELF_EDGES = 64,
		DDF_TREE = 128,
		DDF_TRI_CONTACTS = 256,
		DDF_TREE_SELF = 512,
		DDF_TETS = 1024,
		DDF_JOINTS = 2 >> 11,
	};
}

#ifdef USE_ALEMBIC
// Alembic Includes
#include <Alembic/AbcGeom/All.h>
// This is required to tell Alembic which implementation to use.
#include <Alembic/AbcCoreOgawa/All.h>
#endif

typedef Physics::UnifiedSystem<Physics::GaussSeidelSolver> UnifiedPhysics;
//typedef Physics::UnifiedSystem<Physics::JacobiSolver> UnifiedPhysics;
//typedef Physics::UnifiedSystem<Physics::NewmarkGSSolver> UnifiedPhysics;

struct IGroupView
{
	virtual void Draw(Graphics3D* graphics3D) = 0;
	virtual void Update() { }
	virtual void ExportToText(FILE* txt) { }
#ifdef USE_ALEMBIC
	virtual void ExportToAlembic(Alembic::Abc::OArchive* archive) { }
#endif
	virtual ~IGroupView() = 0;

	IGroupView(UnifiedPhysics* sys) : mSystem(sys) { }
	
	UnifiedPhysics* mSystem;
};

inline IGroupView::~IGroupView() { }

struct ICouplerView // TODO: IView
{
	virtual void Draw(Graphics3D* graphics3D) = 0;
	virtual ~ICouplerView() = 0;

	ICouplerView(UnifiedPhysics* sys) : mSystem(sys) { }

	UnifiedPhysics* mSystem;
};

inline ICouplerView::~ICouplerView() { }

class GranularView : public IGroupView
{
public:
	GranularView(Physics::Granular* granular, UnifiedPhysics* sys) : mGranular(granular), IGroupView(sys) { }
	virtual void Draw(Graphics3D* graphics3D) override
	{
		//// particle color
		//graphics3D->SetColor(0.83f, 0.67f, 0.f);
		//// draw particles
		//for (size_t i = 0; i < mGranular->GetCount(); i++)
		//{
		//	Vector3 pos = mSystem->GetBody(mGranular->GetStart() + i).pos;
		//	graphics3D->DrawSphere(pos, mGranular->GetRadius());
		//}
	}

private:
	Physics::Granular* mGranular;	
};

class ClothView : public IGroupView
{
public:
	ClothView(Physics::Cloth* cloth, UnifiedPhysics* sys)
		: mCloth(cloth)
		, IGroupView(sys)
#ifdef USE_ALEMBIC
		, mMeshyObj(nullptr)
#endif
	{ }
	
	~ClothView() override
	{
#ifdef USE_ALEMBIC
		if (mMeshyObj)
			delete mMeshyObj;
#endif
	}

	void Draw(Graphics3D* graphics3D) override;
	void Update() override
	{
		mCloth->UpdateMesh(mSystem->GetBodies());
	}
	
	void ExportToPovRay(FILE* txt);
	void ExportToText(FILE* txt);
#ifdef USE_ALEMBIC
	void ExportToAlembic(Alembic::Abc::OArchive* archive);
#endif

private:
	Physics::Cloth* mCloth;
#ifdef USE_ALEMBIC
	Alembic::AbcGeom::OPolyMesh* mMeshyObj;
#endif
};

struct MeshInterp
{
	int id1, id2, id3;
	float u, v, w;//barycentric
	Vector3 point;
	Vector3 original;
	float distance;
	int tet_index;
	int vert_index;
};

class SoftFEMView : public IGroupView
{
public:
	SoftFEMView(Physics::SoftFEM* soft, UnifiedPhysics* sys) 
		: mSoft(soft)
		, IGroupView(sys)
#ifdef USE_ALEMBIC
		, mMeshyObj(nullptr)
#endif
		, mDebugDrawFlags(/*Physics::DDF_TETS*/0)
	{
	}

	// TODO: destructor

	void Draw(Graphics3D* graphics3D) override;

	void MapMesh();
	void UpdateMesh();

	static Physics::SoftFEM* LoadFromTetFile(const char* path, UnifiedPhysics& sys, float scale, const Vector3& offset);
	static Physics::SoftFEM* LoadFromVolFile(const char* path, UnifiedPhysics& sys, float scale, const Vector3& offset);

#ifdef USE_ALEMBIC
	void ExportToAlembic(Alembic::Abc::OArchive* archive) override;
#endif

private:
	Physics::SoftFEM* mSoft;
	std::map<int, std::vector<MeshInterp>> map_triangles;
#ifdef USE_ALEMBIC
	std::unique_ptr<Alembic::AbcGeom::OPolyMesh> mMeshyObj;
#endif
	int mDebugDrawFlags;
	static int mCounter;
public:
	Geometry::Mesh mMesh;
	Geometry::Mesh* mCollMesh;
};

class RigidsView : public IGroupView
{
public:
	RigidsView(Physics::Rigids* rigids, UnifiedPhysics* sys) : mRigids(rigids), IGroupView(sys), mDebugDrawFlags(Physics::DDF_CONTACTS)
	{
	}

	~RigidsView() override { }

	void Draw(Graphics3D* graphics3D) override;

#ifdef USE_ALEMBIC
	void ExportToAlembic(Alembic::Abc::OArchive* archive);
#endif

private:
	Physics::Rigids* mRigids;
	Texture mChecker;
#ifdef USE_ALEMBIC
	std::vector<std::unique_ptr<Alembic::AbcGeom::OXform>> mLinks;
	std::vector<std::unique_ptr<Alembic::AbcGeom::OPolyMesh>> mMeshes;
#endif
	int mDebugDrawFlags;
};

class SoftCouplerView : public ICouplerView
{
public:
	SoftCouplerView(Physics::SoftCoupler* coupler, UnifiedPhysics* sys) : mCoupler(coupler), ICouplerView(sys) { }

	void Draw(Graphics3D* graphics3D) override
	{
		//for (size_t i = 0; i < mCouplerRigid->mTriangleContacts.mTris.size(); i++)
		//{
		//	graphics3D->SetColor(1, 0, 0);
		//	graphics3D->DrawSphere(mSystem->GetBody(mCouplerRigid->mTriangleContacts.mTris[i].i1).pos, 0.1f);
		//	graphics3D->DrawSphere(mSystem->GetBody(mCouplerRigid->mTriangleContacts.mTris[i].i2).pos, 0.1f);
		//	graphics3D->DrawSphere(mSystem->GetBody(mCouplerRigid->mTriangleContacts.mTris[i].i3).pos, 0.1f);		
		//}

		for (size_t i = 0; i < mCoupler->GetContacts().GetNumContacts(); i++)
		{
			graphics3D->SetColor(1, 1, 1);
			const Physics::TriangleRigidContact& tri = mCoupler->GetContacts().GetContact(i);
			//if (tri.i3 != 0)
				//continue;
			const Physics::Body& body = mSystem->GetBody(tri.i4);
			Vector3 p = qRotate(body.q, tri.a) + body.pos;
			graphics3D->DrawLine(p, p + tri.normal * 10.f);
			const Physics::Body& b1 = mSystem->GetBody(tri.i1);
			const Physics::Body& b2 = mSystem->GetBody(tri.i2);
			const Physics::Body& b3 = mSystem->GetBody(tri.i3);
			Vector3 q = tri.w1 * b1.pos + tri.w2 * b2.pos + tri.w3 * b3.pos;
			graphics3D->SetColor(0, 1, 0);
			graphics3D->DrawLine(p, q);
		}
	}

private:
	Physics::SoftCoupler* mCoupler;
};

class UnifiedView
{
public:
	enum UserDataType
	{
		UD_DEFAULT = 0,
		UD_STATIC = 1, // TODO: change to more relevant
		UD_SPHERE = 2,
		UD_INVISIBLE = 3,
	};
public:
	UnifiedView();
	void Init()
	{
		Texture::SetFiltering(Texture::BILINEAR);
		mChecker.LoadTexture("checker.bmp");
	}

	~UnifiedView();

	void LoadFromConfig(XMLElement* root);

	void InitRigids();
	void InitStack();
	void InitFEM();
	void InitJoints();
	void InitCoupling();
	void InitFriction();
	void InitClothSphere();
	void InitCantilever();
	void InitStairs();
	void InitCable();
    void InitKinematic();

    void UpdateKinematic();

	Physics::Cloth* ConstructCloth(int divisions, float inc, Vector3 offset, bool horizontal, int attached, float mass = 15.f, float stiffness = 1e8f, float damping = 0);
	Physics::Granular* ConstructGranular(float radius, int numParticles, const Vector3& offset, const Vector3& velocity, float& halfExtent);
	Physics::SoftFEM* ConstructCantilever(int width, int height, int depth);
	SoftFEMView* ConstructSoftFEM(const char* path, Vector3 offset, float scale);

	void InitCloth(int divisions, float inc, Vector3 offset, bool horizontal, bool attached);

	void InitGranular(float radius, int numParticles);

	void Draw(Graphics3D* graphics3D);

	void Step();

	void ExportToPovRay(FILE* txt);
	void ExportToText(FILE* txt);

    void ExportToText();

	void ExportToAlembic();
	void SetMaxFrames(int val) { mMaxFrames = val; }
	void SetAbcExport(bool val) { mAbcExport = val; }

    void Clear();

private:
	Physics::Body& AddRigid(Physics::Rigids* rigids, size_t i, std::shared_ptr<Physics::Collidable>, float invMass, const Matrix3& Icinv);
	void AddChandelierChildren(Physics::Rigids* rigids, const Physics::Capsule& cap1, int i1, int i2, int i3, int level);

private:
	UnifiedPhysics mSystem;
	std::vector<IGroupView*> mViews; // TODO: shared_ptr
	std::vector<std::shared_ptr<ICouplerView>> mCouplerViews;
	Texture mChecker;
	int mNumFrames;
	//Physics::ClothRigidCoupler* mCoupler; // temp
	Geometry::Mesh mMesh;
#ifdef USE_ALEMBIC
	Alembic::Abc::OArchive* mArchive;
	std::vector<Alembic::AbcGeom::OXform*> mTransforms;
	std::vector<Alembic::AbcGeom::OPolyMesh*> mMeshes;
#endif
	Geometry::Mesh mSphereMesh, mBoxMesh;
	int mMaxLevels;
	int mMaxFrames;
	bool mAbcExport;
    
    // used by the Kinematic demo only
    int mCounter;
    Physics::Body* mBase;
};

Physics::Box CreateBox(const Vector3& pos, const Quaternion& q, const Vector3& dim, float invMass, int idx, Physics::Group* group, Matrix3& Icinv);

#endif // UNIFIED_VIEW_H