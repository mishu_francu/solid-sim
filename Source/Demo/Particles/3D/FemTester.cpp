#include "FemTester.h"
#include <Physics/FEM/FemDataStructures.h>

#ifdef RUN_TESTS

#include "FemPhysicsLinearExplicit.h"
#include "FemPhysicsLinearIncompressible.h"
#include <iostream>

namespace FEM_SYSTEM
{
	inline bool EqualEps(real a, real b, real eps = 1e-14)
	{
		return abs(a - b) < eps;
	}

	inline bool EqualVector(const Vector3R& a, const Vector3R& b, real eps = 1e-14)
	{
		return EqualEps(a.x, b.x, eps) && 
			EqualEps(a.y, b.y, eps) && 
			EqualEps(a.z, b.z, eps);
	}

	static bool EqualMatrix(const Matrix3R& a, const Matrix3R& b)
	{
		for (uint32 i = 0; i < 3; i++)
		{
			for (uint32 j = 0; j < 3; j++)
			{
				if (!EqualEps(a.m[i][j], b.m[i][j]))
					return false;
			}
		}
		return true;
	}

	static bool EqualMatrix(const Matrix3R& a, const EigenMatrix& b)
	{
		for (uint32 i = 0; i < 3; i++)
		{
			for (uint32 j = 0; j < 3; j++)
			{
				if (!EqualEps(a.m[i][j], b(i, j)))
					return false;
			}
		}
		return true;
	}

	static uint16 stdOrder[4] = { 0, 1, 2, 3 };
	static uint16 permutations[][4] =
	{ // A=0, B=1, C=2, D=4
		{0, 1, 2, 3}, //ABCD
		{1, 0, 2, 3}, //BACD
		{2, 0, 1, 3}, //CABD
		{0, 2, 1, 3}, //ACBD
		{1, 2, 0, 3}, //BCAD
		{2, 1, 0, 3}, //CBAD
		{3, 1, 0, 2}, //DBAC
		{1, 3, 0, 2}, //BDAC
		{0, 3, 1, 2}, //ADBC
		{3, 0, 1, 2}, //DABC
		{1, 0, 3, 2}, //BADC
		{0, 1, 3, 2}, //ABDC
		{0, 2, 3, 1}, //ACDB
		{2, 0, 3, 1}, //CADB
		{3, 0, 2, 1}, //DACB
		{0, 3, 2, 1}, //ADCB
		{2, 3, 0, 1}, //CDAB
		{3, 2, 0, 1}, //DCAB
		{3, 2, 1, 0}, //DCBA
		{2, 3, 1, 0}, //CDBA
		{1, 3, 2, 0}, //BDCA
		{3, 1, 2, 0}, //DBCA
		{2, 1, 3, 0}, //CBDA
		{1, 2, 3, 0}, //BCDA
	};

	static void InitTetrahedron(uint16 order[4], std::vector<Node>& testNodes, std::vector<Tetrahedron>& testTets, real& x0, int numBCs = 3)
	{
		// set nodes
		real hw = 1;
		x0 = 2 * hw;
		testNodes.resize(4);
		testNodes[0].pos = FEM_SYSTEM::Vector3R(0, -hw, hw);
		testNodes[1].pos = FEM_SYSTEM::Vector3R(0, -hw, -hw);
		testNodes[2].pos = FEM_SYSTEM::Vector3R(0, hw, hw);
		testNodes[3].pos = FEM_SYSTEM::Vector3R(x0, -hw, hw);
		//testNodes[0].pos = FEM_SYSTEM::Vector3R(0, 0, -1);
		//testNodes[1].pos = FEM_SYSTEM::Vector3R(0, 1, 0);
		//testNodes[2].pos = FEM_SYSTEM::Vector3R(1, 0, 0);
		//testNodes[3].pos = FEM_SYSTEM::Vector3R(1, 1, -1);
		// set tet (such that global and local node indices coincide)
		testTets.resize(1);
		testTets[0].i[0] = order[0];
		testTets[0].i[1] = order[1];
		testTets[0].i[2] = order[2];
		testTets[0].i[3] = order[3];
		// boundary conditions
		for (int i = 0; i < numBCs; i++)
			testNodes[i].invMass = 0;
	}

	void FemTester::Test()
	{
		TestStiffness();

		// test innermost tet
		//for (int j = 0; j < 24; j++)
		//{
		//	// init tetrahedron
		//	std::vector<Node> testNodes;
		//	std::vector<Tetrahedron> testTets;
		//	real x0;
		//	InitTetrahedron(permutations[j], testNodes, testTets, x0, 2);

		//	// create linear elasticity simulator object
		//	real youngModulus = 1;
		//	real poissonRatio = 0.25;
		//	FemPhysicsLinearExplicit testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_EXPLICIT_LINEAR, 1);

		//	// apply displacement
		//	real disp = 0.5;
		//	testFem.mDeformedPositions[0][0] += disp;
		//	testFem.mDeformedPositions[1][0] += disp;

		//	// compute deformation gradient
		//	Matrix3R F;
		//	testFem.ComputeDeformationGradient(0, F);
		//	Matrix3R refDefGrad;
		//	refDefGrad(0, 0) = (x0 + disp) / x0;
		//	ASSERT(EqualMatrix(F, refDefGrad));

		//	// compute displacement gradient (we are interested in du_x/dx)
		//	Matrix3R Du = F - Matrix3R::Identity(); // TODO: testFem.CoputeDisplacementGradient()
		//	real refDispGrad = disp / x0;
		//	ASSERT(EqualEps(Du.m[0][0], refDispGrad));

		//	// compute Cauchy strain using direct formula
		//	Matrix3R strain = 0.5 * (Du + !Du); // TODO: testFem.ComputeStrain()
		//	Matrix3R refStrain(0);
		//		refStrain(0, 0) = Du(0, 0);
		//	ASSERT(EqualMatrix(strain, refStrain));
		//	real eps = strain(0, 0);

		//	// Lame parameters
		//	real mu = 0.5 * youngModulus / (1 + poissonRatio);
		//	real lambda = youngModulus * poissonRatio / (1 + poissonRatio) / (1 - 2 * poissonRatio);

		//	// compute stress

		//	// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		//	Matrix3R stress1 = 2 * mu * strain + lambda * strain.Trace() * Matrix3R::Identity();
		//	Matrix3R refStress;
		//	{
		//		Vector3R refNormalStress(0.6, 0.2, 0.2);
		//		refStress = Matrix3R(refNormalStress);
		//	}
		//	ASSERT(EqualMatrix(stress1, refStress));

		//	// compute the total force at node 3
		//	Matrix3R X(testFem.mBarycentricJacobians[0].y[1], testFem.mBarycentricJacobians[0].y[2], testFem.mBarycentricJacobians[0].y[3]);
		//	Matrix3R forces = -testFem.mElementVolumes[0] * stress1 * X;
		//	Vector3R f[4];
		//	f[0] = forces(0) + forces(1) + forces(2);
		//	f[1] = -forces(0);
		//	f[2] = -forces(1);
		//	f[3] = -forces(2);

		//	Vector3R fGlobal[4];
		//	for (int n = 0; n < 4; n++)
		//	{
		//		int g = testFem.mTetMesh->GetGlobalIndex(0, n);
		//		fGlobal[g] = f[n];
		//	}
		//}

		for (int i = 1; i < 4; i++) // number of BCs
		{
			for (int j = 0; j < 24; j++)
			{
				TestDisplacement(i, 0, permutations[j]); // pure translation
				TestDisplacement(i, 1, permutations[j]); // pure shear
			}
		}
		// TODO: test rotation, shear due to translation on Z
		// TODO: test scaling or just volume increase (with no deviatoric component)
		// TODO: test barycentric Jacobians
		// TODO: test mass matrix and body forces vector
		TestCube();
		// TODO: test mass matrix
		// TODO: test integrator
		
		// piece-wise constant pressure
		TestPureTranslationIncompressible(0);
		TestPureShearIncompressible(0);
		TestVolumeIncreaseIncompressible(0);

		// linear pressure
		TestPureTranslationIncompressible(1);
		TestPureShearIncompressible(1);
		TestVolumeIncreaseIncompressible(1);
		// TODO: test the pressure mesh

		TestPureTranslationQuadratic();
		TestPureShearQuadratic();

		TestPureTranslationQuadraticIncompressible(0);
		TestPureShearQuadraticIncompressible(0);
		TestVolumeIncreaseQuadraticIncompressible(0);

		//TestPureTranslationQuadraticIncompressible(1);
		//TestPureShearQuadraticIncompressible(1);
		//TestVolumeIncreaseQuadraticIncompressible(1);

		TestVolumeIncrease();
		TestVolumeIncreaseQuadratic();
	}

	void FemTester::TestStiffness()
	{
		// init tetrahedron
		std::vector<Node> testNodes;
		std::vector<Tetrahedron> testTets;
		real x0;
		int numBCs = 3; // TODO: test for less BCs
		InitTetrahedron(stdOrder, testNodes, testTets, x0, numBCs);

		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearExplicit testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_LINEAR_ELASTICITY, 1);

		// Lame parameters
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();
		ASSERT(EqualEps(mu, 0.4) && EqualEps(lambda, 0.4));

		// bulk modulus
		real bulkModulus = testFem.GetBulkModulus();
		ASSERT(bulkModulus == 2.0 / 3.0);

		// compute strain Jacobian w.r.t to nodal positions/displacements
		Matrix3R Bn[4], Bs[4];
		testFem.ComputeStrainJacobian(0, Bn, Bs);

		// check the stiffness matrix

		// method 1
		Matrix3R K = testFem.mElementVolumes[0] * (Bn[3] * testFem.mNormalElasticityMatrix * Bn[3] + mu * Bs[3] * Bs[3]);
		ASSERT(EqualMatrix(K, testFem.mStiffnessMatrix));
	}

	void FemTester::TestDisplacement(int numBCs, int axis, uint16 order[4])
	{
		// init tetrahedron
		std::vector<Node> testNodes;
		std::vector<Tetrahedron> testTets;
		real x0;
		InitTetrahedron(order, testNodes, testTets, x0, numBCs);

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearExplicit testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_LINEAR_ELASTICITY, 1);

		// apply displacement
		real disp = 1.0;
		int idx = 3 - numBCs; // 3 is the global index
		testFem.mDeformedPositions[idx][axis] += disp;

		// compute displacement vector for node 3 only
		Vector3R u3 = testFem.GetDeformedPosition(3) - testFem.mReferencePositions[3];
		Vector3R refDisp;
		refDisp[axis] = disp;
		ASSERT(EqualVector(u3, refDisp));

		// compute deformation gradient
		Matrix3R F;
		testFem.ComputeDeformationGradient(0, F);
		Matrix3R refDefGrad;
		if (axis == 0)
			refDefGrad(0, 0) = (x0 + disp) / x0;
		else
			refDefGrad(1, 0) = disp / x0;
		ASSERT(EqualMatrix(F, refDefGrad));

		// compute displacement gradient (we are interested in du_x/dx)
		Matrix3R Du = F - Matrix3R::Identity(); // TODO: testFem.CoputeDisplacementGradient()
		real refDispGrad = disp / x0;
		ASSERT(EqualEps(Du.m[axis][0], refDispGrad));

		// compute Cauchy strain using direct formula
		Matrix3R strain = 0.5 * (Du + !Du); // TODO: testFem.ComputeStrain()
		Matrix3R refStrain(0);
		if (axis == 0)
			refStrain(0, 0) = Du(0, 0);
		else
		{
			refStrain(1, 0) = 0.25 * disp;
			refStrain(0, 1) = 0.25 * disp;
		}
		ASSERT(EqualMatrix(strain, refStrain));
		real eps = strain(axis, 0);

		// find the local index of 3
		int localIdx = -1;
		for (int i = 0; i < 4; i++)
		{
			if (testFem.mTetMesh->GetGlobalIndex(0, i) == 3)
			{
				localIdx = i;
				break;
			}
		}
		ASSERT(localIdx >= 0);

		// compute strain using the direct formula vstrain = B3 * u3 = [ 0 y2 0 y3 0 y1]^T
		Vector6 strain1; // engineering strain (Voigt vector form)
		strain1.setZero();
		if (axis == 0)
		{
			strain1(0) = testFem.mBarycentricJacobians[0].y[localIdx].x * disp;
			strain1(4) = testFem.mBarycentricJacobians[0].y[localIdx].y * disp;
			strain1(5) = testFem.mBarycentricJacobians[0].y[localIdx].z * disp;
			ASSERT(EqualEps(strain1(0), eps) && strain1(4) == 0 && strain1(5) == 0);
		}
		else
		{
			strain1(1) = testFem.mBarycentricJacobians[0].y[localIdx].y * disp;
			strain1(3) = testFem.mBarycentricJacobians[0].y[localIdx].z * disp;
			strain1(5) = testFem.mBarycentricJacobians[0].y[localIdx].x * disp;
			ASSERT(strain1(1) == 0 && strain1(4) == 0 && strain1(5) == 2 * eps);
		}

		// compute strain Jacobian w.r.t to nodal positions/displacements
		Matrix3R Bn[4], Bs[4];
		testFem.ComputeStrainJacobian(0, Bn, Bs);

		// compute strain using the formula vstrain = B3 * u3
		SymTensor strain2; // engineering strain (Voigt vector form)
		strain2.diag = Bn[localIdx] * u3;
		strain2.shear = Bs[localIdx] * u3;
		if (axis == 0)
		{
			ASSERT(EqualVector(strain2.diag, Vector3R(eps, 0, 0)));
			ASSERT(EqualVector(strain2.shear, Vector3R(0, 0, 0)));
		}
		else
		{
			ASSERT(EqualVector(strain2.diag, Vector3R(0, 0, 0)));
			ASSERT(EqualVector(strain2.shear, Vector3R(0, 0, 2 * eps)));
		}

		// Lame parameters
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();

		// compute stress

		// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		Matrix3R stress1 = 2 * mu * strain + lambda * strain.Trace() * Matrix3R::Identity();
		Matrix3R refStress;
		if (axis == 0)
		{
			Vector3R refNormalStress(0.6, 0.2, 0.2);
			refNormalStress.Scale(disp);
			refStress = Matrix3R(refNormalStress);
		}
		else
		{
			real refStressXY = 0.2 * disp;
			refStress = Matrix3R(0.0, refStressXY, 0.0,
				refStressXY, 0.0, 0.0,
				0.0, 0.0, 0.0);
		}
		ASSERT(EqualMatrix(stress1, refStress));

		// formula 2 (using elasticity matrix and vector notation): vstress = E * vstrain
		SymTensor stress2;
		stress2.diag = testFem.mNormalElasticityMatrix * strain2.diag;
		stress2.shear = mu * strain2.shear;
		if (axis == 0)
		{
			ASSERT(EqualVector(stress2.diag, disp * Vector3R(0.6, 0.2, 0.2)));
			ASSERT(EqualVector(stress2.shear, Vector3R())); // zero
		}
		else
		{
			ASSERT(EqualVector(stress2.diag, Vector3R()));
			ASSERT(EqualVector(stress2.shear, Vector3R(0, 0, 0.2 * disp)));
		}

		// compute the total force at node 3
		Matrix3R X(testFem.mBarycentricJacobians[0].y[1], testFem.mBarycentricJacobians[0].y[2], testFem.mBarycentricJacobians[0].y[3]);
		Matrix3R forces = -testFem.mElementVolumes[0] * stress1 * X;
		Vector3R f[4];
		f[0] = forces(0) + forces(1) + forces(2);
		f[1] = -forces(0);
		f[2] = -forces(1);
		f[3] = -forces(2);
		real refForce = axis == 0 ? 0.4 : 2.0 / 15.0;
		ASSERT(EqualEps(f[localIdx][axis], refForce * disp));

		// check that the nodal forces are the same
		uint32 n = testFem.GetNumFreeNodes() * 3;
		EigenVector u(n);
		u.setZero();
		u(idx * 3 + axis) = disp;
		EigenVector fEigen = testFem.mStiffnessMatrix * u;
		Vector3Array fVec = GetStdVector(fEigen);
		for (uint32 i = 0; i < testFem.GetNumLocalNodes(); i++)
		{
			int globalIdx = testFem.mTetMesh->GetGlobalIndex(0, i);
			if (globalIdx < numBCs)
				continue;
			ASSERT(EqualVector(fVec[globalIdx - numBCs], f[i]));
		}
	}

	void FemTester::TestCube()
	{
		// create cube

		// create nodes
		std::vector<Node> testNodes(8);
		int node = 0;
		real w = 1.0;
		for (int i = 0; i < 2; i++)
		{
			real x = i * w;
			for (int k = 0; k < 2; k++)
			{
				for (int l = 0; l < 2; l++)
				{
					testNodes[node].pos = Vector3R(x, k * w, -l * w);
					if (i == 0)
						testNodes[node].invMass = 0.f; // Dirichlet boundary conditions
					node++;
				}
			}
		}
		// set tets
		std::vector<Tetrahedron> testTets(5);
		int idx[8] = { 0, 1, 3, 2, 4, 5, 7, 6 };
		testTets[0] = Tetrahedron(idx[0], idx[1], idx[3], idx[4]);
		testTets[1] = Tetrahedron(idx[1], idx[3], idx[4], idx[6]);
		testTets[2] = Tetrahedron(idx[1], idx[5], idx[6], idx[4]);
		testTets[3] = Tetrahedron(idx[1], idx[3], idx[6], idx[2]);
		testTets[4] = Tetrahedron(idx[4], idx[3], idx[7], idx[6]);

		// create simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearExplicit testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_LINEAR_ELASTICITY, 1);

		// apply displacements
		real disp = 0.5;
		testFem.mDeformedPositions[0].x += disp;
		testFem.mDeformedPositions[1].x += disp;
		testFem.mDeformedPositions[2].x += disp;
		testFem.mDeformedPositions[3].x += disp;

		// test each element
		real x0 = w;
		Vector3R totalForces[8];
		for (uint32 e = 0; e < testFem.GetNumElements(); e++)
		{
			// compute deformation gradient
			Matrix3R F;
			testFem.ComputeDeformationGradient(e, F);
			real refDefGrad = (x0 + disp) / x0;
			ASSERT(EqualMatrix(F, Matrix3R(Vector3R(refDefGrad, 1, 1))));

			// compute displacement gradient (we are interested in du_x/dx)
			Matrix3R Du = F - Matrix3R::Identity(); // TODO: testFem.ComputeDisplacementGradient()
			real refDispGrad = disp / x0;
			ASSERT(EqualEps(Du.m[0][0], refDispGrad));

			// compute Cauchy strain using direct formula
			Matrix3R strain = 0.5 * (Du + !Du); // TODO: testFem.ComputeStrain()
			real exx = strain(0, 0);

			// as Du is symmetric we should have strain = Du
			ASSERT(EqualMatrix(Du, strain));

			// Lame parameters
			real mu = testFem.GetShearModulus();
			real lambda = testFem.GetLameFirstParam();

			// compute stress

			// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
			Matrix3R stress1 = 2 * mu * strain + lambda * exx * Matrix3R::Identity();
			Vector3R refNormalStress(0.6, 0.2, 0.2);
			ASSERT(EqualMatrix(stress1, Matrix3R(refNormalStress)));

			// formula 2 (using elasticity matrix and vector notation): vstress = E * vstrain
			SymTensor stress2;
			stress2.diag = testFem.mNormalElasticityMatrix * Vector3R(exx, 0, 0);
			ASSERT(EqualVector(stress2.diag, refNormalStress));

			Matrix3R X(testFem.mBarycentricJacobians[e].y[1], testFem.mBarycentricJacobians[e].y[2], testFem.mBarycentricJacobians[e].y[3]);
			Matrix3R forces = -testFem.mElementVolumes[e] * stress1 * X;
			Vector3R f[4];
			f[0] = -forces(0) - forces(1) - forces(2);
			f[1] = forces(0);
			f[2] = forces(1);
			f[3] = forces(2);

			for (uint32 n = 0; n < 4; n++)
			{
				uint32 idx = testFem.mTetMesh->GetGlobalIndex(e, n);
				if (idx >= testFem.mNumBCs)
					totalForces[idx] += f[n];
			}
		}
	}

	void FemTester::TestPureTranslationIncompressible(int pressureOrder)
	{
		// init tetrahedron
		std::vector<Node> testNodes;
		std::vector<Tetrahedron> testTets;
		real x0;
		InitTetrahedron(stdOrder, testNodes, testTets, x0);

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearIncompressible testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_INCOMPRESSIBLE, 1, pressureOrder);

		// apply displacement
		real disp = 1.0;
		testFem.mDeformedPositions[0].x += disp;

		// compute displacement vector for node 3 only
		Vector3R u3 = testFem.GetDeformedPosition(3) - testFem.mReferencePositions[3];
		ASSERT(EqualVector(u3, Vector3R(disp, 0, 0)));

		// compute deformation gradient
		Matrix3R F;
		testFem.ComputeDeformationGradient(0, F);
		real refDefGrad = (x0 + disp) / x0;
		ASSERT(EqualMatrix(F, Matrix3R(Vector3R(refDefGrad, 1, 1))));

		// compute displacement gradient (we are interested in du_x/dx)
		Matrix3R Du = F - Matrix3R::Identity(); // TODO: testFem.ComputeDisplacementGradient()
		real refDispGrad = disp / x0;
		ASSERT(EqualEps(Du.m[0][0], refDispGrad));

		// compute Cauchy strain using direct formula
		Matrix3R strain = 0.5 * (Du + !Du); // TODO: testFem.ComputeStrain()
		real exx = strain(0, 0);

		// as Du is symmetric we should have strain = Du
		ASSERT(EqualMatrix(Du, strain));

		real ev = exx; // volumetric strain
		Matrix3R deviatoricStrain = strain;
		deviatoricStrain(0, 0) -= ev / 3.0;
		deviatoricStrain(1, 1) -= ev / 3.0;
		deviatoricStrain(2, 2) -= ev / 3.0;

		// compute volumetric strain
		real strain2vol = dot(testFem.mBarycentricJacobians[0].y[3], u3);
		ASSERT(strain2vol == ev);

		Matrix3R Bn[4], Bs[4];
		testFem.ComputeStrainJacobian(0, Bn, Bs);

		// project the normal matrices
		Matrix3R P(2. / 3., -1. / 3., -1. / 3.,
			-1. / 3., 2. / 3., -1. / 3.,
			-1. / 3., -1. / 3., 2. / 3.);

		// compute deviatoric strain
		Matrix3R Bn3 = P * Bn[3];
		Vector3R strain2diag = Bn3 * u3;
		Vector3R strain2shear = Bs[3] * u3;
		ASSERT(EqualVector(strain2diag, Vector3R(2.0 * ev / 3.0, -ev / 3.0, -ev / 3.0)));
		ASSERT(EqualVector(strain2shear, Vector3R(0, 0, 0)));

		// compute strain as vstrain = J * u / vol
		Eigen::Matrix<real, 3, 1> u;
		u.setZero();
		u(0) = disp;
		EigenMatrix constr = testFem.mVolJacobianMatrix * u;
		if (pressureOrder == 0) // FIXME
		{
			real testEv = constr(0, 0) / testFem.mElementVolumes[0];
			ASSERT(testEv == ev);
		}

		// elastic parameters
		real bulkModulus = testFem.GetBulkModulus();
		real mu = testFem.GetShearModulus();

		// check the deviatoric stiffness matrix
		Matrix3R K = testFem.mElementVolumes[0] * mu * (2.0 * !Bn3 * Bn3 + Bs[3] * Bs[3]);
		ASSERT(EqualMatrix(K, testFem.mDeviatoricStiffnessMatrix));

		// compute pressure
		real pressure = bulkModulus * ev;

		// compute deviatoric stress
		Matrix3R deviatoricStress = 2 * mu * deviatoricStrain;

		// compute total stress
		Matrix3R stress = deviatoricStress + pressure * Matrix3R::Identity();
		Vector3R refNormalStress(0.6, 0.2, 0.2);
		refNormalStress.Scale(disp);
		ASSERT(EqualMatrix(stress, Matrix3R(refNormalStress)));

		// compute the force using the traditional FEM formula after adding up the total stress
		Vector3R f3 = testFem.mElementVolumes[0] * Bn[3] * (deviatoricStress.GetDiagonal() + Vector3R(pressure));
		ASSERT(EqualEps(f3.x, 0.4 * disp));

		// check the static condensation stiffness matrix
		EigenMatrix Kcondens = testFem.mDeviatoricStiffnessMatrix + testFem.mVolJacobianMatrix.transpose() * testFem.mVolComplianceMatrix.inverse() * testFem.mVolJacobianMatrix;
		// check that the nodal forces are the same
		Eigen::Matrix<real, 3, 1> f = Kcondens * u;
		ASSERT(EqualEps(f(0), 0.4 * disp) && f(1) == 0 && f(2) == 0);
	}

	void FemTester::TestPureShearIncompressible(int pressureOrder)
	{
		// init tetrahedron
		std::vector<Node> testNodes;
		std::vector<Tetrahedron> testTets;
		real x0;
		InitTetrahedron(stdOrder, testNodes, testTets, x0);

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearIncompressible testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_INCOMPRESSIBLE, 1, pressureOrder);

		// apply displacement
		real disp = 1.0;
		testFem.mDeformedPositions[0].y += disp;

		// compute displacement vector for node 3 only
		Vector3R u3 = testFem.GetDeformedPosition(3) - testFem.mReferencePositions[3];
		ASSERT(EqualVector(u3, Vector3R(0, disp, 0)));

		// compute deformation gradient
		Matrix3R F;
		testFem.ComputeDeformationGradient(0, F);
		Matrix3R refDefGrad;
		refDefGrad(1, 0) = disp / x0;
		ASSERT(EqualMatrix(F, refDefGrad));

		// compute displacement gradient (we are interested in du_x/dx)
		Matrix3R Du = F - Matrix3R::Identity(); // TODO: testFem.ComputeDisplacementGradient()
		real refDispGrad = disp / x0;
		ASSERT(EqualEps(Du.m[1][0], refDispGrad));

		// compute Cauchy strain using direct formula
		Matrix3R strain = 0.5 * (Du + !Du); // TODO: testFem.ComputeStrain()
		real exx = strain(0, 0);
		real exy = strain(1, 0);

		real ev = exx; // volumetric strain
		ASSERT(ev == 0);

		Matrix3R deviatoricStrain = strain;
		deviatoricStrain(0, 0) -= ev / 3.0;
		deviatoricStrain(1, 1) -= ev / 3.0;
		deviatoricStrain(2, 2) -= ev / 3.0;

		// compute volumetric strain
		real strain2vol = dot(testFem.mBarycentricJacobians[0].y[3], u3);
		ASSERT(strain2vol == ev);

		Matrix3R Bn[4], Bs[4];
		testFem.ComputeStrainJacobian(0, Bn, Bs);

		// project the normal matrices
		Matrix3R P(2. / 3., -1. / 3., -1. / 3.,
			-1. / 3., 2. / 3., -1. / 3.,
			-1. / 3., -1. / 3., 2. / 3.);

		// compute deviatoric strain
		Matrix3R Bn3 = P * Bn[3];
		Vector3R strain2diag = Bn3 * u3;
		Vector3R strain2shear = Bs[3] * u3;
		ASSERT(EqualVector(strain2diag, Vector3R(0, 0, 0)));
		ASSERT(EqualVector(strain2shear, Vector3R(0, 0, 2 * exy)));

		// compute volumetric strain as vstrain = J * u / vol
		Eigen::Matrix<real, 3, 1> u;
		u.setZero();
		u(1) = disp;
		EigenMatrix constr = testFem.mVolJacobianMatrix * u;
		ASSERT(constr(0, 0) / testFem.mElementVolumes[0] == ev);

		// elastic parameters
		real bulkModulus = youngModulus / (1.0 - 2.0 * poissonRatio) / 3.0;
		real mu = 0.5 * youngModulus / (1.0 + poissonRatio); // shear modulus

		// check the deviatoric stiffness matrix
		Matrix3R K = testFem.mElementVolumes[0] * mu * (2.0 * !Bn3 * Bn3 + Bs[3] * Bs[3]);
		ASSERT(EqualMatrix(K, testFem.mDeviatoricStiffnessMatrix));

		// compute pressure
		real pressure = bulkModulus * ev;

		// compute deviatoric stress
		Matrix3R deviatoricStress = 2 * mu * deviatoricStrain;

		// compute total stress
		Matrix3R stress = deviatoricStress + pressure * Matrix3R::Identity();
		real refStressXY = 0.2 * disp;
		Matrix3R refStress = Matrix3R(0.0, refStressXY, 0.0,
			refStressXY, 0.0, 0.0,
			0.0, 0.0, 0.0);
		ASSERT(EqualMatrix(stress, refStress));

		// compute the force using the traditional FEM formula after adding up the total stress
		Vector3R f3 = testFem.mElementVolumes[0] * Bs[3] * (Vector3R(0, 0, refStressXY) + Vector3R(pressure));
		ASSERT(EqualEps(f3.y, 2.0 / 15.0 * disp));

		// check the static condensation stiffness matrix
		EigenMatrix Kcondens = testFem.mDeviatoricStiffnessMatrix + testFem.mVolJacobianMatrix.transpose() * testFem.mVolComplianceMatrix.inverse() * testFem.mVolJacobianMatrix;
		// check that the nodal forces are the same
		Eigen::Matrix<real, 3, 1> f = Kcondens * u;
		ASSERT(EqualEps(f(1), f3.y) && f(0) == 0 && f(2) == 0);

		// the volumetric part shouldn't matter
		Kcondens = testFem.mDeviatoricStiffnessMatrix;
		// check that the nodal forces are the same
		f = Kcondens * u;
		ASSERT(EqualEps(f(1), f3.y) && f(0) == 0 && f(2) == 0);
	}

	void FemTester::TestPureTranslationQuadratic()
	{
		// init tetrahedron
		std::vector<Node> testNodes;
		std::vector<Tetrahedron> testTets;
		real x0;
		InitTetrahedron(stdOrder, testNodes, testTets, x0, 3);

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearExplicit testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_LINEAR_ELASTICITY, 2);

		// apply displacement
		real disp = 1.0;
		uint32 n = testFem.GetNumFreeNodes();
		Vector3Array u(n);
		for (uint32 idx = 0; idx < n; idx++)
		{
			real factor = idx == 0 ? 1.0 : 0.5;
			testFem.mDeformedPositions[idx][0] += factor * disp;
			u[idx].x = factor * disp;
		}

		// reference strain
		real exx = 0.5;
		Vector3R refStrainNormal(exx, 0, 0);
		Matrix3R refStrain(refStrainNormal);

		// compute the strain with the formula B * u
		// code taken from FemPhysicsLinearMixed::ComputeLocalJacobianBB2
		// TODO: share the code (e.g. in a base class)
		// B is constant due to the sub-parametric mapping
		Matrix3R Bn[10], Bs[10];
		for (int A = 0; A < 10; A++)
		{
			Vector3R v;
			for (int n = 0; n < 4; n++)
			{
				MultiIndex B = testFem.mTetMesh->mIJKL + A * 4;
				if (B.Decrement(n))
					v += 0.5 * testFem.mBarycentricJacobians[0].y[n];
			}

			// the normal matrix for node A
			Bn[A] = Matrix3R(v); // diagonal matrix

			// the shear matrix for node A
			Bs[A] = Symm(v);
		}

		Vector3R strainNormal = Bn[3] * u[0] + Bn[6] * u[1] + Bn[8] * u[2] + Bn[9] * u[3];
		Vector3R strainShear = Bs[3] * u[0] + Bs[6] * u[1] + Bs[8] * u[2] + Bs[9] * u[3];
		ASSERT(EqualVector(strainNormal, refStrainNormal));
		ASSERT(EqualVector(strainShear, Vector3R()));

		// Lame parameters
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();

		// compute stress

		// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		Matrix3R stress1 = 2 * mu * refStrain + lambda * refStrain.Trace() * Matrix3R::Identity();
		Matrix3R refStress;
		{
			Vector3R refNormalStress(0.6, 0.2, 0.2);
			refNormalStress.Scale(disp);
			refStress = Matrix3R(refNormalStress);
		}
		ASSERT(EqualMatrix(stress1, refStress));

		// compute nodal forces f = Ku
		EigenVector f = testFem.mStiffnessMatrix * GetEigenVector(u);
		Vector3Array fVec = GetStdVector(f);
		real fx = 0;
		for (uint32 i = 0; i < testFem.GetNumFreeNodes(); i++)
			fx += fVec[i].x;
		ASSERT(EqualEps(fx, 0.4));
	}

	void FemTester::TestPureShearQuadratic()
	{
		// init tetrahedron
		std::vector<Node> testNodes;
		std::vector<Tetrahedron> testTets;
		real x0;
		InitTetrahedron(stdOrder, testNodes, testTets, x0, 3);

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearExplicit testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_LINEAR_ELASTICITY, 2);

		// apply displacement
		real disp = 1.0;
		uint32 n = testFem.GetNumFreeNodes();
		Vector3Array u(n);
		for (uint32 idx = 0; idx < n; idx++)
		{
			real factor = idx == 0 ? 1.0 : 0.5;
			testFem.mDeformedPositions[idx].y += factor * disp;
			u[idx].y = factor * disp;
		}

		// reference strain
		real exy = 0.25;
		Vector3R refStrainShear(0, 0, 2 * exy);
		Matrix3R refStrain(0);
		refStrain(0, 1) = exy;
		refStrain(1, 0) = exy;

		// compute the strain with the formula B * u
		// code taken from FemPhysicsLinearMixed::ComputeLocalJacobianBB2
		// TODO: share the code (e.g. in a base class)
		// B is constant due to the sub-parametric mapping
		Matrix3R Bn[10], Bs[10];
		for (int A = 0; A < 10; A++)
		{
			Vector3R v;
			for (int n = 0; n < 4; n++)
			{
				MultiIndex B = testFem.mTetMesh->mIJKL + A * 4;
				if (B.Decrement(n))
					v += 0.5 * testFem.mBarycentricJacobians[0].y[n];
			}

			// the normal matrix for node A
			Bn[A] = Matrix3R(v); // diagonal matrix

			// the shear matrix for node A
			Bs[A] = Symm(v);
		}

		Vector3R strainNormal = Bn[3] * u[0] + Bn[6] * u[1] + Bn[8] * u[2] + Bn[9] * u[3];
		Vector3R strainShear = Bs[3] * u[0] + Bs[6] * u[1] + Bs[8] * u[2] + Bs[9] * u[3];
		ASSERT(EqualVector(strainNormal, Vector3R()));
		ASSERT(EqualVector(strainShear, refStrainShear));

		// Lame parameters
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();

		// compute stress

		// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		Matrix3R stress1 = 2 * mu * refStrain + lambda * refStrain.Trace() * Matrix3R::Identity();
		real refStressXY = 0.2;
		Matrix3R refStress(0.0, refStressXY, 0.0,
			refStressXY, 0.0, 0.0,
			0.0, 0.0, 0.0);
		ASSERT(EqualMatrix(stress1, refStress));

		// compute nodal forces f = Ku
		EigenVector f = testFem.mStiffnessMatrix * GetEigenVector(u);
		Vector3Array fVec = GetStdVector(f);
		real fy = 0;
		for (uint32 i = 0; i < testFem.GetNumFreeNodes(); i++)
			fy += fVec[i].y;
		ASSERT(EqualEps(fy, 2.0 / 15.0));
	}

	void FemTester::TestPureTranslationQuadraticIncompressible(int pressureOrder)
	{
		// init tetrahedron
		std::vector<Node> testNodes;
		std::vector<Tetrahedron> testTets;
		real x0;
		InitTetrahedron(stdOrder, testNodes, testTets, x0, 3);

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearIncompressible testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_INCOMPRESSIBLE, 2, pressureOrder);

		// apply displacement
		real disp = 1.0;
		uint32 n = testFem.GetNumFreeNodes();
		Vector3Array u(n);
		for (uint32 idx = 0; idx < n; idx++)
		{
			real factor = idx == 0 ? 1.0 : 0.5;
			testFem.mDeformedPositions[idx][0] += factor * disp;
			u[idx].x = factor * disp;
		}

		// reference strain
		real exx = 0.5;
		Vector3R refStrainNormal(exx, 0, 0);
		Matrix3R refStrain(refStrainNormal);
		real ev = exx;

		// compute strain as vstrain = J * u / vol
		EigenVector uEigen = GetEigenVector(u);
		//EigenMatrix constr = testFem.mVolJacobianMatrix * uEigen;
		//if (pressureOrder == 0) // FIXME
		//{
		//	real testEv = constr(0, 0) / testFem.mElementVolumes[0];
		//	ASSERT(testEv == ev);
		//}

		// compute the strain with the formula B * u
		// code taken from FemPhysicsLinearMixed::ComputeLocalJacobianBB2
		// TODO: share the code (e.g. in a base class)
		// B is constant due to the sub-parametric mapping
		//Matrix3R Bn[10], Bs[10];
		//for (int A = 0; A < 10; A++)
		//{
		//	Vector3R v;
		//	for (int n = 0; n < 4; n++)
		//	{
		//		MultiIndex B = testFem.mTetMesh->mIJKL + A * 4;
		//		if (B.Decrement(n))
		//			v += 0.5 * testFem.mBarycentricJacobians[0].y[n];
		//	}

		//	// the normal matrix for node A
		//	Bn[A] = Matrix3R(v); // diagonal matrix

		//	// the shear matrix for node A
		//	Bs[A] = Symm(v);
		//}

		//Vector3R strainNormal = Bn[3] * u[0] + Bn[6] * u[1] + Bn[8] * u[2] + Bn[9] * u[3];
		//Vector3R strainShear = Bs[3] * u[0] + Bs[6] * u[1] + Bs[8] * u[2] + Bs[9] * u[3];
		//ASSERT(EqualVector(strainNormal, refStrainNormal));
		//ASSERT(EqualVector(strainShear, Vector3R()));

		// Lame parameters
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();

		// compute stress

		// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		Matrix3R stress1 = 2 * mu * refStrain + lambda * refStrain.Trace() * Matrix3R::Identity();
		Matrix3R refStress;
		{
			Vector3R refNormalStress(0.6, 0.2, 0.2);
			refNormalStress.Scale(disp);
			refStress = Matrix3R(refNormalStress);
		}
		ASSERT(EqualMatrix(stress1, refStress));

		// compute nodal forces f = Ku
		EigenMatrix Kcondens = testFem.mDeviatoricStiffnessMatrix + testFem.mVolJacobianMatrix.transpose() * testFem.mVolComplianceMatrix.inverse() * testFem.mVolJacobianMatrix;
		EigenVector f = Kcondens * uEigen;
		Vector3Array fVec = GetStdVector(f);
		real fx = 0;
		for (uint32 i = 0; i < testFem.GetNumFreeNodes(); i++)
			fx += fVec[i].x;
		ASSERT(EqualEps(fx, 0.4, 1e-7));
	}

	void FemTester::TestPureShearQuadraticIncompressible(int pressureOrder)
	{
		// init tetrahedron
		std::vector<Node> testNodes;
		std::vector<Tetrahedron> testTets;
		real x0;
		InitTetrahedron(stdOrder, testNodes, testTets, x0, 3);

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearIncompressible testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_INCOMPRESSIBLE, 2, pressureOrder);

		// apply displacement
		real disp = 1.0;
		uint32 n = testFem.GetNumFreeNodes();
		Vector3Array u(n);
		for (uint32 idx = 0; idx < n; idx++)
		{
			real factor = idx == 0 ? 1.0 : 0.5;
			testFem.mDeformedPositions[idx].y += factor * disp;
			u[idx].y = factor * disp;
		}

		// reference strain
		real exy = 0.25;
		Vector3R refStrainShear(0, 0, 2 * exy);
		Matrix3R refStrain(0);
		refStrain(0, 1) = exy;
		refStrain(1, 0) = exy;

		// compute strain as vstrain = J * u / vol
		EigenVector uEigen = GetEigenVector(u);
		EigenMatrix constr = testFem.mVolJacobianMatrix * uEigen;
		real testEv = constr(0, 0) / testFem.mElementVolumes[0];
		ASSERT(testEv == 0);

		// compute the strain with the formula B * u
		// code taken from FemPhysicsLinearMixed::ComputeLocalJacobianBB2
		// TODO: share the code (e.g. in a base class)
		// B is constant due to the sub-parametric mapping
		//Matrix3R Bn[10], Bs[10];
		//for (int A = 0; A < 10; A++)
		//{
		//	Vector3R v;
		//	for (int n = 0; n < 4; n++)
		//	{
		//		MultiIndex B = testFem.mTetMesh->mIJKL + A * 4;
		//		if (B.Decrement(n))
		//			v += 0.5 * testFem.mBarycentricJacobians[0].y[n];
		//	}

		//	// the normal matrix for node A
		//	Bn[A] = Matrix3R(v); // diagonal matrix

		//	// the shear matrix for node A
		//	Bs[A] = Symm(v);
		//}

		//Vector3R strainNormal = Bn[3] * u[0] + Bn[6] * u[1] + Bn[8] * u[2] + Bn[9] * u[3];
		//Vector3R strainShear = Bs[3] * u[0] + Bs[6] * u[1] + Bs[8] * u[2] + Bs[9] * u[3];
		//ASSERT(EqualVector(strainNormal, Vector3R()));
		//ASSERT(EqualVector(strainShear, refStrainShear));

		// Lame parameters
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();

		// compute stress

		// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		Matrix3R stress1 = 2 * mu * refStrain + lambda * refStrain.Trace() * Matrix3R::Identity();
		real refStressXY = 0.2;
		Matrix3R refStress(0.0, refStressXY, 0.0,
			refStressXY, 0.0, 0.0,
			0.0, 0.0, 0.0);
		ASSERT(EqualMatrix(stress1, refStress));

		// compute nodal forces f = Ku
		EigenMatrix Kcondens = testFem.mDeviatoricStiffnessMatrix + testFem.mVolJacobianMatrix.transpose() * testFem.mVolComplianceMatrix.inverse() * testFem.mVolJacobianMatrix;
		EigenVector f = Kcondens * GetEigenVector(u);
		Vector3Array fVec = GetStdVector(f);
		real fy = 0;
		for (uint32 i = 0; i < testFem.GetNumFreeNodes(); i++)
			fy += fVec[i].y;
		ASSERT(EqualEps(fy, 2.0 / 15.0));

		// the volumetric part should not even matter, as we only have shear
		Kcondens = testFem.mDeviatoricStiffnessMatrix;
		f = Kcondens * GetEigenVector(u);
		fVec = GetStdVector(f);
		fy = 0;
		for (uint32 i = 0; i < testFem.GetNumFreeNodes(); i++)
			fy += fVec[i].y;
		ASSERT(EqualEps(fy, 2.0 / 15.0));
	}

	void FemTester::TestVolumeIncrease()
	{
		// init tetrahedron
		std::vector<Node> testNodes(4);
		std::vector<Tetrahedron> testTets(1);
		testNodes[0].pos = FEM_SYSTEM::Vector3R(0, 0, 0);
		testNodes[1].pos = FEM_SYSTEM::Vector3R(1, 0, 0);
		testNodes[2].pos = FEM_SYSTEM::Vector3R(0, 1, 0);
		testNodes[3].pos = FEM_SYSTEM::Vector3R(0, 0, 1);
		testNodes[0].invMass = 0;
		testTets[0].i[0] = 0;
		testTets[0].i[1] = 1;
		testTets[0].i[2] = 2;
		testTets[0].i[3] = 3;

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearExplicit testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_LINEAR_ELASTICITY, 1);

		// apply displacement
		real disp = 0.5;
		testFem.mDeformedPositions[0][0] += disp;
		testFem.mDeformedPositions[1][1] += disp;
		testFem.mDeformedPositions[2][2] += disp;

		// compute displacement vector for node 3 only
		//Vector3R u3 = testFem.GetDeformedPosition(3) - testFem.mReferencePositions[3];
		//Vector3R refDisp;
		//refDisp[axis] = disp;
		//ASSERT(EqualVector(u3, refDisp));

		// compute deformation gradient
		Matrix3R F;
		testFem.ComputeDeformationGradient(0, F);
		Matrix3R refDefGrad(1.5);
		ASSERT(EqualMatrix(F, refDefGrad));

		// compute displacement gradient (we are interested in du_x/dx)
		Matrix3R Du = F - Matrix3R::Identity(); // TODO: testFem.CoputeDisplacementGradient()
		real refDispGrad = 0.5;
		ASSERT(EqualVector(Du.GetDiagonal(), Vector3R(refDispGrad)));

		// compute Cauchy strain using direct formula
		Matrix3R strain = 0.5 * (Du + !Du); // TODO: testFem.ComputeStrain()
		ASSERT(EqualMatrix(strain, Matrix3R(Vector3R(refDispGrad))));
		//real eps = strain(axis, 0);

		//// find the local index of 3
		//int localIdx = -1;
		//for (int i = 0; i < 4; i++)
		//{
		//	if (testFem.mTetMesh->GetGlobalIndex(0, i) == 3)
		//	{
		//		localIdx = i;
		//		break;
		//	}
		//}
		//ASSERT(localIdx >= 0);

		//// compute strain using the direct formula vstrain = B3 * u3 = [ 0 y2 0 y3 0 y1]^T
		//Vector6 strain1; // engineering strain (Voigt vector form)
		//strain1.setZero();
		//if (axis == 0)
		//{
		//	strain1(0) = testFem.mBarycentricJacobians[0].y[localIdx].x * disp;
		//	strain1(4) = testFem.mBarycentricJacobians[0].y[localIdx].y * disp;
		//	strain1(5) = testFem.mBarycentricJacobians[0].y[localIdx].z * disp;
		//	ASSERT(EqualEps(strain1(0), eps) && strain1(4) == 0 && strain1(5) == 0);
		//}
		//else
		//{
		//	strain1(1) = testFem.mBarycentricJacobians[0].y[localIdx].y * disp;
		//	strain1(3) = testFem.mBarycentricJacobians[0].y[localIdx].z * disp;
		//	strain1(5) = testFem.mBarycentricJacobians[0].y[localIdx].x * disp;
		//	ASSERT(strain1(1) == 0 && strain1(4) == 0 && strain1(5) == 2 * eps);
		//}

		//// compute strain Jacobian w.r.t to nodal positions/displacements
		//Matrix3R Bn[4], Bs[4];
		//testFem.ComputeStrainJacobian(0, Bn, Bs);

		//// compute strain using the formula vstrain = B3 * u3
		//SymTensor strain2; // engineering strain (Voigt vector form)
		//strain2.diag = Bn[localIdx] * u3;
		//strain2.shear = Bs[localIdx] * u3;
		//if (axis == 0)
		//{
		//	ASSERT(EqualVector(strain2.diag, Vector3R(eps, 0, 0)));
		//	ASSERT(EqualVector(strain2.shear, Vector3R(0, 0, 0)));
		//}
		//else
		//{
		//	ASSERT(EqualVector(strain2.diag, Vector3R(0, 0, 0)));
		//	ASSERT(EqualVector(strain2.shear, Vector3R(0, 0, 2 * eps)));
		//}

		// Lame parameters
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();

		// compute stress

		// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		Matrix3R stress1 = 2 * mu * strain + lambda * strain.Trace() * Matrix3R::Identity();
		Matrix3R refStress(1.0);
		//if (axis == 0)
		//{
		//	Vector3R refNormalStress(0.6, 0.2, 0.2);
		//	refNormalStress.Scale(disp);
		//	refStress = Matrix3R(refNormalStress);
		//}
		//else
		//{
		//	real refStressXY = 0.2 * disp;
		//	refStress = Matrix3R(0.0, refStressXY, 0.0,
		//		refStressXY, 0.0, 0.0,
		//		0.0, 0.0, 0.0);
		//}
		ASSERT(EqualMatrix(stress1, refStress));

		// formula 2 (using elasticity matrix and vector notation): vstress = E * vstrain
		SymTensor stress2;
		stress2.diag = testFem.mNormalElasticityMatrix * Vector3R(0.5);
		stress2.shear = mu * Vector3R();
		//if (axis == 0)
		//{
			ASSERT(EqualVector(stress2.diag, Vector3R(1.0)));
			ASSERT(EqualVector(stress2.shear, Vector3R())); // zero
		//}
		//else
		//{
		//	ASSERT(EqualVector(stress2.diag, Vector3R()));
		//	ASSERT(EqualVector(stress2.shear, Vector3R(0, 0, 0.2 * disp)));
		//}

		// compute the total force at node 3
		Matrix3R X(testFem.mBarycentricJacobians[0].y[1], testFem.mBarycentricJacobians[0].y[2], testFem.mBarycentricJacobians[0].y[3]);
		Matrix3R forces = -testFem.mElementVolumes[0] * stress1 * X;
		Vector3R f[4];
		f[0] = forces(0) + forces(1) + forces(2);
		f[1] = -forces(0);
		f[2] = -forces(1);
		f[3] = -forces(2);
		real refForce = 1.0 / 6.0;
		ASSERT(EqualEps(f[1].x, refForce) && EqualEps(f[2].y, refForce) && EqualEps(f[3].z, refForce));

		// check that the nodal forces are the same
		uint32 n = testFem.GetNumFreeNodes() * 3;
		EigenVector u(n);
		u.setZero();
		u(0) = disp;
		u(4) = disp;
		u(8) = disp;
		EigenVector fEigen = testFem.mStiffnessMatrix * u;
		Vector3Array fVec = GetStdVector(fEigen);
		int numBCs = 1;
		for (uint32 i = 0; i < testFem.GetNumLocalNodes(); i++)
		{
			int globalIdx = testFem.mTetMesh->GetGlobalIndex(0, i);
			if (globalIdx < numBCs)
				continue;
			ASSERT(EqualVector(fVec[globalIdx - numBCs], f[i]));
		}
	}

	void FemTester::TestVolumeIncreaseIncompressible(int pressureOrder)
	{
		// init tetrahedron
		std::vector<Node> testNodes(4);
		std::vector<Tetrahedron> testTets(1);
		testNodes[0].pos = FEM_SYSTEM::Vector3R(0, 0, 0);
		testNodes[1].pos = FEM_SYSTEM::Vector3R(1, 0, 0);
		testNodes[2].pos = FEM_SYSTEM::Vector3R(0, 1, 0);
		testNodes[3].pos = FEM_SYSTEM::Vector3R(0, 0, 1);
		testNodes[0].invMass = 0;
		testTets[0].i[0] = 0;
		testTets[0].i[1] = 1;
		testTets[0].i[2] = 2;
		testTets[0].i[3] = 3;

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearIncompressible testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_LINEAR_ELASTICITY, 1, pressureOrder);

		// apply displacement
		real disp = 0.5;
		testFem.mDeformedPositions[0][0] += disp;
		testFem.mDeformedPositions[1][1] += disp;
		testFem.mDeformedPositions[2][2] += disp;

		// compute deformation gradient
		Matrix3R F;
		testFem.ComputeDeformationGradient(0, F);
		Matrix3R refDefGrad(1.5);
		ASSERT(EqualMatrix(F, refDefGrad));

		// compute displacement gradient (we are interested in du_x/dx)
		Matrix3R Du = F - Matrix3R::Identity(); // TODO: testFem.CoputeDisplacementGradient()
		real refDispGrad = 0.5;
		ASSERT(EqualVector(Du.GetDiagonal(), Vector3R(refDispGrad)));

		// compute Cauchy strain using direct formula
		Matrix3R strain = 0.5 * (Du + !Du); // TODO: testFem.ComputeStrain()
		ASSERT(EqualMatrix(strain, Matrix3R(Vector3R(refDispGrad))));

		real ev = strain.Trace(); // volumetric strain
		Matrix3R deviatoricStrain = strain;
		deviatoricStrain(0, 0) -= ev / 3.0;
		deviatoricStrain(1, 1) -= ev / 3.0;
		deviatoricStrain(2, 2) -= ev / 3.0;

		// compute volumetric strain
		Vector3R u1(disp, 0, 0);
		Vector3R u2(0, disp, 0);
		Vector3R u3(0, 0, disp);
		real strain2vol = dot(testFem.mBarycentricJacobians[0].y[1], u1) +
			dot(testFem.mBarycentricJacobians[0].y[2], u2) +
			dot(testFem.mBarycentricJacobians[0].y[3], u3);
		ASSERT(strain2vol == ev);

		Matrix3R Bn[4], Bs[4];
		testFem.ComputeStrainJacobian(0, Bn, Bs);

		// project the normal matrices
		Matrix3R P(2. / 3., -1. / 3., -1. / 3.,
			-1. / 3., 2. / 3., -1. / 3.,
			-1. / 3., -1. / 3., 2. / 3.);

		// compute deviatoric strain
		Matrix3R Bn1 = P * Bn[1];
		Matrix3R Bn2 = P * Bn[2];
		Matrix3R Bn3 = P * Bn[3];
		Vector3R strain2diag = Bn1 * u1 + Bn2 * u2 + Bn3 * u3;
		Vector3R strain2shear = Bs[1] * u1 + Bs[2] * u2 + Bs[3] * u3;
		ASSERT(EqualVector(strain2diag, Vector3R(0, 0, 0)));
		ASSERT(EqualVector(strain2shear, Vector3R(0, 0, 0)));

		// compute volumetric strain as vstrain = J * u / vol
		Vector3Array u(3);
		u[0] = u1;
		u[1] = u2;
		u[2] = u3;
		EigenMatrix constr = testFem.mVolJacobianMatrix * GetEigenVector(u);
		// TODO: is there an integration factor of 2 somewhere?
		if (pressureOrder == 0)
		{
			real testEv = constr(0, 0) / testFem.mElementVolumes[0];
			ASSERT(testEv == ev);
		}

		// elastic parameters
		real bulkModulus = testFem.GetBulkModulus();
		real mu = testFem.GetShearModulus();

		// compute pressure
		real pressure = bulkModulus * ev;

		// compute deviatoric stress
		Matrix3R deviatoricStress = 2 * mu * deviatoricStrain;

		// compute total stress
		Matrix3R stress = deviatoricStress + pressure * Matrix3R::Identity();
		Vector3R refNormalStress(1.0);
		ASSERT(EqualMatrix(stress, Matrix3R(refNormalStress)));

		// compute the total force at node 3
		Matrix3R X(testFem.mBarycentricJacobians[0].y[1], testFem.mBarycentricJacobians[0].y[2], testFem.mBarycentricJacobians[0].y[3]);
		Matrix3R forces = -testFem.mElementVolumes[0] * stress * X;
		Vector3R f[4];
		f[0] = forces(0) + forces(1) + forces(2);
		f[1] = -forces(0);
		f[2] = -forces(1);
		f[3] = -forces(2);
		real refForce = 1.0 / 6.0;
		ASSERT(EqualEps(f[1].x, refForce) && EqualEps(f[2].y, refForce) && EqualEps(f[3].z, refForce));

		// check that the nodal forces are the same
		uint32 n = testFem.GetNumFreeNodes() * 3;
		EigenMatrix Kcondens = testFem.mDeviatoricStiffnessMatrix + testFem.mVolJacobianMatrix.transpose() * testFem.mVolComplianceMatrix.inverse() * testFem.mVolJacobianMatrix;
		EigenVector fEigen = Kcondens * GetEigenVector(u);
		Vector3Array fVec = GetStdVector(fEigen);
		int numBCs = 1;
		for (uint32 i = 0; i < testFem.GetNumLocalNodes(); i++)
		{
			int globalIdx = testFem.mTetMesh->GetGlobalIndex(0, i);
			if (globalIdx < numBCs)
				continue;
			ASSERT(EqualVector(fVec[globalIdx - numBCs], f[i], 1e-7));
		}

		// the deviatoric part shouldn't matter
		Kcondens = testFem.mVolJacobianMatrix.transpose() * testFem.mVolComplianceMatrix.inverse() * testFem.mVolJacobianMatrix;
		fEigen = Kcondens * GetEigenVector(u);
		fVec = GetStdVector(fEigen);
		for (uint32 i = 0; i < testFem.GetNumLocalNodes(); i++)
		{
			int globalIdx = testFem.mTetMesh->GetGlobalIndex(0, i);
			if (globalIdx < numBCs)
				continue;
			ASSERT(EqualVector(fVec[globalIdx - numBCs], f[i], 1e-7));
		}
	}
	
	void FemTester::TestVolumeIncreaseQuadratic()
	{
		// init tetrahedron
		std::vector<Node> testNodes(4);
		std::vector<Tetrahedron> testTets(1);
		testNodes[0].pos = FEM_SYSTEM::Vector3R(0, 0, 0);
		testNodes[1].pos = FEM_SYSTEM::Vector3R(1, 0, 0);
		testNodes[2].pos = FEM_SYSTEM::Vector3R(0, 1, 0);
		testNodes[3].pos = FEM_SYSTEM::Vector3R(0, 0, 1);
		testNodes[0].invMass = 0;
		testTets[0].i[0] = 0;
		testTets[0].i[1] = 1;
		testTets[0].i[2] = 2;
		testTets[0].i[3] = 3;

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearExplicit testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_LINEAR_ELASTICITY, 2);

		// apply scaling
		real scale = 1.5;
		for (uint32 i = 0; i < testFem.GetNumFreeNodes(); i++)
			testFem.mDeformedPositions[i] *= scale;

		// compute stress
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();
		Matrix3R strain(Vector3R(0.5));

		// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		Matrix3R stress1 = 2 * mu * strain + lambda * strain.Trace() * Matrix3R::Identity();
		Matrix3R refStress(1.0);
		ASSERT(EqualMatrix(stress1, refStress));

		// check that the nodal forces are the same
		uint32 n = testFem.GetNumFreeNodes();
		Vector3Array uVec(n);
		for (uint32 i = 0; i < n; i++)
		{
			uVec[i] = testFem.mDeformedPositions[i] - testFem.mReferencePositions[i + 1];
		}
		EigenVector fEigen = testFem.mStiffnessMatrix * GetEigenVector(uVec);
		Vector3Array fVec = GetStdVector(fEigen);
		Vector3R fTotal;
		for (uint32 i = 0; i < n; i++)
		{
			fTotal += fVec[i];
		}
		ASSERT(EqualVector(fTotal, Vector3R(1.0 / 18.0))); // why it is like this, I don't know
	}

	void FemTester::TestVolumeIncreaseQuadraticIncompressible(int pressureOrder)
	{
		// init tetrahedron
		std::vector<Node> testNodes(4);
		std::vector<Tetrahedron> testTets(1);
		testNodes[0].pos = FEM_SYSTEM::Vector3R(0, 0, 0);
		testNodes[1].pos = FEM_SYSTEM::Vector3R(1, 0, 0);
		testNodes[2].pos = FEM_SYSTEM::Vector3R(0, 1, 0);
		testNodes[3].pos = FEM_SYSTEM::Vector3R(0, 0, 1);
		testNodes[0].invMass = 0;
		testTets[0].i[0] = 0;
		testTets[0].i[1] = 1;
		testTets[0].i[2] = 2;
		testTets[0].i[3] = 3;

		// create linear elasticity simulator object
		real youngModulus = 1;
		real poissonRatio = 0.25;
		FemPhysicsLinearIncompressible testFem(testTets, testNodes, youngModulus, poissonRatio, 0, IT_INCOMPRESSIBLE, 2, pressureOrder);

		// apply scaling
		real scale = 1.5;
		for (uint32 i = 0; i < testFem.GetNumFreeNodes(); i++)
			testFem.mDeformedPositions[i] *= scale;

		// compute stress
		real mu = testFem.GetShearModulus();
		real lambda = testFem.GetLameFirstParam();
		Matrix3R strain(Vector3R(0.5));

		// formula 1: stress = 2 * mu * strain + lambda * trace(strain) * identity
		Matrix3R stress1 = 2 * mu * strain + lambda * strain.Trace() * Matrix3R::Identity();
		Matrix3R refStress(1.0);
		ASSERT(EqualMatrix(stress1, refStress));

		// check that the nodal forces are the same
		uint32 n = testFem.GetNumFreeNodes();
		Vector3Array uVec(n);
		for (uint32 i = 0; i < n; i++)
		{
			uVec[i] = testFem.mDeformedPositions[i] - testFem.mReferencePositions[i + 1];
		}
		EigenMatrix Kcondens = testFem.mDeviatoricStiffnessMatrix + testFem.mVolJacobianMatrix.transpose() * testFem.mVolComplianceMatrix.inverse() * testFem.mVolJacobianMatrix;
		EigenVector fEigen = Kcondens * GetEigenVector(uVec);
		Vector3Array fVec = GetStdVector(fEigen);
		Vector3R fTotal;
		for (uint32 i = 0; i < n; i++)
		{
			fTotal += fVec[i];
		}
		ASSERT(EqualVector(fTotal, Vector3R(1.0 / 18.0))); // why it is like this (and not 1/6), I don't know
	}
} // namespace FEM_SYSTEM

#endif // USE_DOUBLE_FOR_FEM