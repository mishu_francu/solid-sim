// Mihai Francu
// 12 Oct 2015

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Demo/Particles/3D/FemSystem.h>
#include <Physics/FEM/FemPhysicsLinearElasticity.h>
#include <Physics/FEM/FemPhysicsLinearMixed.h>
#include <Physics/FEM/FemPhysicsLinearIncompressible.h>
#include <Physics/FEM/FemPhysicsCorotationalElasticity.h>
#include <Physics/FEM/ElasticEnergy.h>
//#include <Physics/FEM/FemPhysicsAnalyticCantileverNonLinearElasticity.h>
//#include <Physics/FEM/FemPhysicsAnalyticCantileverLinearElasticity.h>
#include <Physics/FEM/FemPhysicsMixed.h>
#include <Physics/FEM/FemIO.h>
#include <Physics/FEM/FemCollisions.h>
#include <Math/Matrix.h>
#include <Geometry/Assets.h>
#include <Engine/xml.h>

#pragma warning( disable : 4267) // for size_t-uint conversions

using namespace Geometry;

namespace FEM_SYSTEM
{
	Mesh CreateBoxMesh()
	{
		Mesh boxMesh;
		for (int i = 0; i < 3; i++) // the current axis
		{
			for (int j = 0; j < 2; j++) // the direction of the axis
			{
				for (int k = 0; k < 2; k++) // the current triangle
				{
					Vector3 a, b, c;
					float sgnDir = -1.f + 2 * j;
					float sgnTr = -1.f + 2 * k;
					Vector3 n;
					a[i] = sgnDir; b[i] = sgnDir; c[i] = sgnDir;
					n[i] = sgnDir;
					int axis1 = (i + 1) % 3;
					int axis2 = (i + 2) % 3;
					if (sgnDir < 0)
					{
						a[axis1] = -1; a[axis2] = -1;
						b[axis1] = -sgnTr; b[axis2] = 1;
						c[axis1] = 1; c[axis2] = sgnTr;
					}
					else
					{
						a[axis1] = -1; a[axis2] = -1;
						b[axis1] = 1; b[axis2] = sgnTr;
						c[axis1] = -sgnTr; c[axis2] = 1;
					}

					boxMesh.vertices.push_back(a);
					boxMesh.vertices.push_back(b);
					boxMesh.vertices.push_back(c);

					boxMesh.normals.push_back(n);
					boxMesh.normals.push_back(n);
					boxMesh.normals.push_back(n);

					boxMesh.indices.push_back(boxMesh.vertices.size() - 1);
					boxMesh.indices.push_back(boxMesh.vertices.size() - 2);
					boxMesh.indices.push_back(boxMesh.vertices.size() - 3);
				}
			}
		}
		return boxMesh;
	}

FemSystem::FemSystem(const std::string& name)
#ifdef USE_ALEMBIC
	: mAbcArchive(Alembic::AbcCoreOgawa::WriteArchive(), name + ".abc")
	, mAbcMesh(nullptr)
#endif
{
	femSystemDrawing = nullptr;
	femPhysics = nullptr;

#ifdef USE_ALEMBIC
	mBoxMesh = CreateBoxMesh();
#endif
}

FemSystem::~FemSystem()
{
	if (femSystemDrawing)
		delete femSystemDrawing;

	if (femPhysics)
		delete femPhysics;

	this->Clear();

#ifdef USE_ALEMBIC
	if (mAbcMesh != nullptr)
	{
		delete mAbcMesh;
	}
	for (size_t i = 0; i < mCollMesh.size(); i++)
	{
		if (mCollMesh[i] != nullptr)
		{
			delete mCollMesh[i];
		}
	}
#endif
}

void FemSystem::Init(FemConfig& config)
{
	UninvertElements();
	
	Printf("Num tets: %d\n", tets.size());
	Printf("Num nodes: %d\n", nodes.size());
	if (config.mType == MT_LINEAR_ELASTICITY)
		femPhysics = new FemPhysicsLinearElasticity(tets, nodes, config);
	else if (config.mType == MT_INCOMPRESSIBLE_LINEAR_ELASTICITY
		|| config.mType == MT_INCOMPRESSIBLE_COROTATIONAL_ELASTICITY)
		femPhysics = new FemPhysicsLinearIncompressible(tets, nodes, config);
	else if (config.mType == MT_CONSTRAINT_LINEAR)
		femPhysics = new FemPhysicsLinearMixed(tets, nodes, config);
	else if (config.mType == MT_COROTATIONAL_ELASTICITY)
		femPhysics = new FemPhysicsCorotationalElasticity(tets, nodes, config);
	else if (config.mType == MT_ANALYTIC_CANTILEVER_NONLINEAR_ELASTICITY)
		femPhysics = nullptr;// new FemPhysicsAnalyticCantileverNonLinearElasticity(tets, nodes, config);
	else if (config.mType == MT_ANALYTIC_CANTILEVER_LINEAR_ELASTICITY)
		femPhysics = nullptr;// new FemPhysicsAnalyticCantileverLinearElasticity(tets, nodes, config);
	else if (config.mType == MT_NONLINEAR_ELASTICITY) // matrix free implementation (linear and nonlinear together)
		femPhysics = new FemPhysicsMatrixFree(tets, nodes, config);
	else if (config.mType == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
		femPhysics = new FemPhysicsMixed(tets, nodes, config);

	femSystemDrawing = new FemDrawing(tets, nodes, *femPhysics);
}


bool FemSystem::LoadFromTetFile(const char* path, real scale, const Vector3R& offset)
{
	FILE *ptr_file;
	char buf[1000];

	errno_t ret = fopen_s(&ptr_file, path, "r");
	if (ret != 0)
	{
		printf("Couldn't open file %s\n", path);
		return false;
	}
	const int STATE_READ = 0;
	const int STATE_VOL = 1;
	const int STATE_ELEM = 2;
	const int STATE_POINTS = 3;
	const int STATE_POINT = 4;
	int state = STATE_READ;
	int nod = 0;
	while (fgets(buf, 1000, ptr_file) != NULL)
	{
		char* ptr = buf;
		char* word = buf;

		switch (state)
		{
			case STATE_READ:
			{
				if (strncmp(buf, "num_vertices", 12) == 0)
				{
					word = ptr + 12;
					int n = atoi(word);
					nodes.resize(n);
				}
				else if (strncmp(buf, "VERTICES", 8) == 0)
				{
					state = STATE_POINT;
				}
				else if (strncmp(buf, "TETRAS", 6) == 0)
				{
					state = STATE_ELEM;
				}
				break;
			}
			case STATE_ELEM:
			{
				int indx[4];
				int counter = 0;
				while (ptr = strchr(word, ' '))
				{
					*ptr = '\0';

					int x = atoi(word);
					indx[counter] = x;
					counter++;
					word = ptr + 1;
				}
				if (counter == 0)
				{
					state = STATE_READ;
					break;
				}
				AddTetrahedron(indx[0], indx[1], indx[2], indx[3]);
				break;
			}
			case STATE_POINT:
			{
				Vector3R pos;
				int counter = 0;
				while (ptr = strchr(word, ' '))
				{
					if (ptr == word)
					{
						word++;
						continue;
					}
					*ptr = '\0';
					real x = (real)atof(word);
					pos[counter] = x;
					counter++;
					word = ptr + 1;
				}
				if (counter == 0)
				{
					state = STATE_ELEM;
					break;
				}
				nodes.at(nod++).pos = scale * pos + offset;
				break;
			}

		}
	}

	fclose(ptr_file);
	return true;
}

bool FemSystem::LoadFromTet1File(const char* path, real scale, const Vector3R& offset)
{
	return IO::LoadFromTet1File(path, scale, offset, nodes, tets);
}

bool FemSystem::LoadFromNodeEleFile(const char* path, real scale, const Vector3R& offset)
{
	size_t oneIndexed = 0;
	bool firstLine = true;

	// First read the .node file
	FILE *ptr_file;
	char buf[1000];

	errno_t ret = fopen_s(&ptr_file, path, "r");
	if (ret != 0)
	{
		Printf("Couldn't open file %s\n", path);
		return false;
	}

	// Skip the first line
	fgets(buf, 1000, ptr_file);

	int nod = 0;
	while (fgets(buf, 1000, ptr_file) != NULL)
	{
		char* ptr = buf;
		// eat any initial whitespaces
		while (ptr[0] == ' ') ptr = ptr + 1;

		// Skip comments
		if (ptr[0] == '#')
		{
			continue;
		}

		// format: i x y z
		char* e = strchr(ptr, ' ');
		*e = '\0';
		size_t idx = (size_t)atof(ptr);

		if (firstLine) {
			//If the index in the first line is 1, the file is 1-indexed.
			if (idx == 1) {
				oneIndexed = 1;
			}
			firstLine = false;
		}

		idx -= oneIndexed;

		Vector3R pos;
		for (size_t k = 0; k < 2; k++)
		{
			ptr = e + 1;
			// eat prefixed-whitespace
			while (ptr[0] == ' ') ptr = ptr + 1;

			e = strchr(ptr, ' ');
			*e = '\0';
			real v = (real)atof(ptr);
			pos[k] = v;
		}

		ptr = e + 1;
		pos[2] = (real)atof(ptr);

		nodes.push_back(Node());
		nodes[nod].pos = scale * pos + offset;
		nod = nod + 1;
	}

	fclose(ptr_file);

	// Next read the .ele file
	std::string pathEle = std::string(path);
	pathEle.pop_back();
	pathEle.pop_back();
	pathEle.pop_back();
	pathEle.pop_back();
	pathEle.append("ele");
	ret = fopen_s(&ptr_file, pathEle.c_str(), "r");
	if (ret != 0)
	{
		Printf("Couldn't open file %s\n", path);
		return false;
	}

	// Skip the first line
	fgets(buf, 1000, ptr_file);

	while (fgets(buf, 1000, ptr_file) != NULL)
	{
		char* ptr = buf;
		// eat any initial whitespaces
		while (ptr[0] == ' ') ptr = ptr + 1;

		// Skip comments
		if (ptr[0] == '#')
		{
			continue;
		}

		// format: i j1 j2 j3 j4
		char* e = strchr(ptr, ' ');
		*e = '\0';
		size_t idx = (size_t)atoi(ptr) - oneIndexed;


		size_t idxs[4];

		for (size_t k = 0; k < 3; k++)
		{
			ptr = e + 1;
			// eat prefixed whitespace
			while (ptr[0] == ' ') ptr = ptr + 1;

			e = strchr(ptr, ' ');
			*e = '\0';
			idxs[k] = (size_t)atoi(ptr) - oneIndexed;
		}

		ptr = e + 1;
		idxs[3] = (size_t)atoi(ptr) - oneIndexed;

		AddTetrahedron(idxs[0], idxs[1], idxs[2], idxs[3]);
	}

	fclose(ptr_file);

	return true;
}


// duplicated code from SoftFEM class
struct Face
{
	uint16 i, j, k;
	int elem;
	int face;

	Face(uint16 a, uint16 b, uint16 c, int e, int f) : elem(e), face(f)
	{
		i = min(a, min(b, c));
		k = max(a, max(b, c));
		j = a + b + c - i - k;
	}
};

bool CompareFaces(const Face& t1, const Face& t2)
{
	if (t1.i < t2.i)
		return true;
	if (t1.i == t2.i)
	{
		if (t1.j < t2.j)
			return true;
		if (t1.j == t2.j && t1.k < t2.k)
			return true;
	}
	return false;
}

// initially the boundary mesh is built in meter units
void FemSystem::BuildBoundaryMesh()
{
#ifdef USE_ALEMBIC
	// FIXME
	FemCollision* coll = femPhysics->GetCollision();
	if (coll)
	{
		mCollMesh.resize(coll->GetNumCollidables());
		std::fill(mCollMesh.begin(), mCollMesh.end(), nullptr);
	}
#endif

	std::vector<Face> faces;
	for (size_t ii = 0; ii < tets.size(); ii++)
	{
		uint16 i = tets[ii].idx[0];
		uint16 j = tets[ii].idx[1];
		uint16 k = tets[ii].idx[2];
		uint16 l = tets[ii].idx[3];

		faces.push_back(Face(j, k, l, ii, 0));
		faces.push_back(Face(i, k, l, ii, 1));
		faces.push_back(Face(i, j, l, ii, 2));
		faces.push_back(Face(i, j, k, ii, 3));
	}

	std::sort(faces.begin(), faces.end(), CompareFaces);

	std::vector<Face> boundary;
	std::vector<uint16> boundaryVertices;
	for (size_t i = 0; i < faces.size(); i++)
	{
		if (i < faces.size() - 1 &&
			faces[i].i == faces[i + 1].i &&
			faces[i].j == faces[i + 1].j &&
			faces[i].k == faces[i + 1].k)
		{
			// if the next triangle is the same then this is clearly not a boundary triangle
			i++;
			continue;
		}
		boundary.push_back(faces[i]);
		boundaryVertices.push_back(faces[i].i);
		boundaryVertices.push_back(faces[i].j);
		boundaryVertices.push_back(faces[i].k);
	}

	// identify unique boundary vertices
	std::sort(boundaryVertices.begin(), boundaryVertices.end());

	// create vertex buffer and retain mappings
	std::vector<Vector3> vertices;
	mNodesToBoundaryMap.resize(nodes.size());
	std::fill(mNodesToBoundaryMap.begin(), mNodesToBoundaryMap.end(), -1);
	mBoundaryToNodesMap.resize(boundaryVertices.size());
	std::fill(mBoundaryToNodesMap.begin(), mBoundaryToNodesMap.end(), -1);
	for (size_t i = 0; i < boundaryVertices.size(); i++)
	{
		if (i > 0 && boundaryVertices[i] == boundaryVertices[i - 1])
			continue;
		vertices.push_back(DoubleToFloat(nodes[boundaryVertices[i]].pos));
		int idx = vertices.size() - 1;
		mNodesToBoundaryMap[boundaryVertices[i]] = idx;
		mBoundaryToNodesMap[idx] = boundaryVertices[i];
	}

	// generate mesh
	mBoundaryMesh.vertices = vertices;
	const std::vector<int>& map = mNodesToBoundaryMap; // map from boundary mesh vertex indices to node indices

	// add the triangles
	mBoundaryMesh.indices.clear();
	mBoundaryToElementsMap.resize(boundary.size());
	for (size_t idx = 0; idx < boundary.size(); idx++)
	{
		// use the 4th vertex in the tet to identify which way is inside (for triangle winding)
		int ii = boundary[idx].elem;
		uint16 i = tets[ii].idx[0];
		uint16 j = tets[ii].idx[1];
		uint16 k = tets[ii].idx[2];
		uint16 l = tets[ii].idx[3];

		mBoundaryToElementsMap[idx] = ii;
		if (boundary[idx].face == 0)
		{
			const Vector3 v1 = DoubleToFloat(nodes[j].pos);
			const Vector3 v2 = DoubleToFloat(nodes[k].pos);
			const Vector3 v3 = DoubleToFloat(nodes[l].pos);
			const Vector3 v4 = DoubleToFloat(nodes[i].pos);
			Vector3 n = cross(v2 - v1, v3 - v1);
			bool flip = n.Dot(v4 - v1) > 0;
			mBoundaryMesh.AddTriangle(map[j], map[k], map[l], flip);
		}
		if (boundary[idx].face == 1)
		{
			const Vector3 v1 = DoubleToFloat(nodes[i].pos);
			const Vector3 v2 = DoubleToFloat(nodes[k].pos);
			const Vector3 v3 = DoubleToFloat(nodes[l].pos);
			const Vector3 v4 = DoubleToFloat(nodes[j].pos);
			Vector3 n = cross(v2 - v1, v3 - v1);
			bool flip = n.Dot(v4 - v1) > 0;
			mBoundaryMesh.AddTriangle(map[i], map[k], map[l], flip);
		}
		if (boundary[idx].face == 2)
		{
			const Vector3 v1 = DoubleToFloat(nodes[i].pos);
			const Vector3 v2 = DoubleToFloat(nodes[j].pos);
			const Vector3 v3 = DoubleToFloat(nodes[l].pos);
			const Vector3 v4 = DoubleToFloat(nodes[k].pos);
			Vector3 n = cross(v2 - v1, v3 - v1);
			bool flip = n.Dot(v4 - v1) > 0;
			mBoundaryMesh.AddTriangle(map[i], map[j], map[l], flip);
		}
		if (boundary[idx].face == 3)
		{
			const Vector3 v1 = DoubleToFloat(nodes[i].pos);
			const Vector3 v2 = DoubleToFloat(nodes[j].pos);
			const Vector3 v3 = DoubleToFloat(nodes[k].pos);
			const Vector3 v4 = DoubleToFloat(nodes[l].pos);
			Vector3 n = cross(v2 - v1, v3 - v1);
			bool flip = n.Dot(v4 - v1) > 0;
			mBoundaryMesh.AddTriangle(map[i], map[j], map[k], flip);
		}
	}

	mBoundaryMesh.ComputeNormals();
}

void FemSystem::UpdateBoundaryMesh(HeatMapType heatMapType)
{
	std::vector<real> scalarField;
	real minPressure = std::numeric_limits<real>::max();
	real maxPressure = std::numeric_limits<real>::min();
	if (heatMapType != HMT_NONE)
	{
		mBoundaryMesh.pressures.resize(nodes.size());
		mBoundaryMesh.colors.resize(nodes.size());

		scalarField.resize(nodes.size());	
		if (heatMapType == HMT_VOL_STRAIN)
			femPhysics->GetNodeVolumetricStrains(scalarField);
		else if (heatMapType == HMT_PRESSURE)
			femPhysics->GetNodePressures(scalarField);
		else if (heatMapType == HMT_VON_MISES)
			femPhysics->GetNodeVonMisesStresses(scalarField);
		for (uint32 i = 0; i < nodes.size(); i++)
		{
			{
				real p = scalarField[i];

				if (p < minPressure) {
					minPressure = p;
				}
				if (p > maxPressure) {
					maxPressure = p;
				}
			}
		}
	}

	for (size_t i = 0; i < nodes.size(); i++)
	{
		Vector3R v = ((const FemPhysicsBase*)femPhysics)->GetDeformedPosition(i);
		nodes[i].pos = v; // hack for drawing tetrahedra
		//v.Scale(100); // convert from m to cm
		if (mNodesToBoundaryMap[i] >= 0)
		{
			mBoundaryMesh.vertices[mNodesToBoundaryMap[i]] = DoubleToFloat(v);
			if (heatMapType != HMT_NONE)
			{
				real p = scalarField[i];
				real normalizedPressure = (p - minPressure) / (maxPressure - minPressure);

				Vector3 col = GetColour(normalizedPressure, 0, 1);

				mBoundaryMesh.pressures[mNodesToBoundaryMap[i]] = normalizedPressure;
				mBoundaryMesh.colors[mNodesToBoundaryMap[i]] = col;
			}
		}
	}
	mBoundaryMesh.ComputeNormals();
}

void FemSystem::ExportToNodeEleFile(std::fstream& outNode, std::fstream& outEle)
{
	// TODO it currently only supports meshes stored as TetrahedralMesh and uses GetInitialPosition

	// For now always export with 1-indexed indices
	int prefix_index = 1;

	// NODE file
	outNode << std::to_string(femPhysics->GetNumNodes()) + " 3 0 0\n";

	for (uint32 i = 0; i < femPhysics->GetNumNodes(); i++)
	{
		outNode << std::to_string(i + prefix_index);
		outNode << " ";
		outNode << std::to_string(femPhysics->GetInitialPosition(i).x);
		outNode << " ";
		outNode << std::to_string(femPhysics->GetInitialPosition(i).y);
		outNode << " ";
		outNode << std::to_string(femPhysics->GetInitialPosition(i).z);
		outNode << "\n";
	}
	//outNode << "# Generated by solid-sim\n";

	// ELE file
	outEle << std::to_string(femPhysics->GetNumElements()) + " 4 0\n";

	for (uint32 i = 0; i < femPhysics->GetNumElements(); i++)
	{
		outEle << std::to_string(i + prefix_index);
		outEle << " ";
		outEle << std::to_string(tets[i].idx[0] + prefix_index);
		outEle << " ";
		outEle << std::to_string(tets[i].idx[1] + prefix_index);
		outEle << " ";
		outEle << std::to_string(tets[i].idx[2] + prefix_index);
		outEle << " ";
		outEle << std::to_string(tets[i].idx[3] + prefix_index);
		outEle << "\n";
	}
	//outEle << "# Generated by solid-sim\n";
}

void FemSystem::ExportToVTKHeatMap(std::fstream& outfile)
{
	IO::ExportToVTKHeatMap(outfile, femPhysics);
}

void FemSystem::ExportToVTKFile(std::fstream& outfile)
{
	IO::ExportToVTKFile(outfile, femPhysics);
}

void FemSystem::ExportToAlembic(bool exportNormals)
{
#ifdef USE_ALEMBIC
	// create the time sampling object
	Alembic::Abc::TimeSamplingPtr abcTimeSampling(new Alembic::Abc::TimeSampling(1.0 / 60.0, 0.0));

	if (mAbcMesh == nullptr)
	{
		// create the transform node
		OXform abcXform(mAbcArchive.getTop(), "xform", abcTimeSampling);

		// set the transform
		OXformSchema& schema = abcXform.getSchema();
		XformSample xs;
		//xs.setTranslation(V3f(0, 0, 0));
		//xs.setRotation(V3f(1, 0, 0), 0);
		xs.setScale(V3f(100, 100, 100));
		schema.set(xs);

		// create the mesh
		mAbcMesh = new OPolyMesh(abcXform, "mesh", abcTimeSampling);
	}

	// set the mesh
	const auto& mesh = mVisualMesh.vertices.size() != 0 ? mVisualMesh :
		mBBBoundaryMesh.vertices.size() != 0 ? mBBBoundaryMesh : mBoundaryMesh;
	OPolyMeshSchema &abcMeshSchema = mAbcMesh->getSchema();
	size_t numFaces = mesh.indices.size() / 3;
	std::vector<int> counts(numFaces);
	std::fill(counts.begin(), counts.end(), 3);
	if (exportNormals)
	{
		OPolyMeshSchema::Sample mesh_samp(
			P3fArraySample((const V3f *)&mesh.vertices[0], mesh.vertices.size()),
			Int32ArraySample((const int*)&mesh.indices[0], mesh.indices.size()),
			Int32ArraySample(&counts[0], numFaces),
			OV2fGeomParam::Sample(),
			ON3fGeomParam::Sample(N3fArraySample((const N3f *)&mesh.normals[0], mesh.normals.size()), kVertexScope),
			OC3fGeomParam::Sample(C3fArraySample((const C3f *)&mesh.colors[0], mesh.colors.size()), kVertexScope)
		);

		//ON3fGeomParam::Sample normalSample(N3fArraySample((const N3f *)&mesh.normals[0], mesh.normals.size()), kVertexScope);
		//mesh_samp.setNormals(normalSample);

		abcMeshSchema.set(mesh_samp);
	}
	else
	{
		OPolyMeshSchema::Sample mesh_samp(
			P3fArraySample((const V3f *)&mesh.vertices[0], mesh.vertices.size()),
			Int32ArraySample((const int*)&mesh.indices[0], mesh.indices.size()),
			Int32ArraySample(&counts[0], numFaces)
		);

		abcMeshSchema.set(mesh_samp);
	}

	// go through colliders
	FemCollision* coll = femPhysics->GetCollision();
	for (size_t i = 0; coll && i < coll->GetNumCollidables(); i++)
	{
		const Physics::Collidable* collidable = coll->GetCollidable(i);
		if (collidable->mType == Physics::CT_WALLS)
		{
			const Physics::Walls* walls = (const Physics::Walls*)collidable;
			Vector3 c = 100.f * (walls->center);
			Vector3 e = 0.5f * 100.f * (walls->mBox.GetExtent());
			if (mCollMesh[i] == nullptr)
			{
				// create the transform node
				std::string name = "collider" + std::to_string(i);
				OXform abcXform(mAbcArchive.getTop(), name, abcTimeSampling);

				// set the transform
				OXformSchema& schema = abcXform.getSchema();
				XformSample xs;
				xs.setTranslation(V3f(c.x, c.y, c.z));
				xs.setScale(V3f(e.x, e.y, e.z));
				schema.set(xs);

				// create the mesh
				mCollMesh[i] = new OPolyMesh(abcXform, "mesh", abcTimeSampling);
			}

			OPolyMeshSchema &abcMeshSchema = mCollMesh[i]->getSchema();
			size_t numFaces = mBoxMesh.indices.size() / 3;
			std::vector<int> counts(numFaces);
			std::fill(counts.begin(), counts.end(), 3);
			OPolyMeshSchema::Sample mesh_samp(
				P3fArraySample((const V3f *)&mBoxMesh.vertices[0], mBoxMesh.vertices.size()),
				Int32ArraySample((const int*)&mBoxMesh.indices[0], mBoxMesh.indices.size()),
				Int32ArraySample(&counts[0], numFaces)
			);

			abcMeshSchema.set(mesh_samp);
		}
	}
#endif
}

void FemSystem::LoadVisualMesh(const char* path, const Vector3& offset, float scale)
{
	bool ret = LoadMesh(path, mVisualMesh, offset, scale);
	if (ret)
		MapMesh();
}

void FemSystem::BuildBBBoundaryMesh()
{
#ifdef HO_RENDERING
	BuildBoundaryMesh();

	// Construct a higher order boundary/surface mesh based on the linear boundary mesh
	const std::vector<int> boundaryToNodesMap = mNodesToBoundaryMap;

	auto indexOf = [](std::vector<int> map, int val) -> uint32
	{
		std::vector<int>::iterator it = std::find(map.begin(), map.end(), val);
		if (it == map.end())
		{
			return -1;
		}
		return uint32(std::distance(map.begin(), it));
	};

	auto binom = [](int n, int k) -> int
	{
		int val = 1;

		// Utilize that C(n, k) = C(n, n-k) 
		if (k > n - k)
			k = n - k;

		for (int i = 0; i < k; ++i)
		{
			val *= (n - i);
			val /= (i + 1);
		}

		return val;
	};

	auto evalBB = [&binom](float l1, float l2, float l3, size_t noNodes, const Vector3R* nodes, const int* IJKs) -> Vector3R
	{
		Vector3R val(0, 0, 0);

		for (size_t m = 0; m < noNodes; m++)
		{
			int i = IJKs[m * 3 + 0], j = IJKs[m * 3 + 1], k = IJKs[m * 3 + 2];
			val += nodes[m] * (binom(i + j, j) * binom(i + j + k, k) * std::pow(l1, i) * std::pow(l2, j) * std::pow(l3, k));
		}

		return val;
	};

	// - for each linear triangle, identify it in the higher order mesh, and then triangulate it
	size_t subdivisions = 4; // 0, means stay linear, it follows the 'Lagrange' subdivision formula
	size_t noTriangles = mBoundaryMesh.GetNumTriangles();
	TetrahedralMesh<uint32>* tetMesh = GetFemPhysics()->GetTetMesh();
	int order = tetMesh->GetOrder(); float fOrder = float(order);
	uint32 c1 = 0, c2 = 0, c3 = 0;

	std::vector<uint32> gIndicesOfTriangle;
	gIndicesOfTriangle.resize(3 + tetMesh->GetNodesPerEdge(order) * 3 + tetMesh->GetNodesPerFace(order));

	std::vector<Vector3R> positionOfNodes;
	positionOfNodes.resize(gIndicesOfTriangle.size());

	std::vector<int> IJKs;
	IJKs.resize(gIndicesOfTriangle.size() * 3);

	for (size_t i = 0; i < noTriangles; i++)
	{
		//if (i > 0) continue;
		c1 = indexOf(boundaryToNodesMap, mBoundaryMesh.indices[i * 3 + 0]);
		c2 = indexOf(boundaryToNodesMap, mBoundaryMesh.indices[i * 3 + 1]);
		c3 = indexOf(boundaryToNodesMap, mBoundaryMesh.indices[i * 3 + 2]);

		int eidx = tetMesh->GetGlobalIndicesForSurfaceTriangle(c1, c2, c3, &gIndicesOfTriangle[0], &IJKs[0]);

		// GetGlobalIndicesForSurfaceTriangle does not guarentee that the order of c1,c2,c3 is kept
		// thus we check if the new surface triangles must be flipped
		bool flip = false;
		if (gIndicesOfTriangle[0] == c1 && gIndicesOfTriangle[1] == c3 ||
			gIndicesOfTriangle[1] == c1 && gIndicesOfTriangle[2] == c3 ||
			gIndicesOfTriangle[2] == c1 && gIndicesOfTriangle[0] == c3)
		{
			flip = true;
		}

		for (size_t k = 0; k < gIndicesOfTriangle.size(); k++)
		{
			positionOfNodes[k] = GetFemPhysics()->GetDeformedPosition(gIndicesOfTriangle[k]) * 100.f;
		}

		uint I1, J1, K1;
		uint I2, J2, K2;
		uint I3, J3, K3;

		for (size_t j = 1; j < subdivisions + 2; j++)
		{
			I1 = (subdivisions + 1) - (j - 1); J1 = (j - 1); K1 = 0;
			I2 = I1 - 1; J2 = J1 + 1; K2 = K1;
			I3 = I1 - 1; J3 = J1; K3 = K1 + 1;

			uint16 previous_p1idx;

			for (size_t k = 0; k < j; k++)
			{
				uint i1 = I1, j1 = J1 - k, k1 = K1 + k;
				uint i2 = I2, j2 = J2 - k, k2 = K2 + k;
				uint i3 = I3, j3 = J3 - k, k3 = K3 + k;

				float l11 = i1 / float(subdivisions + 1), l12 = j1 / float(subdivisions + 1), l13 = k1 / float(subdivisions + 1);
				float l21 = i2 / float(subdivisions + 1), l22 = j2 / float(subdivisions + 1), l23 = k2 / float(subdivisions + 1);
				float l31 = i3 / float(subdivisions + 1), l32 = j3 / float(subdivisions + 1), l33 = k3 / float(subdivisions + 1);

				Vector3R p1 = evalBB(l11, l12, l13, positionOfNodes.size(), &positionOfNodes[0], &IJKs[0]);
				Vector3R p2 = evalBB(l21, l22, l23, positionOfNodes.size(), &positionOfNodes[0], &IJKs[0]);
				Vector3R p3 = evalBB(l31, l32, l33, positionOfNodes.size(), &positionOfNodes[0], &IJKs[0]);

				size_t tmp = mBBBoundaryMesh.vertices.size();
				uint16 idx1 = mBBBoundaryMesh.AddVertex(DoubleToFloat(p1), true);
				if (tmp < mBBBoundaryMesh.vertices.size())
				{
					// p1 was unique and added to the list
					bb_l1.push_back(l11);
					bb_l2.push_back(l12);
					bb_l3.push_back(l13);

					bb_noNodes.push_back(positionOfNodes.size());
					bb_nodes.push_back(gIndicesOfTriangle);
					bb_IJKs.push_back(IJKs);
				}
				tmp = mBBBoundaryMesh.vertices.size();
				uint16 idx2 = mBBBoundaryMesh.AddVertex(DoubleToFloat(p2), true);
				if (tmp < mBBBoundaryMesh.vertices.size())
				{
					// p2 was unique and added to the list
					bb_l1.push_back(l21);
					bb_l2.push_back(l22);
					bb_l3.push_back(l23);

					bb_noNodes.push_back(positionOfNodes.size());
					bb_nodes.push_back(gIndicesOfTriangle);
					bb_IJKs.push_back(IJKs);
				}

				tmp = mBBBoundaryMesh.vertices.size();
				uint16 idx3 = mBBBoundaryMesh.AddVertex(DoubleToFloat(p3), true);
				if (tmp < mBBBoundaryMesh.vertices.size())
				{
					// p3 was unique and added to the list
					bb_l1.push_back(l31);
					bb_l2.push_back(l32);
					bb_l3.push_back(l33);

					bb_noNodes.push_back(positionOfNodes.size());
					bb_nodes.push_back(gIndicesOfTriangle);
					bb_IJKs.push_back(IJKs);
				}

				mBBBoundaryMesh.AddTriangle(idx1, idx2, idx3, flip);

				if (k > 0)
				{
					mBBBoundaryMesh.AddTriangle(idx1, previous_p1idx, idx2, flip);
				}
				previous_p1idx = idx1;
			}
		}
	}

	// - compute triangle normals
	mBBBoundaryMesh.ComputeNormals();
#endif
}

void FemSystem::UpdateBBBoundaryMesh()
{
#ifdef HO_RENDERING
	//UpdateBoundaryMesh(); // TODO not sure if this is needed

	auto binom = [](int n, int k) -> int
	{
		int val = 1;

		// Utilize that C(n, k) = C(n, n-k) 
		if (k > n - k)
			k = n - k;

		for (int i = 0; i < k; ++i)
		{
			val *= (n - i);
			val /= (i + 1);
		}

		return val;
	};

	auto evalBB = [&binom](float l1, float l2, float l3, size_t noNodes, const Vector3R* nodes, const int* IJKs) -> Vector3R
	{
		Vector3R val(0, 0, 0);

		for (size_t m = 0; m < noNodes; m++)
		{
			int i = IJKs[m * 3 + 0], j = IJKs[m * 3 + 1], k = IJKs[m * 3 + 2];
			val += nodes[m] * (binom(i + j, j) * binom(i + j + k, k) * std::pow(l1, i) * std::pow(l2, j) * std::pow(l3, k));
		}

		return val;
	};
	std::vector<Vector3R> nodes;
	for (size_t i = 0; i < mBBBoundaryMesh.vertices.size(); i++)
	{
		nodes.resize(bb_noNodes[i]);
		for (size_t j = 0; j < bb_noNodes[i]; j++)
		{
			nodes[j] = GetFemPhysics()->GetDeformedPosition(bb_nodes[i][j]) * 100.f;
		}

		Vector3R p = evalBB(bb_l1[i], bb_l2[i], bb_l3[i], bb_noNodes[i],
			&nodes[0],
			&bb_IJKs[i][0]);

		mBBBoundaryMesh.vertices[i] = DoubleToFloat(p);
	}
	mBBBoundaryMesh.ComputeNormals();
#endif
}

// TODO: check for duplicates and use the one in Collision3D.h/cpp instead
inline Vector3 ClosestPtPointTriangle(const Vector3& p,
	const Vector3& a,
	const Vector3& b,
	const Vector3& c)
{
	// Check if P in vertex region outside A
	Vector3 ab = b - a;
	Vector3 ac = c - a;
	Vector3 ap = p - a;
	float d1 = ab.Dot(ap);
	float d2 = ac.Dot(ap);
	if (d1 <= 0.0f && d2 <= 0.0f) return a; // barycentric coordinates (1,0,0)
	// Check if P in vertex region outside B
	Vector3 bp = p - b;
	float d3 = ab.Dot(bp);
	float d4 = ac.Dot(bp);
	if (d3 >= 0.0f && d4 <= d3) return b; // barycentric coordinates (0,1,0)
	// Check if P in edge region of AB, if so return projection of P onto AB
	float vc = d1 * d4 - d3 * d2;
	if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f) {
		float v = d1 / (d1 - d3);
		return a + v * ab; // barycentric coordinates (1-v,v,0)
	}
	// Check if P in vertex region outside C
	Vector3 cp = p - c;
	float d5 = ab.Dot(cp);
	float d6 = ac.Dot(cp);
	if (d6 >= 0.0f && d5 <= d6) return c; // barycentric coordinates (0,0,1)
	// Check if P in edge region of AC, if so return projection of P onto AC
	float vb = d5 * d2 - d1 * d6;
	if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f) {
		float w = d2 / (d2 - d6);
		return a + w * ac; // barycentric coordinates (1-w,0,w)
	}
	// Check if P in edge region of BC, if so return projection of P onto BC
	float va = d3 * d6 - d5 * d4;
	if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f) {
		float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
		return b + w * (c - b); // barycentric coordinates (0,1-w,w)
	}
	// P inside face region. Compute Q through its barycentric coordinates (u,v,w)
	float denom = 1.0f / (va + vb + vc);
	float v = vb * denom;
	float w = vc * denom;
	return a + ab * v + ac * w; // = u*a + v*b + w*c, u = va * denom = 1.0f - v - w
}

inline int PointOutsideOfPlane(const Vector3& p,
	const Vector3& a,
	const Vector3& b,
	const Vector3& c,
	const Vector3& d)
{
	float signp = (p - a).Dot((b - a).Cross(c - a)); // [AP AB AC]
	float signd = (d - a).Dot((b - a).Cross(c - a)); // [AD AB AC]
	// Points on opposite sides if expression signs are opposite
	return signp * signd < 0.0f;
}

inline int PointOutsideOfPlane(const Vector3& p,
	const Vector3& a,
	const Vector3& b,
	const Vector3& c)
{
	return (p - a).Dot((b - a).Cross(c - a)) >= 0.0f; // [AP AB AC] >= 0
}

inline float ClosestPtPointTetrahedron(const Vector3& p,
	const Vector3& a,
	const Vector3& b,
	const Vector3& c,
	const Vector3& d,
	int& id1,
	int& id2,
	int& id3,
	Vector3& point)
{
	// Start out assuming point inside all halfspaces, so closest to itself
	int count = 0;
	float bestSqDist = FLT_MAX;
	// If point outside face abc then compute closest point on abc
	if (PointOutsideOfPlane(p, a, b, c))
	{
		Vector3 q = ClosestPtPointTriangle(p, a, b, c);
		float sqDist = (q - p).Dot(q - p);
		// Update best closest point if (squared) distance is less than current best
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q; id1 = 0; id2 = 1; id3 = 2; }
	}
	else
	{
		count++;
	}
	// Repeat test for face acd
	if (PointOutsideOfPlane(p, a, c, d)) {
		Vector3 q = ClosestPtPointTriangle(p, a, c, d);
		float sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q; id1 = 0; id2 = 2; id3 = 3; }
	}
	else
	{
		count++;
	}
	// Repeat test for face adb
	if (PointOutsideOfPlane(p, a, d, b)) {
		Vector3 q = ClosestPtPointTriangle(p, a, d, b);
		float sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q; id1 = 0; id2 = 3; id3 = 1; }
	}
	else
	{
		count++;
	}
	// Repeat test for face bdc
	if (PointOutsideOfPlane(p, b, d, c)) {
		Vector3 q = ClosestPtPointTriangle(p, b, d, c);
		float sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q;  id1 = 1; id2 = 3; id3 = 2; }
	}
	else
	{
		count++;
	}

	//point inside tetrahedron
	if (count == 4)
	{
		Vector3 q = ClosestPtPointTriangle(p, a, b, c);
		float sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q; id1 = 0; id2 = 1; id3 = 2; }

		q = ClosestPtPointTriangle(p, a, c, d);
		sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q; id1 = 0; id2 = 2; id3 = 3; }

		q = ClosestPtPointTriangle(p, a, d, b);
		sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q; id1 = 0; id2 = 3; id3 = 1; }

		q = ClosestPtPointTriangle(p, b, d, c);
		sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q;  id1 = 1; id2 = 3; id3 = 2; }

	}

	return bestSqDist;
}


inline void BarycentricCoordinates(const Vector3& p,
	const Vector3& a,
	const Vector3& b,
	const Vector3& c,
	float& u,
	float& v,
	float& w)
{
	Vector3 v0 = c - a;
	Vector3 v1 = b - a;
	Vector3 v2 = p - a;

	// Compute dot products
	float dot00 = v0.Dot(v0);
	float dot01 = v0.Dot(v1);
	float dot02 = v0.Dot(v2);
	float dot11 = v1.Dot(v1);
	float dot12 = v1.Dot(v2);

	// Compute barycentric coordinates
	float invDenom = 1.0f / (dot00 * dot11 - dot01 * dot01);
	u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	v = (dot00 * dot12 - dot01 * dot02) * invDenom;
	w = 1.0f - u - v;
}

void FemSystem::MapMesh()
{
	mMapData.resize(mVisualMesh.vertices.size());
	for (uint j = 0; j < mVisualMesh.vertices.size(); j++)
	{
		float minDistance = FLT_MAX;
		int index = 0;
		const Vector3& vert = mVisualMesh.vertices[j];
		Vector3 closestP;
		int id1, id2, id3;
		float u, v, w;
		for (uint i = 0; i < mBoundaryMesh.GetNumTriangles(); i++)
		{
			int id1_t, id2_t, id3_t;
			id1_t = mBoundaryMesh.indices[i * 3];
			id2_t = mBoundaryMesh.indices[i * 3 + 1];
			id3_t = mBoundaryMesh.indices[i * 3 + 2];
			const Vector3& x1 = mBoundaryMesh.vertices[id1_t];
			const Vector3& x2 = mBoundaryMesh.vertices[id2_t];
			const Vector3& x3 = mBoundaryMesh.vertices[id3_t];
			Vector3 closestP_temp;
			closestP_temp = ClosestPtPointTriangle(vert, x1, x2, x3);
			Vector3 delta = vert - closestP_temp;
			float d = delta.Length();
			if (d < minDistance)
			{
				minDistance = d;
				index = i; // triangle index
				closestP = closestP_temp;
				id1 = id1_t;
				id2 = id2_t;
				id3 = id3_t;
			}
		}
		MeshInterp m;
		//m.distance = minDistance;
		m.id1 = id1;
		m.id2 = id2;
		m.id3 = id3;
		m.point = closestP;
		m.tet_index = index; // triangle index

		Vector3 v1 = DoubleToFloat(mBoundaryMesh.vertices[id1]);
		Vector3 v2 = DoubleToFloat(mBoundaryMesh.vertices[id2]);
		Vector3 v3 = DoubleToFloat(mBoundaryMesh.vertices[id3]);
		
		Vector3 normal = cross(v2 - v1, v3 - v1);
		normal.Normalize();
		m.distance = normal.Dot(vert - closestP);

		BarycentricCoordinates(closestP, v1, v2, v3, u, v, w);
		m.u = u;
		m.v = v;
		m.w = w;
		m.vert_index = j;
		m.original = vert;
		mMapData[j] = m;
	}
}

void FemSystem::UpdateVisualMesh()
{
	for (uint i = 0; i < mMapData.size(); i++)
	{
		const MeshInterp& m = mMapData[i];
		const Vector3& p1 = mBoundaryMesh.vertices[m.id1];
		const Vector3& p2 = mBoundaryMesh.vertices[m.id2];
		const Vector3& p3 = mBoundaryMesh.vertices[m.id3];

		Vector3 normal = cross(p2 - p1, p3 - p1);
		normal.Normalize();

		Vector3 p = p1 * m.w + p2 * m.v + p3 * m.u;//barycentric
		p += normal * m.distance;

		mVisualMesh.vertices[m.vert_index] = p;
	}
	mVisualMesh.ComputeNormals(); // slow but only way for now
}


void FemSystem::UninvertElements()
{
	for (size_t i = 0; i < tets.size(); i++)
	{
		Tet& tet = tets.at(i);
		const Vector3R& x0 = nodes.at(tet.idx[0]).pos0;
		const Vector3R& x1 = nodes.at(tet.idx[1]).pos0;
		const Vector3R& x2 = nodes.at(tet.idx[2]).pos0;
		const Vector3R& x3 = nodes.at(tet.idx[3]).pos0;
		Vector3R d1 = x1 - x0;
		Vector3R d2 = x2 - x0;
		Vector3R d3 = x3 - x0;
		Matrix3R mat(d1, d2, d3); // this is the reference shape matrix Dm [Sifakis][Teran03]
		real vol = (mat.Determinant()) / 6.f; // signed volume of the tet
		if (vol < 0)
		{
			// swap the first two indices so that the volume is positive next time
			std::swap(tet.idx[0], tet.idx[1]);
		}
	}

}

void FemSystem::ReshuffleFixedNodes()
{
	std::vector<Node> newNodes;
	std::vector<uint32> map(nodes.size()); // map from old indices to new ones
	// add fixed nodes first
	mNumFixed = 0;
	for (size_t i = 0; i < nodes.size(); i++)
	{
		if (nodes[i].invMass == 0)
		{
			map[i] = newNodes.size();
			newNodes.push_back(nodes[i]);
			mNumFixed++;
		}
	}
	// then the other nodes
	for (size_t i = 0; i < nodes.size(); i++)
	{
		if (nodes[i].invMass != 0)
		{
			map[i] = newNodes.size();
			newNodes.push_back(nodes[i]);
		}
	}
	nodes = newNodes; // replace old nodes with shuffled ones
	// remap tets
	for (size_t i = 0; i < tets.size(); i++)
	{
		for (int j = 0; j < 4; j++)
		{
			tets[i].idx[j] = map[tets[i].idx[j]];
		}
	}
}

bool FemSystem::LoadFromFebFile(const char* path, std::vector<int>& fixedNodes, std::vector<uint32>& surfTris, std::set<uint32>& innerSurface, real scale)
{
	return IO::LoadFromFebFile(path, nodes, tets, fixedNodes, surfTris, innerSurface, scale);
}

bool FemSystem::LoadFromVolFile(const char* path, real scale)
{
	return IO::LoadFromVolFile(path, scale, nodes, tets);
}

bool FemSystem::LoadFromXmlFile(const char* path, std::vector<int>& fixedNodes, std::vector<uint32>& surfTris, FemConfig& config, std::vector<CableDescriptor>& cables)
{
	return IO::LoadFromXmlFile(path, nodes, tets, fixedNodes, surfTris, config, cables);
}

void FemSystem::CreateCable(const CableDescriptor& descriptor, real scale)
{
	Cable cable;
	int numSprings = descriptor.divs;
	real div = descriptor.length / numSprings;
	cable.mCableNodes.resize(numSprings + 1);
	cable.mCablePositions.resize(numSprings + 1);
	// generate a sequence of points along x axis
	for (int i = 0; i <= numSprings; i++)
	{
		Vector3R pos = i * div * descriptor.dir;
		pos += descriptor.offset;
		// register the point to the tet mesh
		real minScore = 1e20;
		int elem = -1;
		real minScorePos = 1e20;
		int elemPos = -1;
		for (uint32 e = 0; e < GetNumTets(); e++)
		{
			real score = 0;
			const Tet& tet = tets[e];

			Vector3R x0 = scale * nodes[tet.idx[0]].pos;
			Vector3R x1 = scale * nodes[tet.idx[1]].pos;
			Vector3R x2 = scale * nodes[tet.idx[2]].pos;
			Vector3R x3 = scale * nodes[tet.idx[3]].pos;
			real w0, w1, w2, w3;
			ComputeBarycentric(pos, x0, x1, x2, x3, w0, w1, w2, w3);
			real eps = -0.001;
			bool positive = w0 > eps && w1 > eps && w2 > eps && w3 > eps;

			for (int j = 0; j < 4; j++)
			{
				Vector3R node = 100.0 * nodes[tet.idx[j]].pos;
				score += (node - pos).LengthSquared();
			}
			if (score < minScore)
			{
				minScore = score;
				elem = e;
			}
			if (positive && score < minScorePos)
			{
				minScorePos = score;
				elemPos = e;
			}
		}
		if (elemPos >= 0)
			elem = elemPos;
		const Tet& tet = tets[elem];
		Vector3R x0 = scale * nodes[tet.idx[0]].pos;
		Vector3R x1 = scale * nodes[tet.idx[1]].pos;
		Vector3R x2 = scale * nodes[tet.idx[2]].pos;
		Vector3R x3 = scale * nodes[tet.idx[3]].pos;
		real w0, w1, w2, w3;
		ComputeBarycentric(pos, x0, x1, x2, x3, w0, w1, w2, w3);
		Printf("%g, %g, %g, %g\n", w0, w1, w2, w3);
		real eps = 0.1;
		if (descriptor.useFreeCable && (w0 < -eps || w1 < -eps || w2 < -eps || w3 < -eps))
			cable.mCableNodes[i].elem = -elem;
		else
			cable.mCableNodes[i].elem = elem;
		cable.mCableNodes[i].bary.Set(w0, w1, w2);
		cable.mCablePositions[i] = (1 / scale) * pos;
	}

	cable.mActuation = 1;
	cable.mCableRestLength = div / scale;
	cable.mCableStiffness = descriptor.stiffness;

	femPhysics->AddCable(cable);
}


} // namespace FEM_SYSTEM