#include "RigidSys3DDemo.h"
#include "Physics/RigidSystem.h"
#include "Graphics3D/Graphics3D.h"
#include "Demo/Particles/3D/Demo3DCommon.h"
#include "Graphics2D/Texture.h"

void RigidSys3DDemo::Create()
{
	//Texture::SetFiltering(Texture::TRILINEAR);
	//fabric.LoadTexture("texture2.bmp");
	Texture::SetFiltering(Texture::NEAREST);
	mCheckerTexture.reset(new Texture());
	mCheckerTexture->LoadTexture("checker.bmp");
}

void RigidSys3DDemo::Init()
{
	mRigidSys3D.reset(new Physics::RigidSystem);

	InitBox();
}

void RigidSys3DDemo::InitBox()
{
	// add a few spheres
	for (int i = 0; i < 100; i++)
	{
		Physics::Rigid rigid;
		Physics::Rigid::Sphere sph;
		sph.r = 1.5f;
		rigid.shapes.push_back(std::make_shared<Physics::Rigid::Sphere>(sph));
		rigid.Icinv = (5.f / (2 * sph.r * sph.r * Physics::RigidSystem::UNIT * Physics::RigidSystem::UNIT)) * Matrix3();

		int idx = i % 10;
		float dist = 2 * sph.r + 1;
		rigid.X.Set((idx % 3) * dist - 5, (i / 10) * dist + 5, (idx / 3) * dist - 5);
		rigid.v.Set(GetRandomReal11(), GetRandomReal11(), GetRandomReal11());
		rigid.v.Scale(10);
		rigid.q = Quaternion::Rotation(0.1f, Vector3(0, 0, 1));
		rigid.R = qToMatrix(rigid.q);
		mRigidSys3D->AddRigid(rigid);
	}

	// ground
	Physics::Rigid ground;
	ground.invmass = 0;
	ground.Icinv = Matrix3::Zero();
	ground.Iinv = ground.Icinv;
	Physics::Rigid::Box floor;
	floor.D.Set(50, 0.5f, 50);
	ground.shapes.push_back(std::make_shared<Physics::Rigid::Box>(floor));
	Physics::Rigid::Box wallSlab;
	wallSlab.D.Set(0.5f, 10, 10);
	wallSlab.T.Set(10.5f, 10.0f, 0);
	ground.shapes.push_back(std::make_shared<Physics::Rigid::Box>(wallSlab));
	wallSlab.T.Set(-10.5f, 10.0f, 0);
	ground.shapes.push_back(std::make_shared<Physics::Rigid::Box>(wallSlab));
	wallSlab.D.Set(10.0f, 10.0f, 0.5f);
	wallSlab.T.Set(0, 10.0f, 10.5f);
	ground.shapes.push_back(std::make_shared<Physics::Rigid::Box>(wallSlab));
	wallSlab.T.Set(0, 10.0f, -10.5f);
	ground.shapes.push_back(std::make_shared<Physics::Rigid::Box>(wallSlab));
	mRigidSys3D->AddRigid(ground);
}

void RigidSys3DDemo::InitCloth()
{
	InitClothFromRigids(13, 10, 40, Vector3(), true, true, 0);

	Physics::Rigid ground;
	ground.invmass = 0;
	ground.Icinv = Matrix3::Zero();
	ground.Iinv = ground.Icinv;
	Physics::Rigid::Box floor;
	floor.D.Set(1000, 5, 1000);
	floor.T.Set(0, -300, 0);
	ground.shapes.push_back(std::make_shared<Physics::Rigid::Box>(floor));
	mRigidSys3D->AddRigid(ground);

	// add a few spheres
	for (int i = 0; i < 300; i++)
	{
		Physics::Rigid rigid;
		Physics::Rigid::Sphere sph;
		sph.r = 15;
		rigid.shapes.push_back(std::make_shared<Physics::Rigid::Sphere>(sph));
		rigid.Icinv = (5.f / (2 * sph.r * sph.r * Physics::RigidSystem::UNIT * Physics::RigidSystem::UNIT)) * Matrix3();

		int idx = i % 10;
		float dist = 2 * sph.r + 1;
		rigid.X.Set((idx % 3) * dist - 50, (i / 10) * dist + 250, (idx / 3) * dist - 50);
		//rigid.w.Set(1.f, 0, 0);
		rigid.v.Set(GetRandomReal11(), GetRandomReal11(), GetRandomReal11());
		rigid.v.Scale(10);
		rigid.q = Quaternion::Rotation(0.1f, Vector3(0, 0, 1));
		rigid.R = qToMatrix(rigid.q);
		//Physics::Rigid::Box box;
		//box.D.Set(10, 10, 10);		
		//rigid.shapes.push_back(std::make_shared<Physics::Rigid::Box>(box));
		mRigidSys3D->AddRigid(rigid);
	}
}

void RigidSys3DDemo::InitClothFromRigids(int divisions, float radius, float inc, const Vector3& offset, bool horizontal, bool attached, int size)
{
	const int n = divisions;
	const int numParticles = n * n;

	float scale = 1.f;// / unit;

	float x = -0.5f * divisions * inc;
	const float startY = x;
	const float incUV = 1.f / n;
	// TODO: proportional to divisions
	const float h = 0.016f; // FIXME
	const float stretch = 10000;// mModel->GetStiffness();
	const float shear = 10;// mModel->GetShearFactor();
	const float bend = 1;// mModel->GetBendFactor();
	for (int i = 0; i < n * n; i++)
	{
		Physics::Rigid rigid;
		Physics::Rigid::Sphere sph;
		sph.r = radius;
		rigid.shapes.push_back(std::make_shared<Physics::Rigid::Sphere>(sph));
		//rigid.Icinv = (5.f / (2 * sph.r * sph.r * Physics::RigidSystem::UNIT * Physics::RigidSystem::UNIT)) * Matrix3();
		rigid.Icinv = Matrix3::Zero();

		rigid.q = Quaternion::Rotation(0.1f, Vector3(0, 0, 1)); // TODO: make default
		rigid.R = qToMatrix(rigid.q);
		mRigidSys3D->AddRigid(rigid);
	}
	for (int j = 0, base = 0; j < n; j++, base += n)
	{
		Physics::Rigid* rigid = mRigidSys3D->GetRigid(size + base);
		if (horizontal)
			rigid->X = (Vector3(x, 0, startY) + offset) * scale;
		else
			rigid->X = (Vector3(x, startY, 0) + offset) * scale;
		rigid->v.SetZero();
		rigid->invmass = 1;
		if (base >= n)
		{
			mRigidSys3D->AddLink(base - n, base, stretch);
			mRigidSys3D->AddLink(base - n + 1, base, shear);
			if (base >= 2 * n)// && !mModel->GetDihedral())
				mRigidSys3D->AddLink(base - 2 * n, base, bend);
		}
		if (attached && (j == 0 || j == n - 1))
		{
			rigid->invmass = 0.f;
			rigid->Icinv = Matrix3::Zero();
		}
		for (int i = 1; i < n; i++)
		{
			int idx = base + i + size;
			rigid = mRigidSys3D->GetRigid(idx);
			if (horizontal)
				rigid->X = (Vector3(x, 0, startY + inc * i) + offset) * scale;
			else
				rigid->X = (Vector3(x, startY + inc * i, /*i * 0.5f*/0) + offset) * scale;
			rigid->v.SetZero();
			rigid->invmass = 1;
			if (idx > base)
			{
				mRigidSys3D->AddLink(idx - 1, idx, stretch);
				if (idx > base + 1)// && !mModel->GetDihedral())
					mRigidSys3D->AddLink(idx - 2, idx, bend);
				if (idx >= n)
				{
					mRigidSys3D->AddLink(idx - n, idx, stretch);
					if (i < n - 1)
						mRigidSys3D->AddLink(idx - n + 1, idx, shear);
					if (j > 1)// && !mModel->GetDihedral())
						mRigidSys3D->AddLink(idx - 2 * n, idx, bend);
				}
				// shear links
				if (idx > n)
				{
					mRigidSys3D->AddLink(idx - n - 1, idx, shear);
					//if ((i & 1) == (j & 1))
					//{
					//	mModel->AddTriangle(idx - n - 1, idx, idx - n);
					//	mModel->AddTriangle(idx - n - 1, idx - 1, idx);
					//	mMesh.AddTriangle(idx - n - 1, idx, idx - n);
					//	mMesh.AddTriangle(idx - n - 1, idx - 1, idx);
					//}
					//else
					//{
					//	mModel->AddTriangle(idx - n, idx - 1, idx);
					//	mModel->AddTriangle(idx - 1, idx - n, idx - n - 1);
					//	mMesh.AddTriangle(idx - n, idx - 1, idx);
					//	mMesh.AddTriangle(idx - 1, idx - n, idx - n - 1);
					//}
				}
			}
		}
		if (attached && (j == 0 || j == n - 1))
		{
			rigid->invmass = 0.f;
			rigid->Icinv = Matrix3::Zero();
		}
		x += inc;
	}
}

void RigidSys3DDemo::Update(float dt)
{
	mRigidSys3D->StepSI();
}

void RigidSys3DDemo::Draw(Graphics3D* graphics3D, bool showDebug, int debugDrawFlags)
{
	for (size_t i = 0; i < mRigidSys3D->GetNumRigids(); i++)
	{		
		Physics::Rigid* rigid = mRigidSys3D->GetRigid(i);
		for (size_t j = 0; j < rigid->shapes.size(); j++)
		{
			graphics3D->SetColor(0.f, 0.94f, 0.8f);
			Physics::Rigid::Shape* shape = rigid->shapes[j].get();
			// TODO: proper transform
			if (shape->type == Physics::Rigid::BOX)
			{
				Physics::Rigid::Box* box1 = (Physics::Rigid::Box*)shape;
				//if (rigid->invmass != 0)
					graphics3D->DrawCube(rigid->X + shape->T, 2.f * box1->D, rigid->R); // TODO: vector extent
				//else
					//graphics3D->DrawWireCube(rigid->X + shape->T, 2 * box1->D);
			}
			else if (shape->type == Physics::Rigid::SPHERE)
			{
				graphics3D->SetColor(0.82f, 0.f, 0.94f);
				Physics::Rigid::Sphere* sph = (Physics::Rigid::Sphere*)shape;
				//graphics3D->DrawSphere(rigid1->X, sph->r);
				if (rigid->Icinv.m[0][0] == 0)
					graphics3D->SetColor(0.82f, 0.94f, 0.f);
				graphics3D->DrawSphereTex(rigid->X, sph->r, rigid->R, mCheckerTexture.get());
			}
		}
	}
	if (debugDrawFlags & DDF_CONTACTS)
	{
		for (size_t i = 0; i < mRigidSys3D->GetNumContacts(); i++)
		{
			graphics3D->SetColor(0.82f, 0, 0);
			Physics::RigidSystem::Contact contact = mRigidSys3D->GetContact(i);
			graphics3D->DrawLine(contact.pa, contact.pa - contact.n * 50.f);
		}
	}
	for (size_t i = 0; i < mRigidSys3D->GetNumJoints(); i++)
	{
		graphics3D->SetColor(0.82f, 0, 0);
		Physics::RigidSystem::Joint joint = mRigidSys3D->GetJoint(i);
		graphics3D->DrawLine(mRigidSys3D->GetRigid(joint.i)->X, mRigidSys3D->GetRigid(joint.j)->X);
	}
}
