#include "ShapeUtility.h"

Physics::Capsule CreateCapsule(const Vector3& pos, const Quaternion& q, float hh, float r, float invMass, Matrix3* Icinv)
{
    Physics::Capsule cap;
    cap.center = pos;
    cap.hh = hh;
    cap.r = r;
    cap.rot = q;
    if (Icinv)
    {
        float ylen = cap.r + cap.hh;
        float zlen = cap.r;
        float xlen = cap.r;
        // TODO: capsule inertia (this is box inertia) !!!
        *Icinv = Matrix3((12.f * invMass /*/ (Physics::Constants::unit * Physics::Constants::unit)*/) *
            Vector3(1.f / (ylen * ylen + zlen * zlen),
                1.f / (xlen * xlen + zlen * zlen),
                1.f / (ylen * ylen + xlen * xlen)));
    }
    return cap;
}

Physics::Box CreateBox(const Vector3& pos, const Quaternion& q, const Vector3& dim, float invMass, int idx, Physics::Group* group, Matrix3& Icinv)
{
    Physics::Box box;
    box.center = pos;
    box.rot = q;
    box.D = dim;
    box.mUserIndex = idx;
    box.mGroup = group;
    Icinv = Matrix3((12.f * invMass /*/ (Physics::Constants::unit * Physics::Constants::unit)*/) *
        Vector3(1.f / (box.D.Y() * box.D.Y() + box.D.Z() * box.D.Z()),
            1.f / (box.D.X() * box.D.X() + box.D.Z() * box.D.Z()),
            1.f / (box.D.Y() * box.D.Y() + box.D.X() * box.D.X())));
    return box;
}

Physics::Sphere CreateSphere(const Vector3& pos, const Quaternion& q, float r, float invMass, Matrix3& Icinv)
{
    Physics::Sphere sph;
    sph.center = pos;
    sph.radius = r;
    sph.rot = q;
    float inertia = 2.5f * invMass / (r * r);
    Icinv = Matrix3(inertia);
    return sph;
}

