#include "PhysicsSystem3D.h"
#include <Geometry/Collision3D.h>

using namespace Geometry;

struct BoxInfo
{
	Aabb3 box;
	int idx; // associated primitive index
};

//typedef std::vector<BoxInfo> BoxGroup;
struct BoxGroup
{
	std::vector<BoxInfo> infos;
	Aabb3 box;
};

void SplitGroup(const BoxGroup& root, std::vector<BoxGroup>& groupsOut, int level = 0)
{
	// split along median cutting plane
	BoxGroup groups[2];

	// choose longest extent
	Vector3 ext = root.box.GetExtent();
	int dir = ext.GetMaxComponent();
	float center = 0.5f * (root.box.min[dir] + root.box.max[dir]);

	for (size_t e = 0; e < root.infos.size(); e++)
	{
		if (root.infos[e].box.min[dir] < center)
		{
			groups[0].infos.push_back(root.infos[e]); // left group
			groups[0].box.Add(root.infos[e].box);
		}
		if (root.infos[e].box.max[dir] > center)
		{
			groups[1].infos.push_back(root.infos[e]); // right group
			groups[1].box.Add(root.infos[e].box);
		}
	}
	
	if (level == 2)
	{
		groupsOut.push_back(groups[0]);
		groupsOut.push_back(groups[1]);
	}
	else
	{
		SplitGroup(groups[0], groupsOut, level + 1);
		SplitGroup(groups[1], groupsOut, level + 1);
	}
}

void PhysicsSystem3D::PruneEdgeCollisionsStatic(std::vector<PrimitivePair>& candidates)
{
	// first compute the swept AABB of each edge and the overall AABB
	const Vector3 extrude(1.0f);
	Aabb3 bigBox;
	BoxGroup root;
	root.infos.resize(edges.size());
	for (size_t e = 0; e < edges.size(); e++)
	{
		int i1 = edges[e].i1;
		int i2 = edges[e].i2;
		const Vector3& x1 = particles[i1].prevPos;
		const Vector3& x2 = particles[i2].prevPos;

		Vector3 minV = vmin(x1, x2) - extrude;
		Vector3 maxV = vmax(x1, x2) + extrude;
		Aabb3 box(minV, maxV);

		BoxInfo boxInfo;
		boxInfo.box = box;
		boxInfo.idx = e;
		root.infos[e] = boxInfo;

		bigBox.Add(box);
	}
	root.box = bigBox;

	std::vector<BoxGroup> groups;
	SplitGroup(root, groups);

	// for each group check for overlapping boxes
	candidates.clear();
	for (size_t k = 0; k < groups.size(); k++)
	{
		// O(n^2) checks
		for (size_t i = 0; i < groups[k].infos.size() - 1; i++)
		{
			for (size_t j = i + 1; j < groups[k].infos.size(); j++)
			{
				if (AabbOverlap3D(groups[k].infos[i].box, groups[k].infos[j].box))
				{
					PrimitivePair pair;
					pair.idx1 = groups[k].infos[i].idx;
					pair.idx2 = groups[k].infos[j].idx;
					candidates.push_back(pair);
				}
			}
		}
	}
}

void PhysicsSystem3D::PruneEdgeCollisions(std::vector<PrimitivePair>& candidates)
{
	// first compute the swept AABB of each edge and the overall AABB
	const Vector3 extrude(1.0f);
	Aabb3 bigBox;
	BoxGroup root;
	root.infos.resize(edges.size());
	for (size_t e = 0; e < edges.size(); e++)
	{
		int i1 = edges[e].i1;
		int i2 = edges[e].i2;
		const Vector3& x1 = particles[i1].prevPos;
		const Vector3& x2 = particles[i2].prevPos;
		const Vector3& y1 = particles[i1].pos;
		const Vector3& y2 = particles[i2].pos;
		Vector3 v1 = y1 - x1;
		Vector3 v2 = y2 - x2;

		Vector3 minV1 = vmin(x1, x2);
		Vector3 maxV1 = vmax(x1, x2);
		Vector3 minV2 = vmin(y1, y2);
		Vector3 maxV2 = vmax(y1, y2);
		Vector3 minV = vmin(minV1, minV2) - extrude; // FIXME: magic number
		Vector3 maxV = vmax(maxV1, maxV2) + extrude;
		Aabb3 box(minV, maxV);

		BoxInfo boxInfo;
		boxInfo.box = box;
		boxInfo.idx = e;
		root.infos[e] = boxInfo;

		//bigBox.min = vmin(bigBox.min, minV);
		//bigBox.max = vmax(bigBox.max, maxV);
		bigBox.Add(box);
	}
	root.box = bigBox;

	std::vector<BoxGroup> groups;
	SplitGroup(root, groups);

	// for each group check for overlapping boxes
	candidates.clear();
	for (size_t k = 0; k < groups.size(); k++)
	{
		// O(n^2) checks
		for (size_t i = 0; i < groups[k].infos.size() - 1; i++)
		{
			for (size_t j = i + 1; j < groups[k].infos.size(); j++)
			{
				if (AabbOverlap3D(groups[k].infos[i].box, groups[k].infos[j].box))
				{
					PrimitivePair pair;
					pair.idx1 = groups[k].infos[i].idx;
					pair.idx2 = groups[k].infos[j].idx;
					candidates.push_back(pair);
				}
			}
		}
	}
}

void PhysicsSystem3D::SelfCollision()
{
	PROFILE_SCOPE("SelfCollision");
	
	const float thickness = 4.f; //0.5f * radius; // hack
	const float radTol = /*radius + tolerance*/ 5.f + thickness;
	const Vector3 extrude(radTol);
	std::vector<int> ids;
	ids.resize(particles.size());
	for (size_t i = 0; i < ids.size(); i++)
		ids[i] = i;
	// go through all triangles
	Vector3 n, p;
	float d;
	for (size_t j = 0; j < cloth->indices.size(); j += 3)
	{
		const uint16 i1 = cloth->indices[j];
		const uint16 i2 = cloth->indices[j + 1];
		const uint16 i3 = cloth->indices[j + 2];
		Vector3 v1 = particles[i1].pos;
		Vector3 v2 = particles[i2].pos;
		Vector3 v3 = particles[i3].pos;

		// build AABB
		Vector3 minV = vmin(vmin(v1, v2), v3) - extrude;
		Vector3 maxV = vmax(vmax(v1, v2), v3) + extrude;

		broadphase.TestObject(Aabb3(minV, maxV), ids);
		
		for (size_t ii = 0; ii < ids.size(); ii++)
		{
			size_t i = ids[ii];

			//if (groups[0].Contains(i))
				//continue; // hack
			if (i == i1 || i == i2 || i == i3 || particles[i].invMass == 0)
				continue;
			Vector3 v = particles[i].pos; // TODO: transform

			if (!PointInAabb3D(minV, maxV, v))
				continue;

			Vector3 n = (v2 - v1).Cross(v3 - v1); // TODO: store normals
			n.Normalize();
			BarycentricCoords coords;
			//Vector3 dir = particles[i].prevPos - particles[i].pos; dir.Normalize();			
			bool intersect;
			Vector3 nOut = n;
			intersect = IntersectSphereTriangle1(v, radTol, v1, v2, v3, particles[i].prevPos, nOut, p, d, coords);
			if (intersect)
			{
				Constraint3D c;
				c.type = Constraint3D::SELF_TRIANGLE;
				c.i1 = i1;
				c.i2 = i2;
				c.i3 = i3;
				c.i4 = (ParticleIdx)i;
				c.w1 = coords.u;
				c.w2 = coords.v;
				c.w3 = coords.w;
				c.len = /*radius + */thickness;
				Vector3 q = coords.u * particles[i1].prevPos + coords.v * particles[i2].prevPos + coords.w * particles[i3].prevPos;
				Vector3 n1 = particles[i].prevPos - q;
				n1.Normalize();
				Vector3 n2 = (particles[i2].prevPos - particles[i1].prevPos).Cross(particles[i3].prevPos - particles[i1].prevPos);
				n2.Normalize();
				const float eps = 0.05f;
				if (fabs(coords.u - 1.f) < eps || fabs(coords.v - 1.f) < eps || fabs(coords.w - 1.f) < eps)
					c.normal = n1;
				else
					c.normal = n1.Dot(n) > 0 ? n2 : -1.f * n2;
				constraints.push_back(c);
			}
		}
	}

	// edge-edge tests
	//{
	////MEASURE_TIME("Edge-edge");
	//for (size_t i = 0; i < cloth->indices.size() - 1; i++)
	//{
	//	// construct edge
	//	int i1 = cloth->indices[i];
	//	int i2 = cloth->indices[i + 1];
	//	if ((i + 1) % 3 == 0)
	//		i2 = cloth->indices[i - 2];

	//	Vector3 minV = vmin(particles[i1].pos, particles[i2].pos) - 0.5f * extrude;
	//	Vector3 maxV = vmin(particles[i1].pos, particles[i2].pos) + 0.5f * extrude;

	//	// loop through all subsequent non-incident edges
	//	for (size_t j = i + 1; j < cloth->indices.size() - 1; j++)
	//	{
	//		// construct test edge
	//		int j1 = cloth->indices[j];
	//		int j2 = cloth->indices[j + 1];
	//		if ((j + 1) % 3 == 0)
	//			j2 = cloth->indices[j - 2];

	//		// skip if edges are adjacent
	//		if (i1 == j1 || i1 == j2 || i2 == j1 || i2 == j2)
	//			continue;

	//		Vector3 minW = vmin(particles[j1].pos, particles[j2].pos) - 0.5f * extrude;
	//		Vector3 maxW = vmin(particles[j1].pos, particles[j2].pos) + 0.5f * extrude;
	//		if (!AabbOverlap3D(Aabb3(minW, maxW), Aabb3(minV, maxV)))
	//			continue;

	//		// check distance between the 2 edges
	//		float s, t;
	//		Vector3 c1, c2;
	//		float dSqr = ClosestPtSegmSegm(particles[i1].pos, particles[i2].pos, particles[j1].pos, particles[j2].pos, s, t, c1, c2);

	//		const float eps = 0.01f;
	//		if (dSqr <= radTol * radTol && fabs(s - 1.f) > eps && fabs(s) > eps
	//			&& fabs(t - 1.f) > eps && fabs(t) > eps)
	//		{
	//			// TODO: create self edge-edge contact
	//			Constraint3D c;
	//			c.type = Constraint3D::SELF_EDGES;
	//			c.i1 = i1;
	//			c.i2 = i2;
	//			c.i3 = j1;
	//			c.i4 = j2;
	//			c.w1 = s;
	//			c.w2 = t;
	//			c.len = /*radius + */thickness;
	//			Vector3 q1 = particles[i1].prevPos + s * (particles[i2].prevPos - particles[i1].prevPos);
	//			Vector3 q2 = particles[j1].prevPos + t * (particles[j2].prevPos - particles[j1].prevPos);
	//			Vector3 n = q2 - q1;
	//			n.Normalize();
	//			c.normal = n;
	//			constraints.push_back(c);
	//		}
	//	}
	//}
	//}
}

// TODO: move to math
// returns the number of real roots
int SolveCubicRealRoots(float a0, float a1, float a2, float& t1, float& t2, float& t3)
{
	float q = (3.f * a1 - a2 * a2) / 9.f;
	float r = (9.f * a2 * a1 - 27 * a0 - 2 * a2 * a2 * a2) / 54.f;
	float qCube = q * q * q;
	float d = qCube + r * r;
	float t0 = -a2 / 3.f;
	const float eps = 1e-6f;
	if (d > eps)
	{
		// one real root
		float r1 = r + sqrt(d);
		float r2 = r - sqrt(d);
		//ASSERT(r1 >= 0.f && r2 >= 0.f);
		if (r1 >= 0.f && r2 >= 0.f)
		{
			float s = pow(r1, 1.f / 3.f);
			float t = pow(r2, 1.f / 3.f);
			t1 = t0 + s + t;
			return 1;
		}
		// TODO: else?
		return 0;
	}
	else if (d < eps)
	{
		// 3 real roots
		ASSERT(q < 0);
		float theta = acosf(r / sqrt(-qCube));
		float rho = sqrt(-q);
		t1 = t0 + rho * cos(theta / 3.f);
		t2 = t0 + rho * cos((theta + 2 * PI) / 3.f);
		t3 = t0 + rho * cos((theta + 4 * PI) / 3.f);
		return 3;
	}
	else //if (d == 0)
	{
		// 3 real roots, 2 equal
		ASSERT(r >= 0.f);
		float s = pow(r, 1.f / 3.f);
		t1 = t0 + 2 * s;
		t2 = t3 = t0 - s;
		return 2;
	}
	return 0;
}

inline bool IntersectSphereTriangle2(const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4, float h,
									 Vector3& n, BarycentricCoords& coords)
{
	n = (x2 - x1).Cross(x3 - x1);
	float clt = sqrtf(0.5f * n.Length());
	n.Normalize();
	float dist = n.Dot(x4 - x3);
	if (fabs(dist) > h)
		return false;
	Vector3 proj = x4 - dist * n;
	coords = Barycentric(x1, x2, x3, proj);
	float delta = h / clt;
	if (coords.u < -delta || coords.u > 1.f + delta ||
		coords.v < -delta || coords.v > 1.f + delta ||
		coords.w < -delta || coords.w > 1.f + delta)
		return false;
	if (dist < 0.f)
		n.Flip();
	return true;
}

void PhysicsSystem3D::SelfCollision1()
{
	// inspired from Bridson
	PROFILE_SCOPE("SelfCollision");

	const float thickness = 3.f;
	const float radTol = 1.f + thickness;
	const Vector3 extrude(radTol);
	std::vector<int> ids;
	// go through all initial triangles
	for (size_t j = 0; j < cloth->indices.size(); j += 3)
	{
		const uint16 i1 = cloth->indices[j];
		const uint16 i2 = cloth->indices[j + 1];
		const uint16 i3 = cloth->indices[j + 2];
		Vector3 v1 = particles[i1].prevPos;
		Vector3 v2 = particles[i2].prevPos;
		Vector3 v3 = particles[i3].prevPos;

		// build AABB
		Vector3 minV = vmin(vmin(v1, v2), v3) - extrude;
		Vector3 maxV = vmax(vmax(v1, v2), v3) + extrude;

		broadphase.TestObject(Aabb3(minV, maxV), ids);

		for (size_t ii = 0; ii < ids.size(); ii++)
		{
			size_t i = ids[ii];

			if (i == i1 || i == i2 || i == i3 || particles[i].invMass == 0)
				continue;
			Vector3 v = particles[i].prevPos;

			if (!PointInAabb3D(minV, maxV, v))
				continue;

			BarycentricCoords coords;
			Vector3 n;
			//bool intersect = IntersectSphereTriangle2(v1, v2, v3, v, radTol, n, coords);
			Vector3 p;
			float dist;
			bool intersect = false;
			intersect = IntersectSphereTriangle(v, radTol, v1, v2, v3, n, p, dist, coords);
			//if (!intersect)
			//	intersect = IntersectSweptSphereTriangle(v, radTol, particles[i].pos - v, particles[i1].pos, particles[i2].pos, particles[i3].pos, p, n, dist, coords);
			if (intersect)
			{
				//Printf("static %d %d %d %d\n", i1, i2, i3, i);
				Constraint3D c;
				c.type = Constraint3D::SELF_TRIANGLE;
				c.i1 = i1;
				c.i2 = i2;
				c.i3 = i3;
				c.i4 = (ParticleIdx)i;
				c.w1 = coords.u;
				c.w2 = coords.v;
				c.w3 = coords.w;
				c.len = thickness;
				c.normal = n;
				constraints.push_back(c);
			}
		}
	}
	// go through all moving triangles and points
	//ids.resize(particles.size());
	//for (size_t i = 0; i < ids.size(); i++)
	//	ids[i] = i;
	for (size_t j = 0; j < cloth->indices.size(); j += 3)
	{
		const uint16 i1 = cloth->indices[j];
		const uint16 i2 = cloth->indices[j + 1];
		const uint16 i3 = cloth->indices[j + 2];
		Vector3 x1 = particles[i1].prevPos;
		Vector3 x2 = particles[i2].prevPos;
		Vector3 x3 = particles[i3].prevPos;
		Vector3 x1f = particles[i1].pos;
		Vector3 x2f = particles[i2].pos;
		Vector3 x3f = particles[i3].pos;

		// build AABB
		Vector3 minVi = vmin(vmin(x1, x2), x3);
		Vector3 maxVi = vmax(vmax(x1, x2), x3);
		Vector3 minVf = vmin(vmin(x1f, x2f), x3f);
		Vector3 maxVf = vmax(vmax(x1f, x2f), x3f);
		Vector3 minV = vmin(minVi, minVf) - extrude;
		Vector3 maxV = vmax(maxVi, maxVf) + extrude;

		broadphase.TestObject(Aabb3(minV, maxV), ids); // hope the extrude catches the prevPos too

		for (size_t ii = 0; ii < ids.size(); ii++)
		{
			size_t i = ids[ii];

			if (i == i1 || i == i2 || i == i3 || particles[i].invMass == 0)
				continue;

			Vector3 x4 = particles[i].prevPos;
			Vector3 x4f = particles[i].pos;

			Vector3 v1 = x1f - x1;
			Vector3 v2 = x2f - x2;
			Vector3 v3 = x3f - x3;
			Vector3 v4 = x4f - x4;

			// root finding
			Vector3 a = x2 - x1;
			Vector3 b = v2 - v1;
			Vector3 c = x3 - x1;
			Vector3 d = v3 - v1;
			Vector3 e = x4 - x1;
			Vector3 f = v4 - v1;

			/*Vector3 c1 = a.Cross(c);
			Vector3 c2 = a.Cross(d);
			Vector3 c3 = b.Cross(c);
			Vector3 c4 = b.Cross(d);
			Vector3 c5 = c2 + c3;

			float p0 = c1.Dot(e);
			float p1 = c5.Dot(e) + c1.Dot(f);
			float p2 = c4.Dot(e) + c5.Dot(f);
			float p3 = c4.Dot(f);

			const float eps = 1e-6f;
			int nRoots = 0;
			float t[3];
			t[0] = t[1] = t[2] = -1.f;
			if (fabs(p3) > eps)
			{
				nRoots = SolveCubicRealRoots(p0 / p3, p1 / p3, p2 / p3, t[0], t[1], t[2]);
			}
			else if (fabs(p2) > eps)
			{
				// quadratic
				float t1, t2;
				t1 = t2 = -1.f;
				float delta = p1 * p1 - 4 * p2 * p0;
				if (delta > 0.f)
				{
					float dSqrt = sqrtf(delta);
					t1 = 0.5f * (-p1 + dSqrt) / p2;
					t2 = 0.5f * (-p1 - dSqrt) / p2;
					nRoots = 2;
				}
				else if (delta == 0.f)
				{
					t1 = t2 = -0.5f * p1 / p2;
					nRoots = 1;
				}
			}
			//TODO: else (linear eq.)
			
			for (int k = 0; k < nRoots; k++)
			{
				if (t[k] >= 0.f && t[k] <= 1.f)
				{
					//Printf("%f\n", t[k]);
					Printf("ccd %d %d %d %d\n", i1, i2, i3, i);

					BarycentricCoords coords;
					Vector3 n;
					Vector3 p;
					float dist;
					bool intersect = IntersectSphereTriangle(x4 + t[k] * v4, 1e-1f, x1 + t[k] * v1, x2 + t[k] * v2, x3 + t[k] * v3, n, p, dist, coords);
					if (intersect)
					{
						Constraint3D c;
						c.type = Constraint3D::SELF_TRIANGLE;
						c.i1 = i1;
						c.i2 = i2;
						c.i3 = i3;
						c.i4 = i;
						c.w1 = coords.u;
						c.w2 = coords.v;
						c.w3 = coords.w;
						c.len = thickness;
						c.normal = n;
						constraints.push_back(c);
					}
				}
			}*/
			float fun0 = e.Dot(a.Cross(c));
			const int nSteps = 40;
			const float step = 1.f / nSteps;
			float t = step;
			for (int k = 0; k < nSteps; k++, t += step)
			{
				const float s = t + step;
				Vector3 interp1 = a + s * b;
				Vector3 interp2 = c + s * d;
				Vector3 interp3 = e + s * f;
				float fun = interp3.Dot(interp1.Cross(interp2));

				if (fun * fun0 > 0)
				{
					fun0 = fun;
					continue;
				}

				BarycentricCoords coords;
				Vector3 n;
				//bool intersect = IntersectSphereTriangle2(x1 + t * v1, x2 + t * v2, x3 + t * v3, x4 + t * v4, radTol, n, coords);
				Vector3 p;
				float dist;
				bool intersect = IntersectSphereTriangle(x4 + t * v4, thickness, x1 + t * v1, x2 + t * v2, x3 + t * v3, n, p, dist, coords);
				if (intersect)
				{

					//Printf("t=%.2f %d %d %d %d\n", t, i1, i2, i3, i);
					Constraint3D c;
					c.type = Constraint3D::SELF_TRIANGLE;
					c.i1 = i1;
					c.i2 = i2;
					c.i3 = i3;
					c.i4 = (ParticleIdx)i;
					c.w1 = coords.u;
					c.w2 = coords.v;
					c.w3 = coords.w;
					c.len = thickness;
					c.normal = n;
					constraints.push_back(c);

					break;
				}
			}
		}
	}
}

inline float EvaluateFun(const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
						 const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4, float t)
{
	Vector3 x21 = x2 - x1;
	Vector3 v21 = v2 - v1;
	Vector3 y21 = x21 + t * v21;
	Vector3 x31 = x3 - x1;
	Vector3 v31 = v3 - v1;
	Vector3 y31 = x31 + t * v31;
	Vector3 x41 = x2 - x1;
	Vector3 v41 = v2 - v1;
	Vector3 y41 = x41 + t * v41;
	return y41.Dot(y21.Cross(y31));
}

void PhysicsSystem3D::SelfCollision2()
{
	PROFILE_SCOPE("SelfCollision");
	
	const float thickness = 4.f;
	const float radTol = 2.f + thickness;
	const Vector3 extrude(radTol);
	// go through all triangles
	std::vector<int> ids;
	Vector3 n, p;
	float d;
	BarycentricCoords coords;

	const int nSteps = 10;
	const float step = 1.f / nSteps;
	
	{
	//PROFILE_SCOPE("Triangles");
	for (size_t j = 0; j < cloth->indices.size(); j += 3)
	{
		const uint16 i1 = cloth->indices[j];
		const uint16 i2 = cloth->indices[j + 1];
		const uint16 i3 = cloth->indices[j + 2];
		const Vector3& x1 = particles[i1].prevPos;
		const Vector3& x2 = particles[i2].prevPos;
		const Vector3& x3 = particles[i3].prevPos;
		const Vector3& y1 = particles[i1].pos;
		const Vector3& y2 = particles[i2].pos;
		const Vector3& y3 = particles[i3].pos;
		Vector3 v1 = y1 - x1;
		Vector3 v2 = y2 - x2;
		Vector3 v3 = y3 - x3;

		// build AABB (of swept triangle)
		Vector3 minV1 = vmin(vmin(x1, x2), x3);
		Vector3 maxV1 = vmax(vmax(x1, x2), x3);
		Vector3 minV2 = vmin(vmin(y1, y2), y3);
		Vector3 maxV2 = vmax(vmax(y1, y2), y3);
		Vector3 minV = vmin(minV1, minV2) - extrude;
		Vector3 maxV = vmax(maxV1, maxV2) + extrude;

		broadphase.TestObject(Aabb3(minV, maxV), ids);
		
		for (size_t ii = 0; ii < ids.size(); ii++)
		{
			size_t i = ids[ii];

			if (i == i1 || i == i2 || i == i3 || particles[i].invMass == 0)
				continue;
			const Vector3& x4 = particles[i].prevPos;
			const Vector3& y4 = particles[i].pos;
			Vector3 v4 = y4 - x4;

			// first test (t = 0)
			bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
			if (intersect)
			{
				Constraint3D c;
				c.type = Constraint3D::SELF_TRIANGLE;
				c.i1 = i1;
				c.i2 = i2;
				c.i3 = i3;
				c.i4 = (ParticleIdx)i;
				c.w1 = coords.u;
				c.w2 = coords.v;
				c.w3 = coords.w;
				c.len = thickness;
				c.normal = n;
				constraints.push_back(c);
			}
			if (intersect)
				continue;

			float t = step;
			//float prevFun = EvaluateFun(x1, x2, x3, x4, v1, v2, v3, v4, t);
			for (int k = 0; k < nSteps - 1; k++)
			{
				//float fun = EvaluateFun(x1, x2, x3, x4, v1, v2, v3, v4, t + step);
				//if (fun * prevFun > 0)
				//{
				//	t += step;
				//	prevFun = fun;
				//	continue;
				//}

				Vector3 z1 = x1 + t * v1;
				Vector3 z2 = x2 + t * v2;
				Vector3 z3 = x3 + t * v3;
				Vector3 z4 = x4 + t * v4;
				
				bool intersect = IntersectSphereTriangle(z4, thickness, z1, z2, z3, n, p, d, coords);
				if (intersect)
				{
					Constraint3D c;
					c.type = Constraint3D::SELF_TRIANGLE;
					c.i1 = i1;
					c.i2 = i2;
					c.i3 = i3;
					c.i4 = (ParticleIdx)i;
					c.w1 = coords.u;
					c.w2 = coords.v;
					c.w3 = coords.w;
					c.len = thickness;
					c.normal = n;
					constraints.push_back(c);
				}
				if (intersect)
					break;
				t += step;
			}
		}
	}
	}

	{
	//PROFILE_SCOPE("Edges");
	// go through all edges
	const float eps = 0.01f;
	std::vector<int> edgeIds;
	for (size_t i = 0; i < edges.size() - 1; i++)
	{
		int i1 = edges[i].i1;
		int i2 = edges[i].i2;
		const Vector3& x1 = particles[i1].prevPos;
		const Vector3& x2 = particles[i2].prevPos;
		const Vector3& y1 = particles[i1].pos;
		const Vector3& y2 = particles[i2].pos;
		Vector3 v1 = y1 - x1;
		Vector3 v2 = y2 - x2;

		Vector3 minV1 = vmin(x1, x2);
		Vector3 maxV1 = vmax(x1, x2);
		Vector3 minV2 = vmin(y1, y2);
		Vector3 maxV2 = vmax(y1, y2);
		Vector3 minV = vmin(minV1, minV2) - extrude;
		Vector3 maxV = vmax(maxV1, maxV2) + extrude;
		Aabb3 box(minV, maxV);

		broadphase.TestObject(box, ids);
		edgeIds.clear();
		for (size_t j = 0; j < ids.size(); j++)
		{
			int idx = ids[j];
			for (size_t k = 0; k < incidence[idx].size(); k++)
				if (incidence[idx][k] > (int)i)
					edgeIds.push_back(incidence[idx][k]);
		}
		std::sort(edgeIds.begin(), edgeIds.end());

		int prevJ = -1;
		//for (size_t j = i + 1; j < edges.size(); j++)
		for (size_t jj = 0; jj < edgeIds.size(); jj++)
		{
			int j = edgeIds[jj];
			if (j == prevJ)
				continue;
			prevJ = j;

			int j1 = edges[j].i1;
			int j2 = edges[j].i2;

			// skip if edges are adjacent
			if (i1 == j1 || i1 == j2 || i2 == j1 || i2 == j2)
				continue;

			const Vector3& x3 = particles[j1].prevPos;
			const Vector3& x4 = particles[j2].prevPos;
			const Vector3& y3 = particles[j1].pos;
			const Vector3& y4 = particles[j2].pos;
			Vector3 v3 = y3 - x3;
			Vector3 v4 = y4 - x4;

			Vector3 minW1 = vmin(x3, x4);
			Vector3 maxW1 = vmax(x3, x4);
			Vector3 minW2 = vmin(y3, y4);
			Vector3 maxW2 = vmax(y3, y4);
			Vector3 minW = vmin(minW1, minW2);
			Vector3 maxW = vmax(maxW1, maxW2);
			if (!AabbOverlap3D(Aabb3(minW, maxW), box))
				continue;

			Vector3 c1, c2;
			float s, t;
			bool intersect = false;
			//if (TestSegmentAABB(x3, x4, box))
			{
				//PROFILE_SCOPE("Edges static");
				// check distance between the 2 edges
				float dSqr = ClosestPtSegmSegm(x1, x2, x3, x4, s, t, c1, c2);

				intersect = dSqr <= radTol * radTol
					&& fabs(s - 1.f) > eps && fabs(s) > eps
					&& fabs(t - 1.f) > eps && fabs(t) > eps;
				if (intersect)
				{
					Constraint3D c;
					c.type = Constraint3D::SELF_EDGES;
					c.i1 = i1;
					c.i2 = i2;
					c.i3 = j1;
					c.i4 = j2;
					c.w1 = s;
					c.w2 = t;
					c.len = thickness;
					Vector3 q1 = x1 + s * (x2 - x1);
					Vector3 q2 = x3 + t * (x4 - x3);
					Vector3 n = q2 - q1;
					n.Normalize();
					c.normal = n;
					constraints.push_back(c);
				}
			}
			if (intersect)
				continue;

			float dt = step;
			//float prevFun = EvaluateFun(x1, x2, x3, x4, v1, v2, v3, v4, t);
			for (int k = 0; k < nSteps - 1; k++)
			{
				//float fun = EvaluateFun(x1, x2, x3, x4, v1, v2, v3, v4, t + step);
				//if (fun * prevFun > 0)
				//{
				//	t += step;
				//	prevFun = fun;
				//	continue;
				//}

				Vector3 z1 = x1 + dt * v1;
				Vector3 z2 = x2 + dt * v2;
				Vector3 z3 = x3 + dt * v3;
				Vector3 z4 = x4 + dt * v4;
				//if (!TestSegmentAABB(z3, z4, box))
				//{
				//	t += step;
				//	continue;
				//}
				float dSqr = ClosestPtSegmSegm(z1, z2, z3, z4, s, t, c1, c2);

				intersect = dSqr <= thickness * thickness
					&& fabs(s - 1.f) > eps && fabs(s) > eps
					&& fabs(t - 1.f) > eps && fabs(t) > eps;
				if (intersect)
				{
					Constraint3D c;
					c.type = Constraint3D::SELF_EDGES;
					c.i1 = i1;
					c.i2 = i2;
					c.i3 = j1;
					c.i4 = j2;
					c.w1 = s;
					c.w2 = t;
					c.len = thickness;
					// TODO: use dt
					Vector3 q1 = x1 + s * (x2 - x1);
					Vector3 q2 = x3 + t * (x4 - x3);
					Vector3 n = q2 - q1;
					n.Normalize();
					c.normal = n;
					constraints.push_back(c);
				}
				if (intersect)
					break;
				t += step;
			}
		}
	}
	}
}

void PhysicsSystem3D::SelfCollisionStatic()
{
	PROFILE_SCOPE("SelfCollision Static");
	
	int count = 0;
	const float thickness = 4.f;
	const float radTol = 1.f + thickness;
	const Vector3 extrude(radTol);
	// go through all triangles
	std::vector<int> ids;
	Vector3 n, p;
	float d;
	BarycentricCoords coords;

	{
	//PROFILE_SCOPE("Triangles");
	for (size_t j = 0; j < cloth->indices.size(); j += 3)
	{
		const uint16 i1 = cloth->indices[j];
		const uint16 i2 = cloth->indices[j + 1];
		const uint16 i3 = cloth->indices[j + 2];
		const Vector3& x1 = particles[i1].prevPos;
		const Vector3& x2 = particles[i2].prevPos;
		const Vector3& x3 = particles[i3].prevPos;

		// build AABB (of swept triangle)
		Vector3 minV = vmin(vmin(x1, x2), x3) - extrude;
		Vector3 maxV = vmax(vmax(x1, x2), x3) + extrude;

		broadphase.TestObject(Aabb3(minV, maxV), ids); // the grid samples pos, not prevPos!!!
		
		for (size_t ii = 0; ii < ids.size(); ii++)
		{
			size_t i = ids[ii];

			if (i == i1 || i == i2 || i == i3 || particles[i].invMass == 0)
				continue;
			const Vector3& x4 = particles[i].prevPos;

			bool intersect = IntersectSphereTriangle(x4, thickness, x1, x2, x3, n, p, d, coords);
			if (intersect)
			{
				Constraint3D c;
				c.type = Constraint3D::SELF_TRIANGLE;
				c.i1 = i1;
				c.i2 = i2;
				c.i3 = i3;
				c.i4 = (ParticleIdx)i;
				c.w1 = coords.u;
				c.w2 = coords.v;
				c.w3 = coords.w;
				c.len = thickness;
				c.normal = n;
				constraints.push_back(c);
				//count++;
			}
		}
	}
	}

	{
	//PROFILE_SCOPE("Edges");
	// go through all edges
		const float eps = 0.01f;

		std::vector<PrimitivePair> candidates;
		PruneEdgeCollisionsStatic(candidates);
		for (size_t k = 0; k < candidates.size(); k++)
		{
			int i = candidates[k].idx1;
			int j = candidates[k].idx2;

			int i1 = edges[i].i1;
			int i2 = edges[i].i2;
			const Vector3& x1 = particles[i1].prevPos;
			const Vector3& x2 = particles[i2].prevPos;

			int j1 = edges[j].i1;
			int j2 = edges[j].i2;

			// skip if edges are adjacent
			if (i1 == j1 || i1 == j2 || i2 == j1 || i2 == j2)
				continue;

			const Vector3& x3 = particles[j1].prevPos;
			const Vector3& x4 = particles[j2].prevPos;

			Vector3 c1, c2;
			float s, t;
			bool intersect = false;
			//if (TestSegmentAABB(x3, x4, box))
			{
				//PROFILE_SCOPE("Edges static");
				// check distance between the 2 edges
				float dSqr = ClosestPtSegmSegm(x1, x2, x3, x4, s, t, c1, c2);

				intersect = dSqr <= thickness * thickness//radTol * radTol
					&& fabs(s - 1.f) > eps && fabs(s) > eps
					&& fabs(t - 1.f) > eps && fabs(t) > eps;
				if (intersect)
				{
					Constraint3D c;
					c.type = Constraint3D::SELF_EDGES;
					c.i1 = i1;
					c.i2 = i2;
					c.i3 = j1;
					c.i4 = j2;
					c.w1 = s;
					c.w2 = t;
					c.len = thickness;
					Vector3 q1 = x1 + s * (x2 - x1);
					Vector3 q2 = x3 + t * (x4 - x3);
					Vector3 n = q2 - q1;
					n.Normalize();
					c.normal = n;
					constraints.push_back(c);
					count++;
				}
			}
		}

	//std::vector<int> edgeIds;
	//for (size_t i = 0; i < edges.size() - 1; i++)
	//{
	//	int i1 = edges[i].i1;
	//	int i2 = edges[i].i2;
	//	const Vector3& x1 = particles[i1].prevPos;
	//	const Vector3& x2 = particles[i2].prevPos;

	//	Vector3 minV = vmin(x1, x2) - extrude;
	//	Vector3 maxV = vmax(x1, x2) + extrude;
	//	Aabb3 box(minV, maxV);

	//	broadphase.TestObject(box, ids);
	//	edgeIds.clear();
	//	for (size_t j = 0; j < ids.size(); j++)
	//	{
	//		int idx = ids[j];
	//		for (size_t k = 0; k < incidence[idx].size(); k++)
	//			if (incidence[idx][k] > (int)i)
	//				edgeIds.push_back(incidence[idx][k]);
	//	}
	//	std::sort(edgeIds.begin(), edgeIds.end());

	//	int prevJ = -1;
	//	for (size_t jj = 0; jj < edgeIds.size(); jj++)
	//	{
	//		int j = edgeIds[jj];
	//		if (j == prevJ)
	//			continue;
	//		prevJ = j;

	//		int j1 = edges[j].i1;
	//		int j2 = edges[j].i2;

	//		// skip if edges are adjacent
	//		if (i1 == j1 || i1 == j2 || i2 == j1 || i2 == j2)
	//			continue;

	//		const Vector3& x3 = particles[j1].prevPos;
	//		const Vector3& x4 = particles[j2].prevPos;

	//		Vector3 minW = vmin(x3, x4);
	//		Vector3 maxW = vmax(x3, x4);
	//		if (!AabbOverlap3D(Aabb3(minW, maxW), box))
	//			continue;

	//		Vector3 c1, c2;
	//		float s, t;
	//		bool intersect = false;
	//		//if (TestSegmentAABB(x3, x4, box))
	//		{
	//			//PROFILE_SCOPE("Edges static");
	//			// check distance between the 2 edges
	//			float dSqr = ClosestPtSegmSegm(x1, x2, x3, x4, s, t, c1, c2);

	//			intersect = dSqr <= radTol * radTol
	//				&& fabs(s - 1.f) > eps && fabs(s) > eps
	//				&& fabs(t - 1.f) > eps && fabs(t) > eps;
	//			if (intersect)
	//			{
	//				Constraint3D c;
	//				c.type = Constraint3D::SELF_EDGES;
	//				c.i1 = i1;
	//				c.i2 = i2;
	//				c.i3 = j1;
	//				c.i4 = j2;
	//				c.w1 = s;
	//				c.w2 = t;
	//				c.len = thickness;
	//				Vector3 q1 = x1 + s * (x2 - x1);
	//				Vector3 q2 = x3 + t * (x4 - x3);
	//				Vector3 n = q2 - q1;
	//				n.Normalize();
	//				c.normal = n;
	//				constraints.push_back(c);
	//				count++;
	//			}
	//		}
	//	}
	//}
	}
	//Printf("static: %d\n", count);
}

inline void EdgeEdgeTest(const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
						 const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4,
						 int i1, int i2, int i3, int i4, float thickness, std::vector<Constraint3D>& constraints)
{
	const int nSteps = 20;
	const float step = 1.f / nSteps;
	const float eps = 0.01f;

	Vector3 c1, c2;
	float s, t;
	bool intersect = false;
	float dt = 0;//step;
	for (int k = 0; k < nSteps/* - 1*/; k++)
	{
		Vector3 z1 = x1 + dt * v1;
		Vector3 z2 = x2 + dt * v2;
		Vector3 z3 = x3 + dt * v3;
		Vector3 z4 = x4 + dt * v4;
		float dSqr = ClosestPtSegmSegm(z1, z2, z3, z4, s, t, c1, c2);

		intersect = dSqr <= thickness * thickness
			&& fabs(s - 1.f) > eps && fabs(s) > eps
			&& fabs(t - 1.f) > eps && fabs(t) > eps;
		if (intersect)
		{
			Constraint3D c;
			c.type = Constraint3D::SELF_EDGES;
			c.i1 = i1;
			c.i2 = i2;
			c.i3 = i3;
			c.i4 = i4;
			c.w1 = s;
			c.w2 = t;
			c.len = thickness;
			// TODO: use dt
			Vector3 q1 = x1 + s * (x2 - x1);
			Vector3 q2 = x3 + t * (x4 - x3);
			Vector3 n = q2 - q1;
			n.Normalize();
			c.normal = n;
			constraints.push_back(c);
			//count++;
		}
		if (intersect)
			break;
		t += step;
	}
}

void PhysicsSystem3D::SelfCollisionDynamic()
{
	PROFILE_SCOPE("SelfCollision Dynamic");
	
	int count = 0;
	const float thickness = 4.f;
	const float radTol = 1.f + thickness; // TODO: no tolerance?
	Vector3 extrude(/*radTol*/2.5f);
	// go through all triangles
	std::vector<int> ids;
	Vector3 n, p;
	float d;
	BarycentricCoords coords;

	const int nSteps = 20;
	const float step = 1.f / nSteps;
	
	{
	PROFILE_SCOPE("Triangles");
	// TODO: exclude triangles already found in static test
	for (size_t j = 0; j < cloth->indices.size(); j += 3)
	{
		const uint16 i1 = cloth->indices[j];
		const uint16 i2 = cloth->indices[j + 1];
		const uint16 i3 = cloth->indices[j + 2];
		const Vector3& x1 = particles[i1].prevPos;
		const Vector3& x2 = particles[i2].prevPos;
		const Vector3& x3 = particles[i3].prevPos;
		const Vector3& y1 = particles[i1].pos;
		const Vector3& y2 = particles[i2].pos;
		const Vector3& y3 = particles[i3].pos;
		Vector3 v1 = y1 - x1;
		Vector3 v2 = y2 - x2;
		Vector3 v3 = y3 - x3;

		// build AABB (of swept triangle)
		Vector3 minV1 = vmin(vmin(x1, x2), x3);
		Vector3 maxV1 = vmax(vmax(x1, x2), x3);
		Vector3 minV2 = vmin(vmin(y1, y2), y3);
		Vector3 maxV2 = vmax(vmax(y1, y2), y3);
		Vector3 minV = vmin(minV1, minV2) - extrude;
		Vector3 maxV = vmax(maxV1, maxV2) + extrude;

		broadphase.TestObject(Aabb3(minV, maxV), ids);
		
		for (size_t ii = 0; ii < ids.size(); ii++)
		{
			size_t i = ids[ii];

			if (i == i1 || i == i2 || i == i3 || particles[i].invMass == 0)
				continue;
			const Vector3& x4 = particles[i].prevPos;
			const Vector3& y4 = particles[i].pos;
			Vector3 v4 = y4 - x4;

			// find time of impact
			//float f0 = EvaluateFun(x1, x2, x3, x4, v1, v2, v3, v4, 0.f);
			//float f1 = EvaluateFun(x1, x2, x3, x4, v1, v2, v3, v4, 1.f);
			//if (f0 * f1 > 0)
			//	continue;

			float t = 0;
			for (int k = 0; k < nSteps; k++)
			{
				Vector3 z1 = x1 + t * v1;
				Vector3 z2 = x2 + t * v2;
				Vector3 z3 = x3 + t * v3;
				Vector3 z4 = x4 + t * v4;
				
				bool intersect = IntersectSphereTriangle(z4, thickness, z1, z2, z3, n, p, d, coords);
				if (intersect)
				{
					Constraint3D c;
					c.type = Constraint3D::SELF_TRIANGLE;
					c.i1 = i1;
					c.i2 = i2;
					c.i3 = i3;
					c.i4 = (ParticleIdx)i;
					c.w1 = coords.u;
					c.w2 = coords.v;
					c.w3 = coords.w;
					c.len = thickness;
					c.normal = n;
					constraints.push_back(c);
				}
				if (intersect)
					break;
				t += step;
			}
		}
	}
	}

	{
	PROFILE_SCOPE("Edges");
#define PRUNE_EDGES
#ifdef BRUTE_FORCE_EDGES
	// go through all edges
	extrude = Vector3(0.1f);
	for (size_t i = 0; i < edges.size() - 1; i++)
	{
		int i1 = edges[i].i1;
		int i2 = edges[i].i2;
		const Vector3& x1 = particles[i1].prevPos;
		const Vector3& x2 = particles[i2].prevPos;
		const Vector3& y1 = particles[i1].pos;
		const Vector3& y2 = particles[i2].pos;
		Vector3 v1 = y1 - x1;
		Vector3 v2 = y2 - x2;

		Vector3 minV1 = vmin(x1, x2);
		Vector3 maxV1 = vmax(x1, x2);
		Vector3 minV2 = vmin(y1, y2);
		Vector3 maxV2 = vmax(y1, y2);
		Vector3 minV = vmin(minV1, minV2) - extrude;
		Vector3 maxV = vmax(maxV1, maxV2) + extrude;
		Aabb3 box(minV, maxV);

		//int prevJ = -1;
		for (size_t j = i + 1; j < edges.size(); j++)
		{
			int j1 = edges[j].i1;
			int j2 = edges[j].i2;

			// skip if edges are adjacent
			if (i1 == j1 || i1 == j2 || i2 == j1 || i2 == j2)
				continue;

			const Vector3& x3 = particles[j1].prevPos;
			const Vector3& x4 = particles[j2].prevPos;
			const Vector3& y3 = particles[j1].pos;
			const Vector3& y4 = particles[j2].pos;
			Vector3 v3 = y3 - x3;
			Vector3 v4 = y4 - x4;

			Vector3 minW1 = vmin(x3, x4);
			Vector3 maxW1 = vmax(x3, x4);
			Vector3 minW2 = vmin(y3, y4);
			Vector3 maxW2 = vmax(y3, y4);
			Vector3 minW = vmin(minW1, minW2) - extrude;
			Vector3 maxW = vmax(maxW1, maxW2) + extrude;
			if (!AabbOverlap3D(Aabb3(minW, maxW), box))
				continue;

			EdgeEdgeTest(x1, x2, x3, x4, v1, v2, v3, v4, i1, i2, j1, j2, thickness, constraints);
		}
	}
#elif defined(PRUNE_EDGES)
	std::vector<PrimitivePair> candidates;
	PruneEdgeCollisions(candidates);
	for (size_t k = 0; k < candidates.size(); k++)
	{
		int i = candidates[k].idx1;
		int j = candidates[k].idx2;

		int i1 = edges[i].i1;
		int i2 = edges[i].i2;
		const Vector3& x1 = particles[i1].prevPos;
		const Vector3& x2 = particles[i2].prevPos;
		const Vector3& y1 = particles[i1].pos;
		const Vector3& y2 = particles[i2].pos;
		Vector3 v1 = y1 - x1;
		Vector3 v2 = y2 - x2;

		int j1 = edges[j].i1;
		int j2 = edges[j].i2;

		// skip if edges are adjacent
		if (i1 == j1 || i1 == j2 || i2 == j1 || i2 == j2)
			continue;

		const Vector3& x3 = particles[j1].prevPos;
		const Vector3& x4 = particles[j2].prevPos;
		const Vector3& y3 = particles[j1].pos;
		const Vector3& y4 = particles[j2].pos;
		Vector3 v3 = y3 - x3;
		Vector3 v4 = y4 - x4;

		EdgeEdgeTest(x1, x2, x3, x4, v1, v2, v3, v4, i1, i2, j1, j2, thickness, constraints);
	}
#endif
	}
}

// TODO: move to header file
inline void PhysicsSystem3D::ProjectTriangleSelf(Constraint3D& contact, float h)
{
	const float h2 = h * h;
	Vector3& p1 = particles[contact.i1].pos;
	Vector3& p2 = particles[contact.i2].pos;
	Vector3& p3 = particles[contact.i3].pos;
	Vector3& p4 = particles[contact.i4].pos;
	Vector3 p = contact.w1 * p1 + contact.w2 * p2 + contact.w3 * p3;
	Vector3 n = contact.normal;
	float len0 = contact.len;
	float len = n.Dot(particles[contact.i4].pos - p);
	if (len > len0)
		return;
	float invMass = particles[contact.i1].invMass + particles[contact.i2].invMass + particles[contact.i3].invMass + particles[contact.i4].invMass;
	float s = 2.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / (h2 * invMass);
	float dLambda = s * (len - len0);
	Vector3 disp = dLambda * n;
	p1 += disp * (h2 * contact.w1 * particles[contact.i1].invMass);
	p2 += disp * (h2 * contact.w2 * particles[contact.i2].invMass);
	p3 += disp * (h2 * contact.w3 * particles[contact.i3].invMass);
	p4 -= disp * (h2 * particles[contact.i4].invMass);
	particles[contact.i1].velocity += disp * (h * contact.w1 * particles[contact.i1].invMass);
	particles[contact.i2].velocity += disp * (h * contact.w2 * particles[contact.i2].invMass);
	particles[contact.i3].velocity += disp * (h * contact.w3 * particles[contact.i3].invMass);
	particles[contact.i4].velocity -= disp * (h * particles[contact.i4].invMass);
}

inline void PhysicsSystem3D::ProjectEdgesSelf(Constraint3D& contact, float h)
{
	const float h2 = h * h;
	Vector3& v1 = particles[contact.i1].pos;
	Vector3& v2 = particles[contact.i2].pos;
	Vector3& v3 = particles[contact.i3].pos;
	Vector3& v4 = particles[contact.i4].pos;
	Vector3 p = v1 + contact.w1 * (v2 - v1);
	Vector3 q = v3 + contact.w2 * (v4 - v3);
	Vector3 n = contact.normal;
	float len = n.Dot(q - p);
	if (len > contact.len)
		return;
	float invMass = particles[contact.i1].invMass + particles[contact.i2].invMass + particles[contact.i3].invMass + particles[contact.i4].invMass;
	float omw1 = 1.f - contact.w1;
	float omw2 = 1.f - contact.w2;
	float s = 2.f / (contact.w1 * contact.w1 + omw1 * omw1 + contact.w2 * contact.w2 + omw2 * omw2) / (h2 * invMass);
	float dLambda = s * (len - contact.len);
	Vector3 disp = dLambda * n;
	v1 += disp * (h2 *omw1 * particles[contact.i1].invMass);
	v2 += disp * (h2 * contact.w1 * particles[contact.i2].invMass);
	v3 -= disp * (h2 * omw2 * particles[contact.i3].invMass);
	v4 -= disp * (h2 * contact.w2 * particles[contact.i4].invMass);
	particles[contact.i1].velocity += disp * (h *omw1 * particles[contact.i1].invMass);
	particles[contact.i2].velocity += disp * (h * contact.w1 * particles[contact.i2].invMass);
	particles[contact.i3].velocity -= disp * (h * omw2 * particles[contact.i3].invMass);
	particles[contact.i4].velocity -= disp * (h * contact.w2 * particles[contact.i4].invMass);
}

inline void PhysicsSystem3D::ProjectContact(Constraint3D& contact, float h, float omega)
{
	Particle3D& p1 = particles[contact.i1];
	Vector3 delta = p1.pos - contact.point;
	float len0 = contact.len;
	float len = delta.Dot(contact.normal);
	if (len > len0)
		return;
	delta = contact.normal;
	const float depth = len - len0;
	delta.Scale(contact.stiffness * omega * depth);
	p1.pos.Subtract(delta);
	p1.velocity -= (1.f / h) * delta;
}

inline void PhysicsSystem3D::ProjectContactVelocity(Constraint3D& contact, float h, float omega)
{
	// Baumgarte is a disaster - collisions are broken
	Particle3D& p1 = particles[contact.i1];
	float v = dot(p1.velocity, contact.normal);
	const float beta = 0.0f / h;
	float dLambda = -(v + beta * contact.depth);
	float lambda0 = contact.lambda;
	float lambda = max(0.f, lambda0 + dLambda);
	dLambda = lambda - lambda0;
	p1.velocity += dLambda * contact.normal;
}

void PhysicsSystem3D::MicroStepUnified(float h)
{
	// TODO: collision detection here for velocity formulation?
	// integrate candidate positions using Symplectic Euler
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += h * gravity;
		particles[i].prevPos = particles[i].pos;
		particles[i].pos += h * particles[i].velocity;
	}

	// discrete collision detection
	constraints.clear();
	Collide();
	constraints.insert(constraints.end(), links.begin(), links.end());

	// constraint projection (solver)
	SolveUnified(h);

	if (collFlags & COLL_SELF)
	{
		//constraints.clear();
		//constraints.insert(constraints.end(), links.begin(), links.end());
		SelfCollisionDynamic();
		SolveUnified(h);
		//PostFriction();
	}

	PostFriction();
	// this is only for velocity based projectors
	// (re)integrate positions based on velocities
	//for (size_t i = 0; i < particles.size(); i++)
	//{
	//	if (particles[i].invMass == 0)
	//		continue;
	//	particles[i].pos = particles[i].prevPos + h * particles[i].velocity;
	//}
}

void PhysicsSystem3D::SolveUnified(float h)
{
	const float omega = 1.f; // SOR factor
	const int n = numIterations;
	const float h2 = h * h;
	for (int k = 0; k < n; ++k)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			if (constraints[i].type == Constraint3D::SELF_TRIANGLE)
			{
				ProjectTriangleSelf(constraints[i], h);
			}
			else if (constraints[i].type == Constraint3D::SELF_EDGES)
			{
				ProjectEdgesSelf(constraints[i], h);
			}
			else if (constraints[i].type == Constraint3D::CONTACT)
			{
				ProjectContact(constraints[i], h);
			}
			else
			{			
				Particle3D& p1 = particles[constraints[i].i1];
				Vector3 delta = p1.pos;
				Particle3D& p2 = particles[constraints[i].i2];
				delta.Subtract(p2.pos);
				float len0 = constraints[i].len;
				float len = delta.Length();
				if (constraints[i].type != Constraint3D::LINK && len > len0) // COLL_PAIR
					continue;
				delta.Scale(1 / len);
				const float depth = len - len0;
				delta.Scale(constraints[i].stiffness * omega * depth / (h2 * (p1.invMass + p2.invMass))); // TODO: multiply only by h
				p1.pos -= (h2 * p1.invMass) * delta;
				p2.pos += (h2 * p2.invMass) * delta;
				p1.velocity -= (h * p1.invMass) * delta;
				p2.velocity += (h * p2.invMass) * delta;
			}
		}
	}
}