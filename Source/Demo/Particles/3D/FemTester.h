#ifndef FEM_TESTER_H
#define FEM_TESTER_H

#include <Engine/Types.h>

namespace FEM_SYSTEM
{
	class FemTester
	{
	public:
		void Test();

	private:
		void TestStiffness();
		void TestDisplacement(int numBCs, int axis, uint16 order[4]);

		void TestCube();
		
		void TestPureTranslationIncompressible(int pressureOrder);
		void TestPureShearIncompressible(int pressureOrder);

		void TestPureTranslationQuadratic();
		void TestPureShearQuadratic();

		void TestPureTranslationQuadraticIncompressible(int pressureOrder);
		void TestPureShearQuadraticIncompressible(int pressureOrder);

		void TestVolumeIncrease();
		void TestVolumeIncreaseQuadratic();
		void TestVolumeIncreaseIncompressible(int pressureOrder);
		void TestVolumeIncreaseQuadraticIncompressible(int pressureOrder);
	};

} // namespace FEM_SYSTEM

#endif // FEM_TESTER_H
