#include "UnifiedView.h"
#include <Geometry/Assets.h>
#include <Geometry/AabbTree.h>
#include <algorithm>

#include <Physics/Unified/RigidSoftCoupler.h>
#include "ShapeUtility.h"

using namespace Geometry;

int SoftFEMView::mCounter = 0;

inline Vector3 ClosestPtPointTriangle(const Vector3& p,
									  const Vector3& a,
									  const Vector3& b,
									  const Vector3& c)
{
	// Check if P in vertex region outside A
	Vector3 ab = b - a;
	Vector3 ac = c - a;
	Vector3 ap = p - a;
	float d1 = ab.Dot(ap);
	float d2 = ac.Dot(ap);
	if (d1 <= 0.0f && d2 <= 0.0f) return a; // barycentric coordinates (1,0,0)
	// Check if P in vertex region outside B
	Vector3 bp = p - b;
	float d3 = ab.Dot(bp);
	float d4 = ac.Dot(bp);
	if (d3 >= 0.0f && d4 <= d3) return b; // barycentric coordinates (0,1,0)
	// Check if P in edge region of AB, if so return projection of P onto AB
	float vc = d1*d4 - d3*d2;
	if (vc <= 0.0f && d1 >= 0.0f && d3 <= 0.0f) {
		float v = d1 / (d1 - d3);
		return a + v * ab; // barycentric coordinates (1-v,v,0)
	}
	// Check if P in vertex region outside C
	Vector3 cp = p - c;
	float d5 = ab.Dot(cp);
	float d6 = ac.Dot(cp);
	if (d6 >= 0.0f && d5 <= d6) return c; // barycentric coordinates (0,0,1)
	// Check if P in edge region of AC, if so return projection of P onto AC
	float vb = d5*d2 - d1*d6;
	if (vb <= 0.0f && d2 >= 0.0f && d6 <= 0.0f) {
		float w = d2 / (d2 - d6);
		return a + w * ac; // barycentric coordinates (1-w,0,w)
	}
	// Check if P in edge region of BC, if so return projection of P onto BC
	float va = d3*d6 - d5*d4;
	if (va <= 0.0f && (d4 - d3) >= 0.0f && (d5 - d6) >= 0.0f) {
		float w = (d4 - d3) / ((d4 - d3) + (d5 - d6));
		return b + w * (c - b); // barycentric coordinates (0,1-w,w)
	}
	// P inside face region. Compute Q through its barycentric coordinates (u,v,w)
	float denom = 1.0f / (va + vb + vc);
	float v = vb * denom;
	float w = vc * denom;
	return a + ab * v + ac * w; // = u*a + v*b + w*c, u = va * denom = 1.0f - v - w
}

inline int PointOutsideOfPlane(const Vector3& p,
							   const Vector3& a,
							   const Vector3& b,
							   const Vector3& c,
							   const Vector3& d)
{
	float signp = (p - a).Dot((b - a).Cross(c - a)); // [AP AB AC]
	float signd = (d - a).Dot((b - a).Cross(c - a)); // [AD AB AC]
	// Points on opposite sides if expression signs are opposite
	return signp * signd < 0.0f;
}

inline int PointOutsideOfPlane(const Vector3& p, 
							   const Vector3& a, 
							   const Vector3& b,
							   const Vector3& c)
{
	return (p - a).Dot((b - a).Cross(c - a)) >= 0.0f; // [AP AB AC] >= 0
}

inline float ClosestPtPointTetrahedron(const Vector3& p, 
									   const Vector3& a,
									   const Vector3& b,
									   const Vector3& c,
									   const Vector3& d,
									   int& id1,
									   int& id2,
									   int& id3,
									   Vector3& point)
{
	// Start out assuming point inside all halfspaces, so closest to itself
	int count = 0;
	float bestSqDist = FLT_MAX;
	// If point outside face abc then compute closest point on abc
	if (PointOutsideOfPlane(p, a, b, c))
	{
		Vector3 q = ClosestPtPointTriangle(p, a, b, c);
		float sqDist = (q - p).Dot(q - p);
		// Update best closest point if (squared) distance is less than current best
		if (sqDist < bestSqDist){ bestSqDist = sqDist; point = q; id1 = 0; id2 = 1; id3 = 2; }
	}
	else
	{
		count++;
	}
	// Repeat test for face acd
	if (PointOutsideOfPlane(p, a, c, d)) {
		Vector3 q = ClosestPtPointTriangle(p, a, c, d);
		float sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist){ bestSqDist = sqDist; point = q; id1 = 0; id2 = 2; id3 = 3; }
	}
	else
	{
		count++;
	}
	// Repeat test for face adb
	if (PointOutsideOfPlane(p, a, d, b)) {
		Vector3 q = ClosestPtPointTriangle(p, a, d, b);
		float sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist){ bestSqDist = sqDist; point = q; id1 = 0; id2 = 3; id3 = 1; }
	}
	else
	{
		count++;
	}
	// Repeat test for face bdc
	if (PointOutsideOfPlane(p, b, d, c)) {
		Vector3 q = ClosestPtPointTriangle(p, b, d, c);
		float sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q;  id1 = 1; id2 = 3; id3 = 2; }
	}
	else
	{
		count++;
	}

	//point inside tetrahedron
	if (count == 4)
	{
		Vector3 q = ClosestPtPointTriangle(p, a, b, c);
		float sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist){ bestSqDist = sqDist; point = q; id1 = 0; id2 = 1; id3 = 2; }

		q = ClosestPtPointTriangle(p, a, c, d);
		sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist){ bestSqDist = sqDist; point = q; id1 = 0; id2 = 2; id3 = 3; }

		q = ClosestPtPointTriangle(p, a, d, b);
		sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist){ bestSqDist = sqDist; point = q; id1 = 0; id2 = 3; id3 = 1; }

		q = ClosestPtPointTriangle(p, b, d, c);
		sqDist = (q - p).Dot(q - p);
		if (sqDist < bestSqDist) { bestSqDist = sqDist; point = q;  id1 = 1; id2 = 3; id3 = 2; }

	}

	return bestSqDist;
}


inline void BarycentricCoordinates(const Vector3& p,
								   const Vector3& a,
								   const Vector3& b,
								   const Vector3& c,
								   float& u,
								   float& v,
								   float& w)
{
	Vector3 v0 = c - a;
	Vector3 v1 = b - a;
	Vector3 v2 = p - a;

	// Compute dot products
	float dot00 = v0.Dot(v0);
	float dot01 = v0.Dot(v1);
	float dot02 = v0.Dot(v2);
	float dot11 = v1.Dot(v1);
	float dot12 = v1.Dot(v2);

	// Compute barycentric coordinates
	float invDenom = 1.0f / (dot00 * dot11 - dot01 * dot01);
	u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	v = (dot00 * dot12 - dot01 * dot02) * invDenom;
	w = 1.0f - u - v;
}


#ifdef USE_ALEMBIC
// Alembic Includes
#include <Alembic/AbcGeom/All.h>
// This is required to tell Alembic which implementation to use.
#include <Alembic/AbcCoreOgawa/All.h>

// Other includes
#include <iostream>
#include <assert.h>

// We include some global mesh data to test with from an external source
// to keep this example code clean.
#include <Alembic/AbcGeom/Tests/MeshData.h>

//-*****************************************************************************
//-*****************************************************************************
// NAMESPACES
//
// Each library has a namespace which is the same as the name of the library.
// We shorten those here for brevity.
//-*****************************************************************************
//-*****************************************************************************

using namespace std;
using namespace Alembic::AbcGeom; // Contains Abc, AbcCoreAbstract

//-*****************************************************************************
//-*****************************************************************************
// WRITING OUT AN ANIMATED MESH
//
// Here we'll create an "Archive", which is Alembic's term for the actual
// file on disk containing all of the scene geometry. The Archive will contain
// a single animated Transform with a single static PolyMesh as its child.
//-*****************************************************************************
//-*****************************************************************************

void ClothView::ExportToAlembic(Alembic::Abc::OArchive* archive)
{
#ifdef USE_ALEMBIC
	if (mMeshyObj == nullptr)
	{
		Alembic::Abc::TimeSamplingPtr ts( new Alembic::Abc::TimeSampling( 1.0 / 60.0, 0.0 ) );
		Alembic::AbcGeom::OXform xfobj(archive->getTop(), "cloth", ts);
		mMeshyObj = new Alembic::AbcGeom::OPolyMesh(xfobj, "cloth_mesh", ts);
	}

	OPolyMeshSchema &mesh = mMeshyObj->getSchema();

	// UVs and Normals use GeomParams, which can be written or read
	// as indexed or not, as you'd like.

	// The typed GeomParams have an inner Sample class that is used
	// for setting and getting data.
	//OV2fGeomParam::Sample uvsamp( V2fArraySample( (const V2f *)g_uvs,
	//	g_numUVs ),
	//	kFacevaryingScope );
	// indexed normals
	//ON3fGeomParam::Sample nsamp( N3fArraySample( (const N3f *)g_normals,
	//	g_numNormals ),
	//	kFacevaryingScope );

	// Set a mesh sample.
	// We're creating the sample inline here,
	// but we could create a static sample and leave it around,
	// only modifying the parts that have changed.

	// Alembic is strongly typed. P3fArraySample is for an array
	// of 32-bit points, which are the mesh vertices. g_verts etc.
	// are defined in MeshData.cpp.
	const Mesh& myMesh = mCloth->GetMesh();
	size_t numFaces = myMesh.indices.size() / 3;
	std::vector<int> counts(numFaces);
	std::fill(counts.begin(), counts.end(), 3);
	OPolyMeshSchema::Sample mesh_samp(
		P3fArraySample( ( const V3f * )&myMesh.vertices[0], myMesh.vertices.size() ),
		Int32ArraySample( (const int*)&myMesh.indices[0], myMesh.indices.size() ),
		Int32ArraySample( &counts[0], numFaces ));
		//uvsamp, nsamp );

	// not actually the right data; just making it up - FIXME!
	//Box3d cbox;
	//cbox.extendBy( V3d( 1.0, -1.0, 0.0 ) );
	//cbox.extendBy( V3d( -1.0, 1.0, 3.0 ) );	
	//mesh_samp.setSelfBounds( cbox );

	// Set the sample twice.
	// Because the data is the same in both samples, Alembic will
	// store only one copy, but note that two samples have been set.
	mesh.set( mesh_samp );
	//mesh.set( mesh_samp );


	// Alembic objects close themselves automatically when they go out
	// of scope. So - we don't have to do anything to finish
	// them off!
	//std::cout << "Writing: " << archive->getName() << std::endl;
#endif // !USE_ALEMBIC
}
#endif // !USE_ALEMBIC

void ClothView::ExportToPovRay(FILE* txt)
{
	const Mesh* mesh = &mCloth->GetMesh();
	fprintf(txt, "mesh2 {\n");
	fprintf(txt, "\tvertex_vectors { %d", mesh->vertices.size());
	for (size_t j = 0; j < mesh->vertices.size(); j++)
	{
		fprintf(txt, "\t\t, <%f,%f,%f>", mesh->vertices[j].X(), mesh->vertices[j].Y(), mesh->vertices[j].Z());
	}
	fprintf(txt, "\t}\n");
	fprintf(txt, "\tnormal_vectors { %d", mesh->normals.size());
	for (size_t j = 0; j < mesh->normals.size(); j++)
	{
		fprintf(txt, "\t\t, <%f,%f,%f>", mesh->normals[j].X(), mesh->normals[j].Y(), mesh->normals[j].Z());
	}
	fprintf(txt, "\t}\n");
	fprintf(txt, "\tface_indices { %d", mesh->indices.size() / 3);
	for (size_t j = 0; j < mesh->indices.size(); j += 3)
	{
		fprintf(txt, "\t\t, <%d,%d,%d>", mesh->indices[j], mesh->indices[j + 1], mesh->indices[j + 2]);
	}
	fprintf(txt, "\t}\n");
	fprintf(txt, "\tpigment { Cyan } \n");
	fprintf(txt, "}\n");
}

void ExportMeshToText(FILE* txt, const Mesh& mesh, const Vector3& color, const Vector3& center, const Quaternion& rot)
{
	fprintf(txt, "0, %d, %d,\n", mesh.vertices.size(), mesh.indices.size());
	// write color
	fprintf(txt, "%f, %f, %f,\n", color.X(), color.Y(), color.Z());
	// write transform
	fprintf(txt, "%f, %f, %f, %f, %f, %f, %f,\n",
		center.X(), center.Y(), center.Z(),
		rot.s, rot.v.X(), rot.v.Y(), rot.v.Z());
	for (size_t j = 0; j < mesh.vertices.size(); j++)
	{
		fprintf(txt, "%f, %f, %f,\n", mesh.vertices[j].X(), mesh.vertices[j].Y(), mesh.vertices[j].Z());
	}
	for (size_t j = 0; j < mesh.vertices.size(); j++)
	{
		fprintf(txt, "%f, %f, %f,\n", mesh.normals[j].X(), mesh.normals[j].Y(), mesh.normals[j].Z());
	}
	for (size_t j = 0; j < mesh.indices.size(); j += 3)
	{
		fprintf(txt, "%d, %d, %d,\n", mesh.indices[j], mesh.indices[j + 1], mesh.indices[j + 2]);
	}
}

void ClothView::ExportToText(FILE* txt)
{
	const Mesh& mesh = mCloth->GetMesh();
	ExportMeshToText(txt, mesh, Vector3(0, 1, 0), Vector3(), Quaternion());
	//fprintf(txt, "0, %d, %d,\n", mesh.vertices.size(), mesh.indices.size());
	//for (size_t j = 0; j < mesh.vertices.size(); j++)
	//{
	//	fprintf(txt, "%f, %f, %f,\n", mesh.vertices[j].X(), mesh.vertices[j].Y(), mesh.vertices[j].Z());
	//}
	//for (size_t j = 0; j < mesh.vertices.size(); j++)
	//{
	//	fprintf(txt, "%f, %f, %f,\n", mesh.normals[j].X(), mesh.normals[j].Y(), mesh.normals[j].Z());
	//}
	//for (size_t j = 0; j < mesh.indices.size(); j += 3)
	//{
	//	fprintf(txt, "%d, %d, %d,\n", mesh.indices[j], mesh.indices[j + 1], mesh.indices[j + 2]);
	//}
}

void ClothView::Draw(Graphics3D* graphics3D)
{
	// draw cloth mesh
	//mCloth->UpdateMesh(mSystem->GetBodies());
	graphics3D->SetColor(0, 1, 1);
	graphics3D->DrawMesh(mCloth->GetMesh().vertices, mCloth->GetMesh().normals, mCloth->GetMesh().indices);
	glCullFace(GL_FRONT);
	graphics3D->SetColor(0, 0, 1);
	graphics3D->SetFlipNormals(true);
	graphics3D->DrawMesh(mCloth->GetMesh().vertices, mCloth->GetMesh().normals, mCloth->GetMesh().indices);
	graphics3D->SetFlipNormals(false);
	glCullFace(GL_BACK);

	// draw particles
	//graphics3D->SetColor(0.83f, 0.17f, 0.27f);
	//for (size_t i = 0; i < mCloth->GetCount(); i++)
	//{
	//	Vector3 pos = mSystem->GetBodyFromGroup(i, mCloth).pos;
	//	graphics3D->DrawSphere(pos, mCloth->GetThickness());
	//}

	// draw links
	//graphics3D->SetColor(0.8f, 0.3f, 0.2f);
	//for (size_t i = 0; i < mCloth->GetLinks().size(); i++)
	//{
	//	const Physics::Link& link = mCloth->GetLinks()[i];
	//	graphics3D->DrawLine(mSystem->GetBody(link.i1).pos, mSystem->GetBody(link.i2).pos);
	//}
}

UnifiedView::UnifiedView() : mNumFrames(0), mMaxLevels(2), mMaxFrames(-1), mAbcExport(false)
{
#ifdef USE_ALEMBIC
	mArchive = new Alembic::Abc::OArchive(Alembic::AbcCoreOgawa::WriteArchive(), "simulation.abc" );

	// create sphere mesh
	const int divsTheta = 10;
	const int divsPhi = 10;
	float theta = 0;
	const float dt = 2 * PI / divsTheta;
	const float dp = PI / (divsPhi - 1);
	//std::vector<Vector2> uvs;

	for (int i = 0; i < divsTheta; i++)
	{
		float phi = -PI / 2;
		for (int j = 0; j < divsPhi; j++)
		{
			Vector3 v1(cos(theta) * cos(phi), sin(phi), sin(theta) * cos(phi)); // TODO: sin/cos of sum?
			//uvs.push_back(Vector2(theta / (2 * PI), (phi + PI / 2) / PI));
			phi += dp;
			//PosNormal pn;
			//pn.v = v1;
			mSphereMesh.vertices.push_back(v1);
			v1.Normalize();
			//pn.n = v1;
			//vertices.push_back(pn);
			mSphereMesh.normals.push_back(v1);
		}
		theta += dt;
	}

	for (int i = 0; i < divsTheta; i++)
	{
		for (int j = 0; j < divsPhi - 1; j++)
		{
			int ni = (i + 1) % divsTheta;
			int nj = (j + 1) % divsPhi;
			int i1 = (i * divsPhi + j);
			int i2 = (ni * divsPhi + j);
			int i3 = (i * divsPhi + nj);
			int i4 = (ni * divsPhi + nj);
			
			mSphereMesh.indices.push_back(i1);
			mSphereMesh.indices.push_back(i2);
			mSphereMesh.indices.push_back(i3);

			mSphereMesh.indices.push_back(i2);
			mSphereMesh.indices.push_back(i4);
			mSphereMesh.indices.push_back(i3);
		}
	}

	// create box mesh
	for (int i = 0; i < 3; i++) // the current axis
	{
		for (int j = 0; j < 2; j++) // the direction of the axis
		{
			for (int k = 0; k < 2; k++) // the current triangle
			{
				Vector3 a, b, c;
				float sgnDir = -1.f + 2 * j;
				float sgnTr = -1.f + 2 * k;
				Vector3 n;
				a[i] = sgnDir; b[i] = sgnDir; c[i] = sgnDir;
				n[i] = sgnDir;
				int axis1 = (i + 1) % 3;
				int axis2 = (i + 2) % 3;
				if (sgnDir < 0)
				{
					a[axis1] = -1; a[axis2] = -1;
					b[axis1] = -sgnTr; b[axis2] = 1;
					c[axis1] = 1; c[axis2] = sgnTr;
				}
				else
				{
					a[axis1] = -1; a[axis2] = -1;
					b[axis1] = 1; b[axis2] = sgnTr;
					c[axis1] = -sgnTr; c[axis2] = 1;
				}

				mBoxMesh.vertices.push_back(a);
				mBoxMesh.vertices.push_back(b);
				mBoxMesh.vertices.push_back(c);

				mBoxMesh.normals.push_back(n);
				mBoxMesh.normals.push_back(n);
				mBoxMesh.normals.push_back(n);

				mBoxMesh.indices.push_back(mBoxMesh.vertices.size() - 1);
				mBoxMesh.indices.push_back(mBoxMesh.vertices.size() - 2);
				mBoxMesh.indices.push_back(mBoxMesh.vertices.size() - 3);
			}
		}
	}
#endif
}

UnifiedView::~UnifiedView()
{
	for (size_t i = 0; i < mViews.size(); i++)
	{
		delete mViews[i];
	}
#ifdef USE_ALEMBIC	
	for (size_t i = 0; i < mMeshes.size(); i++)
	{
		delete mMeshes[i];
		delete mTransforms[i];
	}
	delete mArchive;
#endif
}

Physics::SoftFEM* UnifiedView::ConstructCantilever(int width, int height, int depth)
{
	Physics::SoftFEM* soft = new Physics::SoftFEM();
	size_t n = width*height*depth;
	mSystem.AddGroup(std::shared_ptr<Physics::Group>(soft), n);
	float scale = 1.f;

	// create nodes
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < depth; k++)
			{
				mSystem.GetBodies()[soft->GetStart() + i*height*depth + j*depth + k].pos = scale * Vector3((float)i, (float)j, (float)k);
			}
		}
	}

	// create tets
	std::vector<unsigned int> indices;
	for (int i = 0; i < width - 1; i++)
	{
		for (int j = 0; j < height - 1; j++)
		{
			for (int k = 0; k < depth - 1; k++)
			{
				// For each block, the 8 corners are numerated as:
				//     4*-----*7
				//     /|    /|
				//    / |   / |
				//  5*-----*6 |
				//   | 0*--|--*3
				//   | /   | /
				//   |/    |/
				//  1*-----*2
				unsigned int p0 = i*height*depth + j*depth + k;
				unsigned int p1 = p0 + 1;
				unsigned int p3 = (i + 1)*height*depth + j*depth + k;
				unsigned int p2 = p3 + 1;
				unsigned int p7 = (i + 1)*height*depth + (j + 1)*depth + k;
				unsigned int p6 = p7 + 1;
				unsigned int p4 = i*height*depth + (j + 1)*depth + k;
				unsigned int p5 = p4 + 1;

				// Ensure that neighboring tetras are sharing faces
				if ((i + j + k) % 2 == 1)
				{
					soft->AddTetrahedron(p2, p1, p6, p3);
					soft->AddTetrahedron(p6, p3, p4, p7);
					soft->AddTetrahedron(p4, p1, p6, p5);
					soft->AddTetrahedron(p3, p1, p4, p0);
					soft->AddTetrahedron(p6, p1, p4, p3);
				}
				else
				{
					soft->AddTetrahedron(p0, p2, p5, p1);
					soft->AddTetrahedron(p7, p2, p0, p3);
					soft->AddTetrahedron(p5, p2, p7, p6);
					soft->AddTetrahedron(p7, p0, p5, p4);
					soft->AddTetrahedron(p0, p2, p7, p5);
				}
			}
		}
	}

	soft->DistributeMass(/*(float)n*/1.f, mSystem.GetBodies());

	// fix nodes
	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < height; j++)
		{
			for (int k = 0; k < depth; k++)
				mSystem.GetBodies()[soft->GetStart() + i*height*depth + j*depth + k].invMass = 0;
		}
	}

	return soft;
}

Physics::Cloth* UnifiedView::ConstructCloth(int divisions, float inc, Vector3 offset, bool horizontal, int attached, float mass, float stiffness, float damping)
{
	// init a cloth group
	Physics::Cloth* cloth = new Physics::Cloth();
	//cloth->SetThickness(inc * 0.6f);
	cloth->GetMesh().Clear();

	//isQuadMesh = true;
	//mDivisions = divisions;
	const int n = divisions;
	int numParticles = n * n;
	//mModel->Clear();
	//cloth->SetNumParticles(numParticles);
	uint32 group = mSystem.AddGroup(std::shared_ptr<Physics::Group>(cloth), numParticles);

	float scale = 1.f;// / unit;
	//float totalMass = 15.0f;
	float particleMass = mass / numParticles;

	float x = -0.5f * divisions * inc;
	const float startY = x;
	const float incUV = 1.f / n;
	// TODO: proportional to divisions
	const float h = 0.016f;
	const float stretch = stiffness; //mModel->GetStiffness();
	const float shear = stretch * 0.1f; //mModel->GetShearFactor();
	const float bend = stretch * 0.01f; //mModel->GetBendFactor();
	cloth->SetDamping(damping);
	cloth->GetMesh().vertices.resize(numParticles);
	//mMesh.uvs.resize(numParticles);
	for (int j = 0, base = 0; j < n; j++, base += n)
	{
		Vector3 pos;
		if (horizontal)
			pos = (Vector3(x, 0, startY) + offset) * scale;
		else
			pos = (Vector3(x, startY, 0) + offset) * scale;
		mSystem.GetBodyFromGroup(base, cloth).pos = pos;
		cloth->GetMesh().vertices[base] = pos;
		mSystem.GetBodyFromGroup(base, cloth).vel.SetZero();
		mSystem.GetBodyFromGroup(base, cloth).invMass = 1.f / particleMass;
		//Vector2 uv(j * incUV, 0);
		//mSystem.GetBodyFromGroup(base, cloth).uv.Set(x, 0);
		//mMesh.uvs[base] = uv;
		if (base >= n)
		{
			cloth->AddLink(mSystem.GetBodies(), base - n, base, stretch);
			cloth->AddLink(mSystem.GetBodies(), base - n + 1, base, shear);
			if (base >= 2 * n)// && !mModel->GetDihedral())
				cloth->AddLink(mSystem.GetBodies(), base - 2 * n, base, bend);
		}
		for (int i = 1; i < n; i++) 
		{
			int idx = base + i;
			if (horizontal)
				pos = (Vector3(x, -i * 0.6f, startY + inc * i) + offset) * scale;
			else
				pos = (Vector3(x, startY + inc * i, /*i * 0.5f*/0) + offset) * scale;
			mSystem.GetBodyFromGroup(idx, cloth).pos = pos;
			cloth->GetMesh().vertices[idx] = pos;
			mSystem.GetBodyFromGroup(idx, cloth).vel.SetZero();
			mSystem.GetBodyFromGroup(idx, cloth).invMass = 1.f / particleMass;
			//Vector2 uv(j * incUV, i * incUV);
			//mModel->GetParticle(idx).uv.Set(x, inc * i);
			//mMesh.uvs[idx] = uv;
			if (idx > base)
			{
				cloth->AddLink(mSystem.GetBodies(), idx - 1, idx, stretch);
				if (idx > base + 1)// && !mModel->GetDihedral())
					cloth->AddLink(mSystem.GetBodies(), idx - 2, idx, bend);
				if (idx >= n)
				{
					cloth->AddLink(mSystem.GetBodies(), idx - n, idx, stretch);
					if (i < n - 1)
						cloth->AddLink(mSystem.GetBodies(), idx - n + 1, idx, shear);
					if (j > 1)// && !mModel->GetDihedral())
						cloth->AddLink(mSystem.GetBodies(), idx - 2 * n, idx, bend);
				}
				// shear links
				if (idx > n)
				{
					cloth->AddLink(mSystem.GetBodies(), idx - n - 1, idx, shear);
					if ((i & 1) == (j & 1))
					{
						cloth->AddTriangle(idx - n - 1, idx, idx - n);
						cloth->AddTriangle(idx - n - 1, idx - 1, idx);
						cloth->GetMesh().AddTriangle(idx - n - 1, idx, idx - n);
						cloth->GetMesh().AddTriangle(idx - n - 1, idx - 1, idx);
					} else 
					{
						cloth->AddTriangle(idx - n, idx - 1, idx);
						cloth->AddTriangle(idx - 1, idx - n, idx - n - 1);
						cloth->GetMesh().AddTriangle(idx - n, idx - 1, idx);
						cloth->GetMesh().AddTriangle(idx - 1, idx - n, idx - n - 1);
					}
				}
			}
		}
		if (attached && (j == 0 || j == n - 1))
		{
			mSystem.GetBodyFromGroup(base + n - 1, cloth).invMass = 0.f;
			if (attached == 2)
				mSystem.GetBodyFromGroup(base, cloth).invMass = 0.f;
		}
		x += inc;
	}

	// create a mesh collidable
	//cloth->UpdateMesh(mSystem.GetBodies());
	//Physics::CollisionMesh mesh(&cloth->GetMesh());
	//mesh.mUserIndex = cloth->GetStart();
	//mesh.mGroupIndex = group;
	//mesh.mGroup = cloth;
	//mesh.mTolerance = cloth->GetThickness();
	//uint32 meshId = mSystem.mCollision.AddCollidable(std::make_shared<Physics::CollisionMesh>(mesh));
	//cloth->mBtMesh = (btGImpactMeshShape*)mSystem.mCollision.mBtShapes[meshId].mBtShape.get();

	// add a sphere for each particle
	//for (int i = 0; i < numParticles; i++)
	//{
	//	auto& body = mSystem.GetBodyFromGroup(i, cloth);
	//	Physics::Sphere sph;
	//	sph.radius = cloth->GetThickness();
	//	sph.center = body.pos;
	//	sph.mUserData = 3;
	//	sph.mGroupIndex = group;
	//	sph.mGroup = cloth;
	//	sph.mUserIndex = i + cloth->GetStart();
	//	body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Sphere>(sph));
	//}

	// build unique edges array
	std::vector<Mesh::Edge> duplicates(cloth->GetMesh().indices.size() - 1);
	for (size_t i = 0; i < cloth->GetMesh().indices.size() - 1; i++)
	{
		// construct edge
		int i1 = cloth->GetMesh().indices[i];
		int i2 = cloth->GetMesh().indices[i + 1];
		if ((i + 1) % 3 == 0)
		{
			i2 = cloth->GetMesh().indices[i - 2];
		}
		if (i1 > i2)
			std::swap(i1, i2);
		Mesh::Edge edge(i1, i2);
		edge.t1 = i / 3;
		duplicates[i] = edge;
		//ASSERT(i1 != i2); // FIXME
	}
	// sort it so we can find duplicates
	std::sort(duplicates.begin(), duplicates.end(), Geometry::CompareEdges);

	Mesh::Edge currEdge = duplicates[0];
	for (size_t i = 1; i < duplicates.size(); i++)
	{
		if (duplicates[i].i1 == duplicates[i - 1].i1 && duplicates[i].i2 == duplicates[i - 1].i2)
		{
			currEdge.t2 = duplicates[i].t1;
			continue;
		}
		cloth->AddEdge(currEdge);
		currEdge = duplicates[i];
	}
	cloth->AddEdge(currEdge);

	// create a cloth view object
	ClothView* clothView = new ClothView(cloth, &mSystem);
	mViews.push_back(clothView);

	//cloth->UpdateMesh(mSystem.GetBodies());
	cloth->GetMesh().ComputeNormals();
	return cloth;
}

void UnifiedView::ExportToText(FILE* txt)
{
	for (size_t i = 0; i < mSystem.mCollision.GetNumCollidables(); i++)
	{
		auto shape = mSystem.mCollision.GetCollidable(i);
		if (shape->mType == Physics::CT_SPHERE)
		{
			const Physics::Sphere* sph = (const Physics::Sphere*)shape;
			fprintf(txt, "2, %f, %f, %f, %f, %f, %f, %f, %f,\n",
				sph->center.X(), sph->center.Y(), sph->center.Z(),
				sph->rot.s, sph->rot.v.X(), sph->rot.v.Y(), sph->rot.v.Z(), sph->radius);
		}
		if (shape->mType == Physics::CT_BOX)
		{
			Physics::Box* box = (Physics::Box*)shape;
			fprintf(txt, "1, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f,\n",
				box->center.X(), box->center.Y(), box->center.Z(),
				box->rot.s, box->rot.v.X(), box->rot.v.Y(), box->rot.v.Z(), 
				box->D.X(), box->D.Y(), box->D.Z());
		}
		if (shape->mType == Physics::CT_MESH)
		{
			Physics::CollisionMesh* collMesh = (Physics::CollisionMesh*)shape;
			ExportMeshToText(txt, *(collMesh->mesh), Vector3(0, 0, 1), collMesh->center, collMesh->rot);
		}
	}
}

void UnifiedView::ExportToPovRay(FILE* txt)
{
	fprintf(txt, "#include \"colors.inc\"\n");
	fprintf(txt, "#include \"stones.inc\"\n");
	fprintf(txt, "#declare CheckerSphere = \
				 sphere { \
				 <0, 0, 0>, 1 \
				 texture { \
				 pigment { checker White Red }	\
				 } \
				 }\n");
	fprintf(txt, "camera {\n \
				 location <70, 40, 100> \n \
				 look_at  <0, -15,  10> } \n");
	fprintf(txt, "#declare Box=box {<-1, -1, -1>, < 1, 1, 1> texture { pigment { Magenta} }}\n");
	fprintf(txt, "#declare Box1=box {<-1, -1, -1>, < 1, 1, 1> texture { pigment { Pink } }}\n");
	//fprintf(txt, "light_source { <20, 40, -30> color Gray20}\n");
	fprintf(txt, "global_settings { \
				 max_trace_level 3 \
				 radiosity { \
				 pretrace_start 0.08 \
				 pretrace_end   0.01 \
				 count 50 \
				 nearest_count 10 \
				 recursion_limit 1 \
				 low_error_factor 0.2 \
				 gray_threshold 0 \
				 minimum_reuse 0.015 \
				 brightness 1.0 \
				 adc_bailout 0.01 }}\n");
	//fprintf(txt, "sky_sphere { pigment { gradient y	color_map {	[0, 1  color Gray50 color Gray80] }	} }\n");
	fprintf(txt, "sky_sphere{\
				 pigment{ gradient <0,1,0>\
				 color_map{\
				 [0.0 color rgb<1,1,1>        ]\
				 [0.8 color rgb<0.5,0.5,0.5>]\
				 [1.0 color rgb<0.3,0.3,0.3>]}}}\n");

	// draw collidables
	for (size_t i = 0; i < mSystem.mCollision.GetNumCollidables(); i++)
	{
		auto shape = mSystem.mCollision.GetCollidable(i);
		if (shape->mType == Physics::CT_SPHERE)
		{
			const Physics::Sphere* sph = (const Physics::Sphere*)shape;
			Matrix3 R = !qToMatrix(sph->rot);
			fprintf(txt, "object { CheckerSphere scale %f \
						 matrix <%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f> }\n", sph->radius, 
						 R.m[0][0], R.m[0][1], R.m[0][2],
						 R.m[1][0], R.m[1][1], R.m[1][2],
						 R.m[2][0], R.m[2][1], R.m[2][2],
						 sph->center.X(), sph->center.Y(), sph->center.Z());
		}
		if (shape->mType == Physics::CT_BOX)
		{
			Physics::Box* box = (Physics::Box*)shape;
			Matrix3 R = !qToMatrix(box->rot);
			if (box->mUserData == 0)
			{
				fprintf(txt, "object { Box scale <%f, %f, %f> \
							 matrix <%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f> }\n",
							 box->D.X(), box->D.Y(), box->D.Z(),
							 R.m[0][0], R.m[0][1], R.m[0][2],
							 R.m[1][0], R.m[1][1], R.m[1][2],
							 R.m[2][0], R.m[2][1], R.m[2][2],
							 box->center.X(), box->center.Y(), box->center.Z());
			}
			else
			{
				fprintf(txt, "object { Box1 scale <%f, %f, %f> \
							 matrix <%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f> }\n",
							 box->D.X(), box->D.Y(), box->D.Z(),
							 R.m[0][0], R.m[0][1], R.m[0][2],
							 R.m[1][0], R.m[1][1], R.m[1][2],
							 R.m[2][0], R.m[2][1], R.m[2][2],
							 box->center.X(), box->center.Y(), box->center.Z());
			}
		}
		if (shape->mType == Physics::CT_MESH && shape->mUserData == 0)
		{
			const Physics::CollisionMesh* collMesh = (const Physics::CollisionMesh*)shape;
			const Mesh* mesh = collMesh->mesh;
			fprintf(txt, "mesh2 {\n");
			fprintf(txt, "\tvertex_vectors { %d", mesh->vertices.size());
			for (size_t j = 0; j < mesh->vertices.size(); j++)
			{
				fprintf(txt, "\t\t, <%f,%f,%f>", mesh->vertices[j].X(), mesh->vertices[j].Y(), mesh->vertices[j].Z());
			}
			fprintf(txt, "\t}\n");
			//fprintf(txt, "\tnormal_vectors { %d", mesh->normals.size());
			//for (size_t j = 0; j < mesh->normals.size(); j++)
			//{
			//	fprintf(txt, "\t\t, <%f,%f,%f>", mesh->normals[j].X(), mesh->normals[j].Y(), mesh->normals[j].Z());
			//}
			//fprintf(txt, "\t}\n");
			fprintf(txt, "\tface_indices { %d", mesh->indices.size() / 3);
			for (size_t j = 0; j < mesh->indices.size(); j += 3)
			{
				fprintf(txt, "\t\t, <%d,%d,%d>", mesh->indices[j], mesh->indices[j + 1], mesh->indices[j + 2]);
			}
			fprintf(txt, "\t}\n");
			fprintf(txt, "}\n");
		}
	}
}

void UnifiedView::InitRigids()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsPos(10);
	mSystem.GetSolver().SetNumIterationsVel(0);

	//auto cloth = ConstructCloth(35, 5.f, Vector3(0, 35, 0), true, 2);

	// init rigids group
	Physics::Rigids* rigids = new Physics::Rigids();
	rigids->SetFriction(0.3f);
	rigids->SetBaumgarte(0.4f);
	int numRigids = 100;
	mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), numRigids + 1);

	// add a bunny
	bool ret = LoadMesh("Models/bunny.obj", mMesh, Vector3(0, 0, 0), 1.0f);
	//bool ret = LoadCollisionMesh("Models/sphere_low.dae", mMesh, Vector3(0, 0, 0), 10.0f);
	float meshRadius;
	Vector3 meshCenter;
	mMesh.GetBoundingSphere(meshCenter, meshRadius);
	//if (ret)
	//{				
	//	Physics::Body& body = mSystem.GetBodyFromGroup(numRigids + 1, rigids);
	//	Physics::CollisionMesh collMesh(&mMesh);
	//	collMesh.mUserIndex = numRigids + 1 + rigids->GetStart();
	//	body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::CollisionMesh>(collMesh));
	//	// sphere inertia for now
	//	float mr = 3.f * Physics::Constants::unit;
	//	body.invMass = 0.01f; //3.f / (4.f * PI * mr * mr * mr);
	//	body.Icinv = ((5.f * body.invMass) / (2 * mr * mr)) * Matrix3();

	//	body.rigid = true;
	//	body.pos.Set(0, 20, 0);
	//	body.R = qToMatrix(body.q); // TODO: remove
	//}

	float radius = 2.f;
	radius = meshRadius;// * 0.7f;
	float spacing = 2.02f * radius;
	int side = max(2, (int)(pow(numRigids, 1.f / 3.f)));
	int k = 0;
	float half = (side - 1) * spacing * 0.5f;
	float maxY = spacing; //half;
	float y = maxY;
	while (k < numRigids)
	{
		float x = -half;
		for (int i = 0; i < side && k < numRigids; i++)
		{
			float z = -half;
			for (int j = 0; j < side && k < numRigids; j++)
			{					
				Physics::Body& body = mSystem.GetBodyFromGroup(k, rigids);
				//if (ret)
				//{
				//	Physics::CollisionMesh collMesh(&mMesh);
				//	collMesh.center.Set(x, y, z);
				//	collMesh.mUserIndex = k + rigids->GetStart();
				//	collMesh.tree = ComputeMeshTree(mMesh, Physics::ATF_VERTICES, 10, collMesh.mTolerance);
				//	collMesh.mTolerance = 0.1f;
				//	body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::CollisionMesh>(collMesh));
				//	// sphere inertia for now
				//	float mr = 3.f * Physics::Constants::unit;
				//	body.invMass = 0.35f; //3.f / (4.f * PI * mr * mr * mr);
				//	body.Icinv = ((5.f * body.invMass) / (2 * mr * mr)) * Matrix3();
				//}
				//if ((i + j) % 2 == 0)
				//{
				//	Physics::Sphere sph;
				//	sph.center.Set(x, y, z);
				//	sph.radius = radius;
				//	sph.mUserIndex = k + rigids->GetStart(); // TODO: GetBodyIndex()
				//	sph.mTolerance = 2.f;
				//	body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Sphere>(sph));						
				//	body.invMass = 2.f;
				//	body.Icinv = ((5.f * body.invMass) / (2 * radius * radius * Physics::Constants::unit * Physics::Constants::unit)) * Matrix3(); // TODO: automate
				//}
				//else
				{
					Physics::Box box;
					box.center.Set(x, y, z);
					box.D.Set(1.2f, 1.5f, 1.2f);
					//box.D.Scale(5.f);
					box.mUserIndex = k + rigids->GetStart(); // TODO: GetBodyIndex()
					body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Box>(box));
					body.Icinv = Matrix3((12.f * body.invMass / (Physics::Constants::unit * Physics::Constants::unit)) *
						Vector3(1.f / (box.D.Y() * box.D.Y() + box.D.Z() * box.D.Z()),
						1.f / (box.D.X() * box.D.X() + box.D.Z() * box.D.Z()),
						1.f / (box.D.Y() * box.D.Y() + box.D.X() * box.D.X())));
				}
				//{
				//	Physics::Capsule cap;
				//	cap.center.Set(x, y, z);
				//	cap.hh = 1.f;
				//	cap.r = 1.f;
				//	cap.mUserIndex = k + rigids->GetStart();
				//	body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Capsule>(cap));
				//	// TODO: capsule inertia
				//	//body.Icinv = ((5.f * body.invMass) / (2 * cap.r * cap.r * Physics::Constants::unit * Physics::Constants::unit)) * Matrix3();
				//	float ylen = cap.r + cap.hh;
				//	float zlen = cap.r;
				//	float xlen = cap.r;
				//	body.Icinv = Matrix3((12.f * body.invMass / (Physics::Constants::unit * Physics::Constants::unit)) *
				//		Vector3(1.f / (ylen * ylen + zlen * zlen),
				//		1.f / (xlen * xlen + zlen * zlen),
				//		1.f / (ylen * ylen + xlen * xlen)));
				//}

				//rigids->AddRigid(rigid);
				body.rigid = true;
				//body.invMass = 0.1f;
				body.pos.Set(x, y, z);
				//body.w.Set(1.f, 0, 0);
				body.vel.Set(GetRandomReal11(), GetRandomReal11(), GetRandomReal11());
				//body.vel.Scale(10);
				//body.q = Quaternion::Rotation(0.1f, Vector3(0, 0, 1));
				body.R = qToMatrix(body.q); // TODO: automate

				k++;
				z += spacing;
			}
			x += spacing;
		}
		y -= spacing;
	}

	// create a ground
	{
		Physics::Body& body = mSystem.GetBodyFromGroup(numRigids, rigids);
		body.invMass = 0;
		body.Icinv = Matrix3::Zero(); // TODO: default?
		body.Iinv = body.Icinv; // TODO: automate
		Physics::Box ground;
		ground.D.Set(400, 10.5f, 400);
		ground.mUserData = 1;
		ground.mUserIndex = numRigids + rigids->GetStart();
		body.pos.Set(0, -50, 0);
		ground.center = body.pos;
		ground.mTolerance = 0.1f;
		body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Box>(ground));
	}

	//auto cloth1 = ConstructCloth(25, 3.f, Vector3(0, -25, 50), true, 2);

	// create a view object
	RigidsView* rigidsView = new RigidsView(rigids, &mSystem);
	mViews.push_back(rigidsView);

	//float he;
	//ConstructGranular(0.8f, 100, Vector3(0, 25, 0), Vector3(), he);

	// add coupling between cloth and granular
	//Physics::ClothGranularCoupler* coupler = new Physics::ClothGranularCoupler();
	//mSystem.mCollision.AddCoupler(std::shared_ptr<Physics::ICoupler>(coupler));

	// add coupling between cloth and rigids
	Physics::ClothRigidCoupler* coupler = new Physics::ClothRigidCoupler();
	mSystem.mCollision.AddCoupler(std::shared_ptr<Physics::ICoupler>(coupler));
	//mCoupler = coupler;
}

void UnifiedView::InitStack()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsPos(100);
	mSystem.GetSolver().SetNumIterationsVel(0);

	// init rigids group
	Physics::Rigids* rigids = new Physics::Rigids();
	rigids->SetFriction(0.5f);
	rigids->SetBaumgarte(0.4f);
	//rigids->SetRegularization(.1f);
	//rigids->SetDamping(20 * 0.016f);
	int numRigids = 10;
	mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), numRigids + 1);

	float radius = 1.6f;
	float spacing = 2.02f * radius;
	int side = 1;
	int k = 0;
	float half = (side - 1) * spacing * 0.5f;
	float groundHh = 10.5f;
	float groundY = 0;
	float maxY = groundY + numRigids * spacing + groundHh - radius;
	float y = maxY;
	while (k < numRigids)
	{
		float x = -half;
		for (int i = 0; i < side && k < numRigids; i++)
		{
			float z = -half;
			for (int j = 0; j < side && k < numRigids; j++)
			{					
				Physics::Body& body = mSystem.GetBodyFromGroup(k, rigids);
				{
					Physics::Box box;
					box.center.Set(x, y, z);
					box.D.Set(1.2f, 1.5f, 1.2f);
					box.mTolerance = 0.1f;
					box.mUserIndex = k + rigids->GetStart(); // TODO: GetBodyIndex()
					body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Box>(box));
					body.Icinv = Matrix3((12.f * body.invMass / (Physics::Constants::unit * Physics::Constants::unit)) *
						Vector3(1.f / (box.D.Y() * box.D.Y() + box.D.Z() * box.D.Z()),
						1.f / (box.D.X() * box.D.X() + box.D.Z() * box.D.Z()),
						1.f / (box.D.Y() * box.D.Y() + box.D.X() * box.D.X())));
				}

				body.rigid = true;
				body.pos.Set(x, y, z);
				body.R = qToMatrix(body.q);

				k++;
				z += spacing;
			}
			x += spacing;
		}
		y -= spacing;
	}

	// create a ground
	{
		Physics::Body& body = mSystem.GetBodyFromGroup(numRigids, rigids);
		body.invMass = 0;
		body.Icinv = Matrix3::Zero(); // TODO: default?
		body.Iinv = body.Icinv; // TODO: automate
		Physics::Box ground;
		ground.D.Set(400, groundHh, 400);
		ground.mUserData = 1;
		ground.mUserIndex = numRigids + rigids->GetStart();
		body.pos.Set(0, groundY, 0);
		ground.center = body.pos;
		ground.mTolerance = 0.1f;
		body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Box>(ground));
	}

	// create a view object
	RigidsView* rigidsView = new RigidsView(rigids, &mSystem);
	mViews.push_back(rigidsView);
}

void UnifiedView::InitFriction()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsPos(500);
	mSystem.GetSolver().SetNumIterationsVel(0);

	// init rigids group
	Physics::Rigids* rigids = new Physics::Rigids();
	rigids->SetFriction(0.1f);
	rigids->SetBaumgarte(0.4f);
	//rigids->SetRegularization(.1f);
	//rigids->SetDamping(20 * 0.016f);
	int numRigids = 2;
	mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), numRigids);

	float invMass = 1;
	int idx = 0;
	Matrix3 Icinv;
	auto box = CreateBox(Vector3(0, 1.5f, 0), Quaternion(), Vector3(1.2f, 1.5f, 1.2f), invMass, idx, rigids, Icinv);
	auto& body = AddRigid(rigids, idx, std::make_shared<Physics::Box>(box), invMass, Icinv);
	//body.vel.Set(35.355f, 0, 35.355f);
	body.vel.Set(50.0f, 0, 0);

	// create a ground
	{
		Physics::Body& body = mSystem.GetBodyFromGroup(1, rigids);
		body.invMass = 0;
		body.Icinv = Matrix3::Zero(); // TODO: default?
		body.Iinv = body.Icinv; // TODO: automate
		Physics::Box ground;
		ground.D.Set(400, 10, 400);
		ground.mUserData = 1;
		ground.mUserIndex = 1 + rigids->GetStart();
		body.pos.Set(0, -10, 0);
		ground.center = body.pos;
		ground.mTolerance = 0.1f;
		body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Box>(ground));
	}

	// create a view object
	RigidsView* rigidsView = new RigidsView(rigids, &mSystem);
	mViews.push_back(rigidsView);
}

Physics::Body& UnifiedView::AddRigid(Physics::Rigids* rigids, size_t i, std::shared_ptr<Physics::Collidable> shape, float invMass, const Matrix3& Icinv)
{
	Physics::Body& body = mSystem.GetBodyFromGroup(i, rigids);
    if (shape)
    {
        shape->mUserIndex = rigids->GetStart() + i;
        body.pos = shape->center;
        body.q = shape->rot;
        body.collIdx = mSystem.mCollision.AddCollidable(shape);
    }
	body.invMass = invMass;
	body.Icinv = Icinv;
	body.R = qToMatrix(body.q);
	body.rigid = true; // !!!
	return body;
}

void UnifiedView::AddChandelierChildren(Physics::Rigids* rigids, const Physics::Capsule& cap1, int i1, int i2, int i3, int level)
{
	float invMass = 1;
	Matrix3 Icinv;
	float height = 6;
	float hh = cap1.hh * 0.5f;
	if (level % 2 == 0)
		hh = cap1.hh - 0.5f;
	// add capsule 2
	{
		Vector3 pos;//(0, -height * (level + 1), cap1.hh + cap1.r);
		Quaternion q2;
		if (level % 2 == 0)
		{
			q2.SetAxisAngle(PI / 2, Vector3(0, 0, 1));
			pos = cap1.center + Vector3(0, -height, cap1.hh + cap1.r);
		}
		else
		{
			q2.SetAxisAngle(PI / 2, Vector3(1, 0, 0));
			pos = cap1.center + Vector3(cap1.hh + cap1.r, -height, 0);
		}
		Physics::Capsule cap2 = CreateCapsule(pos, q2, hh, 2, invMass, &Icinv);
		Physics::Body& body2 = AddRigid(rigids, i2, std::make_shared<Physics::Capsule>(cap2), invMass, Icinv);
		//if (level == 1)
			body2.vel.Set(GetRandomReal11(), GetRandomReal11(), GetRandomReal11());

		if (level < mMaxLevels - 1)
			AddChandelierChildren(rigids, cap2, i2, 2 * i2, 2 * i2 + 1, level + 1);
	}

	// add capsule 3
	{
		Vector3 pos;//(0, -height * (level + 1), -cap1.hh - cap1.r);
		Quaternion q3;
		if (level % 2 == 0)
		{
			q3.SetAxisAngle(PI / 2, Vector3(0, 0, 1));
			pos = cap1.center + Vector3(0, -height, -cap1.hh - cap1.r);
		}
		else
		{
			q3.SetAxisAngle(PI / 2, Vector3(1, 0, 0));
			pos = cap1.center + Vector3(-cap1.hh - cap1.r, -height, 0);
		}
		//invMass = 0.5f;
		Physics::Capsule cap3 = CreateCapsule(pos, q3, hh, 2, invMass, &Icinv);
		Physics::Body& body3 = AddRigid(rigids, i3, std::make_shared<Physics::Capsule>(cap3), invMass, Icinv);
		
		body3.vel.Set(GetRandomReal11(), GetRandomReal11(), GetRandomReal11());

		if (level < mMaxLevels - 1)
			AddChandelierChildren(rigids, cap3, i3, 2 * i3, 2 * i3 + 1, level + 1);
	}

	// add joint
	if (level % 2 == 0)
	{
		rigids->AddJoint(mSystem.GetBodies(), i1, i2, cap1.center + Vector3(0, 0, cap1.hh + cap1.r));
		rigids->AddJoint(mSystem.GetBodies(), i1, i3, cap1.center + Vector3(0, 0, -cap1.hh - cap1.r));
	}
	else
	{
		rigids->AddJoint(mSystem.GetBodies(), i1, i2, cap1.center + Vector3(cap1.hh + cap1.r, 0, 0));
		rigids->AddJoint(mSystem.GetBodies(), i1, i3, cap1.center + Vector3(-cap1.hh - cap1.r, 0, 0));
	}
}

void UnifiedView::InitJoints()
{
	mViews.clear();
	mSystem.Clear();

    mSystem.GetSolver().SetNumIterationsPos(0);
    mSystem.GetSolver().SetNumIterationsVel(100);

	Physics::Rigids* rigids = new Physics::Rigids();
	int numRigids = 1 << (mMaxLevels + 1);
	mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), numRigids);

	// add capsule 1
	Physics::Capsule cap1;
	float invMass = 1;
	Matrix3 Icinv;
	Quaternion q1;
	q1.SetAxisAngle(PI / 2, Vector3(1, 0, 0));
	cap1 = CreateCapsule(Vector3(0), q1, 16.f + mMaxLevels * 2, 2, invMass, &Icinv);
	AddRigid(rigids, 1, std::make_shared<Physics::Capsule>(cap1), invMass, Icinv);

	// add dummy object
	Physics::Body& dummy = mSystem.GetBodyFromGroup(0, rigids);
	dummy.invMass = 0;
	dummy.Icinv = Matrix3(0);
	dummy.Iinv = Matrix3(0);
	dummy.rigid = true;

	// add joint
	rigids->AddJoint(mSystem.GetBodies(), 0, 1, Vector3(0, 0, 0));

	AddChandelierChildren(rigids, cap1, 1, 2, 3, 0);
	//mSystem.GetBodyFromGroup(numRigids - 1, rigids).vel.Set(5.f, 0, 0);

	// create a view object
	RigidsView* rigidsView = new RigidsView(rigids, &mSystem);
	mViews.push_back(rigidsView);
}

void ConstructCapsule(float radius, float height, std::vector<Vector3>& vertices, std::vector<Vector3>& normals, std::vector<uint32>& indices)
{
	const int divsTheta = 10;
	const int divsPhi = 5;
	float theta = 0;
	const float dt = 2 * PI / divsTheta;
	const float dp = PI / (divsPhi - 1) / 2;
	std::vector<Vector2> uvs;

	for (int i = 0; i < divsTheta; i++)
	{
		float phi = 0;//PI / 2;
		for (int j = 0; j < divsPhi; j++)
		{
			Vector3 v1(cos(theta) * cos(phi), sin(phi), sin(theta) * cos(phi)); // TODO: sin/cos of sum?
			uvs.push_back(Vector2(theta / (2 * PI), (phi + PI / 2) / PI));
			phi += dp;			
			vertices.push_back(v1 * radius + Vector3(0, height, 0));
			v1.Normalize();			
			normals.push_back(v1);
		}
		theta += dt;
	}
	size_t half = vertices.size();

	theta = 0;
	for (int i = 0; i < divsTheta; i++)
	{
		float phi = 0;//-PI / 2;
		for (int j = 0; j < divsPhi; j++)
		{
			Vector3 v1(cos(theta) * cos(phi), sin(phi), sin(theta) * cos(phi)); // TODO: sin/cos of sum?
			uvs.push_back(Vector2(theta / (2 * PI), (phi + PI / 2) / PI));
			phi -= dp;			
			vertices.push_back(v1 * radius - Vector3(0, height, 0));
			v1.Normalize();			
			normals.push_back(v1);
		}
		theta += dt;
	}

	for (int i = 0; i < divsTheta; i++)
	{
		for (int j = 0; j < divsPhi - 1; j++)
		{
			int ni = (i + 1) % divsTheta;
			int nj = (j + 1) % divsPhi;
			int i1 = (i * divsPhi + j);
			int i2 = (ni * divsPhi + j);
			int i3 = (i * divsPhi + nj);
			int i4 = (ni * divsPhi + nj);

			indices.push_back(i3);
			indices.push_back(i1);
			indices.push_back(i2);
			
			indices.push_back(i3);
			indices.push_back(i2);
			indices.push_back(i4);

			indices.push_back(i2 + half);
			indices.push_back(i1 + half);
			indices.push_back(i3 + half);

			indices.push_back(i4 + half);
			indices.push_back(i2 + half);
			indices.push_back(i3 + half);
		}
	}

	for (int i = 0; i < divsTheta; i++)
	{
		int ni = (i + 1) % divsTheta;

		indices.push_back(ni * divsPhi);
		indices.push_back(i * divsPhi);
		indices.push_back(i * divsPhi + half);

		indices.push_back(i * divsPhi + half);
		indices.push_back(ni * divsPhi + half);
		indices.push_back(ni * divsPhi);
	}
}

void UnifiedView::ExportToAlembic()
{
	PROFILE_SCOPE("ExportAbc");
	MEASURE_TIME("ExportAbc");
#ifdef USE_ALEMBIC
	size_t numColl = mSystem.mCollision.GetNumCollidables();
	if (mMeshes.size() != numColl) // FIXME
	{
		mMeshes.resize(numColl);
		std::fill(mMeshes.begin(), mMeshes.end(), nullptr);
		mTransforms.resize(numColl);
		std::fill(mTransforms.begin(), mTransforms.end(), nullptr);
	}
	for (size_t i = 0; i < numColl; i++)
	{
		auto shape = mSystem.mCollision.GetCollidable(i);
		if (shape->mType == Physics::CT_MESH && shape->mUserData == 0)
		{
			const Physics::CollisionMesh* collMesh = (const Physics::CollisionMesh*)shape;
			const Mesh* mesh = collMesh->mesh;

			if (mMeshes[i] == nullptr)
			{
				Alembic::Abc::TimeSamplingPtr ts( new Alembic::Abc::TimeSampling( 1.0 / 60.0, 0.0 ) );
				std::string name("coll");
				name += std::to_string(i);
				mTransforms[i] = new OXform(mArchive->getTop(), name.c_str(), ts);				
				name += "_mesh";
				mMeshes[i] = new OPolyMesh(*mTransforms[i], name.c_str(), ts);				
			}

			OXformSchema& schema = mTransforms[i]->getSchema();
			XformSample xs;
			float angle;
			Vector3 axis;
			collMesh->rot.GetAxisAngle(angle, axis);
			xs.setTranslation(V3f(shape->center.X(), shape->center.Y(), shape->center.Z()));
			xs.setRotation(V3f(axis.X(), axis.Y(), axis.Z()), angle * 180.f / PI);			
			schema.set(xs);
			
			OPolyMeshSchema &abcMesh = mMeshes[i]->getSchema();			
			size_t numFaces = mesh->indices.size() / 3;
			std::vector<int> counts(numFaces);
			std::fill(counts.begin(), counts.end(), 3);
			OPolyMeshSchema::Sample mesh_samp(
				P3fArraySample( ( const V3f * )&mesh->vertices[0], mesh->vertices.size() ),
				Int32ArraySample( (const int*)&mesh->indices[0], mesh->indices.size() ),
				Int32ArraySample( &counts[0], numFaces ));
			abcMesh.set(mesh_samp);
		}
		else if (shape->mType == Physics::CT_SPHERE)
		{
			const Physics::Sphere* sph = (const Physics::Sphere*)shape;
			const Mesh* mesh = &mSphereMesh;

			if (mMeshes[i] == nullptr)
			{
				Alembic::Abc::TimeSamplingPtr ts( new Alembic::Abc::TimeSampling( 1.0 / 60.0, 0.0 ) );
				std::string name("sphere");
				name += std::to_string(i);
				mTransforms[i] = new OXform(mArchive->getTop(), name.c_str(), ts);				
				name += "_mesh";
				mMeshes[i] = new OPolyMesh(*mTransforms[i], name.c_str(), ts);				

				OPolyMeshSchema &abcMesh = mMeshes[i]->getSchema();			
				size_t numFaces = mesh->indices.size() / 3;
				std::vector<int> counts(numFaces);
				std::fill(counts.begin(), counts.end(), 3);

				OPolyMeshSchema::Sample mesh_samp(
					P3fArraySample( ( const V3f * )&mesh->vertices[0], mesh->vertices.size() ),
					Int32ArraySample( (const int*)&mesh->indices[0], mesh->indices.size() ),
					Int32ArraySample( &counts[0], numFaces ));
				abcMesh.set(mesh_samp);
			}

			OXformSchema& schema = mTransforms[i]->getSchema();
			XformSample xs;
			float angle;
			Vector3 axis;
			shape->rot.GetAxisAngle(angle, axis);
			//Printf("angle: %f, axis: %f %f %f\n", angle, axis.X(), axis.Y(), axis.Z());
			xs.setTranslation(V3f(shape->center.X(), shape->center.Y(), shape->center.Z()));
			xs.setRotation(V3f(axis.X(), axis.Y(), axis.Z()), angle * 180.f / PI);
			xs.setScale(V3f(sph->radius, sph->radius, sph->radius));
			schema.set(xs);

		}
		else if (shape->mType == Physics::CT_BOX)
		{
			const Physics::Box* box = (const Physics::Box*)shape;
			const Mesh* mesh = &mBoxMesh;

			if (mMeshes[i] == nullptr)
			{
				Alembic::Abc::TimeSamplingPtr ts( new Alembic::Abc::TimeSampling( 1.0 / 60.0, 0.0 ) );
				std::string name("box");
				name += std::to_string(i);
				mTransforms[i] = new OXform(mArchive->getTop(), name.c_str(), ts);				
				name += "_mesh";
				mMeshes[i] = new OPolyMesh(*mTransforms[i], name.c_str(), ts);				

				OPolyMeshSchema &abcMesh = mMeshes[i]->getSchema();			
				size_t numFaces = mesh->indices.size() / 3;
				std::vector<int> counts(numFaces);
				std::fill(counts.begin(), counts.end(), 3);
				OPolyMeshSchema::Sample mesh_samp(
					P3fArraySample( ( const V3f * )&mesh->vertices[0], mesh->vertices.size() ),
					Int32ArraySample( (const int*)&mesh->indices[0], mesh->indices.size() ),
					Int32ArraySample( &counts[0], numFaces ));
				abcMesh.set(mesh_samp);			
			}

			OXformSchema& schema = mTransforms[i]->getSchema();
			XformSample xs;
			float angle;
			Vector3 axis;
			shape->rot.GetAxisAngle(angle, axis);		
			xs.setTranslation(V3f(shape->center.X(), shape->center.Y(), shape->center.Z()));
			xs.setRotation(V3f(axis.X(), axis.Y(), axis.Z()), angle * 180.f / PI);
			xs.setScale(V3f(box->D.X(), box->D.Y(), box->D.Z()));
			schema.set(xs);
		}
		if (shape->mType == Physics::CT_CAPSULE)
		{
			const Physics::Capsule* cap = (const Physics::Capsule*)shape;

			if (mMeshes[i] == nullptr)
			{
				Alembic::Abc::TimeSamplingPtr ts( new Alembic::Abc::TimeSampling( 1.0 / 60.0, 0.0 ) );
				std::string name("capsule");
				name += std::to_string(i);
				mTransforms[i] = new OXform(mArchive->getTop(), name.c_str(), ts);				
				name += "_mesh";
				mMeshes[i] = new OPolyMesh(*mTransforms[i], name.c_str(), ts);				

				std::vector<Vector3> vertices;
				std::vector<Vector3> normals;
				std::vector<uint32> indices;
				ConstructCapsule(cap->r, cap->hh, vertices, normals, indices);

				OPolyMeshSchema &abcMesh = mMeshes[i]->getSchema();			
				size_t numFaces = indices.size() / 3;
				std::vector<int> counts(numFaces);
				std::fill(counts.begin(), counts.end(), 3);
				OPolyMeshSchema::Sample mesh_samp(
					P3fArraySample( ( const V3f * )&vertices[0], vertices.size() ),
					Int32ArraySample( (const int*)&indices[0], indices.size() ),
					Int32ArraySample( &counts[0], numFaces ));
				abcMesh.set(mesh_samp);
			}

			OXformSchema& schema = mTransforms[i]->getSchema();
			XformSample xs;
			float angle;
			Vector3 axis;
			cap->rot.GetAxisAngle(angle, axis);
			xs.setTranslation(V3f(shape->center.X(), shape->center.Y(), shape->center.Z()));
			xs.setRotation(V3f(axis.X(), axis.Y(), axis.Z()), angle * 180.f / PI);			
			schema.set(xs);
		}
	}

	// export each group
	for (size_t i = 0; i < mViews.size(); i++)
	{
		mViews[i]->ExportToAlembic(mArchive);
	}
#endif // !USE_ALEMBIC
}

void UnifiedView::Clear()
{
    mViews.clear();
    mSystem.Clear();
}

Physics::Granular* UnifiedView::ConstructGranular(float radius, int numParticles, const Vector3& offset, const Vector3& velocity, float& halfExtent)
{
	// init a granular group
	Physics::Granular* granular = new Physics::Granular();
	//granular->SetNumParticles(numParticles);
	granular->SetRadius(radius);		
	uint32 group = mSystem.AddGroup(std::shared_ptr<Physics::Group>(granular), numParticles);

	//phys3D.SetCollisionFlags(PhysicsSystem3D::COLL_WALLS | PhysicsSystem3D::COLL_MESH | PhysicsSystem3D::COLL_PARTICLES);
	const float spacing = 2.2f * radius;
	const float maxVel = 0.1f * Physics::Constants::unit;

	int side = /*divisions > 0 ? divisions : */(int)(pow(numParticles, 1.f / 3.f) + 1);
	int k = 0;
	float half = (side - 1) * spacing * 0.5f;
	float maxY = half;
	float y = maxY;
	while (k < numParticles)
	{
		float x = -half;
		for (int i = 0; i < side && k < numParticles; i++)
		{
			float z = -half;
			for (int j = 0; j < side && k < numParticles; j++)
			{
				mSystem.GetBody(k + granular->GetStart()).pos = Vector3(x, y, z) + offset;
				if (velocity.LengthSquared() == 0)
					mSystem.GetBody(k + granular->GetStart()).vel = maxVel * Vector3(GetRandomReal11(), GetRandomReal11(), GetRandomReal11());
				else
					mSystem.GetBody(k + granular->GetStart()).vel = velocity;

				Physics::Sphere sph;
				sph.radius = radius;
				sph.center = Vector3(x, y, z) + offset;
				sph.mUserData = 2;
				sph.mGroupIndex = group;
				sph.mGroup = granular;
				sph.mUserIndex = k + granular->GetStart();
				sph.mTolerance = 0.1f;
				mSystem.GetBody(k + granular->GetStart()).collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Sphere>(sph));

				k++;
				z += spacing;
			}
			x += spacing;
		}
		y -= spacing;
	}

	if (halfExtent == 0)
		halfExtent = half + spacing;
	granular->Init(2 * halfExtent, 2 * halfExtent, 2 * halfExtent);

	// create a granular view object
	GranularView* granularView = new GranularView(granular, &mSystem);
	mViews.push_back(granularView);

	return granular;
}

Physics::SoftFEM* SoftFEMView::LoadFromVolFile(const char* path, UnifiedPhysics& sys, float scale, const Vector3& offset)
{
	// init a soft body FEM group
	Physics::SoftFEM* soft = new Physics::SoftFEM();

	FILE *ptr_file;
	char buf[1000];

	const int STATE_READ = 0;
	const int STATE_VOL = 1;
	const int STATE_ELEM = 2;
	const int STATE_POINTS = 3;
	const int STATE_POINT = 4;

	errno_t ret = fopen_s(&ptr_file, path, "r");
	if (ret != 0)
	{
		Printf("Couldn't open file %s\n", path);
		return false;
	}

	int state = STATE_READ;
	int nod = 0;
	while (fgets(buf, 1000, ptr_file) != NULL)
	{
		int counter = 0;
		char* ptr = buf;
		char* word = buf;
		int idx[4];
		switch (state)
		{
		case STATE_READ:
			if (strncmp(buf, "volumeelements", 14) == 0)
			{
				state = STATE_VOL;
			}
			else if (strncmp(buf, "points", 6) == 0)
			{
				state = STATE_POINTS;
			}
			break;
		case STATE_VOL:
			{
				int n = atoi(buf);
				state = STATE_ELEM;
			}
			break;
		case STATE_ELEM:
			while (ptr = strchr(word, ' '))
			{
				*ptr = '\0';
				counter++;
				int x = atoi(word);
				if (counter > 2)
				{
					idx[counter - 3] = x - 1;
				}
				word = ptr + 1;
			}
			idx[3] = atoi(word) - 1;
			if (counter == 0)
				state = STATE_READ;
			else
				soft->AddTetrahedron(idx[0], idx[1], idx[2], idx[3]);
			break;
		case STATE_POINTS:
			{
				int n = atoi(buf);
				//SetNumNodes(n);
				sys.AddGroup(std::shared_ptr<Physics::Group>(soft), n);
				state = STATE_POINT;
			}
			break;
		case STATE_POINT:
			Vector3 pos;
			while (ptr = strchr(word, ' '))
			{
				if (ptr == word)
				{
					word++;
					continue;
				}
				*ptr = '\0';
				float x = (float)atof(word);
				pos[counter] = x;
				counter++;
				word = ptr + 1;
			}
			pos[2] = (float)atof(word);
			if (counter == 0)
				state = STATE_READ;
			else
				//nodes->at(nod++).pos = scale * pos;
				sys.GetBodies()[soft->GetStart() + nod++].pos = scale * pos + offset;
			break;
		}
	}

	fclose(ptr_file);
	return soft;
}

Physics::SoftFEM* SoftFEMView::LoadFromTetFile(const char* path, UnifiedPhysics& sys, float scale, const Vector3& offset)
{
	// init a soft body FEM group
	Physics::SoftFEM* soft = new Physics::SoftFEM();

	FILE *ptr_file;
	char buf[1000];

	errno_t ret = fopen_s(&ptr_file, path, "r");
	if (ret != 0)
	{
		printf("Couldn't open file %s\n", path);
		return nullptr;
	}
	const int STATE_READ = 0;
	const int STATE_VOL = 1;
	const int STATE_ELEM = 2;
	const int STATE_POINTS = 3;
	const int STATE_POINT = 4;
	int state = STATE_READ;
	int nod = 0;
	while (fgets(buf, 1000, ptr_file) != NULL)
	{
		char* ptr = buf;
		char* word = buf;

		switch (state)
		{
		case STATE_READ:
			{	
				if (strncmp(buf, "num_vertices", 12) == 0)
				{
					word = ptr + 12;
					int n = atoi(word);
					//nodes.resize(n);
					sys.AddGroup(std::shared_ptr<Physics::Group>(soft), n);
				}
				else if (strncmp(buf, "VERTICES", 8) == 0)
				{
					state = STATE_POINT;
				}
				else if (strncmp(buf, "TETRAS", 6) == 0)
				{
					state = STATE_ELEM;
				}
				break;
			}
		case STATE_ELEM:
			{
				int indx[4];
				int counter = 0;
				while (ptr = strchr(word, ' '))
				{
					*ptr = '\0';

					int x = atoi(word);
					indx[counter] = x;
					counter++;
					word = ptr + 1;
				}
				if (counter == 0)
				{
					state = STATE_READ;
					break;
				}
				soft->AddTetrahedron(indx[0], indx[1], indx[2], indx[3]);
				break;
			}
		case STATE_POINT:
			{
				Vector3 pos;
				int counter = 0;
				while (ptr = strchr(word, ' '))
				{
					if (ptr == word)
					{
						word++;
						continue;
					}
					*ptr = '\0';
					float x = (float)atof(word);
					pos[counter] = x;
					counter++;
					word = ptr + 1;
				}
				if (counter == 0)
				{
					state = STATE_ELEM;
					break;
				}
				sys.GetBodies()[soft->GetStart() + nod++].pos = scale * pos + offset;
				break;
			}

		}
	}

	fclose(ptr_file);
	return soft;
}

void UnifiedView::InitCoupling()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsPos(30);

	Vector3 offset(17, 100, 0);
	float scale = 5.f;
	auto soft = SoftFEMView::LoadFromVolFile("Models/torus2.vol", mSystem, scale, offset);

	float e = 0.5f * 1000000.f / (Physics::Constants::unit * Physics::Constants::unit); // Young's modulus in GPa
	float nu = 0.1f;
	soft->Init(mSystem.GetBodies(), e, nu);
	//soft->SetVelocity(mSystem.GetBodies(), Vector3(0, 0, 25));

	// create a view object
	SoftFEMView* softView = new SoftFEMView(soft, &mSystem);
	softView->mCollMesh = soft->GetCollisonMesh();
	softView->mCollMesh->ComputeNormals();

	// add a collidable object for the collision mesh
	auto collMesh = std::make_shared<Physics::CollisionMesh>(softView->mCollMesh);
	collMesh->mGroup = soft;
	collMesh->mUserIndex = soft->GetStart();
	soft->mCollIdx = mSystem.mCollision.AddCollidable(collMesh);

	mViews.push_back(softView); // TODO: AddView

	// set up rigid bodies
	Physics::Rigids* rigids = new Physics::Rigids();
	rigids->SetFriction(0.3f);
	int numRigids = 3;
	mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), numRigids);

	Matrix3 Icinv;
	float invMass;
	Physics::Box box;
	int idx = 0;

	invMass = 0;
	Quaternion q;
	q.SetAxisAngle(0.02f, Vector3(0, 0, 1));
	box = CreateBox(Vector3(-20, -40, 0), q, Vector3(10, 400, 40), invMass, idx, rigids, Icinv);
	box.mUserData = 1;
	AddRigid(rigids, idx, std::make_shared<Physics::Box>(box), invMass, Icinv);

	q.SetAxisAngle(-0.02f, Vector3(0, 0, 1));
	box = CreateBox(Vector3(30, -40, 0), q, Vector3(10, 400, 40), invMass, ++idx, rigids, Icinv);
	box.mUserData = 1;
	AddRigid(rigids, idx, std::make_shared<Physics::Box>(box), invMass, Icinv);

	invMass = 0.1f;
	box = CreateBox(Vector3(1, 100, 0), Quaternion(), Vector3(10, 20, 20), invMass, ++idx, rigids, Icinv);
	AddRigid(rigids, idx, std::make_shared<Physics::Box>(box), invMass, Icinv);

	// add coupling to rigids
	Physics::RigidSoftCoupler* coupler = new Physics::RigidSoftCoupler();
	coupler->SetFriction(0.1f);
	mSystem.mCollision.AddCoupler(std::shared_ptr<Physics::ICoupler>(coupler));
}

void UnifiedView::InitCantilever()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsPos(100);

	auto soft = ConstructCantilever(30, 5, 5);
	float e = 1.f * 1e9f / (Physics::Constants::unit * Physics::Constants::unit); // Young's modulus in GPa
	float nu = 0.3f;
	soft->Init(mSystem.GetBodies(), e, nu);

	// create a view object
	SoftFEMView* softView = new SoftFEMView(soft, &mSystem);
	softView->mCollMesh = soft->GetCollisonMesh(); // TODO: do it in the constructor
	softView->mCollMesh->ComputeNormals();

	// add a collidable object for the collision mesh
	auto collMesh = std::make_shared<Physics::CollisionMesh>(softView->mCollMesh);
	collMesh->mGroup = soft;
	collMesh->mUserIndex = soft->GetStart();
	//collMesh->mUserData = UD_STATIC;
	soft->mCollIdx = mSystem.mCollision.AddCollidable(collMesh);

	mViews.push_back(softView); // TODO: a method to do this
}

SoftFEMView* UnifiedView::ConstructSoftFEM(const char* path, Vector3 offset, float scale)
{
	//float scale = 4.f;
	//Vector3 offset(0, 60, -30);
	//auto soft = SoftFEMView::LoadFromTetFile("cow.tet", mSystem, scale, offset);

	//Vector3 offset(17, 20, 0);
	//float scale = 5.f;
	Physics::SoftFEM* soft;
	if (strncmp(path + strlen(path) - 3, "vol", 3) == 0)
		soft = SoftFEMView::LoadFromVolFile(path, mSystem, scale, offset);	
	else
		soft = SoftFEMView::LoadFromTetFile(path, mSystem, scale, offset);

	if (soft == nullptr)
		return nullptr; // probably file not found

	//Vector3 offset(0, 60, -50);
	//float scale = 2.f;
	//auto soft = SoftFEMView::LoadFromTetFile("Models/dragon.tet", mSystem, scale, offset);

	//Vector3 offset(0, 85, -50);
	//float scale = 1.f;
	//auto soft = SoftFEMView::LoadFromVolFile("Models/bunny_low.vol", mSystem, scale, offset);

	//auto cloth = ConstructCloth(20, 6, Vector3(0, -10, 0), true, 2);

	float e = 35.f * 1000000.f / (Physics::Constants::unit * Physics::Constants::unit); // Young's modulus in GPa
	float nu = 0.4f;
	soft->DistributeMass(50.f, mSystem.GetBodies());
	//mSystem.GetBodyFromGroup(0, soft).invMass = 0;
	soft->Init(mSystem.GetBodies(), e, nu);
	//soft->SetVelocity(mSystem.GetBodies(), Vector3(0, 0, 30));

	// create a view object
	SoftFEMView* softView = new SoftFEMView(soft, &mSystem);
	softView->mCollMesh = soft->GetCollisonMesh();
	softView->mCollMesh->ComputeNormals();

	// add a collidable object for the collision mesh
	auto collMesh = std::make_shared<Physics::CollisionMesh>(softView->mCollMesh);
	collMesh->mGroup = soft;
	collMesh->mUserIndex = soft->GetStart();
	soft->mCollIdx = mSystem.mCollision.AddCollidable(collMesh);

	// load a render mesh and map it to the nodes
	bool ret = false;
	//ret = LoadCollisionMesh("cow.obj", softView->mMesh, offset, scale);
	//bool ret = LoadCollisionMesh("Models/dragon_new.obj", softView->mMesh, offset, scale);
	//bool ret = LoadCollisionMesh("Models/bunny.obj", softView->mMesh, offset, scale);
	if (ret)
	{
		softView->MapMesh();
		collMesh->mUserData = UD_STATIC;
	}
	mViews.push_back(softView);

	return softView;
}

void UnifiedView::InitFEM()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsPos(12);
	mSystem.GetSolver().SetNumIterationsVel(0);

	ConstructSoftFEM("../Models/torus2.vol", Vector3(14, 18, 0), 5.f);
	//ConstructSoftFEM("../Models/dino.vol", Vector3(17, 20, 0), 3.f); // TODO: find the file
	//ConstructSoftFEM("../Models/dragon.tet", Vector3(17, 24, 0), 2.f);
	//ConstructSoftFEM("../Models/cow.tet", Vector3(17, 24, 0), 4.f);
	//ConstructSoftFEM("../Models/tire1.vol", Vector3(17, 24, 0), 10.f);
	
	// set up rigid bodies
	Physics::Rigids* rigids = new Physics::Rigids();
	rigids->SetFriction(0.3f);
	int numRigids = 1;
	mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), numRigids);

	// create a ground
	{
		Physics::Body& body = mSystem.GetBodyFromGroup(0, rigids);
		body.invMass = 0;
		body.Icinv = Matrix3::Zero(); // TODO: default?
		body.Iinv = body.Icinv; // TODO: automate
		Physics::Box ground;
		ground.D.Set(400, 10, 400);
		ground.mUserData = 1;
		ground.mUserIndex = rigids->GetStart();
		body.pos.Set(0, -10, 0);
		ground.center = body.pos;
		//ground.mTolerance = 0.1f;
		ground.mGroup = rigids;
		body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Box>(ground));
	}

	// add coupling
	Physics::RigidSoftCoupler* couplerRigid = new Physics::RigidSoftCoupler();
	mSystem.mCollision.AddCoupler(std::shared_ptr<Physics::ICoupler>(couplerRigid));
}

void UnifiedView::InitStairs()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsPos(25);
	mSystem.GetSolver().SetNumIterationsVel(0);

	//float scale = 4.f;
	//Vector3 offset(0, 60, -30);
	//auto soft = SoftFEMView::LoadFromTetFile("cow.tet", mSystem, scale, offset);

	Vector3 offset(0, 60, -50);
	float scale = 2.f;
	auto soft = SoftFEMView::LoadFromTetFile("../Models/dragon.tet", mSystem, scale, offset);
	if (soft == nullptr)
		return;

	//Vector3 offset(0, 85, -50);
	//float scale = 1.f;
	//auto soft = SoftFEMView::LoadFromVolFile("Models/bunny_low.vol", mSystem, scale, offset);

	//auto cloth = ConstructCloth(20, 6, Vector3(0, -10, 0), true, 2);

	float e = 0.5f * 1000000.f / (Physics::Constants::unit * Physics::Constants::unit); // Young's modulus in GPa
	float nu = 0.1f;
	//float h = 0.016f;
	soft->DistributeMass(50.f, mSystem.GetBodies());
	soft->Init(mSystem.GetBodies(), e, nu);//, h * e, h * nu);
	soft->SetVelocity(mSystem.GetBodies(), Vector3(0, 0, 30));

	// create a view object
	SoftFEMView* softView = new SoftFEMView(soft, &mSystem);
	softView->mCollMesh = soft->GetCollisonMesh();
	softView->mCollMesh->ComputeNormals();

	// add a collidable object for the collision mesh
	auto collMesh = std::make_shared<Physics::CollisionMesh>(softView->mCollMesh);
	collMesh->mGroup = soft;
	collMesh->mUserIndex = soft->GetStart();
	collMesh->mUserData = 0;
	soft->mCollIdx = mSystem.mCollision.AddCollidable(collMesh);

	// load a render mesh and map it to the nodes
	//bool ret = LoadCollisionMesh("cow.obj", softView->mMesh, offset, scale);
	bool ret = LoadMesh("Models/dragon_new.obj", softView->mMesh, offset, scale);
	//bool ret = LoadCollisionMesh("Models/bunny.obj", softView->mMesh, offset, scale);
	if (ret)
		softView->MapMesh();
	mViews.push_back(softView);

	// set up collision with walls
	//float halfExtent = 63;
	//Aabb3 walls(Vector3(-halfExtent), Vector3(halfExtent));
	//mSystem.mCollision.AddCollidable(std::shared_ptr<Physics::Collidable>(new Physics::Walls(walls)));	

	// set up rigid bodies
	Physics::Rigids* rigids = new Physics::Rigids();
	rigids->SetFriction(0.3f);
	int numRigids = 8;
	mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), numRigids);

	// create a ground
	{
		Physics::Body& body = mSystem.GetBodyFromGroup(0, rigids);
		body.invMass = 0;
		body.Icinv = Matrix3::Zero(); // TODO: default?
		body.Iinv = body.Icinv; // TODO: automate
		Physics::Box ground;
		ground.D.Set(400, 10, 400);
		ground.mUserData = 1;
		ground.mUserIndex = rigids->GetStart();
		body.pos.Set(0, -10, 0);
		ground.center = body.pos;
		//ground.mTolerance = 0.1f;
		ground.mGroup = rigids;
		body.collIdx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Box>(ground));
	}

	Matrix3 Icinv;
	float invMass;
	Physics::Box box;

	// platforms
	invMass = 0;
	box = CreateBox(Vector3(0, 60, -50), Quaternion(), Vector3(40, 4, 15), invMass, 1, rigids, Icinv);
	AddRigid(rigids, 1, std::make_shared<Physics::Box>(box), invMass, Icinv);

	box = CreateBox(Vector3(0, 20, 0), Quaternion(), Vector3(40, 4, 15), invMass, 1, rigids, Icinv);
	AddRigid(rigids, 2, std::make_shared<Physics::Box>(box), invMass, Icinv);

	box = CreateBox(Vector3(0, 40, -30), Quaternion(), Vector3(40, 4, 15), invMass, 1, rigids, Icinv);
	AddRigid(rigids, 3, std::make_shared<Physics::Box>(box), invMass, Icinv);

	// add boxes
	invMass = 1.0f;
	box = CreateBox(Vector3(0, 10, 25), Quaternion(), Vector3(4, 10, 4), invMass, 1, rigids, Icinv);
	AddRigid(rigids, 4, std::make_shared<Physics::Box>(box), invMass, Icinv);

	box = CreateBox(Vector3(0, 10, 40), Quaternion(), Vector3(4, 10, 4), invMass, 1, rigids, Icinv);
	AddRigid(rigids, 5, std::make_shared<Physics::Box>(box), invMass, Icinv);

	box = CreateBox(Vector3(15, 10, 25), Quaternion(), Vector3(4, 10, 4), invMass, 1, rigids, Icinv);
	AddRigid(rigids, 6, std::make_shared<Physics::Box>(box), invMass, Icinv);

	box = CreateBox(Vector3(15, 10, 40), Quaternion(), Vector3(4, 10, 4), invMass, 1, rigids, Icinv);
	AddRigid(rigids, 7, std::make_shared<Physics::Box>(box), invMass, Icinv);

	// add coupling
	//Physics::ClothSoftCoupler* coupler = new Physics::ClothSoftCoupler();
	//mSystem.mCollision.AddCoupler(std::shared_ptr<Physics::ICoupler>(coupler));

	Physics::RigidSoftCoupler* coupler = new Physics::RigidSoftCoupler();
	mSystem.mCollision.AddCoupler(std::shared_ptr<Physics::ICoupler>(coupler));
}

void SoftFEMView::Draw(Graphics3D* graphics3D)
{
	mCounter = 0;

	if (mDebugDrawFlags & Physics::DDF_TETS)
	{
		// draw debug tets	
		graphics3D->SetFlags(8);
		graphics3D->SetColor(1, 1, 1);
		for (size_t i = 0; i < mSoft->GetTets().size(); i++)
		{
			const Physics::Tetrahedron& tet = mSoft->GetTets()[i];
			graphics3D->DrawTetrahedron(mSystem->GetBody(tet.i[0]).pos, mSystem->GetBody(tet.i[1]).pos, mSystem->GetBody(tet.i[2]).pos, mSystem->GetBody(tet.i[3]).pos);
		}
		graphics3D->SetFlags(0);
	}

	if (mDebugDrawFlags & Physics::DDF_CONTACTS)
	{
		//for (size_t i = 0; i < mCouplerRigid->mTriangleContacts.mTris.size(); i++)
		//{
		//	graphics3D->SetColor(1, 0, 0);
		//	graphics3D->DrawSphere(mSystem->GetBody(mCouplerRigid->mTriangleContacts.mTris[i].i1).pos, 0.1f);
		//	graphics3D->DrawSphere(mSystem->GetBody(mCouplerRigid->mTriangleContacts.mTris[i].i2).pos, 0.1f);
		//	graphics3D->DrawSphere(mSystem->GetBody(mCouplerRigid->mTriangleContacts.mTris[i].i3).pos, 0.1f);		
		//}

		// TODO: find another to draw this
		//for (size_t i = 0; i < mCouplerRigid->mRigidContacts.GetNumContacts(); i++)
		//{
		//	graphics3D->SetColor(0.82f, 0, 0);
		//	const Physics::RigidContact& contact = mCouplerRigid->mRigidContacts.GetContact(i);
		//	Physics::Body& body1 = mSystem->GetBody(contact.i);
		//	Vector3 pa = body1.R * contact.a1 + body1.pos;
		//	graphics3D->DrawLine(pa, pa + contact.n);
		//	graphics3D->DrawSphere(pa, 0.1f);
		//	Physics::Body& body2 = mSystem->GetBody(contact.j);
		//	Vector3 pb = body2.R * contact.a2 + body2.pos;
		//	graphics3D->SetColor(0, 0.82f, 0);
		//	graphics3D->DrawSphere(pb, 0.1f);
		//}
	}

	//// draw particles
	//graphics3D->SetColor(0.83f, 0.17f, 0.27f);
	//for (size_t i = 0; i < mSoft->GetCount(); i++)
	//{
	//	if (mSystem->GetBodyFromGroup(i, mSoft).surface)
	//	{
	//		Vector3 pos = mSystem->GetBodyFromGroup(i, mSoft).pos;
	//		graphics3D->DrawSphere(pos, 0.2f);
	//	}
	//}

	// draw deforrmed render mesh
	//graphics3D->SetColor(0.82f, 0.94f, 0.8f);
	//UpdateMesh();
	//graphics3D->DrawMesh(mMesh.vertices, mMesh.normals, mMesh.indices);
	
	//mSoft->UpdateCollisionMesh(mSystem->GetBodies());
	//graphics3D->DrawMesh(mCollMesh->vertices, mCollMesh->normals, mCollMesh->indices);

	//btVector3 boxMin, boxMax;
	//btVector3 pos = Physics::V2BV(mSystem->mCollision.GetCollidable(mSoft->mCollIdx)->center);
	//mSystem->mCollision.mBtShapes[mSoft->mCollIdx].mBtShape->getAabb(btTransform(btMatrix3x3::getIdentity(), pos), boxMin, boxMax);
	//Aabb3 box;
	//box.min = Physics::BV2V(boxMin);
	//box.max = Physics::BV2V(boxMax);
	//graphics3D->DrawWireCube(box.GetCenter(), box.GetExtent());
}


void SoftFEMView::MapMesh()
{
	uint size = mMesh.vertices.size();
	for (uint j = 0; j < size; j++)
	{
		float minDistance = FLT_MAX;
		int index = 0;
		const Vector3& vert = mMesh.vertices[j];
		Vector3 closestP;
		int id1, id2, id3;
		float u, v, w;
		for (uint i = 0; i < mSoft->GetTets().size(); i++)
		{
			const Physics::Tetrahedron& tet = mSoft->GetTets()[i];
			
			const Vector3& x0 = mSystem->GetBody(tet.i[0]).pos;
			const Vector3& x1 = mSystem->GetBody(tet.i[1]).pos;
			const Vector3& x2 = mSystem->GetBody(tet.i[2]).pos;
			const Vector3& x3 = mSystem->GetBody(tet.i[3]).pos;
			Vector3 closestP_temp;
			int id1_t, id2_t, id3_t;
			float d = ClosestPtPointTetrahedron(vert, x0, x1, x2, x3, id1_t, id2_t, id3_t, closestP_temp);
			if (abs(d) < minDistance)
			{
				minDistance = d;
				index = i;
				closestP = Vector3(closestP_temp.X(), closestP_temp.Y(), closestP_temp.Z());
				id1 = id1_t;
				id2 = id2_t;
				id3 = id3_t;
			}
		}
		MeshInterp m;
		m.distance = minDistance;
		m.id1 = id1;
		m.id2 = id2;
		m.id3 = id3;
		m.point = closestP;
		m.tet_index = index;
		BarycentricCoordinates(closestP,
			mSystem->GetBody(mSoft->GetTets().at(index).i[id1]).pos,
			mSystem->GetBody(mSoft->GetTets().at(index).i[id2]).pos,
			mSystem->GetBody(mSoft->GetTets().at(index).i[id3]).pos,
			u,
			v,
			w);
		m.u = u;
		m.v = v;
		m.w = w;
		m.vert_index = j;
		m.original = vert;
		map_triangles[index].push_back(m);
	}
}

void SoftFEMView::UpdateMesh()
{
	uint size = map_triangles.size();
	for (uint i = 0; i < size; i++)
	{
		uint size2 = map_triangles[i].size();
		for (uint j = 0; j < size2; j++)
		{
			const MeshInterp& m = map_triangles[i].at(j);
			const Vector3& p1 = mSystem->GetBody(mSoft->GetTets().at(m.tet_index).i[m.id1]).pos;
			const Vector3& p2 = mSystem->GetBody(mSoft->GetTets().at(m.tet_index).i[m.id2]).pos;
			const Vector3& p3 = mSystem->GetBody(mSoft->GetTets().at(m.tet_index).i[m.id3]).pos;

			Vector3 p = p1*m.w + p2*m.v + p3*m.u;//barycentric

			mMesh.vertices[m.vert_index] = p;
		}
	}
}

void UnifiedView::Draw(Graphics3D* graphics3D)
{
	// draw each group
	for (size_t i = 0; i < mViews.size(); i++)
	{
		mViews[i]->Draw(graphics3D);
	}

	// draw each coupler
	for (size_t i = 0; i < mCouplerViews.size(); i++)
	{
		mCouplerViews[i]->Draw(graphics3D);
	}

	// draw collidables
	for (size_t i = 0; i < mSystem.mCollision.GetNumCollidables(); i++)
	{
		auto shape = mSystem.mCollision.GetCollidable(i);
		if (shape->mType == Physics::CT_MESH && shape->mUserData == 0)
		{
			graphics3D->SetColor(0.82f, 0.94f, 0.8f);
			const Physics::CollisionMesh* collMesh = (const Physics::CollisionMesh*)shape;
			const Mesh* mesh = collMesh->mesh;

			SimpleMesh renderMesh;
			renderMesh.vertices = mesh->vertices; // TODO: do not copy data
			renderMesh.normals = mesh->normals;
			renderMesh.indices = mesh->indices;
			renderMesh.pos = collMesh->center;
			renderMesh.rot = qToMatrix(collMesh->rot);
			graphics3D->DrawMesh(renderMesh);
			//graphics3D->DrawMesh(mesh->vertices, mesh->normals, mesh->indices);
			//if (showDebug && (debugDrawFlags & DDF_TREE))
			//DrawTree(collMesh->tree);
		}
		if (shape->mType == Physics::CT_SPHERE)
		{
			const Physics::Sphere* sph = (const Physics::Sphere*)shape;
			if (sph->mUserData == UD_SPHERE)
			{
				graphics3D->SetColor(0.83f, 0.67f, 0.f);
				graphics3D->DrawSphere(sph->center, sph->radius);
			}
			else if (sph->mUserData != UD_INVISIBLE)
			{
				graphics3D->SetColor(0.82f, 0.94f, 0.8f);				
				graphics3D->DrawSphereTex(sph->center, sph->radius, qToMatrix(sph->rot), &mChecker);
			}
		}
		if (mSystem.mCollision.GetCollidable(i)->mType == Physics::CT_WALLS)
		{
			graphics3D->SetColor(0.82f, 0.94f, 0.8f);
			const Physics::Walls* walls = (const Physics::Walls*)mSystem.mCollision.GetCollidable(i);
			Vector3 c = walls->mBox.GetCenter();
			Vector3 e = walls->mBox.GetExtent();
			graphics3D->DrawWireCube(c, e);
		}
		if (mSystem.mCollision.GetCollidable(i)->mType == Physics::CT_BOX)
		{
			graphics3D->SetColor(0.f, 0.94f, 0.8f);
			Physics::Box* box = (Physics::Box*)mSystem.mCollision.GetCollidable(i);
			if (box->mUserData == UD_DEFAULT)
				graphics3D->SetColor(0.82f, 0.f, 0.94f);
			graphics3D->DrawCube(box->center, 2.f * box->D, qToMatrix(box->rot));
			//for (size_t j = 0; j < box->mesh.vertices.size(); j++)
			//{
			//	graphics3D->DrawSphere(box->mesh.vertices[j] + box->center, 0.1f);
			//}
		}
		if (shape->mType == Physics::CT_CAPSULE)
		{
			const Physics::Capsule* cap = (const Physics::Capsule*)shape;
			graphics3D->SetColor(0.83f, 0.67f, 0.f);
			//graphics3D->DrawSphere(cap->center - qRotate(cap->rot, Vector3(0, cap->hh, 0)), cap->r);
			//graphics3D->DrawSphere(cap->center + qRotate(cap->rot, Vector3(0, cap->hh, 0)), cap->r);
			graphics3D->DrawCapsule(cap->center, cap->r, cap->hh, qToMatrix(cap->rot));
		}
	}

	// temp
	//for (size_t i = 0; i < mCoupler->mTriangleContacts.mTris.size(); i++)
	//{
	//	graphics3D->SetColor(1, 1, 1);
	//	const Physics::TriangleRigidContact& tri = mCoupler->mTriangleContacts.mTris[i];
	//	//if (tri.i3 != 0)
	//		//continue;
	//	const Physics::Body& body = mSystem.GetBody(tri.i4);
	//	Vector3 p = qRotate(body.q, tri.a) + body.pos;
	//	graphics3D->DrawLine(p, p + tri.normal * 10);
	//	const Physics::Body& b1 = mSystem.GetBody(tri.i1);
	//	const Physics::Body& b2 = mSystem.GetBody(tri.i2);
	//	const Physics::Body& b3 = mSystem.GetBody(tri.i3);
	//	Vector3 q = tri.w1 * b1.pos + tri.w2 * b2.pos + tri.w3 * b3.pos;
	//	graphics3D->SetColor(0, 1, 0);
	//	graphics3D->DrawLine(p, q);
	//}
}

#ifdef USE_ALEMBIC
void SoftFEMView::ExportToAlembic(Alembic::Abc::OArchive* archive)
{
	if (mMesh.vertices.empty())
		return;

	if (mMeshyObj == nullptr)
	{
		Alembic::Abc::TimeSamplingPtr ts( new Alembic::Abc::TimeSampling( 1.0 / 60.0, 0.0 ) );
		std::string name("soft");
		name += std::to_string(mCounter++);
		Alembic::AbcGeom::OXform xfobj(archive->getTop(), name, ts);
		name += "_mesh";
		mMeshyObj.reset(new Alembic::AbcGeom::OPolyMesh(xfobj, name, ts));
	}

	OPolyMeshSchema &mesh = mMeshyObj->getSchema();

	const Mesh& myMesh = mMesh;
	size_t numFaces = myMesh.indices.size() / 3;
	std::vector<int> counts(numFaces);
	std::fill(counts.begin(), counts.end(), 3);
	OPolyMeshSchema::Sample mesh_samp(
		P3fArraySample( ( const V3f * )&myMesh.vertices[0], myMesh.vertices.size() ),
		Int32ArraySample( (const int*)&myMesh.indices[0], myMesh.indices.size() ),
		Int32ArraySample( &counts[0], numFaces ));
	mesh.set( mesh_samp );
}
#endif

void RigidsView::Draw(Graphics3D* graphics3D)
{
	if (mDebugDrawFlags & Physics::DDF_CONTACTS)
	{
		for (size_t i = 0; i < mRigids->GetNumContacts(); i++)
		{
			graphics3D->SetColor(0.82f, 0, 0);
			const Physics::RigidContact& contact = mRigids->GetContact(i);
			Physics::Body& body1 = mSystem->GetBody(contact.i);
			Vector3 pa = body1.R * contact.a1 + body1.pos;
			graphics3D->DrawLine(pa, pa - contact.n);
			graphics3D->DrawSphere(pa, 0.1f);
			Physics::Body& body2 = mSystem->GetBody(contact.j);
			Vector3 pb = body2.R * contact.a2 + body2.pos;
			graphics3D->SetColor(0, 0.82f, 0);
			graphics3D->DrawSphere(pb, 0.1f);
		}
	}
	if (mDebugDrawFlags & Physics::DDF_JOINTS)
	{
		for (size_t i = 0; i < mRigids->GetNumJoints(); i++)
		{
			graphics3D->SetColor(0.82f, 0, 0);
			const Physics::RigidJoint& joint = mRigids->GetJoint(i);
			auto& body1 = mSystem->GetBody(joint.i);
			//graphics3D->DrawSphere(body1.pos + body1.R * joint.a1, 1.f);
			//graphics3D->DrawLine(body1.pos, body1.pos + body1.R * joint.a1);
			auto& body2 = mSystem->GetBody(joint.j);
			//graphics3D->DrawSphere(body2.pos + body2.R * joint.a2, 1.f);
			//graphics3D->DrawLine(body2.pos, body2.pos + body2.R * joint.a2);

			graphics3D->DrawCapsule(body2.pos + 0.5f * (body2.R * joint.a2), 1.f, joint.a2.Length() * 0.5f, Matrix3());
		}
	}

	//{
	//	auto body = mSystem->GetBody(1);
	//	float x = body.pos.X();
	//	float y = body.pos.Y();
	//	float theta = atan2f(x, -y);
	//	float vx = body.vel.X();
	//	float vy = body.vel.Y();
	//	float r = sqrtf(x * x + y * y);
	//	float w;
	//	if (abs(theta) > 0.0001f)
	//		w = vx / (r * cosf(theta));
	//	else
	//		w = vy / (r * sinf(theta));
	//	Printf("%f %f\n", theta, w);
	//}
}

#ifdef USE_ALEMBIC
void RigidsView::ExportToAlembic(Alembic::Abc::OArchive* archive)
{
	if (mLinks.size() != mRigids->GetNumJoints())
	{
		mLinks.resize(mRigids->GetNumJoints());
		//std::fill(mLinks.begin(), mLinks.end(), nullptr);
		mMeshes.resize(mRigids->GetNumJoints());
		//std::fill(mMeshes.begin(), mMeshes.end(), nullptr);
	}
	if ((mDebugDrawFlags & Physics::DDF_JOINTS) == 0)
		return;
	for (size_t i = 0; i < mRigids->GetNumJoints(); i++)
	{
		const Physics::RigidJoint& joint = mRigids->GetJoint(i);			
		auto& body1 = mSystem->GetBody(joint.i);
		auto& body2 = mSystem->GetBody(joint.j);

		if (!mLinks[i])
		{
			Alembic::Abc::TimeSamplingPtr ts( new Alembic::Abc::TimeSampling( 1.0 / 60.0, 0.0 ) );
			std::string name("link");
			name += std::to_string(i);
			mLinks[i].reset(new OXform(archive->getTop(), name.c_str(), ts));
			name += "_mesh";
			mMeshes[i].reset(new OPolyMesh(*mLinks[i], name.c_str(), ts));

			std::vector<Vector3> vertices;
			std::vector<Vector3> normals;
			std::vector<uint32> indices;
			ConstructCapsule(1.f, joint.a2.Length() * 0.5f, vertices, normals, indices);

			OPolyMeshSchema &abcMesh = mMeshes[i]->getSchema();
			size_t numFaces = indices.size() / 3;
			std::vector<int> counts(numFaces);
			std::fill(counts.begin(), counts.end(), 3);
			OPolyMeshSchema::Sample mesh_samp(
				P3fArraySample( ( const V3f * )&vertices[0], vertices.size() ),
				Int32ArraySample( (const int*)&indices[0], indices.size() ),
				Int32ArraySample( &counts[0], numFaces ));
			abcMesh.set(mesh_samp);
		}

		OXformSchema& schema = mLinks[i]->getSchema();
		XformSample xs;
		//float angle;
		//Vector3 axis;
		//cap->rot.GetAxisAngle(angle, axis);
		Vector3 center = body2.pos + 0.5f * (body2.R * joint.a2);
		xs.setTranslation(V3f(center.X(), center.Y(), center.Z()));
		//xs.setRotation(V3f(axis.X(), axis.Y(), axis.Z()), angle * 180.f / PI);			
		schema.set(xs);
	}
}
#endif

void UnifiedView::Step()
{
	PROFILE_SCOPE("Unified step");
	if (mAbcExport)
		ExportToAlembic();
	//ExportToText();
    //UpdateKinematic(); // FIXME
    mSystem.Step();
	// update each group
	for (size_t i = 0; i < mViews.size(); i++)
	{
		mViews[i]->Update();
	}
    mNumFrames++;
	if (mMaxFrames > 0 && mNumFrames >= mMaxFrames)
		Engine::getInstance()->Quit();
}

void UnifiedView::InitCloth(int divisions, float inc, Vector3 offset, bool horizontal, bool attached)
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsVel(0);
	mSystem.GetSolver().SetNumIterationsPos(20);
	mSystem.GetSolver().SetNumIterationsPost(10); // this does damping!

	float halfExtent = 63; //half + spacing;		

	//ConstructGranular(1.5f, 25, Vector3(0, 20, 0), Vector3(0, 0, 80), halfExtent);

	auto cloth = ConstructCloth(divisions, inc, offset, horizontal, attached, 15, 1e4f);
	//auto cloth2 = ConstructCloth(divisions, inc, offset + Vector3(120, 0, 0), horizontal, attached, 15, 1e4f);
	//auto cloth = ConstructCloth(divisions, inc, Vector3(), true, 2);

	// set up collision with walls
	//Aabb3 walls(Vector3(-halfExtent), Vector3(halfExtent));
	//uint32 meshId = mSystem.mCollision.AddCollidable(std::shared_ptr<Physics::Collidable>(new Physics::Walls(walls)));

	// add coupling
	//Physics::ClothGranularCoupler* coupler = new Physics::ClothGranularCoupler();
	//mSystem.mCollision.AddCoupler(std::shared_ptr<Physics::ICoupler>(coupler));
}

void UnifiedView::InitClothSphere()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsVel(0);
	mSystem.GetSolver().SetNumIterationsPos(10);
	mSystem.GetSolver().SetNumIterationsPost(0);

	auto cloth = ConstructCloth(30, 4, Vector3(), true, false);
	cloth->SetFriction(0.4f);

	Physics::Sphere sphere;
	sphere.mUserData = UD_SPHERE;
	sphere.radius = 25;
	sphere.center.Set(0, -50, 0);
	sphere.mTolerance = 0.1f;
	uint32 idx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Sphere>(sphere));	
}

void UnifiedView::InitGranular(float radius, int numParticles)
{
	mViews.clear();
	mSystem.Clear();

	float halfExtent = 0;
	auto granular = ConstructGranular(radius, numParticles, Vector3(), Vector3(0, 5, 0), halfExtent);
	//halfExtent = 63;

	// set up collision with walls
	//Aabb3 walls(Vector3(-halfExtent), Vector3(halfExtent));		
	//mSystem.mCollision.AddCollidable(std::shared_ptr<Physics::Collidable>(new Physics::Walls(walls)));

	// add a ground box
	Physics::Box ground;
	ground.mUserData = 1;
	ground.D.Set(250, 5, 250);	
	ground.center.Set(0, -50, 0);
	uint32 idx = mSystem.mCollision.AddCollidable(std::make_shared<Physics::Box>(ground));
	//mSystem.mCollision.UpdateShape(idx, Vector3(0, -30, 0), Quaternion());
}

void UnifiedView::InitCable()
{
	mViews.clear();
	mSystem.Clear();

	mSystem.GetSolver().SetNumIterationsVel(10);
	mSystem.GetSolver().SetNumIterationsPos(0);

    const bool horizontal = true;
    const bool addBall = false;
    
    int numLinks = 6;
	int numRigids = numLinks + 1; // static body included
    if (addBall)
        numRigids++;
    Physics::Rigids* rigids = new Physics::Rigids();
    mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), numRigids);

	const float hh = 14;
	const float rad = 2;

    Quaternion q1;
    if (horizontal)
        q1.SetAxisAngle(PI / 2, Vector3(0, 0, 1));

    // add dummy object
    AddRigid(rigids, 0, nullptr, 0, Matrix3(0));

    // add capsules
    {
		//Physics::Capsule capsules[numRigids];
		float invMass = 1;
		Matrix3 Icinv;
		for (int i = 0; i < numLinks; i++)
		{
			float coord = -i * (2 * (hh + rad));
			Vector3 pos = horizontal ? Vector3(coord, 0, 0) : Vector3(0, coord, 0);
            Physics::Box box = CreateBox(pos, q1, Vector3(2, hh, 2), invMass, i + 1, rigids, Icinv);
            AddRigid(rigids, i + 1, std::make_shared<Physics::Box>(box), invMass, Icinv);
			if (!horizontal)
				pos.Y() += hh + rad;
			else
				pos.X() += hh + rad;
            //rigids->AddHingeJoint(mSystem.GetBodies(), i, i + 1, pos, Vector3(1, 0, 0));
            rigids->AddLockedJoint(mSystem.GetBodies(), i, i + 1, pos);
        }
	}    

    // give initial velocity to one body
    mSystem.GetBodyFromGroup(numLinks, rigids).vel.Set(100, 100, 100);
    mSystem.GetBodyFromGroup(numLinks, rigids).w.Set(10, 10, 10);

	// add hanging ball	
	if (addBall)
	{
		float invMass = 0.1f;
		float coord = -numLinks * 2 * (hh + rad);
		Vector3 ballPos = horizontal ? Vector3(coord, 0, 0) : Vector3(0, coord, 0);
		Quaternion q;
		Matrix3 Icinv;
		Physics::Sphere ball = CreateSphere(ballPos, q, hh + rad, invMass, Icinv);
		AddRigid(rigids, numRigids - 1, std::make_shared<Physics::Sphere>(ball), invMass, Icinv);
		if (!horizontal)
			ballPos.Y() += hh + rad;
		else
			ballPos.X() += hh + rad;
		rigids->AddJoint(mSystem.GetBodies(), numLinks, numRigids - 1, ballPos);
	}

	// create a view object
	RigidsView* rigidsView = new RigidsView(rigids, &mSystem);
	mViews.push_back(rigidsView);
}

const float BASE_CAPSULE_HALF_LENGTH = 0.3f;
const float BASE_CAPSULE_RADIUS = 1.f;
const float BASE_CAPSULE_HALF_HEIGHT = BASE_CAPSULE_HALF_LENGTH + BASE_CAPSULE_RADIUS;

const float TOP_CAPSULE_HALF_LENGTH = 1.2f;
const float TOP_CAPSULE_RADIUS = 0.5f;
const float TOP_CAPSULE_HALF_HEIGHT = TOP_CAPSULE_HALF_LENGTH + TOP_CAPSULE_RADIUS;

const float ROTATION_RADIUS = 10.f;
const float ROTATION_CENTER_HEIGHT = 12.f;

const float BASE_CAPSULE_INITIAL_POSY = ROTATION_CENTER_HEIGHT - ROTATION_RADIUS;
const float TOP_CAPSULE_INITIAL_POSY = BASE_CAPSULE_INITIAL_POSY + BASE_CAPSULE_HALF_HEIGHT + TOP_CAPSULE_HALF_HEIGHT;

void UnifiedView::InitKinematic()
{
    Clear();

    // Create a rigid group
    Physics::Rigids* rigids = new Physics::Rigids();
    mSystem.AddGroup(std::shared_ptr<Physics::Group>(rigids), 2);

    // Create the kinematic body
    Quaternion rot;
    auto capsule = std::make_shared<Physics::Capsule>(CreateCapsule(Vector3(0, BASE_CAPSULE_INITIAL_POSY, 0), rot, BASE_CAPSULE_HALF_LENGTH, BASE_CAPSULE_RADIUS, 0));
    mBase = &AddRigid(rigids, 0, capsule, 0, Matrix3(0));

    // Create the top
    Matrix3 Icinv;
    auto capsule2 = std::make_shared<Physics::Capsule>(CreateCapsule(Vector3(0, TOP_CAPSULE_INITIAL_POSY, 0), rot, TOP_CAPSULE_HALF_LENGTH, TOP_CAPSULE_RADIUS, 1, &Icinv));
    AddRigid(rigids, 1, capsule2, 1, Icinv);

    // Create the joint between them
    rigids->AddLockedJoint(mSystem.GetBodies(), 0, 1, Vector3(0, BASE_CAPSULE_INITIAL_POSY + BASE_CAPSULE_HALF_HEIGHT + 1, 0));

    // Reset the rotation animation
    mCounter = -180;
}

void UnifiedView::UpdateKinematic()
{
    // This will animate the shapes in order to rotate around a ring with the UP pointing to the center. We animate only the
    // base shape, top should follow with the constraint
    const float ANGLE_RAD = mCounter * PI / 360.f;    // 1/2 degree per count
    const float CAPSULE_ORIENT = PI * 0.5f;

    Quaternion deltaRot;
    deltaRot.SetAxisAngle(ANGLE_RAD + CAPSULE_ORIENT, Vector3(0, 0, 1)); // Around z
    Vector3 deltaPos(ROTATION_RADIUS * cos(ANGLE_RAD), ROTATION_RADIUS * sin(ANGLE_RAD) + ROTATION_CENTER_HEIGHT, 0.0f);

    // Apply the new transform to the base body:
    mBase->SetRotation(deltaRot);
    mBase->pos = deltaPos;

    // Increase the rotation step:
    mCounter++;
}

void UnifiedView::LoadFromConfig(XMLElement* root)
{
}

void UnifiedView::ExportToText()
{
    FILE* txt;
    char fileName[32];
    sprintf_s(fileName, "SimData\\data%d.txt", mNumFrames);
    //sprintf_s(fileName, "PovRay\\frame%d.pov", mNumFrames);
    if (fopen_s(&txt, fileName, "wt") != 0)
    {
        return;
    }

    ExportToText(txt);

    // export each group
    for (size_t i = 0; i < mViews.size(); i++)
    {
        mViews[i]->ExportToText(txt);
    }

    fclose(txt);
}
