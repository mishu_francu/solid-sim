#include "ClothDemo.h"
#include "Physics/ClothModel.h"
#include "Geometry/Collision3D.h"
#include "Geometry/Assets.h"
#include "Geometry/AabbTree.h"
#include "Graphics3D/Graphics3D.h"
#include "Demo/Particles/3D/Demo3DCommon.h"
#include "Math/Utils.h"
#include "Engine/xml.h"

using namespace Physics;
using namespace Geometry;

ClothDemo::ClothDemo()
	: mClothAsset(CLOTH_ASSET_DEFAULT)
	, mHorizontal(false)
	, mAttached(false)
	, mSelected(-1)
	, mCamera(NULL)
{
}

void ClothDemo::Create(int type)
{
	mClothDemoType = type;
	Aabb3 walls(Vector3(-60), Vector3(60));
	mCloth.GetModel().ClearCollidables();
	mCloth.GetModel().AddCollidable(std::shared_ptr<Collidable>(new Walls(walls)));
	mHorizontal = true;
	mAttached = false;
	mCloth.GetModel().SetCollisionFlags(CF_WALLS);
	if (type == CLOTH_DEMO_SPHERE)
	{
		mCloth.GetModel().AddCollidable(std::shared_ptr<Collidable>(new Sphere(Vector3(0, -10, 0), 25)));
	}
	else if (type == CLOTH_DEMO_MESH)
	{
		bool ret = false;
		if (mClothAsset == CLOTH_ASSET_KATJA)
		{
			ret = LoadMesh("../Models/katja.obj", mMesh, Vector3(), 0.8f);
		}
		else if (mClothAsset == CLOTH_ASSET_MD)
		{
			ret = LoadMesh("../Models/avatar2.ply", mMesh, Vector3(0, 0, 0), 0.1f, false);
		}
		else if (mClothAsset == CLOTH_ASSET_BUDDHA)
		{
			ret = LoadMesh("../Models/buddha.obj", mMesh, Vector3(0, -5, 0), 2.f);
		}
		else if (mClothAsset == CLOTH_ASSET_DRAGON)
		{
			ret = LoadMesh("../Models/dragon.obj", mMesh, Vector3(0, -80, 0), 5.f);
		}
		else // default
		{
			ret = LoadMesh("../Models/bunny.obj", mMesh, Vector3(0, -25, 0), 2.f);
		}
		if (ret)
		{
			mMeshCopy = mMesh; // make a copy
			mCollisionMesh.reset(new Physics::CollisionMesh(&mMeshCopy));
			mCloth.GetModel().AddCollidable(std::shared_ptr<Collidable>(mCollisionMesh));
		}
		else
			Printf("Could not load mesh\n");
		mCloth.GetModel().SetCollisionFlags(Physics::CF_WALLS | Physics::CF_VERTICES);
	}
	else if (type == CLOTH_DEMO_SELF)
	{
		mHorizontal = false;
		mCloth.GetModel().SetCollisionFlags(Physics::CF_SELF | Physics::CF_WALLS);
	}
	else
	{
		mHorizontal = false;
		mAttached = true;
		mCloth.GetModel().SetCollisionFlags(0);
	}
}

bool ClothDemo::LoadFromXml(XMLElement* xCloth)
{
	assert(xCloth != nullptr);
	XMLVariable* xNum = xCloth->FindVariableZ("divisions");
	if (xNum)
	{
		mDivisions = xNum->GetValueInt();
	}
	char str[32];
	XMLVariable* xRadius = xCloth->FindVariableZ("radius");
	if (xRadius)
	{
		xRadius->GetValue(str);
		float radius = (float)atof(str);
		mCloth.GetModel().SetThickness(radius);
	}

	XMLVariable* xMethod = xCloth->FindVariableZ("method");
	if (xMethod)
	{
		xMethod->GetValue(str);
		if (strcmp(str, "pbd") == 0)
		{
			mCloth.SetMethod(Physics::METHOD_PBD);
		}
		else if (strcmp(str, "spring") == 0)
		{
			mCloth.SetMethod(Physics::METHOD_SPRING);
		}
		else if (strcmp(str, "soft") == 0)
		{
			mCloth.SetMethod(Physics::METHOD_SOFT);
		}
		else if (strcmp(str, "ivp") == 0)
		{
			mCloth.SetMethod(Physics::METHOD_IVP);
		}
		else
		{
			return false;
		}
	}

	XMLVariable* xSolver = xCloth->FindVariableZ("solver");
	if (xSolver)
	{
		xSolver->GetValue(str);
		if (strcmp(str, "gs") == 0)
		{
			mCloth.GetModel().SetSolver(SOLVER_GAUSS_SEIDEL);
		}
		if (strcmp(str, "jacobi") == 0)
		{
			mCloth.GetModel().SetSolver(SOLVER_JACOBI);
		}
		else if (strcmp(str, "jcr") == 0)
		{
			mCloth.GetModel().SetSolver(SOLVER_JACOBI_CR);
		}
	}

	xNum = xCloth->FindVariableZ("iterations");
	if (xNum)
	{
		mCloth.GetModel().SetNumIterations(xNum->GetValueInt());
	}

	XMLVariable* xSteps = xCloth->FindVariableZ("steps");
	if (xSteps)
	{
		int steps = xSteps->GetValueInt();
		mCloth.SetNumSteps(steps);
	}

	XMLVariable* xTol = xCloth->FindVariableZ("tolerance");
	if (xTol)
	{
		xTol->GetValue(str);
		float tol = (float)atof(str);
		mCloth.GetModel().SetTolerance(tol);
	}

	XMLVariable* xFriction = xCloth->FindVariableZ("mu");
	if (xFriction)
	{
		xFriction->GetValue(str);
		float mu = (float)atof(str);
		mCloth.GetModel().SetFriction(mu);
	}

	XMLVariable* xSelf = xCloth->FindVariableZ("self");
	if (xSelf)
	{
		int self = xSelf->GetValueInt();
		if (self != 0)
			mCloth.GetModel().SetCollisionFlags(mCloth.GetModel().GetCollisionFlags() | Physics::CF_SELF);
		else
			mCloth.GetModel().SetCollisionFlags(mCloth.GetModel().GetCollisionFlags() & ~Physics::CF_SELF);
	}

	XMLVariable* xDihedral = xCloth->FindVariableZ("dihedral");
	if (xDihedral)
	{
		int dihedral = xDihedral->GetValueInt();
		mCloth.GetModel().SetDihedral(dihedral != 0);
	}

	return true;
}

void ClothDemo::Init()
{
	if (mClothDemoType == CLOTH_DEMO_SELF)
	{
		Vector3 offset(0, 0, 0);
		mCloth.Init(4, mDivisions, 120.f / mDivisions, offset, mHorizontal, mAttached);
	}
	else if (mClothAsset == CLOTH_ASSET_DEFAULT || mClothAsset == CLOTH_ASSET_BUDDHA || mClothAsset == CLOTH_ASSET_BUNNY || mClothAsset == CLOTH_ASSET_DRAGON)
	{
		Vector3 offset(0, 30, 10);
		if (mClothAsset == CLOTH_ASSET_BUDDHA)
			offset.Y() += 35;
		mCloth.Init(mDivisions, mDivisions, 80.f / mDivisions, offset, mHorizontal, mAttached);
	}
	else
	{
		Mesh mesh;
		bool ret = false;
		float dy = 0;
		if (mClothAsset == CLOTH_ASSET_KATJA)
			ret = LoadMesh("../Models/dress.obj", mesh, Vector3(), 0.8f);
		else if (mClothAsset == CLOTH_ASSET_MD)
		{
			ret = LoadMesh("../Models/md1.dae", mesh, Vector3(0, 0.3f, -0.4f), 0.1f);
		}
		else if (mClothAsset == CLOTH_ASSET_PATCH)
			ret = LoadMesh("../Models/cloth1.dae", mesh, Vector3(0, 35, 0), 1.f);
		if (ret)
		{
			mCloth.Init(mesh, Vector3(0, dy, 0), mAttached);
		}
	}
}

void ComputeStrainMap(const Physics::ClothPatch& cloth, std::vector<Vector3>& colors)
{
	for (size_t i = 0; i < cloth.GetModel().GetNumParticles(); i++)
		colors[i].SetZero();
	float maxStrain = 0;
	float minStrain = 20;
	for (size_t i = 0; i < cloth.GetModel().GetNumTris(); i++)
	{
		Physics::Triangle tri = cloth.GetModel().GetTriangle(i);
		const Physics::Particle& p1 = cloth.GetModel().GetParticle(tri.i1);
		const Physics::Particle& p2 = cloth.GetModel().GetParticle(tri.i2);
		const Physics::Particle& p3 = cloth.GetModel().GetParticle(tri.i3);
		Vector3 dx1 = (p2.pos - p1.pos);
		Vector3 dx2 = (p3.pos - p1.pos);

		Vector3 wu = tri.invDet * (tri.dv2 * dx1 - tri.dv1 * dx2);
		Vector3 wv = tri.invDet * (-tri.du2 * dx1 + tri.du1 * dx2);

		tri.euu = 0.5f * ((wu * wu) - 1);
		tri.evv = 0.5f * ((wv * wv) - 1);
		tri.euv = wu * wv;

		float strain = sqrtf(tri.euu * tri.euu + tri.evv * tri.evv + tri.euv * tri.euv);
		maxStrain = max(strain, maxStrain);
		minStrain = min(strain, minStrain);

		float s = strain;

		Vector3 e1 = dx1;
		Vector3 e2 = p3.pos - p2.pos;
		Vector3 e3 = -dx2;
		e1.Normalize();
		e2.Normalize();
		e3.Normalize();
		float t1 = acos(-(e1 * e3));
		float t2 = acos(-(e1 * e2));
		float t3 = acos(-(e2 * e3));

		Vector3 col(s, 0, s);
		colors[tri.i1] += (t1 / PI) * col;
		colors[tri.i2] += (t2 / PI) * col;
		colors[tri.i3] += (t3 / PI) * col;
	}
}

void DrawTree(Graphics3D* graphics3D, AabbTree* tree)
{
	if (!tree)
		return;
	if (!tree->triangles.empty() || !tree->vertices.empty())
		graphics3D->DrawWireCube(tree->box.GetCenter(), tree->box.GetExtent());
	if (tree->left)
		DrawTree(graphics3D, tree->left);
	if (tree->right)
		DrawTree(graphics3D, tree->right);
}

void ClothDemo::Draw(Graphics3D* graphics3D, bool showDebug, int debugDrawFlags)
{
	// store viewport properties for mouse picking
	mViewWidth = graphics3D->w;
	mViewHeight = graphics3D->h;
	mViewFOV = graphics3D->fov;
	mCamera = &graphics3D->camera;

	std::vector<Vector3> colors(mCloth.GetModel().GetNumParticles());
	int flags = 0;
	mCloth.UpdateMesh();
	if (showDebug)
	{
		if (debugDrawFlags & DDF_PARTICLES)
		{
			for (size_t i = 0; i < mCloth.GetModel().GetNumParticles(); i++)
			{
				Vector3 pos = mCloth.GetModel().GetParticle(i).pos;
				float m = 1.f;
				graphics3D->SetColor(m * 0.83f, m * 0.67f, 0.f);
				graphics3D->DrawSphere(pos, mCloth.GetModel().GetThickness());
			}
		}
		if (debugDrawFlags & DDF_LINKS)
		{
			graphics3D->SetColor(0.8f, 0.3f, 0.2f);
			for (size_t i = 0; i < mCloth.GetModel().GetNumLinks(); i++)
			{
				const Physics::Link& link = mCloth.GetModel().GetLink(i);
				graphics3D->DrawLine(mCloth.GetModel().GetParticle(link.i1).pos, mCloth.GetModel().GetParticle(link.i2).pos);
			}
		}
		if (debugDrawFlags & DDF_CONTACTS)
		{
			for (size_t i = 0; i < mCloth.GetModel().GetNumContacts(); i++)
			{
				const Physics::Contact contact = mCloth.GetModel().GetContact(i);
				graphics3D->SetColor(0, 0, 1);
				Vector3 p = contact.point;
				graphics3D->DrawLine(p, p + contact.normal * 3.f);
			}
		}
		if (debugDrawFlags & DDF_TRI_CONTACTS)
		{
			for (size_t i = 0; i < mCloth.GetModel().GetNumTriContacts(); i++)
			{
				const Physics::TriContact contact = mCloth.GetModel().GetTriContact(i);
				graphics3D->SetColor(0, 1, 0);
				Vector3 p = contact.point;
				graphics3D->DrawLine(p, p + contact.normal * 3.f);
			}
		}
		if (debugDrawFlags & DDF_SELF_TRIS)
		{
			for (size_t i = 0; i < mCloth.GetModel().GetNumSelfTris(); i++)
			{
				const Physics::SelfContact& link = mCloth.GetModel().GetSelfTriangle(i);
				const Vector3& v1 = mCloth.GetModel().GetParticle(link.i1).pos;
				const Vector3& v2 = mCloth.GetModel().GetParticle(link.i2).pos;
				const Vector3& v3 = mCloth.GetModel().GetParticle(link.i3).pos;
				const Vector3& v4 = mCloth.GetModel().GetParticle(link.i4).pos;
				Vector3 n = link.normal;
				Vector3 p = link.w1 * v1 + link.w2 * v2 + link.w3 * v3;
				graphics3D->DrawLine(v4, v4 + n * 10.f);
				graphics3D->DrawLine(p, p - n * 10.f);
			}
		}
		if (debugDrawFlags & DDF_SELF_EDGES)
		{
			for (size_t i = 0; i < mCloth.GetModel().GetNumSelfEdges(); i++)
			{
				const Physics::SelfContact& link = mCloth.GetModel().GetSelfEdge(i);
				const Vector3& v1 = mCloth.GetModel().GetParticle(link.i1).pos;
				const Vector3& v2 = mCloth.GetModel().GetParticle(link.i2).pos;
				const Vector3& v3 = mCloth.GetModel().GetParticle(link.i3).pos;
				const Vector3& v4 = mCloth.GetModel().GetParticle(link.i4).pos;
				Vector3 p = v1 + link.w1 * (v2 - v1);
				Vector3 q = v3 + link.w2 * (v4 - v3);
				Vector3 n = link.normal;
				graphics3D->DrawLine(p, p - n * 10.f);
				graphics3D->DrawLine(q, q + n * 10.f);
			}
		}
		if (debugDrawFlags & DDF_WARP_WEFT)
		{
			for (size_t i = 0; i < mCloth.GetModel().GetNumTris(); i++)
			{
				Physics::Triangle tri = mCloth.GetModel().GetTriangle(i);
				Physics::Particle& p1 = mCloth.GetModel().GetParticle(tri.i1);
				Physics::Particle& p2 = mCloth.GetModel().GetParticle(tri.i2);
				Physics::Particle& p3 = mCloth.GetModel().GetParticle(tri.i3);
				Vector3 dx1 = (p2.pos - p1.pos);
				Vector3 dx2 = (p3.pos - p1.pos);
				Vector3 wu = tri.invDet * (tri.dv2 * dx1 - tri.dv1 * dx2);
				Vector3 wv = tri.invDet * (-tri.du2 * dx1 + tri.du1 * dx2);
				Vector3 c = (1.f / 3.f) * (p1.pos + p2.pos + p3.pos);
				graphics3D->SetColor(1, 0, 0);
				graphics3D->DrawLine(c, c + wu * tri.su);
				graphics3D->SetColor(0, 1, 0);
				graphics3D->DrawLine(c, c + wv * tri.sv);
			}
		}
		if (debugDrawFlags & DDF_STRAIN)
		{
			flags |= ShaderFlags::VERTEX_COLORS;
			ComputeStrainMap(mCloth, colors);
		}
		if (debugDrawFlags & DDF_TREE_SELF)
			DrawTree(graphics3D, mCloth.GetModel().GetTree());
	}

	// draw cloth mesh
	{
		graphics3D->SetFlags(flags);
		graphics3D->SetColor(0, 1, 1);
		graphics3D->DrawMesh(mCloth.GetMesh().vertices, mCloth.GetMesh().normals, colors, mCloth.GetMesh().indices);
		graphics3D->SetCulling(Graphics3D::CULL_FRONT);
		graphics3D->SetColor(0, 0, 1);
		graphics3D->SetFlipNormals(true);
		graphics3D->DrawMesh(mCloth.GetMesh().vertices, mCloth.GetMesh().normals, colors, mCloth.GetMesh().indices);
		graphics3D->SetFlipNormals(false);
		graphics3D->SetCulling(Graphics3D::CULL_BACK);
		graphics3D->SetFlags(0);
	}

	// draw collidables
	for (size_t i = 0; i < mCloth.GetModel().GetNumCollidables(); i++)
	{
		if (mCloth.GetModel().GetCollidable(i)->mType == CT_MESH)
		{
			graphics3D->SetColor(0.82f, 0.94f, 0.8f);
			const CollisionMesh* collMesh = (const CollisionMesh*)mCloth.GetModel().GetCollidable(i);
			const Mesh* mesh = collMesh->mesh;
			graphics3D->DrawMesh(mesh->vertices, mesh->normals, mesh->indices);
			if (showDebug && (debugDrawFlags & DDF_TREE))
				DrawTree(graphics3D, collMesh->tree);
		}
		else if (mCloth.GetModel().GetCollidable(i)->mType == CT_SPHERE)
		{
			graphics3D->SetColor(0.82f, 0.94f, 0.8f);
			const ClothModel& model = mCloth.GetModel();
			const Sphere* sph = (const Sphere*)model.GetCollidable(i);
			graphics3D->DrawSphere(sph->center, sph->radius);
		}
		else if (mCloth.GetModel().GetCollidable(i)->mType == CT_WALLS)
		{
			graphics3D->SetColor(0.82f, 0.94f, 0.8f);
			const Walls* walls = (const Walls*)mCloth.GetModel().GetCollidable(i);
			Vector3 c = walls->mBox.GetCenter();
			Vector3 e = walls->mBox.GetExtent();
			graphics3D->DrawWireCube(c, e);
		}
	}

	// draw mouse spring
	if (mSelected >= 0)
	{
		graphics3D->SetColor(0, 0, 1);
		graphics3D->DrawSphere(mPick, 0.7f);
		graphics3D->SetColor(1, 0, 0);
		graphics3D->DrawSphere(mCloth.GetModel().GetParticle(mSelected).pos, 0.7f);
		graphics3D->DrawLine(mPick, mCloth.GetModel().GetParticle(mSelected).pos);
	}
}

void ClothDemo::Update(float dt)
{
	mCloth.Step(dt);
}

void ClothDemo::ComputeMouseRay(int x, int y)
{
	assert(mCamera != NULL);
	mMouse.Set(-1.f + 2.f * (float)x / mViewWidth, 1.f - 2.f * (float)y / mViewHeight, -1.f);
	float d = tanf(RADIAN(mViewFOV) * 0.5f);
	float a = (float)mViewWidth / (float)mViewHeight;
	mMouse.X() = mMouse.X() * d * a;
	mMouse.Y() = mMouse.Y() * d;
	Matrix4 mat = Matrix4::Euler(mCamera->GetYaw(), mCamera->GetPitch(), mCamera->GetRoll());
	mat = Matrix4::Transposed(mat);
	mMouse = mat.TransformRay(mMouse);
	mMouse.Normalize();
}

void ClothDemo::OnMouseMove(int x, int y)
{
	if (mCamera == NULL || mSelected < 0)
		return;
	Vector3 mouseOld = mMouse;
	ComputeMouseRay(x, y);
	Vector3 eye = mCamera->GetPosition();
	Vector3 n = mCamera->GetViewDir();
	n.Normalize();
	float t;
	IntersectRayPlane(eye, mMouse, n, n.Dot(mCloth.GetModel().GetParticle(mSelected).pos), t, mPick);

	mCloth.GetModel().UpdateMouseSpring(mPick);
}

void ClothDemo::OnMouseDown(int x, int y)
{
	if (mCamera == NULL)
		return;
	// mouse picking
	Vector3 eye = mCamera->GetPosition();
	ComputeMouseRay(x, y);
	float t;
	mSelected = -1;
	float tMin = 1e7f;
	for (size_t i = 0; i < mCloth.GetModel().GetNumParticles(); i++)
	{
		float radius = mCloth.GetModel().GetThickness();
		if (IntersectRaySphere(eye, mMouse, mCloth.GetModel().GetParticle(i).pos, radius * 2, t, mPick) && t < tMin)
		{
			mSelected = (int)i;
			mCloth.GetModel().AddMouseSpring((int)i, mPick);
			tMin = t;
		}
	}
}

void ClothDemo::OnMouseUp(int x, int y)
{
	if (mSelected > 0)
	{
		mCloth.GetModel().RemoveMouseSpring();
		mSelected = -1;
	}
}
