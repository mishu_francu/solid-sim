#pragma once

#include <vector>
#include <Math/Vector3.h>

class FemUtility
{
public:
	struct VolumeMesh
	{
		struct Tetrahedron
		{
			int mNodeIndices[4];
		};
		std::vector<Math::Vector3> mPoints;
		std::vector<Tetrahedron> mTetrahedra;
	};

public:
	static bool LoadFromTetFile(const char* path, VolumeMesh& output);
	static bool LoadFromVolFile(const char* path, VolumeMesh& output);
};
