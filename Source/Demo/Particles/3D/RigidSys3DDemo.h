#ifndef RIGID_SYS_3D_DEMO_H
#define RIGID_SYS_3D_DEMO_H

#include <memory>
#include <Math/Vector3.h>

namespace Physics
{
	class RigidSystem;
}

class Graphics3D;
class Texture;

class RigidSys3DDemo
{
public:
	void Create();
	void Init();
	void Draw(Graphics3D* graphics3D, bool showDebug, int debugDrawFlags);
	void Update(float dt);

private:
	void InitBox();
	void InitCloth();
	void InitClothFromRigids(int divisions, float radius, float inc, const Math::Vector3& offset, bool horizontal, bool attached, int size);

private:
	std::unique_ptr<Physics::RigidSystem> mRigidSys3D;
	std::unique_ptr<Texture> mCheckerTexture;
};

#endif //RIGID_SYS_3D_DEMO_H
