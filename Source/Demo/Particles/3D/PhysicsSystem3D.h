#ifndef PHYSICS_SYSTEM_3D_H
#define PHYSICS_SYSTEM_3D_H

#include <Math/Vector3.h>
#include <Demo/Particles/Particle.h>
#include <Demo/Particles/Metrics.h>
#include <Demo/Particles/Projector.h>
#include <Physics/GridBroadphase3D.h>
#include <Demo/Particles/PhysicsSystem.h>
#include <Geometry/Mesh.h>
#include <memory>

#ifndef ANDROID_NDK
//#	include <Demo/Particles/ProjectorCL.h>
#endif
#ifdef USE_CUDA
#	include <Demo/Particles/ImpJac.cuh>
#endif

typedef MetricsFrame<Vector3> MetricsFrame3D;
typedef Metrics<Vector3> Metrics3D;

typedef ParticleVelocityT<Vector3> Particle3D;

namespace Collision3D
{
	struct Edge;
}

struct Group
{
	size_t start, stop;

	bool Contains(size_t i) { return i >= start && i < stop; }
};

class PhysicsSystem3D
{
public:
	enum
	{
		WALL_DIST = 200,
		WALL_BOTTOM = -WALL_DIST,
		WALL_TOP = WALL_DIST,
		WALL_LEFT = -WALL_DIST,
		WALL_RIGHT = WALL_DIST,
		WALL_BACK = -WALL_DIST,
		WALL_FRONT = WALL_DIST
	};

	enum MethodType
	{
		METHOD_PBD,
		METHOD_SEQ_IMP,
		METHOD_SEQ_POS,
		METHOD_SEQ_IMP_POST,
		METHOD_IVP
	};

	enum CollisionType
	{
		COLL_NONE = 0,
		COLL_WALLS = 1,
		COLL_SPHERE = 2,
		COLL_SELF = 4,
		COLL_MESH = 8,
		COLL_PARTICLES = 16,
		COLL_PLANE = 32,
	};

	struct ParticleCollision
	{
		Vector3 p, n;
		float d;
		int tri;
		int active;
		int coll;

		ParticleCollision() : active(0) { }
	};

	struct PrimitivePair
	{
		int idx1, idx2;
	};

private:
	std::vector<Particle3D> particles;
	float radius;
	std::vector<Constraint3D> constraints, links, springs, contacts;
	float timeStep; // whole frame time step
	int numIterations;
	int numIterations2; // second number of iterations
	float lengthScale;

	// TODO: take them out
	// collision sphere and mesh
	Vector3 sphPos;
	float sphRad;
	std::unique_ptr<Geometry::Mesh> mesh;
	Vector3 meshOffset;

	// metrics
	Metrics3D metrics;

	// min res projector
	Projector<Particle3D, Constraint3D, Vector3> proj;

	std::vector<float> ev; // initial guess for eigevenvector 
	float rho; // spectral radius

	// grid broadphase stuff
	GridBroadphase3D broadphase, meshGrid;
	std::vector<Geometry::Aabb3> aabbs;

	// cloth mesh
	std::unique_ptr<Geometry::Mesh> cloth;
	std::vector<Geometry::Mesh::Edge> edges; // unique edges
	std::vector<std::vector<int> > incidence; // vertex incidence

	int prevNum;
#if !defined(ANDROID_NDK) && defined(ENABLE_CL)
	ConjResProjectorCL1<Vector3, Particle3D> projCL;
	JacobiVelProjCL<Vector3, Particle3D> velProjCL;
#endif

	Constraint3D mouseSpring;

	Vector3 gravity;
	MethodType method;
	float beta; // baumgarte factor
	float mu; // friction coefficient
	float tolerance;

	PhysicsSystem::SolverType solver;

	std::vector<Group> groups;
	int collFlags;
	float wallDist;
	float wallBottom;
	int numSteps;

	int nFrames;
	std::vector<std::vector<Vector3> > recording;

	bool useOpenCL;
	bool firstRun;

public:
	PhysicsSystem3D();
	~PhysicsSystem3D();
	void Init(float r);
	
	void SetNumParticles(size_t n);
	size_t GetNumParticles() const { return particles.size(); }
	const Particle3D& GetParticle(size_t i) const { return particles[i]; }
	Particle3D& GetParticle(size_t i) { return particles[i]; }	

	size_t GetNumLinks() const { return links.size(); }
	const Constraint3D& GetLink(size_t i) const { return links[i]; }
	
	size_t GetNumConstraints() const { return constraints.size(); }
	const Constraint3D& GetConstraint(size_t i) const { return constraints[i]; }

	float GetTimeStep() const { return timeStep; }
	
	void Step();
	void AddLink(ParticleIdx parIdx1, ParticleIdx parIdx2, int batch = -1, float stiffness = 1.f);

	const Vector3& GetSpherePos() const { return sphPos; }
	float GetSphereRadius() const { return sphRad; }

	void SetNumIterations(int n) { numIterations = n; }
	void SetNumIterations2(int n) { numIterations2 = n; }
	int GetNumIterations() const { return numIterations; }
	float GetLengthScale() const { return lengthScale; }
	MethodType GetMethod() const { return method; }

	Geometry::Mesh* GetMesh() { return mesh.get(); }
	void InitMeshGrid();
	Vector3 GetMeshOffset() const { return meshOffset; }
	void SetMeshOffset(float val) { meshOffset.Set(0, val, 0); }

	Geometry::Mesh* GetClothMesh() { return cloth.get(); }
	void SetClothMesh(const Geometry::Mesh& m);
	void BuildMeshAdjacency();

	void AddMouseSpring(int i, const Vector3& p);
	void RemoveMouseSpring() { mouseSpring.type = Constraint3D::DUMMY; }
	void UpdateMouseSpring(const Vector3& p) { if (mouseSpring.type != Constraint3D::DUMMY) mouseSpring.point = p; }

	void SetMethod(MethodType mt) { method = mt; }
	void SetTolerance(float val) { tolerance = val; }
	void SetFriction(float val) { mu = val; }
	void SetBaumgarte(float val) { beta = val; }
	float GetBaumgarte() const { return beta; }
	void SetSolver(PhysicsSystem::SolverType val) { solver = val; }
	PhysicsSystem::SolverType GetSolver() const { return solver; }

	void ClearGroups();
	void EndGroup();
	bool InGroup(size_t i, size_t g) { return groups[g].Contains(i); }
	int GetCollisionFlags() const { return collFlags; }
	void SetCollisionFlags(int val) { collFlags = val; }
	void SetWallBottom(float val) { wallBottom = val; }
	void SetNumSteps(int val) { numSteps = val; }

	const Vector3& GetPosition(size_t i, int frame) { return recording[frame][i]; }	
	size_t GetRecordingLength() const { return recording.size(); }

	void SetUseOpenCL(bool val) { useOpenCL = val; }

	void OptimizeLinks(const std::vector<int>& order);

	void IntegrateAndCollide(float h);
    void ProjectPostitionsGS(float h, int maxIter = 0);
	void SolveConstraints(float h, int iterations = -1);
	void SolveConstraintsIVP(float h, int iterations = -1);
	void ProjectVelocitiesImplicitGS(float h, int maxIter = 0);

private:
	void WallCollisions();
	void AddContact(ParticleIdx idx, const Vector3& p, const Vector3& n);
	void PairwiseBroadphase();	
	void MicroStep(float h);
	void MicroStepIVP(float h);
	void Collide();
	void CollideWithMesh(const Geometry::Mesh* mesh);
	void CollideWithSphere();
	void UpdateAabbs();
	void SelfCollision();
	void SelfCollision1();
	void SelfCollision2();
	void SelfCollisionStatic();
	void SelfCollisionDynamic();
	void CollideParticles();
	void CollideWithPlane();
	void PruneEdgeCollisions(std::vector<PrimitivePair>& candidates);
	void PruneEdgeCollisionsStatic(std::vector<PrimitivePair>& candidates);

	void PostFriction();
	void IntegrateSE(float h);
	void HandleSprings();

	void MicroStepSI(float h);
	void ProjectVelocities(float h, int iterations);
	void SolveVelocitiesGS(float h, int iterations);
	void SolveVelocitiesJacobi(float h, float omega);
	void SolvePositionsExplicitGS(int iterations);

	void MicroStepSP(float h);
	void SolveSP(float h);
	void SolveSP_GS(float h);
	void SolveSP_CR(float h, float omega, PhysicsSystem::SolverType solver);
	void SolveTriangle(float h, Constraint3D& contact, float alpha, float beta, int k);
	void SolveTriangleCR(float h, Constraint3D& contact, float omega, float alpha, float beta, float b, int k);
	void UpdateTriangleCR(float h, Constraint3D& contact, int k);

	void MicroStepSpring(float h);
	
	void MicroStepUnified(float h);
	void SolveUnified(float h);
	void ProjectContact(Constraint3D& contact, float h, float omega = 1.f);
	void ProjectContactVelocity(Constraint3D& contact, float h, float omega = 1.f);
	void ProjectTriangleSelf(Constraint3D& contact, float h);
	void ProjectEdgesSelf(Constraint3D& contact, float h);

	void PrintInfo();	
};

inline void PhysicsSystem3D::SetNumParticles(size_t n)
{
	particles.resize(n);
	aabbs.resize(n);
}

inline void PhysicsSystem3D::ClearGroups()
{
	groups.clear();
	Group group;
	group.start = 0;
	group.stop = 0;
	groups.push_back(group);
}

inline void PhysicsSystem3D::AddContact(ParticleIdx idx, const Vector3& p, const Vector3& n)
{
	constraints.push_back(Constraint3D(idx, p, n, radius));
}

inline void PhysicsSystem3D::AddLink(ParticleIdx parIdx1, ParticleIdx parIdx2, int batch, float stiffness)
{
	if (stiffness == 0)
		return;
	float len = float((particles[parIdx1].pos - particles[parIdx2].pos).Length());
	Constraint3D link(Constraint3D::LINK, parIdx1, parIdx2, len);
	link.stiffness = stiffness;
	link.batch = batch;
	if (stiffness < 0)
	{
		springs.push_back(link);
		link.stiffness = -stiffness;
	}
	else
		links.push_back(link);
}

#endif // PHYSICS_SYSTEM_3D_H
