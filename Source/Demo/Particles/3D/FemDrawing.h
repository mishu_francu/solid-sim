#ifndef FEM_SYSTEM_DRAWING_H
#define FEM_SYSTEM_DRAWING_H
#include <Physics/FEM/FemDataStructures.h>
#include <Physics/FEM/FemPhysicsBase.h>
#include <Graphics3D\Graphics3D.h>

namespace FEM_SYSTEM
{
	class FemDrawing
	{
		public:
			FemDrawing(std::vector<Tet>& tetrahedra,
					   std::vector<Node>& nodes, FEM_SYSTEM::FemPhysicsBase& femPhysics);
			~FemDrawing();

			void DrawTetrahedra(Graphics3D* graphics) const;
			void DrawBBTetrahedra(Graphics3D* graphics3D) const;

		private:
			int vertexCount;
			std::vector<Tet>& tets;
			std::vector<Node>& nodes;
			FEM_SYSTEM::FemPhysicsBase& mFemPhysics;
	};
}
#endif