// Mihai Francu
// 12 Oct 2015

#ifndef FEM_SYSTEM_H
#define FEM_SYSTEM_H
#include <Physics/FEM/FemDataStructures.h>
#include <Demo/Particles/3D/FemDrawing.h>
#include <Physics/FEM/FemPhysicsMatrixFree.h>
#include <Demo/Particles/3D/FemTester.h>
#include <Geometry/Mesh.h>

#include <memory>
#include <set>

#include <fstream>
#include <ostream>

#ifdef USE_ALEMBIC
// Alembic Includes
#include <Alembic/AbcGeom/All.h>
// This is required to tell Alembic which implementation to use.
#include <Alembic/AbcCoreOgawa/All.h>

using namespace Alembic::AbcGeom;
#endif


namespace FEM_SYSTEM
{
	class FemSystem
	{
	public:
		enum HeatMapType
		{
			HMT_NONE,
			HMT_VOL_STRAIN,
			HMT_PRESSURE,
			HMT_VON_MISES,
		};
	public:
		FemSystem(const std::string& name);
		~FemSystem();

		void   SetNumNodes(size_t n);
		size_t GetNumNodes() const { return nodes.size(); }
		Node&  GetNode(size_t i) { return nodes.at(i); }

		void AddTetrahedron(uint16 i0, uint16 i1, uint16 i2, uint16 i3);
		uint32 GetNumTets() const { return (uint32)tets.size(); }
		const Tet& GetTet(uint32 i) const { return tets[i]; }
	
		void Clear();
		void Init(FemConfig& config);
		void UpdatePositions() { femPhysics->UpdatePositions(nodes); }
	
		bool LoadFromVolFile(const char* path, real scale = 10.f);
		bool LoadFromTetFile(const char* path, real scale, const Vector3R& offset);
		bool LoadFromTet1File(const char* path, real scale, const Vector3R& offset);
		bool LoadFromNodeEleFile(const char * path, real scale, const Vector3R & offset);
		bool LoadFromFebFile(const char* path, std::vector<int>& fixedNodes, std::vector<uint32>& surfTris, std::set<uint32>& innerSurface, real scale);
		bool LoadFromXmlFile(const char* path, std::vector<int>& fixedNodes, std::vector<uint32>& surfTris, FemConfig& config, std::vector<CableDescriptor>& cables);

		FemDrawing* GetFemSystemDrawing() const;
		FemPhysicsBase* GetFemPhysics() const;

		void BuildBoundaryMesh();
		void UpdateBoundaryMesh(HeatMapType heatMapType = HMT_NONE);
		const Geometry::Mesh& GetBoundaryMesh() { return mBoundaryMesh; }

		// NOTE part of experimental feature of HO_RENDERING
		void BuildBBBoundaryMesh();
		// NOTE part of experimental feature of HO_RENDERING
		void UpdateBBBoundaryMesh();
		// NOTE part of experimental feature of HO_RENDERING
		const Geometry::Mesh& GetBBBoundaryMesh() { return mBBBoundaryMesh; }
		int GetNodeFromBoundaryIndex(int idx) const { return mBoundaryToNodesMap[idx]; }
		int GetElementFromBoundaryTriangle(int tri) const { return mBoundaryToElementsMap[tri]; }


		FemTester& GetFemTester() { return mTester; }

		void ExportToNodeEleFile(std::fstream& outNode, std::fstream& outEle);
		void ExportToVTKHeatMap(std::fstream& outfile);
		void ExportToVTKFile(std::fstream& outfile);
		void ExportToAlembic(bool exportNormals = false);

		void LoadVisualMesh(const char* path, const Vector3& offset, float scale);
		void UpdateVisualMesh();
		const Geometry::Mesh& GetVisualMesh() const { return mVisualMesh; }

		void CreateCable(const CableDescriptor& descriptor, real scale = 100);

		Vector3 GetColour(double v, double vmin, double vmax)
		{
			Vector3 c = { 1.0,1.0,1.0 }; // white
			double dv;

			if (v < vmin)
				v = vmin;
			if (v > vmax)
				v = vmax;
			dv = vmax - vmin;

			if (v < (vmin + 0.25 * dv)) {
				c.x = 0;
				c.y = float(4 * (v - vmin) / dv);
			}
			else if (v < (vmin + 0.5 * dv)) {
				c.x = 0;
				c.z = float(1 + 4 * (vmin + 0.25 * dv - v) / dv);
			}
			else if (v < (vmin + 0.75 * dv)) {
				c.x = float(4 * (v - vmin - 0.5 * dv) / dv);
				c.z = 0;
			}
			else {
				c.y = float(1 + 4 * (vmin + 0.75 * dv - v) / dv);
				c.z = 0;
			}

			return(c);
		}

		std::vector<Node> nodes;
		std::vector<Tet> tets;

	private:
		void MapMesh();

		void UninvertElements();

		void ReshuffleFixedNodes();


	private:
		FemDrawing* femSystemDrawing;
		FemPhysicsBase* femPhysics;

		// boundary mesh
		Geometry::Mesh mBoundaryMesh;
		std::vector<int> mNodesToBoundaryMap;
		std::vector<int> mBoundaryToNodesMap;
		std::vector<int> mBoundaryToElementsMap;

		Geometry::Mesh mBBBoundaryMesh;
		//std::map<uint32, std::vector<uint32>> bbElementInfo;
		std::vector<float> bb_l1;
		std::vector<float> bb_l2;
		std::vector<float> bb_l3;
		std::vector<size_t> bb_noNodes;
		std::vector<std::vector<uint32>> bb_nodes;
		std::vector<std::vector<int>> bb_IJKs;

		uint32 mNumFixed = 0;

		// tests
		FemTester mTester;

#ifdef USE_ALEMBIC
		// Alembic
		Alembic::Abc::OArchive mAbcArchive;
		Alembic::AbcGeom::OPolyMesh* mAbcMesh;
		std::vector<Alembic::AbcGeom::OPolyMesh*> mCollMesh;

		Geometry::Mesh mBoxMesh;
#endif

		// visual mesh interpolation
		Geometry::Mesh mVisualMesh;
		std::vector<MeshInterp> mMapData;
	};

	inline void FemSystem::SetNumNodes(size_t n)
	{
		nodes.resize(n);
	}

	inline void FemSystem::AddTetrahedron(uint16 i0, uint16 i1, uint16 i2, uint16 i3)
	{
		Tet tet;
		tet.idx[0] = i0;
		tet.idx[1] = i1;
		tet.idx[2] = i2;
		tet.idx[3] = i3;
		tets.push_back(tet);
	}

	inline void FemSystem::Clear()
	{
		nodes.clear();
		tets.clear();
	}

	inline FemDrawing* FemSystem::GetFemSystemDrawing() const
	{
		return femSystemDrawing;
	}
	inline FemPhysicsBase* FemSystem::GetFemPhysics() const
	{
		return femPhysics;
	}
}

#endif