#include "FemUtility.h"
#include <Engine/Utils.h>

bool FemUtility::LoadFromTetFile(const char* path, VolumeMesh& output)
{
	FILE *ptr_file;
	char buf[1000];

	errno_t ret = fopen_s(&ptr_file, path, "r");
	if (ret != 0)
	{
		printf("Couldn't open file %s\n", path);
		return false;
	}
	const int STATE_READ = 0;
	const int STATE_VOL = 1;
	const int STATE_ELEM = 2;
	const int STATE_POINTS = 3;
	const int STATE_POINT = 4;
	int state = STATE_READ;
	int nod = 0;
	while (fgets(buf, 1000, ptr_file) != NULL)
	{
		char* ptr = buf;
		char* word = buf;

		switch (state)
		{
		case STATE_READ:
		{
			if (strncmp(buf, "num_vertices", 12) == 0)
			{
				word = ptr + 12;
				int n = atoi(word);
				output.mPoints.resize(n);
			}
			else if (strncmp(buf, "VERTICES", 8) == 0)
			{
				state = STATE_POINT;
			}
			else if (strncmp(buf, "TETRAS", 6) == 0)
			{
				state = STATE_ELEM;
			}
			break;
		}
		case STATE_ELEM:
		{
			int indx[4];
			int counter = 0;
			while (ptr = strchr(word, ' '))
			{
				*ptr = '\0';

				int x = atoi(word);
				indx[counter] = x;
				counter++;
				word = ptr + 1;
			}
			if (counter == 0)
			{
				state = STATE_READ;
				break;
			}
			output.mTetrahedra.push_back({ indx[0], indx[1], indx[2], indx[3] });
			break;
		}
		case STATE_POINT:
		{
			Math::Vector3 pos;
			int counter = 0;
			while (ptr = strchr(word, ' '))
			{
				if (ptr == word)
				{
					word++;
					continue;
				}
				*ptr = '\0';
				float x = (float)atof(word);
				pos[counter] = x;
				counter++;
				word = ptr + 1;
			}
			if (counter == 0)
			{
				state = STATE_ELEM;
				break;
			}
			output.mPoints[nod++] = pos;
			break;
		}

		}
	}

	fclose(ptr_file);
	return true;
}

bool FemUtility::LoadFromVolFile(const char* path, VolumeMesh& output)
{
	FILE *ptr_file;
	char buf[1000];

	const int STATE_READ = 0;
	const int STATE_VOL = 1;
	const int STATE_ELEM = 2;
	const int STATE_POINTS = 3;
	const int STATE_POINT = 4;

	errno_t ret = fopen_s(&ptr_file, path, "r");
	if (ret != 0)
	{
		Printf("Couldn't open file %s\n", path);
		return false;
	}

	int state = STATE_READ;
	int nod = 0;
	while (fgets(buf, 1000, ptr_file) != NULL)
	{
		int counter = 0;
		char* ptr = buf;
		char* word = buf;
		int idx[4];
		switch (state)
		{
		case STATE_READ:
			if (strncmp(buf, "volumeelements", 14) == 0)
			{
				state = STATE_VOL;
			}
			else if (strncmp(buf, "points", 6) == 0)
			{
				state = STATE_POINTS;
			}
			break;
		case STATE_VOL:
		{
			int n = atoi(buf);
			state = STATE_ELEM;
		}
		break;
		case STATE_ELEM:
			while (ptr = strchr(word, ' '))
			{
				*ptr = '\0';
				counter++;
				int x = atoi(word);
				if (counter > 2)
				{
					idx[counter - 3] = x - 1;
				}
				word = ptr + 1;
			}
			idx[3] = atoi(word) - 1;
			if (counter == 0)
				state = STATE_READ;
			else
				output.mTetrahedra.push_back({ idx[0], idx[1], idx[2], idx[3] });
			break;
		case STATE_POINTS:
		{
			int n = atoi(buf);
			output.mPoints.resize(n);
			state = STATE_POINT;
		}
		break;
		case STATE_POINT:
			Math::Vector3 pos;
			while (ptr = strchr(word, ' '))
			{
				if (ptr == word)
				{
					word++;
					continue;
				}
				*ptr = '\0';
				float x = (float)atof(word);
				pos[counter] = x;
				counter++;
				word = ptr + 1;
			}
			pos[2] = (float)atof(word);
			if (counter == 0)
				state = STATE_READ;
			else
				output.mPoints[nod++] = pos;
			break;
		}
	}

	fclose(ptr_file);
	return true;
}