#include "FEMDemo.h"
#include <Graphics3D/Graphics3D.h>
#include <Engine/Engine.h>

#include "tetgen1.5.1/tetgen.h"
#ifdef USE_IMGUI
#include <imgui.h>
#endif

#include <algorithm>
#include <array>
#include <stack>
#include <fstream>
#include <ostream>
#include <vector>

#ifdef BULLET_COLLISIONS
#include <Physics/Unified/BulletWrapper.h>
#endif
#include <Geometry/Assets.h>
#include <Geometry/Collision3D.h>

#include <Physics/FEM/FemPhysicsMixed.h>
#include <Physics/FEM/FemCollisions.h>

using namespace Geometry;

#pragma warning( disable : 4267) // for size_t-uint conversions

//#define RUN_TESTS
//#define TETRAHEDRON
//#define USE_GHOST
#define USE_VISUAL_MESH
//#define DRAW_CONTACTS
//#define CEILING

Model mModels[numModels] =
{
	{ "../Models/sphere.vol", .3f, {0, 0, 0}, MFF_VOL, 3, new int[3]{1, 2, 3}}, // 0
	{ "../Models/hinge.vol", .01f, {0, 0, 0}, MFF_VOL, 13, new int[13]{87, 44, 100, 45, 101, 46, 102, 47, 103, 48, 104, 762, 38} }, // 1
	{ "../Models/lshape3d.vol", .5f, {0, 0, 0}, MFF_VOL, 2, new int[2]{8, 5} }, // 2
	{ "../Models/bunny_low.vol", 0.02f, {0, 0, 0}, MFF_VOL, 4, new int[4]{153, 60, 61, 113} },// 3
	{ "../Models/cow.tet", .05f, {0.f, 0.f, 0.f}, MFF_TET, 3, new int[3]{0, 1, 3} }, // 4
	{ "../Models/torus2.vol", 0.05f }, // 5
	{"../Models/box.vol", .3f, {0, 0, 0}, MFF_VOL, 4, new int[4]{0, 5, 10, 15} }, // 6
	{"../Models/box1.vol", .3f, {0, 0, 0}, MFF_VOL, 8, new int[8]{0, 10, 11, 22, 23, 33, 34, 44}}, // 7
	{"../Models/box_refined.vol", .3f, {0, 0, 0}, MFF_VOL, 9, new int[9]{15, 64, 10, 24, 23, 47, 0, 21, 5}}, // 8
	{"../Models/spine.tet", .001f, {0, 0, 0}, MFF_TET1, 9, new int[9]{27, 28, 29, 186, 187, 188, 413, 414, 415}}, // 9
	{"../Models/3_wedge_finger.tet", .001f, {0, 0, 0}, MFF_TET1, 0, nullptr, SCT_SWAP_NONE, 0, FS_MIN}, // 10
	{ "../Models/sphere_fine.vol", .3f, {0, 0, 0}, MFF_VOL, 3, new int[3]{1, 2, 3}}, // 11
	{ "../Models/dragon.tet", .03f, {0.f, 0.f, 0.f}, MFF_TET }, // 12
	{ "../Models/torus1.vol", 0.05f }, // 13
	{"../Models/simplespine.tet", .001f, {0, 0, 0}, MFF_TET1, 10, new int[10]{55, 56, 57, 176, 177, 178, 327, 328, 329, 274}},  // 14
	{"../Models/tube.vol", 0.1f, {0, 0, 0}, MFF_VOL, 24, new int[24]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34}, SCT_SWAP_NONE, 1, FS_MIN | FS_MAX}, // 15
	{"../Models/spine.tet", .001f, {0, 0, 0}, MFF_TET1, 18, new int[18]{27, 28, 29, 186, 187, 188, 413, 414, 415, 22, 23, 24, 180, 181, 182, 407, 408, 409} }, // 16
	{"../Models/tube_hi.vol", 0.1f, {0, 0, 0}, MFF_VOL, 81,
		new int[81]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
					71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111},
		SCT_SWAP_NONE, 1, FS_MIN | FS_MAX}, // 17
	{"../Models/basicbubble.tet", .01f, {0, 0, 0}, MFF_TET1, 0, nullptr, SCT_SWAP_Y_AND_Z},  // 18
	{"../Models/cylinder_hi.vol", .1f, {0, 0, 0}, MFF_VOL, 0, nullptr, SCT_SWAP_NONE, 0, FS_MIN | FS_MAX}, // 19
	{"../Models/torso.vol", 0.01f}, // 20
	{"../Models/torso_smooth_2mm.1.node", 0.01f, {0, 0, 0}, MFF_NODEELE}, // 21
	{"../Models/torso_smooth_4mm.1.node", 0.01f, {0, 0, 0}, MFF_NODEELE}, // 22
	{"../Models/torso_smooth_2.5mm.1.node", 0.01f, {0, 0, 0}, MFF_NODEELE}, // 23
	{"../Models/torso_smooth_5mm.1.node", 0.01f, {0, 0, 0}, MFF_NODEELE}, // 24
	{"../Models/Femur.node", 0.001f, {0, 0, 0}, MFF_NODEELE, 0, nullptr, SCT_SWAP_Y_AND_Z/*55, new int[55]{0,2,4,5,55,90,114,159,205,207,235,274,423,424,425,446,493,584,639,645,662,663,665,706,707,745,788,809,820,853,854,907,969,980,981,993,1008,1042,1050,1073,1074,1075,1113,1143,1160,1170,1218,1233,1272,1278,1340,1341,1360,1366,1367}*/}, // 25
	{"../Models/cartilageASCII.1.node", 1.f, {0, 0, 0}, MFF_NODEELE, 19, new int[19]{437, 435, 434, 432, 428, 425, 422, 416, 415, 414, 412, 411, 410, 409, 408, 407, 404, 403, 402}, SCT_SWAP_Y_AND_Z}, // 26
	{"../Models/cushion.tet", .001f, {0, 0, 0}, MFF_TET1, 18, new int[18]{27, 28, 29, 186, 187, 188, 413, 414, 415, 22, 23, 24, 180, 181, 182, 407, 408, 409} }, // 27
	{"../Models/finger.tet", .1f, {0, 0, 0}, MFF_TET1}, // 28
	{"../Models/tube.node", 0.1f, {0, 0, 0}, MFF_NODEELE}, // 29
	{ "../Models/ball.1.node", .01f, {0, 0, 0}, MFF_NODEELE}, // 30
	{ "../Models/from_stl.feb", .01f, {0, 0, 0}, MFF_FEB}, // 31
	{ "../Models/cat_regular.1.node", .02f, {0, 0, 0}, MFF_NODEELE, 0, nullptr, SCT_SWAP_X_AND_Y}, // 32
	{ "../Models/finger.feb", .01f, {0, 0, 0}, MFF_FEB}, // 33
	//{ "../Models/hammerbot_fine.tet", .01f, {0, 0, 0}, MFF_TET1, 0,  nullptr, SCT_SWAP_Y_AND_Z, 0, FS_MIN}, // 34
	{ "../Models/hammerbot_fine.tet", .01f, {0, 0, 0}, MFF_TET1, 0,  nullptr, SCT_SWAP_X_AND_Y, 1, FS_MAX}, // 34
	{ "../Models/box2.tet", 1, {0, 0, 0}, MFF_TET1, 9, new int[9]{0, 1, 2, 3, 4, 5, 6, 7, 8} }, // 35
	{ "../Models/cantilever.xml", 1, {0, 0, 0}, MFF_XML }, // 36
};

using namespace FEM_SYSTEM;

void FEMDemo::Displace(FEM_SYSTEM::FemSystem* femSys, real delta, bool rotate)
{
	Vector3R dx(delta, 0, 0);
	real cosTheta = cos(delta);
	real sinTheta = sin(delta);
	real mid = 0.01f * 0.5f * mConfig1.mCantileverWidth;
	for (uint32 i = 0; i < femSys->GetNumNodes(); i++)
	{
		auto& node = femSys->GetNode(i);
		// only BC nodes (zero mass) should be displaced
		if (node.invMass == 0)
		{
			if (rotate)
			{
				if (node.pos.x > 0)
				{
					real y0 = node.pos.y - mid;
					real z0 = node.pos.z - mid;
					real y = mid + y0 * cosTheta - z0 * sinTheta;
					real z = mid + y0 * sinTheta + z0 * cosTheta;
					Vector3R newPos(node.pos.x, y, z);
					node.pos = newPos;
				}
			}
			else
			{
				if (node.pos.x > 0)
				{
					node.pos += dx;
				}
				//else
				//	node.pos -= dx;
			}
		}
		// do not displace the free nodes unless you know what you're doing
		else {
			if (mSelectedModel == MI_HALF_PELVIC) {
				node.pos += Vector3R(0, -delta, 0);
			}
		}

	}
	femSys->UpdatePositions();
}

void FEMDemo::Init()
{
	mLoadSkel = mSelectedModel == MI_TORSO_SKIN;

	// reset vars
	mUpdateCounter = 0;
	mElapsedTime = 0;
	mForceStep = 0;
	mDeflectionCurve.clear();
	mDeflectionPoint.clear();
	mCeilingState = 0;

#if defined(_DEBUG) && defined(RUN_TESTS)
	mFemSys->GetFemTester().Test();
#endif	
	mConfig1 =
	{
		"femSys1", // name
		{0, 0, 0}, // offset
		MT_COROTATIONAL_ELASTICITY, // type
		1, // order
		3, // resolution
		false, // use cantilever
		10,
		4,
		9, // model index
		false, // apply tractions
		-12000.f, // applied external pressure
		0, // gravity
		66.f * 1000.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.45f, // Poisson ratio
		true, // apply displacements
		0, // displacement amount
		6, // number of force application steps (quasi-static)
	};

	// Mihai: commented this out as it was overriding my changes in UI
	if (mChosenPreset > 0)
	{
		ApplyConfigPreset(mPresetConfigs[mChosenPreset]);
	}

	// convert UI data to config data
	mConfig1.mType = (FEM_SYSTEM::MethodType)mSelectedSimulator;
	if (mSelectedSimulator == MT_INCOMPRESSIBLE_LINEAR_ELASTICITY)
	{
		mIncompressibleConfig.mUseCorotational = false;
		mIncompressibleConfig.mUseKKTMatrix = true;
		mIncompressibleConfig.mUseStaticCondensation = false;
		mIncompressibleConfig.mPressureOrder = mPressureOrderValue;
		mConfig1.mCustomConfig = &mIncompressibleConfig;
	}
	else if (mSelectedSimulator == MT_INCOMPRESSIBLE_COROTATIONAL_ELASTICITY)
	{
		mIncompressibleConfig.mUseCorotational = true;
		mIncompressibleConfig.mUseKKTMatrix = true;
		mIncompressibleConfig.mUseStaticCondensation = false;
		mIncompressibleConfig.mPressureOrder = mPressureOrderValue;
		mConfig1.mCustomConfig = &mIncompressibleConfig;
	}
	else if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY)
	{
		mNonlinearConfig.mSolver = (NonlinearSolverType)mNonlinearSolverValue;
		mNonlinearConfig.mDescentRate = mDescentRateValue;
		mNonlinearConfig.mOptimizer = mOptimizer;
		mConfig1.mCustomConfig = &mNonlinearConfig;
	}
	else if (mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
	{
		mIncompressibleConfig.mUseCorotational = true;
		mIncompressibleConfig.mUseKKTMatrix = true;
		mIncompressibleConfig.mUseStaticCondensation = true;
		mIncompressibleConfig.mPressureOrder = mPressureOrderValue;
		mIncompressibleConfig.mConstraintScale = mConstraintScaleValue;
		mIncompressibleConfig.mLogConstraint = mLogConstrValue;
		mIncompressibleConfig.mSolver = (NonlinearSolverType)mNonlinearSolverValue;
		mConfig1.mCustomConfig = &mIncompressibleConfig;
	}

	mConfig1.mYoungsModulus = mYoungsModulusValue * 1000.f;
	mConfig1.mPoissonRatio = mPoissonRatioValue;
	mConfig1.mModelIdx = mSelectedModel;
	mConfig1.mAppliedPressure = -mPressureValue;
	mConfig1.mSimType = (SimulationType)mSelectedSimType;
	mConfig1.mGravity = mGravityValue;
	mConfig1.mUseCantilever = mCheckCantilever;
	mConfig1.mNumSubsteps = mSubstepsValue;
	mConfig1.mApplyTractions = mCheckApplyPressure;
	mConfig1.mDisplace = mCheckDisplace;
	mConfig1.mNumForceSteps = mForceStepsValue;
	mConfig1.mResolution = mModelResolution;
	mConfig1.mOrder = mOrder;
	mConfig1.mCantileverWidth = mCantileverWidth;
	mConfig1.mCantileverRatio = mCantileverRatio;
	mConfig1.mDisplacementWidth = mDisplacementWidth;
	mConfig1.mMaterial = (MaterialModelType)mMaterialModel;
	mConfig1.mInvBulkModulus = mInvBulkModulusValue;
	mConfig1.mOuterIterations = mOuterIterationsValue;
	mConfig1.mHasCollisions = mCheckCollisions;
	mConfig1.mContactStiffnes = mContactStiffnessValue;
	mConfig1.mResidualThreshold = mResidualThresholdValue;
	mConfig1.mLogConstraint = mLogConstrValue;
	mConfig1.mUseCables = mUseCables;

	if (mDrawHeatMap) {
		mDrawColor[0] = 0;
		mDrawColor[1] = 0;
		mDrawColor[2] = 0;
	}

	if (mLoadSkel)
	{
		mSkeleton = Skeleton(); // clear previous data
		LoadSkeleton("../Models/torso_anim.dae", mSkeleton);
		mSkeleton.ComputeWorldTransforms();
		mSkeleton.SetCurrentAnimation(0);
	}

	bool ret = LoadMesh("../Models/femurCart.obj", mCollisionMesh, Vector3(0,0,0), 100, true);
	Init(mFemSys, mConfig1); // red
	if (!mFemSys)
		return;

	//if (ret)
	//{
	//	//mCollisionMesh.ComputeNormals();
	//	mFemSys->GetFemPhysics()->SetCollisionMesh(&mCollisionMesh);
	//}


#ifdef BULLET_COLLISIONS
	mFemSys->GetFemPhysics()->InitBulletCollision();
#endif

#ifdef USE_GHOST	
	mConfig2 = mConfig1;
	mConfig2.mName = "femSys2";
	mConfig2.mOffset.z = -100;
	mConfig2.mType = MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY;
	mConfig2.mMaterial = MMT_DISTORTIONAL_NH;
	mIncompressibleConfig.mUseCorotational = true;
	mIncompressibleConfig.mUseKKTMatrix = true;
	mIncompressibleConfig.mUseStaticCondensation = true;
	mIncompressibleConfig.mPressureOrder = mPressureOrderValue;
	mConfig2.mCustomConfig = &mIncompressibleConfig;
	Init(mFemSys2, mConfig2); // green
#endif
}

void FindExtremes(FEM_SYSTEM::FemSystem* femSys, int axis, std::vector<uint32>& extrNodes, bool max = false)
{
	real extr = femSys->GetNode(0).pos[axis];
	for (uint32 i = 0; i < femSys->GetNumNodes(); i++)
	{
		real val = (femSys->GetNode(i).pos[axis]);
		if (!max)
		{
			if (val <= extr)
			{
				if (val < extr)
				{
					extr = val;
					extrNodes.clear();
				}
				extrNodes.push_back(i);
			}
		}
		else
		{
			if (val >= extr)
			{
				if (val > extr)
				{
					extr = val;
					extrNodes.clear();
				}
				extrNodes.push_back(i);
			}
		}
	}
	Printf("extr=%f\n", extr);
}

void FEMDemo::Init(std::unique_ptr<FEM_SYSTEM::FemSystem>& femSys, const Config& config)
{
	femSys.reset(nullptr);
	femSys.reset(new FEM_SYSTEM::FemSystem(config.mName));

	std::vector<int> fixedNodes;
	std::vector<uint32> surfTris;
	mInnerSurface.clear();
	std::vector<CableDescriptor> cables;

	Model& model = mModels[config.mModelIdx];
	if (!config.mUseCantilever)
	{
		bool fileLoaded = false;
		if (model.mFileFormat == MFF_TET)
			fileLoaded = femSys->LoadFromTetFile(model.mName, model.mScale, model.mOffset);
		else if (model.mFileFormat == MFF_VOL)
			fileLoaded = femSys->LoadFromVolFile(model.mName, model.mScale);
		else if (model.mFileFormat == MFF_TET1)
			fileLoaded = femSys->LoadFromTet1File(model.mName, model.mScale, model.mOffset);
		else if (model.mFileFormat == MFF_NODEELE)
			fileLoaded = femSys->LoadFromNodeEleFile(model.mName, model.mScale, model.mOffset);
		else if (model.mFileFormat == MFF_FEB)
		{
			femSys->LoadFromFebFile(model.mName, fixedNodes, surfTris, mInnerSurface, model.mScale);
			model.mFixedNodes = &fixedNodes[0];
			model.mNumFixed = (uint32)fixedNodes.size();
			fileLoaded = true;
		}
		else if (model.mFileFormat == MFF_XML)
		{
			FemConfig xmlConfig;
			femSys->LoadFromXmlFile(model.mName, fixedNodes, surfTris, xmlConfig, cables);
			model.mFixedNodes = &fixedNodes[0];
			model.mNumFixed = (uint32)fixedNodes.size();
			fileLoaded = true;
			// TODO: update the config and UI
		}

		if (!fileLoaded)
		{
			// an error has occurred
			Printf("An error has occurred while loading tet mesh %s\n", model.mName);
			femSys.reset(nullptr);
			return;
		}

		// MAX: Place dirichlet conditions code here
		auto testAddDirechletCondition = [&](int idx)->bool
		{
			const Node& node = femSys->GetNode(idx);
			Vector3R pos = node.pos - config.mOffset * 0.01f;
			if (config.mModelIdx == MI_CUSHION)
			{
				auto cornerCheckReturn = [&](real v0, real v1)->float
				{
					if (v0 > 0 && v1 > 0)
					{
						return (float)sqrt(v0 * v0 + v1 * v1);
					}
					else
					{
						return (float)max(v0, v1);
					}
				};
				float scale = mModels[config.mModelIdx].mScale;
				float x = 40 * scale / 2;
				return pos.x >= 0 && cornerCheckReturn(abs(pos.y) - 8 * scale, abs(pos.z) - 8 * scale) <= 0;
			}
			else
			{
				return false;
			}
		};

		if (config.mModelIdx == MI_CUSHION) // The cushion
		{
			std::vector<int> fixedNodes;
			for (uint32 i = 0; i < femSys->GetNumNodes(); i++)
			{
				if (testAddDirechletCondition(i))
				{
					fixedNodes.push_back(i);
					Printf("%d ", i);
				}
			}
			Printf("\n");
			for (std::vector<int>::iterator it = fixedNodes.begin(); it != fixedNodes.end(); ++it)
			{
				int idx = *it;
				femSys->GetNode(idx).invMass = 0.f;
			}
		}
		else if (!mCheckCollisions) // hack: leave the body free when doing collisions
		{
			for (uint32 i = 0; i < model.mNumFixed; i++)
			{
				int idx = model.mFixedNodes[i];
				femSys->GetNode(idx).invMass = 0.f;
			}
			if (fixedNodes.size() != 0)
				model.mFixedNodes = nullptr; // memory will be realeased by std::vector
		}

		if (config.mModelIdx == MI_HALF_PELVIC) {
			for (int i = 0; i < 200; i++) {
				femSys->GetNode(i).invMass = 0.f;
			}
		}

		// post-process nodes
		FEM_SYSTEM::Vector3R initialVel(0.f, 0, 0.f);
		for (uint32 i = 0; i < femSys->GetNumNodes(); i++)
		{
			FEM_SYSTEM::Node& node = femSys->GetNode(i);
			if (model.mSwapCoords == SCT_SWAP_X_AND_Y)
				std::swap(node.pos.x, node.pos.y);
			else if (model.mSwapCoords == SCT_SWAP_X_AND_Z)
				std::swap(node.pos.x, node.pos.z);
			else if (model.mSwapCoords == SCT_SWAP_Y_AND_Z)
				std::swap(node.pos.y, node.pos.z);
			node.pos += config.mOffset * 0.01f; // convert cm to m;
			node.vel += initialVel;
		}

		if (model.mFixedAxis != -1)
		{
			// find extreme positions
			std::vector<uint32> pinnedNodes;
			int axis = model.mFixedAxis;
			if (model.mFixedSide & FS_MIN)
			{
				FindExtremes(femSys.get(), axis, pinnedNodes);
				for (uint32 i = 0; i < pinnedNodes.size(); i++)
				{
					Printf("%d ", pinnedNodes[i]);
					femSys->GetNode(pinnedNodes[i]).invMass = 0.f;
				}
			}

			if (model.mFixedSide & FS_MAX)
			{
				pinnedNodes.clear();
				FindExtremes(femSys.get(), axis, pinnedNodes, true);
				for (uint32 i = 0; i < pinnedNodes.size(); i++)
				{
					Printf("%d ", pinnedNodes[i]);
					femSys->GetNode(pinnedNodes[i]).invMass = 0.f;
				}
			}
		}
	}
	else // initialize the cantilever
	{
#ifndef TETRAHEDRON
		int ratio = config.mCantileverRatio;
		float width = config.mCantileverWidth;
		const float length = ratio * width;
		if (config.mResolution == 0)
		{
			InitTetGenCantilever(femSys.get(), config.mOffset, length, width, width, 1, 1, 1);
		}
		else if (config.mResolution > 0)
		{
			InitTetGenCantilever(femSys.get(), config.mOffset, length, width, width, ratio * config.mResolution, 1 * config.mResolution, 1 * config.mResolution);
		}
		else
		{
			InitCantilever(femSys.get(), config.mOffset, length, (int)width, -config.mResolution); // FIXME
		}
#else
		InitTetrahedron(femSys.get(), config.mOffset);
#endif
	}

	// convert demo config to FEM config
	FemConfig femConfig;
	femConfig.mYoungsModulus = config.mYoungsModulus;
	femConfig.mPoissonRatio = config.mPoissonRatio;
	femConfig.mGravity = config.mGravity;
	femConfig.mType = config.mType;
	femConfig.mOrder = config.mOrder;
	femConfig.mSimType = config.mSimType;
	femConfig.mNumSubsteps = config.mNumSubsteps;
	femConfig.mCustomConfig = config.mCustomConfig;
	femConfig.mMaterial = (MaterialModelType)config.mMaterial;
	femConfig.mOuterIterations = config.mOuterIterations;
	femConfig.mInnerIterations = mInnerIterationsValue;
	femConfig.mForceApplicationStep = 1.f / mConfig1.mNumForceSteps;
	femConfig.mHasCollisions = config.mHasCollisions;
	femConfig.mInvBulkModulus = config.mInvBulkModulus;
	femConfig.mContactStiffness = config.mContactStiffnes;
	femConfig.mAbsNewtonRsidualThreshold = config.mResidualThreshold;
	femConfig.mVerbose = mVerbose;

	for (uint32 i = 0; i < femSys->GetNumNodes(); i++)
	{
		femSys->GetNode(i).pos0 = femSys->GetNode(i).pos;
	}

	if (mLoadSkel)
	{
		// fix nodes
		mSkinInfos.resize(femSys->GetNumNodes());
		for (uint32 i = 0; i < femSys->GetNumNodes(); i++)
		{
			Node& node = femSys->GetNode(i);
			for (uint32 j = 0; j < mSkeleton.GetNumBones(); j++)
			{
				Skeleton::Bone* bone = mSkeleton.GetBoneById(j); // it's a coincidence here that the id is the same as the index

				Vector3 worldPos = 0.01f * bone->worldPos;

				if (bone->parent == nullptr)
					continue;

				Vector3 parentPos = 0.01f * bone->parent->worldPos;
				float t;
				Vector3 pos = DoubleToFloat(node.pos);
				ClosestPtSegm(pos, parentPos, worldPos, t);
				Vector3 pt = parentPos + t * (worldPos - parentPos);

				float len = (pos - pt).Length();
				if (len < 0.03f)
				{
					node.invMass = 0;
					mSkinInfos[i].mBoneIdx = bone->parent->id;
					Vector3 delta = pos - parentPos;
					mSkinInfos[i].mOffset = qRotate(bone->parent->worldRot.GetConjugate(), delta);
				}
			}
		}
	}

	femSys->Init(femConfig);

	if (config.mUseCables)
	{
		if (config.mUseCantilever || config.mModelIdx == MI_BOX_EX)
		{
			int ratio = config.mCantileverRatio;
			float width = config.mCantileverWidth;
			const float length = ratio * width;
			femSys->CreateCable({ mCableDivisions, length, Vector3R(0, 0, 0), Vector3R(1, 0, 0), mUseFreeCable, mCableStiffness });
			femSys->CreateCable({ mCableDivisions, length, Vector3R(0, 0, width), Vector3R(1, 0, 0), mUseFreeCable, mCableStiffness });
		}
		else if (config.mModelIdx == MI_HAMMER)
		{
			// TODO: need to fix back the hammer orientation
			femSys->CreateCable({ mCableDivisions, 10, Vector3R(0, -5, -2), Vector3R(0, 1, 0), mUseFreeCable, mCableStiffness });
			femSys->CreateCable({ mCableDivisions, 10, Vector3R(0, -5, 2), Vector3R(0, 1, 0), mUseFreeCable, mCableStiffness });
		}
		else if (config.mModelIdx == MI_WEDGE_FINGER)
		{
			femSys->CreateCable({ mCableDivisions, 10.5, Vector3R(-4.5, 1, 0), Vector3R(1, 0, 0), mUseFreeCable, mCableStiffness });
		}
		else if (!cables.empty())
		{
			for (const CableDescriptor& desc : cables)
			{
				femSys->CreateCable(desc, 1);
			}
		}
	}

	FemCollision* coll = femSys->GetFemPhysics()->GetCollision();
	if (femConfig.mHasCollisions && coll)
	{
		// add the ground collider
		Physics::Walls ground;
		float floorY = mFloorOrdinate;
		float extent = 10.5f;
		float hh = 0.5f;
		float offsetX = mFloorAbscisa;
		ground.center.y = floorY - hh;
		ground.center.x = offsetX;
		ground.mBox.min = Vector3(-extent, -hh, -extent);
		ground.mBox.max = Vector3(extent, hh, extent);
		coll->AddCollidable(std::make_shared<Physics::Walls>(ground));

#ifdef STAIRS
		ground.center.y = floorY - hh - .5f;
		ground.center.x = offsetX - 1;
		femSys->GetFemPhysics()->AddCollidable(std::make_shared<Physics::Walls>(ground));

		ground.center.y = floorY - hh - 1.f;
		ground.center.x = offsetX - 2;
		femSys->GetFemPhysics()->AddCollidable(std::make_shared<Physics::Walls>(ground));

		ground.center.y = floorY - hh - 1.5f;
		ground.center.x = offsetX - 3;
		femSys->GetFemPhysics()->AddCollidable(std::make_shared<Physics::Walls>(ground));

		ground.center.y = floorY - hh - 2.f;
		ground.center.x = offsetX - 4;
		femSys->GetFemPhysics()->AddCollidable(std::make_shared<Physics::Walls>(ground));
#endif

#ifdef CEILING
		// add the ceiling
		Physics::Walls ceiling;
		float ceilY = mCeilingOrdinate;
		ceiling.center.y = ceilY + hh;
		ceiling.mBox.min = Vector3(-extent, -hh, -extent);
		ceiling.mBox.max = Vector3(extent, hh, extent);
		femSys->GetFemPhysics()->AddCollidable(std::make_shared<Physics::Walls>(ceiling));
#endif
	}

	if (mDrawHighOrder)
		femSys->BuildBBBoundaryMesh();
	else
		femSys->BuildBoundaryMesh();

	if (!surfTris.empty())
	{
		femSys->GetFemPhysics()->SetBoundaryConditionsSurface(surfTris, config.mAppliedPressure); // pressure is in the order of KPa
	}
	else if (config.mApplyTractions)
	{
		ComputeBoundarySurface(femSys.get(), config);
	}


#ifdef USE_VISUAL_MESH
	if (!config.mUseCantilever && !mDrawHighOrder)
	{
		if (config.mModelIdx == MI_COW)
		{
			femSys->LoadVisualMesh("../Models/cow.obj", DoubleToFloat(model.mOffset + config.mOffset), model.mScale);
			mDrawVisualMesh = true;
		}
		if (config.mModelIdx == MI_BUNNY)
		{
			femSys->LoadVisualMesh("../Models/bunny.obj", DoubleToFloat(model.mOffset + config.mOffset), model.mScale);
			mDrawVisualMesh = true;
		}
		if (config.mModelIdx == MI_DRAGON)
		{
			femSys->LoadVisualMesh("../Models/dragon_new.obj", DoubleToFloat(model.mOffset + config.mOffset), model.mScale);
			mDrawVisualMesh = true;
		}
		if (config.mModelIdx == MI_HAMMER)
		{
			femSys->LoadVisualMesh("../Models/hammerbot_fine.obj", DoubleToFloat(model.mOffset + config.mOffset), model.mScale);
			mDrawVisualMesh = true;
		}
	}
#endif
}

void FEMDemo::ComputeBoundarySurface(FEM_SYSTEM::FemSystem* femSys, const Config& config)
{
	// build surface to apply traction on
	mInnerSurface.clear();
	auto testNode = [&](int idx)->bool
	{
		const Node& node = femSys->GetNode(idx);
		Vector3R pos = node.pos - config.mOffset * 0.01f;
		if (config.mModelIdx == 18)
		{
			float scale = mModels[config.mModelIdx].mScale;
			float size = 6 * scale;
			float height = 25 * scale;
			return abs(pos.x) < size && abs(pos.z) < size && pos.y > -height;
		}
		else if (config.mUseCantilever)
		{
			int ratio = config.mCantileverRatio;
			float width = config.mCantileverWidth;
			const float length = ratio * width * 0.01f; // in meters
			return abs(pos.x - length) < 0.01f;
		}
		if (config.mModelIdx == 26)
		{
			if (femSys->GetNode(idx).invMass > 0) {
				return true;
			}
		}
		else if (config.mModelIdx == MI_CUSHION) // Cushion
		{
			auto cornerCheckReturn = [&](real v0, real v1, real v2)->real
			{
				if (v0 > 0 && v1 > 0 && v2 > 0)
				{
					return sqrt(v0 * v0 + v1 * v1 + v2 * v2);
				}
				else
				{
					return std::max(v0, std::max(v1, v2));
				}
			};
			float scale = mModels[config.mModelIdx].mScale;
			float x = 40 * scale / 2;
			float y = 20 * scale / 2;
			float z = 40 * scale / 2;
			return cornerCheckReturn(abs(pos.x) - x, abs(pos.y) - y, abs(pos.z) - z) <= 0;
		}
		else
		{
			real r = sqrt(pos.x * pos.x + pos.z * pos.z);
			return r < 0.21f;
		}
		return false;
	};
	for (uint32 i = 0; i < femSys->GetNumNodes(); i++)
	{
		if (testNode(i) && femSys->GetNode(i).invMass > 0)
		{
			mInnerSurface.insert(i);
			if (config.mUseCantilever)
			{
				femSys->GetFemPhysics()->AddDirichletBC(i, FemPhysicsBase::AXIS_Y | FemPhysicsBase::AXIS_Z);
			}
		}
	}
	const Mesh& boundaryMesh = femSys->GetBoundaryMesh();

	auto testInnerNode = [&](int idx)->bool
	{
		return (mInnerSurface.find(idx) != mInnerSurface.end());
	};
	std::vector<uint32> innerTriangles;
	std::vector<uint32> innerElements;
	// go through all boundary triangles and find those with faces of interest
	for (uint32 i = 0; i < boundaryMesh.GetNumTriangles(); i++)
	{
		int base = i * 3;
		// vertex indices in the boundary mesh
		int idx1 = boundaryMesh.indices[base];
		int idx2 = boundaryMesh.indices[base + 1];
		int idx3 = boundaryMesh.indices[base + 2];
		// node indices in the tet mesh
		int i1 = femSys->GetNodeFromBoundaryIndex(idx1);
		int i2 = femSys->GetNodeFromBoundaryIndex(idx2);
		int i3 = femSys->GetNodeFromBoundaryIndex(idx3);
		// TODO: should work for part of the vertices too
		if (testInnerNode(i1) && testInnerNode(i2) && testInnerNode(i3))
		{
			Printf("%d %d %d\n", i1, i2, i3);
			innerTriangles.push_back(i1);
			innerTriangles.push_back(i2);
			innerTriangles.push_back(i3);
		}
	}

	femSys->GetFemPhysics()->SetBoundaryConditionsSurface(innerTriangles, config.mAppliedPressure); // pressure is in the order of KPa
}

void TetrahedralizeBox(FEM_SYSTEM::FemSystem* femSys, int idx[8])
{
	femSys->AddTetrahedron(idx[0], idx[1], idx[4], idx[3]);
	femSys->AddTetrahedron(idx[1], idx[5], idx[4], idx[6]);
	femSys->AddTetrahedron(idx[1], idx[4], idx[3], idx[6]);
	femSys->AddTetrahedron(idx[1], idx[3], idx[2], idx[6]);
	femSys->AddTetrahedron(idx[4], idx[3], idx[6], idx[7]);
}

void FEMDemo::InitTetGenCantilever(FEM_SYSTEM::FemSystem * femSys, const FEM_SYSTEM::Vector3R & offset, float l, float h, float w, uint32 hsegments, uint32 vsegments, uint32 dsegments)
{
	Printf(("----- InitTetGenCantilever(offset= (" + std::to_string(offset.x) + "," + std::to_string(offset.y) + "," + std::to_string(offset.z) + "),l= " + std::to_string(l) + ",h= " + std::to_string(h) + ",w= " + std::to_string(w) + ",hsegments= " + std::to_string(hsegments) + ", vsegments= " + std::to_string(vsegments) + ",dsegments= " + std::to_string(dsegments) + ")\n").c_str());

	ASSERT(l > 0);
	ASSERT(h > 0);
	ASSERT(w > 0);

	size_t next_node_idx = 0;
	size_t numberofpoints;
	real *pointlist;

	FEM_SYSTEM::Vector3R origin(0, 0, 0);

	numberofpoints = (hsegments + 1) * (vsegments + 1) * (dsegments + 1);
	pointlist = new real[numberofpoints * 3];

	for (uint32 m = 0; m < hsegments + 1; m++)
	{
		for (uint32 n = 0; n < vsegments + 1; n++)
		{
			for (uint32 o = 0; o < dsegments + 1; o++)
			{
				float x = float(origin.x) + l * (float(m) / float(hsegments));
				float y = float(origin.y) + h * (float(n) / float(vsegments));
				float z = float(origin.z) + w * (float(o) / float(dsegments));

				pointlist[next_node_idx * 3] = x;
				pointlist[next_node_idx * 3 + 1] = y;
				pointlist[(next_node_idx++) * 3 + 2] = z;
			}
		}
	}

	femSys->SetNumNodes(numberofpoints);
	//mForces.resize(numNodes);

	bool fixBothEnds = mConfig1.mDisplace;
	for (size_t i = 0; i < numberofpoints; i++)
	{
		femSys->GetNode(i).pos = FEM_SYSTEM::Vector3R(
			pointlist[i * 3],
			pointlist[i * 3 + 1],
			pointlist[i * 3 + 2]);
		if (femSys->GetNode(i).pos.x == 0 || (femSys->GetNode(i).pos.x == l && fixBothEnds)) // cantilever hack for boundary nodes
		{
			Printf("%d ", i);
			femSys->GetNode(i).invMass = 0;
		}
		femSys->GetNode(i).pos += offset; // offset is assumed to be in cm
		femSys->GetNode(i).pos.Scale(0.01f); // convert cm to m
	}

	int nexttetidx = 0;
	//femSys->ReserveMemoryForTetrahedrons(dsegments*vsegments*hsegments * 6);

	for (int k = 0; k < int(dsegments); k++)
	{
		for (int j = 0; j < int(vsegments); j++)
		{
			for (int i = 0; i < int(hsegments); i++)
			{
				int nextblock_rot = 0;
				if (j % 2 == 0)
				{
					nextblock_rot = 3 - (k % 2) - 2 * (i % 2);
				}
				else
				{
					nextblock_rot = 0 + (k % 2) + 2 * (i % 2);
				}
				if (nextblock_rot > 3)
					throw std::logic_error("nextblock_rot should never be above 3");

				size_t nodeidx0 = i * ((vsegments + 1) * (dsegments + 1)) + j * (dsegments + 1) + k;
				size_t nodeidx1 = nodeidx0 + 1;
				size_t nodeidx2 = nodeidx0 + (dsegments + 1);
				size_t nodeidx3 = nodeidx2 + 1;

				size_t nodeidx4 = nodeidx0 + ((vsegments + 1) * (dsegments + 1));
				size_t nodeidx5 = nodeidx1 + ((vsegments + 1) * (dsegments + 1));
				size_t nodeidx6 = nodeidx2 + ((vsegments + 1) * (dsegments + 1));
				size_t nodeidx7 = nodeidx3 + ((vsegments + 1) * (dsegments + 1));

				if (nextblock_rot == 0)
				{
					femSys->AddTetrahedron(nodeidx0, nodeidx2, nodeidx6, nodeidx7);
					femSys->AddTetrahedron(nodeidx0, nodeidx2, nodeidx3, nodeidx7);
					femSys->AddTetrahedron(nodeidx0, nodeidx1, nodeidx3, nodeidx7);
					femSys->AddTetrahedron(nodeidx0, nodeidx1, nodeidx5, nodeidx7);
					femSys->AddTetrahedron(nodeidx0, nodeidx5, nodeidx4, nodeidx7);
					femSys->AddTetrahedron(nodeidx0, nodeidx4, nodeidx6, nodeidx7);
				}
				else if (nextblock_rot == 1)
				{
					femSys->AddTetrahedron(nodeidx1, nodeidx0, nodeidx6, nodeidx4);
					femSys->AddTetrahedron(nodeidx1, nodeidx0, nodeidx2, nodeidx6);
					femSys->AddTetrahedron(nodeidx3, nodeidx1, nodeidx2, nodeidx6);
					femSys->AddTetrahedron(nodeidx3, nodeidx1, nodeidx7, nodeidx6);
					femSys->AddTetrahedron(nodeidx1, nodeidx7, nodeidx5, nodeidx6);
					femSys->AddTetrahedron(nodeidx1, nodeidx5, nodeidx4, nodeidx6);
				}
				else if (nextblock_rot == 2)
				{
					femSys->AddTetrahedron(nodeidx1, nodeidx3, nodeidx4, nodeidx5);
					femSys->AddTetrahedron(nodeidx1, nodeidx3, nodeidx0, nodeidx4);
					femSys->AddTetrahedron(nodeidx3, nodeidx2, nodeidx0, nodeidx4);
					femSys->AddTetrahedron(nodeidx3, nodeidx2, nodeidx4, nodeidx6);
					femSys->AddTetrahedron(nodeidx3, nodeidx6, nodeidx4, nodeidx7);
					femSys->AddTetrahedron(nodeidx3, nodeidx7, nodeidx4, nodeidx5);
				}
				else
				{
					femSys->AddTetrahedron(nodeidx3, nodeidx2, nodeidx5, nodeidx7);
					femSys->AddTetrahedron(nodeidx3, nodeidx2, nodeidx1, nodeidx5);
					femSys->AddTetrahedron(nodeidx2, nodeidx0, nodeidx1, nodeidx5);
					femSys->AddTetrahedron(nodeidx2, nodeidx0, nodeidx5, nodeidx4);
					femSys->AddTetrahedron(nodeidx2, nodeidx4, nodeidx5, nodeidx6);
					femSys->AddTetrahedron(nodeidx2, nodeidx6, nodeidx5, nodeidx7);
				}
			}
		}
	}
}

void FEMDemo::InitCantilever(FEM_SYSTEM::FemSystem* femSys, const FEM_SYSTEM::Vector3R& offset, float w, int segments, int sides)
{
	// set nodes
	int stride = sides * sides;

	// create nodes
	uint32 numNodes = segments * stride;
	femSys->SetNumNodes(numNodes);
	mForces.resize(numNodes);
	int node = 0;
	for (int i = 0; i < segments; i++)
	{
		float x = i * w;
		for (int k = 0; k < sides; k++)
		{
			for (int l = 0; l < sides; l++)
			{
				FEM_SYSTEM::Vector3R pos(x, k * w, -l * w);
				pos += offset;
				pos.Scale(0.01f); // convert cm to m
				femSys->GetNode(node).pos = pos;
				if (i == 0)
					femSys->GetNode(node).invMass = 0.f; // Dirichlet boundary conditions
				node++;
			}
		}
	}
	// set tets
	for (int i = 0; i < segments - 1; i++)
	{
		for (int k = 0; k < sides - 1; k++)
		{
			for (int l = 0; l < sides - 1; l++)
			{
				int j = i * stride + k * sides + l;
				int map[8] = { j, j + 1, j + sides + 1, j + sides,
					j + stride, j + stride + 1, j + stride + sides + 1, j + stride + sides };
				TetrahedralizeBox(femSys, map);
			}
		}
	}
}

void FEMDemo::InitTetrahedron(FEM_SYSTEM::FemSystem* femSys, const FEM_SYSTEM::Vector3R& offset)
{
	// set nodes
	float hw = 0.05f;
	femSys->SetNumNodes(4);
	femSys->GetNode(0).pos = FEM_SYSTEM::Vector3R(0, -hw, hw) + offset;
	femSys->GetNode(1).pos = FEM_SYSTEM::Vector3R(0, -hw, -hw) + offset;
	femSys->GetNode(2).pos = FEM_SYSTEM::Vector3R(0, hw, hw) + offset;
	femSys->GetNode(3).pos = FEM_SYSTEM::Vector3R(2 * hw, -hw, hw) + offset;
	// set tet (such that global and local node indices coincide)
	femSys->AddTetrahedron(0, 1, 2, 3);
	// initial conditions
	femSys->GetNode(0).invMass = 0.f;
	femSys->GetNode(1).invMass = 0.f;
	femSys->GetNode(2).invMass = 0.f;
}

void FEMDemo::PerformMeasurements(FEM_SYSTEM::FemSystem* femSys, bool createFile)
{
	std::string workingFolder = GetWorkingOutputFolderForMeasurements();

	std::fstream stream;
	std::string filepath;
	unsigned int mode = createFile ? std::fstream::out : std::fstream::app;

	// dt
	filepath = std::string(workingFolder) + "dt.txt";
	stream.open(filepath, mode);

	stream << std::to_string(mElapsedTime) + "\n";
	stream.close();

	if (mMeasureGlobalVolumeError)
	{
		real vol = femSys->GetFemPhysics()->GetTotalVolume() * 1e6f; // in cm^3
		Printf(("Global volume = " + std::to_string(vol) + "\n").c_str());

		filepath = std::string(workingFolder) + "gvolume.txt";
		stream.open(filepath, mode);
		stream << std::to_string(vol) + "\n";
		stream.close();
	}

	if (mMeasureModelXWidth)
	{
		real minv = 100000, maxv = -minv;

		for (size_t i = 0; i < femSys->GetNumNodes(); i++)
		{
			real v = femSys->GetNode(i).pos.z;
			if (v < minv)
				minv = v;
			if (v > maxv)
				maxv = v;
		}

		real width = maxv - minv;

		Printf(("WidthZ = " + std::to_string(width) + "\n").c_str());

		filepath = std::string(workingFolder) + "widthZ.txt";
		stream.open(filepath, mode);
		stream << std::to_string(width) + "\n";
		stream.close();
	}

	if (mMeasureModelXWidth)
	{
		real minv = 100000, maxv = -minv;

		for (size_t i = 0; i < femSys->GetNumNodes(); i++)
		{
			real v = femSys->GetNode(i).pos.x;
			if (v < minv)
				minv = v;
			if (v > maxv)
				maxv = v;
		}

		real width = maxv - minv;

		Printf(("WidthX = " + std::to_string(width) + "\n").c_str());

		filepath = std::string(workingFolder) + "widthX.txt";
		stream.open(filepath, mode);
		stream << std::to_string(width) + "\n";
		stream.close();
	}
	if (mMeasureModelXWidth)
	{
		real minv = 100000, maxv = -minv;

		for (size_t i = 0; i < femSys->GetNumNodes(); i++)
		{
			real v = femSys->GetNode(i).pos.y;
			if (v < minv)
				minv = v;
			if (v > maxv)
				maxv = v;
		}

		real width = maxv - minv;

		Printf(("WidthY = " + std::to_string(width) + "\n").c_str());

		filepath = std::string(workingFolder) + "widthY.txt";
		stream.open(filepath, mode);
		stream << std::to_string(width) + "\n";
		stream.close();
	}
	if (mMeasureModelRadius)
	{
		// calculate average XZ plane radius
		real radius = 0;
		for (size_t i = 0; i < femSys->GetNumNodes(); i++)
		{
			const Node& node = femSys->GetNode(i);
			real r = sqrt(node.pos.x * node.pos.x + node.pos.z * node.pos.z);
			radius += r;
		}
		radius *= 1.f / femSys->GetNumNodes();
		Printf("avg radius = %f\n", radius);

		filepath = std::string(workingFolder) + "radius.txt";
		stream.open(filepath, mode);
		stream << std::to_string(radius) + "\n";
		stream.close();
	}
	if (mMeasureCantileverDeflection)
	{
		if (createFile)
		{
			if (!mCheckCantilever && (mSelectedModel == ModelIds::MI_SPINE || mSelectedModel == ModelIds::MI_SPINE_BRIDGE))
			{
				mDeflectionPoint.push_back(409);
			}
			else
			{
				// we assume it is a general cantilever

				// Find the points that make up the curve for plotting
				for (size_t i = 0; i < femSys->GetNumNodes(); i++)
				{
					const Node& node = femSys->GetNode(i);

					if (std::abs(node.pos.y - mCantileverWidth * 0.01f) < 0.00001f && std::abs(node.pos.z - mCantileverWidth * 0.01f) < 0.00001f)
					{
						mDeflectionCurve.push_back(i);
					}

					if (std::abs(node.pos.x - mCantileverRatio * (mCantileverWidth*0.01f)) < 0.00001f && (node.pos.y == 0.0f || std::abs(node.pos.y - (mCantileverWidth*0.01f)) < 0.00001f) && std::abs(node.pos.z - mCantileverWidth * 0.01f) < 0.00001f)
					{
						mDeflectionPoint.push_back(i);
					}
				}
			}
		}

		filepath = std::string(workingFolder) + "deflectioncurve.txt";
		stream.open(filepath, mode);
		for (size_t i = 0; i < mDeflectionCurve.size(); i++)
		{
			Vector3R pos = femSys->GetNode(mDeflectionCurve[i]).pos;
			stream << std::to_string(pos.x) << " " << std::to_string(pos.y) << " " << std::to_string(pos.z) << " ";
		}
		stream << "\n";
		stream.close();

		filepath = std::string(workingFolder) + "deflectionpoint.txt";
		stream.open(filepath, mode);
		Vector3R avg_pos(0, 0, 0);
		for (size_t i = 0; i < mDeflectionPoint.size(); i++)
		{
			avg_pos += femSys->GetNode(mDeflectionPoint[i]).pos;
		}
		avg_pos = avg_pos * (1.0f / float(mDeflectionPoint.size()));
		stream << std::to_string(avg_pos.x) << " " << std::to_string(avg_pos.y) << " " << std::to_string(avg_pos.z) << "\n";
		stream.close();

	}

	if (mMeasureEnergy)
	{
		// level 0 -> elastic
		// level 1 -> gravitational
		// level 2 -> vol
		// level 3 -> kinetic
		real energy0 = femSys->GetFemPhysics()->ComputeEnergy(0);
		real energy1 = femSys->GetFemPhysics()->ComputeEnergy(1);
		real energy2 = femSys->GetFemPhysics()->ComputeEnergy(2);
		real energy3 = femSys->GetFemPhysics()->ComputeEnergy(3);

		filepath = std::string(workingFolder) + "energy0.txt";
		stream.open(filepath, mode);
		stream << std::to_string(energy0) + "\n";
		stream.close();

		filepath = std::string(workingFolder) + "energy1.txt";
		stream.open(filepath, mode);
		stream << std::to_string(energy1) + "\n";
		stream.close();

		filepath = std::string(workingFolder) + "energy2.txt";
		stream.open(filepath, mode);
		stream << std::to_string(energy2) + "\n";
		stream.close();

		filepath = std::string(workingFolder) + "energy3.txt";
		stream.open(filepath, mode);
		stream << std::to_string(energy3) + "\n";
		stream.close();
	}
}

void FEMDemo::UpdateSystem(FEM_SYSTEM::FemSystem* femSys, float dt, const Config* config)
{
	if (femSys == nullptr)
		return;

	// Ensure that the folder structure exists and that any material information is written if needed
	if (mPerformMeasurements && mUpdateCounter == 0)
	{
		PerformMeasurements(femSys, true);
	}

	if (mUpdateCounter == 0)
	{
		if (mUseAbcExport)
			femSys->ExportToAlembic();
	}


	if (mUseVTKHeatMapExport && mUpdateCounter < 12) {  // the frame count of 12 is arbitrary, should probably be controlled in UI.
		std::fstream vtkStream;

		std::string filepath = "outputFrame" + std::to_string(mUpdateCounter);
		vtkStream.open(filepath + ".vtk", std::fstream::out);
		femSys->ExportToVTKHeatMap(vtkStream);
		vtkStream.close();
	}

	int numForceSteps = mConfig1.mSimType == ST_STATIC ? 1 : mConfig1.mNumForceSteps;
	if (mConfig1.mDisplace && mForceStep < numForceSteps)
	{
		mForceStep++;
		real disp = mCheckRotate ? PI : mConfig1.mDisplacementWidth;
		Displace(mFemSys.get(), disp / numForceSteps, mCheckRotate);
	}

#ifdef CEILING
	Vector3& center = femSys->GetFemPhysics()->GetCollidable(1)->center;
	if (center.y < mFloorOrdinate + 0.4f)
	{
		mCeilingState = 1;
		//center.y = 1;
	}
	if (mCeilingState == 0)
		center.y -= 0.001f;
	//else
	//	center.y += 0.001f;
#endif

	if (config && config->mUseCables)
	{
		uint32 numCables = femSys->GetFemPhysics()->GetNumCables();
		if (numCables > 0)
			femSys->GetFemPhysics()->SetCableActuation(0, mActuation);
		if (numCables > 1)
			femSys->GetFemPhysics()->SetCableActuation(1, mActuation2);
	}
	femSys->GetFemPhysics()->Step(dt);
	mUpdateCounter++;
	if (mConfig1.mSimType == SimulationType::ST_STATIC)
	{
		mElapsedTime = 1.0f;
	}
	else if (mConfig1.mSimType == SimulationType::ST_QUASI_STATIC)
	{
		// normalize the 'elapsed' time for quasi static, such that the final step is always at 1.0f
		// this is useful for plotting.
		mElapsedTime = float(mUpdateCounter) / mConfig1.mNumForceSteps;
	}
	else
	{
		mElapsedTime += dt;
	}

	bool isStep = true;
	if (mConfig1.mSimType == SimulationType::ST_STATIC && mUpdateCounter > 1)
	{
		isStep = false;
	}
	else if (mConfig1.mSimType == SimulationType::ST_QUASI_STATIC && mUpdateCounter > mConfig1.mNumForceSteps)
	{
		isStep = false;
	}

	if (isStep && mUseAbcExport)
		femSys->ExportToAlembic();

	if (mPerformMeasurements && isStep)
	{
		PerformMeasurements(femSys);
	}
}

void CreateFolderIfItDoesNotExist(const char *path)
{
	CreateDirectoryA(path, NULL);
}

std::string FEMDemo::GetWorkingOutputFolderForMeasurements()
{
	CreateFolderIfItDoesNotExist(mMeasurementsRootOutputPath);
	std::string testCasePath = std::string(mMeasurementsRootOutputPath) + "/" + std::string(mTestCaseName) + "/";
	CreateFolderIfItDoesNotExist(testCasePath.c_str());

	std::stringstream outercase;
	std::stringstream innercase;
	std::stringstream *tmpcase;

	if (OuterInnerType(mIOResolution) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOResolution) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mFemSys->GetNumTets() << mOutputDelimiter;
	}

	if (OuterInnerType(mIOYoungModulus) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOYoungModulus) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mYoungsModulusValue << mOutputDelimiter;
	}

	if (OuterInnerType(mIOPoissonRatio) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOPoissonRatio) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mPoissonRatioValue << mOutputDelimiter;
	}

	if (OuterInnerType(mIOSimulationType) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOSimulationType) == OUTER ? &outercase : &innercase;
		(*tmpcase) << ToString(FEM_SYSTEM::SimulationType(mSelectedSimType)) << mOutputDelimiter;
	}

	/*if (mSelectedSimType == ST_STATIC)
	{
		outercase << 1;
	}
	else *//*if (mSelectedSimType == ST_QUASI_STATIC)
	{*/
	if (OuterInnerType(mIOForcesteps) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOForcesteps) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mForceStepsValue;
	}
	//}
	//else if (mSelectedSimType == ST_EXPLICIT || mSelectedSimType == ST_IMPLICIT)
	//{
	if (OuterInnerType(mIOSubsteps) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOSubsteps) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mSubstepsValue;
	}
	//}


	if (OuterInnerType(mIOMethodType) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOMethodType) == OUTER ? &outercase : &innercase;
		(*tmpcase) << ToString(FEM_SYSTEM::MethodType(mSelectedSimulator)) << mOutputDelimiter;
	}

	//if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY ||
	//	mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
	//{
	if (OuterInnerType(mIOOuterIterations) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOOuterIterations) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mOuterIterationsValue << mOutputDelimiter;
	}
	//}

	//if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY)
	//{
	const char* solvers[] = { "Newton", "Newton+line search", "Newton CG", "Nonlinear CG", "Gradient descent", "Steepest descent" };

	if (OuterInnerType(mIOSolver) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOSolver) == OUTER ? &outercase : &innercase;
		(*tmpcase) << solvers[mNonlinearSolverValue] << mOutputDelimiter;
	}

	//if (mNonlinearSolverValue == FemPhysicsMatrixFree::NST_GRADIENT_DESCENT)
	//{
	if (OuterInnerType(mIODescentValue) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIODescentValue) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mDescentRateValue << mOutputDelimiter;
	}
	//}
	//if (mNonlinearSolverValue == FemPhysicsMatrixFree::NST_NEWTON_CG ||
	//	mNonlinearSolverValue == FemPhysicsMatrixFree::NST_NONLINEAR_CG)
	//{
	if (OuterInnerType(mIOInnerIterations) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOInnerIterations) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mInnerIterationsValue << mOutputDelimiter;
	}
	//	}
	//}

	if (OuterInnerType(mIOMaterialModelType) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOMaterialModelType) == OUTER ? &outercase : &innercase;

		if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY ||
			mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
		{
			(*tmpcase) << ToString(FEM_SYSTEM::MaterialModelType(mMaterialModel)) << mOutputDelimiter;
		}

		else if (mSelectedSimulator == MT_LINEAR_ELASTICITY || mSelectedSimulator == MT_INCOMPRESSIBLE_LINEAR_ELASTICITY)
		{
			(*tmpcase) << ToString(FEM_SYSTEM::MaterialModelType::MMT_LINEAR) << mOutputDelimiter;
		}
		else
		{
			(*tmpcase) << ToString(FEM_SYSTEM::MaterialModelType::MMT_COROTATIONAL) << mOutputDelimiter;
		}
	}

	//if (mSelectedSimulator == MT_INCOMPRESSIBLE_LINEAR_ELASTICITY ||
	//	mSelectedSimulator == MT_INCOMPRESSIBLE_COROTATIONAL_ELASTICITY ||
	//	mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
	//{
	if (OuterInnerType(mIOPressureOrder) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIOPressureOrder) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mPressureOrderValue << mOutputDelimiter;
	}
	//}

	if (OuterInnerType(mIODisplacementWidth) != NOT_INCLUDED)
	{
		tmpcase = OuterInnerType(mIODisplacementWidth) == OUTER ? &outercase : &innercase;
		(*tmpcase) << mDisplacementWidth << mOutputDelimiter;
	}

	outercase << "/";
	std::string outercasepath = testCasePath + outercase.str();
	CreateFolderIfItDoesNotExist(outercasepath.c_str());


	innercase << "/";
	std::string innercasepath = outercasepath + innercase.str();
	CreateFolderIfItDoesNotExist(innercasepath.c_str());

	return innercasepath;
}

void FEMDemo::Update(float dt)
{
	// update skeleton
	if (mLoadSkel)
	{
		mSkeleton.UpdateAnimation(dt);
		mSkeleton.ComputeWorldTransforms();

		// "skin" attached nodes
		for (uint32 i = 0; i < mSkinInfos.size(); i++)
		{
			if (mSkinInfos[i].mBoneIdx >= 0)
			{
				int idx = mSkinInfos[i].mBoneIdx;
				Vector3 pos = 0.01f * mSkeleton.GetBoneById(idx)->worldPos + qRotate(mSkeleton.GetBoneById(idx)->worldRot, mSkinInfos[i].mOffset);
				mFemSys->GetFemPhysics()->SetDeformedPosition(i, FloatToDouble(pos));
			}
		}
	}

	UpdateSystem(mFemSys.get(), dt, &mConfig1);

	UpdateSystem(mFemSys2.get(), dt, &mConfig2);

	UpdateSystem(mFemSys3.get(), dt);
	UpdateSystem(mFemSys4.get(), dt);
	UpdateSystem(mFemSys5.get(), dt);
	UpdateSystem(mFemSys6.get(), dt);
}

void FEMDemo::DrawSystem(Graphics3D* graphics3D, FEM_SYSTEM::FemSystem* femSys)
{
	if (!femSys)
		return;

	if (mDrawHighOrder)
		femSys->UpdateBBBoundaryMesh();
	else
		femSys->UpdateBoundaryMesh(mDrawHeatMap ? FemSystem::HMT_PRESSURE : FemSystem::HMT_NONE);

	Matrix4 model = Matrix4::Scale(100, 100, 100);

	if (mDrawBoundaryMesh)
	{
		// draw surface mesh
		if (mDrawHighOrder)
		{
			const Mesh& hoBoundaryMesh = femSys->GetBBBoundaryMesh();
			graphics3D->DrawMesh(hoBoundaryMesh.vertices, hoBoundaryMesh.normals, hoBoundaryMesh.indices);
		}
		else
		{
			const Mesh& boundaryMesh = femSys->GetBoundaryMesh();

			if (mDrawHeatMap) {
				int flags = ShaderFlags::VERTEX_COLORS;
				int flagsTemp = graphics3D->GetRenderFlags();
				graphics3D->SetFlags(flags);
				graphics3D->DrawMesh(boundaryMesh.vertices, boundaryMesh.normals, boundaryMesh.colors, boundaryMesh.indices, 100);
				graphics3D->SetFlags(flagsTemp);
			}
			else {
				graphics3D->DrawMesh(boundaryMesh.vertices, boundaryMesh.normals, boundaryMesh.indices, &model);
			}
		}
	}

	if (mDrawVisualMesh)
	{
		femSys->UpdateVisualMesh();
		graphics3D->SetColor(0, 0, 1);
		const Mesh& visualMesh = femSys->GetVisualMesh();
		graphics3D->DrawMesh(visualMesh.vertices, visualMesh.normals, visualMesh.indices, &model);
	}

	if (mDrawTets)
	{
		// draw tetrahedral mesh (optional)
		if (mDrawHighOrder)
			femSys->GetFemSystemDrawing()->DrawBBTetrahedra(graphics3D);
		else
			femSys->GetFemSystemDrawing()->DrawTetrahedra(graphics3D);
	}

	// draw nodes
	if (mDrawNodes)
	{

		for (uint32 i = 0; i < femSys->GetFemPhysics()->GetNumNodes(); i++)
		{
			FEM_SYSTEM::Vector3R pos = ((const FemPhysicsBase*)femSys->GetFemPhysics())->GetDeformedPosition(i);
			pos.Scale(100); // convert m to cm
			bool fixed = femSys->GetFemPhysics()->IsNodeFixed(i);
			bool inner = mInnerSurface.find(i) != mInnerSurface.end();
			float color = fixed ? 0.f : 1.f;
			float radius = 0.1f;

			if (inner)
				graphics3D->SetColor(0, 1, 0);
			else
				graphics3D->SetColor(color, color, color);

			// TODO: use scalar field computed in FemSystem for heat map drawing per node
			graphics3D->DrawSphere(DoubleToFloat(pos), radius);
		}
	}
}

void FEMDemo::DrawUI()
{
#ifdef USE_IMGUI
	float indentLevel = 16.0f;

	if (ImGui::CollapsingHeader("Drawing"))
	{
		ImGui::Checkbox("Draw tets", &mDrawTets);
		ImGui::Checkbox("Draw mesh", &mDrawBoundaryMesh);
		ImGui::Checkbox("Draw visual mesh", &mDrawVisualMesh);
		ImGui::Checkbox("Draw nodes", &mDrawNodes);
		ImGui::Checkbox("Draw heat map", &mDrawHeatMap);

		ImGui::InputFloat3("Draw Color:", mDrawColor);
	}

	if (ImGui::CollapsingHeader("FEM"))
	{
		// floating precision
#ifdef USE_DOUBLE_FOR_FEM
		ImGui::Text("Floating point precision: double");
#else
		ImGui::Text("Floating point precision: single");
#endif

		if (ImGui::Combo("Config Presets", &mChosenPreset, mPresetConfigNames, mNumberOfPresetConfigs))
		{
			if (mChosenPreset > 0) {
				Config preset = mPresetConfigs[mChosenPreset];

				ApplyConfigPreset(preset);
			}
		}

		bool hasUIChanged = false;

		// use cantilever
		hasUIChanged |= ImGui::Checkbox("Use cantilever", &mCheckCantilever);

		// model
		if (!mCheckCantilever)
		{
			const char* models[numModels];
			for (int i = 0; i < numModels; i++)
			{
				models[i] = mModels[i].mName + 10; // hack to skip the "../Models/" prefix
			}
			hasUIChanged |= ImGui::Combo("Model", &mSelectedModel, models, IM_ARRAYSIZE(models));
		}
		else {
			ImGui::Indent(indentLevel);
			hasUIChanged |= ImGui::InputInt("Resolution", &mModelResolution, 1, 10);
			hasUIChanged |= ImGui::InputFloat("Width", &mCantileverWidth, 1, 10);
			hasUIChanged |= ImGui::InputInt("Ratio", &mCantileverRatio, 1, 10);
			ImGui::Indent(-indentLevel);
		}
		hasUIChanged |= ImGui::InputInt("Mesh Order", &mOrder, 1, 10);

		ImGui::Text("Tetrahedra: %i", mFemSys->GetNumTets());
		ImGui::SameLine();
		ImGui::Text("Nodes: %i", mFemSys->GetNumNodes());
		ImGui::SameLine();
		ImGui::Text("Free Nodes: %i", mFemSys->GetFemPhysics()->GetNumFreeNodes());
		ImGui::SameLine();
		ImGui::Text("Fixed Nodes: %i", mFemSys->GetNumNodes() - mFemSys->GetFemPhysics()->GetNumFreeNodes());

		// method
		const char* items[] = { "Linear elasticity", "Corotational elasticity", "Incompressible linear elasticity",
			"Incompressible corotational elasticity", "Nonlinear elasticity", "Incompressible nonlinear elasticity" };
		hasUIChanged |= ImGui::Combo("FEM simulator", &mSelectedSimulator, items, IM_ARRAYSIZE(items));

		if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY ||
			mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
		{
			hasUIChanged |= ImGui::InputInt("Outer iterations", &mOuterIterationsValue, 1, 10);
			hasUIChanged |= ImGui::InputFloat("Residual threshold", &mResidualThresholdValue, 1e-5f, 1e-4f, "%.10f");

			const char* solvers[] = { "Newton", "Newton+line search", "Newton CG", "Nonlinear CG", "Gradient descent", "Steepest descent", "NLopt",
				"Mixed Newton", "Mixed dual ascent"};
			hasUIChanged |= ImGui::Combo("Solver", &mNonlinearSolverValue, solvers, IM_ARRAYSIZE(solvers));

			if (mNonlinearSolverValue == NST_GRADIENT_DESCENT)
			{
				hasUIChanged |= ImGui::InputFloat("Descent rate", &mDescentRateValue, 1e-5f, 1e-4f, "%.6f");
			}
			if (mNonlinearSolverValue == NST_NEWTON_CG ||
				mNonlinearSolverValue == NST_NONLINEAR_CG)
			{
				hasUIChanged |= ImGui::InputInt("Inner iterations", &mInnerIterationsValue, 1, 10);
			}

			// TODO: use ToString
			const char* models[] = { "Linear", "StVK", "Corotational", "Neo-Hookean", "Neo-Hookean (Ogden)", "Distortional linear",
				"Distortional Mooney", "Distortional Mooney (nu=0)", "Distortional Bonet-Wood (J=ct)", "Distortional Bonet-Wood",
				"Distortional Ogden", "Distortional Smith", "Distortional Neo-Hookean" };
			hasUIChanged |= ImGui::Combo("Material", &mMaterialModel, models, IM_ARRAYSIZE(models));
		}

		if (mSelectedSimulator == MT_INCOMPRESSIBLE_LINEAR_ELASTICITY ||
			mSelectedSimulator == MT_INCOMPRESSIBLE_COROTATIONAL_ELASTICITY ||
			mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
		{
			hasUIChanged |= ImGui::InputInt("Pressure order", &mPressureOrderValue);
		}

		// simulation type
		const char* simTypes[] = { "Static", "Quasi-static", "Explicit", "Implicit" };
		hasUIChanged |= ImGui::Combo("Simulation type", &mSelectedSimType, simTypes, IM_ARRAYSIZE(simTypes));

		// elastic params
		hasUIChanged |= ImGui::InputFloat("Young's modulus (KPa)", &mYoungsModulusValue, 1, 10, 0);		
		hasUIChanged |= ImGui::InputDouble("Poisson ratio", &mPoissonRatioValue, 0.01f, 0.1f, "%.11f");
		if (mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
		{
			hasUIChanged |= ImGui::InputFloat("Inverse bulk modulus", &mInvBulkModulusValue, 0.01f, 0.1f, "%.4f");
			hasUIChanged |= ImGui::InputFloat("Constraint scale", &mConstraintScaleValue, 10, 100, "%.1f");
			ImGui::Checkbox("Log constraint", &mLogConstrValue);
		}

		if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY)
		{
			ImGui::Checkbox("Optimizer", &mOptimizer);
		}

		// gravity
		hasUIChanged |= ImGui::InputFloat("Gravity", &mGravityValue, 0.1f, 1.f, "%.2f");

		// substeps
		if (mSelectedSimType == ST_QUASI_STATIC)
		{
			hasUIChanged |= ImGui::InputInt("Force steps", &mForceStepsValue, 1, 10);
		}
		else if (mSelectedSimType == ST_EXPLICIT || mSelectedSimType == ST_IMPLICIT)
		{
			hasUIChanged |= ImGui::InputInt("#substeps", &mSubstepsValue, 1, 10);
			ImGui::Text("Timestep: %f ms", 16.f / mSubstepsValue);
		}

		ImGui::Checkbox("Verbose", &mVerbose);

		// apply pressure
		hasUIChanged |= ImGui::Checkbox("Apply pressure", &mCheckApplyPressure);

		// pressure
		if (mConfig1.mApplyTractions)
		{
			hasUIChanged |= ImGui::InputFloat("Min pressure", &mMinPressure, 100, 1000, 0);
			hasUIChanged |= ImGui::InputFloat("Max pressure", &mMaxPressure, 100, 1000, 0);
			hasUIChanged |= ImGui::SliderFloat("Pressure", &mPressureValue, mMinPressure, mMaxPressure);
		}

		ImGui::Checkbox("Apply displacement", &mCheckDisplace);
		ImGui::Checkbox("Apply rotation", &mCheckRotate);
		ImGui::Checkbox("Handle collisions", &mCheckCollisions);
		if (mCheckCollisions)
		{
			hasUIChanged |= ImGui::InputFloat("Contact stiffness", &mContactStiffnessValue, 10, 100, "%.1f");
			hasUIChanged |= ImGui::InputFloat("Floor ordinate", &mFloorOrdinate, 0.1f, 1, "%.2f");
			hasUIChanged |= ImGui::InputFloat("Floor abscisa", &mFloorAbscisa, 0.1f, 1, "%.2f");
			hasUIChanged |= ImGui::InputFloat("Ceiling ordinate", &mCeilingOrdinate, 0.1f, 1, "%.2f");
		}

		if (mCheckDisplace)
		{
			ImGui::Indent(indentLevel);
			hasUIChanged |= ImGui::InputFloat("DisplacementWidth", &mDisplacementWidth, 1, 5);
			ImGui::Indent(-indentLevel);
		}

		if (hasUIChanged)
		{
			// Set it back to 'Custom' if any values have been changed
			mChosenPreset = 0;
		}

		ImGui::Checkbox("Use cables", &mUseCables);
		if (mUseCables)
		{
			ImGui::Checkbox("Use free cable", &mUseFreeCable);
			ImGui::InputFloat("Cable stiffness", &mCableStiffness, 1, 5);
			ImGui::InputFloat("Cable damping", &mCableDamping, 1, 5);
			ImGui::InputInt("Cable divisions", &mCableDivisions);
			ImGui::SliderFloat("Cable actuation 1", &mActuation, 0.001f, 1.5f);
			ImGui::SliderFloat("Cable actuation 2", &mActuation2, 0.001f, 1.5f);
		}
	}

	if (ImGui::CollapsingHeader("Testing"))
	{
		ImGui::Text("You must provide with a Test Case Name to write measurements");
		ImGui::InputText("Test Case Name", mTestCaseName, 25);

		if (mTestCaseName[0] == 0)
		{
			mPerformMeasurements = false;
		}
		ImGui::Checkbox("Write Measurments", &mPerformMeasurements);
		ImGui::Text("Output path: ./make/out/x64/%s/%s/", mMeasurementsRootOutputPath, mTestCaseName);

		const char* outerinnerTypes[] = { "Figure", "Plot", "Do not consider" };
		ImGui::Combo(("Resolution:" + std::to_string(mFemSys->GetNumTets())).c_str(), &mIOResolution, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Combo(("YoungModulus:" + std::to_string(mYoungsModulusValue)).c_str(), &mIOYoungModulus, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Combo(("PoissonRatio:" + std::to_string(mPoissonRatioValue)).c_str(), &mIOPoissonRatio, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		std::string labeltmp = "SimulationType:"; labeltmp.append(ToString(FEM_SYSTEM::SimulationType(mSelectedSimType)));
		ImGui::Combo(labeltmp.c_str(), &mIOSimulationType, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));

		//if (mSelectedSimType == ST_QUASI_STATIC)
		//{
		ImGui::Indent(indentLevel);
		ImGui::Combo(("ForceSteps:" + std::to_string(mForceStepsValue)).c_str(), &mIOForcesteps, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Indent(-indentLevel);
		//}
		//else if (mSelectedSimType == ST_EXPLICIT || mSelectedSimType == ST_IMPLICIT)
		//{
		ImGui::Indent(indentLevel);
		ImGui::Combo(("SubSteps:" + std::to_string(mSubstepsValue)).c_str(), &mIOSubsteps, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Indent(-indentLevel);
		//}

		labeltmp = "MethodType:"; labeltmp.append(ToString(FEM_SYSTEM::MethodType(mSelectedSimulator)));
		ImGui::Combo(labeltmp.c_str(), &mIOMethodType, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));

		//if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY ||
		//	mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
		//{
		ImGui::Indent(indentLevel);
		ImGui::Combo(("OuterIterations:" + std::to_string(mOuterIterationsValue)).c_str(), &mIOOuterIterations, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Indent(-indentLevel);
		//}

		//if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY)
		//{
		ImGui::Indent(indentLevel);
		const char* solvers[] = { "Newton", "Newton+line search", "Newton CG", "Nonlinear CG", "Gradient descent", "Steepest descent" };
		labeltmp = "Solver:"; labeltmp.append(solvers[mNonlinearSolverValue]);
		ImGui::Combo(labeltmp.c_str(), &mIOSolver, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));

		//if (mNonlinearSolverValue == FemPhysicsMatrixFree::NST_GRADIENT_DESCENT)
		//{
		ImGui::Indent(indentLevel);
		ImGui::Combo(("DescentValue:" + std::to_string(mDescentRateValue)).c_str(), &mIODescentValue, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Indent(-indentLevel);
		//}
		//if (mNonlinearSolverValue == FemPhysicsMatrixFree::NST_NEWTON_CG ||
		//	mNonlinearSolverValue == FemPhysicsMatrixFree::NST_NONLINEAR_CG)
		//{
		ImGui::Indent(indentLevel);
		ImGui::Combo(("InnerIterations:" + std::to_string(mInnerIterationsValue)).c_str(), &mIOInnerIterations, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Indent(-indentLevel);
		//}
		ImGui::Indent(-indentLevel);
		//}

		//if (mSelectedSimulator == MT_NONLINEAR_ELASTICITY ||
		//	mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
		//{
		ImGui::Indent(indentLevel);
		labeltmp = "MaterialModelType:"; labeltmp.append(ToString(FEM_SYSTEM::MaterialModelType(mMaterialModel)));
		ImGui::Combo(labeltmp.c_str(), &mIOMaterialModelType, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Indent(-indentLevel);
		//}

		//if (mSelectedSimulator == MT_INCOMPRESSIBLE_LINEAR_ELASTICITY ||
		//	mSelectedSimulator == MT_INCOMPRESSIBLE_COROTATIONAL_ELASTICITY ||
		//	mSelectedSimulator == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
		//{
		ImGui::Indent(indentLevel);
		ImGui::Combo(("PressureOrder:" + std::to_string(mPressureOrderValue)).c_str(), &mIOPressureOrder, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Indent(-indentLevel);
		//}

		//if (mCheckDisplace)
		//{
		ImGui::Indent(indentLevel);
		ImGui::Combo(("DisplacementWidth:" + std::to_string(mDisplacementWidth)).c_str(), &mIODisplacementWidth, outerinnerTypes, IM_ARRAYSIZE(outerinnerTypes));
		ImGui::Indent(-indentLevel);
		//}

		ImGui::Text("");

		ImGui::Indent(indentLevel);

		if (!mPerformMeasurements)
		{
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}

		ImGui::Checkbox("Measure global vol error", &mMeasureGlobalVolumeError);
		ImGui::Checkbox("Measure x-width", &mMeasureModelXWidth);
		ImGui::Checkbox("Measure deflection", &mMeasureCantileverDeflection);
		ImGui::Checkbox("Measure Energy", &mMeasureEnergy);

		if (!mPerformMeasurements)
		{
			ImGui::PopStyleVar();
		}

		ImGui::Indent(-indentLevel);
	}


	if (ImGui::CollapsingHeader("Export"))
	{
		ImGui::Text("This tab allows you to export the current mesh in its current deformed state.");
		ImGui::Text("The mesh will be exported as a .ele and .node pair (TetGen).");

		ImGui::InputText("Output Folder", mExportOutputFolder, 50);
		ImGui::InputText("Output filename", mExportFileName, 25);

		ImGui::Checkbox("Export as ELE/NODE", &mExportAsELENODE);
		ImGui::Checkbox("Export as vtk", &mExportAsVTK);


		if (mExportFileName[0] != 0 && ImGui::Button("Export Mesh"))
		{
			CreateFolderIfItDoesNotExist(mExportOutputFolder);

			std::string filepath = mExportOutputFolder;
			if (filepath.back() != '/')
			{
				filepath += +'/';
			}
			filepath += mExportFileName;

			if (mExportAsELENODE)
			{
				std::fstream eleStream, nodeStream;

				eleStream.open(filepath + ".ele", std::fstream::out);
				nodeStream.open(filepath + ".node", std::fstream::out);

				mFemSys->ExportToNodeEleFile(nodeStream, eleStream);

				eleStream.close();
				nodeStream.close();
			}

			if (mExportAsVTK)
			{
				std::fstream vtkStream;
				vtkStream.open(filepath + ".vtk", std::fstream::out);
				mFemSys->ExportToVTKFile(vtkStream);
				vtkStream.close();
			}
		}
	}
#endif // USE_IMGUI
}

void FEMDemo::ApplyConfigPreset(Config preset)
{
	//mConfigName.clear();

	//const char* p = &preset.mName[0];
	//while (*p != '\0')
	//{
	//	mConfigName.push_back(*p);
	//	++p;
	//}

	mYoungsModulusValue = preset.mYoungsModulus;
	mPoissonRatioValue = preset.mPoissonRatio;
	mSelectedSimulator = preset.mType;
	mSelectedModel = preset.mModelIdx;
	mPressureValue = preset.mAppliedPressure;
	mSelectedSimType = preset.mSimType;
	mGravityValue = preset.mGravity;
	mCheckCantilever = preset.mUseCantilever;
	mSubstepsValue = preset.mNumSubsteps;
	mCheckApplyPressure = preset.mApplyTractions;
	mMaterialModel = preset.mMaterial;
	//mNonlinearSolverValue = 0;
	mOuterIterationsValue = preset.mOuterIterations;
	//mInnerIterationsValue = 100;
	//mDescentRateValue = 1e-5f;
	//mPressureOrderValue = 1;
	mCheckDisplace = preset.mDisplace;
	mDisplacementWidth = preset.mDisplacementWidth;
	mForceStepsValue = preset.mNumForceSteps;
	mOrder = preset.mOrder;
	mModelResolution = preset.mResolution;
	mCantileverWidth = preset.mCantileverWidth;
	mCantileverRatio = preset.mCantileverRatio;
	mConstraintScaleValue = preset.mConstraintScale;
	mCheckCollisions = preset.mHasCollisions;
}

void FEMDemo::Draw(Graphics3D* graphics3D)
{
#ifdef USE_IMGUI
	DrawUI();
#endif

	graphics3D->SetColor(mDrawColor[0], mDrawColor[1], mDrawColor[2]);
	DrawSystem(graphics3D, mFemSys.get());
	graphics3D->SetColor(0.f, 1.f, 0.f);
	DrawSystem(graphics3D, mFemSys2.get());
	graphics3D->SetColor(0.f, 0.f, 1.f);
	DrawSystem(graphics3D, mFemSys3.get());
	graphics3D->SetColor(1.f, 1.f, 0.f);
	DrawSystem(graphics3D, mFemSys4.get());
	graphics3D->SetColor(1.f, 0.f, 1.f);
	DrawSystem(graphics3D, mFemSys5.get());
	graphics3D->SetColor(0.5f, 0.25f, 1.f);
	DrawSystem(graphics3D, mFemSys6.get());

	// Mihai: please use this check to draw the collision mesh and contact points
	FemCollision* coll = mFemSys->GetFemPhysics()->GetCollision();
	if (mCheckCollisions && coll)
	{
		// draw collidables
		for (uint32 i = 0; i < coll->GetNumCollidables(); i++)
		{
			const Physics::Collidable* collidable = coll->GetCollidable(i);
			if (collidable->mType == Physics::CT_WALLS)
			{
				graphics3D->SetColor(0.82f, 0.94f, 0.8f);
				const Physics::Walls* walls = (const Physics::Walls*)collidable;
				Vector3 c = 100.f * (walls->mBox.GetCenter() + walls->center);
				Vector3 e = 100.f * (walls->mBox.GetExtent());
				graphics3D->DrawCube(c, e, Matrix3());
			}
		}

		//// draw sphere
		//graphics3D->SetColor(1, 1, 1);
		//graphics3D->DrawSphere(Vector3(20, -10, 0), 10);

		// draw mesh
		//graphics3D->SetColor(0.89f, 0.855f, 0.788f); //Bone color    <<-----
		//graphics3D->DrawMesh(mCollisionMesh.vertices, mCollisionMesh.normals, mCollisionMesh.indices);

#ifdef DRAW_CONTACTS
		// draw contacts	
		for (uint32 i = 0; i < mFemSys->GetFemPhysics()->GetNumCollisionPoints(); i++)
		{
			Vector3 p2, n;
			uint32 idx;
			mFemSys->GetFemPhysics()->GetCollisionPoint(i, idx, p2, n);
			//p2.Scale(0.1f);
			p2.Scale(100.f);
			Vector3 p1 = 100.f * DoubleToFloat(mFemSys->GetFemPhysics()->GetDeformedPosition(idx));
			graphics3D->SetColor(0, 1, 0);
			graphics3D->DrawSphere(p2, 0.1f);
			graphics3D->DrawLine(p2, p2 + 10.f * n);
			graphics3D->SetColor(0, 0, 1);
			graphics3D->DrawSphere(p1, 0.1f);

			//graphics3D->DrawLine(p1, p2);
		}
#endif

		/*for (uint32 i = 0; i < mFemSys->GetFemPhysics()->GetNumCollisionPoints2(); i++)
		{
			Vector3 rigid_point, bary, n;
			Vector3T<int> nodeTriple;
			mFemSys->GetFemPhysics()->GetCollisionPoint2(i, rigid_point, nodeTriple, bary, n);
			rigid_point.Scale(0.1f);
			//p2.Scale(100.f);

			Vector3 p0 = 100.f * DoubleToFloat(mFemSys->GetFemPhysics()->GetDeformedPosition(nodeTriple.x));
			Vector3 p1 = 100.f * DoubleToFloat(mFemSys->GetFemPhysics()->GetDeformedPosition(nodeTriple.y));
			Vector3 p2 = 100.f * DoubleToFloat(mFemSys->GetFemPhysics()->GetDeformedPosition(nodeTriple.z));

			Vector3 pt = bary.x * p0 + bary.y * p1 + bary.z * p2;

			//btVector3 pos = psbSphere->m_nodes[idx].m_x;
			//Vector3 p1 = 100.f * DoubleToFloat(mFemSys->GetNode(idx).pos);
			float radius = 0.1f;
			graphics3D->SetColor(0, 1, 0);
			graphics3D->DrawSphere(pt, radius);
			//graphics3D->DrawLine(pt, pt + 10.f * n);
			graphics3D->SetColor(0, 0, 1);
			graphics3D->DrawSphere(rigid_point, radius);

			//graphics3D->DrawLine(pt, rigid_point);
		}*/
	}

	// draw skeleton
	if (mLoadSkel)
	{
		std::stack<Skeleton::Bone*> stack;
		Skeleton::Bone* root = mSkeleton.GetRoot();
		stack.push(root);
		while (!stack.empty())
		{
			Skeleton::Bone* parent = stack.top();
			stack.pop();

			graphics3D->DrawSphere(parent->worldPos, 1);

			for (size_t i = 0; i < parent->children.size(); i++)
			{
				Skeleton::Bone* child = parent->children[i];
				stack.push(child);

				graphics3D->DrawLine(parent->worldPos, child->worldPos);
			}
		}
	}

	// draw springs
	if (mConfig1.mUseCables)
	{
		for (uint32 k = 0; k < mFemSys->GetFemPhysics()->GetNumCables(); k++)
		{
			const Cable& cable = mFemSys->GetFemPhysics()->GetCable(k);
			for (uint32 i = 0; i < cable.mCableNodes.size() - 1; i++)
			{
				graphics3D->SetColor(0, 1, 0);
				if (cable.mCableNodes[i].elem < 0)
					graphics3D->SetColor(0, 0, 1);

				Vector3R p1 = 100.0 * mFemSys->GetFemPhysics()->GetCablePosition(k, i);
				graphics3D->DrawSphere(DoubleToFloat(p1), 0.1f);

				Vector3R p2 = 100.0 * mFemSys->GetFemPhysics()->GetCablePosition(k, i + 1);
				graphics3D->DrawLine(DoubleToFloat(p1), DoubleToFloat(p2));
				if (i == cable.mCableNodes.size() - 2)
				{
					if (cable.mCableNodes[i + 1].elem < 0)
						graphics3D->SetColor(0, 0, 1);
					graphics3D->DrawSphere(DoubleToFloat(p2), 0.1f);
				}
			}
		}
	}
}
