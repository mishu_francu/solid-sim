#pragma once

#include "Physics/Common.h"
#include "Physics/Unified/Unified.h"

Physics::Capsule CreateCapsule(const Vector3& pos, const Quaternion& q, float hh, float r, float invMass, Matrix3* Icinv = nullptr);

Physics::Box CreateBox(const Vector3& pos, const Quaternion& q, const Vector3& dim, float invMass, int idx, Physics::Group* group, Matrix3& Icinv);

Physics::Sphere CreateSphere(const Vector3& pos, const Quaternion& q, float r, float invMass, Matrix3& Icinv);
