#ifndef CLOTH_DEMO_H
#define CLOTH_DEMO_H

#include "Physics/ClothPatch.h"
#include "Graphics3D/Camera.h"
#include "Geometry/Mesh.h"
#include "Physics/Common.h"

class Graphics3D;

// 3D cloth demo
class ClothDemo
{
public:
	ClothDemo();
	void Create(int type);
	bool LoadFromXml(XMLElement* xCloth);
	void Init();
	void Draw(Graphics3D* graphics3D, bool showDebug, int debugDrawFlags);
	void Update(float dt);
	void OnMouseMove(int x, int y);
	void OnMouseDown(int x, int y);
	void OnMouseUp(int x, int y);

private:
	void ComputeMouseRay(int x, int y);

public:
	enum DemoType
	{
		CLOTH_DEMO_DEFAULT,
		CLOTH_DEMO_SPHERE,
		CLOTH_DEMO_MESH,
		CLOTH_DEMO_SELF,
	};

	enum ClothAsset
	{
		CLOTH_ASSET_DEFAULT,
		CLOTH_ASSET_PATCH,
		CLOTH_ASSET_KATJA,
		CLOTH_ASSET_MD,
		CLOTH_ASSET_BUNNY, 
		CLOTH_ASSET_BUDDHA,
		CLOTH_ASSET_DRAGON,
	};

private:
	Physics::ClothPatch mCloth;
	int mDivisions;
	bool mHorizontal;
	bool mAttached;
	int mClothDemoType;
	int mClothAsset;
	Geometry::Mesh mMesh;
	Geometry::Mesh mMeshCopy;
	std::shared_ptr<Physics::CollisionMesh> mCollisionMesh;
	
	// mouse picking
	Vector3 mMouse;
	Vector3 mPick;
	int mViewWidth;
	int mViewHeight;
	float mViewFOV;
	Camera* mCamera;
	int mSelected;
};

#endif // CLOTH_DEMO_H
