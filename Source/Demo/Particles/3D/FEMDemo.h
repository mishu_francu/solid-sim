#pragma once

#include <memory>
#include <set>
#include <Math/Vector3.h>
#include <Demo/Particles/3D/FemSystem.h>
#include <Physics/FEM/FemPhysicsMatrixFree.h>
#include <Physics/FEM/FemPhysicsLinearIncompressible.h> // not the best include
#include <Physics/FEM/FemIO.h>
#include <Geometry/Mesh.h>
#include "Geometry/Skeleton.h"

using namespace FEM_SYSTEM; // this should be pretty safe, as it's just a demo

#ifdef BULLET_COLLISIONS
#include "BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h"
#include "BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.h"

#include "btBulletDynamicsCommon.h"
#include "BulletSoftBody/btDeformableMultiBodyDynamicsWorld.h"
#include "BulletSoftBody/btSoftBody.h"
#include "BulletSoftBody/btSoftBodyHelpers.h"
#include "BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.h"
#include "BulletDynamics/Featherstone/btMultiBodyConstraintSolver.h"

#include "BulletUtils/b3BulletDefaultFileIO.h"
#endif

struct TetraCube
{
#include "../cube.inl"
};

// forward declarations
class Graphics3D;

enum ModelFileFormat
{
	MFF_INVALID,
	MFF_VOL,
	MFF_TET,
	MFF_TET1,
	MFF_NODEELE,
	MFF_FEB,
	MFF_XML,
};

enum FixedSide
{
	FS_NONE = 0,
	FS_MIN = 1,
	FS_MAX = 2,
};

struct Model
{
	const char* mName;
	float mScale = 1.f;
	FEM_SYSTEM::Vector3R mOffset;
	ModelFileFormat mFileFormat = MFF_VOL;
	uint32 mNumFixed = 0;
	int* mFixedNodes = nullptr;
	SwapCoordsType mSwapCoords = SCT_SWAP_NONE;
	int mFixedAxis = -1;
	int mFixedSide = FS_NONE;

	~Model()
	{
		if (mFixedNodes)
			delete[] mFixedNodes;
	}
};

const int numModels = 37;

enum ModelIds
{
	MI_SPHERE,
	MI_HINGE,
	MI_LSHAPE,
	MI_BUNNY,
	MI_COW,
	MI_TORUS,
	MI_BOX,
	MI_BOX1,
	MI_BOX2,
	MI_SPINE,
	MI_WEDGE_FINGER,
	MI_SPHERE_HI,
	MI_DRAGON,
	MI_TORUS_HI,
	MI_SPINE_SIMPLE,
	MI_TUBE,
	MI_SPINE_BRIDGE,
	MI_TUBE_HI,
	MI_BUBBLE,
	MI_CYLINDER,
	MI_TORSO,
	MI_TORSO1,
	MI_TORSO2,
	MI_TORSO3,
	MI_TORSO_SKIN,
	MI_FEMUR,
	MI_HALF_PELVIC,
	MI_CUSHION,
	MI_FINGER,
	MI_TUBE_NEW,
	MI_BALL,
	MI_ADVANCED_BUBBLE,
	MI_CAT,
	MI_FINGER_FEB,
	MI_HAMMER,
	MI_BOX_EX,
	MI_CANTILEVER,
};


class FEMDemo
{
public:

	struct SkinInfo
	{
		int mBoneIdx = -1; // index of the associated bone (-1 means not associated to any bone)
		Vector3 mOffset; // local space position of the node
	};

	enum OuterInnerType
	{
		OUTER, // Changes creates a new figure
		INNER, // Changes creates a new plot
		NOT_INCLUDED // Changes creates nothing new
	};

	struct Config
	{
		const char* mName; // name used for identification (e.g. for Alembic export)
		Vector3R mOffset; // offset position in the world
		MethodType mType; // FEM numerical model identifier (e.g. corotational)
		uint32 mOrder; // order of shape functions
		int mResolution; // integer increasing with the resolution of the mesh
		bool mUseCantilever; // whether to use the cantilever or a loaded mesh
		float mCantileverWidth = 10; // The width and depth of the cantilever, when using a cantilever
		int mCantileverRatio = 4; // Length of the cantilever is mCantileverWidth * mCantileverRatio
		uint32 mModelIdx; // index of the loaded mesh
		bool mApplyTractions; // whether to apply tractions on the surface boundary or not
		float mAppliedPressure; // external pressure applied as external traction
		float mGravity; // gravitational acceleration
		float mYoungsModulus; // Young's modulus
		double mPoissonRatio; // Poisson ratio
		bool mDisplace;
		float mDisplacementWidth;
		uint32 mNumForceSteps = 4; // number of force application steps (quasi-static)
		SimulationType mSimType; // the type of force application/integration
		int mNumSubsteps; // number of integration sub-steps (for dynamics)
		MaterialModelType mMaterial; // material model type (mostly for nonlinear)
		uint32 mOuterIterations = 1; // main number of iterations for solvers
		float mInvBulkModulus; // reciprocal of bulk modulus
		float mConstraintScale = 1; // used by nonlinear mixed FEM
		bool mHasCollisions = false;
		float mContactStiffnes;
		float mResidualThreshold;
		bool mLogConstraint = false;
		bool mUseCables = false;
		void* mCustomConfig = nullptr;
	};

	const Config mPresetConfigCustom{
		"Custom",
	};

	const Config mPresetConfigCompressedRubberBlock{
		"CompressedRubberBlock", // name
		{0, 0, 0}, // offset
		MethodType::MT_LINEAR_ELASTICITY, // type
		1, // order
		4, // resolution
		true, // use cantilever
		20, // width
		1, // ratio
		0, // model index
		false, // apply tractions
		10000, // applied external pressure
		0, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.49f, // Poisson ratio
		true, // apply displacements
		-0.05f, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		10, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_LINEAR,
		1, // Newton iterations
		-1, // inverse bulk modulus,
		1e6f, // constraints scale
	};

	const Config mPresetConfigCompressedRubberBlock15kPa{
		"CompressedRubberBlock_15kPa", // name
		{0, 0, 0}, // offset
		MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
		1, // order
		12, // resolution
		true, // use cantilever
		20, // width
		1, // ratio
		0, // model index
		true, // apply tractions
		15000, // applied external pressure
		0, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.4999f, // Poisson ratio
		false, // apply displacements
		-0.05f, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		10, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_OGDEN,
		100, // Newton iterations
		-1, // inverse bulk modulus,
		1, // constraints scale
	};

	const Config mPresetConfigCompressedRubberBlock15kPaStdFEM{
	"CompressedRubberBlock_15kPaStdFEM", // name
	{0, 0, 0}, // offset
	MethodType::MT_NONLINEAR_ELASTICITY, // type
	1, // order
	12, // resolution
	true, // use cantilever
	20, // width
	1, // ratio
	0, // model index
	true, // apply tractions
	15000, // applied external pressure
	0, // gravity
	66.f, // Young's modulus in KPa - silicone rubber (above 66)
	0.499, // Poisson ratio
	false, // apply displacements
	-0.05f, // amount of displacement
	10, // number of force application steps (quasi-static)
	SimulationType::ST_QUASI_STATIC,
	10, // integration sub-steps (dynamic)
	MaterialModelType::MMT_NEO_HOOKEAN,
	100, // Newton iterations
	-1, // inverse bulk modulus,
	1e6f, // constraints scale
	};

	const Config mPresetConfigCompressedRubberBlock50kPa{
		"CompressedRubberBlock_50kPa", // name
		{0, 0, 0}, // offset
		MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
		1, // order
		12, // resolution
		true, // use cantilever
		20, // width
		1, // ratio
		0, // model index
		true, // apply tractions
		50000, // applied external pressure
		0, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.4999f, // Poisson ratio
		false, // apply displacements
		-0.05f, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		10, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_OGDEN,
		100, // Newton iterations
		-1, // inverse bulk modulus,
		1, // constraints scale
	};

	const Config mPresetConfigCompressedRubberBlock100kPa{
		"CompressedRubberBlock_100kPa", // name
		{0, 0, 0}, // offset
		MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
		1, // order
		10, // resolution
		true, // use cantilever
		20, // width
		1, // ratio
		0, // model index
		true, // apply tractions
		100000, // applied external pressure
		0, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.5f, // Poisson ratio
		false, // apply displacements
		-0.05f, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		10, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_OGDEN,
		5, // Newton iterations
		-1, // inverse bulk modulus,
		1e6f, // constraints scale
	};

	const Config mPresetConfigCantilever{
		"Cantilever", // name
		{0, 0, 0}, // offset
		MethodType::MT_NONLINEAR_ELASTICITY, // type
		1, // order
		2, // resolution
		false, // use cantilever
		10, // width
		4, // ratio
		MI_WEDGE_FINGER, // model index
		false, // apply tractions
		0, // applied external pressure
		-9.8f, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.45f, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_IMPLICIT,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_NEO_HOOKEAN,
		100, // Newton iterations
	};

	const Config mPresetConfigCantileverDist{
		"CantileverDist", // name
		{0, 0, 0}, // offset
		MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
		1, // order
		1, // resolution
		true, // use cantilever
		10, // width
		10, // ratio
		0, // model index
		false, // apply tractions
		0, // applied external pressure
		-9.8f, // gravity
		6000.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.49f, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_OGDEN,
		6, // Newton iterations
	};

	const Config mPresetConfigCantileverStdFEM{
		"CantileverStdFEM", // name
		{0, 0, 0}, // offset
		MethodType::MT_NONLINEAR_ELASTICITY, // type
		1, // order
		1, // resolution
		true, // use cantilever
		10, // width
		10, // ratio
		0, // model index
		false, // apply tractions
		0, // applied external pressure
		-9.8f, // gravity
		6000.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.49f, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		18, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_NEO_HOOKEAN,
		6, // Newton iterations
	};

	const Config mPresetConfigHangingSpineDist{
		"HangingSpineDistOgden", // name
		{0, 0, 0}, // offset
		MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
		1, // order
		1, // resolution
		false, // use cantilever
		10, // width
		4, // ratio
		MI_SPINE, // model index
		false, // apply tractions
		0, // applied external pressure
		-9.8f, // gravity
		266.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.49f, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_OGDEN,
		10, // Newton iterations
	};

	const Config mPresetConfigHangingSpineStdFEM{
		"HangingSpineStdFEM", // name
		{0, 0, 0}, // offset
		MethodType::MT_NONLINEAR_ELASTICITY, // type
		1, // order
		1, // resolution
		false, // use cantilever
		10, // width
		4, // ratio
		MI_SPINE, // model index
		false, // apply tractions
		0, // applied external pressure
		-9.8f, // gravity
		266.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.499, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_NEO_HOOKEAN,
		10, // Newton iterations
	};

	const Config mPresetConfigHangingSpineDistImplicit{
	"HangingSpineDistOgdenImplicit", // name
	{0, 0, 0}, // offset
	MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
	1, // order
	1, // resolution
	false, // use cantilever
	10, // width
	4, // ratio
	MI_SPINE, // model index
	false, // apply tractions
	0, // applied external pressure
	-1.5f, // gravity
	266.f, // Young's modulus in KPa - silicone rubber (above 66)
	0.49f, // Poisson ratio
	false, // apply displacements
	0, // amount of displacement
	10, // number of force application steps (quasi-static)
	SimulationType::ST_IMPLICIT,
	1, // integration sub-steps (dynamic)
	MaterialModelType::MMT_DISTORTIONAL_OGDEN,
	3, // Newton iterations
	};

	const Config mPresetConfigHangingSpineStdFEMImplicit{
		"HangingSpineStdFEMImplicit", // name
		{0, 0, 0}, // offset
		MethodType::MT_NONLINEAR_ELASTICITY, // type
		1, // order
		1, // resolution
		false, // use cantilever
		10, // width
		4, // ratio
		MI_SPINE, // model index
		false, // apply tractions
		0, // applied external pressure
		-1.5f, // gravity
		266.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.49f, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_IMPLICIT,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_NEO_HOOKEAN,
		3, // Newton iterations
	};

	const Config mPresetConfigInflatedTube{
		"Inflated tube", // name
		{0, 0, 0}, // offset
		MethodType::MT_NONLINEAR_ELASTICITY, // type
		1, // order
		2, // resolution
		false, // use cantilever
		10, // width
		4, // ratio
		MI_TUBE_HI, // model index
		true, // apply tractions/pressure
		10000.f, // applied external pressure
		0, // gravity
		90.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.499, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_NEO_HOOKEAN,
		5, // Newton iterations
	};

	const Config mPresetConfigTorso{
		"Torso", // name
		{0, 0, 0}, // offset
		MethodType::MT_NONLINEAR_ELASTICITY, // type
		1, // order
		2, // resolution
		false, // use cantilever
		10, // width
		4, // ratio
		24, // model index
		false, // apply tractions/pressure
		0.f, // applied external pressure
		0, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.45f, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_IMPLICIT,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_LINEAR,
		1, // outer iterations
	};

	const Config mPresetConfigStretchedCylinder{
		"StretchedCylinder", // name
		{0, 0, 0}, // offset
		MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
		1, // order
		4, // resolution
		true, // use cantilever
		20, // width
		1, // ratio
		MI_CYLINDER, // model index
		false, // apply tractions
		0, // applied external pressure
		0, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.4, // Poisson ratio
		true, // apply displacements
		0.2f, // amount of displacement (2x)
		50, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		10, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_OGDEN,
		200, // Newton iterations
		-1, // inverse bulk modulus,
		1, // constraints scale
	};

	const Config mPresetConfigStretchedCylinderNH{
		"StretchedCylinderNH", // name
		{0, 0, 0}, // offset
		MethodType::MT_NONLINEAR_ELASTICITY, // type
		1, // order
		4, // resolution
		true, // use cantilever
		20, // width
		1, // ratio
		MI_CYLINDER, // model index
		false, // apply tractions
		0, // applied external pressure
		0, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.4, // Poisson ratio
		true, // apply displacements
		0.2f, // amount of displacement (2x)
		50, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		10, // integration sub-steps (dynamic)
		MaterialModelType::MMT_NEO_HOOKEAN,
		200, // Newton iterations
		-1, // inverse bulk modulus,
		1, // constraints scale
	};

	const Config mPresetConfigTwistedCube{
		"TwistedCube", // name
		{0, 0, 0}, // offset
		MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
		1, // order
		6, // resolution
		true, // use cantilever
		20, // width
		1, // ratio
		0, // model index
		false, // apply tractions
		0, // applied external pressure
		0, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.45f, // Poisson ratio
		true, // apply displacements
		0.2f, // amount of displacement (2x)
		10, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		10, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_OGDEN,
		100, // Newton iterations
		-1, // inverse bulk modulus,
		1, // constraints scale
	};

	const Config mPresetConfigContact{
		"Contact", // name
		{0, 0, 0}, // offset
		MethodType::MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY, // type
		1, // order
		2, // resolution
		false, // use cantilever
		10, // width
		4, // ratio
		MI_SPHERE, // model index
		false, // apply tractions
		0, // applied external pressure
		-1, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.499, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		10, // number of force application steps (quasi-static)
		SimulationType::ST_IMPLICIT,
		4, // integration sub-steps (dynamic)
		MaterialModelType::MMT_DISTORTIONAL_OGDEN,
		100, // Newton iterations
		-1, // inverse bulk modulus,
		1, // constraints scale
		true, // enable collisions
	};

	const Config mPresetConfigTetrahedron{
		"Tetrahedron", // name
		{0, 0, 0}, // offset
		MethodType::MT_NONLINEAR_ELASTICITY, // type
		1, // order
		4, // resolution
		true, // use cantilever
		20, // width
		1, // ratio
		0, // model index
		false, // apply tractions
		10000, // applied external pressure
		-9.8, // gravity
		66.f, // Young's modulus in KPa - silicone rubber (above 66)
		0.4f, // Poisson ratio
		false, // apply displacements
		0, // amount of displacement
		1, // number of force application steps (quasi-static)
		SimulationType::ST_QUASI_STATIC,
		1, // integration sub-steps (dynamic)
		MaterialModelType::MMT_NEO_HOOKEAN,
		5, // Newton iterations
		-1, // inverse bulk modulus,
		1, // constraints scale
	};


	static const int mNumberOfPresetConfigs = 20;
	int mChosenPreset = 6;

	const char * const mPresetConfigNames[mNumberOfPresetConfigs] =
	{
		"Custom",
		"CompressedRubberBlock",
		mPresetConfigCompressedRubberBlock15kPa.mName,
		mPresetConfigCompressedRubberBlock15kPaStdFEM.mName,
		mPresetConfigCompressedRubberBlock50kPa.mName,
		mPresetConfigCompressedRubberBlock100kPa.mName,
		"Cantilever",
		mPresetConfigCantileverDist.mName,
		mPresetConfigCantileverStdFEM.mName,
		mPresetConfigHangingSpineDist.mName,
		mPresetConfigHangingSpineStdFEM.mName,
		mPresetConfigHangingSpineDistImplicit.mName,
		mPresetConfigHangingSpineStdFEMImplicit.mName,
		"InflatedTube",
		"Torso",
		"StretchedCylinder",
		"StretchedCylinderNH",
		"TwistedCube",
		"Contact",
		"Tetrahedron"
	};

	const Config mPresetConfigs[mNumberOfPresetConfigs] =
	{
		mPresetConfigCustom,
		mPresetConfigCompressedRubberBlock,
		mPresetConfigCompressedRubberBlock15kPa,
		mPresetConfigCompressedRubberBlock15kPaStdFEM,
		mPresetConfigCompressedRubberBlock50kPa,
		mPresetConfigCompressedRubberBlock100kPa,
		mPresetConfigCantilever,
		mPresetConfigCantileverDist,
		mPresetConfigCantileverStdFEM,
		mPresetConfigHangingSpineDist,
		mPresetConfigHangingSpineStdFEM,
		mPresetConfigHangingSpineDistImplicit,
		mPresetConfigHangingSpineStdFEMImplicit,
		mPresetConfigInflatedTube,
		mPresetConfigTorso,
		mPresetConfigStretchedCylinder,
		mPresetConfigStretchedCylinderNH,
		mPresetConfigTwistedCube,
		mPresetConfigContact,
		mPresetConfigTetrahedron,
	};

public:
	void Init();
	void Update(float dt);
	void Draw(Graphics3D* graphics3D);

private:
	void Init(std::unique_ptr<FEM_SYSTEM::FemSystem>& femSys, const Config& config);
	void InitCantilever(FEM_SYSTEM::FemSystem* femSys, const FEM_SYSTEM::Vector3R& offset,
		float w = 100.f, int segments = 5, int sides = 2);
	void InitTetGenCantilever(FEM_SYSTEM::FemSystem* femSys, const FEM_SYSTEM::Vector3R& offset,
		float l = 400.f, float h = 100.f, float w = 100.f, uint32 hsegments = 1, uint32 vsegment = 1, uint32 dsegment = 1);
	void InitTetrahedron(FEM_SYSTEM::FemSystem* femSys, const FEM_SYSTEM::Vector3R& offset);
	void InitSprings(FEM_SYSTEM::FemSystem* femSys, real length, Vector3R offset, Vector3R dir);
	void DrawSystem(Graphics3D* graphics3D, FEM_SYSTEM::FemSystem* femSys);
	void DrawUI();
	void ApplyConfigPreset(Config preset);

	void UpdateSystem(FEM_SYSTEM::FemSystem* femSys, float dt, const Config* config = nullptr);

	void ComputeBoundarySurface(FEM_SYSTEM::FemSystem* femSys, const Config& config);
	void Displace(FEM_SYSTEM::FemSystem* femSys, real delta, bool rotate);


	// Related to collecting data from experiments
	std::string GetWorkingOutputFolderForMeasurements();
	void PerformMeasurements(FEM_SYSTEM::FemSystem* femSys, bool createFile = false);

private:
	Geometry::Mesh mCollisionMesh;

	std::unique_ptr<FEM_SYSTEM::FemSystem> mFemSys;
	std::unique_ptr<FEM_SYSTEM::FemSystem> mFemSys2;
	std::unique_ptr<FEM_SYSTEM::FemSystem> mFemSys3;
	std::unique_ptr<FEM_SYSTEM::FemSystem> mFemSys4;
	std::unique_ptr<FEM_SYSTEM::FemSystem> mFemSys5;
	std::unique_ptr<FEM_SYSTEM::FemSystem> mFemSys6;
	std::vector<FEM_SYSTEM::Vector3R> mForces;

	bool mUseAbcExport = true;
	bool mUseVTKHeatMapExport = false;
	bool mDrawVisualMesh = false;

	Config mConfig1;
	Config mConfig2;
	FemPhysicsLinearIncompressible::Config mIncompressibleConfig;
	FemPhysicsMatrixFree::Config mNonlinearConfig;

	std::set<uint32> mInnerSurface;

	int mForceStep = 0;

	// UI stuff
	float mYoungsModulusValue = 66;
	double mPoissonRatioValue = 0.49f;
	int mSelectedSimulator = MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY;
	int mSelectedModel = 26;
	float mPressureValue = 1000.f;
	float mMinPressure = 0;
	float mMaxPressure = 15000;
	int mSelectedSimType = ST_QUASI_STATIC;
	float mGravityValue = -9.8f;
	bool mCheckCantilever = false;
	int mSubstepsValue = 1;
	bool mCheckApplyPressure = false;
	int mMaterialModel = MMT_NEO_HOOKEAN_OGDEN;
	int mNonlinearSolverValue = 0;
	int mOuterIterationsValue = 1;
	int mInnerIterationsValue = 100;
	float mDescentRateValue = 1e-5f;
	int mPressureOrderValue = 1;
	bool mCheckDisplace = true;
	int mForceStepsValue = 10;
	int mOrder = 1;
	int mModelResolution = 2;
	float mCantileverWidth = 10;
	int mCantileverRatio = 4;
	bool mCheckRotate = false;
	bool mCheckCollisions = false;
	float mDisplacementWidth = 0.00075f;
	float mInvBulkModulusValue = -1.f;
	float mConstraintScaleValue = 1;
	float mContactStiffnessValue = 200000;
	float mFloorOrdinate = -0.4f;
	float mFloorAbscisa = 0.4f;
	float mCeilingOrdinate = 0.4f;
	float mResidualThresholdValue = 0.01;
	bool mLogConstrValue = false;
	bool mOptimizer = false;
	bool mVerbose = true;
	bool mUseCables = true;
	bool mUseFreeCable = false;
	float mCableStiffness = 1e3f;
	float mCableDamping = 0.001f;
	int mCableDivisions = 10;
	float mActuation = 1;
	float mActuation2 = 1;

	size_t mUpdateCounter = 0;
	float mElapsedTime = 0;
	bool mPerformMeasurements = false;
	const char * mMeasurementsRootOutputPath = "Measurements";
	bool mMeasureGlobalVolumeError = true;
	bool mMeasureModelXWidth = true;
	bool mMeasureModelRadius = true;
	bool mMeasureCantileverDeflection = true;
	bool mMeasureEnergy = true;

	char mTestCaseName[25] = "default";
	char *mOutputDelimiter = "-";

	std::vector<size_t> mDeflectionCurve;
	std::vector<size_t> mDeflectionPoint;


	int mIOYoungModulus = OUTER;
	int mIOPoissonRatio = OUTER;
	int mIOSimulationType = NOT_INCLUDED;
	int mIOSubsteps = NOT_INCLUDED;
	int mIOForcesteps = NOT_INCLUDED;
	int mIOMethodType = INNER;
	int mIOOuterIterations = NOT_INCLUDED;
	int mIOSolver = NOT_INCLUDED;
	int mIODescentValue = NOT_INCLUDED;
	int mIOInnerIterations = NOT_INCLUDED;
	int mIOMaterialModelType = INNER;
	int mIOPressureOrder = NOT_INCLUDED;
	int mIOResolution = INNER;
	int mIODisplacementWidth = NOT_INCLUDED;

	// drawing flags
	bool mDrawTets = false;
	bool mDrawBoundaryMesh = true;
	bool mDrawNodes = false;
	float mDrawColor[3] = { 1.0f, 0, 0 };
	bool mDrawHeatMap = false;
	bool mDrawHighOrder = false;

	char *mExportOutputFolder = new char[50]{ "./" };
	char *mExportFileName = new char[25]{ "" };

	bool mExportAsELENODE = false;
	bool mExportAsVTK = true;

	bool mLoadSkel = false;
	Geometry::Skeleton mSkeleton;
	std::vector<SkinInfo> mSkinInfos;

	int mCeilingState = 0;
};
