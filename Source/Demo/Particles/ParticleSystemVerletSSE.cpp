#include <Demo/Particles/ParticleSystemVerlet.h>

#if USE_SSE == 1
struct EightVectors
{
	__m256 x, y;
};

//typedef struct _VERTEX_AOS {
//	float x, y, z, w;
//} Vertex_aos; // AoS structure declaration
//
//typedef struct _VERTEX_SOA {
//	float x[4];
//	float y[4];
//	float z[4];
//	float w[4];
//} Vertex_soa; // SoA structure declaration
//
//void swizzle_asm (Vertex_aos *vin, Vertex_soa *vout)
//{
//	// in mem: x1y1z1w1-x2y2z2w2-x3y3z3w3-x4y4z4w4-
//	// SWIZZLE XYZW --> XXXX
//	_asm
//	{
//		mov ebx, vin // get structure addresses
//		mov edx, vout
//		movaps xmm1, [ebx ] // x4 x3 x2 x1
//		movaps xmm2, [ebx + 16] // y4 y3 y2 y1
//		movaps xmm3, [ebx + 32] // z4 z3 z2 z1
//		movaps xmm4, [ebx + 48] // w4 w3 w2 w1
//		movaps xmm7, xmm4 // xmm7= w4 z4 y4 x4
//		movhlps xmm7, xmm3 // xmm7= w4 z4 w3 z3
//		movaps xmm6, xmm2 // xmm6= w2 z2 y2 x2
//		movlhps xmm3, xmm4 // xmm3= y4 x4 y3 x3
//		movhlps xmm2, xmm1 // xmm2= w2 z2 w1 z1
//		movlhps xmm1, xmm6 // xmm1= y2 x2 y1 x1
//		movaps xmm6, xmm2// xmm6= w2 z2 w1 z1
//		movaps xmm5, xmm1// xmm5= y2 x2 y1 x1
//		shufps xmm2, xmm7, 0xDD // xmm2= w4 w3 w2 w1 => v4
//		shufps xmm1, xmm3, 0x88 // xmm1= x4 x3 x2 x1 => v1
//		shufps xmm5, xmm3, 0xDD // xmm5= y4 y3 y2 y1 => v2
//		shufps xmm6, xmm7, 0x88 // xmm6= z4 z3 z2 z1 => v3
//		movaps [edx], xmm1 // store X
//		movaps [edx+16], xmm5 // store Y
//		movaps [edx+32], xmm6 // store Z
//		movaps [edx+48], xmm2 // store W
//	}
//}

void ParticleSystemVerlet::IntegrateVerletSSE()
{	
	size_t n = particles.size() / 4;
	std::vector<FourVectors> pos(n), prev(n);
	std::vector<vector4> im(n);
	for (size_t i = 0; i < n; i++)
	{
		size_t base = i * 4;
		vector4 v1 = particles[base].pos.GetNative();
		vector4 v2 = particles[base + 1].pos.GetNative();
		vector4 v3 = particles[base + 2].pos.GetNative();
		vector4 v4 = particles[base + 3].pos.GetNative();
		_MM_TRANSPOSE4_PS(v1, v2, v3, v4);
		pos[i].x = v1;
		pos[i].y = v2;

		v1 = particles[base].prevPos.GetNative();
		v2 = particles[base + 1].prevPos.GetNative();
		v3 = particles[base + 2].prevPos.GetNative();
		v4 = particles[base + 3].prevPos.GetNative();
		_MM_TRANSPOSE4_PS(v1, v2, v3, v4);
		prev[i].x = v1;
		prev[i].y = v2;

		im[i] = _mm_setr_ps(particles[base].invMass, particles[base + 1].invMass, particles[base + 2].invMass, particles[base + 3].invMass);
	}

	const float h2 = lengthScale * timeStep * timeStep;
	Vector2 force = gravity;
	force.Scale(h2);
	const FourVectors f(force);
	vector4 one = _mm_set1_ps(1.f);
	for (size_t i = 0; i < n; i++)
	{
		vector4 m = _mm_min_ps(im[i], one);
		FourVectors v = pos[i] - prev[i];		
		pos[i] += m * (v + f);
	}

	for (size_t i = 0; i < n; i++)
	{
		size_t base = i * 4;
		for (size_t j = 0; j < 4; j++)
			particles[base + j].prevPos = particles[base + j].pos;
		vector4 v1 = pos[i].x;
		vector4 v2 = pos[i].y;
		vector4 v3 = _mm_setzero_ps();
		vector4 v4 = _mm_setzero_ps();
		_MM_TRANSPOSE4_PS(v1, v2, v3, v4);
		particles[base].pos.GetNative() = v1;
		particles[base + 1].pos.GetNative() = v2;
		particles[base + 2].pos.GetNative() = v3;
		particles[base + 3].pos.GetNative() = v4;
	}

}

void ParticleSystemVerlet::ProjectLinksJacobiSSE()
{
	PROFILE_SCOPE("ProjectJacobiSSE");
	size_t m = links.size() / 4;
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < m; i++)
		{
			// TODO: cache optimization?
			size_t base = i * 4;
			vector4 v[4], w[4];
			for (size_t j = 0; j < 4; j++)
			{
				int idx = links[base + j].i1;
				v[j] = particles[idx].pos.GetNative();
				idx = links[base + j].i2;
				w[j] = particles[idx].pos.GetNative();
			}
			_MM_TRANSPOSE4_PS(v[0], v[1], v[2], v[3]);
			_MM_TRANSPOSE4_PS(w[0], w[1], w[2], w[3]);
			links4[i].pos1.x = v[0];
			links4[i].pos1.y = v[1];
			links4[i].pos2.x = w[0];
			links4[i].pos2.y = w[1];

			FourVectors delta = links4[i].pos1 - links4[i].pos2;
			vector4 len0 = links4[i].len;
			vector4 len = delta.Normalize();
			links4[i].lambda = _mm_mul_ps(_mm_sub_ps(len, len0), links4[i].sim);
			links4[i].normal = links4[i].lambda * delta;
		}
		for (size_t i = 0; i < m; i++)
		{
			FourVectors delta = links4[i].normal;
			vector4 v[4];
			v[0] = delta.x;
			v[1] = delta.y;
			v[2] = v[3] = _mm_setzero_ps();
			_MM_TRANSPOSE4_PS(v[0], v[1], v[2], v[3]); // TODO: optimize?
			size_t base = i * 4;
			for (size_t j = 0; j < 4; j++)
			{
				int i1 = links[base + j].i1;
				int i2 = links[base + j].i2;
				Vector2 disp; disp.GetNative() = v[j];
				particles[i1].pos.Subtract(disp * particles[i1].invMass);
				particles[i2].pos.Add(disp * particles[i2].invMass);
			}			
		}
	}
}

void ParticleSystemVerlet::ProjectLinksGaussSeidelSSE()
{
	PROFILE_SCOPE("ProjectGS SSE");
	size_t m = links.size() / 4;
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < m; i++)
		{
			// TODO: cache optimization?
			size_t base = i * 4;
			vector4 v[4], w[4];
			for (size_t j = 0; j < 4; j++)
			{
				int idx = links[base + j].i1;
				v[j] = particles[idx].pos.GetNative();
				idx = links[base + j].i2;
				w[j] = particles[idx].pos.GetNative();
			}
			_MM_TRANSPOSE4_PS(v[0], v[1], v[2], v[3]);
			_MM_TRANSPOSE4_PS(w[0], w[1], w[2], w[3]);
			links4[i].pos1.x = v[0];
			links4[i].pos1.y = v[1];
			links4[i].pos2.x = w[0];
			links4[i].pos2.y = w[1];

			FourVectors delta = links4[i].pos1 - links4[i].pos2;
			vector4 len0 = links4[i].len;
			vector4 len = delta.Normalize();
			vector4 lambda = _mm_mul_ps(_mm_sub_ps(len, len0), links4[i].sim);
			delta = lambda * delta;

			v[0] = delta.x;
			v[1] = delta.y;
			v[2] = v[3] = _mm_setzero_ps();
			_MM_TRANSPOSE4_PS(v[0], v[1], v[2], v[3]); // TODO: optimize?
			for (size_t j = 0; j < 4; j++)
			{
				int i1 = links[base + j].i1;
				int i2 = links[base + j].i2;
				Vector2 disp; disp.GetNative() = v[j];
				particles[i1].pos.Subtract(disp * particles[i1].invMass);
				particles[i2].pos.Add(disp * particles[i2].invMass);
			}			
		}
	}
}

void ParticleSystemVerlet::ProjectLinksJacobi()
{
	PROFILE_SCOPE("ProjectLinksJacobi");
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < links.size(); i++)
		{
			ParticleVerlet* p1 = &particles[links[i].i1];
			ParticleVerlet* p2 = &particles[links[i].i2];
			Vector2 delta = p1->pos - p2->pos;
			float len0 = links[i].len;
			float len = delta.Length();
			delta.Scale(1 / len);
			links[i].normal = delta;
			links[i].lambda = (len - len0) / (p1->invMass + p2->invMass);
		}
		for (size_t i = 0; i < links.size(); i++)
		{
			Vector2 delta = links[i].lambda * links[i].normal;
			particles[links[i].i1].pos.Subtract(delta * particles[links[i].i1].invMass);
			particles[links[i].i2].pos.Add(delta * particles[links[i].i2].invMass);
		}
	}
}

void ParticleSystemVerlet::ProjectLinksGS()
{
	PROFILE_SCOPE("ProjectLinksGS");
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < links.size(); i++)
		{
			ParticleVerlet* p1 = &particles[links[i].i1];
			ParticleVerlet* p2 = &particles[links[i].i2];
			Vector2 delta = p1->pos - p2->pos;
			float len0 = links[i].len;
			float len = delta.Length();
			delta.Scale(1 / len);
			delta.Scale((len - len0) / (p1->invMass + p2->invMass));
			p1->pos.Subtract(delta * p1->invMass);
			p2->pos.Add(delta * p2->invMass);
		}
	}
}

void ParticleSystemVerlet::ProjectPositionsMinResSSE(bool gsWarmStart)
{
	PROFILE_SCOPE("Project MinRes SSE");

	const size_t n = constraints.size();
	if (n == 0)
		return;
	int numIters = numIterations;
	if (gsWarmStart)
	{
		ProjectPositions(1);
		if (--numIters == 0)
			return;
	}

	float rSqr = 0;
	float ySqr = 0;
	for (size_t i = 0; i < n; i++)
	{
		const int i1 = constraints[i].i1;
		Vector2 delta;
		const int i2 = constraints[i].i2;
		delta = particles[i2].pos - particles[i1].pos;
		r[i] = delta.Length() - constraints[i].len;		
		
		delta.Normalize(); // TODO: return length
		normals[i] = delta;
		z[i] = diag[i] * r[i];
	}
	// update dots between normals and compute alpha
	for (size_t k = 0; k < sparseMat.size(); k++)
	{
		sparseMat[k].dot = normals[sparseMat[k].i].Dot(normals[sparseMat[k].j]);
		const float val = sparseMat[k].a * sparseMat[k].dot;
		sparseMat[k].val = val;
		const size_t i = sparseMat[k].i;
		const size_t j = sparseMat[k].j;
		const float di = val * r[j];
		const float dj = val * r[i];
		z[i] += di;
		z[j] += dj;
	}
	for (size_t i = 0; i < n; i++)
	{
		rSqr += r[i] * z[i];
		ySqr += z[i] * z[i];
	}

	float alpha = rSqr / ySqr;

	for (size_t i = 0; i < n; i++)
	{
		const Constraint& link = constraints[i];
		Vector2 disp = alpha * r[i] * normals[i];
		particles[link.i1].pos += particles[link.i1].invMass * disp;
		particles[link.i2].pos -= particles[link.i2].invMass * disp;
		d[i] = r[i];
	}

	// the iterations
	size_t m = n / 4;
	for (size_t i = 0; i < m; i++)
	{
		vector4 v[4];
		size_t base = i * 4;
		for (size_t j = 0; j < 4; j++) 
			v[j] = normals[base + j].GetNative();
		_MM_TRANSPOSE4_PS(v[0], v[1], v[2], v[3]);
		links4[i].normal.x = v[0];
		links4[i].normal.y = v[1];
	}
	for (int iter = 1; iter < numIters; iter++)
	{		
		float rSqr1 = 0;
		for (size_t i = 0; i < m; i++)
		{
			size_t base = i * 4;
			vector4 v[4], w[4];
			for (size_t j = 0; j < 4; j++)
			{
				int idx = links[base + j].i1;
				v[j] = particles[idx].pos.GetNative();
				idx = links[base + j].i2;
				w[j] = particles[idx].pos.GetNative();
			}
			_MM_TRANSPOSE4_PS(v[0], v[1], v[2], v[3]);
			_MM_TRANSPOSE4_PS(w[0], w[1], w[2], w[3]);
			// TODO: no need for links4
			links4[i].pos1.x = v[0];
			links4[i].pos1.y = v[1];
			links4[i].pos2.x = w[0];
			links4[i].pos2.y = w[1];

			FourVectors delta = links4[i].pos1 - links4[i].pos2;
			vector4 len0 = links4[i].len;
			vector4 len = delta.Length();
			vector4 ri = _mm_sub_ps(len, len0);
			vector4 diagi = _mm_load_ps(&diag[base]);
			rSqr1 += _mm_cvtss_f32(_mm_dp_ps(ri, _mm_mul_ps(diagi, ri), 0xf1));

			_mm_store_ps(&r[base], ri);
		}

		for (size_t k = 0; k < sparseMat.size(); k++)
		{
			const float val = sparseMat[k].val;
			const size_t i = sparseMat[k].i;
			const size_t j = sparseMat[k].j;
			rSqr1 += val * (r[i] * r[i] + r[j] * r[j]);
		}
		
		float beta = rSqr1 / rSqr;
		
		rSqr = rSqr1;
		float delta = 0;
		float zSqr = 0;
		vector4 beta4 = _mm_set1_ps(beta);
		for (size_t i = 0; i < m; i++)
		{
			size_t base = i * 4;
			vector4 di = _mm_load_ps(&d[base]);
			vector4 ri = _mm_load_ps(&r[base]);
			di = _mm_add_ps(ri, _mm_mul_ps(di, beta4));
			_mm_store_ps(&d[base], di);
			vector4 diagi = _mm_load_ps(&diag[base]);
			vector4 zi = _mm_mul_ps(diagi, di);
			_mm_store_ps(&z[base], zi);
		}
		for (size_t k = 0; k < sparseMat.size(); k++)
		{
			const float val = sparseMat[k].val;
			const size_t i = sparseMat[k].i;
			const size_t j = sparseMat[k].j;
			const float di = val * d[j];
			const float dj = val * d[i];
			z[i] += di;
			z[j] += dj;
		}
		for (size_t i = 0; i < m; i++)
		{
			size_t base = i * 4;
			vector4 zi = _mm_load_ps(&z[base]);
			zSqr += _mm_cvtss_f32(_mm_dp_ps(zi, zi, 0xf1));
			vector4 ri = _mm_load_ps(&r[base]);
			delta += _mm_cvtss_f32(_mm_dp_ps(ri, zi, 0xf1));
		}

		alpha = delta / zSqr;

		vector4 alpha4 = _mm_set1_ps(alpha);
		for (size_t i = 0; i < m; i++)
		{
			size_t base = i * 4;
			vector4 di = _mm_load_ps(&d[base]);
			vector4 lambda = _mm_mul_ps(di, alpha4);
			FourVectors delta = links4[i].normal;
			delta.x = _mm_mul_ps(lambda, delta.x);
			delta.y = _mm_mul_ps(lambda, delta.y);
			vector4 v[4];
			v[0] = delta.x;
			v[1] = delta.y;
			v[2] = v[3] = _mm_setzero_ps();
			_MM_TRANSPOSE4_PS(v[0], v[1], v[2], v[3]);
			for (size_t j = 0; j < 4; j++)
			{
				int i1 = links[base + j].i1;
				int i2 = links[base + j].i2;
				Vector2 disp; disp.GetNative() = v[j];
				particles[i1].pos.Add(disp * particles[i1].invMass);
				particles[i2].pos.Subtract(disp * particles[i2].invMass);
			}			
		}
	}
}

#endif