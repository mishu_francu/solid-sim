#include "Rigid.h"
#include "Engine/Base.h"

using Math::clamp;

void RigidSystem::StepVTS(float h)
{
	Collide();

	// integrate velocities
	for (size_t i = 0; i < rigids.size(); i++)
	{
		// TODO: external torque?
		rigids[i].vel += h * gravity;
	}

	// pre-cache data
	std::vector<ContactEx> contactsEx(contacts.size());
	for (size_t i = 0; i < contacts.size(); i++)
	{
		const Contact& contact = contacts[i];
		//Printf("contact %d: %f, %f\n", i, contact.n.GetX(), contact.n.GetY());
		int j1 = contact.idx1;
		Vector2 t1 = contact.q.GetPerpendicular();
		t1.Rotate(rigids[j1].theta);

		Vector2 q = contact.q;
		q.Rotate(rigids[j1].theta); // q in world space
		float qxn = q.Cross(contact.n);

		contactsEx[i].t1 = t1;
		contactsEx[i].stab = 0.5f * contact.depth / h;
		contactsEx[i].qxn = qxn;

		if (contact.idx2 >= 0)
		{
			int j2 = contact.idx2;
			Vector2 t2 = contact.p.GetPerpendicular(); 
			t2.Rotate(rigids[j2].theta);

			Vector2 p = contact.p;
			p.Rotate(rigids[j2].theta); // p in world space
			float pxn = p.Cross(contact.n);

			contactsEx[i].t2 = t2;
			contactsEx[i].pxn = pxn;
		}
	}

	// solver
	const int numIterations = 20;
	const float mu = 0.25f;
	for (int k = 0; k < numIterations; k++)
	{
		// friction
		for (size_t i = 0; i < contacts.size(); i++)
		{
			Contact& contact = contacts[i];
			if (contact.idx2 < 0)
			{
				// static contact
				int j = contact.idx1;
				Vector2 vrel = rigids[j].vel + rigids[j].omega * contactsEx[i].t1; // linear velocity of q
				float vnrel = -vrel.Dot(contact.n);

				Vector2 v12 = vrel; v12.Flip();
				Vector2 vt = v12 - contact.n * vnrel;
				float vtrel = vt.Length();
				if (vtrel != 0.0f)
				{
					float inertia = rigids[j].inertia * contactsEx[i].qxn * contactsEx[i].qxn;
					float dLambdaF = vtrel / (rigids[j].invMass + inertia); // FIXME

					float lambdaF0 = contact.lambdaF;
					contact.lambdaF = lambdaF0 + dLambdaF;
					const float limit = mu * contact.lambda;
					contact.lambdaF = clamp(contact.lambdaF, -limit, limit);
					dLambdaF = contact.lambdaF - lambdaF0;

					vt.Scale(dLambdaF / vtrel);
					rigids[j].vel += vt * rigids[j].invMass;
					Vector2 q = contact.q;
					q.Rotate(rigids[j].theta); // q in world space
					rigids[j].omega += rigids[j].inertia * q.Cross(vt);
				}
			}
			else
			{
				// contact between rigids
				int j1 = contact.idx1;
				Vector2 v1 = rigids[j1].vel + rigids[j1].omega * contactsEx[i].t1;

				int j2 = contact.idx2;
				Vector2 v2 = rigids[j2].vel + rigids[j2].omega * contactsEx[i].t2;

				Vector2 v12 = v2 - v1;
				float vnrel = contact.n.Dot(v12);
				Vector2 vt = v12 - contact.n * vnrel;
				float vtrel = vt.Length();
				if (vtrel != 0.0f)
				{
					float inertia1 = rigids[j1].inertia * contactsEx[i].qxn * contactsEx[i].qxn;
					float inertia2 = rigids[j2].inertia * contactsEx[i].pxn * contactsEx[i].pxn;
					float dLambdaF = vtrel / (rigids[j1].invMass + rigids[j2].invMass + inertia1 + inertia2); // FIXME

					float lambdaF0 = contact.lambdaF;
					contact.lambdaF = lambdaF0 + dLambdaF;
					const float limit = mu * contact.lambda;
					contact.lambdaF = clamp(contact.lambdaF, -limit, limit);
					dLambdaF = contact.lambdaF - lambdaF0;

					vt.Scale(dLambdaF / vtrel);
					rigids[j1].vel += vt * rigids[j1].invMass;
					Vector2 q = contact.q;
					q.Rotate(rigids[j1].theta); // q in world space
					rigids[j1].omega += rigids[j1].inertia * q.Cross(vt);

					rigids[j2].vel -= vt * rigids[j2].invMass;
					Vector2 p = contact.p;
					p.Rotate(rigids[j2].theta); // p in world space
					rigids[j2].omega -= rigids[j2].inertia * p.Cross(vt);
				}
			}
		}
		// contact
		for (size_t i = 0; i < contacts.size(); i++)
		{
			Contact& contact = contacts[i];
			if (contact.idx2 < 0)
			{
				// static contact
				int j = contact.idx1;
				Vector2 vrel = rigids[j].vel + rigids[j].omega * contactsEx[i].t1; // linear velocity of q
				float vnrel = -vrel.Dot(contact.n);
				float dLambda = (vnrel + contactsEx[i].stab) / (rigids[j].invMass + rigids[j].inertia * contactsEx[i].qxn * contactsEx[i].qxn);

				float l0 = contact.lambda;
				contact.lambda = max(0.f, l0 + dLambda);
				dLambda = contact.lambda - l0;

				rigids[j].vel += dLambda * contact.n;
				rigids[j].omega += rigids[j].inertia * dLambda * contactsEx[i].qxn;
			}
			else
			{
				// contact between rigids
				int j1 = contact.idx1;
				Vector2 v1 = rigids[j1].vel + rigids[j1].omega * contactsEx[i].t1;

				int j2 = contact.idx2;
				Vector2 v2 = rigids[j2].vel + rigids[j2].omega * contactsEx[i].t2;

				float vnrel = contact.n.Dot(v2 - v1);
				float inertia1 = rigids[j1].inertia * contactsEx[i].qxn * contactsEx[i].qxn;
				float inertia2 = rigids[j2].inertia * contactsEx[i].pxn * contactsEx[i].pxn;
				float dLambda = (vnrel + contactsEx[i].stab) / (rigids[j1].invMass + rigids[j2].invMass + inertia1 + inertia2);

				float l0 = contact.lambda;
				contact.lambda = max(0.f, l0 + dLambda);
				dLambda = contact.lambda - l0;

				rigids[j1].vel += rigids[j1].invMass * dLambda * contact.n;
				rigids[j1].omega += rigids[j1].inertia * dLambda * contactsEx[i].qxn;

				rigids[j2].vel -= rigids[j2].invMass * dLambda * contact.n;
				rigids[j2].omega -= rigids[j2].inertia * dLambda * contactsEx[i].pxn;
			}
		}
	}

	// integrate positions
	for (size_t i = 0; i < rigids.size(); i++)
	{
		rigids[i].pos += h * rigids[i].vel;
		rigids[i].theta += h * rigids[i].omega;
	}
}

#define HALF_PBD
void RigidSystem::StepPBD(float h)
{	
	// integrate velocities
	for (size_t i = 0; i < rigids.size(); i++)
	{
		//rigids[i].omega += h * torque; // external torque?
		rigids[i].pos0 = rigids[i].pos;
#ifdef HALF_PBD
		rigids[i].pos += h * rigids[i].vel;
#endif
		rigids[i].vel += h * gravity;
		rigids[i].theta0 = rigids[i].theta;
#ifndef HALF_PBD
		rigids[i].pos += h * rigids[i].vel;
#endif
		rigids[i].theta += h * rigids[i].omega;
	}

	Collide();

	// solver
	const int numIterations = 50;
	const float mu = 0.f;
	const float invH = 1.f / h;
	for (int k = 0; k < numIterations; k++)
	{
		for (size_t i = 0; i < contacts.size(); i++)
		{
			Contact& contact = contacts[i];
			if (contact.idx2 < 0)
			{
				int j = contact.idx1;
				Vector2 q = contact.q;
				q.Rotate(rigids[j].theta);
				float cross = q.Cross(contact.n);
				float dLambda = -contact.n.Dot(q + rigids[j].pos - contact.p) / (rigids[j].invMass + rigids[j].inertia * cross * cross);

				//if (dLambda < 0) continue;
				float l0 = contact.lambda;
				contact.lambda = max(0.f, l0 + dLambda);
				dLambda = contact.lambda - l0;

				rigids[j].pos += rigids[j].invMass * dLambda * contact.n;
				rigids[j].theta += rigids[j].inertia * dLambda * cross;

				rigids[j].vel += invH * rigids[j].invMass * dLambda * contact.n;
				rigids[j].omega += invH * rigids[j].inertia * dLambda * cross;

				// friction
				Vector2 t1 = q.GetPerpendicular();
				Vector2 vrel = rigids[j].vel + rigids[j].omega * t1; // linear velocity of q
				float vnrel = -vrel.Dot(contact.n);

				Vector2 v12 = vrel; v12.Flip();
				Vector2 vt = v12 - contact.n * vnrel;
				float vtrel = vt.Length();
				if (vtrel != 0.0f)
				{
					float inertia = rigids[j].inertia * cross * cross;
					float dLambdaF = vtrel / (rigids[j].invMass + inertia);

					float lambdaF0 = contact.lambdaF;
					contact.lambdaF = lambdaF0 + dLambdaF;
					const float limit = mu * contact.lambda * invH;
					contact.lambdaF = clamp(contact.lambdaF, -limit, limit);
					dLambdaF = contact.lambdaF - lambdaF0;

					vt.Scale(dLambdaF / vtrel);
					rigids[j].vel += vt * rigids[j].invMass;
					rigids[j].omega += rigids[j].inertia * q.Cross(vt);
					rigids[j].pos += h * vt * rigids[j].invMass;
					rigids[j].theta += h * rigids[j].inertia * q.Cross(vt);
				}
			}
			else
			{
				// contact between rigids
				int j1 = contact.idx1;
				int j2 = contact.idx2;				
				
				Vector2 q = contact.q;
				q.Rotate(rigids[j1].theta);
				float cross1 = q.Cross(contact.n);
				Vector2 t1 = q.GetPerpendicular();
				//q += rigids[j1].pos;

				Vector2 p = contact.p;
				p.Rotate(rigids[j2].theta);
				float cross2 = p.Cross(contact.n);
				Vector2 t2 = p.GetPerpendicular();
				//p += rigids[j2].pos;

				float inertia1 = rigids[j1].inertia * cross1 * cross1;
				float inertia2 = rigids[j2].inertia * cross2 * cross2;
				float inertia = rigids[j2].invMass + rigids[j2].invMass + inertia1 + inertia2;
				float dLambda = -contact.n.Dot(q + rigids[j1].pos - p - rigids[j2].pos) / inertia;

				//if (dLambda < 0) continue;
				float l0 = contact.lambda;
				contact.lambda = max(0.f, l0 + dLambda);
				dLambda = contact.lambda - l0;

				rigids[j1].pos += rigids[j1].invMass * dLambda * contact.n;
				rigids[j1].theta += rigids[j1].inertia * dLambda * cross1;

				rigids[j2].pos -= rigids[j2].invMass * dLambda * contact.n;
				rigids[j2].theta -= rigids[j2].inertia * dLambda * cross2;

				rigids[j1].vel += invH * rigids[j1].invMass * dLambda * contact.n;
				rigids[j1].omega += invH * rigids[j1].inertia * dLambda * cross1;

				rigids[j2].vel -= invH * rigids[j2].invMass * dLambda * contact.n;
				rigids[j2].omega -= invH * rigids[j2].inertia * dLambda * cross2;

				Vector2 v1 = rigids[j1].vel + rigids[j1].omega * t1;
				Vector2 v2 = rigids[j2].vel + rigids[j2].omega * t2;

				Vector2 v12 = v2 - v1;
				float vnrel = contact.n.Dot(v12);
				Vector2 vt = v12 - contact.n * vnrel;
				float vtrel = vt.Length();
				if (vtrel != 0.0f)
				{
					float dLambdaF = vtrel / inertia;

					float lambdaF0 = contact.lambdaF;
					contact.lambdaF = lambdaF0 + dLambdaF;
					const float limit = mu * contact.lambda * invH;
					contact.lambdaF = clamp(contact.lambdaF, -limit, limit);
					dLambdaF = contact.lambdaF - lambdaF0;

					vt.Scale(dLambdaF / vtrel);
					rigids[j1].vel += vt * rigids[j1].invMass;
					rigids[j1].omega += rigids[j1].inertia * q.Cross(vt);
					rigids[j1].pos += h * vt * rigids[j1].invMass;
					rigids[j1].theta += h * rigids[j1].inertia * q.Cross(vt);

					rigids[j2].vel -= vt * rigids[j2].invMass;
					rigids[j2].omega -= rigids[j2].inertia * p.Cross(vt);
					rigids[j2].pos -= h * vt * rigids[j2].invMass;
					rigids[j2].theta -= h * rigids[j2].inertia * p.Cross(vt);
				}
			}
		}
#ifdef HALF_PBD
		if (k == numIterations / 2)
		{
			for (size_t i = 0; i < rigids.size(); i++)
			{
				rigids[i].pos = rigids[i].pos0 + h * rigids[i].vel;
				//rigids[i].pos += h * h * gravity;
			}
		}
#endif
	}

	// update velocities
	//for (size_t i = 0; i < rigids.size(); i++)
	//{
	//	rigids[i].vel = invH * (rigids[i].pos - rigids[i].pos0);
	//	rigids[i].omega = invH * (rigids[i].theta - rigids[i].theta0);
	//}
}

//#define CONTACT_CACHING

//inline void RigidSystem::Collide()
//{
//	// the bottom line
//	const Vector2 nBottom(0, -1);
//	const Vector2 pBottom(0.f, Engine::getInstance()->getScreenHeight());
//
//	// the left line
//	const Vector2 nLeft(1, 0);
//	const Vector2 pLeft(0.f, Engine::getInstance()->getScreenHeight());
//
//	// the right line
//	const Vector2 nRight(-1, 0);
//	const Vector2 pRight(Engine::getInstance()->getScreenWidth(), Engine::getInstance()->getScreenHeight());
//
//	const float tol = 0.15f * r;
//	float rTol = r + tol;
//#ifndef CONTACT_CACHING
//	contacts.clear();
//#else
//	// remove inactive contacts
//	{
//		// inside a scope so the new vector is freed
//		std::vector<Contact> contacts1;
//		for (size_t i = 0; i < contacts.size(); i++)
//		{
//			Vector2 q = contacts[i].q;
//			if (contacts[i].idx2 < 0)
//			{
//				int j = contacts[i].idx1;
//				q.Rotate(rigids[j].theta);
//				q += rigids[j].pos;
//				float d = contacts[i].n.Dot(q - contacts[i].p);
//				if (d <= tol)
//				{
//					contacts[i].depth = -d;
//					contacts1.push_back(contacts[i]);
//				}
//			}
//			else
//			{
//				int j1 = contacts[i].idx1;
//				q.Rotate(rigids[j1].theta);
//				q += rigids[j1].pos;
//
//				Vector2 p = contacts[i].p;
//				int j2 = contacts[i].idx2;
//				p.Rotate(rigids[j2].theta);
//				p += rigids[j2].pos;
//
//				//Vector2 delta = q - p;
//				//float d = contacts[i].n.Dot(delta);
//				//float l2 = delta.Length();
//				//float dx = sqrtf(l2 - d * d);
//				
//				Vector2 a(-hh, 0.f);
//				a.Rotate(rigids[j1].theta);
//				a += rigids[j1].pos;
//				Vector2 b(hh, 0.f);
//				b.Rotate(rigids[j1].theta);
//				b += rigids[j1].pos;
//
//				Vector2 c(-hh, 0.f);
//				c.Rotate(rigids[j2].theta);
//				c += rigids[j2].pos;
//				Vector2 d(hh, 0.f);
//				d.Rotate(rigids[j2].theta);
//				d += rigids[j2].pos;
//
//				float t;
//				Vector2 p1 = ClosestPtSegm(q, c, d, t);
//				Vector2 q1 = ClosestPtSegm(p, a, b, t);
//				float d1 = (q - p1).Length();
//				float d2 = (p - q1).Length();
//				float dist = min(d1, d2) - r;
//				//if (d <= tol /*&& dx <= 10.0f*/)
//				if (dist <= 2 * tol)
//				{
//					contacts[i].depth = -dist;
//					if (d2 < d1)
//					{
//						Vector2 n1 = q1 - p;
//						n1.Normalize();
//						q1 = q1 - r * n1;
//						q1 -= rigids[j1].pos;
//						q1.Rotate(-rigids[j1].theta);
//						contacts[i].q = q1;
//						contacts[i].n = n1;
//					}
//					else
//					{
//						Vector2 n1 = q - p1;
//						n1.Normalize();
//						p1 = p1 + r * n1;
//						p1 -= rigids[j2].pos;
//						p1.Rotate(-rigids[j2].theta);
//						contacts[i].p = p1;
//						contacts[i].n = n1;
//					}
//					contacts1.push_back(contacts[i]);
//				}
//			}
//		}
//		contacts.swap(contacts1);
//	}
//#endif
//
//	Vector2 p1, p2;
//	for (size_t i = 0; i < rigids.size(); i++)
//	{
//		// wall collisions
//#ifdef CONTACT_CACHING
//		ClosestPoints closestPts;
//		if (CapsuleHalfPlaneCollision(hh, r, rTol, rigids[i].theta, rigids[i].pos, nBottom, pBottom, closestPts))
//		{
//			closestPts.id2 = CID_BOTTOM;
//			AddContact(i, closestPts);
//		}
//		if (CapsuleHalfPlaneCollision(hh, r, rTol, rigids[i].theta, rigids[i].pos, nRight, pRight, closestPts))
//		{
//			closestPts.id2 = CID_RIGHT;
//			AddContact(i, closestPts);
//		}
//		if (CapsuleHalfPlaneCollision(hh, r, rTol, rigids[i].theta, rigids[i].pos, nLeft, pLeft, closestPts))
//		{
//			closestPts.id2 = CID_LEFT;
//			AddContact(i, closestPts);
//		}
//#else
//		ClosestPtCapsuleLine(i, nBottom, pBottom, rTol);
//		ClosestPtCapsuleLine(i, nRight, pRight, rTol);
//		ClosestPtCapsuleLine(i, nLeft, pLeft, rTol);
//		//ClosestPtCircleLine(i, n, p, rTol);
//#endif
//
//		for (size_t j = i + 1; j < rigids.size(); j++)
//		{
//#ifdef CONTACT_CACHING
//			// TODO: don't recalculate world coordinates
//			Vector2 a(-hh, 0.f);
//			a.Rotate(rigids[i].theta);
//			a += rigids[i].pos;
//			Vector2 b(hh, 0.f);
//			b.Rotate(rigids[i].theta);
//			b += rigids[i].pos;
//
//			Vector2 c(-hh, 0.f);
//			c.Rotate(rigids[j].theta);
//			c += rigids[j].pos;
//			Vector2 d(hh, 0.f);
//			d.Rotate(rigids[j].theta);
//			d += rigids[j].pos;
//
//			float s, t;
//			float dist = ClosestPtSegmSegm(a, b, c, d, p1, p2, s, t);
//			if (dist > 0 && dist <= 2 * rTol)
//			{
//				Vector2 n = p1 - p2;
//				n.Normalize();
//				p1 -= rigids[i].pos + r * n;
//				p1.Rotate(-rigids[i].theta);
//				p2 -= rigids[j].pos - r * n;
//				p2.Rotate(-rigids[j].theta);
//
//				Contact contact;
//				contact.n = n;
//				contact.q = p1;
//				contact.p = p2;
//				contact.idx1 = i;
//				contact.idx2 = j;
//				contact.depth = 2 * r - dist;
//				contact.lambda = 0.f;
//				//contact.id1 = s == 1.f ? CID_LEFT : s == 0.f ? CID_RIGHT : CID_MIDDLE;
//				//contact.id2 = t == 1.f ? CID_LEFT : t == 0.f ? CID_RIGHT : CID_MIDDLE;
//				contact.id1 = s < 0.5f ? CID_LEFT : CID_RIGHT;
//				contact.id2 = t < 0.5f ? CID_LEFT : CID_RIGHT;
//
//				// TODO: binary search
//				bool found = false;
//				for (size_t k = 0; k < contacts.size(); k++)
//				{
//					if (contacts[k].idx1 == i && contacts[k].idx2 == j &&
//						contacts[k].id1 == contact.id1 && contacts[k].id2 == contact.id2)
//					{
//						contacts[k] = contact;
//						found = true;
//						break;
//					}
//				}
//
//				if (!found)
//				{
//					contacts.push_back(contact);
//				}
//			}
//#else
//			ClosestPtCapsuleCapsule(i, j, rTol);
//#endif
//		}
//	}
//}

void RigidSystem::Collide()
{
	// the bottom line
	const Vector2 nBottom(0, -1);
	const Vector2 pBottom(0.f, Engine::getInstance()->getScreenHeight());

	// the left line
	const Vector2 nLeft(1, 0);
	const Vector2 pLeft(0.f, Engine::getInstance()->getScreenHeight());

	// the right line
	const Vector2 nRight(-1, 0);
	const Vector2 pRight(Engine::getInstance()->getScreenWidth(), Engine::getInstance()->getScreenHeight());

	const float tol = 4; // TODO: take from file
	contacts.clear();

	Vector2 p1, p2;
	for (size_t i = 0; i < rigids.size(); i++)
	{
		const Shape* shape = (const Shape*)rigids[i].shape.get();
		if (shape->type == Shape::CAPSULE)
		{
			const Capsule* cap = (const Capsule*)shape;
			float rTol = cap->r + tol;
			// wall collisions
			ClosestPtCapsuleLine(i, nBottom, pBottom, rTol);
			ClosestPtCapsuleLine(i, nRight, pRight, rTol);
			ClosestPtCapsuleLine(i, nLeft, pLeft, rTol);

			for (size_t j = i + 1; j < rigids.size(); j++)
			{
				ClosestPtCapsuleCapsule(i, j, tol);
			}
		}
		else if (shape->type == Shape::SPHERE)
		{
			const Sphere* sph = (const Sphere*)shape;
			float rTol = sph->r + tol;
			// wall collisions
			ClosestPtCircleLine(i, nBottom, pBottom, rTol, sph->r);
			ClosestPtCircleLine(i, nRight, pRight, rTol, sph->r);
			ClosestPtCircleLine(i, nLeft, pLeft, rTol, sph->r);

			for (size_t j = i + 1; j < rigids.size(); j++)
			{
				ClosestPtCircleCircle(i, j, tol);
			}
		}
	}
}

inline void RigidSystem::AddCapsuleContact(Vector2 p, Vector2 q, Vector2 n, int i, int j, float safeDist, float dist, float s, float t)
{
	if (dist > safeDist) // TODO: square
		return;
	ASSERT(n.LengthSquared() != 0);
	Vector2 normal = n;
	normal.Normalize();

	float tol = 0.00001f;
	Vector2 n1, n2;
	n1 = n2 = normal;

	const Capsule& cap1 = *(const Capsule*)rigids[i].shape.get();
	const Capsule& cap2 = *(const Capsule*)rigids[j].shape.get();

	// TODO: do not recompute endpoints
	Vector2 a(-cap1.hh, 0.f);
	a.Rotate(rigids[i].theta);
	a += rigids[i].pos;
	Vector2 b(cap1.hh, 0.f);
	b.Rotate(rigids[i].theta);
	b += rigids[i].pos;
	Vector2 ab = b - a;

	float dot1 = n.Dot(ab);
	bool inside1 = (s > tol && s < 1 - tol) || (dot1 <= 0 && s < tol) || (dot1 > 0 && s > 1 - tol);
	if (inside1)
	{
		n1 = ab.GetPerpendicular();
		if (n1.Dot(n) < 0) n1.Flip();
		n1.Normalize();
		//p -= r * n1;
		float s = dist / n1.Dot(n);
		p -= s * cap1.r * normal;
		//normal = n1;
	}
	else
	{
		p -= cap1.r * normal;
	}

	Vector2 c(-cap2.hh, 0.f);
	c.Rotate(rigids[j].theta);
	c += rigids[j].pos;
	Vector2 d(cap2.hh, 0.f);
	d.Rotate(rigids[j].theta);
	d += rigids[j].pos;
	Vector2 cd = d - c;
	float dot2 = n.Dot(cd);
	bool inside2 = (t > tol && t < 1 - tol) || (dot2 >= 0 && t < tol) || (dot2 < 0 && t > 1 - tol);

	if (inside2)
	{
		n2 = cd.GetPerpendicular();
		if (n2.Dot(n) < 0) n2.Flip();
		n2.Normalize();
		//q += r * n2;
		float s = dist / n2.Dot(n);
		q += s * cap2.r * normal;
		//normal = n2;
	}
	else
	{
		q += cap2.r * normal;
	}

	if (inside1 && inside2)
		normal = n1;
	//if (inside1)
	//	normal = n1;
	//if (inside2)
	//	normal = n2;
	//normal = n1 + n2;
	//normal.Normalize();

	float dist1 = normal.Dot(p - q);
	//if (dist1 > 0)
		//return;
	q -= rigids[j].pos;
	q.Rotate(-rigids[j].theta);
	p -= rigids[i].pos;
	p.Rotate(-rigids[i].theta);

	RigidSystem::Contact contact;
	contact.n = normal;
	contact.q = p; // FIXME!
	contact.p = q;
	contact.idx1 = i;
	contact.idx2 = j;
	contact.depth = -dist1; //2 * r - dist;
	contact.lambda = 0.f;
	contacts.push_back(contact);
}

void RigidSystem::ClosestPtCapsuleCapsule(int i, int j, float tol)
{
	const Capsule& cap1 = *(const Capsule*)rigids[i].shape.get();
	const Capsule& cap2 = *(const Capsule*)rigids[j].shape.get();

	// TODO: get segment from capsule struct
	Vector2 a(-cap1.hh, 0.f);
	a.Rotate(rigids[i].theta);
	a += rigids[i].pos;
	Vector2 b(cap1.hh, 0.f);
	b.Rotate(rigids[i].theta);
	b += rigids[i].pos;
	
	Vector2 c(-cap2.hh, 0.f);
	c.Rotate(rigids[j].theta);
	c += rigids[j].pos;
	Vector2 d(cap2.hh, 0.f);
	d.Rotate(rigids[j].theta);
	d += rigids[j].pos;

	Vector2 p, q, n;
	float s, t;
	float safeDist = cap1.r + cap2.r + 2 * tol; // or just tol?
	//const float r = rigids[i].cap.r;

	{
		p = b; q = c;
		n = p - q;
		float dist = n.Length();
		AddCapsuleContact(p, q, n, i, j, safeDist, dist, 1, 0);

		p = a; q = d;
		n = p - q;
		dist = n.Length();
		AddCapsuleContact(p, q, n, i, j, safeDist, dist, 0, 1);

		p = a; q = c;
		n = p - q;
		dist = n.Length();
		AddCapsuleContact(p, q, n, i, j, safeDist, dist, 0, 0);

		p = b; q = d;
		n = p - q;
		dist = n.Length();
		AddCapsuleContact(p, q, n, i, j, safeDist, dist, 1, 1);
	}

	Vector2 ab = b - a;
	Vector2 cd = d - c;
	float eps = 0.01f; // parallel test
	float m11 = ab.LengthSquared();
	float m22 = cd.LengthSquared();
	float m12 = ab.Dot(cd);
	float det = m11 * m22 - m12 * m12;
	float cross = ab.Cross(cd) / sqrt(m11 * m22);;
	if (fabs(cross) < eps || fabs(det) < eps)
	{
		float t1;
		Vector2 p1 = ClosestPtLine(c, a, b, s);
		Vector2 q1 = ClosestPtLine(b, c, d, t1);
		//float dab = (c - p1).LengthSquared();
		//float dcd = (b - q1).LengthSquared();
		//float tolCol = 0.1f;
		//if (dab < tolCol || dcd < tolCol)
		//float area1 = fabs(Orient2D(a, c, d));
		//float area2 = fabs(Orient2D(b, c, d));
		//float area3 = fabs(Orient2D(c, a, b));
		//float area4 = fabs(Orient2D(d, a, b));
		//float tol1 = cd.Length() * 0.01f;
		//float tol2 = ab.Length() * 0.01f;
		//if (area1 < tol1 || area2 < tol1 || area3 < tol2 || area4 < tol2)

		// parallel
		float dist;
		float tolB = 0.01f;
		p = p1;
		q = c;
		t = 0;
		if (s < -tolB || s > 1 + tolB)
		{
			p = ClosestPtLine(d, a, b, s);
			q = d;
			t = 1;
		}		
		if (s >= -tolB && s <= 1 + tolB)
		{
			n = p - q;
			dist = n.Length();
			AddCapsuleContact(p, q, n, i, j, safeDist, dist, s, t);
		}

		p = b;
		s = 1;
		q = q1;
		t = t1;
		if (t < -tolB || t > 1 + tolB)
		{
			p = a;
			s = 0;
			q = ClosestPtLine(a, c, d, t);
		}
		if (t >= -tolB && t <= 1 + tolB)
		{
			n = p - q;
			dist = n.Length();
			AddCapsuleContact(p, q, n, i, j, safeDist, dist, s, t);
		}
		return;
	}

	Vector2 ac = c - a;
	float rhs1 = ac.Dot(ab);
	float rhs2 = -ac.Dot(cd);
	s = (m22 * rhs1 + m12 * rhs2) / det;
	t = (m12 * rhs1 + m11 * rhs2) / det;
	
	float s1 = clamp(s, 0.f, 1.f);
	float ds = fabs(s - s1);
	float t1 = clamp(t, 0.f, 1.f);
	float dt = fabs(t - t1);

	//Vector2 p, q;
	if (ds > dt)
	{
		p = a + s1 * ab;
		q = ClosestPtSegm(p, c, d, t1);
	}
	else
	{
		q = c + t1 * cd;
		p = ClosestPtSegm(q, a, b, s1);
	}

	n = p - q;
	float dist = n.Length();
	AddCapsuleContact(p, q, n, i, j, safeDist, dist, s1, t1);
}

//void RigidSystem::ClosestPtCapsuleCapsule(int i, int j, float rTol)
//{
//	Vector2 a(-hh, 0.f);
//	a.Rotate(rigids[i].theta);
//	a += rigids[i].pos;
//	Vector2 b(hh, 0.f);
//	b.Rotate(rigids[i].theta);
//	b += rigids[i].pos;
//
//	Vector2 c(-hh, 0.f);
//	c.Rotate(rigids[j].theta);
//	c += rigids[j].pos;
//	Vector2 d(hh, 0.f);
//	d.Rotate(rigids[j].theta);
//	d += rigids[j].pos;
//
//	float t;
//	Vector2 pi;
//	if (TestSegmSegm(a, b, c, d, t, pi))
//	{
//		return; // do nothing
//	}
//
//	float t1, t2;
//	Vector2 c1 = ClosestPtSegm(c, a, b, t1);
//	Vector2 d1 = ClosestPtSegm(d, a, b, t2);
//	Vector2 p = d1;
//	float s = t2;
//	Vector2 q = ClosestPtSegm(p, c, d, t);
//	Vector2 pp = c1;
//	float ss = t1;
//	Vector2 qq = ClosestPtSegm(pp, c, d, t);
//	if ((pp - qq).LengthSquared() < (p - q).LengthSquared())
//	{
//		std::swap(p, pp);
//		std::swap(q, qq);
//		std::swap(s, ss);
//	}
//
//	Vector2 x21 = b - a;
//	Vector2 x43 = d - c;
//	const float eps = 0.0001f;
//	//float s, t;
//	//Vector2 p, q;
//	//float cross = fabs(x21.Cross(x43));
//	////if (cross < eps)
//	////{
//	////	p = a; s = 0;
//	////	//q = x3; t = 0;
//	////	q = ClosestPtSegm(p, c, d, t);
//	////	p = ClosestPtSegm(q, a, b, s);
//	////	return (q - p).Length();
//	////}
//	Vector2 x31 = c - a;
//	float dot1 = x21.LengthSquared();
//	float dot2 = x21.Dot(x43);
//	float dot3 = x43.LengthSquared();
//	//float dot4 = x21.Dot(x31);
//	//float dot5 = -x43.Dot(x31);
//	float det = dot1 * dot3 - dot2 * dot2;
//	//// TODO: det == 0 - parallel case?
//	////ASSERT(det != 0);
//	////if (det == 0.f)
//	////{
//	////	p = x1; s = 0;
//	////	//q = x3; t = 0;
//	////	q = ClosestPtSegm(p, x3, x4, t);
//	////	p = ClosestPtSegm(q, x1, x2, s);
//	////	return (q - p).Length();
//	////}
//	//s = (dot3 * dot4 + dot2 * dot5) / det;
//	//t = (dot2 * dot4 + dot1 * dot4) / det;
//	//float s1 = clamp(s, 0.f, 1.f);
//	//float t1 = clamp(t, 0.f, 1.f);
//	//float ds = fabs(s1 - s);
//	//float dt = fabs(t1 - t);
//	//s = s1;
//	//t = t1;
//	//if (ds > dt)
//	//{
//	//	p = x1 + s * x21;
//	//	q = ClosestPtSegm(p, x3, x4, t);
//	//}
//	//else
//	//{
//	//	q = x3 + t * x43;
//	//	p = ClosestPtSegm(q, x1, x2, s);
//	//}
//
//	float dist = (p - q).Length(); // TODO: optimize for normal
//
//	if (dist > 2 * rTol) // TODO: square?
//		return;
//
//	Vector2 n;
//
//	Vector2 ab = b - a; ab.Normalize();
//	Vector2 cd = d - c; cd.Normalize();
//	n = -1.f * cd.GetPerpendicular();
//	// test if parallel
//	bool parallel = fabs(ab.Cross(cd)) < 0.1f || det < eps;
//	if (!parallel)
//	{
//		n = p - q;
//		n.Normalize();
//	}
//
//	p -= rigids[i].pos + r * n;
//	p.Rotate(-rigids[i].theta);
//	q -= rigids[j].pos - r * n;
//	q.Rotate(-rigids[j].theta);
//
//	Contact contact;
//	contact.n = n;
//	contact.q = p; // FIXME!
//	contact.p = q;
//	contact.idx1 = i;
//	contact.idx2 = j;
//	contact.depth = 2 * r - dist;
//	contact.lambda = 0.f;
//	contacts.push_back(contact);
//
//	if (!parallel)
//		return;
//
//	pp -= rigids[i].pos + r * n;
//	pp.Rotate(-rigids[i].theta);
//	qq -= rigids[j].pos - r * n;
//	qq.Rotate(-rigids[j].theta);
//
//	//contact.n = n;
//	contact.q = pp; // FIXME!
//	contact.p = qq;
//	//contact.idx1 = i;
//	//contact.idx2 = j;
//	//contact.depth = 2 * r - dist;
//	//contact.lambda = 0.f;
//	contacts.push_back(contact);
//}

