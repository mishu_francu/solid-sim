#ifndef PARTICLE_SYSTEM_GRAVITY_H
#define PARTICLE_SYSTEM_GRAVITY_H

#include "ParticleSystemVelocity.h"

template <int INTEGRATOR>
class ParticleSystemGravity : public ParticleSystemVelocity
{
public:
	ParticleSystemGravity() 
	{
		SetNumMicroSteps(1);
	}

	const char* GetName() const { return "Gravity"; }
	void MicroStep();

private:
	void Integrate();
};

#endif // PARTICLE_SYSTEM_GRAVITY_H
