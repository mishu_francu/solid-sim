#include "ParticleSystemNonlinear.h"

void ParticleSystemNonlinear::MicroStep3()
{
	int limit = numIterations / 2;
	// unconstrained step
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += timeStep * gravity * lengthScale;
		particles[i].prevPos = particles[i].pos;
		if (limit < 0)
			particles[i].pos += timeStep * particles[i].velocity;
	}

	Collide();

	// precompute velocity error
	for (size_t i = 0; i < constraints.size(); i++)
	{
		constraints[i].lambda = 0;
		int i1 = constraints[i].i1;
		if (constraints[i].type == Constraint::CONTACT)
		{
			constraints[i].err = particles[i1].velocity.Dot(constraints[i].normal);
		}
		else
		{
			int i2 = constraints[i].i2;
			Vector2 delta = particles[i1].pos - particles[i2].pos;
			delta.Normalize();
			Vector2 vrel = particles[i1].velocity - particles[i2].velocity;
			constraints[i].err = delta.Dot(vrel);
		}
	}

	// outer Newton iterations
	std::vector<Vector2> acc(particles.size());
	for (int k = 0; k < numIterations; k++)
	{
		std::fill(acc.begin(), acc.end(), Vector2(0));
		// precompute terms (rhs)
		for (size_t i = 0; i < constraints.size(); i++)
		{
			int i1 = constraints[i].i1;
			if (constraints[i].type == Constraint::CONTACT)
			{
				constraints[i].depth = constraints[i].normal.Dot(particles[i1].pos - constraints[i].point) - constraints[i].len;
			}
			else
			{
				int i2 = constraints[i].i2;
				Vector2 delta = particles[i1].pos - particles[i2].pos;
				constraints[i].depth = delta.Length() - constraints[i].len;
				delta.Normalize();
				constraints[i].normal = delta;
			}
			if (k <= limit)
				constraints[i].depth += timeStep * constraints[i].err;
		}
		// Gauss-Seidel iterations
		for (int l = 0; l < 4; l++)
		{
			for (size_t i = 0; i < constraints.size(); i++)
			{
				if (constraints[i].type == Constraint::CONTACT)
				{
					int i1 = constraints[i].i1;
					float res = acc[i1].Dot(constraints[i].normal) + constraints[i].depth;
					//float res = timeStep * particles[i1].velocity.Dot(constraints[i].normal) + constraints[i].depth;
					float dLambda = -res;
					float lambda0 = constraints[i].lambda;
					constraints[i].lambda = lambda0 + dLambda;
					if (constraints[i].lambda < 0.f)
						constraints[i].lambda = 0.f;
					dLambda = constraints[i].lambda - lambda0;
					
					constraints[i].disp = constraints[i].normal * dLambda;
					acc[i1] += constraints[i].disp;
					particles[i1].pos += constraints[i].disp;
					particles[i1].velocity += (1.f / timeStep) * constraints[i].disp;
				}
				else
				{
					int i1 = constraints[i].i1;
					int i2 = constraints[i].i2;
					Vector2 delta = acc[i1] - acc[i2];
					//Vector2 delta = timeStep * (particles[i1].velocity - particles[i2].velocity);
					float res = delta.Dot(constraints[i].normal) + constraints[i].depth;
					float dLambda = -res / (particles[i1].invMass + particles[i2].invMass);

					if (constraints[i].type == Constraint::COLL_PAIR)
					{
						float lambda0 = constraints[i].lambda;
						constraints[i].lambda = lambda0 + dLambda;
						if (constraints[i].lambda < 0.f)
							constraints[i].lambda = 0.f;
						dLambda = constraints[i].lambda - lambda0;
					}

					constraints[i].disp = constraints[i].normal * dLambda;
					acc[i1] += particles[i1].invMass * constraints[i].disp;
					acc[i2] -= particles[i2].invMass * constraints[i].disp;

					// TODO: use acc instead at the end
					particles[i1].pos += particles[i1].invMass * constraints[i].disp;
					particles[i2].pos -= particles[i2].invMass * constraints[i].disp;
					particles[i1].velocity += (particles[i1].invMass / timeStep) * constraints[i].disp;
					particles[i2].velocity -= (particles[i2].invMass / timeStep) * constraints[i].disp;
				}
			}
		}
		if (k == limit)
		{
			// add prev vel and gravity
			for (size_t i = 0; i < particles.size(); i++)
			{
				if (particles[i].invMass == 0)
					continue;
				particles[i].pos = particles[i].prevPos + timeStep * particles[i].velocity;
			}
		}
	}

	PrintMetrics();
}

// PBD
void ParticleSystemNonlinear::MicroStep2()
{
	// unconstrained step
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += timeStep * gravity * lengthScale;
		particles[i].prevPos = particles[i].pos;
		particles[i].pos += timeStep * particles[i].velocity;
	}

	Collide();

	for (size_t i = 0; i < constraints.size(); i++)
	{
		constraints[i].lambda = 0;
	}

	float dampRatio = 0.5f;
	float a = 1.f / (1.f + dampRatio);
	float b = dampRatio * timeStep * a;
	for (int k = 0; k < numIterations; k++)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			if (constraints[i].type == Constraint::CONTACT)
			{
				int i1 = constraints[i].i1;
				float res = constraints[i].normal.Dot(particles[i1].pos - constraints[i].point) - constraints[i].len;
				float dLambda = -res;
				float lambda0 = constraints[i].lambda;
				constraints[i].lambda = lambda0 + dLambda;
				if (constraints[i].lambda < 0.f)
					constraints[i].lambda = 0.f;
				dLambda = constraints[i].lambda - lambda0;

				constraints[i].disp = constraints[i].normal * dLambda;
				particles[i1].pos += constraints[i].disp;
				particles[i1].velocity += (1.f / timeStep) * constraints[i].disp;
			}
			else
			{
				int i1 = constraints[i].i1;
				int i2 = constraints[i].i2;
				Vector2 delta = particles[i1].pos - particles[i2].pos;
				float res = delta.Length() - constraints[i].len;
				delta.Normalize();
				constraints[i].normal = delta;
				Vector2 vrel = particles[i1].velocity - particles[i2].velocity;
				res = a * res + b * vrel.Dot(delta);
				float dLambda = -res / (particles[i1].invMass + particles[i2].invMass);

				if (constraints[i].type == Constraint::COLL_PAIR)
				{
					float lambda0 = constraints[i].lambda;
					constraints[i].lambda = lambda0 + dLambda;
					if (constraints[i].lambda < 0.f)
						constraints[i].lambda = 0.f;
					dLambda = constraints[i].lambda - lambda0;
				}

				constraints[i].disp = constraints[i].normal * dLambda;

				particles[i1].pos += particles[i1].invMass * constraints[i].disp;
				particles[i2].pos -= particles[i2].invMass * constraints[i].disp;
				particles[i1].velocity += (particles[i1].invMass / timeStep) * constraints[i].disp;
				particles[i2].velocity -= (particles[i2].invMass / timeStep) * constraints[i].disp;
			}
		}
	}

	PrintMetrics();
}

void ParticleSystemNonlinear::MicroStep1()
{
	Collide();

	// unconstrained step
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += timeStep * gravity * lengthScale;
		particles[i].prevPos = particles[i].pos;
		particles[i].pos += timeStep * particles[i].velocity;
	}

	// velocity filter only
	for (int k = 0; k < numIterations; k++)
	{
		for (size_t i = 0; i < links.size(); i++)
		{
			int i1 = links[i].i1;
			int i2 = links[i].i2;
			Vector2 delta = particles[i1].pos - particles[i2].pos;
			float res = delta.Length() - links[i].len;
			delta.Normalize();
			links[i].normal = delta;
			float dLambda = -res / (particles[i1].invMass + particles[i2].invMass);
			links[i].disp = links[i].normal * (dLambda * links[i].stiffness);

			particles[i1].pos += particles[i1].invMass * links[i].disp;
			particles[i2].pos -= particles[i2].invMass * links[i].disp;
			particles[i1].velocity += (particles[i1].invMass / timeStep) * links[i].disp;
			particles[i2].velocity -= (particles[i2].invMass / timeStep) * links[i].disp;
		}
	}

	for (size_t i = 0; i < constraints.size(); i++)
	{
		constraints[i].lambda = 0;
	}

	// contacts only
	for (size_t i = 0; i < constraints.size(); i++)
	{
		int i1 = constraints[i].i1;
		if (constraints[i].type == Constraint::CONTACT)
		{
			constraints[i].depth = constraints[i].normal.Dot(particles[i1].prevPos - constraints[i].point) - constraints[i].len;
		}
		else if (constraints[i].type == Constraint::COLL_PAIR)
		{
			int i2 = constraints[i].i2;
			Vector2 delta = particles[i1].prevPos - particles[i2].prevPos;
			constraints[i].depth = delta.Length() - constraints[i].len;
			delta.Normalize();
			constraints[i].normal = delta;
		}
	}
	// Gauss-Seidel iterations
	for (int l = 0; l < numIterations; l++)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			if (constraints[i].type == Constraint::CONTACT)
			{
				int i1 = constraints[i].i1;
				float res = timeStep * particles[i1].velocity.Dot(constraints[i].normal) + constraints[i].depth;
				float dLambda = -res;
				float lambda0 = constraints[i].lambda;
				constraints[i].lambda = lambda0 + dLambda;
				if (constraints[i].lambda < 0.f)
					constraints[i].lambda = 0.f;
				dLambda = constraints[i].lambda - lambda0;

				constraints[i].disp = constraints[i].normal * dLambda;
				//particles[i1].pos += constraints[i].disp;
				particles[i1].velocity += (1.f / timeStep) * constraints[i].disp;
			}			
			else if (constraints[i].type == Constraint::COLL_PAIR)
			{
				int i1 = constraints[i].i1;
				int i2 = constraints[i].i2;
				Vector2 delta = timeStep * (particles[i1].velocity - particles[i2].velocity);
				float res = delta.Dot(constraints[i].normal) + constraints[i].depth;
				float dLambda = -res / (particles[i1].invMass + particles[i2].invMass);

				float lambda0 = constraints[i].lambda;
				constraints[i].lambda = lambda0 + dLambda;
				if (constraints[i].lambda < 0.f)
					constraints[i].lambda = 0.f;
				dLambda = constraints[i].lambda - lambda0;

				constraints[i].disp = constraints[i].normal * dLambda;

				// TODO: use acc instead at the end
				//particles[i1].pos += particles[i1].invMass * constraints[i].disp;
				//particles[i2].pos -= particles[i2].invMass * constraints[i].disp;
				particles[i1].velocity += (particles[i1].invMass / timeStep) * constraints[i].disp;
				particles[i2].velocity -= (particles[i2].invMass / timeStep) * constraints[i].disp;
			}
		}
	}

	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].pos = particles[i].prevPos + timeStep * particles[i].velocity;
	}

	//// outer Newton iterations - links only
	//std::vector<Vector2> acc(particles.size());
	//for (int k = 0; k < numIterations; k++)
	//{
	//	std::fill(acc.begin(), acc.end(), Vector2(0));
	//	// precompute terms (rhs)
	//	for (size_t i = 0; i < constraints.size(); i++)
	//	{
	//		if (constraints[i].type != Constraint::LINK)
	//			continue;
	//		int i1 = constraints[i].i1;
	//		int i2 = constraints[i].i2;
	//		Vector2 delta = particles[i1].pos - particles[i2].pos;
	//		constraints[i].depth = delta.Length() - constraints[i].len;
	//		delta.Normalize();
	//		constraints[i].normal = delta;
	//		if (k <= limit)
	//			constraints[i].depth += timeStep * constraints[i].err;
	//	}
	//	// Gauss-Seidel iterations
	//	for (int l = 0; l < 10; l++)
	//	{
	//		for (size_t i = 0; i < constraints.size(); i++)
	//		{
	//			if (constraints[i].type == Constraint::LINK)
	//			{
	//				int i1 = constraints[i].i1;
	//				int i2 = constraints[i].i2;
	//				Vector2 delta = acc[i1] - acc[i2];
	//				//Vector2 delta = timeStep * (particles[i1].velocity - particles[i2].velocity);
	//				float res = delta.Dot(constraints[i].normal) + constraints[i].depth;
	//				float dLambda = -res / (particles[i1].invMass + particles[i2].invMass);

	//				if (constraints[i].type == Constraint::COLL_PAIR)
	//				{
	//					float lambda0 = constraints[i].lambda;
	//					constraints[i].lambda = lambda0 + dLambda;
	//					if (constraints[i].lambda < 0.f)
	//						constraints[i].lambda = 0.f;
	//					dLambda = constraints[i].lambda - lambda0;
	//				}

	//				constraints[i].disp = constraints[i].normal * dLambda;
	//				acc[i1] += particles[i1].invMass * constraints[i].disp;
	//				acc[i2] -= particles[i2].invMass * constraints[i].disp;

	//				// TODO: use acc instead at the end
	//				particles[i1].pos += particles[i1].invMass * constraints[i].disp;
	//				particles[i2].pos -= particles[i2].invMass * constraints[i].disp;
	//				particles[i1].velocity += (particles[i1].invMass / timeStep) * constraints[i].disp;
	//				particles[i2].velocity -= (particles[i2].invMass / timeStep) * constraints[i].disp;
	//			}
	//		}
	//	}
	//	if (k == limit)
	//	{
	//		// add prev vel and gravity
	//		for (size_t i = 0; i < particles.size(); i++)
	//		{
	//			if (particles[i].invMass == 0)
	//				continue;
	//			particles[i].pos = particles[i].prevPos + timeStep * particles[i].velocity;
	//		}
	//	}
	//}

	PrintMetrics();
}

void ParticleSystemNonlinear::MicroStep()
{
	// unconstrained step
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += timeStep * gravity * lengthScale;
		particles[i].prevPos = particles[i].pos;
		//particles[i].pos += timeStep * particles[i].velocity;
	}

	Collide();

	// precompute terms (rhs)
	for (size_t i = 0; i < constraints.size(); i++)
	{
		int i1 = constraints[i].i1;
		constraints[i].lambda = 0;
		if (constraints[i].type == Constraint::CONTACT)
		{
			constraints[i].depth = (constraints[i].normal.Dot(particles[i1].pos - constraints[i].point) - constraints[i].len) / timeStep;
		}
		else
		{
			int i2 = constraints[i].i2;
			Vector2 delta = particles[i1].pos - particles[i2].pos;
			if (constraints[i].type == Constraint::COLL_PAIR)
			{
				constraints[i].depth = (delta.Length() - constraints[i].len) / timeStep;
				delta.Normalize();
			}
			else
				constraints[i].depth = 0.5f * (delta.LengthSquared() - constraints[i].len * constraints[i].len) / timeStep;
			constraints[i].normal = delta;
		}
	}
	// Gauss-Seidel iterations
	for (int l = 0; l < numIterations; l++)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			if (constraints[i].type == Constraint::CONTACT)
			{
				int i1 = constraints[i].i1;
				float res = particles[i1].velocity.Dot(constraints[i].normal) + constraints[i].depth;
				float dLambda = -res;
				float lambda0 = constraints[i].lambda;
				constraints[i].lambda = lambda0 + dLambda;
				if (constraints[i].lambda < 0.f)
					constraints[i].lambda = 0.f;
				dLambda = constraints[i].lambda - lambda0;

				constraints[i].disp = constraints[i].normal * dLambda;
				particles[i1].velocity += constraints[i].disp;
			}
			else
			{
				int i1 = constraints[i].i1;
				int i2 = constraints[i].i2;
				Vector2 v12 = particles[i1].velocity - particles[i2].velocity;
				float res = v12.Dot(constraints[i].normal) + constraints[i].depth;
				Vector2 w = constraints[i].normal;
				if (constraints[i].type == Constraint::LINK)
					w += (0.5f * timeStep) * v12;
				float mod = w.Dot(constraints[i].normal);
				float dLambda = -res / (particles[i1].invMass + particles[i2].invMass) / mod;

				if (constraints[i].type == Constraint::COLL_PAIR)
				{
					float lambda0 = constraints[i].lambda;
					constraints[i].lambda = lambda0 + dLambda;
					if (constraints[i].lambda < 0.f)
						constraints[i].lambda = 0.f;
					dLambda = constraints[i].lambda - lambda0;
				}

				constraints[i].disp = w * dLambda;

				particles[i1].velocity += particles[i1].invMass * constraints[i].disp;
				particles[i2].velocity -= particles[i2].invMass * constraints[i].disp;
			}
		}
	}

	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].pos = particles[i].prevPos + timeStep * particles[i].velocity;
	}
}

bool ParticleSystemNonlinear::SupportsSolver(SolverType aSolver) 
{ 
	return aSolver == GAUSS_SEIDEL; 
}

