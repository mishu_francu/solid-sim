#ifndef PARTICLE_SYSTEM_SP_H
#define PARTICLE_SYSTEM_SP_H

#include "ParticleSystem.h"

class ParticleSystemSeqPos : public ParticleSystem<ParticleVelocity>
{
public:
	//ParticleSystemSeqPos() { }
	void MicroStep() override;
	bool SupportsSolver(SolverType aSolver) override;

private:
	void Integrate();
	void ProjectVelocities();
	void ProjectVelocitiesCR(float alpha);

};

#endif // PARTICLE_SYSTEM_SP_H
