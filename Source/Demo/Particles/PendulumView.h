#ifndef PENDULUM_VIEW_H
#define PENDULUM_VIEW_H

#include "Pendulum.h"
#include "DoublePendulum.h"
#include <vector>

const float pendulumStep = 0.01f;

class Graphics2D;

typedef PendulumVelocity<float, Integrators::EULER> PendulumType1;
typedef PendulumExact<double> PendulumType2;

typedef DblPendulumProj<float, Integrators::VERLET> DblPendType1;
typedef DoublePendulum<double, Integrators::RK4> DblPendType2;

template<typename PendulumType>
class PendulumView
{
public:
	PendulumView(float len, float t0, float v0, float dt) 
		: pendulum(len, t0, v0, pendulumStep)
		, time(0)
		, step(dt)
	{
	}

	void Step()
	{
		time += step;
		if (time < pendulumStep)
			return;
		time -= pendulumStep;
		thetas.push_back(pendulum.GetTheta());
		vels.push_back(pendulum.GetVel());
		pendulum.Step(pendulumStep);
	}

	void Draw(Graphics2D* g);

//private:
	PendulumType pendulum;
	std::vector<typename PendulumType::RealType> thetas, vels;
	float time, step;
};

template <typename DoublePendulumType>
class DoublePendulumView
{
public:
	DoublePendulumView() : pendulum(170, 0.5f, 0, 0.7f, 0, pendulumStep), time(0), step(0.016f) { }
	void Draw(Graphics2D* graphics);
	void Step() 
	{
		time += step;
		if (time < pendulumStep)
			return;
		time -= pendulumStep;
		pendulum.Step(pendulumStep);
	}

private:
	DoublePendulumType pendulum;
	float time, step;
};

#endif // PENDULUM_VIEW_H