#include "ParticleSystemPbd.h"
#include <Engine/Base.h>

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::MicroStep()
{
	if (solver == SOFT_CONSTR)
	{
		MicroStepSoft3();
		return;
	}

#ifdef USE_OPENCL
	if (firstRun)
	{
		projCL.PrepareBuffers(particles, links);
		projCL.CopyBuffers(particles, links);
		projCL.SetNumIterations(numIterations);
	}
#endif

	if (!firstTime)
	{
		// update velocities
		const Scalar invTimeStep(1.0f / timeStep);
		Vector2 g = gravity;
		g.Scale(Scalar(0.5f * lengthScale * timeStep)); // for Velocity Verlet only
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (particles[i].invMass == 0)
				continue;
			Vector2 impulse = particles[i].pos;
			impulse.Subtract(particles[i].prevPos);
			impulse.Scale(invTimeStep);
			if (INTEGRATOR == EULER || INTEGRATOR == LEAPFROG)
				particles[i].velocity += impulse;
			else if (INTEGRATOR == VELOCITY_VERLET)
				particles[i].velocity.Add(g);
			else
				particles[i].velocity = impulse;
		}
	}

	Integrate();
	Collide();

	// hack - explicit spring forces
	for (size_t i = 0; i < springs.size(); i++)
	{
		ParticleVelocity* p1 = &particles[springs[i].i1];
		ParticleVelocity* p2 = &particles[springs[i].i2];
		Vector2 delta = p1->pos - p2->pos;
		float len0 = springs[i].len;
		float len = delta.Length();
		delta.Scale(1 / len);

		// TODO: Gauss-Seidel force application?

		springs[i].disp = springs[i].stiffness * (len - len0) * delta;
	}
	for (size_t i = 0; i < springs.size(); i++)
	{
		Vector2 delta = springs[i].disp;
		particles[springs[i].i1].pos.Subtract(delta * particles[springs[i].i1].invMass);
		particles[springs[i].i2].pos.Add(delta * particles[springs[i].i2].invMass);
	}
	
	// projection
#ifdef USE_OPENCL
	projCL.CopyBuffers(particles, links);
	projCL.SetNumIterations(numIterations);
	projCL.ProjectPositions(n);
	projCL.ReadBuffers(particles);
#else
	if (solver == MIN_RES)
	{
		//ProjectMinRes(0.25f);

		proj.SetNumIterations(numIterations);
		float rho = 4; //proj.ComputeSpectralRadius(links, particles);
		proj.NewtonMinimumResidual(constraints, particles, 1 / rho);
	}
	else if (solver == CONJ_RES)
	{
		//ProjectNCR();

		proj.SetNumIterations(numIterations);
		float rho = 4; //proj.ComputeSpectralRadius(links, particles);
		proj.NonlinearConjugateResidual(constraints, particles, 1.f / rho);
		//proj.ConjugateJacobi(constraints, particles, .25f);
	}
	else if (solver == GAUSS_SEIDEL)
	{
		ProjectPositions();
		//RelaxationProjector<ParticleVelocity> relax(numIterations);
		//relax.NewtonSOR(constraints, particles);
	}
	else if (solver == JACOBI)
		ProjectPositionsJacobi(0.5f);
	else
		assert(false);
#endif
	
	LogMetrics();
}

template <int INTEGRATOR>
bool ParticleSystemPbd<INTEGRATOR>::SupportsSolver(SolverType aSolver)
{
	return aSolver == MIN_RES || aSolver == CONJ_RES || aSolver == GAUSS_SEIDEL || aSolver == JACOBI;
}

// soft version (symplectic-implicit)
const float alpha = 1.f;
const float beta1 = 1.f;
const float k = 10000.f;
const float damp = 5000.f;

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::MicroStepSoft()
{
	Vector2 hg = timeStep * lengthScale * gravity;
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		particles[i].prevPos = particles[i].pos;
		particles[i].velocity += hg;
		particles[i].pos += alpha * timeStep * particles[i].velocity;
	}

	Collide();
	ProjectPositionsSoft(alpha);

	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		particles[i].pos += (1.f - alpha) * timeStep * particles[i].velocity;
		//particles[i].velocity = (1.f / timeStep) * (particles[i].pos - particles[i].prevPos);
	}
}

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::ProjectPositionsSoft(float tau)
{
	PROFILE_SCOPE("Soft");
	static ParticleVelocity ground; 
	ground.invMass = 0; 
	ground.velocity.SetZero();
	const float hSqr = timeStep * timeStep;
	const float epsilon = 0.f / k;
	const float hBar = timeStep + damp / k;

	int n = numIterations;
	for (int k = 0; k < n; ++k)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			ParticleVelocity* p1 = &particles[constraints[i].i1];
			Vector2 delta = p1->pos;
			ParticleVelocity* p2 = NULL;
			if (constraints[i].type == Constraint::CONTACT)
			{
				p2 = &ground;
				p2->pos = constraints[i].point;
			}
			else
				p2 = &particles[constraints[i].i2];
			delta.Subtract(p2->pos);
			float len0 = constraints[i].len;
			float len = constraints[i].type == Constraint::CONTACT ? delta.Dot(constraints[i].normal) : delta.Length();
			if (constraints[i].type != Constraint::LINK && len > len0)
				continue;
			if (constraints[i].type == Constraint::CONTACT)
				delta = constraints[i].normal;
			else
				delta.Scale(1 / len);
			//float err = (len - len0) / (tau * (p1->invMass + p2->invMass) + epsilon);
			//delta.Scale(constraints[i].stiffness * err);
			//p1->velocity.Subtract(delta * (p1->invMass / timeStep));
			//p2->velocity.Add(delta * (p2->invMass / timeStep));
			//p1->pos.Subtract(delta * (p1->invMass * beta1));
			//p2->pos.Add(delta * (p2->invMass * beta1));
			float vrel = delta.Dot(p1->velocity - p2->velocity);
			float err = (len - len0 + hBar * vrel) / (tau * timeStep * hBar * (p1->invMass + p2->invMass) + epsilon);
			delta.Scale(constraints[i].stiffness * err);
			p1->velocity.Subtract(delta * (p1->invMass * timeStep));
			p2->velocity.Add(delta * (p2->invMass * timeStep));
			p1->pos.Subtract(delta * (p1->invMass * hSqr * beta1));
			p2->pos.Add(delta * (p2->invMass * hSqr * beta1));
		}
	}
}

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::MicroStepNewmark()
{
	Vector2 hg = timeStep * lengthScale * gravity;
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		//particles[i].prevPos = particles[i].pos + (1.f - beta1) * timeStep * particles[i].velocity;
		particles[i].pos += alpha * timeStep * (particles[i].velocity + beta1 * hg);
		particles[i].velocity += hg;
		particles[i].prevPos = particles[i].velocity;
	}

	Collide();
	ProjectPositionsSoft(alpha * beta1);

	if (alpha < 1.f)
	{
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (particles[i].invMass == 0.f)
				continue;
			particles[i].pos += (1.f - alpha) * timeStep * particles[i].prevPos;
		}
	}
}

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::MicroStepSoft1()
{
	Vector2 hg = timeStep * lengthScale * gravity;
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		particles[i].prevPos = particles[i].pos;
		particles[i].velocity += hg;
		particles[i].pos += timeStep * particles[i].velocity;
	}

	Collide();
	ProjectPositionsSoft1();
}

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::ProjectPositionsSoft1()
{
	static ParticleVelocity ground; ground.invMass = 0;
	const float h2 = timeStep * timeStep;

	PROFILE_SCOPE("Soft1");
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			if (k == 0)
				constraints[i].lambda = 0;
			ParticleVelocity* p1 = &particles[constraints[i].i1];
			Vector2 delta = p1->pos;
			ParticleVelocity* p2 = NULL;
			if (constraints[i].type == Constraint::CONTACT)
			{
				p2 = &ground;
				p2->pos = constraints[i].point;
			}
			else
				p2 = &particles[constraints[i].i2];
			delta.Subtract(p2->pos);
			float len0 = constraints[i].len;
			float len = constraints[i].type == Constraint::CONTACT ? delta.Dot(constraints[i].normal) : delta.Length();
			if (constraints[i].type != Constraint::LINK && len > len0)
				continue;
			if (constraints[i].type == Constraint::CONTACT)
				delta = constraints[i].normal;
			else
				delta.Scale(1 / len);
			float epsilon = 1.f / (constraints[i].stiffness * 20000.f);
			//float beta2 = 0.f * constraints[i].stiffness;
			//float vrel = delta.Dot(p1->velocity - p2->velocity);
			//float h2Bar = h2 + timeStep * beta2;
			//float scale = h2 / h2Bar;
			//float err = ((len - len0) + beta2 * vrel) * scale;
			float err = len - len0;
			float res = err - epsilon * constraints[i].lambda;
			float lambda = res / (h2 * (p1->invMass + p2->invMass) + epsilon);
			constraints[i].lambda += lambda;
			delta.Scale(lambda); // TODO: not only the diagonal gets multiplied by h2Bar
			p1->pos.Subtract(h2 * delta * p1->invMass);
			p2->pos.Add(h2 * delta * p2->invMass);
			p1->velocity.Subtract(timeStep * delta * p1->invMass);
			p2->velocity.Add(timeStep * delta * p2->invMass);
		}
	}
}

const float stiffness = 40000;

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::MicroStepSoft2()
{
	// use prevPos as accumulator
	for (size_t i = 0; i < GetNumParticles(); i++)
		particles[i].prevPos.SetZero();
	for (size_t i = 0; i < links.size(); i++)
	{
		const Constraint& link = links[i];
		//ParticleVelocity& p1 = particles[link.i1];
		//ParticleVelocity& p2 = particles[link.i2];			
		//Vector2 delta = p2.pos - p1.pos;
		//float len = delta.Length();
		//delta.Normalize();
		//links[i].normal = delta;
		//float dLen = len - link.len;
		//Vector2 force = links[i].stiffness * stiffness * dLen * delta; // TODO: damping
		//p1.prevPos.Add(force);
		//p2.prevPos.Subtract(force);

		particles[link.i1].prevPos -= 2 * particles[link.i1].invMass * link.disp;
		particles[link.i2].prevPos += 2 * particles[link.i2].invMass * link.disp;
	}
	Vector2 hg = timeStep * lengthScale * gravity;
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		Vector2 force = particles[i].prevPos;
		particles[i].pos += timeStep * particles[i].velocity + 0.5f * timeStep * hg + 0.25f * timeStep * timeStep * particles[i].prevPos;
		particles[i].velocity += hg + 0.5f * timeStep * particles[i].prevPos;
	}

	Collide();
	ProjectPositionsSoft2();
}

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::ProjectPositionsSoft2()
{
	// Midpoint projection
	static ParticleVelocity ground; ground.invMass = 0;
	const float h2 = timeStep * timeStep;

	PROFILE_SCOPE("Soft2");
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < links.size(); i++)
		{
			if (k == 0)
			{
				links[i].disp.SetZero();
				links[i].lambda = 0;
				links[i].dLambda = 0;
			}
			ParticleVelocity* p1 = &particles[links[i].i1];
			Vector2 delta = p1->pos;
			ParticleVelocity* p2 = NULL;
			if (links[i].type == Constraint::CONTACT)
			{
				p2 = &ground;
				p2->pos = links[i].point;
			}
			else
				p2 = &particles[links[i].i2];
			delta.Subtract(p2->pos);
			float len0 = links[i].len;
			float len = links[i].type == Constraint::CONTACT ? delta.Dot(links[i].normal) : delta.Length();
			if (links[i].type != Constraint::LINK && len > len0)
				continue;
			if (links[i].type == Constraint::CONTACT)
				delta = links[i].normal;
			else
				delta.Scale(1 / len);
			const float epsilon = 1.f / (links[i].stiffness * stiffness);
			float err = len - len0;
			const float dampRatio = 1.f * links[i].stiffness;
			float vrel = delta.Dot(p1->velocity - p2->velocity);
			float res = 0.5f * err + 0.5f * dampRatio * vrel - epsilon * links[i].lambda;
			float lambda = res / (0.5f * timeStep * (0.5f * timeStep + dampRatio) * (p1->invMass + p2->invMass) + epsilon);
			links[i].lambda += lambda;
			delta.Scale(lambda); // the constraint force
			links[i].disp += delta; // total constraint force
			p1->pos.Subtract(0.5f * h2 * delta * p1->invMass);
			p2->pos.Add(0.5f * h2 * delta * p2->invMass);
			p1->velocity.Subtract(timeStep * delta * p1->invMass);
			p2->velocity.Add(timeStep * delta * p2->invMass);
		}
	}
}

const float nmAlpha = 0.55f;
const float nmBeta = 0.55f;

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::MicroStepSoft3()
{
	// use prevPos as accumulator
	for (size_t i = 0; i < GetNumParticles(); i++)
		particles[i].prevPos.SetZero();
	for (size_t i = 0; i < links.size(); i++)
	{
		const Constraint& link = links[i];
		//ParticleVelocity& p1 = particles[link.i1];
		//ParticleVelocity& p2 = particles[link.i2];			
		//Vector2 delta = p2.pos - p1.pos;
		//float len = delta.Length();
		//delta.Normalize();
		//links[i].normal = delta;
		//float dLen = len - link.len;
		//Vector2 force = links[i].stiffness * stiffness * dLen * delta; // TODO: damping
		//p1.prevPos.Add(force);
		//p2.prevPos.Subtract(force);

		particles[link.i1].prevPos -= (particles[link.i1].invMass / nmAlpha) * link.disp;
		particles[link.i2].prevPos += (particles[link.i2].invMass / nmAlpha) * link.disp;
	}
	Vector2 hg = timeStep * lengthScale * gravity;
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		Vector2 force = particles[i].prevPos;
		particles[i].pos += timeStep * particles[i].velocity + nmBeta * timeStep * hg + 
			nmBeta * (1.f - nmAlpha) * timeStep * timeStep * particles[i].prevPos;
		particles[i].velocity += hg + (1.f - nmAlpha) * timeStep * particles[i].prevPos;
	}

	Collide();
	ProjectPositionsSoft3();
}

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::ProjectPositionsSoft3()
{
	// Midpoint projection
	static ParticleVelocity ground; ground.invMass = 0;
	const float h2 = timeStep * timeStep;

	PROFILE_SCOPE("Soft3(Newmark)");
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < links.size(); i++)
		{
			if (k == 0)
			{
				links[i].disp.SetZero();
				links[i].lambda = 0;
				links[i].dLambda = 0;
			}
			ParticleVelocity* p1 = &particles[links[i].i1];
			Vector2 delta = p1->pos;
			ParticleVelocity* p2 = NULL;
			if (links[i].type == Constraint::CONTACT)
			{
				p2 = &ground;
				p2->pos = links[i].point;
			}
			else
				p2 = &particles[links[i].i2];
			delta.Subtract(p2->pos);
			float len0 = links[i].len;
			float len = links[i].type == Constraint::CONTACT ? delta.Dot(links[i].normal) : delta.Length();
			if (links[i].type != Constraint::LINK && len > len0)
				continue;
			if (links[i].type == Constraint::CONTACT)
				delta = links[i].normal;
			else
				delta.Scale(1 / len);
			const float epsilon = 1.f / (links[i].stiffness * stiffness);
			float err = len - len0;
			const float dampRatio = 0.0f * links[i].stiffness;
			float vrel = delta.Dot(p1->velocity - p2->velocity);
			float res = nmAlpha * (err + dampRatio * vrel) - epsilon * links[i].lambda;
			float lambda = res / (nmAlpha * timeStep * (nmBeta * timeStep + dampRatio) * (p1->invMass + p2->invMass) + epsilon);
			links[i].lambda += lambda;
			delta.Scale(lambda); // the constraint force
			links[i].disp += delta; // total constraint force
			p1->pos.Subtract(nmBeta * h2 * delta * p1->invMass);
			p2->pos.Add(nmBeta * h2 * delta * p2->invMass);
			p1->velocity.Subtract(timeStep * delta * p1->invMass);
			p2->velocity.Add(timeStep * delta * p2->invMass);
		}
	}
}

template <int INTEGRATOR>
void ParticleSystemPbd<INTEGRATOR>::Integrate()
{
	Vector2 g = gravity;
	float scaledTimeStep = lengthScale * timeStep;
	if (INTEGRATOR == VELOCITY_VERLET)
		scaledTimeStep *= 0.5f;
	g.Scale(scaledTimeStep);
	const float ts = timeStep;
	for (size_t i = 0; i < GetNumParticles(); i++) 
	{
		if (particles[i].invMass == 0)
			continue;

		if (INTEGRATOR == EULER || INTEGRATOR == LEAPFROG)
		{
			// TODO: move in Init
			if (INTEGRATOR == LEAPFROG && firstTime)
				particles[i].velocity.Add(0.5f * g);
			Vector2 add = particles[i].velocity;
			add.Scale(ts);
			particles[i].pos.Add(add);
			particles[i].velocity.Add(g);
			particles[i].prevPos = particles[i].pos;
		}
		else // SYMPLECTIC EULER and VELOCITY VERLET
		{
			particles[i].prevPos = particles[i].pos;
			particles[i].velocity.Add(g);
			Vector2 add = particles[i].velocity;
			add.Scale(ts);
			particles[i].pos.Add(add);
			if (INTEGRATOR == VELOCITY_VERLET)
				particles[i].velocity.Add(g);
		}
	}
	if (firstTime)
	{
		firstTime = false;
	}
}

template class ParticleSystemPbd<EULER>;
template class ParticleSystemPbd<SYMPLECTIC_EULER>;
template class ParticleSystemPbd<VELOCITY_VERLET>;
template class ParticleSystemPbd<LEAPFROG>;
