#ifndef PARTICLES_DLL_H
#define PARTICLES_DLL_H

#ifdef PARTICLES_EXPORTS
#define PARTICLES_API __declspec(dllexport) 
#else
#define PARTICLES_API __declspec(dllimport) 
#endif

extern "C"
{

void PARTICLES_API RunParticles();
void PARTICLES_API RunParticlesAsChild(HWND hParent);
void PARTICLES_API DrawParticles();

}

#endif

