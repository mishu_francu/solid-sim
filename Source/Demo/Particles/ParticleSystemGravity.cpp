#include "ParticleSystemGravity.h"
#include <Engine/Base.h>

template <int INTEGRATOR>
void ParticleSystemGravity<INTEGRATOR>::MicroStep()
{
	Integrate();
	Collide();
	for (int k = 0; k < numIterations; ++k)
		ProjectPositions();

	//for (int i = 0; i < numItersVel; i++)
	//	ProjectVelocities<JAKOBSEN>();

	// update velocities
	const Scalar invTimeStep(1.0f / timeStep);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		Vector2 impulse = particles[i].pos;
		impulse.Subtract(particles[i].prevPos);
		impulse.Scale(invTimeStep);
		particles[i].velocity = Lerp(particles[i].velocity, impulse, 0.9f);
	}
}

template <int INTEGRATOR>
void ParticleSystemGravity<INTEGRATOR>::Integrate()
{
	const Scalar scaledTimeStep(lengthScale * timeStep);
	const Scalar ts(timeStep);
	const Scalar g(6.67f);
	for (size_t i = 0; i < GetNumParticles(); i++) 
	{
		// record old positions
		particles[i].prevPos = particles[i].pos;
		// compute attraction force
		Vector2 f;
		f.SetZero();
		for (size_t j = 0; j < GetNumParticles(); j++) 
		{
			if (i == j)
				continue;
			Vector2 delta = particles[j].pos - particles[i].pos;
			f.Add(delta * (g / Cube(delta.Length())));
		}
		f.Scale(scaledTimeStep);
		particles[i].velocity.Add(f);
		Vector2 add = particles[i].velocity;
		add.Scale(ts);
		particles[i].pos.Add(add);
	}
}

template class ParticleSystemGravity<ParticleSystemVelocity::SYMPLECTIC_EULER>;
