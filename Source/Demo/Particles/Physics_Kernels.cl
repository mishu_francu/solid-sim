#include "Common.cl"

__kernel void integrate(__global const float2* prevPos, __global const float2* pos, __global const float* invMass, __global float2* newPos, const float2 force)
{
	int i = get_global_id(0);
	float2 velocity = pos[i] - prevPos[i];
	// v *= damp; // TODO: damp velocity
	newPos[i] = pos[i] + min(1.f, invMass[i]) * (velocity + force);
}

__kernel void sample(__global const float2* pos, __global int* pic, const float cellSize, int width, int height)
{
	// sample particles into grid cells
	int i = get_global_id(0);
	float2 quant = pos[i] / cellSize;
	int x = (int)quant.x;
	int y = (int)quant.y;
	if (x >= 0 && x < width && y >= 0 && y < height) // TODO: get rid of this
	{
		int cellId = x + y * width;
		pic[i] = ((i & 0xffff) << 16) | (cellId & 0xffff);
	}
	else
	{
		pic[i] = -1;
	}
}

kernel void computeConstraintErrors(__global const Edge* edges, __global float2* normals, __global float* c, __global float2* pos)
{
	int i = get_global_id(0);
	float2 delta = pos[edges[i].i2] - pos[edges[i].i1];
	float len = length(delta);
	float res = edges[i].len - len;
	c[i] = res;
	normals[i] = delta / len;
}

__kernel void computeParticleErrors(__global float2* normals, __global float* c, __global const Incidence* incidence, __global float2* err)
{
	int i = get_global_id(0);
	float2 acc = (float2)(0);
	for(int j = 0; j < 4; j++)
	{
		int info = incidence[i].info[j];
		if (info == 0)
			break;
		int sgn;
		int idx = GetIndexAndSign(info, &sgn);
		acc += sgn * c[idx] * normals[idx]; // TODO: precompute c * n
	}
	err[i] = acc;
}

__kernel void computeRelativeErrors(
	__global const Edge* edges, 
	__global const float2* normals, 
	__global float2* err, 
	__global float* invMass,
	__global float* q)
{
	int i = get_global_id(0);
	int i1 = edges[i].i1;
	int i2 = edges[i].i2;
	float2 diff = invMass[i2] * err[i2] - invMass[i1] * err[i1];
	q[i] = dot(normals[i], diff);
}

// naive approach
/*__kernel void reduceAlpha(__global float* q, __global float* c, const int length, __local float2* scratch, __global float2* result)
{
	int gi = get_global_id(0);
	if (gi != 0)
		return;
	float den = 0, nom = 0;
	for (int j = 0; j < get_global_size(0); j++)
	{
		den += q[j] * q[j];
		nom += c[j] * q[j];
	}
	result[0].x = nom / den;
}*/

__kernel void reduceAlpha(__global float* q, __global float* c, const int length, __local float2* scratch, __global float2* result)
{
	int global_index = get_global_id(0);
	int local_index = get_local_id(0);
	// Load data into local memory
	if (global_index < length) 
	{
		float2 a = (float2)(q[global_index], q[global_index]);
		float2 b = (float2)(q[global_index], c[global_index]);
		scratch[local_index] = a * b;
	} 
	else 
	{
		// Zero is the identity element for the sum operation
		scratch[local_index] = 0;
	}
	barrier(CLK_LOCAL_MEM_FENCE);
	for(int offset = 1;	offset < get_local_size(0);	offset <<= 1) 
	{
		int mask = (offset << 1) - 1;
		if ((local_index & mask) == 0) 
		{
			float2 other = scratch[local_index + offset];
			float2 mine = scratch[local_index];
			scratch[local_index] = mine + other;
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	if (local_index == 0) 
	{
		result[get_group_id(0)] = scratch[0];
	}
}

__kernel void reduceFinal(__global const float2* reduce)
{
	int gi = get_global_id(0);
	if (gi != 0)
		return;
	float den = 0, nom = 0;
	for (int j = 0; j < get_global_size(0); j++)
	{
		den += reduce[j].x;
		nom += reduce[j].y;
	}
	reduce[0].x = nom / den;
}

__kernel void updatePositions(__global float2* pos, __global float* invMass, __global float2* err, __global float2* reduce)
{
	int i = get_global_id(0);
	float alpha = 0.25f; //reduce[0].x;
	pos[i] += alpha * invMass[i] * err[i];
}