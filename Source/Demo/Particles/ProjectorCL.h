#ifndef PROJECTOR_CL
#define PROJECTOR_CL

#include <Physics/OpenCL.h>
#include "Particle.h"
#include <Math/Vector4.h>

#ifdef EMULATE_CL
class JacobiEdges : public CLKernelFunctor
{
public:
	void Run(size_t* size);
private:
	void Kernel(const Edge* edges, Vector4* pos, float* invMass, Vector4* disp, Vector4* normals, Vector4* points);
};

class JacobiParticles : public CLKernelFunctor
{
public:
	void Run(size_t* size);
private:
	void Kernel(Vector4* pos, const float* invMass, const Incidence* incidence, Vector4* disp);	
};

class GaussSeidelSolver : public CLKernelFunctor
{
public:
	void Run(size_t* size);
private:
	void GaussSeidel(Edge* edges, Vector4* pos, const float* invMass);
};
#endif

template <class VectorType, class ParticleType>
class ProjectorCL
{
protected:
	int numIterations;
	CLBuffer positions, invMass, normals, points, acc;
	cl_program program;
	//Vector4* posPtr;

public:
	ProjectorCL() : numIterations(20)/*, posPtr(NULL)*/ { }
	void ReadBuffers(std::vector<ParticleType>& particles);
	void SetNumIterations(int val) { numIterations = val; }
};

typedef struct
{
	ALIGN16 cl_int size;
	cl_float4 acc[MAX_INCIDENCE];
} Accumulator;

template <class VectorType, class ParticleType>
class JacobiProjectorCL : public ProjectorCL<VectorType, ParticleType>
{
protected:
	CLKernel jacEdgesKernel, jacParticlesKernel;
	CLBuffer edges, disp, incEdges;
	size_t nLinks, nParticles, nParticlesReal, nLinksReal;
#ifdef EMULATE_CL
	JacobiEdges jacEdges;
	JacobiParticles jacParticles;
#endif

public:
	void Init();
	void PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links);
	void CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links, bool map = false);
	void ProjectPositions();
};

template <class VectorType, class ParticleType>
class MinResProjectorCL : public JacobiProjectorCL<VectorType, ParticleType>
{
public:
	void Init();
	void ProjectPositions();
};

template <class VectorType, class ParticleType>
class ConjResProjectorCL : public JacobiProjectorCL<VectorType, ParticleType>
{
public:
	void Init();
	void ProjectPositions();
};

template <class VectorType, class ParticleType>
class ConjResProjectorCL1 : public JacobiProjectorCL<VectorType, ParticleType>
{
public:
	void Init();
	void ProjectPositions();
};

template <class VectorType, class ParticleType>
class ConjResProjectorCL2 : public JacobiProjectorCL<VectorType, ParticleType>
{
public:
	void Init();
	void ProjectPositions();
};

enum GSBatchNum
{ 
	LINK_BATCHES = 4,
	COLL_BATCHES = 5,
	MAX_BATCHES = LINK_BATCHES + COLL_BATCHES 
};

template <class VectorType, class ParticleType>
class GSProjectorCL : public ProjectorCL<VectorType, ParticleType>
{
private:
	CLBuffer batches[MAX_BATCHES];
	size_t batchSizes[MAX_BATCHES];
	CLKernel projectKernel;
#ifdef EMULATE_CL
	GaussSeidelSolver gsSolver;
#endif

public:
	void Init();
	void PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links);
	void CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links, bool map = false);
	void ProjectPositions(size_t nParticles);
};

// compute the next highest power of 2 of 32-bit v
//inline int NextPot(int x)
//{
//	unsigned int v = x; 
//
//	v--;
//	v |= v >> 1;
//	v |= v >> 2;
//	v |= v >> 4;
//	v |= v >> 8;
//	v |= v >> 16;
//	v++;
//
//	return v;
//}

//inline int NextMultiple(int x, int div)
//{
//	// TODO: (x + (div - 1)) / div
//	int r = x % div;
//	if (r == 0)
//		return x;
//	return x - r + div;
//}

template <class VectorType, class ParticleType>
inline void ProjectorCL<VectorType, ParticleType>::ReadBuffers(std::vector<ParticleType>& particles)
{
	PROFILE_SCOPE("ReadBuffers");
	// read back positions
	OpenCL* compute = OpenCL::GetInstance();
	Vector4* posPtr = (Vector4*)compute->EnqueueMap(positions, true, CL_MAP_READ);
	// TODO: memcpy
	for (size_t i = 0; i < particles.size(); i++)
	{
		particles[i].pos = posPtr[i];
	}
	compute->EnqueueUnmap(positions, posPtr);
}

template <class VectorType, class ParticleType>
class VelocityProjectorCL
{
protected:
	int numIterations;
	CLBuffer vel, invMass, normals;
	cl_program program;
	Vector4* velPtr;

public:
	VelocityProjectorCL() : numIterations(20), velPtr(NULL) { }
	void ReadBuffers(std::vector<ParticleType>& particles);
	void SetNumIterations(int val) { numIterations = val; }
};

template <class VectorType, class ParticleType>
class JacobiVelProjCL : public VelocityProjectorCL<VectorType, ParticleType>
{
protected:
	CLKernel jacEdgesKernel, jacParticlesKernel;
	CLBuffer edges, disp, incEdges;
	size_t nLinks;
#ifdef EMULATE_CL
	JacobiEdges jacEdges;
	JacobiParticles jacParticles;
#endif

public:
	void Init();
	void PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links);
	void CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links, float beta);
	void ProjectVelocities(size_t nParticles);
};

template <class VectorType, class ParticleType>
inline void VelocityProjectorCL<VectorType, ParticleType>::ReadBuffers(std::vector<ParticleType>& particles)
{
	PROFILE_SCOPE("ReadBuffers");
	// read back velocities
	OpenCL* compute = OpenCL::GetInstance();
	velPtr = (Vector4*)compute->EnqueueMap(vel, true, CL_MAP_READ);
	for (size_t i = 0; i < particles.size(); i++)
	{
		particles[i].velocity = velPtr[i];
	}
	compute->EnqueueUnmap(vel, velPtr);
}

ALIGN16	
typedef struct
{
	ALIGN16 cl_float4 disp;
	cl_float len0, stiffness;
	cl_int idx;
} IncidenceInfo;

ALIGN16
struct IncidenceEx
{
	IncidenceInfo ALIGN16 info[MAX_INCIDENCE];
	cl_int num;

	IncidenceEx() : num(0) { }
};

template <class VectorType, class ParticleType>
class ParticleProjectorCL
{
private:
	CLKernel parProjKernel;
	CLBuffer pos[2];
	CLBuffer incidence, invMass;
	int numIterations;
	size_t nParticles;
	int currBuff;
public:
	void Init();
	void ReadBuffers(std::vector<ParticleType>& particles);
	void SetNumIterations(int val) { numIterations = val; }
	void CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links, bool map = false);
	void PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links);
	void ProjectPositions();
};


#endif // PROJECTOR_CL