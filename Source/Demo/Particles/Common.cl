typedef struct
{
	int i1, i2, type;
	float len, stiff;
} Edge;

#define LINK 1
#define CONTACT 2
#define COLL_PAIR 3

#define MAX_INCIDENCE 16

typedef struct
{
	int info[MAX_INCIDENCE];
} Incidence;

typedef struct
{
	int size;
	float4 disp[MAX_INCIDENCE];
} Accumulator;

int GetIndexAndSign(int info, int* sgn)
{
	int idx;
	if (info > 0)
	{
		idx = info - 1;
		*sgn = 1;
	}
	else
	{
		idx = -info - 1;
		*sgn = -1;
	}
	return idx;
}

