#ifndef PENDULUM_H
#define PENDULUM_H

#ifndef ANDROID_NDK

#include <Engine/Base.h>
#include <Math/Vector2.h>

#include <stdio.h>

#include "Integrator.h"

using Math::Vector2;
using Math::Vector2Tpl;

// TODO: use SEE vectors and __m128d (double precision support)

namespace Integrators
{
	enum ShakeMethod
	{
		QUADRATIC,
		NEWTON1,
		NEWTON2
	};
}

template<typename Real>
class Pendulum
{
public:
	typedef Real RealType;

protected:
	Pendulum(Real len)	
		: length(len)
		, omegaSqr(gravity / length)
	{
		ASSERT(len > 0.f);
	}

public:
	Real GetOmegaSqr() const { return omegaSqr; }
	Real GetLength() const { return length; }

protected:
	Real length;
	static Real gravity;
	Real omegaSqr; // = g / l - the square of the pendulum's angular frequency
};

// reduced coordinates formulation (1DOF)
template<typename Real>
class PendulumReduced : public Pendulum<Real>
{
protected:
	PendulumReduced(Real len, Real t0, Real v0, Real h)
		: Pendulum<Real>(len)
		, theta(t0)
		, vel(v0)
	{
	}

public:
	Real GetTheta() const { return theta; }
	Real GetVel() const { return vel; }
	Vector2 GetPosition() const { return Vector2(length * sin(theta), length * cos(theta)); }

protected:
	Real theta; // the current angle
	Real vel; // the current angular velocity
};

// TODO: damping, driven, chaotic
template<typename Real>
class PendulumAnalytic : public PendulumReduced<Real>
{
protected:
	PendulumAnalytic(Real len, Real t0, Real v0, Real h)
		: PendulumReduced(len, t0, v0, h)
		, time(0)
		, omega(sqrt(omegaSqr))
	{
	}

public:
	Real GetAmplitude() const { return amp; }
	Real GetMaxVel() const { return maxVel; }

protected:
	Real time; // accumulated time
	Real omega; // pendulum angular frequency
	Real amp; // amplitude of oscillation (max angle)
	Real maxVel; // maximum velocity
};

// small angle approximation solution for pendulum
template<typename Real>
class PendulumApprox : public PendulumAnalytic<Real>
{
public:
	PendulumApprox(Real len, Real t0, Real v0, Real h) : PendulumAnalytic(len, t0, v0, h)
	{
		phase = atan(-v0 / (omega * t0));
		amp = t0 / cos(phase);
		maxVel = omega * amp;
	}

	void Step(Real dt)
	{
		time += dt;
		theta = amp * cos(omega * time + phase);
		vel = -maxVel * sin(omega * time + phase);
	}

private:
	Real phase; // the phase of oscillation
};

template<typename Real>
void JacobiEllipticInverse(Real u, Real mc, Real& sn, Real& cn, Real& dn);

template<typename Real = float>
class PendulumExact : public PendulumAnalytic<Real>
{
public:
	PendulumExact(Real len, Real theta0, Real v0, Real dt) : PendulumAnalytic(len, theta0, v0, dt)
	{
		const Real h(0.5);
		Real invariant = h * v0 * v0 - omegaSqr * cos(theta0);
		ASSERT(invariant + omegaSqr >= 0);
		maxVel = sqrt(2 * (invariant + omegaSqr));
		amp = acos(-invariant / omegaSqr);
		k = sin(amp * h);
		phi0 = asin(sin(theta0 * h) / k);
	}

	void Step(Real dt)
	{
		time += dt;
		Real sn, dn, cn;
		JacobiEllipticInverse(omega * time + phi0, 1.f - k * k, sn, cn, dn);
		Real ksn = k * sn;
		theta = 2.f * asin(ksn);
		vel = 2.f * omega * k * cn / sqrt(1.f - ksn * ksn);
	}

private:
	Real k, phi0;
};

template<typename Real, int Method = Integrators::SE1>
class PendulumNumeric : public PendulumReduced<Real>
{
public:
	PendulumNumeric(Real len, Real t0, Real v0, Real dt) 
		: PendulumReduced(len, t0, v0, dt) 
		, maxTheta(0)
	{
		integrator.Initialize(theta, thetaPrev, vel, this, dt);
	}

	Real GetAmplitude() const { return maxTheta; }
	Real GetMaxVel() const { return 50; } // FIXME

	void Step(Real dt)
	{
		integrator(theta, thetaPrev, vel, this, dt);

		Real absTheta = abs(theta);
		if (absTheta > maxTheta)
			maxTheta = absTheta;
	}	

	// TODO: implicit methods
	void StepImplicitEuler(Real dt)
	{
		// FIXME
		Real sinT = sin(theta);
		Real cosT = cos(theta);
		Real det = dt * omegaSqr * cosT + 1;
		theta += dt / det * (-vel + omegaSqr * sinT);
		vel += dt  * omegaSqr / det * (dt * vel * cosT + sinT);
	}

	Real Evaluate(Real theta) const { return -omegaSqr * sin(theta); }

private:
	Real maxTheta;
	Real thetaPrev; // for Verlet only
	Integrator<Method, Real, PendulumNumeric, Real> integrator;
};

template<typename Real>
class PendulumMaximal : public Pendulum<Real>
{
protected:
	PendulumMaximal(Real len, Real t0, Real v0) 
		: Pendulum(len)
		, maxTheta(0)
	{
		pos.Set(len * sin(t0), len * cos(t0));
		vel.Set(pos.GetY(), -pos.GetX());
		vel.Scale(v0);
	}

public:
	Vector2Tpl<Real> GetPosition() const { return pos; }
	Real GetTheta() const { return atan2(pos.GetX(), pos.GetY()); } // tan t = x / y
	Real GetVel() const { return vel.GetX() / pos.GetY(); } // vx = y * w, vy = -x * w

protected:
	Vector2Tpl<Real> pos;
	Vector2Tpl<Real> posPrev;
	Vector2Tpl<Real> vel;
	static Vector2Tpl<Real> g;
	Real maxTheta;
};

template<typename Real, int Method = Integrators::SE1>
class PendulumForce : public PendulumMaximal<Real>
{
public:
	PendulumForce(Real len, Real t0, Real v0, Real dt) 
		: PendulumMaximal(len, t0, v0) 
	{
		integrator.Initialize(pos, posPrev, vel, this, dt);
	}

	Real GetAmplitude() const { return maxTheta; }

	void Step(Real dt)
	{
		// impulsive component
		Vector2Tpl<Real> n = pos;
		n.Normalize(); // TODO: reuse
		Real vt = vel.Dot(n);
		vel.Subtract(vt * n);

		integrator(pos, posPrev, vel, this, dt);

		// project position
		n = pos;
		n.Normalize();
		pos = length * n;
		//vel = (1 / dt) * (pos - oldPos);

		Real thetaAbs = abs(GetTheta());
		if (thetaAbs > maxTheta)
			maxTheta = thetaAbs;
	}

	Vector2Tpl<Real> Evaluate(const Vector2Tpl<Real>& pos, const Vector2Tpl<Real>&) const
	{
		Vector2Tpl<Real> force = g;
		Vector2Tpl<Real> unit = pos;
		unit.Normalize();
		force.Subtract(g.Dot(unit) * unit);
		return force;
	}

private:
	Integrator<Method, Vector2Tpl<Real>, PendulumForce, Real> integrator;
};

template<typename Real, int Method = Integrators::SE1>
class PendulumVelocity : public PendulumMaximal<Real>
{
public:
	PendulumVelocity(Real len, Real t0, Real v0, Real dt) 
		: PendulumMaximal(len, t0, v0) 
	{
		integrator.Initialize(pos, posPrev, vel, this, dt);
	}

	Real GetAmplitude() const { return maxTheta; }

	void Step(Real h)
	{
		// FIXME: step velocity first
		Vector2Tpl<Real> n = pos;
		Real C = pos.Length() - length;
		n.Normalize();
		Real vt = vel.Dot(n) + C / h;
		vel.Subtract(vt * n);

		integrator(pos, posPrev, vel, this, h);

		Real thetaAbs = abs(GetTheta());
		if (thetaAbs > maxTheta)
			maxTheta = thetaAbs;
	}

	Vector2Tpl<Real> Evaluate(const Vector2Tpl<Real>& pos, const Vector2Tpl<Real>&) const
	{
		return g;
	}

private:
	Integrator<Method, Vector2Tpl<Real>, PendulumVelocity, Real> integrator;
};

// DAE
template<typename Real, int Integrator = Integrators::SE1>
class PendulumConstraint : public PendulumMaximal<Real>
{
public:
	PendulumConstraint(Real len, Real t0, Real v0, Real dt) 
		: PendulumMaximal(len, t0, v0)
	{
		Real phase = atan(-v0 / (sqrt(omegaSqr) * t0));
		maxTheta = t0 / cos(phase);

		// half Euler velocity step for leapfrog only
		if (Integrator == LEAPFROG)
		{
			const Real lambda = -(pos.Dot(g) + vel.LengthSquared()) / pos.LengthSquared();
			vel.Add(Real(0.5) * dt * (g + lambda * pos));
		}
		posPrev = pos - dt * vel; // for Verlet
	}

	Real GetOmegaSqr() const { return gravity / pos.Length(); }
	Real GetAmplitude() const { return maxTheta; }

	void Step(Real dt)
	{
		if (Integrator == Integrators::EULER)
			StepEuler(dt);
		else if (Integrator == Integrators::SE1 || Integrator == Integrators::LEAPFROG)
			StepSymplectic(dt);
		else if (Integrator == Integrators::SE2)
			StepSymplectic2(dt);
		else if (Integrator == Integrators::MIDPOINT)
			StepMidpoint(dt);
		else if (Integrator == Integrators::TRAPEZOID)
			StepTrapezoid(dt);
		//else if (Integrator == Integrators::RK4)
		//	StepRk4(dt);
		else if (Integrator == Integrators::VERLET)
			StepVerlet(dt);

		// project position
		Vector2Tpl<Real> n = pos;
		n.Normalize();
		pos = length * n;
		// TODO: update velocity (for non Verlet)

		Real thetaAbs = abs(GetTheta());
		if (thetaAbs > maxTheta)
			maxTheta = thetaAbs;
	}

	// TODO: implicit lambda?

	void StepEuler(Real dt)
	{
		const Real lambda = -(pos.Dot(g) + vel.LengthSquared()) / pos.LengthSquared();
		Vector2 pos0 = pos;
		pos.Add(dt * vel);
		vel.Add(dt * (g + lambda * pos0));
	}

	void StepSymplectic(Real dt)
	{
		const Real lambda = -(pos.Dot(g) + vel.LengthSquared()) / pos.LengthSquared();
		vel.Add(dt * (g + lambda * pos));
		pos.Add(dt * vel);
	}

	void StepSymplectic2(Real dt)
	{
		pos.Add(dt * vel);
		Real lambda = -(pos.Dot(g) + vel.LengthSquared()) / pos.LengthSquared();
		vel.Add(dt * (g + lambda * pos));
	}

	void StepVerlet(Real dt)
	{
		vel = (1.f / dt) * (pos - posPrev);
		const Real lambda = -(pos.Dot(g) + vel.LengthSquared()) / pos.LengthSquared();
		Vector2 posNew = 2.f * pos - posPrev + dt * dt * (g + lambda * pos);
		posPrev = pos;
		pos = posNew;
	}

	void StepMidpoint(Real dt)
	{
		Real h(0.5 * dt);
		Real lambda = -(pos.Dot(g) + vel.LengthSquared()) / pos.LengthSquared();
		Vector2 velMid = vel + h * (g + lambda * pos);
		Vector2 posMid = pos + h * vel;
		
		lambda = -(posMid.Dot(g) + velMid.LengthSquared()) / posMid.LengthSquared();
		vel.Add(dt * (g + lambda * posMid));
		pos.Add(dt * velMid);
	}

	void StepTrapezoid(Real dt)
	{
		Real lambda = -(pos.Dot(g) + vel.LengthSquared()) / pos.LengthSquared();
		Vector2 pos1 = pos + dt * vel;
		Vector2 vel1 = vel + dt * (g + lambda * pos);
		
		Real h(0.5);
		lambda = -(pos1.Dot(g) + vel1.LengthSquared()) / pos1.LengthSquared();
		pos = h * (pos + pos1 + dt * vel1);
		vel = h * (vel + vel1 + dt * (g + lambda * pos1));
	}

	// TODO: add lambda * C to potential energy

	// TODO: Velocity Verlet
protected:
	Vector2 posPrev;
};

template<typename Real = float, int Integrator = Integrators::VERLET>
class PendulumProjection : public PendulumConstraint<Real, Integrator>
{
public:
	PendulumProjection(Real len, Real t0, Real v0, Real dt) 
		: PendulumConstraint(len, t0, v0, dt)
	{
	}

	void Step(Real dt)
	{
		Vector2Tpl<Real> oldPos = pos; // posPrev for Verlet
		if (Integrator == Integrators::EULER)
			StepEuler(dt);
		else if (Integrator == Integrators::SE1 || Integrator == Integrators::SE2 || Integrator == Integrators::LEAPFROG)
			StepSymplectic(dt);
		else if (Integrator == Integrators::MIDPOINT || Integrator == Integrators::TRAPEZOID || Integrator == Integrators::RK4)
			StepSecondOrder(dt);
		else if (Integrator == Integrators::VERLET)
			StepVerlet(dt);

		// projection
		Real len = pos.Length();
		if (len > 0)
			pos.Scale(Scalar(length / len));
		Scalar h(1 / dt); // 0.5 for Verlet
		vel = h * (pos - oldPos);
		if (Integrator == Integrators::EULER)
			vel.Add(Scalar(dt) * g);

		// TODO: is projection along pn (min distance) the same with Shake?

		Real thetaAbs = abs(GetTheta());
		if (thetaAbs > maxTheta)
			maxTheta = thetaAbs;
	}

	void StepVerlet(Real dt)
	{
		Vector2 posNew = 2 * pos - posPrev + dt * dt * g;
		posPrev = pos;
		pos = posNew;
	}

	void StepSymplectic(Real dt)
	{
		vel.Add(dt * g);
		pos.Add(dt * vel);
	}

	void StepEuler(Real dt)
	{
		pos.Add(dt * vel);
	}

	void StepSecondOrder(Real dt)
	{
		Real h(0.5 * dt * dt);
		pos.Add(dt * vel + h * g);
		//vel.Add(dt * g);
	}
};

template<typename Real, int Method = PendulumConstants::QUADRATIC>
class PendulumShake : public PendulumMaximal<Real>
{
public:
	PendulumShake(Real len, Real t0, Real v0, Real dt) 
		: PendulumMaximal(len, t0, v0)
	{
	}

	void Step(Real dt)
	{
		if (Method == PendulumConstants::QUADRATIC)
			StepExact(dt);
		else
			StepNewton(dt);

		Real thetaAbs = abs(GetTheta());
		if (thetaAbs > maxTheta)
			maxTheta = thetaAbs;
	}

	void StepNewton(Real dt)
	{
		//Vector2 p0 = 2 * pos - posPrev + dt * dt * g;
		Vector2Tpl<Real> p0 = pos + dt * vel + dt * dt * g;
		Real lambda = -C(p0) / p0.Dot(pos);
		Vector2Tpl<Real> p1 = p0 + lambda * pos;

		// one more Newton-Raphson iteration
		if (Method == PendulumConstants::NEWTON2)
		{
			// FIXME use p0.Dot(p1) too , otherwise it's implicit (Goldenthal, Jakobsen) and lossy
			lambda = -C(p1) / p1.LengthSquared();
			p1.Add(lambda * p1);
		}

		//vel = Real(0.5 / dt) * (p1 - posPrev);
		vel = Real(1.f / dt) * (p1 - pos);
		pos = p1;
	}

	void StepExact(Real dt)
	{
		//Vector2 p0 = 2 * pos - posPrev + dt * dt * g;
		Vector2Tpl<Real> p0 = pos + dt * vel + dt * dt * g;
		Real dot = p0.Dot(pos);
		Real lenSqr = pos.LengthSquared();
		Real lambda = (-dot + sqrt(dot * dot - 2 * lenSqr * C(p0))) / lenSqr;
		Vector2Tpl<Real> p1 = p0 + lambda * pos;

		//vel = Real(0.5 / dt) * (p1 - posPrev);
		vel = Real(1 / dt) * (p1 - pos);
		pos = p1;
	}

	// TODO: Verlet version, this is SE

private:
	Real C(const Vector2Tpl<Real>& p)
	{
		return Real(0.5f) * (p.LengthSquared() - length * length);
	}
};

template<typename Real>
class PendulumRattle : public PendulumMaximal<Real>
{
public:
	PendulumRattle(Real len, Real t0, Real v0, Real dt) 
		: PendulumMaximal(len, t0, v0)
	{
	}

	void Step(Real dt)
	{
		Vector2Tpl<Real> posNew = pos + dt * vel + 0.5f * dt * dt * g;
		const Real dot = posNew.Dot(pos);
		Real lenSqr = pos.LengthSquared();
		const Real lambda = (-dot + sqrt(dot * dot - lenSqr * (posNew.LengthSquared() - length * length))) / lenSqr;
		vel.Add(0.5f * dt * g + (lambda / dt) * pos);
		pos = posNew + lambda * pos;
		vel.Add(0.5f * dt * g);
		const Real mu = pos.Dot(vel) / pos.LengthSquared();
		vel.Subtract(mu * pos);

		Real thetaAbs = abs(GetTheta());
		if (thetaAbs > maxTheta)
			maxTheta = thetaAbs;
	}
};

// TODO: stiff springs, velocity/impulse based

template<typename Real>
Real Pendulum<Real>::gravity = Real(10.f * 100.0f);

template<typename Real>
Vector2Tpl<Real> PendulumMaximal<Real>::g = Vector2Tpl<Real>(0.f, Pendulum<Real>::gravity);

#endif //ANDROID_NDK

#endif // PENDULUM_H
