#ifndef PARTICLE_SYSTEM_PENALTY_H
#define PARTICLE_SYSTEM_PENALTY_H

#include "ParticleSystem.h"

class ParticleSystemPenalty : public ParticleSystem<ParticleAcceleration>
{
public:
	ParticleSystemPenalty() : stiffness(20000), damping(0), nFrames(0) { }

	void MicroStep();

	bool SupportsSolver(SolverType solver) override { return true; }

private:
	void Integrate();
	void AddPenalties();
	void IntegrateImplicit();
	void IntegrateImplicitDense();
	void IntegrateImplicitGS();
	void IntegrateImplicitCG();
	void IntegrateMidpoint();
	void IntegrateMidpointDv();
	void IntegrateMidpointDense();
	void IntegrateMidpointCG();
	void PrintMetrics();

private:
	float stiffness, damping;
	int nFrames;
};

#endif // PARTICLE_SYSTEM_PENALTY_H
