#include "ParticleSystemSeqPos.h"
#include <Engine/Base.h>
#include <Graphics2D/Color.h>

void ParticleSystemSeqPos::MicroStep()
{
	Integrate();
	Collide();

	//TODO: warm starting
	if (solver == GAUSS_SEIDEL)
		ProjectVelocities();
	else
		ProjectVelocitiesCR(0.25f);

}

bool ParticleSystemSeqPos::SupportsSolver(SolverType aSolver)
{
	return aSolver == GAUSS_SEIDEL || aSolver == CONJ_RES || aSolver == JACOBI || aSolver == JACOBI_CR;
}

void ParticleSystemSeqPos::Integrate()
{
	Vector2 g = gravity;
	g.Scale(lengthScale * timeStep);	
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		// increment velocities
		particles[i].velocity.Add(g);		
	}
}

void ParticleSystemSeqPos::ProjectVelocities()
{
	PROFILE_SCOPE("SP GS");
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		pair.lambda = 0.f;
		pair.lambdaF1 = 0.f;
	}
	ParticleVelocity dummy; dummy.invMass = 0.f;
	for (int k = 0; k < numIterations; ++k)
	{
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint& pair = constraints[i];
			ParticleVelocity* p1 = &particles[pair.i1];
			ParticleVelocity* p2 = &dummy;
			Vector2 v12;
			if (pair.type == Constraint::COLL_PAIR || pair.type == Constraint::LINK)
			{
				p2 = &particles[pair.i2];
				Vector2 n = p1->pos - p2->pos;
				float len = n.Length();
				pair.depth = len - pair.len;
				pair.normal = (1.f / len) * n;
				v12 = p2->velocity - p1->velocity;
			}
			else if (pair.type == Constraint::CONTACT)
			{
				pair.depth = pair.normal.Dot(p1->pos - pair.point) - pair.len;
				v12 = p1->velocity; v12.Flip();
			}

			float vnrel = pair.normal.Dot(v12);
			float alpha = 1.f;
			float beta = .6f;
			float dLambda = (alpha * vnrel - beta * pair.depth / timeStep) / (p1->invMass + p2->invMass);

			float lambda0 = pair.lambda;
			pair.lambda = lambda0 + dLambda;
			if (pair.type != Constraint::LINK && pair.lambda < 0.f)
				pair.lambda = 0.f;
			dLambda = pair.lambda - lambda0;

			Vector2 disp = dLambda * pair.normal;
			p1->velocity += p1->invMass * disp;
			p2->velocity -= p2->invMass * disp;
		
			// friction
			if (pair.type != Constraint::LINK && mu > 0.f)
			{
				Vector2 vt = v12 - pair.normal * vnrel;
				float vtrel = vt.Length();
				if (vtrel != 0.0f)
				{
					float dLambdaF = vtrel / (p1->invMass + p2->invMass);
					float lambdaF0 = pair.lambdaF1;
					pair.lambdaF1 = lambdaF0 + dLambdaF;
					const float limit = mu * pair.lambda;
					pair.lambdaF1 = clamp(pair.lambdaF1, -limit, limit);
					dLambdaF = pair.lambdaF1 - lambdaF0;
			
					vt.Scale(dLambdaF / vtrel);
					disp += vt;
					p1->velocity.Add(vt * p1->invMass);
					p2->velocity.Subtract(vt * p2->invMass);
				}
			}

			if (k > 0)
			{
				p1->pos += timeStep * p1->invMass * disp;
				p2->pos -= timeStep * p2->invMass * disp;
			}
		}

		if (k == 0)
		{
			for (size_t i = 0; i < GetNumParticles(); i++)
			{
				if (particles[i].invMass != 0)
					particles[i].pos += timeStep * particles[i].velocity;
			}
		}
	}
}

void ParticleSystemSeqPos::ProjectVelocitiesCR(float omega)
{
	PROFILE_SCOPE("SP CR");
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		pair.lambda = 0.f;
		pair.lambdaF1 = 0.f;
		pair.disp.SetZero();
	}
	ParticleVelocity dummy; dummy.invMass = 0.f;
	float b = 0;
	for (int k = 0; k < numIterations; k++)
	{
		if (solver == CONJ_RES || solver == JACOBI_CR)
		{
			b = (float)k / (float)(numIterations - 1);
			if (b > 0)
				b = pow(b, 0.6f);
		}
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint& pair = constraints[i];
			ParticleVelocity* p1 = &particles[pair.i1];
			ParticleVelocity* p2 = &dummy;
			Vector2 v12;
			if (pair.type == Constraint::COLL_PAIR)
			{			
				p2 = &particles[pair.i2];
				Vector2 n = p1->pos - p2->pos;
				float len = n.Length();
				pair.depth = pair.len - len;
				n.Normalize();
				pair.normal = n;

				v12 = p2->velocity - p1->velocity;
			}
			else if (pair.type == Constraint::CONTACT)
			{
				pair.depth = pair.len - pair.normal.Dot(p1->pos - pair.point);
				v12 = p1->velocity; 
				v12.Flip();
			}

			float vnrel = pair.normal.Dot(v12);
			float a = 1.f;
			float beta = .5f;
			pair.err = a * vnrel + beta * pair.depth / timeStep;		

			float alpha = omega;
			if (solver == PhysicsSystem::JACOBI || solver == PhysicsSystem::JACOBI_CR)
			{
				if (p1->invMass == 0 && p2->invMass == 0)
					continue;
				alpha = omega / (p1->invMass + p2->invMass);
			}
			pair.dLambda = alpha * pair.err + b * pair.dLambda;

			float lambda0 = pair.lambda;
			pair.lambda = lambda0 + pair.dLambda;
			if (pair.lambda < 0.f)
				pair.lambda = 0.f;
			pair.dLambda = pair.lambda - lambda0;

			pair.disp = pair.dLambda * pair.normal;
	
			// friction
			if (mu > 0.f)
			{
				Vector2 vt = v12 - pair.normal * vnrel;
				float vtrel = vt.Length();
				if (fabs(vtrel) >= 0.01f)
				{
					pair.dLambdaF1 = vtrel * alpha + b * pair.dLambdaF1;
					float lambdaF0 = pair.lambdaF1;
					const float limit = mu * pair.lambda;
					pair.lambdaF1 = clamp(lambdaF0 + pair.dLambdaF1, -limit, limit);
					pair.dLambdaF1 = pair.lambdaF1 - lambdaF0;
			
					vt.Scale(pair.dLambdaF1 / vtrel);
					pair.disp += omega * vt;
				}
			}
		}
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint& pair = constraints[i];
			ParticleVelocity* p1 = &particles[pair.i1];
			ParticleVelocity* p2 = pair.type == Constraint::COLL_PAIR ? &particles[pair.i2] : &dummy;
			p1->velocity += p1->invMass * pair.disp;
			p2->velocity -= p2->invMass * pair.disp;
			
			if (k > 0)
			{
				p1->pos += timeStep * p1->invMass * pair.disp;
				p2->pos -= timeStep * p2->invMass * pair.disp;
			}
		}
		if (k == 0)
		{
			for (size_t i = 0; i < GetNumParticles(); i++)
			{
				particles[i].pos += timeStep * particles[i].velocity;
			}
		}
	}

}

