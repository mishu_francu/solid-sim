#include "PendulumView.h"

#include <Engine/Engine.h>
#include <Graphics2D/Graphics2D.h>

template<typename PendulumType>
void PendulumView<PendulumType>::Draw(Graphics2D* graphics)
{
	graphics->SetBgColor(0xfff3f3f3);
	graphics->SetColor(0xff000000);

	float screenW = Engine::getInstance()->getScreenWidth();
	float screenH = Engine::getInstance()->getScreenHeight();

	// draw 4 quadrants
	graphics->SetLineWidth(1.0f);
	graphics->DrawRect(0.01f * screenW, 0.01f * screenH, 0.48f * screenW, 0.48f * screenH);
	graphics->DrawRect(0.01f * screenW, 0.51f * screenH, 0.48f * screenW, 0.48f * screenH);
	graphics->DrawRect(0.51f * screenW, 0.01f * screenH, 0.48f * screenW, 0.48f * screenH);
	graphics->DrawRect(0.51f * screenW, 0.51f * screenH, 0.48f * screenW, 0.48f * screenH);
	graphics->SetLineWidth(3.0f);

	// TODO: draw current point, log (global) error, invariants
	Vector2 orig(screenW * 0.25f, screenH * 0.25f);
	Vector2 pos(pendulum.GetPosition());
	graphics->DrawLine(orig, orig + pos);
	graphics->DrawCircle(orig + pos, 10);
	
	graphics->DrawString(0.01f * screenW + 10, screenH * 0.5f - 15, "Pendulum");
	graphics->DrawString(0.51f * screenW + 10, screenH * 0.5f - 15, "Time plot");
	graphics->DrawString(0.01f * screenW + 10, screenH * 0.99f - 15, "Phase portrait");
	graphics->DrawString(0.51f * screenW + 10, screenH * 0.99f - 15, "Energy plot");

	graphics->SetLineWidth(2.0f);
	orig.Set(0.25f * screenW, 0.75f * screenH);

	// draw axes
	graphics->SetLineStyle(1);
	float halfLen = screenH * 0.23f;
	graphics->DrawLine(orig.x - halfLen, orig.y, orig.x + halfLen, orig.y);
	graphics->DrawLine(orig.x, orig.y - halfLen, orig.x, orig.y + halfLen);
	graphics->SetLineStyle(0);
	graphics->SetLineWidth(1.0f);

	// phase space
	Scalar scale(30.f);
	Vector2 v0;
	float step = 1.5f;
	Vector2 vStep = Vector2(step, 0.f);
	size_t maxSteps = screenW * 0.48f / step;
	size_t idx0 = 0;
	if (thetas.size() > maxSteps)
		idx0 = thetas.size() - maxSteps;

	if (thetas.size() > 0)
		v0 = orig + Vector2(Scalar(thetas[0]) * scale, Scalar(-vels[0]) * scale);
	for(size_t i = 1; i < thetas.size(); i++)
	{
		Vector2 v1(thetas[i], -vels[i]);
		v1.Scale(scale);
		v1.Add(orig);
		graphics->DrawLine(v0, v1);
		v0 = v1;
	}
		
	// draw position graph
	orig.Set(0.51f * screenW, 0.25f * screenH);
	graphics->DrawLine(orig, Vector2(screenW * 0.99f, orig.y));
	if (thetas.size() > 0)
		v0 = orig + Vector2(0.f, -thetas[idx0] * scale);
	Vector2 origStep = orig + vStep;
	for(size_t i = idx0 + 1; i < thetas.size(); i++)
	{
		Vector2 v1(0.f, -thetas[i] * scale);
		v1.Add(origStep);		
		origStep.Add(vStep);
		graphics->DrawLine(v0, v1);
		v0 = v1;
	}

	// draw velocity graph
	graphics->SetColor(0xff0000ff);
	if (vels.size() > 0)
		v0 = orig + Vector2(0.f, -vels[idx0] * scale);
	origStep = orig + vStep;
	for (size_t i = idx0 + 1; i < thetas.size(); i++)
	{
		Vector2 v1(0.f, -vels[i] * scale);
		v1.Add(origStep);
		origStep.Add(vStep);
		graphics->DrawLine(v0, v1);
		v0 = v1;
	}
	graphics->SetColor(0xff000000);

	// draw energy graph
	scale = Scalar(30.f);
	orig.Set(screenW * 0.51f, screenH * 0.75f);
	origStep = orig + vStep;
	graphics->DrawLine(orig, Vector2(screenW * 0.99f, orig.y));
	if (thetas.size() > 0)
	{
		float e = 0.5f * vels[idx0] * vels[idx0] - pendulum.GetOmegaSqr() * cos(thetas[idx0]);
		v0 = orig + Vector2(0.f, Scalar(e) * scale);
	}
	for(size_t i = idx0 + 1; i < thetas.size(); i++)
	{
		float e = 0.5f * vels[i] * vels[i] - pendulum.GetOmegaSqr() * cos(thetas[i]);
		Vector2 v1(0.f, Scalar(e) * scale);
		v1.Add(origStep);
		origStep.Add(vStep);
		graphics->DrawLine(v0, v1);
		v0 = v1;
	}
}

template <typename DoublePendulumType>
void DoublePendulumView<DoublePendulumType>::Draw(Graphics2D* graphics)
{
	Vector2 orig(Engine::getInstance()->getScreenWidth() * 0.5f, Engine::getInstance()->getScreenHeight() * 0.5f);
	Vector2 pos1(pendulum.GetPosition1());
	graphics->DrawLine(orig, orig + pos1);
	orig.Add(pos1);
	Vector2 pos2(pendulum.GetPosition2());
	graphics->DrawLine(orig, orig + pos2);
}

template class PendulumView<PendulumType1>;
template class PendulumView<PendulumType2>;
template class DoublePendulumView<DblPendType1>;
template class DoublePendulumView<DblPendType2>;
