#ifndef PARTICLES_2D_DEMO_H
#define PARTICLES_2D_DEMO_H

#include <memory>
#include "Math/Vector2.h"

class PhysicsSystem;
class XMLElement;
class Graphics2D;

using Math::Vector2;

class Particles2DDemo
{
public:
	enum DemoType
	{
		DEMO_CLOTH,
		DEMO_PARTICLES,
		DEMO_LINKS,
		DEMO_LATTICE,
		DEMO_CRADLE,
	};

public:
	Particles2DDemo();
	void Create(DemoType type) { mDemoType = type; }
	bool LoadFromXml(XMLElement* xRoot);
	void SetMaxFrames(int val);
	void Draw(Graphics2D* graphics, int color);
	void Init(float width, float height);
	void Update();
	void Test();

private:
	void InitParticles(float width, float height);
	void InitCloth(float width, float height);
	void InitLinks(float width, float height);
	void InitCradle(float width, float height);
	void InitLattice(float width, float height);

	void InitBoxes(int numBoxes, float width, float height);
	bool OverlapsBoxes(const Vector2& p);
	void DrawParticles(Graphics2D* graphics);
	void TestMethod();

private:
	std::unique_ptr<PhysicsSystem> mParticleSys;
	DemoType mDemoType;
	float mRadius;
	int mNumParticles;
	int mDivisions;
};

#endif // PARTICLES_2D_DEMO_H
