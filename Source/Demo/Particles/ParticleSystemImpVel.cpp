#include "ParticleSystemImpVel.h"

void ParticleSystemImpVel::MicroStep()
{
	Collide();

	// integrate velocities
	Vector2 g = gravity;
	g.Scale(lengthScale * timeStep);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0.f)
			continue;
		particles[i].velocity.Add(g);
		particles[i].prevVel = particles[i].velocity;
		particles[i].prevPos = particles[i].pos;
	}

	if (solver == GAUSS_SEIDEL)
	{
		ProjectVelocitiesImplicit();

        // post-stabilization
        ProjectPositions(numIterationsPost);
	}   
	else if (solver == EXACT)
	{
		for (int iter = 0; iter < numIterations; iter++)
		{
			// TODO: fix solver for linear projection
			solver = GAUSS_SEIDEL; // GS for now, with the same number of iterations
			ProjectVelocities();
			solver = EXACT;

			// update positions
			for (size_t i = 0; i < GetNumParticles(); i++)
			{
				if (particles[i].invMass == 0.f)
					continue;
				particles[i].pos = particles[i].prevPos + timeStep * particles[i].velocity;
			}
		}
	}
	else
		assert(false); // solver not supported

	LogMetrics();
}

bool ParticleSystemImpVel::SupportsSolver(SolverType aSolver)
{ 
	return aSolver == EXACT || aSolver == GAUSS_SEIDEL; 
}

void ParticleSystemImpVel::ProjectVelocitiesImplicit()
{
	for (int iter = 0; iter < numIterations; iter++)
	{
		// precompute length and normal
		for (size_t i = 0; i < constraints.size(); i++)
		{
			Constraint& pair = constraints[i];
			ParticleVelocity& p1 = particles[pair.i1];
			if (pair.type == Constraint::COLL_PAIR || pair.type == Constraint::LINK)
			{
				const ParticleVelocity& p2 = particles[pair.i2];
				Vector2 n = p1.pos - p2.pos;
				float len = n.Length();
				pair.depth = pair.len - len;
				n.Normalize();
				pair.normal = n;
			}
			else
			{
				pair.depth = pair.len - pair.normal.Dot(p1.pos - pair.point);
			}
			pair.lambda = 0.f;
			pair.lambdaF1 = 0.f;
		}

		SolveConstraints();

		// update positions
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (particles[i].invMass == 0.f)
				continue;
			particles[i].pos = particles[i].prevPos + timeStep * particles[i].velocity;
		}
	}
}

void ParticleSystemImpVel::SolveConstraintsImplicit()
{
    for (size_t i = 0; i < constraints.size(); i++)
    {
        Constraint& pair = constraints[i];
        ParticleVelocity* p1 = &particles[pair.i1];
        ParticleVelocity dummy; dummy.invMass = 0.f;
        ParticleVelocity* p2 = &dummy;
        Vector2 v12;
        if (pair.type == Constraint::COLL_PAIR || pair.type == Constraint::LINK)
        {
            p2 = &particles[pair.i2];
            v12 = p2->velocity - p1->velocity;
        }
        else if (pair.type == Constraint::CONTACT)
        {
            v12 = p1->velocity; v12.Flip();
        }

        float vnrel = pair.normal.Dot(v12);
        float dLambda = (vnrel + beta * pair.depth / timeStep) / (p1->invMass + p2->invMass);

        float lambda0 = pair.lambda;
        pair.lambda = lambda0 + dLambda;
        if (pair.type != Constraint::LINK && pair.lambda < 0.f)
            pair.lambda = 0.f;
        dLambda = pair.lambda - lambda0;

        Vector2 disp = pair.stiffness * dLambda * pair.normal;

        // friction
        if (pair.type != Constraint::LINK && mu > 0.f)
        {
            Vector2 vt = v12 - pair.normal * vnrel;
            float vtrel = vt.Length();
            if (vtrel != 0.0f)
            {
                float dLambdaF = vtrel / (p1->invMass + p2->invMass);
                float lambdaF0 = pair.lambdaF1;
                pair.lambdaF1 = lambdaF0 + dLambdaF;
                const float limit = mu * pair.lambda;
                pair.lambdaF1 = clamp(pair.lambdaF1, -limit, limit);
                dLambdaF = pair.lambdaF1 - lambdaF0;

                vt.Scale(dLambdaF / vtrel);
                disp += vt;
                //p1->velocity.Add(vt * p1->invMass);
                //p2->velocity.Subtract(vt * p2->invMass);
            }
        }

        p1->velocity += p1->invMass * disp;
        p2->velocity -= p2->invMass * disp;

        disp.Scale(timeStep);
        p1->pos += p1->invMass * disp;
        p2->pos -= p2->invMass * disp;
    }
}
