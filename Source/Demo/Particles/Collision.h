#ifndef COLLISION_H
#define COLLISION_H

#include <Math/Vector2.h>
#include <Math/Utils.h>
#include <vector>
#include <algorithm>
#include <Graphics2D/Aabb.h>
#include <Engine/Utils.h>

struct Primitive
{
	enum Type
	{
		AABB,
		OBB,
		CIRCLE,
		SEGMENT
	};

	Type type;

protected:
	Primitive(Type t) : type(t) { }
};

struct Obb : Primitive
{
	Vector2 pos;
	Vector2 extent;
	float angle;

	Obb() : Primitive(OBB), angle(0) { }
	
	Aabb GetAabb() const
	{
		Vector2 ext = extent;
		ext.Rotate(angle);
		Vector2 ext0 = extent;
		ext0.Flip();
		ext0.Rotate(angle);
		Vector2 ext1(extent.GetX(), -extent.GetY());
		ext1.Rotate(angle);
		Vector2 ext2(-extent.GetX(), extent.GetY());
		ext2.Rotate(angle);
		Vector2 v1 = pos + ext;
		Vector2 v2 = pos + ext0;
		Vector2 v3 = pos + ext1;
		Vector2 v4 = pos + ext2;

		Aabb aabb;
		aabb.min = vmin(v1, vmin(v2, vmin(v3, v4)));
		aabb.max = vmax(v1, vmax(v2, vmax(v3, v4)));
		return aabb;
	}
};

struct Circle : Primitive
{
	Vector2 pos;
	float radius;

	Circle() : Primitive(CIRCLE), radius(0) { }

	Aabb GetAabb() const
	{
		Aabb aabb;
		Vector2 extent(radius, radius);
		aabb.min = pos - extent;
		aabb.max = pos + extent;
		return aabb;
	}
};

struct Segment : Primitive
{
	Vector2 a, b;

	Segment() : Primitive(SEGMENT) { }

	Aabb GetAabb() const
	{
		Aabb aabb;
		aabb.min = vmin(a, b);
		aabb.max = vmax(a, b);
		return aabb;
	}
};

inline float SignedTriArea(const Vector2& a, const Vector2& b, const Vector2& c)
{
	return (a.GetX() - c.GetX()) * (b.GetY() - c.GetY()) - (a.GetY() - c.GetY()) * (b.GetX() - c.GetX());
}

inline bool TestSegmSegm(const Vector2& a, const Vector2& b, const Vector2& c, const Vector2& d, float& t, Vector2& p)
{
	float a1 = SignedTriArea(a, b, d);
	float a2 = SignedTriArea(a, b, c);
	if (a1 * a2 < 0.0f)
	{
		float a3 = SignedTriArea(c, d, a);
		float a4 = a3 + a2 - a1;
		if (a3 * a4 < 0.0f)
		{
			t = a3 / (a3 - a4);
			p = a;
			Vector2 delta = t * (b - a);
			p.Add(delta);
			return true;
		}
	}
	return false;
}

inline float Orient2D(Vector2 a, Vector2 b, Vector2 c)
{
	return (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x);
}

inline Vector2 ClosestPtLine(const Vector2& p, const Vector2& a, const Vector2& b, float& t)
{
	Vector2 u = b - a;
	Vector2 v = p - a;
	ASSERT(u.LengthSquared() != 0);
	t = u.Dot(v) / u.LengthSquared();
	return a + t * u;
}

inline Vector2 ClosestPtSegm(const Vector2& p, const Vector2& a, const Vector2& b, float& t)
{
	Vector2 u = b - a;
	Vector2 v = p - a;
	t = u.Dot(v) / u.LengthSquared();
	if (t <= 0.f)
	{
		t = 0.f;
		return a;
	}
	if (t >= 1.f)
	{
		t = 1.f;
		return b;
	}
	return a + t * u;
}

// p is on the (a,b) at s
// q is on the (c,d) at t
inline float ClosestPtSegmSegm1(const Vector2& a, const Vector2& b, const Vector2& c, const Vector2& d, Vector2& p, Vector2& q, float& s, float& t)
{
	if (TestSegmSegm(a, b, c, d, t, p))
	{
		q = p;
		return 0.f;
	}

	float t1, t2;
	Vector2 c1 = ClosestPtSegm(c, a, b, t1);
	Vector2 d1 = ClosestPtSegm(d, a, b, t2);
	// choose between c1 and d1
	p = d1;
	s = t1;
	if ((c - c1).LengthSquared() <= (d - d1).LengthSquared())
	{
		p = c1;
		s = t2;
	}

	q = ClosestPtSegm(p, c, d, t);
	return (p - q).Length();
}

inline float ClosestPtSegmSegm(const Vector2& x1, const Vector2& x2, const Vector2& x3, const Vector2& x4, Vector2& p, Vector2& q, float& s, float& t)
{
	Vector2 x21 = x2 - x1;
	Vector2 x43 = x4 - x3;
	const float eps = 0.0001f;
	if (fabs(x21.Cross(x43)) < eps)
	{
		p = x1; s = 0;
		//q = x3; t = 0;
		q = ClosestPtSegm(p, x3, x4, t);
		p = ClosestPtSegm(q, x1, x2, s);
		return (q - p).Length();
	}
	Vector2 x31 = x3 - x1;
	float a = x21.LengthSquared();
	float b = x21.Dot(x43);
	float c = x43.LengthSquared();
	float d = x21.Dot(x31);
	float e = -x43.Dot(x31);
	float det = a * c - b * b;
	// TODO: det == 0 - parallel case?
	//ASSERT(det != 0);
	if (det == 0.f)
	{
		p = x1; s = 0;
		//q = x3; t = 0;
		q = ClosestPtSegm(p, x3, x4, t);
		p = ClosestPtSegm(q, x1, x2, s);
		return (q - p).Length();
	}
	s = (c * d + b * e) / det;
	t = (b * d + a * e) / det;
	float s1 = Math::clamp(s, 0.f, 1.f);
	float t1 = Math::clamp(t, 0.f, 1.f);
	float ds = fabsf(s1 - s);
	float dt = fabsf(t1 - t);
	s = s1;
	t = t1;
	if (ds > dt)
	{
		p = x1 + s * x21;
		q = ClosestPtSegm(p, x3, x4, t);
	}
	else
	{
		q = x3 + t * x43;
		p = ClosestPtSegm(q, x1, x2, s);
	}
	return (q - p).Length();
}

// circle vs AABB
bool IntersectAabb(const Vector2& center, float radius, const Vector2& min, const Vector2& max, Vector2& intersPoint, Vector2& normal);

inline bool IntersectObb(const Vector2& center, float radius, const Obb& obb, Vector2& intersPoint, Vector2& normal)
{
	// inverse rotation
	Vector2 c = center - obb.pos; 
	c.Rotate(-obb.angle);
	// intersection with aabb
	Vector2 p, n;
	if (IntersectAabb(c, radius, -1.f * obb.extent, obb.extent, p, n))
	{
		// rotation
		p.Rotate(obb.angle);
		intersPoint = obb.pos + p;
		n.Rotate(obb.angle);
		normal = n;
		return true;
	}
	return false;
}

inline bool IntersectCircle(const Vector2& center, float radius, const Circle* circ, Vector2& intersPoint, Vector2& normal)
{
	float dist = radius + circ->radius;
	Vector2 delta = center - circ->pos;
	if (delta.LengthSquared() < dist * dist)
	{
		delta.Normalize();
		normal = delta;
		intersPoint = circ->pos + circ->radius * delta;
		return true;
	}
	return false;
}

inline bool IntersectSegment(const Vector2& center, float radius, const Segment* segm, Vector2& intersPoint, Vector2& normal)
{
	float t;
	Vector2 p = ClosestPtSegm(center, segm->a, segm->b, t);
	Vector2 delta = center - p;
	float d = delta.Length();
	if (d > radius)
		return false;
	intersPoint = p;
	normal = (1.f / d) * delta; // normalized
	return true;
}

inline bool IntersectPrimitive(const Vector2& center, float radius, const Primitive* prim, Vector2& intersPoint, Vector2& normal)
{
	if (prim->type == Primitive::OBB)
	{
		const Obb* obb = (const Obb*)prim; // TODO: C++ cast
		return IntersectObb(center, radius, *obb, intersPoint, normal);
	}
	else if (prim->type == Primitive::CIRCLE)
	{
		return IntersectCircle(center, radius, (Circle*)prim, intersPoint, normal);
	}
	else if (prim->type == Primitive::SEGMENT)
	{
		return IntersectSegment(center, radius, (Segment*)prim, intersPoint, normal);
	}
	return false; // we don't support the primitive
}

#endif // COLLISION_H