#ifndef PENDULUM_DEMO_H
#define PENDULUM_DEMO_H

#include "PendulumView.h"

class PendulumDemo
{
public:
	PendulumDemo() : mView(100, 1.3f, 0, 0.016f) {}

	void Draw(Graphics2D* g) { mView.Draw(g); }

	void Update() { mView.Step(); }

private:
	PendulumView<PendulumType1> mView;
};

#endif // PENDULUM_DEMO_H
