#ifndef PARTICLE_SYSTEM_RATTLE_H
#define PARTICLE_SYSTEM_RATTLE_H

#include "ParticleSystemVelocity.h"

class ParticleSystemRattle : public ParticleSystemVelocity
{
public:
	ParticleSystemRattle() : mImplicit(false)
	{
		tolerance = radius * 0.5f;
	}

	//const char* GetName() const { return "Rattle"; }
	void MicroStep() override;
	bool SupportsSolver(SolverType aSolver) override;

private:
	bool mImplicit;
};

#endif // PARTICLE_SYSTEM_RATTLE_H
