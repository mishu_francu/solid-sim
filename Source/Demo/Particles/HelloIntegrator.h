#pragma once

#include <vector>
#include <Math/Vector2.h>
#include <Engine/Utils.h>

namespace HelloIntegrator
{

	// "primitive" types
	typedef float real;
	typedef Math::Vector2Tpl<real> vec2;

	// units are SI

	struct Edge
	{
		int idx[2];
	};

	template <typename T>
	class Vector : public std::vector<T>
	{
	public:
		Vector& operator +=(const Vector& rhs)
		{
			ASSERT(size() == rhs.size());
			for (size_t i = 0; i < size(); i++)
				this->at(i) += rhs[i];
			return *this;
		}

		Vector& operator +=(T rhs)
		{
			ASSERT(size() == rhs.size());
			for (size_t i = 0; i < size(); i++)
				this->at(i) += rhs;
			return *this;
		}
	};

	// TODO: lazy evaluation

	template <typename T>
	Vector<T>& operator +(const Vector<T>& a, const Vector<T>& b)
	{
		Vector<T> ret = b;
		for (size_t i = 0; i < b.size(); i++)
			ret[i] += a[i];
		return ret;
	}

	template <typename T>
	Vector<T>& operator *(real a, const Vector<T>& b)
	{
		Vector<T> ret = b;
		for (size_t i = 0; i < b.size(); i++)
			ret[i] *= a;
		return ret;
	}

	template <typename T>
	Vector<T>& operator *(const Vector<real>& a, const Vector<T>& b)
	{
		Vector<T> ret = b;
		for (size_t i = 0; i < b.size(); i++)
			ret[i] *= a[i];
		return ret;
	}

	template <typename T>
	Vector<T>& mask(const Vector<T>& a, const Vector<T>& mask)
	{
		Vector ret(mask.size());
		for (size_t i = 0; i < mask.size(); i++)
			ret[i] = mask[i] == 0 ? T(0) : a[i];
		return ret;
	}

	template <typename T>
	Vector<T>& mask(vec2 a, const Vector<real>& mask)
	{
		Vector<T> ret;
		ret.resize(mask.size());
		for (size_t i = 0; i < mask.size(); i++)
			ret[i] = mask[i] == 0 ? T(0) : a;
		return ret;
	}

	class HelloIntegratorDemo
	{
	public:
		HelloIntegratorDemo()
		{

		}

		void Update()
		{
			const real h = real(0.16);
			vec2 gravity(0, 10);
			vel += mask<vec2>(h * gravity, invMass) + h * invMass * forces;
			pos += h * vel;
		}

	private:
		void SetNumParticles(int n)
		{
			pos.resize(n);
			vel.resize(n);
			invMass.resize(n);
		}

		void AddLink(int a, int b)
		{
			edges.push_back({ a, b });

			// save rest length
			restLengths.push_back((pos[a] - pos[b]).Length());
		}

		void InitLinks(real width, real height);

		void AccumulateForces();

	private:
		// the nodes (attributes)
		Vector<vec2> pos;
		Vector<vec2> vel;
		Vector<real> invMass;
		Vector<vec2> forces;

		// the edges
		std::vector<Edge> edges;

		// edge attributes
		Vector<real> restLengths;
	};

} // namespace HelloIntegrator