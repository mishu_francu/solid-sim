#ifndef RIGID_H
#define RIGID_H

#include <Math/Vector2.h>
#include <vector>
#include <memory>

// TODO: temp
#include "Collision.h"
#include <Engine/Engine.h>

// TODO: put inside namespace or class
struct Shape
{
	enum ShapeType
	{
		SPHERE = 1,
		CAPSULE
	};

	int type;
};

struct Capsule : Shape
{
	float hh, r;
	Capsule() : hh(80), r(50) { type = CAPSULE; }
	Capsule(float halfHeight, float radius) : hh(halfHeight), r(radius) { type = CAPSULE; }
};

struct Sphere : Shape
{
	float r;
	Sphere() : r(50) { type = SPHERE; }
	Sphere(float radius) : r(radius) { type = SPHERE; }
};

struct Rigid
{
	Vector2 pos, vel;
	float theta, omega;
	float invMass;
	float inertia; // inverse moment of inertia
	// PBD
	Vector2 pos0;
	float theta0;
	// shape
	std::shared_ptr<Shape> shape;

	Rigid() : theta(0.f), omega(0.f), invMass(1.f), inertia(0.0004f), shape(NULL) { }	
};

enum ContactId
{
	CID_LEFT,
	CID_RIGHT,
	CID_BOTTOM,
	CID_MIDDLE
};

struct ClosestPoints
{
	Vector2 p, n;
	// TODO: rename to p1
	Vector2 q; // local coords.
	int id1, id2;
	float depth;
};

class RigidSystem
{
public:
	enum { UNIT = 100 };
	struct Contact : ClosestPoints
	{
		int idx1, idx2; // rigid indices
		float lambda, lambdaF;
		Contact() : lambda(0), lambdaF(0), idx2(-1) { }
		Contact(const ClosestPoints& closestPts) : ClosestPoints(closestPts), lambda(0), idx2(-1) { }
	};
	struct ContactEx : Contact
	{
		Vector2 t1, t2;
		float stab, qxn, pxn;
	};

public:
	RigidSystem() : gravity(0, 10 * UNIT) { }
	void Step(float h) { StepVTS(h); }
	void AddRigid(const Rigid& rigid);
	Rigid& GetRigid(size_t i) { return rigids[i]; }
	size_t GetNumRigids() const { return rigids.size(); }

	size_t GetNumContacts() const { return contacts.size(); }
	const Contact& GetContact(size_t i) const { return contacts[i]; }

	// TODO: remove
	//float GetRadius() const { return r; }
	//float GetHalfHeight() const { return hh; }

private:
	void Collide();
	void StepVTS(float h);
	void StepPBD(float h);

	void ClosestPtCircleLine(int i, const Vector2& n, const Vector2& p, float rTol, float r);
	void ClosestPtCapsuleLine(int i, const Vector2& n, const Vector2& p, float rTol);
	void ClosestPtCapsuleCapsule(int i, int j, float tol);
	void ClosestPtCircleCircle(int i, int j, float tol);

	void AddContact(int i, ClosestPoints& closestPts);
	void AddCapsuleContact(Vector2 p, Vector2 q, Vector2 n, int i, int j, float safeDist, float dist, float s, float t);

private:
	Vector2 gravity;
	std::vector<Rigid> rigids;
	//float hh, r; // capsule; TODO: shape
	std::vector<Contact> contacts;
};

inline bool CapsuleHalfPlaneCollision(float hh, float r, float rTol, float theta, const Vector2& pos, // cylinder size and transform
									  const Vector2& n, const Vector2& p, // half-plane
									  ClosestPoints& closestPts) // closest points output pair
{
	Vector2 a(-hh, 0.f);
	a.Rotate(theta);
	a += pos;
	Vector2 b(hh, 0.f);
	b.Rotate(theta);
	b += pos;

	float da = n.Dot(a - p);
	float db = n.Dot(b - p);
	if (da <= rTol && da <= db)
	{
		// contact point in local coordinates
		Vector2 q = a - r * n;
		q -= pos;
		q.Rotate(-theta);
		
		closestPts.n = n;
		closestPts.p = p;
		closestPts.q = q;
		closestPts.depth = r - da;
		closestPts.id1 = CID_LEFT;
		return true;
	}
	if (db <= rTol)
	{
		// contact point in local coordinates
		Vector2 q = b - r * n;
		q -= pos;
		q.Rotate(-theta);

		closestPts.n = n;
		closestPts.p = p;
		closestPts.q = q;
		closestPts.depth = r - db;
		closestPts.id1 = CID_RIGHT;
		return true;
	}

	return false;
}

inline void RigidSystem::ClosestPtCircleLine(int i, const Vector2& n, const Vector2& p, float rTol, float r)
{
	float d = n.Dot(rigids[i].pos - p);
	if (d <= rTol)
	{
		// contact point in local coordinates
		Vector2 q = -r * n;
		q.Rotate(-rigids[i].theta);

		Contact contact;
		contact.n = n;
		contact.p = p;
		contact.q = q;
		contact.idx1 = i;
		contact.depth = r - d;
		contact.lambda = 0.f;
		contacts.push_back(contact);
	}
}

inline void RigidSystem::ClosestPtCircleCircle(int i, int j, float tol)
{
	const Sphere* sph1 = (const Sphere*)rigids[i].shape.get();
	const Sphere* sph2 = (const Sphere*)rigids[j].shape.get();

	Vector2 delta = rigids[j].pos - rigids[i].pos;
	float len = delta.Length();
	if (len > 0 && len < sph1->r + sph2->r + tol)
	{
		delta.Scale(1.f / len);
		delta.Flip();

		Vector2 p = -sph1->r * delta;
		p.Rotate(-rigids[i].theta);
		Vector2 q = sph2->r * delta;
		q.Rotate(-rigids[j].theta);

		RigidSystem::Contact contact;
		contact.n = delta;
		contact.q = p; // FIXME!
		contact.p = q;
		contact.idx1 = i;
		contact.idx2 = j;
		contact.depth = -len + sph1->r + sph2->r;
		contact.lambda = 0.f;
		contacts.push_back(contact);
	}
}

inline void RigidSystem::ClosestPtCapsuleLine(int i, const Vector2& n, const Vector2& p, float rTol)
{
	const Capsule& cap = *(Capsule*)rigids[i].shape.get();

	// segment ends in world space
	Vector2 a(-cap.hh, 0.f);
	a.Rotate(rigids[i].theta);
	a += rigids[i].pos;
	Vector2 b(cap.hh, 0.f);
	b.Rotate(rigids[i].theta);
	b += rigids[i].pos;

	float da = n.Dot(a - p);
	float db = n.Dot(b - p);
	if (da <= rTol)
	{
		// contact point in local coordinates
		Vector2 q = a - cap.r * n;
		q -= rigids[i].pos;
		q.Rotate(-rigids[i].theta);

		Contact contact;
		contact.n = n;
		contact.p = p;
		contact.q = q;
		contact.idx1 = i;
		contact.depth = cap.r - da;
		contact.lambda = 0.f;
		contacts.push_back(contact);
	}
	if (db <= rTol)
	{
		// contact point in local coordinates
		Vector2 q = b - cap.r * n;
		q -= rigids[i].pos;
		q.Rotate(-rigids[i].theta);

		Contact contact;
		contact.n = n;
		contact.p = p;
		contact.q = q;
		contact.idx1 = i;
		contact.depth = cap.r - db;
		contact.lambda = 0.f;
		contacts.push_back(contact);
	}
}

inline void RigidSystem::AddRigid(const Rigid& rigid)
{
	Rigid rigid1(rigid);
	if (rigid.shape->type == Shape::CAPSULE)
	{
		const Capsule& cap = *(Capsule*)rigid.shape.get();
		rigid1.inertia = 1.f / ((cap.r + cap.hh) * (cap.r + cap.hh)); // hack - disk inertia
		// TODO: moment of inertia of capsule
	}
	else if (rigid.shape->type == Shape::SPHERE)
	{
		const Sphere* sph = (const Sphere*)rigid.shape.get();
		rigid1.inertia = 1.f / (sph->r * sph->r);
	}
	rigids.push_back(rigid1);
}

inline void RigidSystem::AddContact(int i, ClosestPoints& closestPts)
{
	// TODO: binary search
	bool found = false;
	for (size_t j = 0; j < contacts.size(); j++)
	{
		if (contacts[j].idx1 == i && contacts[j].id1 == closestPts.id1 && contacts[j].id2 == closestPts.id2)
		{
			contacts[j] = closestPts;
			contacts[j].idx1 = i;
			found = true;
			break;
		}
	}

	if (!found)
	{
		Contact contact(closestPts);
		contact.idx1 = i;
		contacts.push_back(contact);
	}
}

#endif