#ifndef PARTICLE_SYSTEM_IVP_H
#define PARTICLE_SYSTEM_IVP_H

#include "ParticleSystemVelocity.h"

// Implicit Velocity Projection

class ParticleSystemImpVel : public ParticleSystemVelocity
{
public:
	void MicroStep() override;
	bool SupportsSolver(SolverType aSolver) override;

private:
	void ProjectVelocitiesImplicit();
    void SolveConstraintsImplicit();
};

#endif // PARTICLE_SYSTEM_IVP_H
