#include "ParticleSystemSph.h"

// TODO: move PI somewehere else like math
#include <Graphics2D/Graphics.h>

/*void ParticleSystemSph::MicroStep()
{
	Collide();
	// resolve collisions
	//for (int k = 0; k < numIterations; ++k)
	//{
	//	// collisions with walls
	//	for (size_t i = 0; i < walls.size(); i++)
	//		SolveContactPgs(walls[i], radius);
	//}
	// compute density
	for (int i = 0; i < GetNumParticles(); i++)
	{
		particles[i].density = 0;
	}
	for (size_t i = 0; i < pairs.size(); i++)
	{
		const int i1 = pairs[i].i1;
		const int i2 = pairs[i].i2;
		const float dist = (particles[i1].pos - particles[i2].pos).Length();
		const float h = 2 * radius;
		const float kernel = 15 * Cube(1 - dist / h) / (PI * Cube(h));
		particles[i1].density += kernel;
		particles[i2].density += kernel;
	}

	// compute pressure force
	const float k = 1;
	const float mu = 0.1f;
	for (int i = 0; i < GetNumParticles(); i++)
	{
		particles[i].pressure = k * particles[i].density;
		particles[i].force.SetZero();
	}
	for (size_t i = 0; i < pairs.size(); i++)
	{
		const int i1 = pairs[i].i1;
		const int i2 = pairs[i].i2;
		Vector2 delta = particles[i1].pos - particles[i2].pos;
		const float dist = delta.Length(); // TODO: keep precomputed
		const float h = 2 * radius;
		if (delta.NormalizeCheck())
		{
			Vector2 gradient = (45 * Sqr(1 - dist / h) / (PI * Quart(h))) * delta;
			// TODO: simplify w.r.t. k
			float weight = 0.5f * (particles[i1].pressure + particles[i2].pressure) / (particles[i1].density * particles[i2].density);
			gradient.Scale(weight);
			particles[i1].force.Add(gradient);
			particles[i2].force.Subtract(gradient);
			float laplacian = 45 * (h - dist) / (PI * Sqr(Cube(h)));
			Vector2 vrel = particles[i2].velocity - particles[i1].velocity;
			weight = -mu * laplacian / (particles[i1].density * particles[i2].density);
			vrel.Scale(weight);
			particles[i1].force.Add(vrel);
			particles[i2].force.Subtract(vrel);
		}
	}

	// integrate
	for (int i = 0; i < GetNumParticles(); i++)
	{
		particles[i].velocity.Add(timeStep * (gravity + particles[i].force));
		particles[i].pos.Add(timeStep * particles[i].velocity);
	}
}*/

void ParticleSystemSph::MicroStep()
{
	//Collide();

	WallCollisions();

	// resolve world collisions
	//for (int k = 0; k < numIterations; ++k)
	//{
	//	// collisions with walls
	//	for (size_t i = 0; i < walls.size(); i++)
	//		SolveContactPgs(walls[i], Scalar(radius));
	//}
	for (size_t i = 0; i < constraints.size(); i++)
	{
		if (constraints[i].type != Constraint::CONTACT)
			continue;
		const int j = constraints[i].i1;
		float len = constraints[i].normal.Dot(particles[j].pos - constraints[i].point);
		if (len > constraints[i].len)
			continue;
		float depth = len - constraints[i].len;
		Vector2 disp = constraints[i].normal * depth;
		particles[j].pos -= disp;
		particles[j].velocity -= (1.f / timeStep) * disp;
	}

	// TODO: not the first frame
	// update velocities
	//Scalar invTimeStep(1.f / timeStep);
	//for (size_t i = 0; i < GetNumParticles(); i++)
	//{
	//	Vector2 vel = particles[i].pos;
	//	vel.Subtract(particles[i].prevPos);
	//	vel.Scale(invTimeStep);
	//	particles[i].velocity = vel;
	//}
	
	// build neighborhood list
	h = 2.2f * radius;
	const float hSqr(h * h);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].neighbours.clear();
		//particles[i].density = w0;
		for (size_t j = i + 1; j < GetNumParticles(); j++)
		{
			Vector2 delta = particles[i].pos - particles[j].pos;
			if (delta.LengthSquared() > hSqr)
				continue;
			particles[i].neighbours.push_back(Neighbour((ParticleIdx)j, delta));
			delta.Flip();
			particles[j].neighbours.push_back(Neighbour((ParticleIdx)i, delta));
		}
	}
	//for (size_t i = 0; i < pairs.size(); i++)
	//{
	//	const int i1 = pairs[i].i1;
	//	const int i2 = pairs[i].i2;
	//	Vector2 delta = particles[i1].pos - particles[i2].pos;
	//	particles[i1].neighbours.push_back(Neighbour(i2, delta));
	//	delta.Scale(Scalar(-1));
	//	particles[i2].neighbours.push_back(Neighbour(i1, delta));
	//}
	//for (size_t i = 0; i < walls.size(); i++)
	//{
	//	const int idx = walls[i].particleIdx;
	//	float dist = walls[i].normal.Dot(particles[idx].pos - walls[i].point);
	//	particles[idx].neighbours.push_back(Neighbour(-1, (radius + dist) * walls[i].normal, 10.f));
	//}

	StepSph1();
}

void ParticleSystemSph::StepSph()
{
	const float mass = 1.f; //PI * h * h / 4.f;
	const float g0 = 45.f / (PI * Quart(h));
	const float w0 = 15.f / (PI * Cube(h));
	const float k = 250.f;

	// compute density
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		for (size_t j = 0; j < particles[i].neighbours.size(); j++)
		{
			particles[i].density = w0 + /*particles[i].neighbours[j].*/mass * w0 * Cube(1.f - particles[i].neighbours[j].dist / h);
		}
	}

	// compute pressure force and integrate
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].pressure = k * particles[i].density;
		particles[i].force.SetZero();
	}

	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		for (size_t j = 0; j < particles[i].neighbours.size(); j++)
		{
			Vector2 delta = particles[i].neighbours[j].r;
			const float dist = particles[i].neighbours[j].dist;
			if (dist > h)
				continue;
			if (dist == 0.f)
				delta.Set(0, -1);
			else
				delta.Scale(Scalar(1.f / dist));
			Vector2 gradient = (g0 * Sqr(1.f - dist / h)) * delta;
			const int idx = particles[i].neighbours[j].idx;
			float weight = 0.5f * /*particles[i].neighbours[j].*/mass * (particles[i].pressure + particles[idx].pressure) / particles[idx].density; // TODO: simplify wrt k
			particles[i].force.Add(weight * gradient);
			particles[idx].force.Subtract(weight * gradient);
		}
		const Scalar ts(timeStep);
		particles[i].force.Scale(Scalar(1.f / particles[i].density));
		particles[i].velocity.Add(ts * (/*gravity + */particles[i].force));
		particles[i].prevPos = particles[i].pos;
		particles[i].pos.Add(ts * particles[i].velocity);
	}
	
}

void ParticleSystemSph::StepSph1()
{
	// Mueller method from Sjostrom

	// tweakable
	//lengthScale = 50.f;
	const float gasConst = 300.f;
	const float rho0 = 1000; // reference density
	const float mu = 25.f;

	h = h / lengthScale; // in meters
	const float h2 = h * h;
	const float h4 = h2 * h2;
	const float h5 = h4 * h;
	const float h8 = h4 * h4;
	const float mass = 0.25f * rho0 * PI * h2; // TODO: should be area of circle times ref density
	const float cpoly6 = 4.f / PI / h8;
	const float cspiky = -30.f / (PI * h5);
	const float cvisc = 360.f / (29.f * PI * h5);
	// zero out accumulators
	for (size_t i = 0; i < particles.size(); i++)
	{
		particles[i].density = 0.f;
		particles[i].force.SetZero(); // actually an acceleration here
		// add self to neighbours
		particles[i].neighbours.push_back(Neighbour((ParticleIdx)i, Vector2(0)));
	}
	// compute density and pressure
	for (size_t i = 0; i < particles.size(); i++)
	{
		// interpolate density
		for (size_t jj = 0; jj < particles[i].neighbours.size(); jj++)
		{
			// TODO: special code for self contribution
			size_t j = particles[i].neighbours[jj].idx;
			if (j < i) // TODO: only add larger indices to neighbor list
				continue; 
			float r = particles[i].neighbours[jj].dist / lengthScale; // in meters
			float d = h2 - r * r;
			float d3 = d * d * d;
			float wpoly6 = cpoly6 * d3;
			float inc = mass * wpoly6;
			particles[i].density += inc;
			if (i != j)
				particles[j].density += inc;
		}
		// compute pressure
		particles[i].pressure = gasConst * (particles[i].density - rho0);
	}
	// compute accelerations
	for (size_t i = 0; i < particles.size(); i++)
	{
		// interpolate acceleration
		for (size_t jj = 0; jj < particles[i].neighbours.size(); jj++)
		{
			size_t j = particles[i].neighbours[jj].idx;
			if (j <= i)
				continue;
			float r = particles[i].neighbours[jj].dist / lengthScale; // in meters
			float d = h - r;
			float rho = (particles[i].density * particles[j].density);
			// viscosity term
			//Vector2 v = particles[j].velocity - particles[i].velocity;
			//float wvisc = cvisc * d;
			//float weight = mu * mass * wvisc / rho;
			//Vector2 a = weight * v;
			Vector2 a;
			// pressure gradient term
			Vector2 dir = particles[i].pos - particles[j].pos;
			if (dir.NormalizeCheck())
			{
				float wspiky = -cspiky * d * d;
				float weight = 0.5f * mass * (particles[i].pressure + particles[j].pressure) / rho;
				a += weight * wspiky * dir;
			}

			particles[i].force += a;
			particles[j].force -= a;
		}
	}
	// integrate motion (Symplectic Euler)
	for (size_t i = 0; i < particles.size(); i++)
	{
		particles[i].velocity += timeStep * lengthScale * (particles[i].force + gravity);
		particles[i].pos += timeStep * particles[i].velocity;
	}
}