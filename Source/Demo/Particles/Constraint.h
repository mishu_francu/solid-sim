#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include <Engine/Types.h>
#include <Math/Vector2.h>
#include <vector>

typedef uint16 ParticleIdx;

template<typename Vector>
struct ConstraintTemplate
{
	enum ConstraintType
	{
		DUMMY = 0,
		LINK,
		CONTACT,
		COLL_PAIR,
		MOUSE_SPRING,
		EDGE,
		TRIANGLE,
		SELF_TRIANGLE,
		SELF_EDGES
	};

	ConstraintType type; // TODO: uint8
	ParticleIdx i1, i2; // indices of the two bodies
	float len; // rest length
	float lambda; // for when explicitly computing lambda
	// TODO: separate out vector members
	Vector disp; // Jacobi only
	Vector point; // contact point
	Vector normal; // contact normal
	//static float radius;
	float stiffness; // for shear and bend links
	int batch; // for parallel Gauss-Seidel
	// triangle
	ParticleIdx i3;
	float w1, w2, w3;
	// self collision
	ParticleIdx i4;
	float depth;
	//bool flip;
	// friction
	float lambdaF1, lambdaF2;
	Vector t1, t2;	
	 // for MR/CR
	float dLambda;
	float err;
	float dLambdaF1, dLambdaF2;
	float y; // Nesterov

	ConstraintTemplate() : type(DUMMY), i1(0), i2(1), len(0), stiffness(1), batch(-1) { }
	//ConstraintTemplate(ParticleIdx a, ParticleIdx b) : type(COLL_PAIR), i1(a), i2(b), len(2 * radius), stiffness(1), batch(-1) { } // TODO: all default values
	ConstraintTemplate(ConstraintType t, ParticleIdx a, ParticleIdx b, float l) : type(t), i1(a), i2(b), len(l), stiffness(1), batch(-1) { }
	ConstraintTemplate(ParticleIdx i, const Vector& p, const Vector& n, float radius) : type(CONTACT), i1(i), i2(0xffff), point(p), normal(n), len(radius), stiffness(1), batch(-1) { } 
};

typedef ConstraintTemplate<Math::Vector2> Constraint;
typedef std::vector<Constraint> ConstraintVector;

#endif // CONSTRAINT_H