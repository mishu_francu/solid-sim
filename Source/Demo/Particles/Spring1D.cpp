#include "Spring1D.h"

#include <math.h>

Spring1D::Spring1D(float w, float x0, float v0) : omega(w), pos(x0), vel(v0), time(0)
{
	ASSERT(x0 != 0);
	if (v0 != 0)
	{
		phase = atanf(-v0 / (omega * x0));
		amp = x0 / cosf(phase);
	}
	else
	{
		phase = 0;
		amp = x0;
	}
}

void Spring1D::StepAnalytic(float dt)
{
	time += dt;
	float arg = omega * time + phase; // TODO: incremental
	pos = amp * cosf(arg);
	vel = -omega * amp * sinf(arg);
}

void Spring1D::StepExplicitEuler(float dt)
{
	float v = vel;
	vel -= dt * omega * omega * pos;
	pos += dt * v;
}

void Spring1D::StepSymplecticEuler(float dt)
{
	vel -= dt * omega * omega * pos;
	pos += dt * vel;
}

void Spring1D::StepImplicitEuler(float dt)
{
	float k = omega * omega;
	float alpha = 1.f / (k * dt * dt + 1);
	float v = vel;
	vel = (v - dt * k * pos) * alpha;
	pos = (pos + dt * v) * alpha;
}

void Spring1D::StepConstraintVelocity(float dt)
{
	// 0 = vel + beta * pos / h + (h + gamma) * lambda
	const float beta = 1.f;
	const float gamma = 0.01f;
	float lambda = -(vel + beta * pos / dt) / (dt + gamma);
	// Symplectic Euler
	vel += dt * lambda;
	pos += dt * vel;
}

void Spring1D::StepConstraintPosition(float dt)
{
	// 0 = pos + h * vel + h^2 * lambda;
	float lambda = -(vel + pos / dt);
	// Symplectic Euler
	vel += lambda;
	pos += dt * vel;
}

void SpringDamped1D::StepExplicitEuler(float h)
{
	float v0 = v;
	v -= h * (c * v + k * x) / m;
	x += h * v0;
}

void SpringDamped1D::StepSymplecticEuler(float h)
{
	v -= h * (c * v + k * x) / m;
	x += h * v;
}

void SpringDamped1D::StepImplicitEuler(float h)
{
	float v0 = v;
	float alpha = 1.f / (1 + h * c / m + h * h * k / m);
	v = (v - h * k * x / m) * alpha;
	x = (x + h * v0) * alpha;
}
