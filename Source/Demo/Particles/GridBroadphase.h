#ifndef GRID_BROADPHASE_H
#define GRID_BROADPHASE_H

#include <Math/Vector2.h>
#include <Demo/Particles/Collision.h>
#include <Demo/Particles/Constraint.h>

class GridBroadphase
{
private:
	struct ParticleInCell
	{
		int particleId;
		int cellId;

		ParticleInCell() : particleId(0), cellId(0) { }
		ParticleInCell(int p, int c) : particleId(p), cellId(c) { }
		bool operator < (const ParticleInCell& other) const { return cellId < other.cellId; }
	};	

	int width, height;
	float cellSize;
	int* cells;
	std::vector<ParticleInCell> particles;

public:
	GridBroadphase() : cells(NULL) { }
	
	~GridBroadphase()
	{
		if (cells != NULL)
			delete cells;
	}

	void Init(float w, float h, float cellSize);

	void Update(const AabbVector& handles, ConstraintVector& pairs);

	void TestObject(const Aabb& aabb, std::vector<int>& particles);

private:
	void TestCell(const AabbVector& handles, ConstraintVector& pairs, int count, int start, int x, int y);
};

#endif // GRID_BROADPHASE_H