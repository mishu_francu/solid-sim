#ifndef PARTICLE_SYSTEM_SPH_H
#define PARTICLE_SYSTEM_SPH_H

#include "ParticleSystem.h"

class ParticleSystemSph : public ParticleSystem<ParticleFluid>
{
public:
	ParticleSystemSph()
	{
		SetNumMicroSteps(1);
		numIterations = 5;
	}

	void MicroStep() override;
	bool SupportsSolver(SolverType aSolver) override { return true; }

private:
	void StepSph();
	void StepSph1();
private:
	float h; // kernel width
};

#endif // PARTICLE_SYSTEM_SPH_H