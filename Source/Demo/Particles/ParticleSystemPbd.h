#ifndef PARTICLE_SYSTEM_PBD_H
#define PARTICLE_SYSTEM_PBD_H

#include "ParticleSystemVelocity.h"
#include "Projector.h"
#ifndef ANDROID_NDK
//#	include "ProjectorCL.h"
#endif

// TODO: put inside class or namespace
enum IntegratorType
{
	EULER,
	SYMPLECTIC_EULER,
	VELOCITY_VERLET,
	LEAPFROG
};

template <int INTEGRATOR = SYMPLECTIC_EULER>
class ParticleSystemPbd : public ParticleSystemVelocity
{
public:
	ParticleSystemPbd()
	{
#ifdef USE_OPENCL
		projCL.Init();
#endif
	}

	void Init(float w, float h, float r) override
	{
		ParticleSystem::Init(w, h, r);
		firstTime = true;
	}

	void MicroStep() override;
	bool SupportsSolver(SolverType solver) override;

private:
	void Integrate();
	void MicroStepSoft();
	void ProjectPositionsSoft(float tau);
	void MicroStepNewmark();
	// extra damping
	void MicroStepSoft1();
	void ProjectPositionsSoft1();
	// implicit midpoint
	void MicroStepSoft2();
	void ProjectPositionsSoft2();
	// Newmark
	void MicroStepSoft3();
	void ProjectPositionsSoft3();

private:
	bool firstTime;
	Projector<ParticleVelocity> proj;

#if !defined(ANDROID_NDK) && defined(ENABLE_CL)
	MinResProjectorCL<Vector2, ParticleVelocity> projCL;
#endif
};

#endif // PARTICLE_SYSTEM_PBD_H
