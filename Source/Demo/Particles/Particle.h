#ifndef PARTICLE_H
#define PARTICLE_H

#include <Math/Vector2.h>
#include <Demo/Particles/Collision.h>
#include <Demo/Particles/Constraint.h>

enum ParticleType
{
	PARTICLE_BASE,
	PARTICLE_VERLET,
	PARTICLE_VELOCITY,
	PARTICLE_ACCELERATION,
	PARTICLE_FLUID
};

template<typename Vector>
struct ParticleTemplate
{
	enum { type = PARTICLE_BASE };
	Vector pos;
	float invMass;
	ParticleTemplate() : invMass(1.f) 
	{
		pos.SetZero();
	}
	void SetMass(float m) { invMass = m; }
	void GetAabb(Vector& min, Vector& max, const Vector& rad)
	{
		min = pos - rad; 
		max = pos + rad;
	}
};

typedef ParticleTemplate<Vector2> ParticleBase;

template<typename Vector>
struct ParticleVerletT : ParticleTemplate<Vector>
{
	enum { type = PARTICLE_VERLET };
	Vector prevPos;
	ParticleVerletT()
	{
		prevPos.SetZero();
	}
	void SetVelocity(const Vector& speed, float timeStep)
	{
		prevPos = this->pos;
		Vector vel = speed;
		vel.Scale(timeStep);
		prevPos.Subtract(vel);
	}
	Vector GetVelocity() const { return this->pos - prevPos; } // TODO: divided by time step
};

typedef ParticleVerletT<Vector2> ParticleVerlet;

template<typename Vector>
struct ParticleVelocityT : ParticleVerletT<Vector> // prevPos is only needed if approximating velocities
{
	enum { type = PARTICLE_VELOCITY };
	Vector velocity;
	Vector prevVel;

	ParticleVelocityT()
	{
		velocity.SetZero();
		prevVel.SetZero();
	}
	void SetVelocity(const Vector& vel, float timeStep) 
	{
		velocity = vel; 
	}
	const Vector& GetVelocity() const { return velocity; }
};

typedef ParticleVelocityT<Vector2> ParticleVelocity;

struct ParticleAcceleration : ParticleVelocity
{
	enum { type = PARTICLE_ACCELERATION };
	Vector2 acceleration;
};

// TODO: correct spelling
struct Neighbour
{
	ParticleIdx idx;
	Vector2 r;
	float dist;
	float mass; // not used

	Neighbour(ParticleIdx i, const Vector2& delta, float m = 1.f)
	{
		idx = i;
		r = delta;
		dist = (float)r.Length();
		mass = m;
	}
};

struct ParticleFluid : ParticleVelocity
{
	enum { type = PARTICLE_FLUID };
	float density;
	float pressure;
	Vector2 force;
	std::vector<Neighbour> neighbours;
};

struct SparseElement
{
	size_t i, j; // incident constraints indices
	float a; // value of matrix element (inverse mass)
	float dot; // dot between constraint directions
	float val;

	SparseElement() : i(0), j(0), a(0), dot(0), val(0) { }
};


// for OpenCL and CUDA
enum { MAX_INCIDENCE = 16 };
struct Incidence
{
	int info[MAX_INCIDENCE];
};

// TODO: rename to link
struct Edge
{
	// TODO: derive Constraint from it?
	int i1, i2, type;
	float len, stiff;

	Edge() : i1(-1), i2(-1), type(0), len(0), stiff(1.f) { }
};

#endif // PARTICLE_H