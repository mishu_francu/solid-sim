#include "ParticleSystemPenalty.h"

#include <Math/Matrix2.h>
#include <Math/Matrix.h>
#include <Eigen/Dense>

// TODO: implicit midpoint and iterative solver (PCG)

void ParticleSystemPenalty::MicroStep()
{
	//stiffness = 4.f / (timeStep * timeStep);
	//Integrate();
	IntegrateImplicit();
	//IntegrateImplicitDense();
	//IntegrateImplicitCG();
	//IntegrateMidpoint();
	//IntegrateMidpointDv();
	//IntegrateMidpointDense();
	//IntegrateMidpointCG();
	// TODO: per link stiffness (cloth)

	// TODO: only first microstep
	Collide();
	//PrintMetrics();
}

void ParticleSystemPenalty::PrintMetrics()
{
	// compute and print constraint error
	float err = 0;
	float vel = 0;
	for (size_t i = 0; i < constraints.size(); i++)
	{
		Constraint& pair = constraints[i];
		ParticleVelocity* p1 = &particles[pair.i1];
		if (pair.type == Constraint::COLL_PAIR)
		{			
			ParticleVelocity* p2 = &particles[pair.i2];
			Vector2 n = p1->pos - p2->pos;
			float len = n.Length();
			err += max(0.f, pair.len - len);
			n.Normalize();
			vel += max(0.f, n.Dot(p2->velocity - p1->velocity));
		}
		else if (pair.type == Constraint::CONTACT)
		{
			err += max(0.f, pair.len - pair.normal.Dot(p1->pos - pair.point));
			vel += max(0.f, pair.normal.Dot(p1->velocity));
		}
	}	

	// compute and print kinetic energy
	float kin = 0;
	for (size_t i = 0; i < particles.size(); i++)
	{
		if (particles[i].invMass != 0)
			kin += particles[i].velocity.LengthSquared() / particles[i].invMass;
	}
	//Printf("%f,%f,%f\n", err, vel, kin);
	Printf("%f\n", kin);

	nFrames++;
	if (nFrames > 500)
		Engine::getInstance()->Quit();
}

void ParticleSystemPenalty::Integrate()
{
	PROFILE_SCOPE("IntegrateSymplectic");

	// Symplectic Euler
	const float ts = timeStep;
	Vector2 g = gravity;
	g.Scale(lengthScale * timeStep);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		Vector2 add;
		// increment velocities
		particles[i].velocity += g + ts * particles[i].acceleration;
		// increment positions
		add = particles[i].velocity;
		add.Scale(ts);
		particles[i].pos.Add(add);
		// zero force accumulator
		particles[i].acceleration.SetZero();
	}

	AddPenalties();
}

void ParticleSystemPenalty::IntegrateImplicit()
{
	// Mueller style
	PROFILE_SCOPE("IntegrateImplicit");
	// TODO: make global?
	static ParticleAcceleration ground; 
	ground.invMass = 0;
	ground.velocity.SetZero();

	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].acceleration.SetZero(); // it's actually a force
	}

	// TODO: different stiffness per each link
	AddPenalties();

	// build matrix and rhs
	Eigen::VectorXf b(GetNumParticles() * 2);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		const int ii = i * 2;
		Vector2 v = particles[i].velocity + timeStep * (particles[i].invMass * particles[i].acceleration + gravity * lengthScale);
		b[ii] = v.GetX();
		b[ii + 1] = v.GetY();
	}
	Eigen::MatrixXf mat = Eigen::MatrixXf::Identity(GetNumParticles() * 2, GetNumParticles() * 2);
	const float c = stiffness * timeStep * timeStep;
	for (size_t k = 0; k < constraints.size(); k++)
	{
		ParticleAcceleration* p1 = &particles[constraints[k].i1];
		ParticleAcceleration* p2 = NULL;
		if (constraints[k].type == Constraint::CONTACT)
		{
			p2 = &ground;
			p2->pos = constraints[k].point;
		}
		else
		{
			p2 = &particles[constraints[k].i2];
		}
		Vector2 delta = p1->pos;
		delta.Subtract(p2->pos);
		float len = constraints[k].type == Constraint::CONTACT ? delta.Dot(constraints[k].normal) : delta.Length();
		if (constraints[k].type != Constraint::LINK && len > constraints[k].len)
			continue;
		//delta.Normalize();
		if (constraints[k].type == Constraint::CONTACT)
			delta = constraints[k].normal;
		else
			delta.Scale(1 / len);

		const int i = constraints[k].i1;
		const int j = constraints[k].i2;
		Matrix2 T = Matrix2::TensorProduct(delta, delta);
		const float f = constraints[k].len / len; // TODO: precompute
		float ci = stiffness * links[i].stiffness;
		Matrix2 K = c * (f - 1) * Matrix2::Identity() - c * f * T;
		Matrix2 D = -damping * timeStep * T;
		K = K + D;
		const int ii = i * 2;
		const int jj = j * 2;

		mat(ii, ii) -= particles[i].invMass * K.a11;
		mat(ii, ii + 1) -= particles[i].invMass * K.a12;
		mat(ii + 1, ii) -= particles[i].invMass * K.a21;
		mat(ii + 1, ii + 1) -= particles[i].invMass * K.a22;

		if (constraints[k].type == Constraint::CONTACT)
			continue;

		mat(ii, jj) += particles[i].invMass * K.a11;
		mat(ii, jj + 1) += particles[i].invMass * K.a12;
		mat(ii + 1, jj) += particles[i].invMass * K.a21;
		mat(ii + 1, jj + 1) += particles[i].invMass * K.a22;

		mat(jj, jj) -= particles[j].invMass * K.a11;
		mat(jj, jj + 1) -= particles[j].invMass * K.a12;
		mat(jj + 1, jj) -= particles[j].invMass * K.a21;
		mat(jj + 1, jj + 1) -= particles[j].invMass * K.a22;

		mat(jj, ii) += particles[j].invMass * K.a11;
		mat(jj, ii + 1) += particles[j].invMass * K.a12;
		mat(jj + 1, ii) += particles[j].invMass * K.a21;
		mat(jj + 1, ii + 1) += particles[j].invMass * K.a22;
	}

	Eigen::VectorXf V = mat.colPivHouseholderQr().solve(b);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		const int ii = i * 2;		
		particles[i].velocity = Vector2(V[ii], V[ii + 1]);;
		particles[i].pos += timeStep * particles[i].velocity;
	}
	// TODO: more Newton steps
}

void ParticleSystemPenalty::IntegrateImplicitDense()
{
	PROFILE_SCOPE("IntegrateImplicitDense");

	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		const int ii = i * 2;
		particles[i].velocity += timeStep * gravity * lengthScale;
		particles[i].pos += timeStep * particles[i].velocity;
	}

	std::vector<float> v(GetNumParticles() * 2);
	//for (int iter = 0; iter < 2; iter++) // Newton iterations
	{

	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].acceleration.SetZero(); // it's actually a force
	}

	// TODO: different stiffness per each link
	for (size_t i = 0; i < links.size(); i++)
	{
		Constraint& link = links[i];
		ParticleAcceleration& p1 = particles[link.i1];
		ParticleAcceleration& p2 = particles[link.i2];			
		Vector2 delta = p2.pos - p1.pos;
		float len = delta.Length();
		delta.Normalize();
		float dLen = len - link.len;
		Vector2 force = links[i].stiffness * stiffness * dLen * delta;
		p1.acceleration.Add(force);
		p2.acceleration.Subtract(force);
	}

	// build matrix and rhs
	std::vector<float> b(GetNumParticles() * 2);
	MatrixSqr<float> mat(GetNumParticles() * 2);
	mat.SetIdentity();
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		const int ii = i * 2;
		Vector2 rhs = timeStep * particles[i].invMass * particles[i].acceleration;
		b[ii] = rhs.GetX();
		b[ii + 1] = rhs.GetY();
	}
	const float c = stiffness * timeStep * timeStep;
	for (size_t k = 0; k < links.size(); k++)
	{
		// TODO: damping
		const int i = links[k].i1;
		const int j = links[k].i2;
		Vector2 delta = particles[j].pos - particles[i].pos;
		const float len = delta.Length();
		delta.Normalize();
		Matrix2 T = Matrix2::TensorProduct(delta, delta);
		const float f = links[k].len / len;
		float ci = c * links[k].stiffness;
		Matrix2 K = ci * (f - 1) * Matrix2::Identity() - ci * f * T;
		//Matrix2 K = - ci * T; // doesn't work with hanging cloth
		const int ii = i * 2;
		const int jj = j * 2;

		mat(ii, ii) -= particles[i].invMass * K.a11;
		mat(ii, ii + 1) -= particles[i].invMass * K.a12;
		mat(ii + 1, ii) -= particles[i].invMass * K.a21;
		mat(ii + 1, ii + 1) -= particles[i].invMass * K.a22;

		mat(ii, jj) += particles[i].invMass * K.a11;
		mat(ii, jj + 1) += particles[i].invMass * K.a12;
		mat(ii + 1, jj) += particles[i].invMass * K.a21;
		mat(ii + 1, jj + 1) += particles[i].invMass * K.a22;

		mat(jj, jj) -= particles[j].invMass * K.a11;
		mat(jj, jj + 1) -= particles[j].invMass * K.a12;
		mat(jj + 1, jj) -= particles[j].invMass * K.a21;
		mat(jj + 1, jj + 1) -= particles[j].invMass * K.a22;

		mat(jj, ii) += particles[j].invMass * K.a11;
		mat(jj, ii + 1) += particles[j].invMass * K.a12;
		mat(jj + 1, ii) += particles[j].invMass * K.a21;
		mat(jj + 1, ii + 1) += particles[j].invMass * K.a22;
	}

	//SolveGauss(mat, b, v);
	//SolveGaussSeidel(mat, b, v, 20);
	SolveConjugateGradient(mat, b, v, 10);

	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		const int ii = i * 2;
		Vector2 dv = Vector2(v[ii], v[ii + 1]);
		particles[i].velocity += dv;
		particles[i].pos += timeStep * dv;
	}

	}
}

void ParticleSystemPenalty::IntegrateImplicitCG()
{
	PROFILE_SCOPE("IntegrateImplicitCG");

	// unconstrained step
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += timeStep * gravity * lengthScale;
		particles[i].pos += timeStep * particles[i].velocity;
		//particles[i].prevPos = particles[i].pos;
		particles[i].prevPos = particles[i].velocity;
	}

	// add penalty forces; TODO: use AddPenalties
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].acceleration.SetZero(); // it's actually a force
	}
	for (size_t i = 0; i < links.size(); i++)
	{
		Constraint& link = links[i];
		ParticleAcceleration& p1 = particles[link.i1];
		ParticleAcceleration& p2 = particles[link.i2];			
		Vector2 delta = p2.pos - p1.pos;
		float len = delta.Length();
		delta.Normalize();
		links[i].normal = delta;
		float dLen = len - link.len;
		Vector2 force = links[i].stiffness * stiffness * dLen * delta; // TODO: damping
		p1.acceleration.Add(force);
		p2.acceleration.Subtract(force);
	}

	// build rhs
	std::vector<Vector2> r(GetNumParticles());
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		r[i] = timeStep * particles[i].invMass * particles[i].acceleration;
	}
	const float c = stiffness * timeStep * timeStep;

	// conjugate gradient
	std::vector<Vector2> dv(GetNumParticles()); // the unknown
	float delta = r * r;
	std::vector<Vector2> d(r); // conjugate gradient directions
	std::vector<float> y(links.size()); // used for computing J * d
	std::vector<Vector2> z(GetNumParticles()); // used for computing J^T * y
	for (int iter = 0; iter < numIterations; iter++)
	{
		if (delta == 0.f)
			break;

		// TODO: damping
		// compute y = h^2 * k * J * d
		for (size_t i = 0; i < links.size(); i++)
		{
			y[i] = links[i].stiffness * links[i].normal.Dot(d[links[i].i2] - d[links[i].i1]);
		}
		// compute z = W * J^T * y
		memset(&z[0], 0, sizeof(Vector2) * GetNumParticles());
		for (size_t i = 0; i < links.size(); i++)
		{
			Vector2 add = y[i] * links[i].normal;
			z[links[i].i1] += particles[links[i].i1].invMass * add;
			z[links[i].i2] -= particles[links[i].i2].invMass * add;
		}
		// compute alpha
		z = d - c * z; // z = S * d
		float den = d * z;
		float alpha = delta / den;
		//Printf("alpha%d=%f\n", iter, alpha);

		dv = dv + alpha * d;
		//r = r - alpha * z;
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (particles[i].invMass != 0)				
				particles[i].pos += timeStep * alpha * d[i];
			particles[i].acceleration.SetZero(); // it's actually a force
		}
		for (size_t i = 0; i < links.size(); i++)
		{
			Constraint& link = links[i];
			ParticleAcceleration& p1 = particles[link.i1];
			ParticleAcceleration& p2 = particles[link.i2];			
			Vector2 delta = p2.pos - p1.pos;
			float len = delta.Length();
			delta.Normalize();
			links[i].normal = delta;
			float dLen = len - link.len;
			Vector2 force = links[i].stiffness * stiffness * dLen * delta; // TODO: damping
			p1.acceleration.Add(force);
			p2.acceleration.Subtract(force);
		}
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			r[i] = particles[i].prevPos - particles[i].velocity + timeStep * particles[i].invMass * particles[i].acceleration;
		}

		float delta1 = r * r;
		float beta = delta1 / delta;
		delta = delta1;
		d = r + beta * d;
	}

	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		particles[i].velocity += dv[i];
		//particles[i].pos += timeStep * dv[i];
		//particles[i].pos = particles[i].prevPos; // strange energy preserving
	}
}

void ParticleSystemPenalty::IntegrateMidpoint()
{
	PROFILE_SCOPE("IntegrateMidpoint");
	// Mueller style
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].acceleration.SetZero(); // it's actually a force
	}

	for (size_t i = 0; i < links.size(); i++)
	{
		Constraint& link = links[i];
		ParticleAcceleration& p1 = particles[link.i1];
		ParticleAcceleration& p2 = particles[link.i2];			
		Vector2 delta = p2.pos - p1.pos;
		float len = delta.Length();
		delta.Normalize();
		float dLen = len - link.len;
		Vector2 v = p2.velocity - p1.velocity;
		float vRel = v.Dot(delta);
		Vector2 force = (links[i].stiffness * stiffness * dLen + damping * vRel) * delta;
		p1.acceleration.Add(force);
		p2.acceleration.Subtract(force);
	}

	// build matrix and rhs
	Eigen::VectorXf b(GetNumParticles() * 2);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		const int ii = i * 2;
		Vector2 v = particles[i].velocity + timeStep * (particles[i].invMass * particles[i].acceleration + gravity * lengthScale);
		b[ii] = v.GetX();
		b[ii + 1] = v.GetY();
	}
	Eigen::MatrixXf mat = Eigen::MatrixXf::Identity(GetNumParticles() * 2, GetNumParticles() * 2);
	const float c = 0.25f * stiffness * timeStep * timeStep;
	for (size_t k = 0; k < links.size(); k++)
	{
		const int i = links[k].i1;
		const int j = links[k].i2;
		Vector2 delta = particles[j].pos - particles[i].pos;
		const float len = delta.Length();
		delta.Normalize();
		Matrix2 T = Matrix2::TensorProduct(delta, delta);
		const float f = links[k].len / len;
		float ck = links[k].stiffness * c;
		Matrix2 K0 = ck * ((f - 1) * Matrix2::Identity() - f * T);
		Matrix2 D = -0.5f * damping * timeStep * T;
		Matrix2 K = K0 + D;
		const int ii = i * 2;
		const int jj = j * 2;

		mat(ii, ii) -= particles[i].invMass * K.a11;
		mat(ii, ii + 1) -= particles[i].invMass * K.a12;
		mat(ii + 1, ii) -= particles[i].invMass * K.a21;
		mat(ii + 1, ii + 1) -= particles[i].invMass * K.a22;

		mat(ii, jj) += particles[i].invMass * K.a11;
		mat(ii, jj + 1) += particles[i].invMass * K.a12;
		mat(ii + 1, jj) += particles[i].invMass * K.a21;
		mat(ii + 1, jj + 1) += particles[i].invMass * K.a22;

		mat(jj, jj) -= particles[j].invMass * K.a11;
		mat(jj, jj + 1) -= particles[j].invMass * K.a12;
		mat(jj + 1, jj) -= particles[j].invMass * K.a21;
		mat(jj + 1, jj + 1) -= particles[j].invMass * K.a22;

		mat(jj, ii) += particles[j].invMass * K.a11;
		mat(jj, ii + 1) += particles[j].invMass * K.a12;
		mat(jj + 1, ii) += particles[j].invMass * K.a21;
		mat(jj + 1, ii + 1) += particles[j].invMass * K.a22;

		Matrix2 K1 = K0 - D;
		Vector2 vi = particles[i].invMass * K1 * (particles[i].velocity - particles[j].velocity);
		b[ii] += vi.GetX();
		b[ii + 1] += vi.GetY();

		Vector2 vj = particles[j].invMass * K1 * (particles[j].velocity - particles[i].velocity);
		b[jj] += vj.GetX();
		b[jj + 1] += vj.GetY();
	}

	Eigen::VectorXf V = mat.colPivHouseholderQr().solve(b);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		const int ii = i * 2;
		Vector2 v(V[ii], V[ii + 1]);
		particles[i].pos += 0.5f * timeStep * (v + particles[i].velocity);
		particles[i].velocity = v;
	}
}

void ParticleSystemPenalty::IntegrateMidpointDv()
{
	PROFILE_SCOPE("IntegrateMidpointDv");
	// BW style
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].acceleration.SetZero(); // it's actually a force
	}

	for (size_t i = 0; i < links.size(); i++)
	{
		Constraint& link = links[i];
		ParticleAcceleration& p1 = particles[link.i1];
		ParticleAcceleration& p2 = particles[link.i2];			
		Vector2 delta = p2.pos - p1.pos;
		float len = delta.Length();
		delta.Normalize();
		float dLen = len - link.len;
		Vector2 v = p2.velocity - p1.velocity;
		float vRel = v.Dot(delta);
		Vector2 force = (links[i].stiffness * stiffness * dLen + damping * vRel) * delta;
		p1.acceleration.Add(force);
		p2.acceleration.Subtract(force);
	}

	// build matrix and rhs
	Eigen::VectorXf b(GetNumParticles() * 2);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		const int ii = i * 2;
		Vector2 v = timeStep * (particles[i].invMass * particles[i].acceleration + gravity * lengthScale);
		b[ii] = v.GetX();
		b[ii + 1] = v.GetY();
	}
	Eigen::MatrixXf mat = Eigen::MatrixXf::Identity(GetNumParticles() * 2, GetNumParticles() * 2);
	const float c = 0.25f * stiffness * timeStep * timeStep;
	for (size_t k = 0; k < links.size(); k++)
	{
		const int i = links[k].i1;
		const int j = links[k].i2;
		Vector2 delta = particles[j].pos - particles[i].pos;
		const float len = delta.Length();
		delta.Normalize();
		Matrix2 T = Matrix2::TensorProduct(delta, delta);
		const float f = links[k].len / len;
		float ck = links[k].stiffness * c;
		Matrix2 K = ck * ((f - 1) * Matrix2::Identity() - f * T);
		//Matrix2 K0 = -ck * T;
		//Matrix2 D = -0.5f * damping * timeStep * T;
		//Matrix2 K = K0 + D;
		const int ii = i * 2;
		const int jj = j * 2;

		mat(ii, ii) -= particles[i].invMass * K.a11;
		mat(ii, ii + 1) -= particles[i].invMass * K.a12;
		mat(ii + 1, ii) -= particles[i].invMass * K.a21;
		mat(ii + 1, ii + 1) -= particles[i].invMass * K.a22;

		mat(ii, jj) += particles[i].invMass * K.a11;
		mat(ii, jj + 1) += particles[i].invMass * K.a12;
		mat(ii + 1, jj) += particles[i].invMass * K.a21;
		mat(ii + 1, jj + 1) += particles[i].invMass * K.a22;

		mat(jj, jj) -= particles[j].invMass * K.a11;
		mat(jj, jj + 1) -= particles[j].invMass * K.a12;
		mat(jj + 1, jj) -= particles[j].invMass * K.a21;
		mat(jj + 1, jj + 1) -= particles[j].invMass * K.a22;

		mat(jj, ii) += particles[j].invMass * K.a11;
		mat(jj, ii + 1) += particles[j].invMass * K.a12;
		mat(jj + 1, ii) += particles[j].invMass * K.a21;
		mat(jj + 1, ii + 1) += particles[j].invMass * K.a22;

		//Matrix2 K1 = K0 - D;
		Vector2 vi = 2 * particles[i].invMass * K * (particles[i].velocity - particles[j].velocity);
		b[ii] += vi.GetX();
		b[ii + 1] += vi.GetY();

		Vector2 vj = 2 * particles[j].invMass * K * (particles[j].velocity - particles[i].velocity);
		b[jj] += vj.GetX();
		b[jj + 1] += vj.GetY();
	}

	Eigen::VectorXf V = mat.colPivHouseholderQr().solve(b);
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass == 0)
			continue;
		const int ii = i * 2;
		Vector2 dv(V[ii], V[ii + 1]);
		particles[i].pos += timeStep * (particles[i].velocity + 0.5f * dv);
		particles[i].velocity += dv;
	}
}

void ParticleSystemPenalty::IntegrateMidpointDense()
{
	PROFILE_SCOPE("IntegrateMidpointDense");
	// BW style
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].acceleration.SetZero(); // it's actually a force
	}

	for (size_t i = 0; i < links.size(); i++)
	{
		Constraint& link = links[i];
		ParticleAcceleration& p1 = particles[link.i1];
		ParticleAcceleration& p2 = particles[link.i2];			
		Vector2 delta = p2.pos - p1.pos;
		float len = delta.Length();
		delta.Normalize();
		float dLen = len - link.len;
		//Vector2 v = p2.velocity - p1.velocity;
		//float vRel = v.Dot(delta);
		Vector2 force = (links[i].stiffness * stiffness * dLen /*+ damping * vRel*/) * delta;
		p1.acceleration.Add(force);
		p2.acceleration.Subtract(force);
	}

	// explicit part of the integration step
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass != 0)
		{
			particles[i].pos += timeStep * particles[i].velocity + 0.5f * timeStep * timeStep * (lengthScale * gravity + 0.5f * particles[i].acceleration);
			particles[i].velocity += timeStep * lengthScale * gravity + 0.5f * timeStep * particles[i].acceleration;
		}
		particles[i].prevPos = particles[i].velocity; // store this velocity
		particles[i].acceleration.SetZero();
	}

	// Newton iterations
	for (int iter = 0; iter < 10; iter++)
	{
		// compute latest forces and jacobian
		for (size_t i = 0; i < links.size(); i++)
		{
			Constraint& link = links[i];
			ParticleAcceleration& p1 = particles[link.i1];
			ParticleAcceleration& p2 = particles[link.i2];			
			Vector2 delta = p2.pos - p1.pos;
			float len = delta.Length();
			delta.Normalize();
			float dLen = len - link.len;
			Vector2 v = p2.velocity - p1.velocity;
			float vRel = v.Dot(delta);
			Vector2 force = (links[i].stiffness * stiffness * dLen + damping * vRel) * delta;
			p1.acceleration.Add(force);
			p2.acceleration.Subtract(force);
		}
		// build matrix and rhs
		std::vector<float> b(GetNumParticles() * 2);
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			const int ii = i * 2;
			Vector2 v = particles[i].prevPos - particles[i].velocity + 0.5f * timeStep * particles[i].invMass * particles[i].acceleration;
			particles[i].acceleration.SetZero();
			b[ii] = v.GetX();
			b[ii + 1] = v.GetY();
		}
		MatrixSqr<float> mat(GetNumParticles() * 2);
		mat.SetIdentity();
		const float c = 0.25f * stiffness * timeStep * timeStep;
		for (size_t k = 0; k < links.size(); k++)
		{
			const int i = links[k].i1;
			const int j = links[k].i2;
			Vector2 delta = particles[j].pos - particles[i].pos;
			const float len = delta.Length();
			delta.Normalize();
			Matrix2 T = Matrix2::TensorProduct(delta, delta);
			const float f = links[k].len / len;
			float ck = links[k].stiffness * c;
			//Matrix2 K = ck * ((f - 1) * Matrix2::Identity() - f * T);
			Matrix2 K = -ck * T;
			//Matrix2 D = -0.5f * damping * timeStep * T;
			//Matrix2 K = K0 + D;
			const int ii = i * 2;
			const int jj = j * 2;

			mat(ii, ii) -= particles[i].invMass * K.a11;
			mat(ii, ii + 1) -= particles[i].invMass * K.a12;
			mat(ii + 1, ii) -= particles[i].invMass * K.a21;
			mat(ii + 1, ii + 1) -= particles[i].invMass * K.a22;

			mat(ii, jj) += particles[i].invMass * K.a11;
			mat(ii, jj + 1) += particles[i].invMass * K.a12;
			mat(ii + 1, jj) += particles[i].invMass * K.a21;
			mat(ii + 1, jj + 1) += particles[i].invMass * K.a22;

			mat(jj, jj) -= particles[j].invMass * K.a11;
			mat(jj, jj + 1) -= particles[j].invMass * K.a12;
			mat(jj + 1, jj) -= particles[j].invMass * K.a21;
			mat(jj + 1, jj + 1) -= particles[j].invMass * K.a22;

			mat(jj, ii) += particles[j].invMass * K.a11;
			mat(jj, ii + 1) += particles[j].invMass * K.a12;
			mat(jj + 1, ii) += particles[j].invMass * K.a21;
			mat(jj + 1, ii + 1) += particles[j].invMass * K.a22;
		}

		std::vector<float> v(GetNumParticles() * 2);
		SolveConjugateGradient(mat, b, v, numIterations);

		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (particles[i].invMass == 0)
				continue;
			const int ii = i * 2;
			Vector2 dv(v[ii], v[ii + 1]);
			particles[i].pos += 0.5f * timeStep * dv;
			particles[i].velocity += dv;
		}
	}
}

void ParticleSystemPenalty::IntegrateMidpointCG()
{
	PROFILE_SCOPE("IntegrateMidpointCG");
	
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		particles[i].acceleration.SetZero(); // it's actually a force
	}

	for (size_t i = 0; i < links.size(); i++)
	{
		Constraint& link = links[i];
		ParticleAcceleration& p1 = particles[link.i1];
		ParticleAcceleration& p2 = particles[link.i2];			
		Vector2 delta = p2.pos - p1.pos;
		float len = delta.Length();
		delta.Normalize();
		float dLen = len - link.len;
		//Vector2 v = p2.velocity - p1.velocity;
		//float vRel = v.Dot(delta);
		Vector2 force = (links[i].stiffness * stiffness * dLen /*+ damping * vRel*/) * delta;
		p1.acceleration.Add(force);
		p2.acceleration.Subtract(force);
	}

	// explicit part of the integration step
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass != 0)
		{
			particles[i].pos += timeStep * (particles[i].velocity + 0.5f * timeStep * lengthScale * gravity +
				0.25f * timeStep * particles[i].acceleration);
			particles[i].velocity += timeStep * lengthScale * gravity + 0.5f * timeStep * particles[i].acceleration;
		}
		particles[i].prevPos = particles[i].velocity; // store this velocity
		particles[i].acceleration.SetZero();
	}

	const float c = 0.25f * stiffness * timeStep * timeStep;
	// conjugate gradient
	//std::vector<Vector2> dv(GetNumParticles()); // the unknown
	std::vector<Vector2> r(GetNumParticles());
	std::vector<Vector2> d(GetNumParticles()); // conjugate gradient directions
	std::vector<float> y(links.size()); // used for computing J * d
	std::vector<Vector2> z(GetNumParticles()); // used for computing J^T * y
	float delta = 0;
	for (int iter = 0; iter < numIterations; iter++)
	{
		// recompute forces and jacobian
		for (size_t i = 0; i < links.size(); i++)
		{
			Constraint& link = links[i];
			ParticleAcceleration& p1 = particles[link.i1];
			ParticleAcceleration& p2 = particles[link.i2];			
			Vector2 delta = p2.pos - p1.pos;
			float len = delta.Length();
			delta.Normalize();
			links[i].normal = delta;
			float dLen = len - link.len;
			//Vector2 v = p2.velocity - p1.velocity;
			//float vRel = v.Dot(delta);
			Vector2 force = (links[i].stiffness * stiffness * dLen /*+ damping * vRel*/) * delta;
			p1.acceleration.Add(force);
			p2.acceleration.Subtract(force);
		}

		// build residual
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			r[i] = particles[i].prevPos - particles[i].velocity + 0.5f * timeStep * particles[i].invMass * particles[i].acceleration;
			particles[i].acceleration.SetZero();
		}

		// compute beta, the new direction and the new delta
		if (iter == 0)
		{
			d = r;
			delta = r * r;
		}
		else
		{
			float delta1 = r * r;
			float beta = delta1 / delta;
			delta = delta1;
			d = r + beta * d;
		}
		if (delta == 0.f)
			break;

		// compute y = h^2 * k * J * d
		for (size_t i = 0; i < links.size(); i++)
		{
			y[i] = links[i].stiffness * links[i].normal.Dot(d[links[i].i2] - d[links[i].i1]);
		}
		// compute z = W * J^T * y
		memset(&z[0], 0, sizeof(Vector2) * GetNumParticles());
		for (size_t i = 0; i < links.size(); i++)
		{
			Vector2 add = y[i] * links[i].normal;
			z[links[i].i1] += particles[links[i].i1].invMass * add;
			z[links[i].i2] -= particles[links[i].i2].invMass * add;
		}
		// compute alpha
		z = d - c * z; // z = A * d
		float den = d * z;
		float alpha = delta / den;

		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (particles[i].invMass == 0)
				continue;
			Vector2 dv = alpha * d[i];
			particles[i].pos += 0.5f * timeStep * dv;
			particles[i].velocity += dv;
		}
	}
}

void ParticleSystemPenalty::AddPenalties()
{
	// TODO: make global?
	static ParticleAcceleration ground; 
	ground.invMass = 0; 
	ground.velocity.SetZero();

	// add spring forces
	//const float damping = 5 * stiffness * timeStep;
	//const float damping = 2 * sqrt(stiffness); // critical damping (m = 1)
	//const float damping = 10;
	for (size_t i = 0; i < constraints.size(); i++)
	{
		const Constraint& link = constraints[i];
		ParticleAcceleration* p1 = &particles[link.i1];
		ParticleAcceleration* p2 = NULL;
		if (link.type == Constraint::CONTACT)
		{
			p2 = &ground;
			p2->pos = link.point;
		}
		else
		{
			p2 = &particles[link.i2];
		}
		Vector2 delta = p1->pos;
		delta.Subtract(p2->pos);
		float len = link.type == Constraint::CONTACT ? delta.Dot(link.normal) : delta.Length();
		if (constraints[i].type != Constraint::LINK && len > link.len)
			continue;
		//delta.Normalize();
		if (constraints[i].type == Constraint::CONTACT)
			delta = link.normal;
		else
			delta.Scale(1 / len);
		float dLen = len - link.len;
		Vector2 force = delta;
		Vector2 v = p1->velocity - p2->velocity;
		float vrel = v.Dot(delta);
		// Hertz-Bell
		//float dLenSqrt = sqrt(fabs(dLen));
		//dLen *= dLenSqrt;
		//vrel *= dLenSqrt;
		force.Scale(link.stiffness * stiffness * dLen + damping * vrel);
		// TODO: multiply by invMass
		p1->acceleration.Subtract(force);
		p2->acceleration.Add(force);
	}
}
