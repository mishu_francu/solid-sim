inline float Sqr(float x) { return x * x; }
inline float Cube(float x) { return x * x * x; }
inline float Quart(float x) { return x * x * x * x; }

#if (USE_SSE == 1) && defined (USE_SCALAR)
inline Scalar Sqr(const Scalar& x) { return x * x; }
inline Scalar Cube(const Scalar& x) { return x * x * x; }
inline Scalar Quart(const Scalar& x) { return x * x * x * x; }
#endif

inline void PhysicsSystem::Step()
{
	for (int microStep = 0; microStep < numMicroSteps; ++microStep) 
	{
#ifdef RECORD_STATE
		// record state
		// TODO: if check, move into a function
		Frame frame;
		frame.particles.resize(particles.size());
		copy(particles.begin(), particles.end(), frame.particles.begin());
		frame.walls.resize(walls.size());
		copy(walls.begin(), walls.end(), frame.walls.begin());
		frames.push_back(frame);
#endif
		
		MicroStep();
	}
	nFrames++;
	if (mMaxFrames >= 0 && nFrames > mMaxFrames)
		Engine::getInstance()->Quit();
}

template <typename Particle, typename Broadphase>
inline void ParticleSystem<Particle, Broadphase>::AddCollisionPair(int a, int b)
{
	constraints.push_back(Constraint(Constraint::COLL_PAIR, (ParticleIdx)a, (ParticleIdx)b, 2 * radius)); // TODO: asserts
}

template <typename Particle, typename Broadphase>
inline void ParticleSystem<Particle, Broadphase>::AddContact(size_t idx, const Vector2& p, const Vector2& n)
{
	constraints.push_back(Constraint((ParticleIdx)idx, p, n, radius));
}

template <typename Particle, typename Broadphase>
inline Constraint* ParticleSystem<Particle, Broadphase>::AddLink(ParticleIdx parIdx1, ParticleIdx parIdx2, float stiffness)
{
	if (stiffness <= 0)
		return NULL;
	float len = (particles[parIdx1].pos - particles[parIdx2].pos).Length();
	Constraint link(Constraint::LINK, parIdx1, parIdx2, len);
	link.stiffness = stiffness;
	links.push_back(link);

	mesh.AddLine(parIdx1, parIdx2);

	return &links[links.size() - 1];
}

template <typename Real>
inline void PrintMatrix(const MatrixSqr<Real>& mat)
{
	size_t nl = mat.GetSize();
	for (size_t i = 0; i < nl; i++)
	{
		for (size_t j = 0; j < nl; j++)
		{
			Printf("%.2f ", mat.Get(i, j));
			//if (mat.Get(i, j) != 0.f)
			//	Printf("* ");
			//else
			//	Printf("  ");
		}
		Printf("\n");
	}
}

