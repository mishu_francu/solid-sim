#include "ImpJac.cuh"
#include <helper_math.h>

inline int NextMultiple(int x, int div)
{
	int r = x % div;
	if (r == 0)
		return x;
	return x - r + div;
}

#define WAVE_SIZE 256

// kernels
__global__ void conjResEdges(const Edge* edges, float4* pos, float* invMass, float4* disp, float alpha, float beta)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	int i1 = edges[i].i1;
	int i2 = edges[i].i2;
	float len0 = edges[i].len;

	float4 delta = pos[i1] - pos[i2];
	float len = length(delta);
	if (len == 0)
		return;	
	float lambda = (len - len0) * alpha;
	disp[i] = edges[i].stiff * ((lambda / len) * delta + beta * disp[i]);
}

__global__ void minResParticles(float4* pos, const float* invMass, const Incidence* incidence, const float4* disp)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	float4 acc = make_float4(0);
	float im = invMass[i];
	for(int j = 0; j < 16; j++)
	{
		int info = incidence[i].info[j];
		if (info == 0)
			break;
		int sgn = 1;
		int idx = 0;// = GetIndexAndSign(info, &sgn);
		sgn = +1 | (info >> (sizeof(int) * 8 - 1));  // if v < 0 then -1, else +1
		idx = sgn * info - 1;

		acc += sgn * im * disp[idx];
	}
	pos[i] += acc;
}

// host code
template <class VectorType, class ParticleType>
void ProjectorCUDA<VectorType, ParticleType>::PrepareBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links)
{
	size_t n = particles.size();
	nParticles = NextMultiple(n, WAVE_SIZE);
	nLinks = NextMultiple(links.size(), WAVE_SIZE);

	cudaMalloc(&positions, sizeof(Vector4) * nParticles); 
	posPtr = new Vector4[nParticles]; // TODO: free

	size_t maxSize = nLinks;
	cudaMalloc(&edges, sizeof(Edge) * maxSize);
	cudaMalloc(&disp, sizeof(Vector4) * maxSize);

	std::vector<float> im(nParticles);
	for (size_t i = 0; i < n; i++)
	{
		im[i] = particles[i].invMass;
	}
	cudaMalloc(&invMass, sizeof(float) * nParticles);
	cudaMemcpy(invMass, &im[0], sizeof(float) * nParticles, cudaMemcpyHostToDevice);

	cudaMalloc(&incEdges, sizeof(Incidence) * nParticles);
}

template <class VectorType, class ParticleType>
void ProjectorCUDA<VectorType, ParticleType>::CopyBuffers(const std::vector<ParticleType>& particles, const std::vector<ConstraintTemplate<VectorType> >& links)
{
	//PROFILE_SCOPE("CopyBuffers");
	size_t n = particles.size();

	// copy edges
	//Edge* edgePtr = (Edge*)compute->EnqueueMap(edges, true, CL_MAP_WRITE);
	//memset(edgePtr, 0, nLinks * sizeof(Edge));
	std::vector<Edge> edgePtr(nLinks);
	//Incidence* incPtr = (Incidence*)compute->EnqueueMap(incEdges, true, CL_MAP_WRITE);
	std::vector<Incidence> incPtr(nParticles);
	std::vector<int> count(n);
	//memset(incPtr, 0, sizeof(Incidence) * nParticles);
	//ASSERT(nLinks < 10 * n);
	for (size_t i = 0; i < links.size(); i++)
	{
		Edge edge;

		edge.len = links[i].len;
		edge.i1 = links[i].i1;
		edge.type = links[i].type;
		edge.stiff = links[i].stiffness;

		int info = i + 1;

		int i1 = links[i].i1;
		int& n1 = count[i1];
		ASSERT(n1 < MAX_INCIDENCE);
		incPtr[i1].info[n1++] = -info;

		int i2 = links[i].i2;
		if (links[i].type != Constraint::CONTACT)
		{
			int& n2 = count[i2];
			ASSERT(n2 < MAX_INCIDENCE);
			incPtr[i2].info[n2++] = info;
			edge.i2 = i2;
		}

		edgePtr[i] = edge;
	}
	//ASSERT(nContacts <= 2 * (int)n);
	//compute->EnqueueUnmap(edges, edgePtr);
	//compute->EnqueueUnmap(incEdges, incPtr);

	// copy positions
	// TODO: strided read/write
	//posPtr = (Vector4*)compute->EnqueueMap(positions, true, CL_MAP_WRITE);
	for (size_t i = 0; i < n; i++)
	{
		posPtr[i] = particles[i].pos;
	}
	cudaMemcpy(positions, posPtr, particles.size() * sizeof(Vector4), cudaMemcpyHostToDevice);
	//compute->EnqueueUnmap(positions, posPtr);
	//posPtr = NULL;
}

template <class VectorType, class ParticleType>
void ProjectorCUDA<VectorType, ParticleType>::ProjectPositions()
{
	if (nParticles == 0 || nLinks == 0)
		return;

	//size_t localSize = WAVE_SIZE;
	//size_t localSize1 = WAVE_SIZE1;
	const float alpha = 0.2f;
	for (int k = 0; k < numIterations; ++k)
	{
		float beta = (float)k / (float)(numIterations - 1);
		if (beta > 0)
			beta = pow(beta, 0.6f);
		conjResEdges<<<1, WAVE_SIZE>>>(edges, positions, invMass, disp, alpha, beta);
		//minResParticles<<<nParticles / WAVE_SIZE, WAVE_SIZE>>>(positions, invMass, incEdges, disp);
	}
}

template class ProjectorCUDA<Vector3, ParticleVelocityT<Vector3> >;