#include "ParticleSystemSpook.h"

void ParticleSystemSpook::MicroStep()
{
	// integrate velocities
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass != 0)
			particles[i].velocity += timeStep * lengthScale * gravity;
	}

	// precompute length and normal
	for (size_t i = 0; i < links.size(); i++)
	{
		Constraint& pair = links[i];
		ParticleVelocity& p1 = particles[pair.i1];
		ParticleVelocity& p2 = particles[pair.i2];
		Vector2 n = p1.pos - p2.pos;
		float len = n.Length();
		float depth = pair.len - len;
		pair.depth = 4 * depth / (9 * timeStep);
		n.Normalize();
		pair.normal = n;

		Vector2 v12;
		v12 = p2.velocity - p1.velocity;
		float vnrel = pair.normal.Dot(v12);
		pair.depth += vnrel / 9.f;
	}

	// project velocities
	const float eps = 4 * 0.000001f / (9 * timeStep * timeStep);
	for (int k = 0; k < numIterations; k++)
	{
		for (size_t i = 0; i < links.size(); i++)
		{
			Constraint& pair = links[i];
			ParticleVelocity* p1 = &particles[pair.i1];
			ParticleVelocity* p2 = &particles[pair.i2];
			Vector2 v12;
			v12 = p2->velocity - p1->velocity;

			float vnrel = pair.normal.Dot(v12);
			float dLambda = (vnrel + pair.depth) / (p1->invMass + p2->invMass + eps);

			Vector2 disp = dLambda * pair.normal;
			p1->velocity += p1->invMass * disp;
			p2->velocity -= p2->invMass * disp;
		}
	}

	// integrate positions
	for (size_t i = 0; i < GetNumParticles(); i++)
	{
		if (particles[i].invMass != 0)
			particles[i].pos += timeStep * particles[i].velocity;
	}
}