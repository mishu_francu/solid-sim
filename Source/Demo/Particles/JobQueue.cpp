#include "JobQueue.h"
#include <Engine/Base.h>

bool volatile alive = false;
const Job* JobQueue::currJob = NULL;

#ifdef ANDROID_NDK

#include <sys/syscall.h>
#include <sys/types.h>

//#define USE_ATOMICS_SPIN_LOCK
#ifndef USE_ATOMICS_SPIN_LOCK
pthread_mutex_t waitMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t waitCondVar = PTHREAD_COND_INITIALIZER;
pthread_mutex_t completeMutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t completeCondVar = PTHREAD_COND_INITIALIZER;
static volatile int bComplete = 1;
#else
static volatile int bWait = 1;
#endif

void* JobQueueThreadProc(void* ptr)
{
	//unsigned long mask = 2;
	//pid_t pid = gettid();
	//syscall(__NR_sched_setaffinity, 0, sizeof(mask), &mask);
	while (alive)
	{
#ifndef USE_ATOMICS_SPIN_LOCK
		pthread_mutex_lock(&waitMutex);
		pthread_cond_wait(&waitCondVar, &waitMutex);
		pthread_mutex_unlock(&waitMutex);
#else
		while(__sync_lock_test_and_set(&bWait, 1));
#endif
		if (!alive)
			break;
		if (JobQueue::currJob) {
			JobQueue::currJob->DoWork(1);
			JobQueue::currJob = NULL;
		}
#ifndef USE_ATOMICS_SPIN_LOCK
		__sync_lock_release(&bComplete);
#else
		//pthread_mutex_lock(&completeMutex);
		//pthread_cond_signal(&completeCondVar);
		//pthread_mutex_unlock(&completeMutex);
#endif
	}
	return 0;
}

JobQueue::JobQueue()
{
	//pthread_attr_t attr;
	//int ret = pthread_attr_init(&attr);
	//unsigned long mask = 2; // processor 1
	//ret = pthread_attr_setaffinity_np(&attr, sizeof(mask), &mask);
    //assert(!ret);
	int ret = pthread_create(&m_thread, NULL, JobQueueThreadProc, NULL);
	//assert(!ret);
}

JobQueue::~JobQueue()
{
}

void JobQueue::Start()
{
	alive = true;
}

void JobQueue::StopAndWait()
{
	currJob = NULL;
	alive = false;
#ifndef USE_ATOMICS_SPIN_LOCK
	pthread_mutex_lock(&waitMutex);
	pthread_cond_signal(&waitCondVar);
	pthread_mutex_unlock(&waitMutex);
#else
	__sync_lock_release(&bWait);
#endif
	pthread_join(m_thread, NULL);
}

void JobQueue::StartJob(const Job& job)
{
	currJob = &job;
#ifndef USE_ATOMICS_SPIN_LOCK
	pthread_mutex_lock(&waitMutex);
	pthread_cond_signal(&waitCondVar);
	pthread_mutex_unlock(&waitMutex);
#else
	__sync_lock_release(&bWait);
#endif
}

void JobQueue::WaitForJobs()
{
#ifdef USE_ATOMICS_SPIN_WAIT
	//pthread_mutex_lock(&completeMutex);
	//pthread_cond_wait(&completeCondVar, &completeMutex);
	//pthread_mutex_unlock(&completeMutex);
#else
	while(__sync_lock_test_and_set(&bComplete, 1));
#endif
}

#else // !ANDROID_NDK
HANDLE JobQueue::hEvent, JobQueue::waitEvent;

DWORD WINAPI JobQueueThreadProc(__in  LPVOID lpParameter)
{
	while (alive)
	{
		WaitForSingleObject(JobQueue::hEvent, INFINITE);
		if (!alive)
			break;
		if (JobQueue::currJob) {
			JobQueue::currJob->DoWork(1);
			JobQueue::currJob = NULL;
		}
		SetEvent(JobQueue::waitEvent);
	}
	return 0;
}

JobQueue::JobQueue()
{
	m_thread = CreateThread(NULL, 0, &JobQueueThreadProc, NULL, CREATE_SUSPENDED, NULL);
	if (!m_thread)
		Printf("The thread could not be created\n");
	hEvent = CreateEvent(NULL, FALSE, FALSE, L"JobQueueEvent"); 
	waitEvent = CreateEvent(NULL, FALSE, FALSE, L"JobQueueWaitEvent"); 
}

JobQueue::~JobQueue()
{
	CloseHandle(hEvent);
	CloseHandle(waitEvent);
	CloseHandle(m_thread);
}

void JobQueue::Start()
{
	alive = true;
	ResumeThread(m_thread);
}

void JobQueue::StopAndWait()
{
	//WaitForSingleObject(waitEvent, 1000);
	alive = false;
	SetEvent(hEvent);
	WaitForSingleObject(m_thread, INFINITE);
}

#endif
