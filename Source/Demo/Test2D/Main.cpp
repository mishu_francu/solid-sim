#include "Engine/Engine.h"
#include "Graphics2D/Graphics2D.h"

class Test2DDemo : public Engine
{
public:
	void OnCreate() override
	{
		img.LoadTexture("../Res/checker.bmp");
	}

	void OnDraw() override
	{
		graphics->SetColor(0xffffffff);
		graphics->DrawCircle(10, 10, 5);

		graphics->DrawImage(&img, 20, 20);

		graphics->DrawString(100, 10, "Hello world!");

		graphics->SetColor(0xff0000ff);
		graphics->FillRect(400, 50, 40, 70);
	}

private:
	Texture img;
};

// application entry point
int APIENTRY wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	RUN_ENGINE(Test2DDemo, hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}
