#include "ClothModel.h"

namespace Physics
{
	class ClothModelImplicit : public ClothModel
	{
	public:
		ClothModelImplicit(ClothPatch* owner) : ClothModel(owner) { }
		ClothModelImplicit(const ClothModel& model) : ClothModel(model) { }
		void Step(float h) { StepImplicitNCG(h); }
	private:
		void AddElasticForces(bool);
		void StepImplicit(float h);
		void StepImplicitCG(float h);
		void StepImplicitNCG(float h);
		void StepImplicitGS(float h);
		void StepImplicitUV(float h);
	};
}