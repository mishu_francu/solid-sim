#ifndef CLOTH_H
#define CLOTH_H

#include "SelfCollisionDetector.h"
#include "ClothCollisionHandler.h"

#include <memory>

using Math::Vector3;

namespace Geometry
{
	struct Mesh;
}

namespace Physics
{
	class ClothModel;

	enum MethodType
	{
		METHOD_PBD,
		METHOD_SPRING,
		METHOD_SOFT,
		METHOD_NEWMARK,
		METHOD_OPEN_CLOTH,
		METHOD_SEQ_IMP,
		METHOD_SEQ_POS,
		METHOD_IVP,
	};

	class ClothPatch
	{
	public:
		ClothPatch();
		ClothPatch& operator =(const ClothPatch& other);
		~ClothPatch();
		// init quad sim-mesh based on input args
		void Init(int divisionsX, int divisionsY, float inc, const Vector3& offset, bool horizontal, bool attached);
		// init tri-sim-mesh
		void Init(const Geometry::Mesh& mesh, const Vector3& offset, bool attached);
		void Step(float dt);
		void UpdateMesh();
		void ComputeMass();

		const Geometry::Mesh& GetMesh() const { return *mMesh; }
		const ClothModel& GetModel() const { return *mModel; }
		ClothModel& GetModel() { return *mModel; }
		bool GetBicubic() const { return mBicubic; }
		void SetBicubic(bool val) { mBicubic = val; }
		float GetMaxMass() const { return mMaxMass; }
		void SetPosition(const Vector3& v) { mPosition = v; }
		void SetMethod(MethodType val);
		MethodType GetMethod() const { return mMethod; }
		void SetNumSteps(int val) { mNumSteps = val; }

		void DetectSelfCollisions() { mSelfCollDetector.Detect(); }
		ClothCollisionHandler& GetCollisionHandler() { return mCollisionHandler; }

	private:
		bool isQuadMesh;
		bool mBicubic;
		ClothModel* mModel;
		std::shared_ptr<Geometry::Mesh> mMesh;
		float mMaxMass;
		Vector3 mPosition;
		MethodType mMethod;
		int mNumSteps;

		SelfCollisionsDetector mSelfCollDetector;

		ClothCollisionHandler mCollisionHandler;
	};
}

#endif // CLOTH_H