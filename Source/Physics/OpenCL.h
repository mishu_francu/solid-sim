#ifndef OPENCL_H
#define OPENCL_H

//#define EMULATE_CL
//#define PROFILE_KERNELS

#include <CL/cl.h>
#include <CL/cl_ext.h>

#include <Engine/Utils.h>
#ifdef PROFILE_KERNELS
	#include <Engine/Profiler.h>
#endif
#include <Math/Vector2.h>
#include <memory>
#include <vector>
#include <string>

#ifdef _DEBUG
#define CL_CALL(func) { cl_int ret = func; if (ret != CL_SUCCESS) { Printf("CL error %d\n", ret); ASSERT(false); } }
#else
#define CL_CALL(func) func
#endif

const int WAVE_SIZE = 16;
const int WAVE_SIZE1 = WAVE_SIZE;

class OpenCL;

#ifdef EMULATE_CL
class CLBuffer
{
private:
	void* buffer;
	size_t size;
public:
	CLBuffer() : buffer(NULL), size(0) { }
	~CLBuffer()
	{
		// TODO: careful with ownership
		if (buffer)
			free(buffer);
		buffer = NULL;
	}
	void* Get() { return buffer; }
	size_t GetSize() { return size; }
	bool Create(OpenCL& openCL, cl_mem_flags flags, size_t s, void* data = NULL);
};

union CLVariant
{
	CLBuffer* buf;
	float vec[2];
	float f;
	size_t u;
};

class CLKernelFunctor
{
protected:
	enum { MAX_ARGS = 10 };
	CLVariant args[MAX_ARGS];
	size_t globalId;
public:
	CLKernelFunctor()
	{
		memset(args, 0, MAX_ARGS * sizeof(CLVariant));
	}

	void SetArgument(int idx, CLBuffer* buffer)
	{
		ASSERT(idx < MAX_ARGS);
		args[idx].buf = buffer;
	}

	void SetArgument(int idx, Vector2* vec)
	{
		ASSERT(idx < MAX_ARGS);
		args[idx].vec[0] = vec->GetX();
		args[idx].vec[1] = vec->GetY();
	}

	void SetArgument(int idx, float* val)
	{
		ASSERT(idx < MAX_ARGS);
		args[idx].f = *val;
	}

	void SetArgument(int idx, size_t* val)
	{
		ASSERT(idx < MAX_ARGS);
		args[idx].u = *val;
	}

	virtual void Run(size_t* size) = 0;
};

class CLKernel
{
private:
	CLKernelFunctor* kernel;
public:
	CLKernel() : kernel(NULL) { }
	~CLKernel()
	{
		kernel = NULL;
	}

	template<typename T>
	void SetArgument(int idx, T* val)
	{
		ASSERT(kernel != NULL);
		kernel->SetArgument(idx, val);
	}

	void SetArgument(int idx, size_t size) // for local
	{
		ASSERT(false); // TODO: implement
	}

	bool Create(CLKernelFunctor* kf);
	void Run(size_t* size)
	{
		kernel->Run(size);
	}
};
#else
// TODO: make internal
class CLBuffer
{
private:
	cl_mem buffer;
	size_t size;
public:
	CLBuffer() : buffer(NULL), size(0) { }
	~CLBuffer()
	{
		// TODO: careful with ownership
		Release();
	}
	cl_mem& Get() { return buffer; }
	size_t GetSize() { return size; }
	bool Create(cl_mem_flags flags, size_t s, void* data = NULL);
	void Release()
	{
		if (buffer)
			CL_CALL(clReleaseMemObject(buffer));
		buffer = NULL;
	}
};

// TODO: CLProgram
class CLKernel
{
private:
	cl_kernel kernel;
	std::string name;
public:
	CLKernel() : kernel(NULL) { }
	~CLKernel()
	{
		if (kernel)
			CL_CALL(clReleaseKernel(kernel));
		kernel = NULL;
	}

	void SetArgument(int idx, CLBuffer* buffer)
	{
		ASSERT(kernel != NULL);
		CL_CALL(clSetKernelArg(kernel, idx, sizeof(cl_mem), &buffer->Get()));
	}

	void SetArgument(int idx, size_t size) // for local
	{
		ASSERT(kernel != NULL);
		CL_CALL(clSetKernelArg(kernel, idx, size, NULL));
	}

	template<typename T>
	void SetArgument(int idx, T* val)
	{
		ASSERT(kernel != NULL);
		CL_CALL(clSetKernelArg(kernel, idx, sizeof(T), val));
	}

	cl_kernel& Get() { return kernel; }
	bool Create(cl_program program, const char* name);

	const char* GetName() const { return name.c_str(); }
};
#endif

class OpenCL
{
private:
	cl_context mContext;
	cl_uint mNumDevices;
	cl_device_id *mDevices;
	cl_device_id mDevice;
	cl_command_queue mCommandQueue;
	std::vector<std::string> platfNames;
	std::vector<std::string> mDeviceNames;
	cl_uint mPlatfIdx, mDeviceIdx;
	bool mInitialized;
	cl_device_type mDeviceType;

private:
	OpenCL() 
		: mDevices(NULL)
		, mCommandQueue(NULL)
		, mContext(NULL)
		, mPlatfIdx(0)
		, mDeviceIdx(0) 
		, mInitialized(false)
		, mDeviceType(CL_DEVICE_TYPE_ALL)
	{ }

	bool FindDevice(cl_platform_id platform, cl_device_type deviceType);

public:
	~OpenCL() 
	{
		DeInit();
		if (mDevices)
		{
			delete[] mDevices;
			mDevices = NULL;
		}
	}

	bool Init();
	void DeInit();
	void ReInit()
	{
		DeInit();
		Init();
	}
	cl_program Build(const char *filename, const char *params = NULL);

	static OpenCL* GetInstance();

	cl_context GetContext() { return mContext; }
	cl_command_queue GetCommandQueue() { return mCommandQueue; }
	const std::vector<std::string>& GetPlatformNames() const { return platfNames; }
	const std::vector<std::string>& GetDeviceNames() const { return mDeviceNames; }
	size_t GetNumPlatforms() const { return platfNames.size(); }
	void SetPlatform(int val) {	mPlatfIdx = val; }
	void SetDevice(int val) { mDeviceIdx = val; }
	bool GetKernelWorkGroupInfo(cl_kernel &kernel);
	void SetDeviceType(cl_device_type type) { mDeviceType = type; }

#ifdef EMULATE_CL
	void EnqueueKernel(CLKernel& kernel, size_t* workSize)
	{
		kernel.Run(workSize);
	}

	void* EnqueueMap(CLBuffer& buffer, bool block, cl_map_flags flags)
	{
		return buffer.Get();
	}

	void EnqueueUnmap(CLBuffer& buffer, void* ptr)
	{
	}
#else
	void EnqueueKernel(CLKernel& kernel, size_t* workSize, size_t* localSize = NULL)
	{
#ifdef PROFILE_KERNELS
		PROFILE_SCOPE(kernel.GetName());
#endif
		CL_CALL(clEnqueueNDRangeKernel(mCommandQueue, kernel.Get(), 1, NULL, workSize, localSize, 0, NULL, NULL));
#ifdef PROFILE_KERNELS
		clFinish(commandQueue);
#endif
	}

	void* EnqueueMap(CLBuffer& buffer, bool block, cl_map_flags flags)
	{
		cl_int err;
		void* ret = clEnqueueMapBuffer(mCommandQueue, buffer.Get(), block, flags, 0, buffer.GetSize(), 0, NULL, NULL, &err);
		ASSERT(err == CL_SUCCESS);
		return ret;
	}

	void EnqueueUnmap(CLBuffer& buffer, void* ptr)
	{
		CL_CALL(clEnqueueUnmapMemObject(mCommandQueue, buffer.Get(), ptr, 0, NULL, NULL));
	}
#endif
};

#ifdef EMULATE_CL
inline bool CLBuffer::Create(OpenCL& openCL, cl_mem_flags flags, size_t s, void* data)
{
	size = s;
	buffer = malloc(size);
	if (flags & CL_MEM_COPY_HOST_PTR)
	{
		memcpy(buffer, data, size);
	}
	return buffer != NULL;
}

inline bool CLKernel::Create(CLKernelFunctor* kf)
{
	kernel = kf;
	return kernel != NULL;
}
#else
inline bool CLBuffer::Create(cl_mem_flags flags, size_t s, void* data)
{
	Release();
	size = s;
	OpenCL* ocl = OpenCL::GetInstance();
	if (!ocl)
		return false;
	cl_context context = ocl->GetContext();
	ASSERT(context != NULL);
	buffer = clCreateBuffer(context, flags, size, data, NULL);
	ASSERT(buffer != NULL);
	return buffer != NULL;
}

inline bool CLKernel::Create(cl_program program, const char* str)
{
	name = str;
	kernel = clCreateKernel(program, str, NULL);
	return kernel != NULL;
}
#endif
#endif // OPENCL_H