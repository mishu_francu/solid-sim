#include "OpenCL.h"
#include "ClothTypes.h"
#include "Geometry/Collision3D.h"

namespace Geometry
{
	struct Mesh;
}

namespace Physics
{
	class NarrowPhaseCL
	{
	private:
		CLKernel mNarrowPhaseKernel;
		cl_program mProgram;
		CLBuffer mPairsBuffer, mPrevBuffer, mIndexBuffer, mVertexBuffer;
		size_t nContacts, maxContacts;
	public:
		void Init();
		void PrepareBuffers(const std::vector<Particle>& particles, const Geometry::Mesh& mesh);
		void CopyBuffers(const std::vector<PrimitivePair>& pairs, const std::vector<Particle>& particles);
		void NarrowPhase(float radTol);
		void ReadBuffers(size_t nPairs, std::vector<Contact>& contacts);
	};
}