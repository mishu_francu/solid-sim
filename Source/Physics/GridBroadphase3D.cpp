#include <Engine/Engine.h>
#include <Engine/Profiler.h>
#include "GridBroadphase3D.h"
#include <algorithm>

using namespace Geometry;

static int profile;

void GridBroadphase3D::Init(const Vector3& center, float w, float h, float d, float size) 
{
	cellSize = size;
	width = (int)(w / size + 1);
	height = (int)(h / size + 1);
	depth = (int)(d / size + 1);
	if (cells)
		delete[] cells;
	cells = new int[width * height * depth];
	memset(cells, 0xff, width * height * depth * sizeof(int));
	offset = center - 0.5f * Vector3(w, h, d);
}

inline void GridBroadphase3D::TestCell(const std::vector<Aabb3>& handles, std::vector<Constraint3D>& pairs, 
									   int count, int start, int x, int y, int z)
{
	int id = x + y * width + z * width * height;
	if (cells[id] <= 0)
		return;
	int countAdj = (cells[id] >> 24) & 0xff;
	int startAdj = cells[id] & 0xffffff;
	for (int i = 0; i < count; i++)
		for (int j = 0; j < countAdj; j++)
			if (AabbOverlap3D(handles[particles[start + i].particleId], handles[particles[startAdj + j].particleId]))
				pairs.push_back(Constraint3D(Constraint3D::COLL_PAIR, particles[start + i].particleId, particles[startAdj + j].particleId, cellSize));
}

void GridBroadphase3D::Sample(const std::vector<Aabb3>& handles)
{
	PROFILE_SCOPE("GridSample");
	// sample particles into grid cells
	if (particles.size() != handles.size())
		particles.resize(handles.size());
	// TODO: bounding spheres
	size_t num = 0;
	for (size_t i = 0; i < handles.size(); i++)
	{
		// use center
		Vector3 center = handles[i].min;
		center.Add(handles[i].max);
		center.Scale(0.5f);
		center.Subtract(offset);
		int x = (int)(center.X() / cellSize);
		int y = (int)(center.Y() / cellSize);
		int z = (int)(center.Z() / cellSize);
		if (x < 0 || x >= width || y < 0 || y >= height || z < 0 || z >= depth)
			continue;
		int cellId = x + y * width + z * width * height;
		particles[num++] = ParticleInCell((int)i, cellId);
	}
	// sort the particles by cell id
	std::sort(particles.begin(), particles.begin() + num);	
	// set the cell information: cell[i] = <start[i], count[i]>
	// TODO: sparse array
	memset(cells, 0xff, width * height * depth * sizeof(int));
	int lastId = -1;
	int count = 0;
	for (size_t i = 0; i < num; i++)
	{
		if (lastId != particles[i].cellId)
		{			
			if (lastId >= 0)
			{
				ASSERT(count < 256);
				cells[lastId] |= (count & 0xff) << 24;
			}
			lastId = particles[i].cellId;
			cells[lastId] = i & 0xffffff;
			count = 0;	
		}
		count++;
	}
	ASSERT(count < 256);
	cells[lastId] |= (count & 0xff) << 24;
}

void GridBroadphase3D::Update(const std::vector<Aabb3>& handles, std::vector<Constraint3D>& pairs)
{
	PROFILE_SCOPE("GridBroadphase");
	// go through all the cells and query the neighbours
	//pairs.clear();
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			for (int z = 0; z < depth; z++)
			{
				int id = x + y * width + z * width * height;
				if (cells[id] < 0) continue;
				// inside pairs
				int count = (cells[id] >> 24) & 0xff;
				int start = cells[id] & 0xffffff;
				for (int i = 0; i < count - 1; i++)
					for (int j = i + 1; j < count; j++)
						if (AabbOverlap3D(handles[particles[start + i].particleId], handles[particles[start + j].particleId]))
							pairs.push_back(Constraint3D(Constraint3D::COLL_PAIR, particles[start + i].particleId, particles[start + j].particleId, cellSize));
				// TODO: sentinels
				if (x + 1 < width)
				{
					TestCell(handles, pairs, count, start, x + 1, y, z);
				}
				if (y + 1 < height && x - 1 >= 0)
				{
					TestCell(handles, pairs, count, start, x - 1, y + 1, z);
				}
				if (y + 1 < height)
				{
					TestCell(handles, pairs, count, start, x, y + 1, z);
				}
				if (y + 1 < height && x + 1 < width)
				{
					TestCell(handles, pairs, count, start, x + 1, y + 1, z);
				}

				// z + 1
				if (z + 1 < depth)
				{
					TestCell(handles, pairs, count, start, x, y, z + 1);
				}
				if (x + 1 < width && z + 1 < depth)
				{
					TestCell(handles, pairs, count, start, x + 1, y, z + 1);
				}
				if (y + 1 < height && x - 1 >= 0 && z + 1 < depth)
				{
					TestCell(handles, pairs, count, start, x - 1, y + 1, z + 1);
				}
				if (y + 1 < height && z + 1 < depth)
				{
					TestCell(handles, pairs, count, start, x, y + 1, z + 1);
				}
				if (y + 1 < height && x + 1 < width && z + 1 < depth)
				{
					TestCell(handles, pairs, count, start, x + 1, y + 1, z + 1);
				}

				// z - 1
				if (z - 1 >= 0)
				{
					TestCell(handles, pairs, count, start, x, y, z - 1);
				}
				if (x + 1 < width && z - 1 >= 0)
				{
					TestCell(handles, pairs, count, start, x + 1, y, z - 1);
				}
				if (y + 1 < height && x - 1 >= 0 && z - 1 >= 0)
				{
					TestCell(handles, pairs, count, start, x - 1, y + 1, z - 1);
				}
				if (y + 1 < height && z - 1 >= 0)
				{
					TestCell(handles, pairs, count, start, x, y + 1, z - 1);
				}
				if (y + 1 < height && x + 1 < width && z - 1 >= 0)
				{
					TestCell(handles, pairs, count, start, x + 1, y + 1, z - 1);
				}
			}
		}
	}
}

void GridBroadphase3D::TestObject(const Aabb3& aabb, std::vector<int>& ids)
{
	Vector3 minV = aabb.min - offset;
	Vector3 maxV = aabb.max - offset;
	int iMin = max(0, min(width - 1, (int)(minV.X() / cellSize)));
	int iMax = max(0, min(width - 1, (int)(maxV.X() / cellSize)));
	int jMin = max(0, min(height - 1, (int)(minV.Y() / cellSize)));
	int jMax = max(0, min(height - 1, (int)(maxV.Y() / cellSize)));
	int kMin = max(0, min(depth - 1, (int)(minV.Z() / cellSize)));
	int kMax = max(0, min(depth - 1, (int)(maxV.Z() / cellSize)));
	ids.clear();
	for (int x = iMin; x <= iMax; x++)
	{
		for (int y = jMin; y <= jMax; y++)
		{
			for (int z = kMin; z <= kMax; z++)
			{
				int id = x + y * width + z * width * height;
				if (cells[id] < 0) continue;
				int count = (cells[id] >> 24) & 0xff;
				int start = cells[id] & 0xffffff;
				for (int i = 0; i < count; i++)
					ids.push_back(particles[start + i].particleId);
			}
		}
	}
}