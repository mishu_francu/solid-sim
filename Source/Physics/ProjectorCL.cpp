#include "ProjectorCL.h"
#include <Math/Utils.h>

namespace Physics
{
	void JacobiProjectorCL::PrepareBuffers(const std::vector<Particle>& particles, const std::vector<Link>& links)
	{
		size_t n = particles.size();
		nParticlesReal = n;
		nParticles = NextMultiple(n, WAVE_SIZE);
		nLinksReal = links.size();
		nLinks = NextMultiple(links.size(), WAVE_SIZE1);

		// release old ones
		positions.Release();
		edges.Release();
		disp.Release();
		invMass.Release();
		incEdges.Release();
		acc.Release();

		if (!positions.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * nParticles))
			Printf("Failed creating buffer\n");
		prev.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * nParticles);
		acc.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Accumulator) * nParticles);

		size_t maxContacts = 10 * nParticles;
		ctBuffer.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(ContactCL) * maxContacts);
		//indices.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(int) * maxContacts);
		//normals.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(Vector4) * maxContacts);
		//points.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(Vector4) * maxContacts);

		size_t maxSelfTris = 10 * nParticles;
		selfTris.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(SelfContactCL) * maxSelfTris);

		bool ret = edges.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(Edge) * nLinks, NULL);
		disp.Create(CL_MEM_ALLOC_HOST_PTR | CL_MEM_READ_WRITE, sizeof(Vector4) * nLinks);

		std::vector<float> im(nParticles);
		for (size_t i = 0; i < n; i++)
		{
			im[i] = particles[i].invMass;
		}
		invMass.Create(CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float) * nParticles, &im[0]);
		incEdges.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Incidence) * nParticles, NULL);

		OpenCL* compute = OpenCL::GetInstance();
		if (!compute)
			return;

		// copy edges
		Edge* edgePtr = (Edge*)compute->EnqueueMap(edges, true, CL_MAP_WRITE);
		memset(edgePtr, 0, nLinks * sizeof(Edge));
		Incidence* incPtr = (Incidence*)compute->EnqueueMap(incEdges, true, CL_MAP_WRITE);		
		memset(incPtr, 0, sizeof(Incidence) * nParticles);
		count.resize(n);
		memset(&count[0], 0, count.size() * sizeof(int));
		for (size_t i = 0; i < links.size(); i++)
		{
			Edge edge;

			edge.len = links[i].len;
			edge.i1 = links[i].i1;
			edge.stiff = links[i].relaxation;

			int info = i + 1;

			int i1 = links[i].i1;
			int& n1 = count[i1];
			edge.i1 |= n1 << 24;
			ASSERT(n1 < MAX_INCIDENCE);
			incPtr[i1].info[n1++] = -info;

			int i2 = links[i].i2;
			int& n2 = count[i2];
			ASSERT(n2 < MAX_INCIDENCE);
			edge.i2 = i2 | (n2 << 24);
			incPtr[i2].info[n2++] = info;

			edgePtr[i] = edge;
		}
		compute->EnqueueUnmap(edges, edgePtr);
		compute->EnqueueUnmap(incEdges, incPtr);

		// reset accumulators
		Accumulator* accPtr = (Accumulator*)compute->EnqueueMap(acc, true, CL_MAP_WRITE);
		memset(accPtr, 0, nParticles * sizeof(Accumulator));
		compute->EnqueueUnmap(acc, accPtr);

		// reset displacements
		Vector4* dispPtr = (Vector4*)compute->EnqueueMap(disp, true, CL_MAP_WRITE);
		memset(dispPtr, 0, nLinks * sizeof(Vector4));
		compute->EnqueueUnmap(disp, dispPtr);
	}

	void JacobiProjectorCL::CopyBuffers(const std::vector<Particle>& particles, const std::vector<Contact>& contacts, const std::vector<SelfContact>& st)
	{
		//PROFILE_SCOPE("CopyBuffers");
		size_t n = particles.size();
		OpenCL* compute = OpenCL::GetInstance();
		if (!compute)
			return;
		
		// copy contacts
		nContacts = contacts.size(); 
		size_t nc = NextMultiple(contacts.size(), WAVE_SIZE1);
		ContactCL* ctPtr = (ContactCL*)compute->EnqueueMap(ctBuffer, true, CL_MAP_WRITE);
		//Vector4* normalPtr = (Vector4*)compute->EnqueueMap(normals, true, CL_MAP_WRITE);
		//Vector4* pointPtr = (Vector4*)compute->EnqueueMap(points, true, CL_MAP_WRITE);
		memset(ctPtr, 0, sizeof(ContactCL) * nc);
		//memset(pointPtr, 0, sizeof(Vector4) * nc);
		std::vector<int> countDup(count);
		for (size_t i = 0; i < contacts.size(); i++)
		{			
			int i1 = contacts[i].idx;
			int& n1 = countDup[i1];
			ASSERT(n1 < MAX_INCIDENCE);
			n1++;
			ctPtr[i].n = contacts[i].normal;
			ctPtr[i].p = contacts[i].point;
			ctPtr[i].idx = i1 | (n1 << 24);
			ctPtr[i].lambda = 0;
		}

		compute->EnqueueUnmap(ctBuffer, ctPtr);
		//compute->EnqueueUnmap(points, pointPtr);
		//compute->EnqueueUnmap(indices, idxPtr);

		// copy self triangles
		nSelfTris = st.size();
		if (nSelfTris)
		{
			size_t nst = NextMultiple(nSelfTris, WAVE_SIZE1);
			ASSERT(nst <= 10 * nParticles);
			SelfContactCL* stPtr = (SelfContactCL*)compute->EnqueueMap(selfTris, true, CL_MAP_WRITE);
			memset(stPtr, 0, sizeof(SelfContactCL) * nst);
			for (size_t i = 0; i < nSelfTris; i++)
			{
				SelfContactCL item;
				item.i1 = st[i].i1;
				item.i2 = st[i].i2;
				item.i3 = st[i].i3;
				item.i4 = st[i].i4;
				item.w1 = st[i].w1;
				item.w2 = st[i].w2;
				item.w3 = st[i].w3;
				item.normal = st[i].normal;
				stPtr[i] = item;
			}
			compute->EnqueueUnmap(selfTris, stPtr);
		}

		// copy positions
		// TODO: strided read/write
		Vector4* posPtr = (Vector4*)compute->EnqueueMap(positions, true, CL_MAP_WRITE);
		Vector4* prevPtr = (Vector4*)compute->EnqueueMap(prev, true, CL_MAP_WRITE);
		for (size_t i = 0; i < n; i++)
		{
			posPtr[i] = particles[i].pos;
			prevPtr[i] = particles[i].prev;
		}
		compute->EnqueueUnmap(prev, prevPtr);
		compute->EnqueueUnmap(positions, posPtr);
		posPtr = NULL;
	}

	void ConjResProjectorCL1::Init()
	{
		OpenCL* ocl = OpenCL::GetInstance();
		if (!ocl)
			return;
		program = ocl->Build("../Shaders/Solver.cl");//, "-g -s C:\\Work\\GameEngine\\Source\\Demo\\Particles\\Solver.cl");
		// Create kernel object
		if (program)
		{
			jacEdgesKernel.Create(program, "conjResEdges1");
			jacContactsKernel.Create(program, "conjResContacts");
			jacSelfTriKernel.Create(program, "conjResSelfTris");
			jacParticlesKernel.Create(program, "minResParticles1red");
			OpenCL::GetInstance()->GetKernelWorkGroupInfo(jacEdgesKernel.Get());
		}
		//else
		//Engine::getInstance()->Quit();
	}

	void ConjResProjectorCL1::ProjectPositions(float thickness, float friction)
	{
		if (nParticles == 0 || nLinks == 0)
			return;

		float alpha = 0.5f; // TODO: param

		//PROFILE_SCOPE("ProjectConjResCL");
		jacEdgesKernel.SetArgument(0, &edges);
		jacEdgesKernel.SetArgument(1, &positions); // TODO: this the only one that changes
		jacEdgesKernel.SetArgument(2, &invMass);
		jacEdgesKernel.SetArgument(3, &disp);
		jacEdgesKernel.SetArgument(4, &alpha);
		jacEdgesKernel.SetArgument(6, &acc);

		jacContactsKernel.SetArgument(0, &ctBuffer);
		//jacContactsKernel.SetArgument(1, &points);
		//jacContactsKernel.SetArgument(2, &normals);
		jacContactsKernel.SetArgument(1, &positions);
		jacContactsKernel.SetArgument(2, &invMass);
		jacContactsKernel.SetArgument(3, &prev);
		jacContactsKernel.SetArgument(4, &thickness);
		jacContactsKernel.SetArgument(5, &friction);
		//jacContactsKernel.SetArgument(8, &disp);

		jacSelfTriKernel.SetArgument(0, &selfTris);
		jacSelfTriKernel.SetArgument(1, &positions);
		jacSelfTriKernel.SetArgument(2, &invMass);
		jacSelfTriKernel.SetArgument(3, &thickness);
		jacSelfTriKernel.SetArgument(4, &acc);

		jacParticlesKernel.SetArgument(0, &positions);
		jacParticlesKernel.SetArgument(1, &invMass);
		jacParticlesKernel.SetArgument(2, &acc);

		OpenCL* compute = OpenCL::GetInstance();
		size_t localSize = WAVE_SIZE;
		size_t localSize1 = WAVE_SIZE1;
		size_t workSize = nParticles * 16;
		for (int k = 0; k < numIterations; ++k)
		{
			float beta = (float)k / (float)(numIterations - 1);
			if (beta > 0)
				beta = pow(beta, 0.6f);
			jacEdgesKernel.SetArgument(5, &beta);
			compute->EnqueueKernel(jacEdgesKernel, &nLinks, &localSize1);
			compute->EnqueueKernel(jacParticlesKernel, &workSize, &localSize);
			if (nContacts != 0)
			{
				//jacContactsKernel.SetArgument(9, &beta);
				size_t nc = NextMultiple(nContacts, WAVE_SIZE1);
				compute->EnqueueKernel(jacContactsKernel, &nc, &localSize1);
			}
			if (nSelfTris != 0)
			{
				size_t nst = NextMultiple(nSelfTris, WAVE_SIZE1);
				compute->EnqueueKernel(jacSelfTriKernel, &nst, &localSize1);
			}
		}
		clFlush(compute->GetCommandQueue());
	}

	void ConjResProjectorCL::Init()
	{
		OpenCL* ocl = OpenCL::GetInstance();
		if (!ocl)
			return;
		// prefer CPU for this projector
		ocl->SetDeviceType(CL_DEVICE_TYPE_CPU);
		ocl->ReInit();

		program = ocl->Build("../Shaders/Solver.cl");//, "-g -s C:\\Work\\GameEngine\\Source\\Demo\\Particles\\Solver.cl");
		// Create kernel object
		if (program)
		{
			jacEdgesKernel.Create(program, "conjResEdges");
			jacContactsKernel.Create(program, "conjResContacts");
			jacSelfTriKernel.Create(program, "conjResSelfTris");
			jacParticlesKernel.Create(program, "minResParticles");
			OpenCL::GetInstance()->GetKernelWorkGroupInfo(jacEdgesKernel.Get());
		}
	}

	void ConjResProjectorCL::Run(float thickness, float friction)
	{
		if (nParticles == 0 || nLinks == 0)
			return;

		float alpha = 0.5f;

		//PROFILE_SCOPE("ProjectConjResCL");
		jacEdgesKernel.SetArgument(0, &edges);
		jacEdgesKernel.SetArgument(1, &positions); // TODO: this the only one that changes
		jacEdgesKernel.SetArgument(2, &invMass);
		jacEdgesKernel.SetArgument(3, &disp);
		jacEdgesKernel.SetArgument(4, &alpha);

		jacParticlesKernel.SetArgument(0, &positions);
		jacParticlesKernel.SetArgument(1, &invMass);
		jacParticlesKernel.SetArgument(2, &incEdges);
		jacParticlesKernel.SetArgument(3, &disp);

		jacContactsKernel.SetArgument(0, &ctBuffer);
		jacContactsKernel.SetArgument(1, &positions);
		jacContactsKernel.SetArgument(2, &invMass);
		jacContactsKernel.SetArgument(3, &prev);
		jacContactsKernel.SetArgument(4, &thickness);
		jacContactsKernel.SetArgument(5, &friction);

		jacSelfTriKernel.SetArgument(0, &selfTris);
		jacSelfTriKernel.SetArgument(1, &positions);
		jacSelfTriKernel.SetArgument(2, &invMass);
		jacSelfTriKernel.SetArgument(3, &thickness);
		jacSelfTriKernel.SetArgument(4, &acc);

		OpenCL* compute = OpenCL::GetInstance();
		size_t localSize = WAVE_SIZE;
		size_t localSize1 = WAVE_SIZE1;
		float theta = 1;
		for (int k = 0; k < numIterations; ++k)
		{
			//float beta = (float)k / (float)(numIterations - 1);
			//if (beta > 0)
			//	beta = pow(beta, 0.6f);
			float theta0 = theta;
			theta = 0.5f * (-theta * theta + theta * sqrt(theta * theta +4));
			float beta = theta0 * (1 - theta0) / (theta0 * theta0 + theta);

			jacEdgesKernel.SetArgument(5, &beta);
			compute->EnqueueKernel(jacEdgesKernel, &nLinks, &localSize1);
			compute->EnqueueKernel(jacParticlesKernel, &nParticles, &localSize);
			if (nContacts != 0)
			{
				size_t nc = NextMultiple(nContacts, WAVE_SIZE1);
				compute->EnqueueKernel(jacContactsKernel, &nc, &localSize1);
			}
			if (nSelfTris != 0)
			{
				size_t nst = NextMultiple(nSelfTris, WAVE_SIZE1);
				compute->EnqueueKernel(jacSelfTriKernel, &nst, &localSize1);
			}
		}
		clFlush(compute->GetCommandQueue());
	}
}