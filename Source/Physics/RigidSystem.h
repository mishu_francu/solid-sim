#ifndef RIGID_SYSTEM_H
#define RIGID_SYSTEM_H

#include <vector>
#include <Math/Matrix3.h>
#include <Math/Quaternion.h>

#include <memory>

namespace Physics
{
	const float MU_IMP = 0.2f;

	struct Rigid
	{
		enum ShapeType
		{
			BOX,
			SPHERE,
			CYLINDER
		};

		struct Shape
		{
			int type;
			Vector3 X; // absolute position
			Matrix3 R; // absolute rotation
			Vector3 T; //pozitia relativa (translatia)
			Matrix3 O; //rotatia relativa (orientarea)
			Quaternion q;	
		};

		struct Box: Shape
		{
			//float a, b, c; //astea ar cam trebui scoase
			Vector3 D; //dimensiuni (a, b, c)
			Box() { type = BOX; }
			//void SetDimensions(float a, float b, float c);
			void GetSpan(Vector3 axis, float& min, float& max) const
			{
				float r = 0;
				for(int i=0; i<3; i++)
					r += fabsf(axis * R(i)) * D[i];
				float p = X * axis;
				min = p-r;
				max = p+r;
			}
			void GetSupportPoint(Vector3 axis, Vector3 &P) const
			{
				P = X;
				const float threshold = 0.001f;
				for(int i=0; i<3; i++){
					float a = axis * R(i);
					if (a < -threshold)
						P += R(i) * D[i];
					else if (a >= threshold)
						P -= R(i) * D[i];
				}
			}
		};

		struct Sphere: public Shape
		{
			float r;
			Sphere() { type = SPHERE; }
		};

		float invmass;
		Matrix3 Ic, Icinv, I, Iinv, R;
		Vector3 X, v, w, F, M, f, m; // TODO: rename
		Quaternion q;
		std::vector<std::shared_ptr<Shape>> shapes; // TODO: use unique_ptr

		Rigid() : invmass(1) { }
	};

	class RigidSystem
	{
	public:
		enum { UNIT = 10 };
		enum ConstraintType
		{
			BALL_SOCKET,
			ROD,
			CONTACT,
			FRICTION,
			DFRICTION,
			HINGE
		};

		struct Constraint
		{
			int type;
			int i, j;
			float C;
			float beta; // Baumgarte factor
			float lmin, lmax;
			float Jsp1[6], Jsp2[6];
			float Bsp1[6], Bsp2[6];
			float Jf1[6], Jf2[6];
		};

		struct Joint // TODO: inherit from Constraint
		{
			int type;
			int i, j;
			int k; // index of first constraint
			Vector3 a1, a2;
			float L; // for rod
			Vector3 n, t1, t2; // for contact	
			Vector3 s1, s2; // hinge
			float depth;
			// added for SI
			float lambda, lambdaF1, lambdaF2;
			float dLambda, dLambdaF1, dLambdaF2;
			float stiffness;
		};

		struct Contact
		{
			Rigid *A, *B;
			int i1, i2;
			Vector3 pa, pb, n, a1, a2;
			float depth;
		};

	public:
		RigidSystem()
			: h(0.016f)
			, g(-10)
			, mu(0.1f)
			, numIterations(10)
			, beta(0.1f)
		{
		}
		void Step();
		int AddRigid(Rigid& rigid);
		size_t GetNumRigids() const { return rigids.size(); }
		Rigid* GetRigid(size_t i) { return &rigids[i]; }
		int AddContact(int i, int j, Vector3 a1, Vector3 a2, Vector3 n, float depth);
		size_t GetNumContacts() const { return contacts.size(); }
		Contact GetContact(size_t i) const { return contacts[i]; }
		size_t GetNumJoints() const { return links.size(); }
		Joint GetJoint(size_t i) const { return links[i]; }

		// SI/PBD part
		void StepSI();
		void StepPBD();
		void AddLink(size_t i1, size_t i2, float stiffness);

	private:
		void DetectCollision();
		void UpdateJoints();
		bool CollideRigids(int i, int j);
		bool CollideShapes(Rigid::Shape* shape1, Rigid::Shape* shape2, int i1, int i2);

	private:
		float h, g, mu, beta;
		int numIterations;
		std::vector<Rigid> rigids;
		std::vector<Contact> contacts;
		std::vector<Constraint> map;
		std::vector<Joint> joints; // rename to constraints

		// SI part
		std::vector<Joint> links; // rename to joints
	};
}

#endif // RIGID_SYSTEM_H