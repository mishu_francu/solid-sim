#include "ClothModel.h"

namespace Physics
{
	class ClothModelOpen : public ClothModel
	{
	public:
		ClothModelOpen(ClothPatch* owner) : ClothModel(owner) { }
		ClothModelOpen(const ClothModel& model) : ClothModel(model) { }
		void Step(float h) override;
		void Init() override;
	private:
		void ComputeForces();
		void IntegrateImplicit(float deltaTime);
		void CalcForceDerivatives();
		void AddSpring(int a, int b, float ks, float kd, int type);

	//private:
	//	std::vector<glm::vec3> F;
	};
}