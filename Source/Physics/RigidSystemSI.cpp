#include "RigidSystem.h"
#include <algorithm>

#define CCP

namespace Physics
{
	void RigidSystem::StepSI()
	{
		// integrate velocities
		Vector3 gravity(0, g * UNIT, 0);
		for (size_t i = 0; i < rigids.size(); i++)
		{
			if (rigids[i].invmass == 0.f)
				continue;
			// update rotation (for collisions) and inertia first
			rigids[i].R = qToMatrix(rigids[i].q);
			rigids[i].Iinv = rigids[i].R * rigids[i].Icinv * !rigids[i].R;

			rigids[i].v += h * gravity;
			//rigids[i].w += h * torque; // no external torques for now
		}

		DetectCollision();
		joints.insert(joints.end(), links.begin(), links.end());

		// project velocities
		for (size_t i = 0; i < joints.size(); i++)
		{
			RigidSystem::Joint& pair = joints[i];
			if (pair.type == CONTACT)
			{
				// compute tangent frame for box friction
				pair.t1 = pair.n.Perpendicular();
				pair.t2 = pair.n.Cross(pair.t1);
				pair.t2.Normalize();
			}			

			pair.lambda = 0.f;
			pair.lambdaF1 = 0.f;
			pair.lambdaF2 = 0.f;

			pair.dLambda = 0.f;
			pair.dLambdaF1 = 0.f;
			pair.dLambdaF2 = 0.f;
		}

		for (int k = 0; k < numIterations; ++k)
		{
			for (size_t i = 0; i < joints.size(); i++)
			{
				RigidSystem::Joint& pair = joints[i];
				Rigid* p1 = &rigids[pair.i];
				Rigid* p2 = &rigids[pair.j];

				if (pair.type == ROD)
				{
					float mu = 1.f / (p1->invmass + p2->invmass);

					// compute position error
					Vector3 delta = p1->X - p2->X; // TODO: cache
					const float len0 = pair.L;
					float len = delta.Length();
					delta.Scale(1 / len);
					float err = len - len0;

					// compute relative velocity
					Vector3 v12 = p2->v - p1->v;
					float vnrel = pair.n.Dot(v12);

					// TODO: feedback term -> reg
					//float epsilon = len0 / (k * link.stiffness);
					//float s = 1.f / (1.f + mu * epsilon);
					float lambda = mu * (vnrel + beta * err / h);
					pair.lambda += lambda;

					//float omega = 1;
					delta.Scale(lambda);
					p1->v -= delta * p1->invmass;
					p2->v += delta * p2->invmass;
				}
				else if (pair.type == CONTACT)
				{
					Vector3 a1 = p1->R * pair.a1;
					Vector3 a2 = p2->R * pair.a2;
					Vector3 v1 = p1->v + cross(p1->w, a1);
					Vector3 v2 = p2->v + cross(p2->w, a2);
					Vector3 v12 = v2 - v1;
					float vnrel = pair.n.Dot(v12); // TODO: precompute axn

					Vector3 a1xn = cross(a1, pair.n);
					Vector3 a2xn = cross(a2, pair.n);
					float inertia1 = a1xn.Dot(p1->Iinv * a1xn);
					float inertia2 = a2xn.Dot(p2->Iinv * a2xn);
#ifdef CCP
					// convex relaxation
					//Vector3 vt = v12 - vnrel * pair.normal;
					//float vtrel = vt.Length();
					//vnrel -= mu * vtrel;
#endif
					float sum = (p1->invmass + p2->invmass + inertia1 + inertia2);
					float dLambda = (vnrel + beta * pair.depth / h) / sum;

					float lambda0 = pair.lambda;
					pair.lambda = std::max(0.f, lambda0 + dLambda);
					dLambda = pair.lambda - lambda0;

					Vector3 disp = dLambda * pair.n;
					p1->v += p1->invmass * disp;
					p2->v -= p2->invmass * disp;

					p1->w += p1->Iinv * (dLambda * a1xn);
					p2->w -= p2->Iinv * (dLambda * a2xn);

					if (mu > 0.f)
					{
						float limit = mu * pair.lambda;
#ifdef CCP
						Vector3 vt = v12 - vnrel * pair.n;
						float vtrel = vt.Length();
						if (vtrel > 0.0001f)
						{
							// compute diagonal term
							vt.Scale(1.f / vtrel); // normalize
							Vector3 a1xt = cross(a1, vt);
							Vector3 a2xt = cross(a2, vt);
							inertia1 = a1xn.Dot(p1->Iinv * a1xt);
							inertia2 = a2xn.Dot(p2->Iinv * a2xt);
							sum = p1->invmass + p2->invmass + inertia1 + inertia2;

							dLambda = vtrel / sum;
							lambda0 = pair.lambdaF1;
							pair.lambdaF1 = std::min(limit, lambda0 + dLambda);
							//pair.lambdaF1 = lambda0 + dLambda;
							//if (pair.lambdaF1 >= limit)
							//pair.lambdaF1 = limit;
							dLambda = pair.lambdaF1 - lambda0;

							Vector3 p = dLambda * vt;
							p1->v += p1->invmass * p;
							p2->v -= p2->invmass * p;

							p1->w += p1->Iinv * (dLambda * a1xt);
							p2->w -= p2->Iinv * (dLambda * a2xt);
						}
#else
						float vtrel = v12.Dot(pair.t1);
						if (fabs(vtrel) >= 0.0001f)
						{
							float dLambdaF = vtrel / sum; // TODO: check
							float lambdaF0 = pair.lambdaF1;
							pair.lambdaF1 = lambdaF0 + dLambdaF;
							pair.lambdaF1 = clamp(pair.lambdaF1, -limit, limit);
							dLambdaF = pair.lambdaF1 - lambdaF0;

							Vector3 vt = dLambdaF * pair.t1;
							p1->v += vt * p1->invmass;
							p2->v -= vt * p2->invmass;

							p1->w += p1->Iinv * cross(a1, vt);
							p2->w -= p2->Iinv * cross(a2, vt);
						}

						vtrel = v12.Dot(pair.t2);
						if (fabs(vtrel) >= 0.0001f)
						{
							float dLambdaF = vtrel / sum;
							float lambdaF0 = pair.lambdaF2;
							pair.lambdaF2 = lambdaF0 + dLambdaF;
							pair.lambdaF2 = clamp(pair.lambdaF2, -limit, limit);
							dLambdaF = pair.lambdaF2 - lambdaF0;

							Vector3 vt = dLambdaF * pair.t2;
							p1->v += vt * p1->invmass;
							p2->v -= vt * p2->invmass;

							p1->w += p1->Iinv * cross(a1, vt);
							p2->w -= p2->Iinv * cross(a2, vt);
						}
#endif
					}
				}
			}
		}

		// integrate positions
		for (size_t i = 0; i < rigids.size(); i++)
		{
			if (rigids[i].invmass == 0.f)
				continue;
			rigids[i].X += h * rigids[i].v;
			rigids[i].q += 0.5f * h * (rigids[i].w * rigids[i].q); // TODO: Tasora			
			rigids[i].q = qNormalize(rigids[i].q);
		}
	}
}