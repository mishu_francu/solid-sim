#include "OpenCL.h"
#include "ClothTypes.h"
#include <Math/Vector4.h>

namespace Physics
{
	const int MAX_INCIDENCE = 16;

	struct Incidence
	{
		int info[MAX_INCIDENCE];
	};

	typedef struct
	{
		ALIGN16 cl_int size;
		cl_float4 acc[MAX_INCIDENCE];
	} Accumulator;

	class ProjectorCL
	{
	public:
		// TODO: reuse Link
		struct Edge
		{
			int i1, i2;
			float len, stiff;

			Edge() : i1(-1), i2(-1), len(0), stiff(1.f) { }
		};

		ALIGN16	struct SelfContactCL
		{
			cl_uint i1, i2, i3, i4;
			Vector4 normal;
			cl_float w1, w2, w3;
		};

		ALIGN16 struct ContactCL
		{
			Vector4 n, p;
			cl_int idx;
			cl_float lambda;
		};


	protected:
		int numIterations;
		CLBuffer positions, invMass, acc, prev, selfTris, ctBuffer;
		cl_program program;

	public:
		ProjectorCL() : numIterations(20) { }
		void ReadBuffers(std::vector<Particle>& particles);
		void SetNumIterations(int val) { numIterations = val; }
	};

	class JacobiProjectorCL : public ProjectorCL
	{
	protected:
		CLKernel jacEdgesKernel, jacParticlesKernel, jacContactsKernel, jacSelfTriKernel;
		CLBuffer edges, disp, incEdges;
		size_t nLinks, nParticles, nParticlesReal, nLinksReal, nContacts, nSelfTris;
		std::vector<int> count;

	public:
		void PrepareBuffers(const std::vector<Particle>& particles, const std::vector<Link>& links);
		void CopyBuffers(const std::vector<Particle>& particles, const std::vector<Contact>& contacts, const std::vector<SelfContact>& st);
	};

	class ConjResProjectorCL1 : public JacobiProjectorCL
	{
	public:
		void Init();
		void ProjectPositions(float thickness, float friction);
	};

	// this one is more suitable for the CPU
	class ConjResProjectorCL : public JacobiProjectorCL
	{
	public:
		void Init();
		void Run(float thickness, float friction);
	};

	inline void ProjectorCL::ReadBuffers(std::vector<Particle>& particles)
	{
		//PROFILE_SCOPE("ReadBuffers");
		// read back positions
		OpenCL* compute = OpenCL::GetInstance();
		Vector4* posPtr = (Vector4*)compute->EnqueueMap(positions, true, CL_MAP_READ);
		// TODO: memcpy
		for (size_t i = 0; i < particles.size(); i++)
		{
			particles[i].pos = posPtr[i];
		}
		compute->EnqueueUnmap(positions, posPtr);
	}
}