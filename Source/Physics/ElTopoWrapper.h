#pragma once

#include <Math/Vector3.h>
#include <Physics/ElTopo/ccd_wrapper.h>

namespace Physics
{
	// ElTopo helpers
	inline Vec3d V2ETV(const Vector3& v)
	{
		return Vec3d(v.X(), v.Y(), v.Z());
	}

	inline Vector3 ETV2V(Vec3d v)
	{
		return Vector3((float)v[0], (float)v[1], (float)v[2]);
	}

	// Return the vertices of the specified triangle, but in ascending order.
	inline Vec3st sort_triangle(const Vec3st& t)
	{
		if (t[0] < t[1])
		{
			if (t[0] < t[2])
			{
				if (t[1] < t[2])
				{
					return t;
				}
				else
				{
					return Vec3st(t[0], t[2], t[1]);
				}
			}
			else
			{
				return Vec3st(t[2], t[0], t[1]);
			}
		}
		else
		{
			if (t[1] < t[2])
			{
				if (t[0] < t[2])
				{
					return Vec3st(t[1], t[0], t[2]);
				}
				else
				{
					return Vec3st(t[1], t[2], t[0]);
				}
			}
			else
			{
				return Vec3st(t[2], t[1], t[0]);
			}
		}
	}
} // namespace Physics