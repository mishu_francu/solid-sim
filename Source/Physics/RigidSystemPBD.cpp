#include "RigidSystem.h"
#include <algorithm>

namespace Physics
{
	void RigidSystem::StepPBD()
	{
		// integrate velocities and positions (unconstrained)
		Vector3 gravity(0, g * UNIT, 0);
		for (size_t i = 0; i < rigids.size(); i++)
		{
			if (rigids[i].invmass == 0.f)
				continue;

			rigids[i].v += h * gravity;
			//rigids[i].w += h * torque; // no external torques for now

			rigids[i].X += h * rigids[i].v;
			rigids[i].q += 0.5f * h * (rigids[i].w * rigids[i].q); // TODO: Tasora			
			rigids[i].q = qNormalize(rigids[i].q);

			// update rotation and inertia
			rigids[i].R = qToMatrix(rigids[i].q);
			rigids[i].Iinv = rigids[i].R * rigids[i].Icinv * !rigids[i].R;
		}

		// collision detection
		DetectCollision();
		joints.insert(joints.end(), links.begin(), links.end());

		// zero out multipliers
		for (size_t i = 0; i < joints.size(); i++)
		{
			joints[i].lambda = 0;
			joints[i].lambdaF1 = 0;
		}
		// nonlinear constraint solver
		float invH = 1.f / h;
		for (int k = 0; k < numIterations; k++)
		{
			for (size_t i = 0; i < joints.size(); i++)
			{
				if (joints[i].type == ROD)
				{
					Joint& link = joints[i];
					Rigid& p1 = rigids[link.i];
					Rigid& p2 = rigids[link.j];

					float mu = 1.f / (p1.invmass + p2.invmass);

					Vector3 delta = p1.X - p2.X;
					const float len0 = link.L;
					float len = delta.Length();
					delta.Scale(1 / len);
					float err = len - len0;

					//if (fabs(err) < 0.01f * len0) continue;

					// TODO: feedback term -> reg
					float epsilon = len0 / (k * link.stiffness);
					float s = 1.f / (1.f + mu * epsilon); 
					float lambda = err * mu * s;
					link.lambda += lambda;

					float omega = 1;
					delta.Scale(omega * lambda);
					p1.X -= delta * p1.invmass;
					p2.X += delta * p2.invmass;
					p1.v -= invH * delta * p1.invmass;
					p2.v += invH * delta * p2.invmass;
				}
				else if (joints[i].type == CONTACT)
				{
					RigidSystem::Joint& pair = joints[i];
					Rigid* p1 = &rigids[pair.i];
					Rigid* p2 = &rigids[pair.j];

					// TODO: update orientation here?

					// world contact positions
					Vector3 a1 = p1->R * pair.a1;
					Vector3 a2 = p2->R * pair.a2;

					// normal part of inertia
					Vector3 a1xn = cross(a1, pair.n);
					Vector3 a2xn = cross(a2, pair.n);
					float inertia1 = a1xn.Dot(p1->Iinv * a1xn);
					float inertia2 = a2xn.Dot(p2->Iinv * a2xn);
					float inertia = p1->invmass + p2->invmass + inertia1 + inertia2;

					// compute and clamp lambda
					float dLambda = -pair.n.Dot(p1->X + a1 - p2->X - a2) / inertia;
					float lambda0 = pair.lambda;
					pair.lambda = std::max(0.f, lambda0 + dLambda);
					dLambda = pair.lambda - lambda0;

					// integrate using lambda
					Vector3 imp = dLambda * pair.n;
					Vector3 dw1 = dLambda * a1xn;
					Vector3 dw2 = -dLambda * a2xn;

					// rigid 1
					p1->v += invH * p1->invmass * imp;
					p1->w += invH * p1->Iinv * dw1;

					p1->X += p1->invmass * imp;
					p1->q += 0.5f * ((p1->Iinv * dw1) * p1->q);
					p1->q = qNormalize(p1->q);

					p1->R = qToMatrix(p1->q);
					p1->Iinv = p1->R * p1->Icinv * !p1->R;

					// rigid 2
					p2->v -= invH * p2->invmass * imp;
					p2->w += invH * p2->Iinv * dw2;

					p2->X -= p2->invmass * imp;
					p2->q += 0.5f * ((p2->Iinv * dw2) * p2->q);
					p2->q = qNormalize(p2->q);

					p2->R = qToMatrix(p2->q);
					p2->Iinv = p2->R * p2->Icinv * !p2->R;

					// CCP friction
					if (mu > 0)
					{
						// relative velocity
						Vector3 v1 = p1->v + cross(p1->w, a1);
						Vector3 v2 = p2->v + cross(p2->w, a2);
						Vector3 v12 = v2 - v1;
						float vnrel = pair.n.Dot(v12);
						Vector3 vt = v12 - vnrel * pair.n;
						float vtrel = vt.Length();

						float limit = mu * invH * pair.lambda;
						if (vtrel > 0.0001f) // TODO: handle this better
						{
							// compute diagonal term
							vt.Scale(1.f / vtrel); // normalize
							Vector3 a1xt = cross(a1, vt); // TODO: update a1 or run friction first?
							Vector3 a2xt = cross(a2, vt);
							inertia1 = a1xn.Dot(p1->Iinv * a1xt);
							inertia2 = a2xn.Dot(p2->Iinv * a2xt);
							inertia = p1->invmass + p2->invmass + inertia1 + inertia2;

							dLambda = vtrel / inertia;
							lambda0 = pair.lambdaF1;
							pair.lambdaF1 = std::min(limit, lambda0 + dLambda);
							dLambda = pair.lambdaF1 - lambda0;

							Vector3 p = dLambda * vt;
							dw1 = dLambda * a1xt;
							dw2 = -dLambda * a2xt;

							// rigid 1
							p1->v += p1->invmass * p;
							p1->w += p1->Iinv * dw1;

							p1->X += h * p1->invmass * p;
							p1->q += 0.5f * h * ((p1->Iinv * dw1) * p1->q);
							p1->q = qNormalize(p1->q);

							p1->R = qToMatrix(p1->q);
							p1->Iinv = p1->R * p1->Icinv * !p1->R;

							// rigid 2
							p2->v -= p2->invmass * p;
							p2->w += p2->Iinv * dw2;

							p2->X -= h * p2->invmass * p;
							p2->q += 0.5f * h * ((p2->Iinv * dw2) * p2->q);
							p2->q = qNormalize(p2->q);

							p2->R = qToMatrix(p2->q);
							p2->Iinv = p2->R * p2->Icinv * !p2->R;

							// TODO: use old velocities and orientations and integrate only once normal+friction
						}
					}
				}
			}
		}
	}

	void RigidSystem::AddLink(size_t i1, size_t i2, float stiffness)
	{
		if (rigids[i1].invmass == 0 && rigids[i2].invmass == 0)
			return;
		if (stiffness == 0)
			return;
		Joint link;
		link.type = ROD;
		link.i = i1;
		link.j = i2;
		link.stiffness = stiffness;
		link.L = (rigids[i1].X - rigids[i2].X).Length();
		//link.disp.SetZero();
		links.push_back(link);
	}

}