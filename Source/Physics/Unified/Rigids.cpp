#include "Rigids.h"
#include "CollisionManager.h"
#include <Engine/Profiler.h>

namespace Physics
{
	void Rigids::UpdateCollidables(CollisionManager& collMgr, Bodies& bodies)
	{
		for (size_t i = 0; i < mCount; i++)
		{
			Body& body = bodies[i + mStart];
			if (body.collIdx < 0)
				continue;
			collMgr.UpdateShape(body.collIdx, body.pos, body.q);
		}
	}

	void Rigids::CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints)
	{
		//mRigidContacts.Clear();
		const std::vector<CollisionManager::Contact>& newContacts = collMgr.GetContacts();
		
		//for (size_t i = 0; i < mPrevContacts.GetNumContacts(); i++)
		//{
		//	const RigidContact& contact = mPrevContacts.GetContact(i);
		//	mRigidContacts.AddContact(contact);
		//}
		//mPrevContacts.Clear();
		for (size_t i = 0; i < newContacts.size(); i++)
		{
			const CollisionManager::Contact& contact = newContacts[i];
			if ((contact.i1 >= mStart && contact.i1 < mStart + mCount) && (contact.i2 >= mStart && contact.i2 < mStart + mCount))
			{
				Body& A = bodies[contact.i1];
				Body& B = bodies[contact.i2];
				if (A.invMass == 0 && B.invMass == 0)
					continue;
				auto shapeA = collMgr.GetCollidable(A.collIdx);
				auto shapeB = collMgr.GetCollidable(B.collIdx);
				Vector3 a1 = !(bodies[contact.i1].R) * (contact.a1 + shapeA->mTolerance * contact.n- bodies[contact.i1].pos);
				Vector3 a2 = !(bodies[contact.i2].R) * (contact.a2 - shapeB->mTolerance * contact.n - bodies[contact.i2].pos);
				float d = contact.depth - (shapeA->mTolerance + shapeB->mTolerance);				
				AddContact(contact.i1, contact.i2, a1, a2, contact.n, d);
			}
		}		

		constraints.AddList(&mRigidContacts);
		constraints.AddList(&mRigidJoints);
	}

	void Rigids::DetectCollision(Bodies& bodies, Collidables& collidables, Constraints& constraints, bool beforePos)
	{
		mRigidContacts.Clear();
		for (size_t i = 0; i < mCount - 1; i++)
		{
			for (size_t j = i + 1; j < mCount; j++)
			{
				if (bodies[i + mStart].collIdx < 0 || bodies[j + mStart].collIdx < 0)
					continue;
				const Collidable* shape1 = collidables[bodies[i + mStart].collIdx].get();
				const Collidable* shape2 = collidables[bodies[j + mStart].collIdx].get();
				if (shape1->mType == CT_SPHERE && shape2->mType == CT_SPHERE)
				{
					const Sphere* sph1 = (const Sphere*)shape1;
					const Sphere* sph2 = (const Sphere*)shape2;
					// FIXME!
					//RigidSystem::Contact info;
					//if (SphereSphere(sph1, sph2, info))
					//{
					//	Vector3 a1 = !(bodies[i + mStart].R) * (info.pa - bodies[i + mStart].pos);
					//	Vector3 a2 = !(bodies[j + mStart].R) * (info.pb - bodies[j + mStart].pos);
					//	AddContact(i, j, a1, a2, info.n, info.depth);
					//}
				}
				else if (shape1->mType == CT_MESH && shape2->mType == CT_MESH)
				{
					const CollisionMesh* collMesh1 = (const CollisionMesh*)shape1;
					const CollisionMesh* collMesh2 = (const CollisionMesh*)shape2;
					const Geometry::Mesh* mesh1 = collMesh1->mesh;
					const Geometry::Mesh* mesh2 = collMesh2->mesh;
					float radTol = collMesh1->mTolerance + collMesh2->mTolerance;

					MeshCollisionsBullet::MeshVsMesh(bodies, i + mStart, j + mStart, mesh1, mesh2, radTol, true, this);
					//MeshCollisionsElTopo::CollideMeshesCCD(bodies, i + mStart, j + mStart, mesh1, mesh2, radTol, this);
				}
				else if (shape1->mType == CT_MESH && shape2->mType == CT_BOX)
				{
					const CollisionMesh* collMesh1 = (const CollisionMesh*)shape1;
					const Box* box2 = (const Box*)shape2;
					const Geometry::Mesh* mesh1 = collMesh1->mesh;
					const Geometry::Mesh* mesh2 = &box2->mesh;
					float radTol = collMesh1->mTolerance + box2->mTolerance;

					int idx1 = i + mStart;
					int idx2 = j + mStart;

					//MeshCollisionsBullet::MeshVsMesh(bodies, i + mStart, j + mStart, mesh1, mesh2, radTol, true, this);
					MeshCollisions::MeshVsMesh(bodies, i + mStart, j + mStart, mesh1, mesh2, radTol,
						bodies[idx1].R, bodies[idx1].pos, bodies[idx1].pos0,
						bodies[idx2].R, bodies[idx2].pos, bodies[idx2].pos0,
						true, this);
					//MeshCollisions::CollideMeshesCCD(bodies, i + mStart, j + mStart, mesh1, mesh2, radTol, this);
				}
				//else if (shape1->mType == CT_BOX && shape2->mType == CT_MESH)
				//{
				//	const CollisionMesh* collMesh2 = (const CollisionMesh*)shape2;
				//	const Box* box1 = (const Box*)shape1;
				//	const Physics::Mesh* mesh1 = &box1->mesh;
				//	const Physics::Mesh* mesh2 = collMesh2->mesh;
				//	float radTol = collMesh2->mTolerance + box1->mTolerance;

				//	MeshVsMesh(bodies, i, j, mesh1, mesh2, radTol);
				//}
			}
		}
		//constraints.AddList(&mRigidContacts);
	}

	void Rigids::AddJoint(Bodies& bodies, int i, int j, const Vector3& p)
	{
		RigidJoint joint;
		joint.i = i;
		joint.j = j;
		joint.a1 = !bodies[i].R * (p - bodies[i].pos);
		joint.a2 = !bodies[j].R * (p - bodies[j].pos);
		mRigidJoints.AddJoint(joint);
	}

    void Rigids::AddLockedJoint(Bodies& bodies, int i, int j, const Vector3& p)
    {
        LockedJoint joint;
        joint.i = i;
        joint.j = j;
        joint.a1 = !bodies[i].R * (p - bodies[i].pos);
        joint.a2 = !bodies[j].R * (p - bodies[j].pos);
        // store vector part of relative quaternion
        Quaternion q = bodies[i].q.GetConjugate() * bodies[j].q;
        joint.q = q;
        mRigidJoints.AddLockedJoint(joint);
    }

    void Rigids::AddHingeJoint(Bodies& bodies, int i, int j, const Vector3& p, const Vector3& axis)
    {
        HingeJoint joint;
        joint.i = i;
        joint.j = j;
        joint.a1 = !bodies[i].R * (p - bodies[i].pos);
        joint.a2 = !bodies[j].R * (p - bodies[j].pos);
        joint.axis1 = !bodies[i].R * axis;
        joint.axis2 = !bodies[j].R * axis;

        // build an orthonormal frame (in world space)
        Vector3 n = axis;
        Vector3 u = n.Perpendicular();
        Vector3 v = cross(n, u);
        v.Normalize();
        Matrix3 R(n, u, v); // the hinge axis is the Z axis
        Quaternion t;
        t.SetFromMatrix(R);

        joint.q1 = bodies[i].q.GetConjugate() * t;
        joint.q2 = bodies[j].q.GetConjugate() * t;
        
        mRigidJoints.AddHingeJoint(joint);
    }
}