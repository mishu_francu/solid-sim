#ifndef RIGID_JOINTS_H
#define RIGID_JOINTS_H

#include <Math/Vector3.h>
#include <vector>
#include "Unified.h"

namespace Physics
{
    // Ball and socket joint
	struct RigidJoint
	{
		Vector3 a1, a2; // anchors
		Vector3 delta; // displacement between anchors
		size_t i, j; // indices of the bodies
	};

    // Fully locked (fixed) joint
    struct LockedJoint : RigidJoint
    {
        Matrix3 J; // Jacobian - caching storage for solver use only
        Matrix3 Ainv; // inverse of system matrix - caching storage for solver use only
        Quaternion q; // initial relative quaternion
        Vector3 stab; // stabilization term - caching storage for solver use only
    };

    // Hinge joint
    struct HingeJoint : RigidJoint
    {
        Vector3 axis1, axis2; // the hinge axis represented in the local spaces of the bodies
        Quaternion q1, q2; // temp: the initial quaternions of the bodies
    };

	class RigidJoints : public IConstraintList
	{
	public:
		RigidJoints() : mBaumgarte(0.5f) { }

		void Clear() { mJoints.clear(); }

		void AddJoint(RigidJoint& joint) { mJoints.push_back(joint); }
        void AddLockedJoint(LockedJoint& joint) { mLockedJoints.push_back(joint); }
        void AddHingeJoint(HingeJoint& joint) { mHingeJoints.push_back(joint); }

		size_t GetNumJoints() const { return mJoints.size(); }
		const RigidJoint& GetJoint(size_t i) const { return mJoints[i]; }

        float CalculateStats(Bodies& bodies);

        void SolveAcc(float h, Bodies& bodies, PhaseType phase) override { }

		virtual void ApplyForces(float h, Bodies& bodies, PhaseType phase) { }

        void Prepare(Bodies& bodies, PhaseType phase) override;

        void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr) override;

    private:
        void SolveJointsPBD(float h, Bodies& bodies, IIntegrator* integrator);

        void SolveJointsVTS(float h, Bodies& bodies);

	private:
		std::vector<RigidJoint> mJoints;
        std::vector<LockedJoint> mLockedJoints;
        std::vector<HingeJoint> mHingeJoints;
        float mBaumgarte;
	};
}

#endif // RIGID_JOINTS_H