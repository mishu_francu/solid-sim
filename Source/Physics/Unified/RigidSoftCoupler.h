#include "CollisionUtils.h"

namespace Physics
{
	// Coupler class to allow contact between rigid bodies and FEM soft bodies
	class RigidSoftCoupler : public ICoupler
	{
	public:
		void DetectCollision(Group* group1, Group* group2, Collidables& collidables, Bodies& bodies, Constraints& constraints) override 
		{
			//mStaticContacts.mContacts.clear();			
			//mRigidContacts.Clear();
			if (typeid(*group1) == typeid(SoftFEM) && typeid(*group2) == typeid(Rigids))
			{
				Detect((SoftFEM*)group1, (Rigids*)group2, collidables, bodies);
			}
			if (typeid(*group2) == typeid(SoftFEM) && typeid(*group1) == typeid(Rigids))
			{
				Detect((SoftFEM*)group2, (Rigids*)group1, collidables, bodies);
			}			
			//constraints.AddList(&mRigidContacts);
		}

		void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints) override
		{
			//mTriangleContacts.mTris.clear();
			//auto& contacts = collMgr.GetContacts();
			//for (size_t i = 0; i < contacts.size(); i++)
			//{
			//	if (contacts[i].group1 == nullptr || contacts[i].group2 == nullptr)
			//		continue;
			//	if (typeid(*contacts[i].group1) == typeid(SoftFEM) && typeid(*contacts[i].group2) == typeid(Rigids))
			//	{
			//		Add((SoftFEM*)contacts[i].group1, (Rigids*)contacts[i].group2, collMgr, bodies, contacts[i], false);
			//	}
			//	if (typeid(*contacts[i].group2) == typeid(SoftFEM) && typeid(*contacts[i].group1) == typeid(Rigids))
			//	{
			//		Add((SoftFEM*)contacts[i].group2, (Rigids*)contacts[i].group1, collMgr, bodies, contacts[i], true);
			//	}
			//}
			//constraints.AddList(&mTriangleContacts);
			mRigidContacts.Clear();
			constraints.AddList(&mRigidContacts);
		}

		void AddContact(int i, Vector3 p, Vector3 n)
		{
			int idx = mSoft->mMapInv[i] + mSoft->GetStart();

			//StaticContact contact;
			//contact.idx = idx;
			//contact.point = p;
			//contact.normal = n;
			//contact.vel.SetZero();
			//contact.len = 0;//mThickness;
			//mStaticContacts.mContacts.push_back(contact);

			RigidContact contact;
			contact.i = idx;
			contact.j = mRigidIdx;
			contact.n = n;//-mSoft->GetCollisonMesh()->normals[i];
			contact.a1.SetZero();
			contact.a2 = p;
			mRigidContacts.AddContact(contact);
		}

		void SetFriction(float val)
		{
			mRigidContacts.SetFriction(val);
		}

	private:
		void Add(SoftFEM* soft, Rigids* rigids, const CollisionManager& collMgr, Bodies& bodies, const CollisionManager::Contact& contact, bool flip)
		{
			int tri = flip ? contact.tri2 : contact.tri1;
			Geometry::Mesh* mesh = soft->GetCollisonMesh();
			int i1 = soft->mMapInv[mesh->indices[tri * 3]] + soft->GetStart();
			int i2 = soft->mMapInv[mesh->indices[tri * 3 + 1]] + soft->GetStart();
			int i3 = soft->mMapInv[mesh->indices[tri * 3 + 2]] + soft->GetStart();
			int i4 = flip ? contact.i1 : contact.i2;
			Vector3 p = flip ? contact.a2 : contact.a1;
			auto coords = Geometry::Barycentric(bodies[i1].pos, bodies[i2].pos, bodies[i3].pos, p);
			Vector3 a = flip ? contact.a1 : contact.a2;

			bodies[i1].pos.Y() = 0;
			bodies[i1].vel.SetZero();
			bodies[i2].pos.Y() = 0;
			bodies[i2].vel.SetZero();
			bodies[i3].pos.Y() = 0;
			bodies[i3].vel.SetZero();

			//TriangleRigidContact c;
			//c.i1 = i1;
			//c.i2 = i2;
			//c.i3 = i3;
			//c.i4 = i4;
			//c.w1 = coords.u;
			//c.w2 = coords.v;
			//c.w3 = coords.w;
			//c.normal = -contact.n;
			//auto shape = collMgr.GetCollidable(bodies[i4].collIdx);
			//Vector3 a2 = !(bodies[i4].R) * (a - shape->mTolerance * contact.n - bodies[i4].pos);
			//c.a = a2;
			//c.len = 0; // TODO: thickness
			//mTriangleContacts.mTris.push_back(c);
		}

		void Detect(SoftFEM* soft, Rigids* rigids, Collidables& collidables, Bodies& bodies)
		{
			mSoft = soft;
			mBodies = &bodies;

			Matrix3 eye;
			Vector3 zero;
			for (size_t i = 0; i < rigids->GetCount(); i++)
			{
				int ii = i + rigids->GetStart();
				mRigidIdx = ii;
				if (bodies[ii].collIdx < 0)
					continue;
				const Collidable* shape = collidables[bodies[ii].collIdx].get();
				if (shape->mType == CT_BOX)// || shape->mType == CT_MESH)
				{
					//Mesh* mesh = nullptr;
					//if (shape->mType == CT_BOX)
					//	mesh = &((Box*)shape)->mesh;
					//VerticesVsTriangles(soft->GetCollisonMesh(), mesh, eye, bodies[ii].R, zero, bodies[ii].pos, 1.f, *this);

					Box* box = (Box*)shape;
					VerticesVsBox(soft->GetCollisonMesh()->vertices, bodies[ii].R, bodies[ii].pos, box->D, 0.1f, *this);
				}
			}
		}

	public:
		SoftFEM* mSoft;
		Bodies* mBodies;
		int mRigidIdx;
		TriangleRigidContacts mTriangleContacts;
		StaticContacts mStaticContacts;
		RigidContacts mRigidContacts;
	};
}