#include "UnifiedSolver.h"
#include <Engine/Profiler.h>

namespace Physics
{
	inline void UpdateRotationalPart(Body& body)
	{
		// update rotation (for collisions) and inertia first
		body.R = qToMatrix(body.q);
		body.Iinv = body.R * body.Icinv * !body.R; // TODO: use the quaternion directly
		Vector3 r1 = 0.5f * (body.R(0) * body.q).v;
		Vector3 r2 = 0.5f * (body.R(1) * body.q).v;
		Vector3 r3 = 0.5f * (body.R(2) * body.q).v;
		Matrix3 L(r1, r2, r3);
		body.IinvPos = L * body.Icinv * !L;
	}

	void GaussSeidelSolver::Step(float h, Bodies& bodies, Constraints& constraints, CollisionManager& collision)
	{
		PROFILE_SCOPE("GS");
		// integrate velocities
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i].invMass != 0)
				bodies[i].vel += h * Constants::unit * gravity;
			if (bodies[i].rigid)
			{
				UpdateRotationalPart(bodies[i]);
				//rigids[i].w += h * torque; // no external torques for now
			}
			bodies[i].pos0 = bodies[i].pos;
			bodies[i].q0 = bodies[i].q;
		}


		if (mNumIterationsVel != 0)
		{
			// collision detection (with initial positions)
			constraints.Clear();
			collision.Detect(bodies, constraints, true);

			// solve constraints iteratively and sequentially (before position integration)
			constraints.PrepareAll(bodies, PT_VELOCITY);
			for (size_t iter = 0; iter < mNumIterationsVel; iter++)
			{
				constraints.SolveAll(h, bodies, PT_VELOCITY);
			}
		}

		// integrate positions
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i].invMass != 0)
				bodies[i].pos = bodies[i].pos0 + h * bodies[i].vel;
			if (bodies[i].rigid && bodies[i].invMass != 0)
			{
				bodies[i].q += 0.5f * h * (bodies[i].q * bodies[i].w); // TODO: exponential map
				bodies[i].q = qNormalize(bodies[i].q); // TODO: normalize only at the end
				// update rotation and inertia
				UpdateRotationalPart(bodies[i]);			
			}
		}

		if (mNumIterationsPos != 0)
		{
			// collision detection (with integrated positions)
			constraints.Clear();
			collision.Detect(bodies, constraints, false);

			// solve constraints iteratively and sequentially (after position integration)
			constraints.PrepareAll(bodies, PT_POSITION);
			for (size_t iter = 0; iter < mNumIterationsPos; iter++)
			{
				constraints.SolveAll(h, bodies, PT_POSITION);
			}
		}

		// stats!
		constraints.CalculateStats(bodies);

		if (mNumIterationsPost != 0)
		{
			// collision detection (with post-PBD positions)
			//constraints.Clear();
			//collision.Detect(bodies, constraints, false);

			// solve constraints iteratively and sequentially (velocity post-process)
			constraints.PrepareAll(bodies, PT_POSTPROCESS);
			for (size_t iter = 0; iter < mNumIterationsPos; iter++)
			{
				constraints.SolveAll(h, bodies, PT_POSTPROCESS);
			}
		}

		// integrate positions for rendering
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i].invMass != 0)
			{
				bodies[i].pos0 = bodies[i].pos0 + h * bodies[i].vel;
			}
			//if (bodies[i].rigid)
			//{
			//	bodies[i].q += 0.5f * h * (bodies[i].w * bodies[i].q); // TODO: exponential map
			//	bodies[i].q = qNormalize(bodies[i].q); // TODO: normalize only at the end
			//	// update rotation and inertia
			//	UpdateRotationalPart(bodies[i]);			
			//}
		}
	}

	void JacobiSolver::Step(float h, Bodies& bodies, Constraints& constraints, CollisionManager& collision)
	{
		PROFILE_SCOPE("Jacobi");
		// integrate velocities
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i].invMass != 0)
				bodies[i].vel += h * Constants::unit * gravity;
			if (bodies[i].rigid)
			{
				// update rotation (for collisions) and inertia first
				bodies[i].R = qToMatrix(bodies[i].q);
				bodies[i].Iinv = bodies[i].R * bodies[i].Icinv * !bodies[i].R; // TODO: use the quaternion directly
				//rigids[i].w += h * torque; // no external torques for now
			}
			bodies[i].pos0 = bodies[i].pos;
			bodies[i].q0 = bodies[i].q;
		}

		if (mNumIterationsVel != 0)
		{
			// collision detection (with initial positions)
			constraints.Clear();
			collision.Detect(bodies, constraints, true);

			// solve constraints iteratively and sequentially (before position integration)
			constraints.PrepareAll(bodies, PT_VELOCITY);
			for (size_t iter = 0; iter < mNumIterationsVel; iter++)
			{
				constraints.SolveAllAcc(h, bodies, PT_VELOCITY);
				constraints.ApplyForces(h, bodies, PT_VELOCITY);
			}
		}

		// integrate positions
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i].invMass != 0)
				bodies[i].pos = bodies[i].pos0 + h * bodies[i].vel;
			if (bodies[i].rigid)
			{
				bodies[i].q += 0.5f * h * (bodies[i].w * bodies[i].q); // TODO: exponential map
				bodies[i].q = qNormalize(bodies[i].q); // TODO: normalize only at the end
				// update rotation and inertia
				bodies[i].R = qToMatrix(bodies[i].q);
				bodies[i].Iinv = bodies[i].R * bodies[i].Icinv * !bodies[i].R; // TODO: use the quaternion directly
			}
		}

		if (mNumIterationsPos != 0)
		{
			// collision detection (with integrated positions)
			constraints.Clear();
			collision.Detect(bodies, constraints, false);

			// solve constraints iteratively and sequentially (after position integration)
			constraints.PrepareAll(bodies, PT_POSITION);
			for (size_t iter = 0; iter < mNumIterationsPos; iter++)
			{
				//Printf("%d\n", iter);
				constraints.SolveAllAcc(h, bodies, PT_POSITION);
				constraints.ApplyForces(h, bodies, PT_POSITION);
			}
		}

		// stats!
		constraints.CalculateStats(bodies);
	}

	void NewmarkIntegrator::IntegrateVelocities(float h, Bodies& bodies)
	{
		const float invH = 1.0f / h;
		for (size_t i = 0; i < bodies.size(); i++)
		{
			bodies[i].vel0 = bodies[i].vel;
			bodies[i].w0 = bodies[i].w0;
			if (bodies[i].invMass != 0)
				bodies[i].vel += h * Constants::unit * gravity + (1.0f - mAlpha) * invH * bodies[i].force;
			if (bodies[i].rigid)
			{
				UpdateRotationalPart(bodies[i]);
				bodies[i].w += (1.0f - mAlpha) * invH * bodies[i].Iinv * bodies[i].torque; // no external torques for now
			}
			bodies[i].pos0 = bodies[i].pos;
			bodies[i].q0 = bodies[i].q;
		}
	}

	void NewmarkIntegrator::IntegratePositions(float h, Bodies& bodies)
	{
		for (size_t i = 0; i < bodies.size(); i++)
		{
			if (bodies[i].invMass != 0)
				bodies[i].pos = bodies[i].pos0 + h * bodies[i].vel0 + 0.5f * h * h * Constants::unit * gravity + 0.5f * (1.0f - 2 * mBeta) * bodies[i].invMass * bodies[i].force;
			if (bodies[i].rigid)
			{
				Vector3 w = h * bodies[i].w + 0.5f * (1.0f - 2 * mBeta) * bodies[i].Iinv * bodies[i].torque;
				bodies[i].q += 0.5f * (w * bodies[i].q);
				bodies[i].q = qNormalize(bodies[i].q);
				// update rotation and inertia
				UpdateRotationalPart(bodies[i]);
			}
			// reset generalized force
			bodies[i].force.SetZero();
			bodies[i].torque.SetZero();
		}
	}

	void NewmarkIntegrator::ApplyImpulse(float h, Body& b1, Body& b2, const Vector3& imp, const Vector3& dw1, const Vector3& dw2)
	{
		const float invH = 1.f / h;

		// store generalized force (as displacement)
		b1.force += imp;
		b1.torque += dw1;
		b2.force -= imp;
		b2.torque += dw2;

		// rigid 1 velocities
		b1.vel += mAlpha * invH * b1.invMass * imp;
		b1.w += mAlpha * invH * b1.Iinv * dw1;

		// rigid 1 positions
		b1.pos += mBeta * b1.invMass * imp;
		b1.q += mBeta * 0.5f * ((b1.Iinv * dw1) * b1.q);
		b1.q = qNormalize(b1.q);

		b1.R = qToMatrix(b1.q);
		b1.Iinv = b1.R * b1.Icinv * !b1.R;

		// rigid 2 velocities
		b2.vel -= mAlpha * invH * b2.invMass * imp;
		b2.w += mAlpha * invH * b2.Iinv * dw2;

		// rigid 2 positions
		b2.pos -= mBeta * b2.invMass * imp;
		b2.q += mBeta * 0.5f * ((b2.Iinv * dw2) * b2.q);
		b2.q = qNormalize(b2.q);

		b2.R = qToMatrix(b2.q);
		b2.Iinv = b2.R * b2.Icinv * !b2.R;
	}
	
	void NewmarkGSSolver::Step(float h, Bodies& bodies, Constraints& constraints, CollisionManager& collision)
	{
		PROFILE_SCOPE("NewmarkGS");
		mIntegrator.IntegrateVelocities(h, bodies);

		if (/*mNumIterationsVel != */0) // not yet supported
		{
			// collision detection (with initial positions)
			constraints.Clear();
			collision.Detect(bodies, constraints, true);

			// solve constraints iteratively and sequentially (before position integration)
			constraints.PrepareAll(bodies, PT_VELOCITY);
			for (size_t iter = 0; iter < mNumIterationsVel; iter++)
			{
				constraints.SolveAll(h, bodies, PT_VELOCITY);
			}
		}

		mIntegrator.IntegratePositions(h, bodies);

		if (mNumIterationsPos != 0)
		{
			// collision detection (with integrated positions)
			constraints.Clear();
			collision.Detect(bodies, constraints, false);

			// solve constraints iteratively and sequentially (after position integration)
			constraints.PrepareAll(bodies, PT_POSITION);
			for (size_t iter = 0; iter < mNumIterationsPos; iter++)
			{
				constraints.SolveAll(h, bodies, PT_POSITION, &mIntegrator);
			}
		}

		// stats!
		constraints.CalculateStats(bodies);
	}

}