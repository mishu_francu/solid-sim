#ifndef COLLISION_MANAGER_H
#define COLLISION_MANAGER_H

#include <Math/Vector3.h>
#include <Engine/Types.h>
#include "Unified.h"
#include "ICoupler.h"
#include "BulletWrapper.h"

namespace Physics
{
	class CollisionManager
	{
	public:
		struct Contact
		{
			Math::Vector3 a1, a2, n;
			uint32 i1, i2;
			Group* group1;
			Group* group2;
			int tri1, tri2;
			float depth;
		};

	public:
		CollisionManager(Groups& groups) : mGroups(groups)
		{			
		}

		uint32 AddCollidable(std::shared_ptr<Collidable> coll);
		
		void ClearCollidables() 
		{
			mCollidables.clear();
		}

		size_t GetNumCollidables() const { return mCollidables.size(); }
		const Collidable* GetCollidable(size_t i) const { return mCollidables[i].get(); }

		void UpdateShape(uint32 i, const Vector3& pos, const Quaternion& q)
		{
			mCollidables[i]->center = pos;
			mCollidables[i]->rot = q;
		}

		virtual void UpdateMesh(uint32 i) { }
		virtual int GetOriginalTriangle(uint32 i, int tri) const { return tri; }

		const std::vector<Contact>& GetContacts() const { return mContacts; }

		void AddCoupler(std::shared_ptr<ICoupler> coupler) { mCouplers.push_back(coupler); }

		virtual void Detect(Bodies& bodies, Constraints& constraints, bool beforePositions);

	protected:
		Collidables mCollidables;
		Groups& mGroups; // const?
		std::vector<std::shared_ptr<ICoupler>> mCouplers;
		std::vector<Contact> mContacts;
	};

	class CollisionManagerBullet : public CollisionManager
	{
	private:
#ifdef BULLET_COLLISIONS
		struct BulletShape
		{
			std::shared_ptr<btCollisionObject> mBtCollObj;
			std::shared_ptr<btCollisionShape> mBtShape;			
		};
#endif
	public:
		CollisionManagerBullet(Groups& groups) : CollisionManager(groups)
		{
			// FIXME
			double scene_size = 500;
			unsigned int max_objects = 32000;

#ifdef BULLET_COLLISIONS
			// TODO: memory management
			bt_collision_configuration = new btDefaultCollisionConfiguration();			
			bt_dispatcher = new btCollisionDispatcher(bt_collision_configuration);
			btGImpactCollisionAlgorithm::registerAlgorithm(bt_dispatcher);

			btScalar sscene_size = (btScalar) scene_size;
			btVector3 worldAabbMin(-sscene_size, -sscene_size, -sscene_size);
			btVector3 worldAabbMax(sscene_size, sscene_size, sscene_size);
			//This is one type of broadphase, bullet has others that might be faster depending on the application
			//bt_broadphase = new bt32BitAxisSweep3(worldAabbMin, worldAabbMax, max_objects, 0, true);  // true for disabling raycast accelerator
			bt_broadphase = new btDbvtBroadphase();
#endif

			//mBtCollWorld.reset(new btCollisionWorld(bt_dispatcher, bt_broadphase, bt_collision_configuration));
		}

		uint32 AddCollidable(std::shared_ptr<Collidable> coll);

		void Detect(Bodies& bodies, Constraints& constraints, bool beforePositions) override;

		void ClearCollidables() 
		{
#ifdef BULLET_COLLISIONS
			CollisionManager::ClearCollidables();
			mBtCollWorld.reset(new btCollisionWorld(bt_dispatcher, bt_broadphase, bt_collision_configuration));
			mBtShapes.clear();			
#endif
		}

		void UpdateMesh(uint32 i) override;
		//int GetOriginalTriangle(uint32 i, int tri) const override;

	public: // FIXME
#ifdef BULLET_COLLISIONS
		// Bullet stuff
		std::vector<BulletShape> mBtShapes;
		btCollisionConfiguration* bt_collision_configuration;
		btCollisionDispatcher* bt_dispatcher;
		btBroadphaseInterface* bt_broadphase;
		std::unique_ptr<btCollisionWorld> mBtCollWorld;
#endif
	};
}

#endif // COLLISION_MANAGER_H