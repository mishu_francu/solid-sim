
#include <Physics/Unified/Unified.h>
#include <Physics/Unified/ParticleContacts.h>
#include <Physics/Unified/TriangleContacts.h>
#include <Physics/Unified/SoftFEM.h>
#include <Physics/Unified/Granular.h>

namespace Physics
{
	class GranularSoftCoupler : public ICoupler
	{
	public:
		void DetectCollision(Group* group1, Group* group2, Bodies& bodies, Constraints& constraints) override
		{
			if (typeid(*group1) == typeid(SoftFEM) && typeid(*group2) == typeid(Granular))
			{
				DetectCollision((SoftFEM*)group1, (Granular*)group2, bodies, constraints);
			}
			else if (typeid(*group2) == typeid(SoftFEM) && typeid(*group1) == typeid(Granular))
			{
				DetectCollision((SoftFEM*)group2, (Granular*)group1, bodies, constraints);
			}
		}

		void DetectCollision(Cloth* cloth, Granular* granular, Bodies& bodies, Constraints& constraints)
		{
			// brute force point-triangle collisions
			// TODO: CCD, BVH
			mTriangleContacts.mTris.clear();
			float radTol = granular->GetRadius() + cloth->GetThickness() + granular->GetTolerance();
			for (size_t j = 0; j < cloth->mTriangles.size(); j++)
			{
				const uint32 i1 = cloth->mTriangles[j].i1 + cloth->mStart;
				const uint32 i2 = cloth->mTriangles[j].i2 + cloth->mStart;
				const uint32 i3 = cloth->mTriangles[j].i3 + cloth->mStart;
				const Vector3& x1 = bodies[i1].pos;
				const Vector3& x2 = bodies[i2].pos;
				const Vector3& x3 = bodies[i3].pos;

				// build AABB
				//Vector3 minV = vmin(vmin(x1, x2), x3) - extrude;
				//Vector3 maxV = vmax(vmax(x1, x2), x3) + extrude;
				
				// point - triangle
				for (size_t i = 0; i < granular->GetCount(); i++)
				{
					int ii = i + granular->GetStart();
					if (ii == i1 || ii == i2 || ii == i3 || bodies[ii].invMass == 0)
						continue;
					const Vector3& x4 = bodies[ii].pos;
					// TODO: AABB test

					Vector3 n, p;
					float d;
					BarycentricCoords coords;
					bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
					if (intersect)
					{
						TriangleContact c;
						c.i1 = i1;
						c.i2 = i2;
						c.i3 = i3;
						c.i4 = ii;
						c.w1 = coords.u;
						c.w2 = coords.v;
						c.w3 = coords.w;
						c.normal = n;
						c.len = granular->GetRadius() + cloth->GetThickness();
						mTriangleContacts.mTris.push_back(c);

						//mTriangles[jj].collided = true;
						//mParticles[ii].collided = true;
					}
				}
			}
			constraints.AddList(&mTriangleContacts);
		}

	private:
		ParticleContacts mParticleContacts;
		TriangleContacts mTriangleContacts;
	};
}