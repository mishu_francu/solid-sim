#ifndef STATIC_CONTACTS
#define STATIC_CONTACTS

#include "IConstraintList.h"
#include "Unified.h"

namespace Physics
{
	// contact info for particle and static object
	struct StaticContact
	{
		Vector3 point, normal;
		Vector3 vel;
		Vector3 impulse; // for Jacobi
		Vector3 t1, t2; // for friction
		size_t idx;
		float len, depth;
		float lambda, lambdaF1, lambdaF2; // impulses
		float dLambda; // for Jacobi
	};

	// class containing several methods for solving static contact constraints 
	class StaticContacts : public IConstraintList
	{
	public:
		enum SolverFlags
		{
			SF_STAGGERED = 1,
			SF_CYLINDER_PROJ = 2,
			SF_PBD_CONTACT_ONLY = 4,
			SF_NO_PBD_CONTACT = 8,
			SF_POST_FRICTION_ONLY = 16,
		};
	public:
		// TODO: selector between solvers
		StaticContacts() : mBeta(0.9f), mStiffness(1), mFriction(0.1f), mDamping(1.f), mFlags(/*SF_PBD_CONTACT_ONLY | SF_POST_FRICTION_ONLY*/0) { }

		void SetFriction(float val) { mFriction = val; }
		void SetBaumgarte(float val) { mBeta = val; }
		void SetFlags(int val) { mFlags = val; }

		int AddContact(size_t idx, const Vector3& p, const Vector3& n, float len)
		{
			StaticContact contact;
			contact.idx = idx;
			contact.point = p;
			contact.normal = n;
			contact.vel.SetZero();
			contact.len = len;
			mContacts.push_back(contact);
			return (int)(mContacts.size() - 1);
		}

		void Clear() { mContacts.clear(); }

		// called before the iterations begin - mostly data initialization
		void Prepare(Bodies& bodies, PhaseType phase) override;

		// main solver method - chooses between existing methods
		void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr) override;
	private:

		// velocity time stepping - no friction
		void SolveContactVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				Vector3 v12 = p1.vel; v12.Flip();

				float vnrel = contact.normal.Dot(v12);
				float dLambda = (vnrel + mBeta * contact.depth / h) / p1.invMass; // TODO: optimize (1/h and remove mass)

				float lambda0 = contact.lambda;
				contact.lambda = lambda0 + dLambda; // TODO: use clamp
				if (contact.lambda < 0.f)
					contact.lambda = 0.f;
				dLambda = contact.lambda - lambda0;

				Vector3 impulse = dLambda * contact.normal;
				p1.vel += p1.invMass * impulse;
			}
		}

		// a form of nonlinear velocity time stepping without friction
		// I am not actually sure about the Lagrange multiplier formula; TODO: double check!
		void SolveContactNonlinearVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				Vector3 v12 = p1.vel; v12.Flip();

				float vnrel = contact.normal.Dot(v12);
				contact.depth = contact.len - contact.normal.Dot(p1.pos - contact.point);
				float dLambda = (vnrel + mBeta * contact.depth / h) / p1.invMass;

				float lambda0 = contact.lambda;
				contact.lambda = lambda0 + dLambda;
				if (contact.lambda < 0.f)
					contact.lambda = 0.f;
				dLambda = contact.lambda - lambda0;

				Vector3 impulse = dLambda * contact.normal;
				p1.vel += p1.invMass * impulse;
				p1.pos += h * p1.invMass * impulse;
			}
		}

		// velocity time stepping - contact and friction together
		void SolveContactAndFrictionVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				Vector3 v12 = p1.vel; v12.Flip();

				float vnrel = contact.normal.Dot(v12);
				float dLambda = (vnrel + mBeta * contact.depth / h); // TODO: optimize (1/h and remove mass)

				float lambda0 = contact.lambda;
				contact.lambda = lambda0 + dLambda; // TODO: use clamp
				if (contact.lambda < 0.f)
					contact.lambda = 0.f;
				dLambda = contact.lambda - lambda0;

				Vector3 impulse = dLambda * contact.normal;
					
				// CCP friction (kind of, no relaxation)
				float limit = mFriction * contact.lambda;
				Vector3 vt = v12 - vnrel * contact.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel;
					float lambda0 = contact.lambdaF1;
					contact.lambdaF1 = lambda0 + dLambda;
					if (contact.lambdaF1 >= limit)
						contact.lambdaF1 = limit;
					dLambda = contact.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					impulse += dLambda * vt;
				}

				p1.vel += impulse;
			}
		}

		// velocity time stepping - contact and friction together with proper cone projection; TODO: nicer name
		void SolveContactAndFrictionVTS1(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				Vector3 v12 = p1.vel; v12.Flip();

				float vnrel = contact.normal.Dot(v12);
				float dLambda = (vnrel + mBeta * contact.depth / h);

				float lambda0 = contact.lambda;
				contact.lambda = lambda0 + dLambda;
				if (contact.lambda < 0.f)
					contact.lambda = 0.f;
				dLambda = contact.lambda - lambda0;

				Vector3 impulse(0);
					
				float limit = mFriction * contact.lambda;
				Vector3 vt = v12 - vnrel * contact.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f) // TODO: replace magic number
				{
					float dLambdaF = vtrel;
					float lambdaF0 = contact.lambdaF1;
					contact.lambdaF1 = lambdaF0 + dLambdaF;
					if (contact.lambdaF1 >= limit)
					{
						contact.lambda = (contact.lambda + mFriction * contact.lambdaF1) / (mFriction * mFriction + 1);
						dLambda = contact.lambda - lambda0;
						contact.lambdaF1 = mFriction * contact.lambda;
					}
					dLambdaF = contact.lambdaF1 - lambdaF0;

					vt.Scale(1.f / vtrel); // normalize
					impulse += dLambdaF * vt;
				}

				impulse += dLambda * contact.normal;
				p1.vel += impulse;
			}
		}

		// velocity time stepping - friction only
		void SolveFrictionVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				Vector3 v12 = p1.vel; v12.Flip();

				float vnrel = contact.normal.Dot(v12);					
				float limit = mFriction * contact.lambda;
				Vector3 vt = v12 - vnrel * contact.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel;
					float lambda0 = contact.lambdaF1;
					contact.lambdaF1 = lambda0 + dLambda;
					if (contact.lambdaF1 >= limit)
						contact.lambdaF1 = limit;
					dLambda = contact.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 impulse = dLambda * vt;
					p1.vel += impulse;
				}
			}
		}

		// position based dynamics with stiffness
		void SolveContactPBD(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				if (p1.invMass == 0)
					continue;
					
				float depth = contact.normal.Dot(p1.pos - contact.point) - contact.len;
				if (depth > 0)
					continue;
				float dLambda = mStiffness * depth;
				contact.lambda += -invH * dLambda;

				Vector3 disp = dLambda * contact.normal;
				p1.pos -= disp;
				p1.vel -= invH * disp;
			}
		}

		// position based dynamics with damping; TODO: I don't think the stiffness term is correct here
		void SolveContactDampedPBD(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				if (p1.invMass == 0)
					continue;
					
				float depth = contact.normal.Dot(p1.pos - contact.point) - contact.len;
				if (depth > 0)
					continue;
				Vector3 v12 = p1.vel; v12.Flip();
				float vnrel = contact.normal.Dot(v12);
				float dLambda = (mStiffness * depth - h * mDamping * vnrel) / (1 + mDamping);
				contact.lambda += -invH * dLambda;

				Vector3 disp = dLambda * contact.normal;
				p1.pos -= disp;
				p1.vel -= invH * disp;
			}
		}

		void SolveFrictionConstraint(float h, Bodies& bodies, StaticContact& contact)
		{
			if (mFriction == 0)
				return;

			Body& p1 = bodies[contact.idx];
			Vector3 v12 = p1.vel; v12.Flip();
			float vnrel = contact.normal.Dot(v12);

			float limit = mFriction * contact.lambda;
			if (mFlags & SF_CYLINDER_PROJ)
			{
				Vector3 vt = v12 - vnrel * contact.normal;
				float vtrel = vt.Length();
				if (vtrel > Constants::slipEps)
				{
					float dLambda = vtrel;
					float lambda0 = contact.lambdaF1;
					contact.lambdaF1 = lambda0 + dLambda;
					if (contact.lambdaF1 >= limit)
						contact.lambdaF1 = limit;
					dLambda = contact.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 impulse = dLambda * vt;
					p1.vel += impulse;
					p1.pos += h * impulse;
				}
			}
			else
			{
				float vtrel = v12.Dot(contact.t1);
				float dLambdaF1 = vtrel;
				float lambdaF0 = contact.lambdaF1;
				contact.lambdaF1 = lambdaF0 + dLambdaF1;
				contact.lambdaF1 = clamp(contact.lambdaF1, -limit, limit);
				dLambdaF1 = contact.lambdaF1 - lambdaF0;

				vtrel = v12.Dot(contact.t2);
				float dLambdaF2 = vtrel;
				lambdaF0 = contact.lambdaF2;
				contact.lambdaF2 = lambdaF0 + dLambdaF2;
				contact.lambdaF2 = clamp(contact.lambdaF2, -limit, limit);
				dLambdaF2 = contact.lambdaF2 - lambdaF0;

				Vector3 impulse = dLambdaF1 * contact.t1 + dLambdaF2 * contact.t2;
				p1.vel += impulse;
				p1.pos += h * impulse;
			}
		}

		// position based dynamics - friction only
		// basically the same thing as for VTS just that we update the velocity too
		void SolveFrictionPBD(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				SolveFrictionConstraint(h, bodies, mContacts[i]);
			}
		}

		// position based dynamics - contact and friction together
		void SolveContactAndFrictionPBD(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				if (p1.invMass == 0)
					continue;
					
				float depth = contact.normal.Dot(p1.pos - contact.point) - contact.len;
				if (depth > 0)
					continue;
				float dLambda = mStiffness * depth;
				contact.lambda += -invH * dLambda;

				Vector3 disp = dLambda * contact.normal;
				p1.pos -= disp;
				p1.vel -= invH * disp;

				SolveFrictionConstraint(h, bodies, mContacts[i]);
			}
		}

		// Jacobi part

		void SolveAcc(float h, Bodies& bodies, PhaseType phase) override
		{
			float theta0 = mTheta;
			mTheta = 0.5f * (-mTheta * mTheta + mTheta * sqrtf(mTheta * mTheta +4));
			float beta = theta0 * (1 - theta0) / (theta0 * theta0 + mTheta);
			if (phase == PT_VELOCITY)
				SolveContactVTSAcc(h, bodies);
			else if (phase == PT_POSITION)
				SolveContactAndFrictionPBDAcc(h, bodies, beta);
		}

		void SolveContactVTSAcc(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				Vector3 v12 = p1.vel; v12.Flip();

				float vnrel = contact.normal.Dot(v12);
				float dLambda = 0.4f * (vnrel + mBeta * contact.depth / h) / p1.invMass; // TODO: optimize (1/h and remove mass)

				float lambda0 = contact.lambda;
				contact.lambda = lambda0 + dLambda;
				if (contact.lambda < 0.f)
					contact.lambda = 0.f;
				contact.dLambda = contact.lambda - lambda0;
			}
		}

		void ApplyForces(float h, Bodies& bodies, PhaseType phase) override
		{
			if (phase == PT_VELOCITY)
			{
				for (size_t i = 0; i < mContacts.size(); i++)
				{
					StaticContact& contact = mContacts[i];
					Body& p1 = bodies[contact.idx];
					Vector3 impulse = contact.dLambda * contact.normal;
					p1.vel += p1.invMass * impulse;
				}
			}
			else if (phase == PT_POSITION)
				SolveContactAndFrictionPBDApply(h, bodies);
		}

		void SolveContactAndFrictionPBDAcc(float h, Bodies& bodies, float beta)
		{
			float invH = 1.f / h;
			#pragma omp parallel for
			for (int i = 0; i < (int)mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				if (p1.invMass == 0)
					continue;

				float depth = contact.normal.Dot(p1.pos - contact.point) - contact.len;
				if (depth > 0)
				{
					contact.impulse.SetZero();
					continue;
				}
				float dLambda = mStiffness * depth;
				contact.lambda += -invH * dLambda;

				Vector3 oldImp = contact.impulse;
				contact.impulse = -invH * dLambda * contact.normal;
				//p1.pos -= disp;
				//p1.vel -= invH * disp;

				// friction
				Vector3 v12 = p1.vel; v12.Flip();

				float vnrel = contact.normal.Dot(v12);

				float limit = mFriction * contact.lambda;
				Vector3 vt = v12 - vnrel * contact.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel;
					float lambda0 = contact.lambdaF1;
					contact.lambdaF1 = lambda0 + dLambda;
					if (contact.lambdaF1 >= limit)
						contact.lambdaF1 = limit;
					dLambda = contact.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					contact.impulse += dLambda * vt;
					//p1.vel += impulse;
					//p1.pos += h * impulse;
				}

				contact.impulse += beta * oldImp;
			}
		}

		void SolveContactAndFrictionPBDApply(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			#pragma omp parallel for
			for (int i = 0; i < (int)mContacts.size(); i++)
			{
				StaticContact& contact = mContacts[i];
				Body& p1 = bodies[contact.idx];
				if (p1.invMass == 0)
					continue;

				p1.vel += contact.impulse;
				p1.pos += h * contact.impulse;
			}
		}

	private:
		std::vector<StaticContact> mContacts;
		float mBeta, mStiffness, mFriction, mDamping;
		float mTheta; // for Nesterov
		int mFlags;
	};
}

#endif // STATIC_CONTACTS