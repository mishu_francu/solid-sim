#ifndef MESH_COLLISIONS_H
#define MESH_COLLISIONS_H

#include "IConstraintList.h"

namespace Geometry
{
	struct BarycentricCoords;
	struct Mesh;
	struct Aabb3;
	struct AabbTree;
}

namespace Physics
{	
	struct IContactAdder
	{
		virtual void AddContact(int i1, int i2, int i3, int i4, const Geometry::BarycentricCoords& coords,
			const Vector3& normal, const Vector3& anchor, float len) { }
		virtual void AddContact(int i, int j, Vector3 a1, Vector3 a2, Vector3 n, float depth) { }
		virtual ~IContactAdder() { }
	};

	class MeshCollisions
	{
	public:
		static void CollideMeshesCCD(Bodies& bodies, int iMesh1, int iMesh2, const Geometry::Mesh* mesh1, const Geometry::Mesh* mesh2,
			float radTol, IContactAdder* adder);
		static void MeshVsMeshCCD(Bodies& bodies, int i, int j, const Geometry::Mesh* mesh1, const Geometry::Mesh* mesh2,
			const Geometry::AabbTree* mesh2Tree, float radTol, IContactAdder* adder, bool flip = false);
		static void MeshVsMesh(Bodies& bodies, int i, int j, const Geometry::Mesh* mesh1, const Geometry::Mesh* mesh2, float radTol,
			const Matrix3& R1, const Vector3& pos1, const Vector3& pos1Prev, const Matrix3& R2, const Vector3& pos2, const Vector3& pos2Prev,
			bool flip, IContactAdder* adder);

	private:
		static void TriangleVsMesh(const std::vector<Vector3>& vertices, 
			Bodies& bodies, const Geometry::Aabb3& triBox, float radTol,
			int i, int j,
			Vector3 x1, Vector3 x2, Vector3 x3,
			Vector3 v1, Vector3 v2, Vector3 v3,
			Vector3 n1, Vector3 n2, Vector3 n3,
			IContactAdder* adder);

		static void TriangleVsMeshTree(const Geometry::Mesh* mesh, const Geometry::AabbTree* tree,
			Bodies& bodies, const Geometry::Aabb3& triBox, float radTol,
			int iMesh, int iTri,
			Vector3 x1, Vector3 x2, Vector3 x3,
			Vector3 v1, Vector3 v2, Vector3 v3,
			Vector3 n1, Vector3 n2, Vector3 n3);

		static void TrianglesVsVertices(Bodies& bodies, int i, int j, const Geometry::Mesh* mesh1, const Geometry::Mesh* mesh2, float radTol, bool flip);
	};

	class MeshCollisionsElTopo
	{
	public:
		static void CollideMeshesCCD(Bodies& bodies, int iMesh1, int iMesh2, const Geometry::Mesh* mesh1, const Geometry::Mesh* mesh2,
			float radTol, IContactAdder* adder);
	};

	class MeshCollisionsBullet
	{
	public:
		static void MeshVsMesh(Bodies& bodies, int i, int j, const Geometry::Mesh* mesh1, const Geometry::Mesh* mesh2, float radTol,
			bool flip, IContactAdder* adder);
	};
}

#endif // MESH_COLLISIONS_H