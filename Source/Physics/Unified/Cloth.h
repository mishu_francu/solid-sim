#ifndef UNIF_CLOTH_H
#define UNIF_CLOTH_H

#include <Physics/ClothTypes.h> // TODO: remove
#include <Geometry/Mesh.h>
#include "StaticContacts.h"
#include "CollisionManager.h"

namespace Physics
{
	class LinkConstraints : public IConstraintList
	{
		friend class Cloth;
	public:
		enum SolverFlagsType
		{
			SF_REG = 1,
			SF_PBD_ONLY = 2,
		};
	public:
		LinkConstraints() : mDamp(0), mFlags(SF_PBD_ONLY) { }
		virtual void Prepare(Bodies& bodies, PhaseType phase) 
		{
			mTheta = 1.f;
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				mLinks[i].disp.SetZero();
			}
		}
		
		virtual void SolveAcc(float h, Bodies& bodies, PhaseType phase)
		{
			float theta0 = mTheta;
			mTheta = 0.5f * (-mTheta * mTheta + mTheta * sqrtf(mTheta * mTheta + 4));
			float beta = theta0 * (1 - theta0) / (theta0 * theta0 + mTheta);

			const float omega = 0.3f; // TODO: expose as param
			float invH = 1.f / h;
			#pragma omp parallel for
			for (int i = 0; i < (int)mLinks.size(); i++)
			{
				Link& link = mLinks[i];
				Body& p1 = bodies[link.i1];
				Body& p2 = bodies[link.i2];

				float mu = 1.f / (p1.invMass + p2.invMass);

				Vector3 delta = p1.pos - p2.pos;
				const float len0 = link.len;
				float len = delta.Length();
				delta.Scale(1 / len);
				float err = len - len0;

				float lambda = err * mu;
				//link.dLambda = omega * lambda + beta * link.dLambda;
				//link.normal = delta;
				link.disp = omega * lambda * delta + beta * link.disp;
			}
		}
		
		virtual void ApplyForces(float h, Bodies& bodies, PhaseType phase) override
		{
			float invH = 1.f / h;
			#pragma omp parallel for
			for (int i = 0; i < (int)mLinks.size(); i++)
			{
				Link& link = mLinks[i];
				Body& p1 = bodies[link.i1];
				Body& p2 = bodies[link.i2];
				Vector3 delta = link.disp; //link.dLambda * link.normal;
				p1.pos -= p1.invMass * delta;
				p2.pos += p2.invMass * delta;
				delta *= invH;
				p1.vel -= p1.invMass * delta;
				p2.vel += p2.invMass * delta;
			}
		}

		void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr) override
		{
			if (phase == PT_POSITION)
			{
				if ((mFlags & SF_REG) == 0)
					SolvePBD(h, bodies);
				else
					SolveReg(h, bodies);
			}
			else if (phase == PT_POSTPROCESS && (mFlags & SF_PBD_ONLY) == 0)
			{
				SolveVel(h, bodies);
			}
		}

	private:
		void SolveVel(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				Link& link = mLinks[i];
				Body& p1 = bodies[link.i1];
				Body& p2 = bodies[link.i2];

				float mu = 1.f / (p1.invMass + p2.invMass);

				Vector3 delta = p1.pos - p2.pos;
				//const float len0 = link.len;
				float len = delta.Length();
				delta.Scale(1 / len);
				//float err = len - len0;

				float vrel = delta.Dot(p1.vel - p2.vel);
				Vector3 imp = vrel * mu * delta;

				p1.vel -= imp * p1.invMass;
				p2.vel += imp * p2.invMass;
			}
		}

		void SolvePBD(float h, Bodies& bodies)
		{
			const float omega = 1.f;
			float invH = 1.f / h;
			float area = 1;
			const float k = h * h * area * Constants::unit;
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				Link& link = mLinks[i];
				Body& p1 = bodies[link.i1];
				Body& p2 = bodies[link.i2];

				float mu = 1.f / (p1.invMass + p2.invMass);

				Vector3 delta = p1.pos - p2.pos;
				const float len0 = link.len;
				float len = delta.Length();
				delta.Scale(1 / len);
				float err = len - len0;

				float epsilon = len0 / (k * link.stiffness);
				float s = 1.f / (1.f + mu * epsilon); 
				float lambda = err * mu * s;
				link.lambda += lambda;

				delta.Scale(omega * lambda);
				p1.pos -= delta * p1.invMass;
				p2.pos += delta * p2.invMass;
				delta *= invH;
				p1.vel -= delta * p1.invMass;
				p2.vel += delta * p2.invMass;
			}
		}

		void SolveReg(float h, Bodies& bodies)
		{
			const float omega = 1.f;
			float invH = 1.f / h;
			float area = 1;
			const float k = h * h * area * Constants::unit;
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				Link& link = mLinks[i];
				Body& p1 = bodies[link.i1];
				Body& p2 = bodies[link.i2];

				Vector3 delta = p1.pos - p2.pos;
				const float len0 = link.len;
				float len = delta.Length();
				delta.Scale(1 / len);
				float err = len - len0;

				float vrel = delta.Dot(p1.vel - p2.vel);

				float epsilon = len0 / (k * link.stiffness);
				float diag = (1.f + mDamp) * (p1.invMass + p2.invMass) + epsilon * invH * invH;
				float lambda = (err + h * mDamp * vrel) / diag;
				link.lambda += lambda;

				delta.Scale(omega * lambda);
				p1.pos -= delta * p1.invMass;
				p2.pos += delta * p2.invMass;
				delta *= invH;
				p1.vel -= delta * p1.invMass;
				p2.vel += delta * p2.invMass;
			}
		}

	private:
		std::vector<Link> mLinks;
		float mTheta;
		float mDamp;
		int mFlags;
	};

	class Cloth : public Group
	{
	public:
		friend class ClothGranularCoupler;

		struct Triangle
		{
			uint32 i1, i2, i3;
		};
	public:
		Cloth() : mThickness(0.4f), mTolerance(0.2f) { }

		float GetThickness() const { return mThickness; }
		void SetThickness(float val) { mThickness = val; }
		float GetTolerance() const { return mTolerance; }
		void SetDamping(float val) { mLinkConstraints.mDamp = val; }
		void SetFriction(float val) { mStaticContacts.SetFriction(val); }

		std::vector<Link>& GetLinks() { return mLinkConstraints.mLinks; }

		Geometry::Mesh& GetMesh() { return mMesh; }

		void AddEdge(const Geometry::Mesh::Edge& e) { mMesh.edges.push_back(e); }

		void DetectCollision(Bodies& bodies, Collidables& collidables, Constraints& constraints, bool beforePos) override;

		void AddLink(Bodies& bodies, size_t i1, size_t i2, float stiffness)
		{
			Link link;
			link.i1 = unsigned(i1 + mStart);
			link.i2 = unsigned(i2 + mStart);
			if (bodies[link.i1].invMass == 0 && bodies[link.i2].invMass == 0)
				return;
			if (stiffness == 0)
				return;
			link.stiffness = stiffness;
			link.len = (bodies[link.i1].pos - bodies[link.i2].pos).Length();
			link.disp.SetZero();
			mLinkConstraints.mLinks.push_back(link);
		}

		// this should be shared with Granular
		void WallCollisions(Bodies& bodies, const Geometry::Aabb3& walls)
		{
			const float r = mThickness + mTolerance;
			for (size_t i = mStart; i < mStart + mCount; i++)
			{
				const Body& particle = bodies[i];
				if (particle.pos.Y() - r <= walls.min.Y())
					AddContact(i, Vector3(particle.pos.X(), walls.min.Y(), particle.pos.Z()), Vector3(0, 1, 0));
				//if (particle.pos.Y() + r >= WALL_TOP)
				//	AddContact((ParticleIdx)i, Vector3(particle.pos.X(), WALL_TOP, particle.pos.Z()), Vector3(0, -1, 0));
				if (particle.pos.X() - r <= walls.min.X())
					AddContact(i, Vector3(walls.min.X(), particle.pos.Y(), particle.pos.Z()), Vector3(1, 0, 0));
				if (particle.pos.X() + r >= walls.max.X())
					AddContact(i, Vector3(walls.max.X(), particle.pos.Y(), particle.pos.Z()), Vector3(-1, 0, 0));
				if (particle.pos.Z() - r <= walls.min.Z())
					AddContact(i, Vector3(particle.pos.X(), particle.pos.Y(), walls.min.Z()), Vector3(0, 0, 1));
				if (particle.pos.Z() + r >= walls.max.Z())
					AddContact(i, Vector3(particle.pos.X(), particle.pos.Y(), walls.max.Z()), Vector3(0, 0, -1));
			}
		}

		// this too
		int AddContact(size_t idx, const Vector3& p, const Vector3& n)
		{
			return mStaticContacts.AddContact(idx, p, n, mThickness);
		}			

		void SphereCollisions(Bodies& bodies, const Vector3& sphPos, float sphRad)
		{
			const float radTol = mThickness + mTolerance;
			Vector3 n, p;
			float dSqr;
			for (size_t i = 0; i < mCount; i++)
			{
				if (bodies[i + mStart].invMass != 0 && Geometry::IntersectSphereSphere(sphPos, sphRad, bodies[i + mStart].pos, radTol, n, p, dSqr))
					AddContact(i, p, n);
			}
		}

		void AddTriangle(size_t i1, size_t i2, size_t i3)
		{
			Triangle tri;
			tri.i1 = uint32(i1 + mStart);
			tri.i2 = uint32(i2 + mStart);
			tri.i3 = uint32(i3 + mStart);
			mTriangles.push_back(tri);
		}

		void UpdateCollidables(CollisionManager& collMgr, Bodies& bodies) override
		{
			for (size_t i = 0; i < mCount; i++)
			{
				Body& body = bodies[i + mStart];
				if (body.collIdx < 0)
					continue;
				collMgr.UpdateShape(body.collIdx, body.pos, body.q);
			}
		}

		void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints) override
		{
			constraints.AddList(&mLinkConstraints);
		}

		void UpdateMesh(const Bodies& bodies)
		{
			for (size_t i = 0; i < mMesh.vertices.size(); i++)
			{
				mMesh.vertices[i] = bodies[i + mStart].pos0;
			}
			mMesh.ComputeNormals();
		}

	private:
		float mThickness, mTolerance;
		LinkConstraints mLinkConstraints;
		StaticContacts mStaticContacts;
		Geometry::Mesh mMesh;
	public: // FIXME		
		std::vector<Triangle> mTriangles; // TODO: own triangle struct
	};
}

#endif // UNIF_CLOTH_H