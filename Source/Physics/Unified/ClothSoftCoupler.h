
#include <Physics/Unified/Unified.h>
#include <Physics/Unified/ParticleContacts.h>
#include <Physics/Unified/TriangleContacts.h>
#include <Physics/Unified/Cloth.h>
#include <Physics/Unified/SoftFEM.h>

namespace Physics
{
	class ClothSoftCoupler : public ICoupler
	{
	public:
		void DetectCollision(Group* group1, Group* group2, Collidables& collidables, Bodies& bodies, Constraints& constraints) override
		{
			if (typeid(*group1) == typeid(Cloth) && typeid(*group2) == typeid(SoftFEM))
			{
				DetectCollision((Cloth*)group1, (SoftFEM*)group2, bodies, constraints);
			}
			else if (typeid(*group2) == typeid(Cloth) && typeid(*group1) == typeid(SoftFEM))
			{
				DetectCollision((Cloth*)group2, (SoftFEM*)group1, bodies, constraints);
			}
		}

		static float EvaluateFun(float t, const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4, float thickness)
		{
			Vector3 z1 = x1 + t * v1;
			Vector3 z2 = x2 + t * v2;
			Vector3 z3 = x3 + t * v3;
			Vector3 z4 = x4 + t * v4;

			// evaluate function
			Vector3 n1 = cross(z2 - z1, z3 - z1);
			float vol = n1.Dot(z4 - z1);
			float f = fabs(vol) - thickness * n1.Length();
			return f;
		}

		void DetectCollision(Cloth* cloth, SoftFEM* soft, Bodies& bodies, Constraints& constraints)
		{
			// brute force point-triangle collisions
			// TODO: CCD, BVH
			mTriangleContacts.mTris.clear();
			float radTol = soft->GetThickness() + cloth->GetThickness() + cloth->GetTolerance();
			for (size_t j = 0; j < cloth->mTriangles.size(); j++)
			{
				const uint32 i1 = cloth->mTriangles[j].i1;
				const uint32 i2 = cloth->mTriangles[j].i2;
				const uint32 i3 = cloth->mTriangles[j].i3;
				const Vector3& x1 = bodies[i1].pos0;
				const Vector3& x2 = bodies[i2].pos0;
				const Vector3& x3 = bodies[i3].pos0;
				const Vector3& y1 = bodies[i1].pos;
				const Vector3& y2 = bodies[i2].pos;
				const Vector3& y3 = bodies[i3].pos;
				Vector3 v1 = y1 - x1;
				Vector3 v2 = y2 - x2;
				Vector3 v3 = y3 - x3;

				// build AABB
				//Vector3 minV = vmin(vmin(x1, x2), x3) - extrude;
				//Vector3 maxV = vmax(vmax(x1, x2), x3) + extrude;
				
				// point - triangle
				//for (size_t i = 0; i < soft->GetCount(); i++)
				//{
				//	int ii = i + soft->GetStart();
				//	if (bodies[ii].invMass == 0)
				//		continue;
				//	const Vector3& x4 = bodies[ii].pos;
				//	// TODO: AABB test

				//	Vector3 n, p;
				//	float d;
				//	BarycentricCoords coords;
				//	bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
				//	if (intersect)
				//	{
				//		TriangleContact c;
				//		c.i1 = i1;
				//		c.i2 = i2;
				//		c.i3 = i3;
				//		c.i4 = ii;
				//		c.w1 = coords.u;
				//		c.w2 = coords.v;
				//		c.w3 = coords.w;
				//		c.normal = n;
				//		c.len = soft->GetThickness() + cloth->GetThickness();
				//		mTriangleContacts.mTris.push_back(c);
				//	}
				//}

				//CCD
				for (size_t k = 0; k < soft->GetCount(); k++)
				{
					size_t ii = k + soft->GetStart();
					Vector3 y4 = bodies[ii].pos;
					Vector3 x4 = bodies[ii].pos0;

					Vector3 v4 = y4 - x4;

					//Aabb3 vBox(vmin(x4, y4), vmax(x4, y4));
					//vBox.Extrude(cloth->GetTolerance());
					//if (!AabbOverlap3D(triBox, vBox))
					//	continue;

					Vector3 n, p;
					float d;
					Geometry::BarycentricCoords coords;
					//float radTol = cloth->GetTolerance() + cloth->GetThickness();
					bool intersect = Geometry::IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
					if (intersect)
					{
						TriangleContact c;
						c.i1 = i1;
						c.i2 = i2;
						c.i3 = i3;
						c.i4 = (uint32)ii;
						c.w1 = coords.u;
						c.w2 = coords.v;
						c.w3 = coords.w;
						//c.normal = n;
						c.normal = -cross(x2 - x1, x3 - x1);
						c.normal.Normalize();
						c.len = soft->GetThickness() + cloth->GetThickness();
						#pragma omp critical
						mTriangleContacts.mTris.push_back(c);
						continue;
					}
					float f0 = EvaluateFun(0.f, x1, x2, x3, x4, v1, v2, v3, v4, cloth->GetThickness());
					if (f0 < 0)	continue;
					float f1 = EvaluateFun(1.f, x1, x2, x3, x4, v1, v2, v3, v4, cloth->GetThickness());

					float tx0 = 0;
					float tx1 = 1;
					float fx0 = f0;
					float fx1 = f1;
					float tx = tx0;
					for (int l = 0; l < 20; l++)
					{
						float tmid = 0.5f * (tx0 + tx1);
						float fmid = EvaluateFun(tmid, x1, x2, x3, x4, v1, v2, v3, v4, cloth->GetThickness());
						if (fabs(fmid) < 0.01f)
						{
							tx = tmid;
							break;
						}
						if (f0 * fmid < 0)
						{
							tx1 = tmid;
							f1 = fmid;
							tx = tx0;
						}
						else if (fmid * f1 < 0)
						{
							tx0 = tmid;
							f0 = fmid;
							tx = tmid;
						}
						else
						{
							break;
						}
					}					

					if (tx > 0 && f0 > 0)
					{
						Vector3 z1 = x1 + tx * v1;
						Vector3 z2 = x2 + tx * v2;
						Vector3 z3 = x3 + tx * v3;
						Vector3 z4 = x4 + tx * v4;

						bool intersect = Geometry::IntersectSphereTriangle(z4, radTol, z1, z2, z3, n, p, d, coords);
						if (intersect)
						{
							TriangleContact c;
							c.i1 = i1;
							c.i2 = i2;
							c.i3 = i3;
							c.i4 = (uint32)ii;
							c.w1 = coords.u;
							c.w2 = coords.v;
							c.w3 = coords.w;
							//c.normal = n;
							c.normal = -cross(x2 - x1, x3 - x1);
							c.normal.Normalize();
							c.len = soft->GetThickness() + cloth->GetThickness();
							#pragma omp critical
							mTriangleContacts.mTris.push_back(c);
							continue;
						}
					}
				}

			}
			constraints.AddList(&mTriangleContacts);
		}

	private:
		ParticleContacts mParticleContacts;
		TriangleContacts mTriangleContacts;
	};
}