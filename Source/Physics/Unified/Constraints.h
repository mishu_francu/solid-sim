#ifndef CONSTRAINTS_H
#define CONSTRAINTS_H

#include "IConstraintList.h"

namespace Physics
{
	class Constraints
	{
	public:
		void AddList(IConstraintList* list)
		{
			mLists.push_back(list);
		}

		void Clear() { mLists.clear(); }

		void PrepareAll(Bodies& bodies, PhaseType phase)
		{
			for (size_t i = 0; i < mLists.size(); i++)
			{
				mLists[i]->Prepare(bodies, phase);
			}
		}

		void SolveAll(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr)
		{
			for (size_t i = 0; i < mLists.size(); i++)
			{
				mLists[i]->Solve(h, bodies, phase, integrator);
			}
		}

		void SolveAllAcc(float h, Bodies& bodies, PhaseType phase)
		{
			for (size_t i = 0; i < mLists.size(); i++)
			{
				mLists[i]->SolveAcc(h, bodies, phase);
			}
		}

		void ApplyForces(float h, Bodies& bodies, PhaseType phase)
		{
			for (size_t i = 0; i < mLists.size(); i++)
			{
				mLists[i]->ApplyForces(h, bodies, phase);
			}
		}

		void CalculateStats(Bodies& bodies)
		{
			float normSqr = 0;
			for (size_t i = 0; i < mLists.size(); i++)
			{
				normSqr += mLists[i]->CalculateStats(bodies);
			}
			float norm = sqrtf(normSqr);
			//Printf("%f\n", norm);

			float energy = 0;
			for (size_t i = 0; i < bodies.size(); i++)
			{
				if (bodies[i].invMass != 0)
					energy += (0.5f / bodies[i].invMass) * bodies[i].vel.LengthSquared();
			}
			//Printf("%f,%f\n", norm, energy);
			//Printf("%f\n", energy);
		}

	private:
		std::vector<IConstraintList*> mLists;
	};

} // namespace Physics

#endif // CONSTRAINTS_H