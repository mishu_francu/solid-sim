#ifndef SOFT_COUPLER_H
#define SOFT_COUPLER_H

namespace Physics
{
	// Coupler class to allow contact between two soft bodies
	class SoftCoupler : public ICoupler, IContactAdder
	{
	public:
		void DetectCollision(Group* group1, Group* group2, Collidables& collidables, Bodies& bodies, Constraints& constraints) override 
		{
			//mTriangleContacts.Clear();
			if (typeid(*group1) == typeid(SoftFEM) && typeid(*group2) == typeid(SoftFEM))
			{
				Detect((SoftFEM*)group1, (SoftFEM*)group2, collidables, bodies);
			}
			//constraints.AddList(&mTriangleContacts);
		}

		void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints)
		{
			mTriangleContacts.Clear();
			constraints.AddList(&mTriangleContacts);
		}

		TriangleRigidContacts& GetContacts() { return mTriangleContacts; }

	private:
		void Detect(SoftFEM* soft1, SoftFEM* soft2, Collidables& collidables, Bodies& bodies)
		{
			mThickness = soft1->GetThickness() + soft2->GetThickness();
			Matrix3 eye;
			Vector3 zero;
			MeshCollisions::MeshVsMesh(bodies, (int)soft1->GetStart(), (int)soft2->GetStart(),
				soft1->GetCollisonMesh(), soft2->GetCollisonMesh(), mThickness + soft1->GetTolerance() + soft2->GetTolerance(),
				eye, zero, zero, eye, zero, zero, false, this);
		}

		void AddContact(int i1, int i2, int i3, int i4, const Geometry::BarycentricCoords& coords,
			const Vector3& normal, const Vector3& anchor, float len) override
		{
			TriangleRigidContact c;
			c.i1 = i1;
			c.i2 = i2;
			c.i3 = i3;
			c.i4 = i4;
			c.w1 = coords.u;
			c.w2 = coords.v;
			c.w3 = coords.w;
			c.normal = normal;
			//c.a = anchor;
			c.len = mThickness;
			mTriangleContacts.mTris.push_back(c);
		}

	private:
		TriangleRigidContacts mTriangleContacts;
		float mThickness;
	};
}

#endif // SOFT_COUPLER_H