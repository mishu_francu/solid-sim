#ifndef BULLET_WRAPPER_H
#define BULLET_WRAPPER_H

#ifdef BULLET_COLLISIONS
#include <bullet/btBulletCollisionCommon.h>
#include <bullet/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>

inline btVector3 V2BV(const Vector3& v)
{
	return btVector3(v.X(), v.Y(), v.Z());
}

inline btQuaternion Q2BQ(const Quaternion& q)
{
	return btQuaternion(q.v.X(), q.v.Y(), q.v.Z(), q.s);
}

inline Vector3 BV2V(const btVector3& v)
{
	return Vector3(v.x(), v.y(), v.z());
}
#endif

#endif // BULLET_WRAPPER_H
