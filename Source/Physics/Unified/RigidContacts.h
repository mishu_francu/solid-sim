#ifndef RIGID_CONTACT_H
#define RIGID_CONTACT_H

#include <Math/Vector3.h>
#include "Unified.h"

namespace Physics
{
	struct RigidContact
	{
	public:
		Math::Vector3 a1, a2;
		Math::Vector3 n;
		Math::Vector3 imp, dw1, dw2; // for Jacobi
		Math::Vector3 t1, t2;
		size_t i, j;
		float depth;
		float lambda, lambdaF1, lambdaF2;
	};

	class RigidContacts : public IConstraintList
	{
	public:
		RigidContacts() : mBeta(0.4f), mFriction(0.2f), mStiffness(0.8f), mDamping(0.0f), mEpsilon(0.0f) { }

		void SetFriction(float val) { mFriction = val; }
		void SetBaumgarte(float val) { mBeta = val; }
		void SetDamping(float val) { mDamping = val; }
		void SetRegularization(float val) { mEpsilon = val; }

		void Clear() { mContacts.clear(); }

		void AddContact(const RigidContact& contact) { mContacts.push_back(contact); }

		size_t GetNumContacts() const { return mContacts.size(); }
		const RigidContact& GetContact(size_t i) const { return mContacts[i]; }

		float CalculateStats(Bodies& bodies);
		
		void SolveAcc(float h, Bodies& bodies, PhaseType phase) override;

		virtual void ApplyForces(float h, Bodies& bodies, PhaseType phase)
		{
			if (phase == PT_POSITION)
				SolveContactPBDApply(h, bodies);
		}

		void Prepare(Bodies& bodies, PhaseType phase) override;

		void SolveContactPBDAcc(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				RigidContact& pair = mContacts[i];
				Body* p1 = &bodies[pair.i];
				Body* p2 = &bodies[pair.j];				

				// world contact positions
				Vector3 a1 = p1->R * pair.a1;
				Vector3 a2 = p2->R * pair.a2;

				// normal part of inertia
				Vector3 a1xn = cross(a1, pair.n);
				Vector3 a2xn = cross(a2, pair.n);
				float inertia1 = a1xn.Dot(p1->Iinv * a1xn);
				float inertia2 = a2xn.Dot(p2->Iinv * a2xn);
				float inertia = p1->invMass + p2->invMass + inertia1 + inertia2;

				// compute and clamp lambda
				float dLambda = -pair.n.Dot(p1->pos + a1 - p2->pos - a2) / inertia;
				float lambda0 = pair.lambda;
				pair.lambda = max(0.f, lambda0 + dLambda);
				dLambda = pair.lambda - lambda0;
				
				pair.imp = dLambda * pair.n;
				pair.dw1 = dLambda * a1xn;
				pair.dw2 = -dLambda * a2xn;
			}
		}

		void SolveContactAndFrictionPBDAcc(float h, Bodies& bodies, float beta);

		void SolveContactPBDApply(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			#pragma omp parallel for
			for (int i = 0; i < (int)mContacts.size(); i++)
			{
				RigidContact& pair = mContacts[i];
				Body* p1 = &bodies[pair.i];
				Body* p2 = &bodies[pair.j];				

				// rigid 1
				p1->vel += invH * p1->invMass * pair.imp;
				p1->w += invH * p1->Iinv * pair.dw1;

				p1->pos += p1->invMass * pair.imp;
				p1->q += 0.5f * ((p1->Iinv * pair.dw1) * p1->q);
				p1->q = qNormalize(p1->q);

				p1->R = qToMatrix(p1->q);
				p1->Iinv = p1->R * p1->Icinv * !p1->R;

				// rigid 2
				p2->vel -= invH * p2->invMass * pair.imp;
				p2->w += invH * p2->Iinv * pair.dw2;

				p2->pos -= p2->invMass * pair.imp;
				p2->q += 0.5f * ((p2->Iinv * pair.dw2) * p2->q);
				p2->q = qNormalize(p2->q);

				p2->R = qToMatrix(p2->q);
				p2->Iinv = p2->R * p2->Icinv * !p2->R;
			}
		}

		void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr) override
		{
			if (phase == PhaseType::PT_VELOCITY)
			{
				//SolveContactVTS(h, bodies);
				SolveContactAndFrictionVTS(h, bodies);
			}
			if (phase == PhaseType::PT_POSITION)
			{
				//SolveContactPBD(h, bodies);
				//SolveFrictionPBD(h, bodies);
				SolveContactAndFrictionPBD(h, bodies);
			}
			if (phase == PhaseType::PT_POSTPROCESS)
			{
				SolveFrictionVTS(h, bodies);
			}
		}

		void SolveContactVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				RigidContact& pair = mContacts[i];
				Body* p1 = &bodies[pair.i]; // TODO: ref
				Body* p2 = &bodies[pair.j];

				Vector3 a1 = p1->R * pair.a1;
				Vector3 a2 = p2->R * pair.a2;
				Vector3 v1 = p1->vel + cross(p1->w, a1);
				Vector3 v2 = p2->vel + cross(p2->w, a2);
				Vector3 v12 = v2 - v1;
				float vnrel = pair.n.Dot(v12); // TODO: precompute axn

				Vector3 a1xn = cross(a1, pair.n);
				Vector3 a2xn = cross(a2, pair.n);
				float inertia1 = a1xn.Dot(p1->Iinv * a1xn);
				float inertia2 = a2xn.Dot(p2->Iinv * a2xn);

				float sum = (p1->invMass + p2->invMass + inertia1 + inertia2);
				float dLambda = (vnrel + mBeta * pair.depth / h) / sum;

				float lambda0 = pair.lambda;
				pair.lambda = max(0.f, lambda0 + dLambda);
				dLambda = pair.lambda - lambda0;

				Vector3 disp = dLambda * pair.n;
				p1->vel += p1->invMass * disp;
				p2->vel -= p2->invMass * disp;

				p1->w += p1->Iinv * (dLambda * a1xn);
				p2->w -= p2->Iinv * (dLambda * a2xn);
			}
		}

		void SolveContactAndFrictionVTS(float h, Bodies& bodies);
		
		void SolveFrictionVTS(float h, Bodies& bodies);

		void SolveContactPBD(float h, Bodies& bodies);

		void SolveContactAndFrictionPBD(float h, Bodies& bodies);

		void SolveFrictionPBD(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				RigidContact& pair = mContacts[i];
				Body* p1 = &bodies[pair.i];
				Body* p2 = &bodies[pair.j];

				// TODO: update orientation here?

				// world contact positions
				Vector3 a1 = p1->R * pair.a1;
				Vector3 a2 = p2->R * pair.a2;

				// integrate using lambda
				Vector3 imp;// = dLambda * pair.n;
				Vector3 dw1;// = dLambda * a1xn;
				Vector3 dw2;//= -dLambda * a2xn;

				// friction
				Vector3 v1 = p1->vel + cross(p1->w, a1);
				Vector3 v2 = p2->vel + cross(p2->w, a2);
				Vector3 v12 = v2 - v1;
				float vnrel = pair.n.Dot(v12);
				Vector3 vt = v12 - vnrel * pair.n;
				float vtrel = vt.Length();

				float limit = mFriction * invH * pair.lambda;
				if (vtrel > 0.0001f) // TODO: handle this better
				{
					// compute diagonal term
					vt.Scale(1.f / vtrel); // normalize
					Vector3 a1xt = cross(a1, vt); // TODO: update a1 or run friction first?
					Vector3 a2xt = cross(a2, vt);
					float inertia1 = a1xt.Dot(p1->Iinv * a1xt);
					float inertia2 = a2xt.Dot(p2->Iinv * a2xt);
					float inertia = p1->invMass + p2->invMass + inertia1 + inertia2;

					float dLambda = vtrel / inertia;
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = min(limit, lambda0 + dLambda);
					dLambda = pair.lambdaF1 - lambda0;

					dLambda *= h;
					imp += dLambda * vt;
					dw1 += dLambda * a1xt;
					dw2 += -dLambda * a2xt;
				}

				// rigid 1
				p1->vel += invH * p1->invMass * imp;
				p1->w += invH * p1->Iinv * dw1;

				p1->pos += p1->invMass * imp;
				p1->q += 0.5f * ((p1->Iinv * dw1) * p1->q);
				p1->q = qNormalize(p1->q);

				p1->R = qToMatrix(p1->q);
				p1->Iinv = p1->R * p1->Icinv * !p1->R;

				// rigid 2
				p2->vel -= invH * p2->invMass * imp;
				p2->w += invH * p2->Iinv * dw2;

				p2->pos -= p2->invMass * imp;
				p2->q += 0.5f * ((p2->Iinv * dw2) * p2->q);
				p2->q = qNormalize(p2->q);

				p2->R = qToMatrix(p2->q);
				p2->Iinv = p2->R * p2->Icinv * !p2->R;
			}
		}

	private:
		std::vector<RigidContact> mContacts;
		float mBeta, mFriction, mStiffness, mDamping, mEpsilon;
		float mTheta;
	};

}

#endif // RIGID_CONTACT_H