#ifndef UNIFIED_SOLVER_H
#define UNIFIED_SOLVER_H

#include "Unified.h"
#include "CollisionManager.h"

namespace Physics
{
	class IterativeSolver
	{
	public:
		// this constructor sets how many iteration each projection gets
		IterativeSolver(size_t numIterVel, size_t numIterPos, size_t numIterPost) 
			: mNumIterationsVel(numIterVel)
			, mNumIterationsPos(numIterPos)
			, mNumIterationsPost(numIterPost) { }
		void SetNumIterationsPos(int val) { mNumIterationsPos = val; }
		void SetNumIterationsVel(int val) { mNumIterationsVel = val; }
		void SetNumIterationsPost(int val) { mNumIterationsPost = val; }
	protected:
		size_t mNumIterationsVel, mNumIterationsPos, mNumIterationsPost;
	};

	// a Gauss-Seidel style integrator and solver with 3 projection phases
	class GaussSeidelSolver : public IterativeSolver
	{
	public:
		GaussSeidelSolver(size_t numIterVel, size_t numIterPos, size_t numIterPost) : IterativeSolver(numIterVel, numIterPos, numIterPost) { }
		void Step(float h, Bodies& bodies, Constraints& constraints, CollisionManager& collision);
	};

	class JacobiSolver : public IterativeSolver
	{
	public:
		// this constructor sets how many iteration each projection gets
		JacobiSolver(size_t numIterVel, size_t numIterPos, size_t numIterPost) : IterativeSolver(numIterVel, numIterPos, numIterPost) { }
		void Step(float h, Bodies& bodies, Constraints& constraints, CollisionManager& collision);
	};

	class NewmarkIntegrator : public IIntegrator
	{
	public:
		NewmarkIntegrator() : mAlpha(0.6f), mBeta(0.8f) {}
		void IntegrateVelocities(float h, Bodies& bodies) override;
		void IntegratePositions(float h, Bodies& bodies) override;
		void ApplyImpulse(float h, Body& b1, Body& b2, const Vector3& imp, const Vector3& dw1, const Vector3& dw2) override;
	private:
		float mAlpha, mBeta;
	};

	class NewmarkGSSolver : public IterativeSolver
	{
	public:
		NewmarkGSSolver(size_t numIterVel, size_t numIterPos, size_t numIterPost) 
			: IterativeSolver(numIterVel, numIterPos, numIterPost)
			, mBeta(1.0f) { }
		void Step(float h, Bodies& bodies, Constraints& constraints, CollisionManager& collision);
	private:
		float mBeta;
		NewmarkIntegrator mIntegrator;
	};
}

#endif // !UNIFIED_SOLVER_H