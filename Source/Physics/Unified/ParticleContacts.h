#ifndef PARTICLE_CONTACTS
#define PARTICLE_CONTACTS

namespace Physics
{
	// contact info for 2 particles
	struct ParticleContact
	{
		Math::Vector3 normal;
		Math::Vector3 impulse; // for Jacobi
		size_t i1, i2;
		float len, depth;
		float lambda, lambdaF1;
		float dLambda;
	};

	// class containing several methods for solving contact between 2 particles
	struct ParticleContacts : public IConstraintList
	{
		ParticleContacts() : mBeta(0.1f), mStiffness(1), mFriction(0.1f), mDamping(0.01f), mEpsilon(0.001f) { }

		// called before the iterations begin - mostly data initialization
		virtual void Prepare(Bodies& bodies, PhaseType phase) override 
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				// TODO: don't do this part for PBD
				ParticleContact& pair = mContacts[i];
				//if (phase == PT_VELOCITY)
				{
					const Body& p1 = bodies[pair.i1];
					const Body& p2 = bodies[pair.i2];
					Vector3 n = p1.pos - p2.pos;
					float len = n.Length();
					pair.depth = pair.len - len;
					n.Normalize();
					pair.normal = n;
				}

				//pair.t1 = pair.normal.Perpendicular();
				//pair.t2 = pair.normal.Cross(pair.t1);
				//pair.t2.Normalize();

				pair.lambda = 0.f;
				pair.lambdaF1 = 0.f;
				//pair.lambdaF2 = 0.f;

				pair.dLambda = 0.f;
				//pair.dLambdaF1 = 0.f;
				//pair.dLambdaF2 = 0.f;
				pair.impulse.SetZero();
			}

			mTheta = 1;
		}

		// main solver method - chooses between existing methods
		virtual void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr) override
		{
			if (phase == PT_VELOCITY /*&& mMode != GM_PBD*/)
			{
				SolveContactAndFrictionVTS1(h, bodies);
				//SolveContactVTS(h, bodies);
				//SolveFrictionVTS(h, bodies);
			}
			if (phase == PT_POSITION /*&& mMode != GM_VTS*/)
			{
				//SolveContactAndFrictionPBD(h, bodies);
				//SolveContactPBD(h, bodies);
				SolveContactDampedPBD(h, bodies);
				//SolveContactNonlinearVTS(h, bodies);
				SolveFrictionPBD(h, bodies);
			}
			if (phase == PT_POSTPROCESS)
				SolveFrictionVTS(h, bodies);
		}

		// velocity time stepping - no friction
		void SolveContactVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
				float dLambda = (vnrel + mBeta * pair.depth / h) / (p1.invMass + p2.invMass);

				float lambda0 = pair.lambda;
				pair.lambda = lambda0 + dLambda;
				if (pair.lambda < 0.f)
					pair.lambda = 0.f;
				dLambda = pair.lambda - lambda0;

				Vector3 impulse = dLambda * pair.normal;
				p1.vel += p1.invMass * impulse;
				p2.vel -= p2.invMass * impulse;
			}
		}

		// a form of nonlinear velocity time stepping without friction
		void SolveContactNonlinearVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];

				Vector3 n = p1.pos - p2.pos;
				float len = n.Length();
				pair.depth = pair.len - len;
				//n.Normalize();
				//pair.normal = n;

				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
				float dLambda = (vnrel + mBeta * pair.depth / h) / (p1.invMass + p2.invMass);

				float lambda0 = pair.lambda;
				pair.lambda = lambda0 + dLambda;
				if (pair.lambda < 0.f)
					pair.lambda = 0.f;
				dLambda = pair.lambda - lambda0;

				Vector3 impulse = dLambda * pair.normal;
				p1.vel += p1.invMass * impulse;
				p2.vel -= p2.invMass * impulse;
				p1.pos += h * p1.invMass * impulse;
				p2.pos -= h * p2.invMass * impulse;
			}
		}

		// velocity time stepping - contact and friction together
		void SolveContactAndFrictionVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
				float dLambda = (vnrel + mBeta * pair.depth / h) / (p1.invMass + p2.invMass);

				float lambda0 = pair.lambda;
				pair.lambda = lambda0 + dLambda;
				if (pair.lambda < 0.f)
					pair.lambda = 0.f;
				dLambda = pair.lambda - lambda0;

				Vector3 impulse = dLambda * pair.normal;
		
				// CCP friction (no relaxation)
				float limit = mFriction * pair.lambda;
				Vector3 vt = v12 - vnrel * pair.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel / (p1.invMass + p2.invMass);
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = lambda0 + dLambda;
					if (pair.lambdaF1 >= limit)
						pair.lambdaF1 = limit;
					dLambda = pair.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					impulse += dLambda * vt;
				}

				p1.vel += p1.invMass * impulse;
				p2.vel -= p2.invMass * impulse;
			}
		}

		// velocity time stepping - contact and friction together with proper cone projection
		void SolveContactAndFrictionVTS1(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
				float dLambda = (vnrel + mBeta * pair.depth / h) / (p1.invMass + p2.invMass);

				float lambda0 = pair.lambda;
				pair.lambda = lambda0 + dLambda;
				if (pair.lambda < 0.f)
					pair.lambda = 0.f;
				dLambda = pair.lambda - lambda0;

				Vector3 impulse; impulse.SetZero();// = dLambda * pair.normal;
		
				// CCP friction (no relaxation)
				float limit = mFriction * pair.lambda;
				Vector3 vt = v12 - vnrel * pair.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambdaF = vtrel / (p1.invMass + p2.invMass);
					float lambdaF0 = pair.lambdaF1;
					pair.lambdaF1 = lambdaF0 + dLambdaF;
					if (pair.lambdaF1 >= limit)
					{							
						pair.lambda = (pair.lambda + mFriction * pair.lambdaF1) / (mFriction * mFriction + 1);
						dLambda = pair.lambda - lambda0;
						pair.lambdaF1 = mFriction * pair.lambda;
					}
					dLambdaF = pair.lambdaF1 - lambdaF0;

					vt.Scale(1.f / vtrel); // normalize
					impulse += dLambdaF * vt;
				}

				impulse += dLambda * pair.normal;
				p1.vel += p1.invMass * impulse;
				p2.vel -= p2.invMass * impulse;
			}
		}

		// velocity time stepping - friction only
		void SolveFrictionVTS(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
		
				// CCP friction (no relaxation)
				float limit = mFriction * pair.lambda;
				Vector3 vt = v12 - vnrel * pair.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel / (p1.invMass + p2.invMass);
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = lambda0 + dLambda;
					if (pair.lambdaF1 >= limit)
						pair.lambdaF1 = limit;
					dLambda = pair.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 impulse = dLambda * vt;
					p1.vel += p1.invMass * impulse;
					p2.vel -= p2.invMass * impulse;
				}
			}
		}

		// position based dynamics with stiffness
		void SolveContactPBD(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 delta = p1.pos - p2.pos;
				float len = delta.Length();
				if (len > pair.len || len < 0.00001f)
					continue;
				pair.normal = (1.f / len) * delta;
				float dLambda = mStiffness * (len - pair.len) / (p1.invMass + p2.invMass);
				pair.lambda += -invH * dLambda;

				Vector3 disp = dLambda * pair.normal;
				p1.pos -= p1.invMass * disp;
				p2.pos += p2.invMass * disp;
				disp *= invH;
				p1.vel -= p1.invMass * disp;
				p2.vel += p2.invMass * disp;
			}
		}

		// position based dynamics with damping
		void SolveContactDampedPBD(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 delta = p1.pos - p2.pos;
				float len = delta.Length();
				if (len > pair.len || len < 0.00001f) // TODO: accumulate and clamp
					continue;
				pair.normal = (1.f / len) * delta;
				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
				//float dLambda = (mStiffness * (len - pair.len) - h * mDamping * vnrel) / ((p1.invMass + p2.invMass) * (1.f + mDamping));
				float dLambda = h * h * ((len - pair.len) - h * mDamping * vnrel) / ((p1.invMass + p2.invMass) * h * (h + mDamping) + mEpsilon);
				pair.lambda += -invH * dLambda;

				Vector3 disp = dLambda * pair.normal;
				p1.pos -= p1.invMass * disp;
				p2.pos += p2.invMass * disp;
				disp *= invH;
				p1.vel -= p1.invMass * disp;
				p2.vel += p2.invMass * disp;
			}
		}

		// TODO: solve friction for PBD using correct cone projection

		// position based dynamics - friction only
		// basically the same thing as for VTS just that we update the velocity too
		void SolveFrictionPBD(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
		
				float limit = mFriction * pair.lambda;
				Vector3 vt = v12 - vnrel * pair.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel / (p1.invMass + p2.invMass);
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = lambda0 + dLambda;
					if (pair.lambdaF1 >= limit)
						pair.lambdaF1 = limit;
					dLambda = pair.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 impulse = dLambda * vt;
					p1.vel += p1.invMass * impulse;
					p2.vel -= p2.invMass * impulse;
					impulse *= h;
					p1.pos += p1.invMass * impulse;
					p2.pos -= p2.invMass * impulse;
				}
			}
		}

		// position based dynamics - contact and friction together
		void SolveContactAndFrictionPBD(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 delta = p1.pos - p2.pos;
				float len = delta.Length();
				if (len > pair.len || len < 0.00001f)
					continue;
				pair.normal = (1.f / len) * delta;
				float dLambda = mStiffness * (len - pair.len) / (p1.invMass + p2.invMass);
				pair.lambda += -invH * dLambda;

				Vector3 disp = dLambda * pair.normal;
				p1.pos -= p1.invMass * disp;
				p2.pos += p2.invMass * disp;
				disp *= invH;
				p1.vel -= p1.invMass * disp;
				p2.vel += p2.invMass * disp;

				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
		
				float limit = mFriction * pair.lambda;
				Vector3 vt = v12 - vnrel * pair.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel / (p1.invMass + p2.invMass);
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = lambda0 + dLambda;
					if (pair.lambdaF1 >= limit)
						pair.lambdaF1 = limit;
					dLambda = pair.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 impulse = dLambda * vt;
					p1.vel += p1.invMass * impulse;
					p2.vel -= p2.invMass * impulse;
					impulse *= h;
					p1.pos += p1.invMass * impulse;
					p2.pos -= p2.invMass * impulse;
				}
			}
		}

		// Jacobi part

		void SolveAcc(float h, Bodies& bodies, PhaseType phase) override
		{
			float theta0 = mTheta;
			mTheta = 0.5f * (-mTheta * mTheta + mTheta * sqrtf(mTheta * mTheta +4));
			float beta = theta0 * (1 - theta0) / (theta0 * theta0 + mTheta);
			if (phase == PT_VELOCITY)
				SolveContactVTSAcc(h, bodies);
			else if (phase == PT_POSITION)
				SolveContactAndFrictionPBDAcc(h, bodies, beta);
		}

		void SolveContactVTSAcc(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);
				float dLambda = 0.4f * (vnrel + mBeta * pair.depth / h) / (p1.invMass + p2.invMass);

				float lambda0 = pair.lambda;
				pair.lambda = lambda0 + dLambda;
				if (pair.lambda < 0.f)
					pair.lambda = 0.f;
				pair.dLambda = pair.lambda - lambda0;
			}
		}

		void ApplyForces(float h, Bodies& bodies, PhaseType phase) override
		{
			if (phase == PT_VELOCITY)
			{
				for (size_t i = 0; i < mContacts.size(); i++)
				{
					ParticleContact& pair = mContacts[i];
					Body& p1 = bodies[pair.i1];
					Body& p2 = bodies[pair.i2];
					Vector3 impulse = pair.dLambda * pair.normal;
					p1.vel += p1.invMass * impulse;
					p2.vel -= p2.invMass * impulse;
				}
			}
			else if (phase == PT_POSITION)
			{
				SolveContactAndFrictionPBDApply(h, bodies);
			}
		}

		void SolveContactPBDAcc(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 delta = p1.pos - p2.pos;
				float len = delta.Length();
				if (len > pair.len || len < 0.00001f)
				{
					pair.dLambda = 0;
					continue;
				}
				pair.normal = (1.f / len) * delta;
				float dLambda = mStiffness * (len - pair.len) / (p1.invMass + p2.invMass);
				pair.lambda += -invH * dLambda;
				pair.dLambda = dLambda;				
			}
		}

		void SolveContactPBDApply(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];

				Vector3 disp = pair.dLambda * pair.normal;
				p1.pos -= p1.invMass * disp;
				p2.pos += p2.invMass * disp;
				disp *= invH;
				p1.vel -= p1.invMass * disp;
				p2.vel += p2.invMass * disp;
			}
		}

		void SolveContactAndFrictionPBDAcc(float h, Bodies& bodies, float beta)
		{
			float invH = 1.f / h;
			float omega = 1.0f;
			#pragma omp parallel for
			for (int i = 0; i < (int)mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];
				Vector3 delta = p1.pos - p2.pos;
				float len = delta.Length();
				if (len > pair.len || len < 0.00001f)
				{
					pair.impulse.SetZero();
					continue;
				}
				pair.normal = (1.f / len) * delta;
				float dLambda = omega * mStiffness * (len - pair.len) / (p1.invMass + p2.invMass);
				pair.lambda += -invH * dLambda;

				Vector3 oldImp = pair.impulse;
				pair.impulse = -invH * dLambda * pair.normal;

				Vector3 v12 = p2.vel - p1.vel;
				float vnrel = pair.normal.Dot(v12);

				float limit = mFriction * pair.lambda;
				Vector3 vt = v12 - vnrel * pair.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = omega * vtrel / (p1.invMass + p2.invMass);
					float lambda0 = pair.lambdaF1;
					pair.lambdaF1 = lambda0 + dLambda;
					if (pair.lambdaF1 >= limit)
						pair.lambdaF1 = limit;
					dLambda = pair.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					pair.impulse += dLambda * vt;
				}

				pair.impulse += beta * oldImp;
			}
		}

		void SolveContactAndFrictionPBDApply(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			#pragma omp parallel for
			for (int i = 0; i < (int)mContacts.size(); i++)
			{
				ParticleContact& pair = mContacts[i];
				Body& p1 = bodies[pair.i1];
				Body& p2 = bodies[pair.i2];

				Vector3 impulse = pair.impulse;
				p1.vel += p1.invMass * impulse;
				p2.vel -= p2.invMass * impulse;
				impulse *= h;
				p1.pos += p1.invMass * impulse;
				p2.pos -= p2.invMass * impulse;
			}
		}

		std::vector<ParticleContact> mContacts;
		float mBeta, mStiffness, mFriction, mDamping, mEpsilon;
		float mTheta; // for Nesterov
	};
}

#endif // PARTICLE_CONTACTS