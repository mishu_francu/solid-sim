#include "Cloth.h"

namespace Physics
{
	void Cloth::DetectCollision(Bodies& bodies, Collidables& collidables, Constraints& constraints, bool beforePos)
	{
		mStaticContacts.Clear();

		for (size_t i = 0; i < collidables.size(); i++)
		{
			if ((collidables[i]->mType == CT_WALLS)) //TODO: collision filter (flags)
			{
				const Walls* walls = (const Walls*)collidables[i].get();
				WallCollisions(bodies, walls->mBox);
			}
			if ((collidables[i]->mType == CT_SPHERE)) //TODO: collision filter (flags)
			{
				const Sphere* sph = (const Sphere*)collidables[i].get();
				SphereCollisions(bodies, sph->center, sph->radius);
			}
		}

		constraints.AddList(&mLinkConstraints);
		constraints.AddList(&mStaticContacts);
	}
}