#ifndef UNIFIED_H
#define UNIFIED_H

#include <Physics/Common.h>
#include <vector>
#include <memory>
#include <Geometry/Mesh.h>
#include "Constraints.h"

namespace Physics
{
	struct Constants
	{
		static float unit; // the number of geometric units corresponding to 1 cm
		static float slipEps; // sliding velocity epsilon
	};

	typedef std::vector<std::shared_ptr<Collidable>> Collidables;

	class CollisionManager;
	class CollisionManagerBullet;

	class Group
	{
		template<typename Solver> friend class UnifiedSystem;
	public:
		Group() : mCount(0), mStart(0) { }
		size_t GetCount() const { return mCount; } 
		size_t GetStart() const { return mStart; } 

		virtual void DetectCollision(Bodies& bodies, Collidables& collidables, Constraints& constraints, bool beforePos) = 0;
		virtual void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints) = 0;
		virtual void UpdateCollidables(CollisionManager& collMgr, Bodies& bodies) { }

	protected:
		size_t mStart; // starting index for the group
		size_t mCount; // number of bodies in the group
	};

	typedef std::vector<std::shared_ptr<Group>> Groups;

	template<typename Solver>
	class UnifiedSystem
	{
	public:
		UnifiedSystem() : mTimeStep(0.016f), mNumSteps(1), mCollision(mGroups), mSolver(0, 10, 0) 
		{
		}

		Solver& GetSolver() { return mSolver; }

		// allocates a segment of the bodies array to this particular group
		uint32 AddGroup(std::shared_ptr<Group> group, size_t count)
		{
			mGroups.push_back(group);
			group->mStart = mBodies.size();
			group->mCount = count;
			mBodies.resize(mBodies.size() + count);
			return mGroups.size() - 1;
		}

		void Clear() 
		{
			mBodies.clear();
			mGroups.clear();
			mCollision.ClearCollidables();
		}

		Body& GetBody(size_t i) { return mBodies[i]; }
		Body& GetBodyFromGroup(size_t i, Group* group) { return mBodies[i + group->GetStart()]; }
		Bodies& GetBodies() { return mBodies; }

		void Step()
		{
			float h = mTimeStep / mNumSteps;
			for (size_t i = 0; i < mNumSteps; i++)
			{
				mSolver.Step(h, mBodies, mConstraints, mCollision);
			}

			// Update collidable positions and orientations
			// isn't this done too often/already?
			for (size_t i = 0; i < mGroups.size(); i++)
			{
				mGroups[i]->UpdateCollidables(mCollision, mBodies); // TODO: use mCollidables instead
			}
		}

	public:
		CollisionManagerBullet mCollision;

	private:
		Bodies mBodies; // degrees of freedom
		Groups mGroups;
		Constraints mConstraints;
		float mTimeStep;
		Solver mSolver;
		size_t mNumSteps;
	};
}

#endif // UNIFIED_H