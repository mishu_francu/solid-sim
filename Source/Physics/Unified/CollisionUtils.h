
namespace Physics
{
	template<typename ContactAdder>
	void VerticesVsTriangles(Geometry::Mesh* mesh1, Geometry::Mesh* mesh2, const Matrix3& R1, const Matrix3& R2, const Vector3& pos1, const Vector3& pos2,
		float radTol, ContactAdder& adder)
	{
		// triangles 2 vs vertices 1
		//#pragma omp parallel for
		for (int ii = 0; ii < (int)mesh2->indices.size(); ii += 3)
		{
			int i1 = mesh2->indices[ii];
			int i2 = mesh2->indices[ii + 1];
			int i3 = mesh2->indices[ii + 2];
			Vector3 x1 = R2 * mesh2->vertices[i1] + pos2;
			Vector3 x2 = R2 * mesh2->vertices[i2] + pos2;
			Vector3 x3 = R2 * mesh2->vertices[i3] + pos2;

			for (size_t jj = 0; jj < mesh1->vertices.size(); jj++)
			{
				Vector3 x4 = R1 * mesh1->vertices[jj] + pos1;
				BarycentricCoords coords;							
				Vector3 n, p;
				float d;
				bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
				if (intersect)
				{
					//Vector3 normal = cross(x2 - x1, x3 - x1);
					//normal.Normalize();
					//if (flip) normal.Flip();
					//Vector3 a1 = !bodies[j + mStart].R * (p - bodies[j + mStart].pos);
					//Vector3 a2 = mesh1->vertices[jj];
					//#pragma omp critical
					//AddContact(j + mStart, i + mStart, a1, a2, -normal, 0);
					adder.AddContact(jj);
				}
			}
		}
	}

	template<typename ContactAdder>
	void VerticesVsBox(const std::vector<Vector3>& vertices, const Matrix3& R, const Vector3& pos, const Vector3& ext, float tol, ContactAdder& adder)
	{
		Vector3 margin(tol);
		for (size_t i = 0; i < vertices.size(); i++)
		{
			// transform point in box space
			Vector3 p = !R * (vertices[i] - pos);

			if (PointInAabb3D(-ext - margin, ext + margin, p))
			{
				float d[6];
				d[0] = fabs(p.X() - ext.X());
				d[1] = fabs(p.X() + ext.X());
				d[2] = fabs(p.Y() - ext.Y());
				d[3] = fabs(p.Y() + ext.Y());
				d[4] = fabs(p.Z() - ext.Z());
				d[5] = fabs(p.Z() + ext.Z());
				int prox = 0;
				for (int j = 1; j < 6; j++)
				{
					if (d[j] < d[prox])
						prox = j;
				}
				Vector3 normal;
				if (prox == 0)
				{
					p.X() = ext.X();
					normal.Set(1, 0, 0);
				}
				if (prox == 1)
				{
					p.X() = -ext.X();
					normal.Set(-1, 0, 0);
				}
				if (prox == 2)
				{
					p.Y() = ext.Y();
					normal.Set(0, 1, 0);
				}
				if (prox == 3)
				{
					p.Y() = -ext.Y();
					normal.Set(0, -1, 0);
				}
				if (prox == 4)
				{
					p.Z() = ext.Z();
					normal.Set(0, 0, 1);
				}
				if (prox == 5)
				{
					p.Z() = -ext.Z();
					normal.Set(0, 0, -1);
				}
				//Vector3 pw = R * p + pos; // transform p back into world space
				//normal = pw - vertices[i];
				//normal.Normalize();
				normal = R * normal;
				adder.AddContact(i, p, normal);
			}
		}
	}
}