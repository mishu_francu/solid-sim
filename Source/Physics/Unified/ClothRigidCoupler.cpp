#include "ClothRigidCoupler.h"
#include <Engine/Profiler.h>
#include <Geometry/AabbTree.h>
#include <stack>

using namespace Geometry;

static float EvaluateFun(float t, const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
				  const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4, float thickness)
{
	Vector3 z1 = x1 + t * v1;
	Vector3 z2 = x2 + t * v2;
	Vector3 z3 = x3 + t * v3;
	Vector3 z4 = x4 + t * v4;

	// evaluate function
	Vector3 n1 = cross(z2 - z1, z3 - z1);
	float vol = n1.Dot(z4 - z1);
	float f = fabs(vol) - thickness * n1.Length();
	return f;
}

namespace Physics
{
	void ClothRigidCoupler::TriangleVsMeshTree(const Mesh* mesh, const AabbTree* tree, 
		Bodies& bodies, size_t ii, const Aabb3& triBox, const Collidable* shape, const Cloth* cloth,
		uint32 i1, uint32 i2, uint32 i3, 
		Vector3 x1, Vector3 x2, Vector3 x3,
		Vector3 v1, Vector3 v2, Vector3 v3,
		Vector3 n1, Vector3 n2, Vector3 n3)
	{
		if (!AabbOverlap3D(triBox, tree->box))
			return;

		std::stack<const AabbTree*> stack;
		stack.push(tree);

		while (!stack.empty())
		{
			const AabbTree* node = stack.top();
			stack.pop();

			if (!node->vertices.empty())
			{
				// do collision detection with this node
				std::vector<Vector3> temp;
				for (size_t i = 0; i < node->vertices.size(); i++)
				{
					temp.push_back(mesh->vertices[node->vertices[i].idx]);
				}
				// TODO: subset wrapper class

				TriangleVsMesh(temp, bodies, ii, triBox, shape, cloth, i1, i2, i3, x1, x2, x3, v1, v2, v3, n1, n2, n3);
			}

			if (node->left && AabbOverlap3D(triBox, node->left->box))
				stack.push(node->left);
			if (node->right && AabbOverlap3D(triBox, node->right->box))
				stack.push(node->right);
		}
	}

	void ClothRigidCoupler::TriangleVsMesh(const std::vector<Vector3>& vertices, 
		Bodies& bodies, size_t ii, const Aabb3& triBox, const Collidable* shape, const Cloth* cloth,
		uint32 i1, uint32 i2, uint32 i3, 
		Vector3 x1, Vector3 x2, Vector3 x3,
		Vector3 v1, Vector3 v2, Vector3 v3,
		Vector3 n1, Vector3 n2, Vector3 n3)
	{
		for (size_t k = 0; k < vertices.size(); k++)
		{
			//Vector3 x4 = bodies[ii].pos0 + qRotate(bodies[ii].q0, vertices[k]);
			//Vector3 y4 = bodies[ii].pos + qRotate(bodies[ii].q, vertices[k]);
			Vector3 y4 = vertices[k];
			Vector3 x4w = bodies[ii].pos0 + qRotate(bodies[ii].q0, vertices[k]);
			Vector3 x4 = qRotate(bodies[ii].q.GetConjugate(), x4w - bodies[ii].pos);
			
			Vector3 v4 = y4 - x4;

			Aabb3 vBox(vmin(x4, y4), vmax(x4, y4));
			vBox.Extrude(shape->mTolerance);
			if (!AabbOverlap3D(triBox, vBox))
				continue;

			Vector3 n, p;
			float d;
			BarycentricCoords coords;
			float radTol = shape->mTolerance + cloth->GetThickness();
			bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
			if (intersect)
			{
				TriangleRigidContact c;
				c.i1 = i1;
				c.i2 = i2;
				c.i3 = i3;
				c.i4 = (uint32)ii;
				c.w1 = coords.u;
				c.w2 = coords.v;
				c.w3 = coords.w;
				c.normal = n;
				c.normal = qRotate(bodies[ii].q, c.normal);
				c.a = vertices[k];
				c.len = cloth->GetThickness();
				#pragma omp critical
				mTriangleContacts.mTris.push_back(c);
				continue;
			}
			float f0 = EvaluateFun(0.f, x1, x2, x3, x4, v1, v2, v3, v4, cloth->GetThickness());
			if (f0 < 0)	continue;
			float f1 = EvaluateFun(1.f, x1, x2, x3, x4, v1, v2, v3, v4, cloth->GetThickness());

			float tx0 = 0;
			float tx1 = 1;
			float fx0 = f0;
			float fx1 = f1;
			float tx = tx0;
			for (int l = 0; l < 20; l++)
			{
				float tmid = 0.5f * (tx0 + tx1);
				float fmid = EvaluateFun(tmid, x1, x2, x3, x4, v1, v2, v3, v4, cloth->GetThickness());
				if (fabs(fmid) < 0.01f)
				{
					tx = tmid;
					break;
				}
				if (f0 * fmid < 0)
				{
					tx1 = tmid;
					f1 = fmid;
					tx = tx0;
				}
				else if (fmid * f1 < 0)
				{
					tx0 = tmid;
					f0 = fmid;
					tx = tmid;
				}
				else
				{
					break;
				}
			}					

			if (tx > 0 && f0 > 0)
			{
				Vector3 z1 = x1 + tx * v1;
				Vector3 z2 = x2 + tx * v2;
				Vector3 z3 = x3 + tx * v3;
				Vector3 z4 = x4 + tx * v4;

				bool intersect = IntersectSphereTriangle(z4, radTol, z1, z2, z3, n, p, d, coords);
				if (intersect)
				{
					TriangleRigidContact c;
					c.i1 = i1;
					c.i2 = i2;
					c.i3 = i3;
					c.i4 = (uint32)ii;
					c.w1 = coords.u;
					c.w2 = coords.v;
					c.w3 = coords.w;
					//c.normal = n;
					//c.normal = -cross(x2 - x1, x3 - x1);
					c.normal = c.w1 * n1 + c.w2 * n2 + c.w3 * n3;
					c.normal.Flip();
					c.normal.Normalize();
					c.normal = qRotate(bodies[ii].q, c.normal);
					c.a = vertices[k];
					c.len = cloth->GetThickness();
					#pragma omp critical
					mTriangleContacts.mTris.push_back(c);
					continue;
				}
			}
		}
	}

	void ClothRigidCoupler::DetectCollision(Cloth* cloth, Rigids* rigids, Collidables& collidables, Bodies& bodies, Constraints& constraints)
	{
		//PROFILE_SCOPE("Detect collision");
		// brute force point-triangle collisions
		mTriangleContacts.mTris.clear();

		{
			PROFILE_SCOPE("Triangle - vertex");
		#pragma omp parallel for
		for (int j = 0; j < (int)cloth->mTriangles.size(); j++)
		{
			const uint32 i1 = cloth->mTriangles[j].i1;
			const uint32 i2 = cloth->mTriangles[j].i2;
			const uint32 i3 = cloth->mTriangles[j].i3;
			Vector3 x1 = bodies[i1].pos0;
			Vector3 x2 = bodies[i2].pos0;
			Vector3 x3 = bodies[i3].pos0;
			Vector3 y1 = bodies[i1].pos;
			Vector3 y2 = bodies[i2].pos;
			Vector3 y3 = bodies[i3].pos;
			//Vector3 v1 = y1 - x1;
			//Vector3 v2 = y2 - x2;
			//Vector3 v3 = y3 - x3;
			Vector3 n1 = cloth->GetMesh().normals[i1 - cloth->GetStart()];
			Vector3 n2 = cloth->GetMesh().normals[i2 - cloth->GetStart()];
			Vector3 n3 = cloth->GetMesh().normals[i3 - cloth->GetStart()];

			// point - triangle
			for (size_t i = 0; i < rigids->GetCount(); i++)
			{
				size_t ii = i + rigids->GetStart();
				if (bodies[ii].invMass == 0)
					continue;
				if (bodies[ii].collIdx < 0)
					continue;
				const Collidable* shape = collidables[bodies[ii].collIdx].get();
				if (shape->mType == CT_SPHERE)
				{
					const Sphere* sph = (const Sphere*)shape;
					const Vector3& x4 = bodies[ii].pos0;
					// TODO: AABB test

					Vector3 n, p;
					float d;
					BarycentricCoords coords;
					float radTol = sph->radius + sph->mTolerance + cloth->GetThickness();
					bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
					if (intersect)
					{
						TriangleRigidContact c;
						c.i1 = i1;
						c.i2 = i2;
						c.i3 = i3;
						c.i4 = (uint32)ii;
						c.w1 = coords.u;
						c.w2 = coords.v;
						c.w3 = coords.w;
						//c.normal = n;
						//c.normal = -cross(x2 - x1, x3 - x1);
						c.normal = c.w1 * n1 + c.w2 * n2 + c.w3 * n3;
						c.normal.Flip();
						c.normal.Normalize();
						c.len = sph->radius + cloth->GetThickness();
						#pragma omp critical
						mTriangleContacts.mTris.push_back(c);
					}

					// also test the candidate position
					intersect = IntersectSphereTriangle(bodies[ii].pos0, radTol, x1, x2, x3, n, p, d, coords);
					if (intersect)
					{
						TriangleRigidContact c;
						c.i1 = i1;
						c.i2 = i2;
						c.i3 = i3;
						c.i4 = (uint32)ii;
						c.w1 = coords.u;
						c.w2 = coords.v;
						c.w3 = coords.w;
						//c.normal = n;
						//c.normal = -cross(x2 - x1, x3 - x1);
						c.normal = c.w1 * n1 + c.w2 * n2 + c.w3 * n3;
						c.normal.Flip();
						c.normal.Normalize();
						c.len = sph->radius + cloth->GetThickness();
						#pragma omp critical
						mTriangleContacts.mTris.push_back(c);
					}
				}
				if (shape->mType == CT_BOX || shape->mType == CT_MESH)
				{
					// transform triangle in mesh space
					Vector3 x1l = qRotate(bodies[ii].q.GetConjugate(), x1 - bodies[ii].pos);
					Vector3 x2l = qRotate(bodies[ii].q.GetConjugate(), x2 - bodies[ii].pos);
					Vector3 x3l = qRotate(bodies[ii].q.GetConjugate(), x3 - bodies[ii].pos);
					Vector3 y1l = qRotate(bodies[ii].q.GetConjugate(), y1 - bodies[ii].pos);
					Vector3 y2l = qRotate(bodies[ii].q.GetConjugate(), y2 - bodies[ii].pos);
					Vector3 y3l = qRotate(bodies[ii].q.GetConjugate(), y3 - bodies[ii].pos);
					Vector3 n1l = qRotate(bodies[ii].q.GetConjugate(), n1);
					Vector3 n2l = qRotate(bodies[ii].q.GetConjugate(), n2);
					Vector3 n3l = qRotate(bodies[ii].q.GetConjugate(), n3);

					// build AABB
					Vector3 extrude(cloth->GetThickness());
					Vector3 minX = vmin(vmin(x1l, x2l), x3l);
					Vector3 maxX = vmax(vmax(x1l, x2l), x3l);
					Vector3 minY = vmin(vmin(y1l, y2l), y3l);
					Vector3 maxY = vmax(vmax(y1l, y2l), y3l);
					Vector3 minV = vmin(minX, minY) - extrude;
					Vector3 maxV = vmax(maxX, maxY) + extrude;
					Aabb3 triBox(minV, maxV);

					const Mesh* mesh = nullptr;
					if (shape->mType == CT_BOX)
					{
						const Box* box = (const Box*)shape;
						mesh = &box->mesh;
						auto meshBox = mesh->GetAabb(); //(bodies[ii].q, bodies[ii].pos);					
						meshBox.Extrude(shape->mTolerance);
						if (!AabbOverlap3D(triBox, meshBox))
							continue;
						TriangleVsMesh(mesh->vertices, bodies, ii, triBox, shape, cloth, i1, i2, i3, 
							x1l, x2l, x3l, y1l - x1l, y2l - x2l, y3l - x3l, n1l, n2l, n3l);
					}
					else
					{
						const CollisionMesh* collMesh = (const CollisionMesh*)shape;
						mesh = collMesh->mesh;
						TriangleVsMeshTree(mesh, collMesh->tree, bodies, ii, triBox, shape, cloth, i1, i2, i3, 
							x1l, x2l, x3l, y1l - x1l, y2l - x2l, y3l - x3l, n1l, n2l, n3l);
					}
				}
			}
		}
		}

		if (0)
		{
			PROFILE_SCOPE("Edge - edge");
		// edge - edge
		const float eps = 0.01f;
		const float thickness2 = cloth->GetThickness() * cloth->GetThickness();
		#pragma omp parallel for
		for (int k = 0; k < (int)cloth->GetMesh().edges.size(); k++)
		{
			size_t i1 = cloth->GetMesh().edges[k].i1 + cloth->GetStart();
			size_t i2 = cloth->GetMesh().edges[k].i2 + cloth->GetStart();
			const Vector3& x1 = bodies[i1].pos0;
			const Vector3& x2 = bodies[i2].pos0;
			// build AABB
			Vector3 extrude(cloth->GetThickness());
			Vector3 minV = vmin(x1, x2) - extrude;
			Vector3 maxV = vmax(x1, x2) + extrude;
			Aabb3 edgeBox(minV, maxV);

			for (size_t i = 0; i < rigids->GetCount(); i++)
			{
				size_t ii = i + rigids->GetStart();
				if (bodies[ii].invMass == 0)
					continue;
				if (bodies[ii].collIdx < 0)
					continue;
				const Collidable* shape = collidables[bodies[ii].collIdx].get();
				if (shape->mType == CT_BOX || shape->mType == CT_MESH)
				{
					const Mesh* mesh = nullptr;
					if (shape->mType == CT_BOX)
					{
						const Box* box = (const Box*)shape;
						mesh = &box->mesh;
					}
					else
					{
						const CollisionMesh* collMesh = (const CollisionMesh*)shape;
						mesh = collMesh->mesh;
					}
					Aabb3 bbox = mesh->GetAabb(bodies[ii].q0, bodies[ii].pos0); // TODO: cache
					bbox.Extrude(shape->mTolerance);
					if (!AabbOverlap3D(edgeBox, bbox))
						continue;
					for (size_t j = 0; j < mesh->edges.size(); j++)
					{
						int j1 = mesh->edges[j].i1;
						int j2 = mesh->edges[j].i2;

						const Vector3& x3 = bodies[ii].pos0 + qRotate(bodies[ii].q0, mesh->vertices[j1]);
						const Vector3& x4 = bodies[ii].pos0 + qRotate(bodies[ii].q0, mesh->vertices[j2]);

						// build AABB
						Vector3 minV = vmin(x3, x4) - extrude;
						Vector3 maxV = vmax(x3, x4) + extrude;
						Aabb3 edgeBox2(minV, maxV);

						if (!AabbOverlap3D(edgeBox, edgeBox2))
							continue;

						Vector3 c1, c2;
						float s, t;
						bool intersect = false;
						float dSqr = ClosestPtSegmSegm(x1, x2, x3, x4, s, t, c1, c2);

						intersect = dSqr <= thickness2
							&& fabs(s - 1.f) > eps && fabs(s) > eps
							&& fabs(t - 1.f) > eps && fabs(t) > eps;
						if (intersect)
						{
							TriangleRigidContact c;
							c.i1 = (uint32)i1;
							c.i2 = (uint32)i2;
							c.i3 = 0;
							c.i4 = (uint32)ii;
							c.w1 = s;
							c.w2 = 1.f - s;
							c.w3 = 0;
							Vector3 q1 = x1 + s * (x2 - x1);
							Vector3 q2 = x3 + t * (x4 - x3);
							Vector3 n = q2 - q1;
							n.Normalize();
							c.normal = n;
							c.a = (1.f - t) * mesh->vertices[j1] + t * mesh->vertices[j2];
							c.len = cloth->GetThickness();
							#pragma omp critical
							mTriangleContacts.mTris.push_back(c);								
						}
					}
				}
			}
		}
		}

		constraints.AddList(&mTriangleContacts);
	}

}