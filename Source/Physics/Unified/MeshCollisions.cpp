#include "MeshCollisions.h"
#include <Engine/Profiler.h>
#include <Geometry/AabbTree.h>
#include <Geometry/Mesh.h>
#include <Geometry/Collision3D.h>

#include "Physics/ElTopoWrapper.h"
#include "BulletWrapper.h"

#include <stack>

using namespace Geometry;

namespace Physics
{
	// not used
	void MeshCollisions::TrianglesVsVertices(Bodies& bodies, int i, int j, const Mesh* mesh1, const Mesh* mesh2, float radTol, bool flip)
	{
		#pragma omp parallel for
		for (int ii = 0; ii < (int)mesh2->indices.size(); ii += 3)
		{
			int i1 = mesh2->indices[ii];
			int i2 = mesh2->indices[ii + 1];
			int i3 = mesh2->indices[ii + 2];
			Vector3 x1 = bodies[j].R * mesh2->vertices[i1] + bodies[j].pos;
			Vector3 x2 = bodies[j].R * mesh2->vertices[i2] + bodies[j].pos;
			Vector3 x3 = bodies[j].R * mesh2->vertices[i3] + bodies[j].pos;

			Aabb3 triAabb;
			triAabb.Add(x1);
			triAabb.Add(x2);
			triAabb.Add(x3);
			triAabb.Extrude(radTol);

			for (size_t jj = 0; jj < mesh1->vertices.size(); jj++)
			{
				Vector3 x4 = bodies[i].R * mesh1->vertices[jj] + bodies[i].pos;
				if (!PointInAabb3D(triAabb.min, triAabb.max, x4))
					continue;
				BarycentricCoords coords;							
				Vector3 n, p;
				float d;
				bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
				if (!intersect)
				{
					x4 = bodies[i].R * mesh1->vertices[jj] + bodies[i].pos0;
					intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
				}
				if (intersect)
				{
					Vector3 normal = cross(x2 - x1, x3 - x1);
					normal.Normalize();
					if (flip) normal.Flip();
					Vector3 a1 = !bodies[j].R * (p - bodies[j].pos);
					Vector3 a2 = mesh1->vertices[jj];
					//#pragma omp critical
					//AddContact(j, i, a1, a2, -normal, 0);
				}
			}
		}
	}

	void MeshCollisions::CollideMeshesCCD(Bodies& bodies, int iMesh1, int iMesh2, const Mesh* mesh1, const Mesh* mesh2,
		float radTol, IContactAdder* adder)
	{
		MeshVsMeshCCD(bodies, iMesh1, iMesh2, mesh1, mesh2, nullptr, radTol, adder);
		MeshVsMeshCCD(bodies, iMesh2, iMesh1, mesh2, mesh1, nullptr, radTol, adder);
	}

	void MeshCollisions::TriangleVsMeshTree(const Mesh* mesh, const AabbTree* tree, 
		Bodies& bodies, const Aabb3& triBox, float radTol,
		int iMesh, int iTri,
		Vector3 x1, Vector3 x2, Vector3 x3,
		Vector3 v1, Vector3 v2, Vector3 v3,
		Vector3 n1, Vector3 n2, Vector3 n3)
	{
		if (!AabbOverlap3D(triBox, tree->box))
			return;

		std::stack<const AabbTree*> stack;
		stack.push(tree);

		while (!stack.empty())
		{
			const AabbTree* node = stack.top();
			stack.pop();

			if (!node->vertices.empty())
			{
				// do collision detection with this node
				std::vector<Vector3> temp;
				for (size_t i = 0; i < node->vertices.size(); i++)
				{
					temp.push_back(mesh->vertices[node->vertices[i].idx]);
				}
				// TODO: subset wrapper class

				MeshCollisions::TriangleVsMesh(temp, bodies, triBox, radTol, iMesh, iTri, x1, x2, x3, v1, v2, v3, n1, n2, n3, nullptr);
			}

			if (node->left && AabbOverlap3D(triBox, node->left->box))
				stack.push(node->left);
			if (node->right && AabbOverlap3D(triBox, node->right->box))
				stack.push(node->right);
		}
	}

	static float EvaluateFun(float t, const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
		const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4, float thickness)
	{
		Vector3 z1 = x1 + t * v1;
		Vector3 z2 = x2 + t * v2;
		Vector3 z3 = x3 + t * v3;
		Vector3 z4 = x4 + t * v4;

		// evaluate function
		Vector3 n1 = cross(z2 - z1, z3 - z1);
		float vol = n1.Dot(z4 - z1);
		float f = fabs(vol) - thickness * n1.Length();
		return f;
	}

	void MeshCollisions::MeshVsMeshCCD(Bodies& bodies, int iMesh1, int iMesh2, const Mesh* mesh1, const Mesh* mesh2, const AabbTree* mesh2Tree, 
		float radTol, IContactAdder* adder, bool flip)
	{
		auto meshBox = mesh2->GetAabb();
		meshBox.Extrude(radTol * 0.5f);
		{
			PROFILE_SCOPE("Triangle - vertex");
			#pragma omp parallel for
			for (int k = 0; k < (int)mesh1->indices.size(); k += 3)
			{
				// each triangle in mesh1
				const uint32 i1 = mesh1->indices[k];
				const uint32 i2 = mesh1->indices[k + 1];
				const uint32 i3 = mesh1->indices[k + 2];
				int jj = iMesh1;// + mStart;
				Vector3 x1 = qRotate(bodies[jj].q0, mesh1->vertices[i1]) + bodies[jj].pos0;
				Vector3 x2 = qRotate(bodies[jj].q0, mesh1->vertices[i2]) + bodies[jj].pos0;
				Vector3 x3 = qRotate(bodies[jj].q0, mesh1->vertices[i3]) + bodies[jj].pos0;
				Vector3 y1 = qRotate(bodies[jj].q, mesh1->vertices[i1]) + bodies[jj].pos;
				Vector3 y2 = qRotate(bodies[jj].q, mesh1->vertices[i2]) + bodies[jj].pos;
				Vector3 y3 = qRotate(bodies[jj].q, mesh1->vertices[i3]) + bodies[jj].pos;
				Vector3 n1 = mesh1->normals[i1];
				Vector3 n2 = mesh2->normals[i2];
				Vector3 n3 = mesh2->normals[i3];

				// point - triangle

				// transform triangle in mesh2 space
				int ii = iMesh2;// + mStart;
				Vector3 x1l = qRotate(bodies[ii].q.GetConjugate(), x1 - bodies[ii].pos);
				Vector3 x2l = qRotate(bodies[ii].q.GetConjugate(), x2 - bodies[ii].pos);
				Vector3 x3l = qRotate(bodies[ii].q.GetConjugate(), x3 - bodies[ii].pos);
				Vector3 y1l = qRotate(bodies[ii].q.GetConjugate(), y1 - bodies[ii].pos);
				Vector3 y2l = qRotate(bodies[ii].q.GetConjugate(), y2 - bodies[ii].pos);
				Vector3 y3l = qRotate(bodies[ii].q.GetConjugate(), y3 - bodies[ii].pos);
				Vector3 n1l = qRotate(bodies[ii].q.GetConjugate(), n1);
				Vector3 n2l = qRotate(bodies[ii].q.GetConjugate(), n2);
				Vector3 n3l = qRotate(bodies[ii].q.GetConjugate(), n3);

				// build AABB
				Vector3 extrude(radTol * 0.5f);
				Vector3 minX = vmin(vmin(x1l, x2l), x3l);
				Vector3 maxX = vmax(vmax(x1l, x2l), x3l);
				Vector3 minY = vmin(vmin(y1l, y2l), y3l);
				Vector3 maxY = vmax(vmax(y1l, y2l), y3l);
				Vector3 minV = vmin(minX, minY) - extrude;
				Vector3 maxV = vmax(maxX, maxY) + extrude;
				Aabb3 triBox(minV, maxV);

				if (!AabbOverlap3D(triBox, meshBox))
					continue;
				if (mesh2Tree == nullptr)
				{
					TriangleVsMesh(mesh2->vertices, bodies, triBox, radTol, iMesh2, iMesh1, 
						x1l, x2l, x3l, y1l - x1l, y2l - x2l, y3l - x3l, n1l, n2l, n3l, adder);
				}
				else
				{
					//const CollisionMesh* collMesh = (const CollisionMesh*)shape;
					//mesh = collMesh->mesh;
					TriangleVsMeshTree(mesh2, mesh2Tree, bodies, triBox, radTol, iMesh2, iMesh1, 
						x1l, x2l, x3l, y1l - x1l, y2l - x2l, y3l - x3l, n1l, n2l, n3l);				
				}
			}
		}
	}

	void MeshCollisions::TriangleVsMesh(const std::vector<Vector3>& vertices, 
		Bodies& bodies, const Aabb3& triBox, float radTol,
		int iMesh, int iTri,
		Vector3 x1, Vector3 x2, Vector3 x3,
		Vector3 v1, Vector3 v2, Vector3 v3,
		Vector3 n1, Vector3 n2, Vector3 n3,
		IContactAdder* adder)
	{
		radTol *= 0.5f;
		#pragma omp parallel for
		for (int k = 0; k < (int)vertices.size(); k++)
		{
			Vector3 y4 = vertices[k];
			Vector3 x4w = bodies[iMesh].pos0 + qRotate(bodies[iMesh].q0, vertices[k]);
			Vector3 x4 = qRotate(bodies[iMesh].q.GetConjugate(), x4w - bodies[iMesh].pos);

			Vector3 v4 = y4 - x4;

			Aabb3 vBox(vmin(x4, y4), vmax(x4, y4));
			vBox.Extrude(radTol);
			if (!AabbOverlap3D(triBox, vBox))
				continue;

			Vector3 n, p;
			float d;
			BarycentricCoords coords;
			bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
			if (intersect)
			{
				Vector3 normal = -cross(x2 - x1, x3 - x1);
				normal.Normalize();
				//Vector3 normal = n;
				normal = qRotate(bodies[iMesh].q, normal);
				Vector3 a1 = !bodies[iTri].R * (bodies[iMesh].R * p + bodies[iMesh].pos - bodies[iTri].pos);
				Vector3 a2 = vertices[k];
				#pragma omp critical
				adder->AddContact(iTri, iMesh, a1, a2, normal, 0);
				//adder->Add(i1, i2, i3, ii, coords, n, vertices[k], radTol);
			}
			float f0 = EvaluateFun(0.f, x1, x2, x3, x4, v1, v2, v3, v4, radTol);
			if (f0 < 0)	continue;
			float f1 = EvaluateFun(1.f, x1, x2, x3, x4, v1, v2, v3, v4, radTol);

			float tx0 = 0;
			float tx1 = 1;
			float fx0 = f0;
			float fx1 = f1;
			float tx = tx0;
			int numIters = 2;
			for (int l = 0; l < numIters; l++)
			{
				float tmid = 0.5f * (tx0 + tx1);
				float fmid = EvaluateFun(tmid, x1, x2, x3, x4, v1, v2, v3, v4, radTol);
				if (fabs(fmid) < 0.01f)
				{
					tx = tmid;
					break;
				}
				if (f0 * fmid < 0)
				{
					tx1 = tmid;
					f1 = fmid;
					tx = tx0;
				}
				else if (fmid * f1 < 0)
				{
					tx0 = tmid;
					f0 = fmid;
					tx = tmid;
				}
				else
				{
					break;
				}
			}	

			if (tx > 0 && f0 > 0)
			{
				Vector3 z1 = x1 + tx * v1;
				Vector3 z2 = x2 + tx * v2;
				Vector3 z3 = x3 + tx * v3;
				Vector3 z4 = x4 + tx * v4;

				bool intersect = IntersectSphereTriangle(z4, radTol, z1, z2, z3, n, p, d, coords);
				if (intersect)
				{
					Vector3 normal = -cross(x2 - x1, x3 - x1);
					normal.Normalize();
					//Vector3 normal = n;
					normal = qRotate(bodies[iMesh].q, normal);
					Vector3 a1 = !bodies[iTri].R * (bodies[iMesh].R * p + bodies[iMesh].pos - bodies[iTri].pos);
					Vector3 a2 = vertices[k];
					#pragma omp critical
					adder->AddContact(iTri, iMesh, a1, a2, normal, 0);
				}
			}
		}
	}

	void MeshCollisions::MeshVsMesh(Bodies& bodies, int i, int j, const Mesh* mesh1, const Mesh* mesh2, float radTol, 
		const Matrix3& R1, const Vector3& pos1, const Vector3& pos1Prev, const Matrix3& R2, const Vector3& pos2, const Vector3& pos2Prev,
		bool flip, IContactAdder* adder)
	{
		PROFILE_SCOPE("Mesh vs mesh");

		Aabb3 aabb1 = mesh1->GetAabb(R1, pos1);
		aabb1.Extrude(radTol * 0.5f);
		Aabb3 aabb2 = mesh2->GetAabb(R2, pos2);
		aabb2.Extrude(radTol * 0.5f);
		if (!AabbOverlap3D(aabb1, aabb2))
			return;

		// triangles 1 vs vertices 2
		//#pragma omp parallel for
		for (int ii = 0; ii < (int)mesh1->indices.size(); ii += 3)
		{
			int i1 = mesh1->indices[ii];
			int i2 = mesh1->indices[ii + 1];
			int i3 = mesh1->indices[ii + 2];
			Vector3 x1 = R1 * mesh1->vertices[i1] + pos1;
			Vector3 x2 = R1 * mesh1->vertices[i2] + pos1;
			Vector3 x3 = R1 * mesh1->vertices[i3] + pos1;

			Aabb3 triAabb;
			triAabb.Add(x1);
			triAabb.Add(x2);
			triAabb.Add(x3);
			triAabb.Extrude(2 * radTol);

			for (size_t jj = 0; jj < mesh2->vertices.size(); jj++)
			{
				Vector3 x4 = R2 * mesh2->vertices[jj] + pos2;
				Vector3 x4prev = R2 * mesh2->vertices[jj] + pos2Prev;
				Vector3 n, p;
				float d;
				BarycentricCoords coords;							
				bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
				if (!intersect)
				{
					intersect = IntersectSphereTriangle(x4prev, radTol, x1, x2, x3, n, p, d, coords);
				}
				if (intersect)
				{
					Vector3 a1 = !R1 * (p - pos1);
					Vector3 a2 = mesh2->vertices[jj];
					//Vector3 normal = cross(x2 - x1, x3 - x1);
					//normal.Normalize();
					Vector3 normal = mesh2->normals[jj];
					if (flip) normal.Flip();
					//#pragma omp critical
					adder->AddContact(i, j, a1, a2, -normal, 0);
					adder->AddContact(i1 + i, i2 + i, i3 + i, jj + j, coords, n, a2, 0);
				}
			}
		}

		// triangles 2 vs vertices 1
		//#pragma omp parallel for
		for (int ii = 0; ii < (int)mesh2->indices.size(); ii += 3)
		{
			int i1 = mesh2->indices[ii];
			int i2 = mesh2->indices[ii + 1];
			int i3 = mesh2->indices[ii + 2];
			Vector3 x1 = R2 * mesh2->vertices[i1] + pos2;
			Vector3 x2 = R2 * mesh2->vertices[i2] + pos2;
			Vector3 x3 = R2 * mesh2->vertices[i3] + pos2;

			for (size_t jj = 0; jj < mesh1->vertices.size(); jj++)
			{
				Vector3 x4 = R1 * mesh1->vertices[jj] + pos1;
				BarycentricCoords coords;							
				Vector3 n, p;
				float d;
				bool intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
				if (!intersect)
				{
					x4 = R1 * mesh1->vertices[jj] + pos1Prev;
					intersect = IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
				}
				if (intersect)
				{
					//Vector3 normal = cross(x2 - x1, x3 - x1);
					//normal.Normalize();
					Vector3 normal = mesh1->normals[jj];
					if (flip) normal.Flip();
					Vector3 a1 = !R2 * (p - pos2);
					Vector3 a2 = mesh1->vertices[jj];
					//#pragma omp critical
					adder->AddContact(j, i, a1, a2, -normal, 0);
					adder->AddContact(i1 + j, i2 + j, i3 + j, jj + i, coords, n, a2, 0);
				}
			}
		}
	}

	void MeshCollisionsElTopo::CollideMeshesCCD(Bodies& bodies, int i, int j, const Mesh* mesh1, const Mesh* mesh2, 
		float radTol, IContactAdder* adder)
	{
		// triangles 2 vs vertices 1
		#pragma omp parallel for
		for (int ii = 0; ii < (int)mesh2->indices.size(); ii += 3)
		{
			size_t i1 = mesh2->indices[ii];
			size_t i2 = mesh2->indices[ii + 1];
			size_t i3 = mesh2->indices[ii + 2];
			Vec3st tri(i1, i2, i3);
			Vec3st sorted_tri = sort_triangle(tri);
			i1 = sorted_tri[0];
			i2 = sorted_tri[1];
			i3 = sorted_tri[2];
			// TODO: use old rotations
			Vector3 x1 = bodies[j].R * mesh2->vertices[i1] + bodies[j].pos0;
			Vector3 x2 = bodies[j].R * mesh2->vertices[i2] + bodies[j].pos0;
			Vector3 x3 = bodies[j].R * mesh2->vertices[i3] + bodies[j].pos0;
			Vector3 y1 = bodies[j].R * mesh2->vertices[i1] + bodies[j].pos0;
			Vector3 y2 = bodies[j].R * mesh2->vertices[i2] + bodies[j].pos0;
			Vector3 y3 = bodies[j].R * mesh2->vertices[i3] + bodies[j].pos0;

			for (size_t jj = 0; jj < mesh1->vertices.size(); jj++)
			{
				Vector3 x4 = bodies[i].R * mesh1->vertices[jj] + bodies[i].pos0;
				Vector3 y4 = bodies[i].R * mesh1->vertices[jj] + bodies[i].pos;

				double s1, s2, s3; // barycentric coords
				Vec3d normal;
				double rel_disp;
				if ( point_triangle_collision(V2ETV(x4), V2ETV(y4), jj,
					V2ETV(x1), V2ETV(y1), i1,
					V2ETV(x2), V2ETV(y2), i2,
					V2ETV(x3), V2ETV(y3), i3,
					s1, s2, s3, normal, rel_disp) )
				{
					Vector3 p = (float)s1 * x1 + (float)s2 * x2 + (float)s3 * x3;
					Vector3 a2 = !bodies[j].R * (p - bodies[j].pos);
					Vector3 a1 = mesh1->vertices[jj];
					Vector3 n = ETV2V(normal);
					float dot = mesh1->normals[jj].Dot(n);
					#pragma omp critical
					adder->AddContact(i, j, a1, a2, n, (float)rel_disp);
				}

			}
		}
	}

	void MeshCollisionsBullet::MeshVsMesh(Bodies& bodies, int i, int j, const Mesh* mesh1, const Mesh* mesh2, float radTol, 
		bool flip, IContactAdder* adder)
	{
		Aabb3 aabb1 = mesh1->GetAabb(bodies[i].q, bodies[i].pos);
		aabb1.Extrude(0.5f * radTol);
		Aabb3 aabb2 = mesh2->GetAabb(bodies[j].q, bodies[j].pos);
		aabb2.Extrude(0.5f * radTol);
		if (!AabbOverlap3D(aabb1, aabb2))
			return;
		
#ifdef BULLET_COLLISIONS
		GIM_TRIANGLE_CONTACT contact_data;

		// triangles vs triangles
		#pragma omp parallel for
		for (int ii = 0; ii < (int)mesh1->indices.size(); ii += 3)
		{
			size_t i1 = mesh1->indices[ii];
			size_t i2 = mesh1->indices[ii + 1];
			size_t i3 = mesh1->indices[ii + 2];
			Vector3 x1 = bodies[i].R * mesh1->vertices[i1] + bodies[i].pos;
			Vector3 x2 = bodies[i].R * mesh1->vertices[i2] + bodies[i].pos;
			Vector3 x3 = bodies[i].R * mesh1->vertices[i3] + bodies[i].pos;

			// build AABB
			Vector3 xMin = vmin(x1, vmin(x2, x3));
			Vector3 xMax = vmax(x1, vmax(x2, x3));
			Aabb3 box1(xMin, xMax);
			box1.Extrude(0.5f * radTol);

			btPrimitiveTriangle tri1;
			tri1.m_vertices[0] = V2BV(x1);
			tri1.m_vertices[1] = V2BV(x2);
			tri1.m_vertices[2] = V2BV(x3);
			tri1.buildTriPlane();

			for (size_t jj = 0; jj < mesh2->indices.size(); jj += 3)
			{
				size_t j1 = mesh2->indices[jj];
				size_t j2 = mesh2->indices[jj + 1];
				size_t j3 = mesh2->indices[jj + 2];
				Vector3 y1 = bodies[j].R * mesh2->vertices[j1] + bodies[j].pos;
				Vector3 y2 = bodies[j].R * mesh2->vertices[j2] + bodies[j].pos;
				Vector3 y3 = bodies[j].R * mesh2->vertices[j3] + bodies[j].pos;

				// build AABB
				Vector3 yMin = vmin(y1, vmin(y2, y3));
				Vector3 yMax = vmax(y1, vmax(y2, y3));
				Aabb3 box2(yMin, yMax);
				box2.Extrude(0.5f * radTol);

				// early out
				if (!AabbOverlap3D(box1, box2))
					continue;

				// do the actual test
				btPrimitiveTriangle tri2;
				tri2.m_vertices[0] = V2BV(y1);
				tri2.m_vertices[1] = V2BV(y2);
				tri2.m_vertices[2] = V2BV(y3);
				tri2.buildTriPlane();

				if (tri1.overlap_test_conservative(tri2))
				{
					if (tri1.find_triangle_collision_clip_method(tri2, contact_data))
					{
						Vector3 n = BV2V(contact_data.m_separating_normal);
						float depth = contact_data.m_penetration_depth;
						Vector3 p = BV2V(contact_data.m_points[0]); // TODO: more points
						Vector3 a1 = !bodies[i].R * (p - bodies[i].pos);
						Vector3 a2 = !bodies[j].R * (p - bodies[j].pos);
						#pragma omp critical
						adder->AddContact(i, j, a1, a2, n, depth);
					}
				}
			}
		}
#endif
	}
}