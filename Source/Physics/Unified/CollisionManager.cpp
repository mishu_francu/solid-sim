#include "CollisionManager.h"

using namespace Geometry;

namespace Physics
{
	void CollisionManager::Detect(Bodies& bodies, Constraints& constraints, bool beforePositions)
	{
		// Update collidable positions and orientations
		for (size_t i = 0; i < mGroups.size(); i++)
		{
			mGroups[i]->UpdateCollidables(*this, bodies); // TODO: use mCollidables instead of this
		}

		// in group collision detection
		for (size_t i = 0; i < mGroups.size(); i++)
		{
			mGroups[i]->DetectCollision(bodies, mCollidables, constraints, beforePositions);
		}

		// inter group collision detection ...
		for (size_t i = 0; i < mGroups.size() - 1; i++)
		{
			for (size_t j = i + 1; j < mGroups.size(); j++)					
			{
				// dispatcher...
				for (size_t k = 0; k < mCouplers.size(); k++)
				{
					mCouplers[k]->DetectCollision(mGroups[i].get(), mGroups[j].get(), mCollidables, bodies, constraints);
				}
			}
		}
	}

	uint32 CollisionManager::AddCollidable(std::shared_ptr<Collidable> coll) 
	{
		if (coll->mType == CT_MESH)
		{
			CollisionMesh* collMesh = (CollisionMesh*)coll.get();
			collMesh->mesh->ConstructEdges();
		}
		if (coll->mType == CT_BOX)
		{
			// convert to mesh
			Box* box = (Box*)coll.get();
			Mesh& mesh = box->mesh;

			mesh.vertices.resize(8);
			mesh.vertices[0].Set(-box->D.X(), -box->D.Y(), -box->D.Z());
			mesh.vertices[1].Set(box->D.X(), -box->D.Y(), -box->D.Z());
			mesh.vertices[2].Set(box->D.X(), box->D.Y(), -box->D.Z());
			mesh.vertices[3].Set(-box->D.X(), box->D.Y(), -box->D.Z());
			mesh.vertices[4].Set(-box->D.X(), -box->D.Y(), box->D.Z());
			mesh.vertices[5].Set(box->D.X(), -box->D.Y(), box->D.Z());
			mesh.vertices[6].Set(box->D.X(), box->D.Y(), box->D.Z());
			mesh.vertices[7].Set(-box->D.X(), box->D.Y(), box->D.Z());

			bool flip = false;
			mesh.AddTriangle(0, 1, 2, flip);
			mesh.AddTriangle(0, 2, 3, flip);
			mesh.AddTriangle(1, 5, 6, flip);
			mesh.AddTriangle(1, 6, 2, flip);
			mesh.AddTriangle(5, 4, 6, flip);
			mesh.AddTriangle(4, 7, 6, flip);
			mesh.AddTriangle(0, 3, 7, flip);
			mesh.AddTriangle(0, 7, 4, flip);
			mesh.AddTriangle(3, 2, 6, flip);
			mesh.AddTriangle(3, 6, 7, flip);
			mesh.AddTriangle(0, 5, 1, flip);
			mesh.AddTriangle(0, 4, 5, flip);

			mesh.edges.push_back(Mesh::Edge(0, 1));
			mesh.edges.push_back(Mesh::Edge(1, 2));
			mesh.edges.push_back(Mesh::Edge(2, 3));
			mesh.edges.push_back(Mesh::Edge(3, 0));
			mesh.edges.push_back(Mesh::Edge(5, 6));
			mesh.edges.push_back(Mesh::Edge(6, 7));
			mesh.edges.push_back(Mesh::Edge(7, 4));
			mesh.edges.push_back(Mesh::Edge(4, 5));
			mesh.edges.push_back(Mesh::Edge(1, 5));
			mesh.edges.push_back(Mesh::Edge(2, 6));
			mesh.edges.push_back(Mesh::Edge(3, 7));
			mesh.edges.push_back(Mesh::Edge(0, 4));

			mesh.ComputeNormals();
		}
		mCollidables.push_back(coll);
		return uint32(mCollidables.size() - 1);
	}

	uint32 CollisionManagerBullet::AddCollidable(std::shared_ptr<Collidable> coll) 
	{
#ifdef BULLET_COLLISIONS
		BulletShape bts;
		if (coll->mType == CT_SPHERE) // TODO: use RTTI
		{
			const Sphere* sph = (const Sphere*)coll.get();
			bts.mBtCollObj.reset(new btCollisionObject());
			bts.mBtShape.reset(new btSphereShape(sph->radius + sph->mTolerance));
			//bts.mBtShape->setMargin(sph->mTolerance);
			bts.mBtCollObj->setCollisionShape(bts.mBtShape.get());
			bts.mBtCollObj->setUserIndex(coll->mUserIndex);
			bts.mBtCollObj->setUserPointer(coll->mGroup);
			bts.mBtCollObj->getWorldTransform().setOrigin(V2BV(coll->center));
			bts.mBtCollObj->getWorldTransform().setRotation(Q2BQ(coll->rot));

			mBtCollWorld->addCollisionObject(bts.mBtCollObj.get());
		}
		else if (coll->mType == CT_BOX)
		{
			const Box* box = (const Box*)coll.get();
			bts.mBtCollObj.reset(new btCollisionObject());
			bts.mBtShape.reset(new btBoxShape(V2BV(box->D + Vector3(box->mTolerance))));
			//bts.mBtShape->setMargin(box->mTolerance);
			bts.mBtCollObj->setCollisionShape(bts.mBtShape.get());
			bts.mBtCollObj->setUserIndex(coll->mUserIndex);
			bts.mBtCollObj->setUserPointer(coll->mGroup);
			bts.mBtCollObj->getWorldTransform().setOrigin(V2BV(coll->center));
			bts.mBtCollObj->getWorldTransform().setRotation(Q2BQ(coll->rot));

			mBtCollWorld->addCollisionObject(bts.mBtCollObj.get());
		}
		else if (coll->mType == CT_CAPSULE)
		{
			const Capsule* cap = (const Capsule*)coll.get();
			bts.mBtCollObj.reset(new btCollisionObject());
			bts.mBtShape.reset(new btCapsuleShape(cap->r, 2 * cap->hh));
			bts.mBtCollObj->setCollisionShape(bts.mBtShape.get());
			bts.mBtCollObj->setUserIndex(coll->mUserIndex);
			bts.mBtCollObj->setUserPointer(coll->mGroup);
			bts.mBtCollObj->getWorldTransform().setOrigin(V2BV(coll->center));
			bts.mBtCollObj->getWorldTransform().setRotation(Q2BQ(coll->rot));

			mBtCollWorld->addCollisionObject(bts.mBtCollObj.get());
		}
		else if (coll->mType == CT_MESH)
		{
			const CollisionMesh* collMesh = (const CollisionMesh*)coll.get();
			
			btTriangleIndexVertexArray* indexVertexArrays = new btTriangleIndexVertexArray((int)collMesh->mesh->GetNumTriangles(),
				(int*)&collMesh->mesh->indices[0], 3 * sizeof(int),
				(int)collMesh->mesh->vertices.size(),
				(btScalar*)&collMesh->mesh->vertices[0], sizeof(btScalar) * 3);
			btGImpactMeshShape* trimesh = new btGImpactMeshShape(indexVertexArrays);
			trimesh->setMargin(coll->mTolerance);
			trimesh->updateBound();

			bts.mBtCollObj.reset(new btCollisionObject());
			bts.mBtShape.reset(trimesh);
			bts.mBtCollObj->setCollisionShape(bts.mBtShape.get());			
			bts.mBtCollObj->setUserIndex(coll->mUserIndex);
			bts.mBtCollObj->setUserPointer(coll->mGroup);
			mBtCollWorld->addCollisionObject(bts.mBtCollObj.get());
		}
		mBtShapes.push_back(bts); // this is just to keep the indices the same; FIXME!
#endif
		return CollisionManager::AddCollidable(coll);
	}

	void CollisionManagerBullet::UpdateMesh(uint32 i)
	{
		if (mCollidables[i]->mType != CT_MESH)
			return;
		CollisionMesh* collMesh = (CollisionMesh*)mCollidables[i].get();

#ifdef BULLET_COLLISIONS
		auto& bts = mBtShapes[i];
		btCollisionShape* btShape = mBtShapes[i].mBtShape.get();
		if (typeid(*btShape) != typeid(btGImpactMeshShape))
			return;
		btGImpactMeshShape* trimesh = dynamic_cast<btGImpactMeshShape*>(btShape);
		if (trimesh == nullptr)
			return;

		unsigned char* vertexbase = nullptr;
		unsigned char* indexbase = nullptr;
		int numverts, stride, indexstride, numfaces;
		PHY_ScalarType type, indicestype;
		trimesh->getMeshInterface()->getLockedVertexIndexBase(&vertexbase, numverts, type, stride, &indexbase, indexstride, numfaces, indicestype);

		float minY = 1e6f;
		Vector3* ptr = (Vector3*)vertexbase;
		for (int i = 0; i < numverts; i++)
		{
			*ptr = collMesh->mesh->vertices[i] - collMesh->center;
			minY = min(minY, ptr->Y());
			ptr++;
		}

		trimesh->getMeshInterface()->unLockVertexBase(0);
		trimesh->postUpdate();
		trimesh->updateBound();
#endif
	}

	void CollisionManagerBullet::Detect(Bodies& bodies, Constraints& constraints, bool beforePositions)
	{
		// Update collidable positions and orientations
		for (size_t i = 0; i < mGroups.size(); i++)
		{
			mGroups[i]->UpdateCollidables(*this, bodies);
		}
#ifdef BULLET_COLLISIONS
		for (size_t i = 0; i < mCollidables.size(); i++)
		{
			if (mBtShapes[i].mBtCollObj)
			{
				mBtShapes[i].mBtCollObj->getWorldTransform().setOrigin(V2BV(mCollidables[i]->center));
				mBtShapes[i].mBtCollObj->getWorldTransform().setRotation(Q2BQ(mCollidables[i]->rot));
			}
		}
#endif

		// in group collision detection
		for (size_t i = 0; i < mGroups.size(); i++)
		{
			mGroups[i]->DetectCollision(bodies, mCollidables, constraints, beforePositions);
		}

#ifdef BULLET_COLLISIONS
		// Perform collision detection using Bullet
		//if (0)
		{
			mBtCollWorld->performDiscreteCollisionDetection();

			int numManifolds = mBtCollWorld->getDispatcher()->getNumManifolds();
			mContacts.clear();
			for (int i = 0; i < numManifolds; i++)
			{
				btPersistentManifold* contactManifold = mBtCollWorld->getDispatcher()->getManifoldByIndexInternal(i);
				const btCollisionObject* obA = static_cast<const btCollisionObject*>(contactManifold->getBody0());
				const btCollisionObject* obB = static_cast<const btCollisionObject*>(contactManifold->getBody1());
				contactManifold->refreshContactPoints(obA->getWorldTransform(), obB->getWorldTransform());
				int numContacts = contactManifold->getNumContacts();
				// get the body indices
				int i1 = obA->getUserIndex();
				int i2 = obB->getUserIndex();
				//For each contact point in that manifold
				for (int j = 0; j < numContacts; j++)
				{
					//Get the contact information
					btManifoldPoint& pt = contactManifold->getContactPoint(j);
					btVector3 ptA = pt.getPositionWorldOnA();
					btVector3 ptB = pt.getPositionWorldOnB();
					btVector3 normal = pt.m_normalWorldOnB;

					Contact contact;
					contact.i1 = i1;
					contact.i2 = i2;
					contact.tri1 = pt.m_index0;
					contact.tri2 = pt.m_index1;
					contact.group1 = (Group*)obA->getUserPointer();
					contact.group2 = (Group*)obB->getUserPointer();
					contact.n.Set(normal.getX(), normal.getY(), normal.getZ()); // TODO: BV2V
					// anchors are sent in world space
					contact.a1.Set(ptA.getX(), ptA.getY(), ptA.getZ());
					contact.a2.Set(ptB.getX(), ptB.getY(), ptB.getZ());
					contact.depth = -pt.getDistance();
					mContacts.push_back(contact);
				}
			}
		}
#endif

		// Send contact data to groups
		for (size_t i = 0; i < mGroups.size(); i++)
		{
			mGroups[i]->CreateContactConstraints(*this, bodies, constraints);
		}

		// Send contact data to couplers
		for (size_t k = 0; k < mCouplers.size(); k++)
		{
			mCouplers[k]->CreateContactConstraints(*this, bodies, constraints);
		}

		// inter group collision detection ...
		for (size_t i = 0; i < mGroups.size() - 1; i++)
		{
			for (size_t j = i + 1; j < mGroups.size(); j++)					
			{
				// dispatcher...
				for (size_t k = 0; k < mCouplers.size(); k++)
				{
					mCouplers[k]->DetectCollision(mGroups[i].get(), mGroups[j].get(), mCollidables, bodies, constraints);
				}
			}
		}
	}
}