#include "TriangleContacts.h"

namespace Physics
{
	void TriangleRigidContacts::SolveContactPBD(float h, Bodies& bodies)
	{
		float invH = 1.f / h;
		for (size_t i = 0; i < mTris.size(); i++)
		{
			TriangleRigidContact& contact = mTris[i];
			Vector3& v1 = bodies[contact.i1].pos;
			Vector3& v2 = bodies[contact.i2].pos;
			Vector3& v3 = bodies[contact.i3].pos;
			Vector3& v4 = bodies[contact.i4].pos;
			Vector3 p = contact.w1 * v1 + contact.w2 * v2 + contact.w3 * v3;
			Vector3 n = contact.normal;
			float len0 = contact.len;
			Vector3 a = qRotate(bodies[contact.i4].q, contact.a);
			float len = n.Dot(bodies[contact.i4].pos + a - p);
			if (len > len0)
				continue;

			Vector3 axn = cross(a, n);
			float inertia = axn.Dot(bodies[contact.i4].Iinv * axn);

			float invMass = bodies[contact.i1].invMass + bodies[contact.i2].invMass + 
				bodies[contact.i3].invMass + bodies[contact.i4].invMass + inertia;
			float s = 1.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / invMass;
			float dLambda = s * (len - len0);
			contact.lambda += invH * dLambda;
			Vector3 disp = dLambda * n;
			v1 += disp * (contact.w1 * bodies[contact.i1].invMass);
			v2 += disp * (contact.w2 * bodies[contact.i2].invMass);
			v3 += disp * (contact.w3 * bodies[contact.i3].invMass);
			v4 -= disp * (bodies[contact.i4].invMass);

			Vector3 dw = -dLambda * axn;
			bodies[contact.i4].q += 0.5f * ((bodies[contact.i4].Iinv * dw) * bodies[contact.i4].q);
			bodies[contact.i4].q = qNormalize(bodies[contact.i4].q);

			disp *= invH;
			bodies[contact.i1].vel += disp * (contact.w1 * bodies[contact.i1].invMass);
			bodies[contact.i2].vel += disp * (contact.w2 * bodies[contact.i2].invMass);
			bodies[contact.i3].vel += disp * (contact.w3 * bodies[contact.i3].invMass);
			bodies[contact.i4].vel -= disp * (bodies[contact.i4].invMass);

			bodies[contact.i4].w += bodies[contact.i4].Iinv * dw;
		}
	}
}