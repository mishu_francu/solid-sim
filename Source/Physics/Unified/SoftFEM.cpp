#include "SoftFEM.h"
#include <Engine/Profiler.h>

using namespace Geometry;

namespace Physics
{
	void SoftFEM::SetVelocity(Bodies& bodies, const Vector3& v)
	{
		for (size_t i = 0; i < mCount; i++)
		{
			bodies[i + mStart].vel = v;
		}
	}

	void SoftFEM::Init(Bodies& bodies, float young, float poisson)//, float yDamping, float pDamping)
	{
		// update tet indices
		//for (size_t i = 0; i < mTetConstraints.mTets.size(); i++)
		//{
		//	mTetConstraints.mTets[i].i[0] += mStart;
		//	mTetConstraints.mTets[i].i[1] += mStart;
		//	mTetConstraints.mTets[i].i[2] += mStart;
		//	mTetConstraints.mTets[i].i[3] += mStart;
		//}
		mTetConstraints.mStart = mStart;

		// elastic params
		mYoung = young;
		mPoisson = poisson;

		float nu = mPoisson;
		float omn = 1.f - nu;
		float om2n = 1.f - 2 * nu;
		float s = mYoung / (1.f + nu);
		float f = s / om2n;
		E = f * Matrix3(omn, nu, nu,
			nu, omn, nu,
			nu, nu, omn);

		Matrix3 Einv = E.GetInverse();
		const float mu = 0.5f * mYoung / (1.f + nu); // 0.5? same thing as s?
		const float muInv = 1.f / mu;

		Eigen::Matrix<float, 6, 6> C;
		C.setZero();
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				C(i, j) = Einv[i][j];
			}
			C(i + 3, i + 3) = muInv;
		}
		mTetConstraints.C = C;

		// damping params
		//ed = yDamping;
		//nud = pDamping;
		//float omnd = 1.f - nud;
		//float om2nd = 1.f - 2 * nud;
		//float fd = ed / (1.f + nud) / om2nd;
		//Ed = fd * Matrix3(omnd, nud, nud,
		//				nud, omnd, nud,
		//				nud, nud, omnd);

		//for (size_t i = 0; i < nodes.size(); i++)
		//{
		//	nodes[i].pos0 = nodes[i].pos;
		//}

		// init the shape matrices
		//Printf("%d tets\n", tets.size());
		for (size_t i = 0; i < mTets.size(); i++)
		{
			Tetrahedron& tet = mTets[i];
			const Vector3& x0 = bodies[tet.i[0] + mStart].pos;
			const Vector3& x1 = bodies[tet.i[1] + mStart].pos;
			const Vector3& x2 = bodies[tet.i[2] + mStart].pos;
			const Vector3& x3 = bodies[tet.i[3] + mStart].pos;
			Vector3 d1 = x1 - x0;
			Vector3 d2 = x2 - x0;
			Vector3 d3 = x3 - x0;
			Matrix3 mat(d1, d2, d3);
			tet.X = mat.GetInverse();
			tet.Xtr = !tet.X;
			tet.vol = mat.Determinant() / 6.f;

			// for linear FEM
			Vector3 y[4];
			y[1] = tet.X[0];
			y[2] = tet.X[1];
			y[3] = tet.X[2];
			y[0] = y[1] + y[2] + y[3]; 
			y[0].Flip();

			//Matrix3 Dn[4], Ds[4];
			//for (int j = 0; j < 4; j++)
			//{
			//	const Vector3& yy = y[j];
			//	Dn[j] = Matrix3(yy);
			//	Ds[j] = Matrix3(yy.Y(), 0.f, yy.Z(),
			//					yy.X(), yy.Z(), 0.f,
			//					0.f, yy.Y(), yy.X());
			//}
			//for (int j = 0; j < 4; j++)
			//{
			//	for (int k = 0; k < 4; k++)
			//	{
			//		tet.K[j][k] = tet.vol * (Dn[j] * E * Dn[k] + s * Ds[j] * (!Ds[k]));
			//	}
			//}

			// H = de/dx
			// Cauchy strain Jacobian (pre-computed terms)
			for (int j = 0; j < 4; j++)
			{
				tet.Hn[j] = Matrix3(y[j]);
				tet.Hs[j] = 0.5f * Matrix3(0, y[j].Z(), y[j].Y(),
					y[j].Z(), 0, y[j].X(),
					y[j].Y(), y[j].X(), 0);
			}
		}

		// generate collision mesh
		BuildBoundaryMesh(bodies);

		//mInitialPos = bodies[mStart].pos;
	}

	void SoftFEM::DetectCollision(Bodies& bodies, Collidables& collidables, Constraints& constraints, bool beforePos)
	{
		PROFILE_SCOPE("Soft collisions");
		mStaticContacts.Clear();

		for (size_t i = 0; i < collidables.size(); i++)
		{
			if ((collidables[i]->mType == CT_WALLS))
			{
				const Walls* walls = (const Walls*)collidables[i].get();
				WallCollisions(bodies, walls->mBox);
			}
		}

		constraints.AddList(&mStaticContacts);
	}

	void SoftFEM::WallCollisions(Bodies& bodies, const Aabb3& walls)
	{
		const float r = mThickness + mTolerance;
		//for (size_t i = mStart; i < mStart + mCount; i++)
		for (size_t j = 0; j < mCollMesh.vertices.size(); j++)
		{
			int i = mMapInv[j] + mStart;
			const Body& particle = bodies[i];
			if (particle.pos.Y() - r <= walls.min.Y())
				AddContact(i, Vector3(particle.pos.X(), walls.min.Y(), particle.pos.Z()), Vector3(0, 1, 0));
			//if (particle.pos.Y() + r >= WALL_TOP)
			//	AddContact((ParticleIdx)i, Vector3(particle.pos.X(), WALL_TOP, particle.pos.Z()), Vector3(0, -1, 0));
			if (particle.pos.X() - r <= walls.min.X())
				AddContact(i, Vector3(walls.min.X(), particle.pos.Y(), particle.pos.Z()), Vector3(1, 0, 0));
			if (particle.pos.X() + r >= walls.max.X())
				AddContact(i, Vector3(walls.max.X(), particle.pos.Y(), particle.pos.Z()), Vector3(-1, 0, 0));
			if (particle.pos.Z() - r <= walls.min.Z())
				AddContact(i, Vector3(particle.pos.X(), particle.pos.Y(), walls.min.Z()), Vector3(0, 0, 1));
			if (particle.pos.Z() + r >= walls.max.Z())
				AddContact(i, Vector3(particle.pos.X(), particle.pos.Y(), walls.max.Z()), Vector3(0, 0, -1));
		}
	}

	void TetrahedronConstraints::Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator)
	{
		Matrix3 id;
		for (size_t i = 0; i < mTets.size(); i++)
		{
			Tetrahedron& tet = mTets[i];
			Vector3 y[4];
			Matrix3 Hs[4];
			Matrix3 Hn[4];
			Vector3 d1, d2, d3;
			Matrix3 F;
			float volSqrt;
			Eigen::Matrix<float, 6, 12> J;
			Eigen::Matrix<float, 6, 1> eps;
			Eigen::Matrix<float, 6, 12> Je; // Jacobian due to strain (de/dx)
			Vector3 delV[4];
			Eigen::Matrix<float, 1, 12> gradV;
			Matrix3 strain;

			// compute deformation gradient
			const Vector3& x0 = bodies[tet.i[0] + mStart].pos;
			const Vector3& x1 = bodies[tet.i[1] + mStart].pos;
			const Vector3& x2 = bodies[tet.i[2] + mStart].pos;
			const Vector3& x3 = bodies[tet.i[3] + mStart].pos;
			d1 = x1 - x0;
			d2 = x2 - x0;
			d3 = x3 - x0;
			Matrix3 Ds(d1, d2, d3);
			float det = Ds.Determinant();
			float vol = fabs(det) / 6.f;
			volSqrt = sqrtf(vol);
			F = Ds * tet.X;
			Matrix3 Ftr = !F;

#ifdef STVK
			y[1] = tet.X[0];
			y[2] = tet.X[1];
			y[3] = tet.X[2];
			y[0] = y[1] + y[2] + y[3]; 
			y[0].Flip();
			// Green strain Jacobian
			for (int j = 0; j < 4; j++)
			{
				//Hn[j] = Hn[j] * Ftr;
				Hn[j] = Matrix3(y[j].X() * Ftr.m[0][0], y[j].X() * Ftr.m[0][1], y[j].X() * Ftr.m[0][2],
					y[j].Y() * Ftr.m[1][0], y[j].Y() * Ftr.m[1][1], y[j].Y() * Ftr.m[1][2],
					y[j].Z() * Ftr.m[2][0], y[j].Z() * Ftr.m[2][1], y[j].Z() * Ftr.m[2][2]);
				Hs[j] = tet.Hs[j] * Ftr;
			}
#else
			for (int j = 0; j < 4; j++)
			{
				Hn[j] = tet.Hn[j];
				Hs[j] = tet.Hs[j];
			}
#endif

			// compute strain
#ifndef STVK
			strain = 0.5f * (F + !F) - id; // Cauchy strain
#else
			strain = 0.5f * (Ftr * F - id); // Green strain
#endif

			delV[1] = cross(d2, d3);
			delV[2] = cross(d3, d1);
			delV[3] = cross(d1, d2);
			delV[0] = -delV[1] - delV[2] - delV[3];

			// convert to 6-vector
			Vector3 epsN(strain[0][0], strain[1][1], strain[2][2]);
			Vector3 epsS(strain[1][2], strain[0][2], strain[0][1]);
			for (int j = 0; j < 3; j++)
			{
				eps[j] = epsN[j];
				eps[j + 3] = epsS[j];
				for (int k = 0; k < 4; k++)
				{
					gradV(0, j + k * 3) = delV[k][j];
				}
			}

			for (int k = 0; k < 4; k++)
			{
				for (int l = 0; l < 3; l++)
				{
					for (int m = 0; m < 3; m++)
					{
						Je(l + 3, m + k * 3) = volSqrt * Hs[k][l][m];
						Je(l, m + k * 3) = volSqrt * Hn[k][l][m];
					}
				}
			}

			Eigen::Matrix<float, 6, 12> Jv; // Jacobian due to volume
			Jv = (0.5f / (6.f * volSqrt)) * eps * gradV;

			J = Je + Jv; // Jv adds instability

			Eigen::Matrix<float, 6, 1> c = volSqrt * eps;
			Eigen::Matrix<float, 6, 6> S;
			auto Jtr = J.transpose();
			Eigen::Matrix<float, 12, 12> W;
			W.setZero();
			W(0, 0) = W(1, 1) = W(2, 2) = bodies[tet.i[0] + mStart].invMass;
			W(3, 3) = W(4, 4) = W(5, 5) = bodies[tet.i[1] + mStart].invMass;
			W(6, 6) = W(7, 7) = W(8, 8) = bodies[tet.i[2] + mStart].invMass;
			W(9, 9) = W(10, 10) = W(11, 11) = bodies[tet.i[3] + mStart].invMass;
			Eigen::Matrix<float, 6, 6> A = J * W * Jtr;

			// compute constraint
			S = h * h * A + C;

			Eigen::VectorXf f, lambda;
			lambda = S.llt().solve(c);
			f = Jtr * lambda;

			// apply force
			for (int k = 0; k < 4; k++)
			{
				int nod = tet.i[k] + mStart;
				if (bodies[nod].invMass == 0)
					continue;
				int base = k * 3;
				Vector3 fn(f[base], f[base + 1], f[base + 2]);
				bodies[nod].vel -= (h * bodies[nod].invMass) * fn;
				bodies[nod].pos -= (h * h * bodies[nod].invMass) * fn;
			}
		}
	}

	struct Face
	{
		uint16 i, j, k;
		int elem;
		int face;

		Face(uint16 a, uint16 b, uint16 c, int e, int f) : elem(e), face(f)
		{
			i = min(a, min(b, c));
			k = max(a, max(b, c));
			j = a + b + c - i - k;
		}
	};

	bool CompareFaces(const Face& t1, const Face& t2)
	{
		if (t1.i < t2.i)
			return true;
		if (t1.i == t2.i)
		{
			if (t1.j < t2.j)
				return true;
			if (t1.j == t2.j && t1.k < t2.k)
				return true;
		}
		return false;
	}

	void SoftFEM::BuildBoundaryMesh(const Bodies& bodies)
	{
		std::vector<Face> faces;
		for (size_t ii = 0; ii < mTetConstraints.mTets.size(); ii++)
		{
			uint16 i = mTetConstraints.mTets[ii].i[0];
			uint16 j = mTetConstraints.mTets[ii].i[1];
			uint16 k = mTetConstraints.mTets[ii].i[2];
			uint16 l = mTetConstraints.mTets[ii].i[3];

			faces.push_back(Face(j, k, l, ii, 0));
			faces.push_back(Face(i, k, l, ii, 1));
			faces.push_back(Face(i, j, l, ii, 2));
			faces.push_back(Face(i, j, k, ii, 3));
		}

		std::sort(faces.begin(), faces.end(), CompareFaces);

		std::vector<Face> boundary;
		std::vector<uint16> boundaryVertices;
		for (size_t i = 0; i < faces.size(); i++)
		{
			if (i < faces.size() - 1 &&
				faces[i].i == faces[i + 1].i &&
				faces[i].j == faces[i + 1].j &&
				faces[i].k == faces[i + 1].k)
			{
				// if the next triangle is the same then this is clearly not a boundary triangle
				i++;
				continue;
			}
			boundary.push_back(faces[i]);
			boundaryVertices.push_back(faces[i].i);
			boundaryVertices.push_back(faces[i].j);
			boundaryVertices.push_back(faces[i].k);
		}

		// identify unique boundary vertices
		std::sort(boundaryVertices.begin(), boundaryVertices.end());

		std::vector<Vector3> vertices;
		mMap.resize(mCount);
		std::fill(mMap.begin(), mMap.end(), -1);
		for (size_t i = 0; i < boundaryVertices.size(); i++)
		{
			if (i > 0 && boundaryVertices[i] == boundaryVertices[i - 1])
				continue;
			vertices.push_back(bodies[boundaryVertices[i] + mStart].pos);
			mMap[boundaryVertices[i]] = vertices.size() - 1;
			mMapInv.push_back(boundaryVertices[i]);
		}

		// generate mesh
		mCollMesh.vertices = vertices;

		// add the triangles
		for (size_t idx = 0; idx < boundary.size(); idx++)
		{
			// use the 4th vertex in the tet to identify which way is inside (for triangle winding)
			int ii = boundary[idx].elem;
			uint16 i = mTetConstraints.mTets[ii].i[0];
			uint16 j = mTetConstraints.mTets[ii].i[1];
			uint16 k = mTetConstraints.mTets[ii].i[2];
			uint16 l = mTetConstraints.mTets[ii].i[3];

			if (boundary[idx].face == 0)
			{				
				const Vector3& v1 = bodies[j + mStart].pos;
				const Vector3& v2 = bodies[k + mStart].pos;
				const Vector3& v3 = bodies[l + mStart].pos;
				const Vector3& v4 = bodies[i + mStart].pos;
				Vector3 n = cross(v2 - v1, v3 - v1);
				bool flip = n.Dot(v4 - v1) > 0;
				mCollMesh.AddTriangle(mMap[j], mMap[k], mMap[l], flip);
			}
			if (boundary[idx].face == 1)
			{				
				const Vector3& v1 = bodies[i + mStart].pos;
				const Vector3& v2 = bodies[k + mStart].pos;
				const Vector3& v3 = bodies[l + mStart].pos;
				const Vector3& v4 = bodies[j + mStart].pos;
				Vector3 n = cross(v2 - v1, v3 - v1);
				bool flip = n.Dot(v4 - v1) > 0;
				mCollMesh.AddTriangle(mMap[i], mMap[k], mMap[l], flip);
			}
			if (boundary[idx].face == 2)
			{				
				const Vector3& v1 = bodies[i + mStart].pos;
				const Vector3& v2 = bodies[j + mStart].pos;
				const Vector3& v3 = bodies[l + mStart].pos;
				const Vector3& v4 = bodies[k + mStart].pos;
				Vector3 n = cross(v2 - v1, v3 - v1);
				bool flip = n.Dot(v4 - v1) > 0;
				mCollMesh.AddTriangle(mMap[i], mMap[j], mMap[l], flip);
			}
			if (boundary[idx].face == 3)
			{				
				const Vector3& v1 = bodies[i + mStart].pos;
				const Vector3& v2 = bodies[j + mStart].pos;
				const Vector3& v3 = bodies[k + mStart].pos;
				const Vector3& v4 = bodies[l + mStart].pos;
				Vector3 n = cross(v2 - v1, v3 - v1);
				bool flip = n.Dot(v4 - v1) > 0;
				mCollMesh.AddTriangle(mMap[i], mMap[j], mMap[k], flip);
			}
		}
	}

	void SoftFEM::UpdateCollisionMesh(const Bodies& bodies)
	{
		for (size_t i = 0; i < mCount; i++)
		{
			if (mMap[i] < 0)
				continue;
			mCollMesh.vertices[mMap[i]] = bodies[i + mStart].pos;
		}
		mCollMesh.ComputeNormals();
	}
}