#include "StaticContacts.h"

namespace Physics
{
	void StaticContacts::Prepare(Bodies& bodies, PhaseType phase)
	{
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			StaticContact& contact = mContacts[i];
			//if (phase == PT_VELOCITY)
			{
				const Body& p = bodies[contact.idx];
				contact.depth = contact.len - contact.normal.Dot(p.pos - contact.point);
			}

			contact.t1 = contact.normal.Perpendicular();
			contact.t2 = contact.normal.Cross(contact.t1);
			contact.t2.Normalize();

			//if (phase != PT_POSTPROCESS)
				contact.lambda = 0.f;
			contact.lambdaF1 = 0.f;
			contact.lambdaF2 = 0.f;

			contact.impulse.SetZero();
		}

		mTheta = 1;
	}

	void StaticContacts::Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator)
	{
		if (phase == PT_VELOCITY)
		{
			SolveContactAndFrictionVTS(h, bodies);
			//SolveContactVTS(h, bodies);
			//SolveFrictionVTS(h, bodies);
		}
		if (phase == PT_POSITION && (mFlags & SF_NO_PBD_CONTACT) == 0)
		{
			if ((mFlags & SF_PBD_CONTACT_ONLY) != 0)
			{
				SolveContactPBD(h, bodies);
			}
			else if ((mFlags & SF_STAGGERED) == 0)
			{
				SolveContactAndFrictionPBD(h, bodies);
				//SolveContactDampedPBD(h, bodies);
				//SolveContactNonlinearVTS(h, bodies);
			}
			else
			{
				SolveContactPBD(h, bodies);
				SolveFrictionPBD(h, bodies);
			}
		}
		if (phase == PT_POSTPROCESS)
		{
			if ((mFlags & SF_POST_FRICTION_ONLY) != 0)
				SolveFrictionVTS(h, bodies);
			else
			{
				SolveContactAndFrictionVTS(h, bodies);
				//SolveContactVTS(h, bodies);			
			}
		}
	}

}