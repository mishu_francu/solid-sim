#ifndef ICOUPLER_H
#define ICOUPLER_H

namespace Physics
{
	class Group;
	//class Collidables;
	//class Bodies;
	class Constraints;
	class CollisionManager;

	struct ICoupler
	{
		virtual void DetectCollision(Group* group1, Group* group2, Collidables& collidables, Bodies& bodies, Constraints& constraints) = 0;
		virtual void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints) { }
	};
}

#endif // ICOUPLER_H