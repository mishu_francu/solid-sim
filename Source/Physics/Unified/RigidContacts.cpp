#include "RigidContacts.h"

//#define MAG_PROJ
//#define CYLINDER_PROJ
#define BOX_PROJ
//#define CONE_PROJ

namespace Physics
{
	float RigidContacts::CalculateStats(Bodies& bodies)
	{
		float normSqr = 0;
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			RigidContact& pair = mContacts[i];
			Body* p1 = &bodies[pair.i];
			Body* p2 = &bodies[pair.j];

			Vector3 a1 = p1->R * pair.a1;
			Vector3 a2 = p2->R * pair.a2;

			float error = pair.n.Dot(p1->pos + a1 - p2->pos - a2);
			//normSqr += error * error;

			Vector3 v1 = p1->vel + cross(p1->w, a1);
			Vector3 v2 = p2->vel + cross(p2->w, a2);
			Vector3 v12 = v2 - v1;
			float vnrel = pair.n.Dot(v12);

			normSqr += vnrel * vnrel;
		}
		return normSqr;
	}

	void RigidContacts::SolveAcc(float h, Bodies& bodies, PhaseType phase)
	{
		if (phase == PT_POSITION)
		{
			float theta0 = mTheta;
			mTheta = 0.5f * (-mTheta * mTheta + mTheta * sqrt(mTheta * mTheta + 4));
			float beta = theta0 * (1 - theta0) / (theta0 * theta0 + mTheta);

			//SolveContactPBDAcc(h, bodies);
			SolveContactAndFrictionPBDAcc(h, bodies, beta);
		}
	}

	void RigidContacts::Prepare(Bodies& bodies, PhaseType phase)
	{
		mTheta = 1.f;
		if (phase == PhaseType::PT_POSTPROCESS)
			return;
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			// compute tangent frame for box friction
			mContacts[i].t1 = mContacts[i].n.Perpendicular();
			mContacts[i].t2 = mContacts[i].n.Cross(mContacts[i].t1);
			mContacts[i].t2.Normalize();

			mContacts[i].lambda = 0;
			mContacts[i].lambdaF1 = 0;
			mContacts[i].lambdaF2 = 0;

			mContacts[i].imp.SetZero();
			mContacts[i].dw1.SetZero();
			mContacts[i].dw2.SetZero();
		}
	}

	void RigidContacts::SolveContactAndFrictionVTS(float h, Bodies& bodies)
	{
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			RigidContact& pair = mContacts[i];
			Body* p1 = &bodies[pair.i]; // TODO: ref
			Body* p2 = &bodies[pair.j];

			Vector3 a1 = p1->R * pair.a1;
			Vector3 a2 = p2->R * pair.a2;
			Vector3 v1 = p1->vel + cross(p1->w, a1);
			Vector3 v2 = p2->vel + cross(p2->w, a2);
			Vector3 v12 = v2 - v1;
			float vnrel = pair.n.Dot(v12); // TODO: precompute axn

			Vector3 a1xn = cross(a1, pair.n);
			Vector3 a2xn = cross(a2, pair.n);
			float inertia1 = a1xn.Dot(p1->Iinv * a1xn);
			float inertia2 = a2xn.Dot(p2->Iinv * a2xn);

			float sum = (p1->invMass + p2->invMass + inertia1 + inertia2);
			float dLambda = (vnrel + mBeta * pair.depth / h) / sum;

			float lambda0 = pair.lambda;
			pair.lambda = max(0.f, lambda0 + dLambda);			

			// TODO: for cylinder projection, can update the normal velocities here

			// friction
			float limit = mFriction * pair.lambda;
#ifdef MAG_PROJ
			Vector3 vt = v12 - vnrel * pair.n;
			float vtrel = vt.Length();
			if (vtrel > 0.0001f) // TODO: magic number, else branch
			{
				// compute diagonal term
				vt.Scale(1.f / vtrel); // normalize
				Vector3 a1xt = cross(a1, vt); // TODO: precompute
				Vector3 a2xt = cross(a2, vt);
				inertia1 = a1xt.Dot(p1->Iinv * a1xt);
				inertia2 = a2xt.Dot(p2->Iinv * a2xt);
				sum = p1->invMass + p2->invMass + inertia1 + inertia2;

				float dLambdaF = vtrel / sum;
				float lambdaF0 = pair.lambdaF1;
#ifdef CYLINDER_PROJ
				pair.lambdaF1 = min(limit, lambdaF0 + dLambdaF);
#else // magnitude cone projection
				pair.lambdaF1 = lambdaF0 + dLambdaF;
				if (pair.lambdaF1 >= limit)
				{
					pair.lambda = (pair.lambda + mFriction * pair.lambdaF1) / (mFriction * mFriction + 1);
					dLambda = pair.lambda - lambda0;
					pair.lambdaF1 = mFriction * pair.lambda;
				}
#endif
				dLambdaF = pair.lambdaF1 - lambdaF0;

				Vector3 p = dLambdaF * vt;
				p1->vel += p1->invMass * p;
				p2->vel -= p2->invMass * p;

				p1->w += p1->Iinv * (dLambdaF * a1xt);
				p2->w -= p2->Iinv * (dLambdaF * a2xt);
			}
#elif defined(BOX_PROJ)
			float vtrel = v12.Dot(pair.t1);
			{
				Vector3 a1xt = cross(a1, pair.t1);
				Vector3 a2xt = cross(a2, pair.t1);
				float inertia1 = a1xt.Dot(p1->Iinv * a1xt);
				float inertia2 = a2xt.Dot(p2->Iinv * a2xt);
				float sum = (p1->invMass + p2->invMass + inertia1 + inertia2);

				float dLambdaF = vtrel / sum;
				float lambdaF0 = pair.lambdaF1;
				pair.lambdaF1 = lambdaF0 + dLambdaF;
				pair.lambdaF1 = clamp(pair.lambdaF1, -limit, limit);
				dLambdaF = pair.lambdaF1 - lambdaF0;

				Vector3 vt = dLambdaF * pair.t1;
				p1->vel += vt * p1->invMass;
				p2->vel -= vt * p2->invMass;

				p1->w += p1->Iinv * (dLambdaF * a1xt);
				p2->w -= p2->Iinv * (dLambdaF * a2xt);
			}

			vtrel = v12.Dot(pair.t2);
			{
				Vector3 a1xt = cross(a1, pair.t2);
				Vector3 a2xt = cross(a2, pair.t2);
				float inertia1 = a1xt.Dot(p1->Iinv * a1xt);
				float inertia2 = a2xt.Dot(p2->Iinv * a2xt);
				float sum = (p1->invMass + p2->invMass + inertia1 + inertia2);

				float dLambdaF = vtrel / sum;
				float lambdaF0 = pair.lambdaF2;
				pair.lambdaF2 = lambdaF0 + dLambdaF;
				pair.lambdaF2 = clamp(pair.lambdaF2, -limit, limit);
				dLambdaF = pair.lambdaF2 - lambdaF0;

				Vector3 vt = dLambdaF * pair.t2;
				p1->vel += vt * p1->invMass;
				p2->vel -= vt * p2->invMass;

				p1->w += p1->Iinv * (dLambdaF * a1xt);
				p2->w -= p2->Iinv * (dLambdaF * a2xt);
			}
#elif defined(CONE_PROJ)
			// first compute the 2 tangential multipliers
			float lambdaF10 = pair.lambdaF1;
			Vector3 a1xt1 = cross(a1, pair.t1);
			Vector3 a2xt1 = cross(a2, pair.t1);
			float vtrel = v12.Dot(pair.t1);
			{
				float inertia1 = a1xt1.Dot(p1->Iinv * a1xt1);
				float inertia2 = a2xt1.Dot(p2->Iinv * a2xt1);
				float sum = (p1->invMass + p2->invMass + inertia1 + inertia2);

				float dLambdaF = vtrel / sum;
				pair.lambdaF1 = lambdaF10 + dLambdaF;
			}

			vtrel = v12.Dot(pair.t2);
			Vector3 a1xt2 = cross(a1, pair.t2);
			Vector3 a2xt2 = cross(a2, pair.t2);
			float lambdaF20 = pair.lambdaF2;
			{
				float inertia1 = a1xt2.Dot(p1->Iinv * a1xt2);
				float inertia2 = a2xt2.Dot(p2->Iinv * a2xt2);
				float sum = (p1->invMass + p2->invMass + inertia1 + inertia2);

				float dLambdaF = vtrel / sum;
				pair.lambdaF2 = lambdaF20 + dLambdaF;
			}

			// perform cone projection
			float gr = sqrtf(pair.lambdaF1 * pair.lambdaF1 + pair.lambdaF2 * pair.lambdaF2);
			if (gr > limit)
			{
				pair.lambda = (gr * mFriction + pair.lambda) / (mFriction * mFriction + 1);
				float pr = mFriction * pair.lambda;
				float s = pr / gr;
				pair.lambdaF1 *= s;
				pair.lambdaF2 *= s;
			}
			
			// apply tangential impulses
			float dLambdaF1 = pair.lambdaF1 - lambdaF10;
			float dLambdaF2 = pair.lambdaF2 - lambdaF20;
			
			Vector3 vt = dLambdaF1 * pair.t1 + dLambdaF2 * pair.t2;
			p1->vel += vt * p1->invMass;
			p2->vel -= vt * p2->invMass;

			p1->w += p1->Iinv * (dLambdaF1 * a1xt1 + dLambdaF2 * a1xt2);
			p2->w -= p2->Iinv * (dLambdaF1 * a2xt1 + dLambdaF2 * a2xt2);
#endif
			
			// apply normal impulse
			dLambda = pair.lambda - lambda0;
			Vector3 disp = dLambda * pair.n;
			p1->vel += p1->invMass * disp;
			p2->vel -= p2->invMass * disp;

			p1->w += p1->Iinv * (dLambda * a1xn);
			p2->w -= p2->Iinv * (dLambda * a2xn);
		}
	}

	void RigidContacts::SolveFrictionVTS(float h, Bodies& bodies)
	{
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			RigidContact& pair = mContacts[i];
			Body* p1 = &bodies[pair.i]; // TODO: ref
			Body* p2 = &bodies[pair.j];

			Vector3 a1 = p1->R * pair.a1;
			Vector3 a2 = p2->R * pair.a2;
			Vector3 v1 = p1->vel + cross(p1->w, a1);
			Vector3 v2 = p2->vel + cross(p2->w, a2);
			Vector3 v12 = v2 - v1;
			float vnrel = pair.n.Dot(v12); // TODO: precompute axn

			// friction
			float limit = mFriction * pair.lambda / h; // temp hack for running after PBD (division by h)
			Vector3 vt = v12 - vnrel * pair.n;
			float vtrel = vt.Length();
			if (vtrel > 0.0001f) // TODO: magic number, else branch
			{
				// compute diagonal term
				vt.Scale(1.f / vtrel); // normalize
				Vector3 a1xt = cross(a1, vt); // TODO: precompute
				Vector3 a2xt = cross(a2, vt);
				float inertia1 = a1xt.Dot(p1->Iinv * a1xt);
				float inertia2 = a2xt.Dot(p2->Iinv * a2xt);
				float sum = p1->invMass + p2->invMass + inertia1 + inertia2;

				float dLambda = vtrel / sum;
				float lambda0 = pair.lambdaF1;
				pair.lambdaF1 = min(limit, lambda0 + dLambda);
				dLambda = pair.lambdaF1 - lambda0;

				Vector3 p = dLambda * vt;
				p1->vel += p1->invMass * p;
				p2->vel -= p2->invMass * p;

				p1->w += p1->Iinv * (dLambda * a1xt);
				p2->w -= p2->Iinv * (dLambda * a2xt);
			}
		}
	}

	void RigidContacts::SolveContactAndFrictionPBD(float h, Bodies& bodies)
	{
		float invH = 1.f / h;
		float normSqr = 0;
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			RigidContact& pair = mContacts[i];
			Body* p1 = &bodies[pair.i];
			Body* p2 = &bodies[pair.j];

			// TODO: update orientation here?

			// world contact positions
			Vector3 a1 = p1->R * pair.a1;
			Vector3 a2 = p2->R * pair.a2;

			Vector3 v1 = p1->vel + cross(p1->w, a1);
			Vector3 v2 = p2->vel + cross(p2->w, a2);
			Vector3 v12 = v2 - v1;
			float vnrel = pair.n.Dot(v12);

			// normal part of inertia
			Vector3 a1xn = cross(a1, pair.n);
			Vector3 a2xn = cross(a2, pair.n);
			float inertia1 = a1xn.Dot(p1->IinvPos * a1xn);
			float inertia2 = a2xn.Dot(p2->IinvPos * a2xn);
			float inertia = p1->invMass + p2->invMass + inertia1 + inertia2;

			// compute and clamp lambda
			//float dLambda = (-mStiffness * pair.n.Dot(p1->pos + a1 - p2->pos - a2) + h * mDamping * vnrel) / inertia;
			float error = pair.n.Dot(p1->pos + a1 - p2->pos - a2);
			normSqr += error * error;
			float dLambda = h * h * (-error - h * mDamping * vnrel) / (inertia * h * (h + mDamping) + mEpsilon);
			float lambda0 = pair.lambda;
			pair.lambda = max(0.f, lambda0 + dLambda);
			dLambda = pair.lambda - lambda0;

			// integrate using lambda
			Vector3 imp = dLambda * pair.n;
			Vector3 dw1 = dLambda * a1xn;
			Vector3 dw2 = -dLambda * a2xn;

			// friction
			float limit = mFriction * invH * pair.lambda;
#ifdef MAG_PROJ
			Vector3 vt = v12 - vnrel * pair.n;
			float vtrel = vt.Length();
			if (mFriction != 0 && vtrel > 0.0001f) // TODO: handle this better
			{
				// compute diagonal term
				vt.Scale(1.f / vtrel); // normalize
				Vector3 a1xt = cross(a1, vt); // TODO: update a1 or run friction first?
				Vector3 a2xt = cross(a2, vt);
				inertia1 = a1xt.Dot(p1->IinvPos * a1xt);
				inertia2 = a2xt.Dot(p2->IinvPos * a2xt);
				inertia = p1->invMass + p2->invMass + inertia1 + inertia2;

				float dLambdaF = vtrel / inertia;
				float lambdaF0 = pair.lambdaF1;
#ifdef CYLINDER_PROJ
				pair.lambdaF1 = min(limit, lambdaF0 + dLambdaF);
#else // CONE_PROJ
				pair.lambdaF1 = lambdaF0 + dLambdaF;
				if (pair.lambdaF1 >= limit)
				{
					pair.lambda = (pair.lambda + h * mFriction * pair.lambdaF1) / (mFriction * mFriction + 1);
					dLambda = pair.lambda - lambda0;
					pair.lambdaF1 = mFriction * invH * pair.lambda;
				}
#endif
				dLambdaF = pair.lambdaF1 - lambdaF0;

				dLambdaF *= h;
				imp = dLambda * pair.n + dLambdaF * vt;
				dw1 = dLambda * a1xn + dLambdaF * a1xt;
				dw2 = -dLambda * a2xn - dLambdaF * a2xt;;
			}
#elif defined(BOX_PROJ)
			if (mFriction != 0)
			{
				float vtrel = v12.Dot(pair.t1);
				Vector3 a1xt1 = cross(a1, pair.t1);
				Vector3 a2xt1 = cross(a1, pair.t1);
				inertia1 = a1xt1.Dot(p1->IinvPos * a1xt1);
				inertia2 = a2xt1.Dot(p2->IinvPos * a2xt1);
				inertia = p1->invMass + p2->invMass + inertia1 + inertia2;
				float dLambdaF1 = vtrel / inertia;
				float lambdaF0 = pair.lambdaF1;
				pair.lambdaF1 = lambdaF0 + dLambdaF1;
				pair.lambdaF1 = clamp(pair.lambdaF1, -limit, limit);
				dLambdaF1 = pair.lambdaF1 - lambdaF0;

				vtrel = v12.Dot(pair.t2);
				Vector3 a1xt2 = cross(a1, pair.t2);
				Vector3 a2xt2 = cross(a1, pair.t2);
				inertia1 = a1xt1.Dot(p1->IinvPos * a1xt2);
				inertia2 = a2xt1.Dot(p2->IinvPos * a2xt2);
				inertia = p1->invMass + p2->invMass + inertia1 + inertia2;
				float dLambdaF2 = vtrel / inertia;
				lambdaF0 = pair.lambdaF2;
				pair.lambdaF2 = lambdaF0 + dLambdaF2;
				pair.lambdaF2 = clamp(pair.lambdaF2, -limit, limit);
				dLambdaF2 = pair.lambdaF2 - lambdaF0;

				imp = dLambda * pair.n + h * dLambdaF1 * pair.t1 + h * dLambdaF2 * pair.t2;
				dw1 = dLambda * a1xn + p1->Iinv * (h * (dLambdaF1 * a1xt1 + dLambdaF2 * a1xt2));
				dw2 = -dLambda * a2xn - p2->Iinv * (h * (dLambdaF1 * a2xt1 + dLambdaF2 * a2xt2));
			}
#elif defined(CONE_PROJ)
			// first compute the 2 tangential multipliers
			float lambdaF10 = pair.lambdaF1;
			Vector3 a1xt1 = cross(a1, pair.t1);
			Vector3 a2xt1 = cross(a2, pair.t1);
			float vtrel = v12.Dot(pair.t1);
			{
				float inertia1 = a1xt1.Dot(p1->Iinv * a1xt1);
				float inertia2 = a2xt1.Dot(p2->Iinv * a2xt1);
				float sum = (p1->invMass + p2->invMass + inertia1 + inertia2);

				float dLambdaF = vtrel / sum;
				pair.lambdaF1 = lambdaF10 + dLambdaF;
			}

			vtrel = v12.Dot(pair.t2);
			Vector3 a1xt2 = cross(a1, pair.t2);
			Vector3 a2xt2 = cross(a2, pair.t2);
			float lambdaF20 = pair.lambdaF2;
			{
				float inertia1 = a1xt2.Dot(p1->Iinv * a1xt2);
				float inertia2 = a2xt2.Dot(p2->Iinv * a2xt2);
				float sum = (p1->invMass + p2->invMass + inertia1 + inertia2);

				float dLambdaF = vtrel / sum;
				pair.lambdaF2 = lambdaF20 + dLambdaF;
			}

			// perform cone projection
			float gr = sqrtf(pair.lambdaF1 * pair.lambdaF1 + pair.lambdaF2 * pair.lambdaF2);
			if (gr > limit)
			{
				pair.lambda = (h * gr * mFriction + pair.lambda) / (mFriction * mFriction + 1);
				float pr = mFriction * pair.lambda;
				float s = pr / (gr * h);
				pair.lambdaF1 *= s;
				pair.lambdaF2 *= s;
			}

			// calculate impulses
			dLambda = pair.lambda - lambda0;
			float dLambdaF1 = pair.lambdaF1 - lambdaF10;
			float dLambdaF2 = pair.lambdaF2 - lambdaF20;
			imp = dLambda * pair.n + h * dLambdaF1 * pair.t1 + h * dLambdaF2 * pair.t2;
			dw1 = dLambda * a1xn + p1->Iinv * h * (dLambdaF1 * a1xt1 + dLambdaF2 * a1xt2);
			dw2 = -dLambda * a2xn - p2->Iinv * h * (dLambdaF1 * a2xt1 + dLambdaF2 * a2xt2);
#endif

			// rolling friction
			//Vector3 wr = p2->w - p1->w;
			//Vector3 wt = wr - pair.n.Dot(wr) * pair.n;
			//float wtrel = wt.Length();
			//if (wtrel > 0.0001f)
			//{
			//	// compute diagonal term
			//	wt.Scale(1.f / wtrel); // normalize
			//	//Vector3 a1xt = cross(a1, vt); // TODO: update a1 or run friction first?
			//	//Vector3 a2xt = cross(a2, vt);
			//	//inertia1 = a1xt.Dot(p1->Iinv * a1xt);
			//	//inertia2 = a2xt.Dot(p2->Iinv * a2xt);
			//	inertia = p1->invMass + p2->invMass;// + inertia1 + inertia2;

			//	dLambda = wtrel;// / inertia;
			//	lambda0 = pair.lambdaT;
			//	pair.lambdaT = min(0.2f * invH * pair.lambda, lambda0 + dLambda);
			//	dLambda = pair.lambdaT - lambda0;

			//	dLambda *= h;
			//	//imp += dLambda * vt;
			//	dw1 += dLambda * wt;
			//	dw2 += -dLambda * wt;
			//}

			// rigid 1
			p1->vel += invH * p1->invMass * imp;
			p1->w += invH * p1->Iinv * dw1;

			p1->pos += p1->invMass * imp;
			p1->q += 0.5f * ((p1->Iinv * dw1) * p1->q);
			p1->q = qNormalize(p1->q);

			//p1->R = qToMatrix(p1->q);
			//p1->Iinv = p1->R * p1->Icinv * !p1->R;
			p1->UpdateRotationalPart();

			// rigid 2
			p2->vel -= invH * p2->invMass * imp;
			p2->w += invH * p2->Iinv * dw2;

			p2->pos -= p2->invMass * imp;
			p2->q += 0.5f * ((p2->Iinv * dw2) * p2->q);
			p2->q = qNormalize(p2->q);

			//p2->R = qToMatrix(p2->q);
			//p2->Iinv = p2->R * p2->Icinv * !p2->R;
			p2->UpdateRotationalPart();
		}
	}

	void RigidContacts::SolveContactPBD(float h, Bodies& bodies)
	{
		float invH = 1.f / h;
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			RigidContact& pair = mContacts[i];
			Body* p1 = &bodies[pair.i];
			Body* p2 = &bodies[pair.j];

			// TODO: update orientation here?

			// world contact positions
			Vector3 a1 = p1->R * pair.a1;
			Vector3 a2 = p2->R * pair.a2;

			// normal part of inertia
			Vector3 a1xn = cross(a1, pair.n);
			Vector3 a2xn = cross(a2, pair.n);
			float inertia1 = a1xn.Dot(p1->Iinv * a1xn);
			float inertia2 = a2xn.Dot(p2->Iinv * a2xn);
			float inertia = p1->invMass + p2->invMass + inertia1 + inertia2;

			// compute and clamp lambda
			float err = pair.n.Dot(p1->pos + a1 - p2->pos - a2);
			float dLambda = - err / inertia;
			float lambda0 = pair.lambda;
			pair.lambda = max(0.f, lambda0 + dLambda);
			dLambda = pair.lambda - lambda0;

			//Printf("%f\n", err);

			// integrate using lambda
			Vector3 imp = dLambda * pair.n;
			Vector3 dw1 = dLambda * a1xn;
			Vector3 dw2 = -dLambda * a2xn;

			// rigid 1
			p1->vel += invH * p1->invMass * imp;
			p1->w += invH * p1->Iinv * dw1;

			p1->pos += p1->invMass * imp;
			p1->q += 0.5f * ((p1->Iinv * dw1) * p1->q);
			p1->q = qNormalize(p1->q);

			p1->R = qToMatrix(p1->q);
			p1->Iinv = p1->R * p1->Icinv * !p1->R;

			// rigid 2
			p2->vel -= invH * p2->invMass * imp;
			p2->w += invH * p2->Iinv * dw2;

			p2->pos -= p2->invMass * imp;
			p2->q += 0.5f * ((p2->Iinv * dw2) * p2->q);
			p2->q = qNormalize(p2->q);

			p2->R = qToMatrix(p2->q);
			p2->Iinv = p2->R * p2->Icinv * !p2->R;
		}
	}

	void RigidContacts::SolveContactAndFrictionPBDAcc(float h, Bodies& bodies, float beta)
	{
		float invH = 1.f / h;
		float omega = 0.1f;
		float normSqr = 0;
		#pragma omp parallel for
		for (int i = 0; i < (int)mContacts.size(); i++)
		{
			RigidContact& pair = mContacts[i];
			Body* p1 = &bodies[pair.i];
			Body* p2 = &bodies[pair.j];				

			// world contact positions
			Vector3 a1 = p1->R * pair.a1;
			Vector3 a2 = p2->R * pair.a2;

			// normal part of inertia
			Vector3 a1xn = cross(a1, pair.n);
			Vector3 a2xn = cross(a2, pair.n);
			float inertia1 = a1xn.Dot(p1->IinvPos * a1xn);
			float inertia2 = a2xn.Dot(p2->IinvPos * a2xn);
			float inertia = p1->invMass + p2->invMass + inertia1 + inertia2;

			// compute and clamp lambda
			float error = pair.n.Dot(p1->pos + a1 - p2->pos - a2);
			normSqr += error * error;
			float dLambda = -omega * error / inertia;
			float lambda0 = pair.lambda;
			pair.lambda = max(0.f, lambda0 + dLambda);
			dLambda = pair.lambda - lambda0;

			Vector3 oldImp = pair.imp;
			Vector3 oldDw1 = pair.dw1;
			Vector3 oldDw2 = pair.dw2;

			pair.imp = dLambda * pair.n;
			pair.dw1 = dLambda * a1xn;
			pair.dw2 = -dLambda * a2xn;

			Vector3 v1 = p1->vel + cross(p1->w, a1);
			Vector3 v2 = p2->vel + cross(p2->w, a2);
			Vector3 v12 = v2 - v1;
			float vnrel = pair.n.Dot(v12);

			// friction
			Vector3 vt = v12 - vnrel * pair.n;
			float vtrel = vt.Length();

			// TODO: the other friction models
			float limit = mFriction * invH * pair.lambda;
			if (vtrel > Constants::slipEps)
			{
				// compute diagonal term
				vt.Scale(1.f / vtrel); // normalize
				Vector3 a1xt = cross(a1, vt); // TODO: update a1 or run friction first?
				Vector3 a2xt = cross(a2, vt);
				inertia1 = a1xt.Dot(p1->IinvPos * a1xt);
				inertia2 = a2xt.Dot(p2->IinvPos * a2xt);
				inertia = p1->invMass + p2->invMass + inertia1 + inertia2;

				dLambda = omega * vtrel / inertia;
				lambda0 = pair.lambdaF1;
				pair.lambdaF1 = min(limit, lambda0 + dLambda);
				dLambda = pair.lambdaF1 - lambda0;

				dLambda *= h;
				pair.imp += dLambda * vt;
				pair.dw1 += dLambda * a1xt;
				pair.dw2 += -dLambda * a2xt;
			}

			pair.imp += beta * oldImp;
			pair.dw1 += beta * oldDw1;
			pair.dw2 += beta * oldDw2;
		}
	}


}