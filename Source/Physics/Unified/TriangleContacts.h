#ifndef TRIANGLE_CONTACTS_H
#define TRIANGLE_CONTACTS_H

#include <Math/Vector3.h>
#include <Engine/Types.h>
#include "IConstraintList.h"

namespace Physics
{
	struct TriangleContact
	{
		Math::Vector3 normal;
		Math::Vector3 disp, dw; // for Jacobi
		uint32 i1, i2, i3, i4;
		float w1, w2, w3;
		float len;
		float lambda, lambdaF1;
	};

	class TriangleContacts : public IConstraintList
	{
	public:
		TriangleContacts() : mFriction(0.1f) { }

		void Prepare(Bodies& bodies, PhaseType phase) override
		{
			for (size_t i = 0; i < mTris.size(); i++)
			{
				mTris[i].lambda = 0;
				mTris[i].lambdaF1 = 0;
			}
		}
		virtual void SolveAcc(float h, Bodies& bodies, PhaseType phase) { }
		virtual void ApplyForces(float h, Bodies& bodies, PhaseType phase) override { }

		void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr) override
		{
			SolveContactPBD(h, bodies);
			SolveFrictionPBD(h, bodies);
		}

		void SolveContactPBD(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mTris.size(); i++)
			{
				TriangleContact& contact = mTris[i];
				Vector3& v1 = bodies[contact.i1].pos;
				Vector3& v2 = bodies[contact.i2].pos;
				Vector3& v3 = bodies[contact.i3].pos;
				Vector3& v4 = bodies[contact.i4].pos;
				Vector3 p = contact.w1 * v1 + contact.w2 * v2 + contact.w3 * v3;
				Vector3 n = contact.normal;
				float len0 = contact.len;
				float len = n.Dot(bodies[contact.i4].pos - p);
				if (len > len0)
					continue;
				float invMass = bodies[contact.i1].invMass + bodies[contact.i2].invMass + 
					bodies[contact.i3].invMass + bodies[contact.i4].invMass;
				float s = 1.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / invMass;
				float dLambda = s * (len - len0);
				contact.lambda += invH * dLambda;
				Vector3 disp = dLambda * n;
				v1 += disp * (contact.w1 * bodies[contact.i1].invMass);
				v2 += disp * (contact.w2 * bodies[contact.i2].invMass);
				v3 += disp * (contact.w3 * bodies[contact.i3].invMass);
				v4 -= disp * (bodies[contact.i4].invMass);
				disp *= invH;
				bodies[contact.i1].vel += disp * (contact.w1 * bodies[contact.i1].invMass);
				bodies[contact.i2].vel += disp * (contact.w2 * bodies[contact.i2].invMass);
				bodies[contact.i3].vel += disp * (contact.w3 * bodies[contact.i3].invMass);
				bodies[contact.i4].vel -= disp * (bodies[contact.i4].invMass);
			}
		}

		void SolveFrictionPBD(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mTris.size(); i++)
			{
				TriangleContact& contact = mTris[i];
				Vector3& v1 = bodies[contact.i1].vel;
				Vector3& v2 = bodies[contact.i2].vel;
				Vector3& v3 = bodies[contact.i3].vel;
				Vector3& v4 = bodies[contact.i4].vel;
				Vector3 v = contact.w1 * v1 + contact.w2 * v2 + contact.w3 * v3;
				Vector3 n = contact.normal;
				Vector3 v12 = v4 - v;
				float vnrel = contact.normal.Dot(v12);

				float invMass = bodies[contact.i1].invMass + bodies[contact.i2].invMass + 
					bodies[contact.i3].invMass + bodies[contact.i4].invMass;
				float s = 1.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / invMass;

				float limit = mFriction * contact.lambda;
				Vector3 vt = v12 - vnrel * contact.normal;
				float vtrel = vt.Length();
				if (vtrel > 0.0001f)
				{
					float dLambda = vtrel * s;
					float lambda0 = contact.lambdaF1;
					contact.lambdaF1 = lambda0 + dLambda;
					if (contact.lambdaF1 >= limit)
						contact.lambdaF1 = limit;
					dLambda = contact.lambdaF1 - lambda0;

					vt.Scale(1.f / vtrel); // normalize
					Vector3 impulse = -dLambda * vt;

					v1 += impulse * (contact.w1 * bodies[contact.i1].invMass);
					v2 += impulse * (contact.w2 * bodies[contact.i2].invMass);
					v3 += impulse * (contact.w3 * bodies[contact.i3].invMass);
					v4 -= impulse * (bodies[contact.i4].invMass);
					impulse *= h;
					bodies[contact.i1].pos += impulse * (contact.w1 * bodies[contact.i1].invMass);
					bodies[contact.i2].pos += impulse * (contact.w2 * bodies[contact.i2].invMass);
					bodies[contact.i3].pos += impulse * (contact.w3 * bodies[contact.i3].invMass);
					bodies[contact.i4].pos -= impulse * (bodies[contact.i4].invMass);
				}
			}
		}

	//private:
	public: // FIXME
		std::vector<TriangleContact> mTris;
		float mFriction;
	};

	struct TriangleRigidContact : public TriangleContact
	{
		Vector3 a;
		//int vtxIdx;
	};

	class TriangleRigidContacts : public IConstraintList
	{
	public:
		TriangleRigidContacts() : mFriction(0.1f) { }

		void Clear() { mTris.clear(); };

		size_t GetNumContacts() const { return mTris.size(); }
		
		TriangleRigidContact& GetContact(size_t i ) { return mTris[i]; }

		void Prepare(Bodies& bodies, PhaseType phase) override
		{
			for (size_t i = 0; i < mTris.size(); i++)
			{
				mTris[i].lambda = 0;
				mTris[i].lambdaF1 = 0;
			}
		}
		virtual void SolveAcc(float h, Bodies& bodies, PhaseType phase) 
		{
			if (phase == PT_POSITION)
			{
				SolveContactPBDAcc(h, bodies);
			}
		}
		virtual void ApplyForces(float h, Bodies& bodies, PhaseType phase)
		{
			if (phase == PT_POSITION)
				SolveContactPBDApply(h, bodies);
		}

		void SolveContactPBDAcc(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			float omega = 0.1f;
			for (size_t i = 0; i < mTris.size(); i++)
			{
				TriangleRigidContact& contact = mTris[i];
				Vector3& v1 = bodies[contact.i1].pos;
				Vector3& v2 = bodies[contact.i2].pos;
				Vector3& v3 = bodies[contact.i3].pos;
				Vector3& v4 = bodies[contact.i4].pos;
				Vector3 p = contact.w1 * v1 + contact.w2 * v2 + contact.w3 * v3;
				Vector3 n = contact.normal;
				float len0 = contact.len;
				Vector3 a = qRotate(bodies[contact.i4].q, contact.a);
				float len = n.Dot(bodies[contact.i4].pos + a - p);
				if (len > len0)
				{
					contact.disp.SetZero();
					contact.dw.SetZero();
					continue;
				}

				Vector3 axn = cross(a, n);
				float inertia = axn.Dot(bodies[contact.i4].Iinv * axn);

				float invMass = bodies[contact.i1].invMass + bodies[contact.i2].invMass + 
					bodies[contact.i3].invMass + bodies[contact.i4].invMass + inertia;
				float s = 1.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / invMass;
				float dLambda = s * (len - len0);
				contact.lambda += invH * dLambda;
				
				contact.disp = omega * dLambda * n;
				contact.dw = -omega * dLambda * axn;
			}
		}

		void SolveContactPBDApply(float h, Bodies& bodies)
		{
			float invH = 1.f / h;
			for (size_t i = 0; i < mTris.size(); i++)
			{
				TriangleRigidContact& contact = mTris[i];
				Vector3 disp = contact.disp;
				bodies[contact.i1].pos += disp * (contact.w1 * bodies[contact.i1].invMass);
				bodies[contact.i2].pos += disp * (contact.w2 * bodies[contact.i2].invMass);
				bodies[contact.i3].pos += disp * (contact.w3 * bodies[contact.i3].invMass);
				bodies[contact.i4].pos -= disp * (bodies[contact.i4].invMass);

				Vector3 dw = contact.dw;
				bodies[contact.i4].q += 0.5f * ((bodies[contact.i4].Iinv * dw) * bodies[contact.i4].q);
				bodies[contact.i4].q = qNormalize(bodies[contact.i4].q);

				disp *= invH;
				bodies[contact.i1].vel += disp * (contact.w1 * bodies[contact.i1].invMass);
				bodies[contact.i2].vel += disp * (contact.w2 * bodies[contact.i2].invMass);
				bodies[contact.i3].vel += disp * (contact.w3 * bodies[contact.i3].invMass);
				bodies[contact.i4].vel -= disp * (bodies[contact.i4].invMass);

				bodies[contact.i4].w += bodies[contact.i4].Iinv * dw;
			}
		}

		void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr) override
		{
			SolveContactPBD(h, bodies);
			//SolveFrictionPBD(h, bodies);
		}

		void SolveContactPBD(float h, Bodies& bodies);

		void SolveFrictionPBD(float h, Bodies& bodies)
		{
			for (size_t i = 0; i < mTris.size(); i++)
			{
				TriangleRigidContact& contact = mTris[i];
				Vector3& v1 = bodies[contact.i1].vel;
				Vector3& v2 = bodies[contact.i2].vel;
				Vector3& v3 = bodies[contact.i3].vel;
				Vector3& v4 = bodies[contact.i4].vel;
				Vector3 v = contact.w1 * v1 + contact.w2 * v2 + contact.w3 * v3;
				Vector3 n = contact.normal;
				Vector3 a = bodies[contact.i4].R * contact.a;
				Vector3 v12 = v4  + cross(bodies[contact.i4].w, a) - v;
				float vnrel = contact.normal.Dot(v12);

				float limit = mFriction * contact.lambda;
				Vector3 vt = v12 - vnrel * contact.normal;
				float vtrel = vt.Length();
				if (vtrel < 0.0001f)
					continue;
				vt.Scale(1.f / vtrel); // normalize
				
				Vector3 axt = cross(a, vt);
				float inertia = axt.Dot(bodies[contact.i4].Iinv * axt);

				float invMass = bodies[contact.i1].invMass + bodies[contact.i2].invMass + 
					bodies[contact.i3].invMass + bodies[contact.i4].invMass + inertia;
				float s = 1.f / (1.f + contact.w1 * contact.w1 + contact.w2 * contact.w2 + contact.w3 * contact.w3) / invMass;

				float dLambda = vtrel * s;
				float lambda0 = contact.lambdaF1;
				contact.lambdaF1 = lambda0 + dLambda;
				if (contact.lambdaF1 >= limit)
					contact.lambdaF1 = limit;
				dLambda = contact.lambdaF1 - lambda0;

				Vector3 impulse = -dLambda * vt;
				Vector3 dw = dLambda * axt;

				v1 += impulse * (contact.w1 * bodies[contact.i1].invMass);
				v2 += impulse * (contact.w2 * bodies[contact.i2].invMass);
				v3 += impulse * (contact.w3 * bodies[contact.i3].invMass);
				v4 -= impulse * (bodies[contact.i4].invMass);

				bodies[contact.i4].w += bodies[contact.i4].Iinv * dw;

				impulse *= h;
				bodies[contact.i1].pos += impulse * (contact.w1 * bodies[contact.i1].invMass);
				bodies[contact.i2].pos += impulse * (contact.w2 * bodies[contact.i2].invMass);
				bodies[contact.i3].pos += impulse * (contact.w3 * bodies[contact.i3].invMass);
				bodies[contact.i4].pos -= impulse * (bodies[contact.i4].invMass);
				
				dw *= h;
				bodies[contact.i4].q += 0.5f * ((bodies[contact.i4].Iinv * dw) * bodies[contact.i4].q);
				bodies[contact.i4].q = qNormalize(bodies[contact.i4].q);
			}
		}

	//private:
	public: // FIXME
		std::vector<TriangleRigidContact> mTris;
		float mFriction;
	};
}

#endif // TRIANGLE_CONTACTS_H