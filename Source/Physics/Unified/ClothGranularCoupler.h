
#include <Physics/Unified/Unified.h>
#include <Physics/Unified/ParticleContacts.h>
#include <Physics/Unified/TriangleContacts.h>
#include <Physics/Unified/Cloth.h>
#include <Physics/Unified/Granular.h>
#include <Engine/Utils.h>

namespace Physics
{
	class ClothGranularCoupler : public ICoupler
	{
	public:
		void DetectCollision(Group* group1, Group* group2, Collidables& collidables, Bodies& bodies, Constraints& constraints) override
		{
			if (typeid(*group1) == typeid(Cloth) && typeid(*group2) == typeid(Granular))
			{
				DetectCollision((Cloth*)group1, (Granular*)group2, bodies, constraints);
			}
			else if (typeid(*group2) == typeid(Cloth) && typeid(*group1) == typeid(Granular))
			{
				DetectCollision((Cloth*)group2, (Granular*)group1, bodies, constraints);
			}
		}

		void DetectCollision(Cloth* cloth, Granular* granular, Bodies& bodies, Constraints& constraints)
		{
			mParticleContacts.mContacts.clear();
			// brute force sphere collisions
			//for (size_t i = 0; i < cloth->GetCount(); i++)
			//{
			//	for (size_t j = 0; j < granular->GetCount(); j++)
			//	{
			//		size_t i1 = i + cloth->GetStart();
			//		size_t i2 = j + granular->GetStart();
			//		Body& body1 = bodies[i1];
			//		Body& body2 = bodies[i2];
			//		Vector3 delta = body1.pos - body2.pos;
			//		float dist = cloth->GetThickness() + granular->GetRadius();// + cloth->GetTolerance() + granular->GetTolerance();
			//		if (delta.LengthSquared() < dist * dist)
			//		{
			//			ParticleContact contact;
			//			contact.i1 = i1;
			//			contact.i2 = i2;
			//			contact.len = dist;						
			//			mParticleContacts.mContacts.push_back(contact);
			//		}
			//	}
			//}
			constraints.AddList(&mParticleContacts);

			// brute force point-triangle collisions
			// TODO: CCD, BVH
			mTriangleContacts.mTris.clear();
			
			float radTol = granular->GetRadius() + cloth->GetThickness() + granular->GetTolerance();
			for (size_t j = 0; j < cloth->mTriangles.size(); j++)
			{
				const uint32 i1 = cloth->mTriangles[j].i1;
				const uint32 i2 = cloth->mTriangles[j].i2;
				const uint32 i3 = cloth->mTriangles[j].i3;
				const Vector3& x1 = bodies[i1].pos;
				const Vector3& x2 = bodies[i2].pos;
				const Vector3& x3 = bodies[i3].pos;

				// build AABB
				//Vector3 minV = vmin(vmin(x1, x2), x3) - extrude;
				//Vector3 maxV = vmax(vmax(x1, x2), x3) + extrude;
				
				// point - triangle
				for (size_t i = 0; i < granular->GetCount(); i++)
				{
					size_t ii = i + granular->GetStart();
					if (bodies[ii].invMass == 0)
						continue;
					const Vector3& x4 = bodies[ii].pos;
					// TODO: AABB test

					Vector3 n, p;
					float d;
					Geometry::BarycentricCoords coords;
					bool intersect = Geometry::IntersectSphereTriangle(x4, radTol, x1, x2, x3, n, p, d, coords);
					if (intersect)
					{
						TriangleContact c;
						c.i1 = i1;
						c.i2 = i2;
						c.i3 = i3;
						c.i4 = (uint32)ii;
						c.w1 = coords.u;
						c.w2 = coords.v;
						c.w3 = coords.w;
						c.normal = n;
						c.len = granular->GetRadius() + cloth->GetThickness();
						mTriangleContacts.mTris.push_back(c);
					}
				}
			}
			constraints.AddList(&mTriangleContacts);
		}

		void AddContact(Cloth* cloth, Granular* granular, int i0, int tri, const Vector3& p, const Vector3& n)
		{
			//int idx = tri;
			//uint32 j1, j2, j3;
			//cloth->mBtMesh->getMeshPart(0)->getTrimeshPrimitiveManager()->get_indices(idx, j1, j2, j3);
			//const Vector3& v1 = cloth->GetMesh().vertices[j1];
			//const Vector3& v2 = cloth->GetMesh().vertices[j2];
			//const Vector3& v3 = cloth->GetMesh().vertices[j3];
			//BarycentricCoords coords = Barycentric(v1, v2, v3, p); // TODO: check if p is in the triangle plane

			//TriangleContact c;
			//c.i1 = j1 + cloth->GetStart();
			//c.i2 = j2 + cloth->GetStart();
			//c.i3 = j3 + cloth->GetStart();
			//c.i4 = i0;
			//c.w1 = coords.u;
			//c.w2 = coords.v;
			//c.w3 = coords.w;
			//c.normal = n;
			//c.len = granular->GetRadius();// + cloth->GetThickness();
			//mTriangleContacts.mTris.push_back(c);
		}

		void AddContact(Cloth* cloth, Granular* granular, int i1, int i2)
		{
			float dist = cloth->GetThickness() + granular->GetRadius();
			ParticleContact contact;
			contact.i1 = i1;
			contact.i2 = i2;
			contact.len = dist;						
			mParticleContacts.mContacts.push_back(contact);
		}

		void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints) override
		{
			//Printf("Bullet\n");
			for (size_t i = 0; i < collMgr.GetContacts().size(); i++)
			{
				const CollisionManager::Contact& pair = collMgr.GetContacts()[i];
				Group* group1 = pair.group1;
				Group* group2 = pair.group2;
				if (group1 == nullptr || group2 == nullptr || group1 == group2)
					continue;
				//Printf("%x %x\n", group1, group2);
				if (typeid(*group1) == typeid(Cloth) && typeid(*group2) == typeid(Granular))
				{
					//AddContact((Cloth*)group1, (Granular*)group2, pair.i2, pair.tri1, pair.a1, -pair.n);
					AddContact((Cloth*)group1, (Granular*)group2, pair.i1, pair.i2);
				}
				else if (typeid(*group2) == typeid(Cloth) && typeid(*group1) == typeid(Granular))
				{
					//AddContact((Cloth*)group2, (Granular*)group1, pair.i1, pair.tri2, pair.a2, pair.n);
					AddContact((Cloth*)group2, (Granular*)group1, pair.i1, pair.i2);
				}				
			}
		}

	private:
		ParticleContacts mParticleContacts;
		TriangleContacts mTriangleContacts;
	};
}