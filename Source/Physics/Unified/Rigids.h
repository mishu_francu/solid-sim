#ifndef RIGIDS_H
#define RIGIDS_H

#include <Physics/Unified/Unified.h>
#include <Physics/Unified/RigidContacts.h>
#include <Physics/Unified/RigidJoints.h>
#include "CollisionManager.h"
#include "MeshCollisions.h"

namespace Physics
{
	class Rigids : public Group, IContactAdder
	{
	public:
		void UpdateCollidables(CollisionManager& collMgr, Bodies& bodies) override;

		void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints) override;

		size_t GetNumContacts() const { return mRigidContacts.GetNumContacts(); }
		const RigidContact& GetContact(size_t i) const { return mRigidContacts.GetContact(i); }
		size_t GetNumJoints() const { return mRigidJoints.GetNumJoints(); }
		const RigidJoint& GetJoint(size_t i) const { return mRigidJoints.GetJoint(i); }

		void AddJoint(Bodies& bodies, int i, int j, const Vector3& p);
        void AddLockedJoint(Bodies& bodies, int i, int j, const Vector3& p);
        void AddHingeJoint(Bodies& bodies, int i, int j, const Vector3& p, const Vector3& axis);

		void DetectCollision(Bodies& bodies, Collidables& collidables, Constraints& constraints, bool beforePos) override;

		void SetFriction(float val) { mRigidContacts.SetFriction(val); }
		void SetBaumgarte(float val) { mRigidContacts.SetBaumgarte(val); }
		void SetRegularization(float val) { mRigidContacts.SetRegularization(val); }
		void SetDamping(float val) { mRigidContacts.SetDamping(val); }

	private:
		void MeshVsMesh(Bodies& bodies, int i, int j, const Geometry::Mesh* mesh1, const Geometry::Mesh* mesh2, float radTol, bool flip = false);
		void TrianglesVsVertices(Bodies& bodies, int i, int j, const Geometry::Mesh* mesh1, const Geometry::Mesh* mesh2, float radTol, bool flip = false);

		// is this still used?
		void AddContact(size_t idx, const Vector3& a, const Vector3& n, float depth)
		{
			RigidContact contact;
			contact.i = idx + mStart;
			contact.j = 0; // TODO: constant for fixed body
			contact.a1.SetZero();
			contact.a2.SetZero();
			contact.n = n;
			//contact.vel.SetZero();
			contact.depth = depth;
			mRigidContacts.AddContact(contact);
		}			

		void AddContact(int i, int j, Vector3 a1, Vector3 a2, Vector3 n, float depth)
		{
			RigidContact jnt;
			jnt.i = i;
			jnt.j = j;
			jnt.a1 = a1;
			jnt.a2 = a2;
			jnt.depth = depth;
			jnt.n = n;
			mRigidContacts.AddContact(jnt);
			//mPrevContacts.AddContact(jnt);
		}

	private:
		RigidContacts mRigidContacts;
		RigidJoints mRigidJoints;
		RigidContacts mPrevContacts;
	};
}

#endif // RIGIDS_H