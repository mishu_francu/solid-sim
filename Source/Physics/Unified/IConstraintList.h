#ifndef ICONSTRAINTLIST_H
#define ICONSTRAINTLIST_H

#include <Math/Matrix3.h>
#include <Math/Quaternion.h>
#include <vector>

namespace Physics
{
	struct Body
	{
		Math::Vector3 pos, vel, pos0;
		float invMass;
		//bool surface; // temp hack for FEM
		// rigid starts here; TODO: minimize footprint
		Matrix3 Icinv, Iinv, IinvPos;
		Matrix3 R; // rotation matrix (redundant, but useful)
		Quaternion q, q0;
		Vector3 w; // angular velocity
		Vector3 force, torque, vel0, w0; // for Newmark
		bool rigid; // TODO: flags
		int collIdx;
		Body() : invMass(1), rigid(false), collIdx(-1) { }

		inline void UpdateRotationalPart()
		{
			// update rotation (for collisions) and inertia first
			R = qToMatrix(q);
			Iinv = R * Icinv * !R; // TODO: use the quaternion directly
			Vector3 r1 = 0.5f * (R(0) * q).v;
			Vector3 r2 = 0.5f * (R(1) * q).v;
			Vector3 r3 = 0.5f * (R(2) * q).v;
			Matrix3 L(r1, r2, r3);
			IinvPos = L * Icinv * !L;
		}

        inline void SetRotation(const Quaternion& quat)
        {
            q = quat;
            UpdateRotationalPart();
        }
	};

	typedef std::vector<Body> Bodies;

	struct IIntegrator
	{
		virtual ~IIntegrator() { }
		virtual void IntegrateVelocities(float h, Bodies& bodies) = 0;
		virtual void IntegratePositions(float h, Bodies& bodies) = 0;
		virtual void ApplyImpulse(float h, Body& b1, Body& b2, const Vector3& imp, const Vector3& dw1, const Vector3& dw2) = 0;
	};

	enum PhaseType
	{
		PT_VELOCITY,
		PT_POSITION,
		PT_POSTPROCESS
	};

	class IConstraintList
	{
	public:
		virtual ~IConstraintList() { }
		virtual void Prepare(Bodies& bodies, PhaseType phase) = 0;
		virtual void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr) = 0;
		virtual void SolveAcc(float h, Bodies& bodies, PhaseType phase) = 0;
		virtual void ApplyForces(float h, Bodies& bodies, PhaseType phase) = 0;
		virtual float CalculateStats(Bodies& bodies) { return 0; }
	};
}

#endif
