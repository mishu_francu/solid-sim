#ifndef SOFT_FEM_H
#define SOFT_FEM_H

#include <Physics/Unified/Unified.h>
#include <Physics/Unified/StaticContacts.h>
#include <Eigen/Dense>
#include "CollisionManager.h"

#define STVK // FIXME

namespace Physics
{
	struct Tetrahedron
	{
		Matrix3 Hn[4], Hs[4]; // fixed Jacobian terms
		Matrix3 X, Xtr; // undeformed shape matrix
		uint16 i[4]; // node indices
		//Matrix3 R;
		float vol; // volume
		// linear FEM
		//Matrix3 K[4][4]; // stiffness matrix
	};

	class TetrahedronConstraints : public IConstraintList
	{
		friend class SoftFEM;
	public:
		TetrahedronConstraints(std::vector<Tetrahedron>& tets) : mTets(tets) { }
		virtual void Prepare(Bodies& bodies, PhaseType phase) { }
		virtual void SolveAcc(float h, Bodies& bodies, PhaseType phase) { }
		virtual void ApplyForces(float h, Bodies& bodies, PhaseType phase) override { }

		virtual void Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator = nullptr);

	private:
		std::vector<Tetrahedron>& mTets;
		Eigen::Matrix<float, 6, 6, Eigen::DontAlign> C; // compliance matrix
		size_t mStart;
	//public:
		//EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	};

	class SoftFEM : public Group
	{
	public:
		SoftFEM() : mTetConstraints(mTets), mThickness(0.5f), mTolerance(0.2f), mYoung(1), mCollIdx(-1) { }

		const std::vector<Tetrahedron>& GetTets() const { return mTets; }

		float GetThickness() const { return mThickness; }
		float GetTolerance() const { return mTolerance; }

		void Init(Bodies& bodies, float young, float poisson);

		void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints) override
		{
			constraints.AddList(&mTetConstraints);
		}

		void DetectCollision(Bodies& bodies, Collidables& collidables, Constraints& constraints, bool beforePos) override;

		void AddTetrahedron(uint16 i0, uint16 i1, uint16 i2, uint16 i3)
		{
			Tetrahedron tet;
			tet.i[0] = i0;
			tet.i[1] = i1;
			tet.i[2] = i2;
			tet.i[3] = i3;
			mTets.push_back(tet);
		}

		void SetVelocity(Bodies& bodies, const Vector3& v);

		void BuildBoundaryMesh(const Bodies& bodies);

		Geometry::Mesh* GetCollisonMesh() { return &mCollMesh; }

		void UpdateCollisionMesh(const Bodies& bodies);

		virtual void UpdateCollidables(CollisionManager& collMgr, Bodies& bodies) 
		{
			UpdateCollisionMesh(bodies);
			//if (mCollIdx >= 0)
			//{
			//	collMgr.UpdateShape(mCollIdx, bodies[mStart].pos - mInitialPos, Quaternion());
			//	collMgr.UpdateMesh(mCollIdx);
			//}
		}

		void DistributeMass(float mass, Bodies& bodies)
		{
			float impp = mCount / mass;
			for (size_t i = 0; i < mCount; i++)
				bodies[i + mStart].invMass = impp;
		}

	private:
		void WallCollisions(Bodies& bodies, const Geometry::Aabb3& walls);

		int AddContact(size_t idx, const Vector3& p, const Vector3& n)
		{
			return mStaticContacts.AddContact(idx, p, n, mThickness);
		}

	private:
		std::vector<Tetrahedron> mTets;
		float mYoung, mPoisson;
		Matrix3 E; // normal stress-strain matrix
		float mThickness, mTolerance;
		StaticContacts mStaticContacts;
		TetrahedronConstraints mTetConstraints;
		Geometry::Mesh mCollMesh;
		std::vector<int> mMap; // nodes to mesh mapping

	public: // FIXME!
		int mCollIdx;
		//Vector3 mInitialPos;
		std::vector<int> mMapInv; // mesh to nodes mapping
	};
}

#endif // SOFT_FEM_H