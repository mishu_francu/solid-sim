#include "RigidJoints.h"
#include "Math/Matrix2.h"

namespace Physics
{
    Matrix3 Skew(const Vector3& v)
    {
        return Matrix3(0,      v.Z(), -v.Y(),
                       -v.Z(), 0,     v.X(),
                       v.Y(),  -v.X(), 0);
    }

    float RigidJoints::CalculateStats(Bodies& bodies)
    {
        float normSqr = 0;
        for (size_t i = 0; i < mJoints.size(); i++)
        {
            RigidJoint& pair = mJoints[i];
            Body* p1 = &bodies[pair.i];
            Body* p2 = &bodies[pair.j];

            // world contact positions
            Vector3 a1 = p1->R * pair.a1;
            Vector3 a2 = p2->R * pair.a2;

            Vector3 delta = p1->pos + a1 - p2->pos - a2;
            normSqr += delta.LengthSquared();
        }
        return normSqr;
    }

    template <typename Joint>
    void PrepareP2PJointsVTS(Bodies& bodies, std::vector<Joint> &joints)
    {
        for (size_t i = 0; i < joints.size(); i++)
        {
            Joint& pair = joints[i];
            Body* p1 = &bodies[pair.i];
            Body* p2 = &bodies[pair.j];

            // world contact positions
            Vector3 a1 = p1->R * pair.a1;
            Vector3 a2 = p2->R * pair.a2;

            pair.delta = p1->pos + a1 - p2->pos - a2;
        }
    }

    void RigidJoints::Prepare(Bodies& bodies, PhaseType phase)
    {
        if (phase == PT_VELOCITY)
        {
            PrepareP2PJointsVTS(bodies, mJoints);
            PrepareP2PJointsVTS(bodies, mLockedJoints);
            PrepareP2PJointsVTS(bodies, mHingeJoints);

            // prepare locked joints
            for (size_t i = 0; i < mLockedJoints.size(); i++)
            {
                LockedJoint& pair = mLockedJoints[i];
                Body* p1 = &bodies[pair.i];
                Body* p2 = &bodies[pair.j];

                Quaternion q1 = p1->q * pair.q;
                Quaternion q2 = p2->q;
                Matrix3 eye;
                Matrix3 T1 = q1.s * eye + Skew(q1.v);
                Matrix3 T2 = q2.s * eye + Skew(q2.v);
                Matrix3 J = -0.5f * (Matrix3::TensorProduct(q1.v, q2.v) + T1 * T2);
                Matrix3 Jt = J.GetTranspose();
                Matrix3 A = J * (p1->Iinv + p2->Iinv) * Jt;
                Matrix3 Ainv = A.GetInverse();

                // stabilization
                Quaternion q = p1->q.GetConjugate() * p2->q;
                Vector3 rotError = (pair.q.GetConjugate() * q).v;

                pair.J = J;
                pair.Ainv = Ainv;
                pair.stab = rotError;
            }
        }
    }

    void RigidJoints::Solve(float h, Bodies& bodies, PhaseType phase, IIntegrator* integrator)
    {
        if (phase == PT_POSITION)
            SolveJointsPBD(h, bodies, integrator);
        if (phase == PT_VELOCITY)
            SolveJointsVTS(h, bodies);
    }

    void RigidJoints::SolveJointsPBD(float h, Bodies& bodies, IIntegrator* integrator)
    {
        float invH = 1.f / h;
        Matrix3 eye;
        for (size_t i = 0; i < mJoints.size(); i++)
        {
            RigidJoint& pair = mJoints[i];
            Body* p1 = &bodies[pair.i];
            Body* p2 = &bodies[pair.j];

            // world contact positions
            Vector3 a1 = p1->R * pair.a1;
            Vector3 a2 = p2->R * pair.a2;

            Vector3 delta = p1->pos + a1 - p2->pos - a2;
            Matrix3 a1x = Skew(a1);
            Matrix3 a2x = Skew(a2);
            Matrix3 A = (p1->invMass + p2->invMass) * eye - a1x * p1->Iinv * a1x - a2x * p2->Iinv * a2x;

            Vector3 imp = -A.GetInverse() * delta;
            Vector3 dw1 = cross(a1, imp);
            Vector3 dw2 = -cross(a2, imp);

            //integrator->ApplyImpulse(h, *p1, *p2, imp, dw1, dw2);

            // store generalized force (as displacement)
            p1->force += imp;
            p1->torque += dw1;
            p2->force -= imp;
            p2->torque += dw2;

            // rigid 1
            p1->vel += invH * p1->invMass * imp;
            p1->w += invH * p1->Iinv * dw1;

            p1->pos += p1->invMass * imp;
            p1->q += 0.5f * ((p1->Iinv * dw1) * p1->q);
            p1->q = qNormalize(p1->q);

            p1->R = qToMatrix(p1->q);
            p1->Iinv = p1->R * p1->Icinv * !p1->R;

            // rigid 2
            p2->vel -= invH * p2->invMass * imp;
            p2->w += invH * p2->Iinv * dw2;

            p2->pos -= p2->invMass * imp;
            p2->q += 0.5f * ((p2->Iinv * dw2) * p2->q);
            p2->q = qNormalize(p2->q);

            p2->R = qToMatrix(p2->q);
            p2->Iinv = p2->R * p2->Icinv * !p2->R;
        }
    }

    void SolveLockedVTS_ZeroQuaternion(float beta, Body* p1, Body* p2, const LockedJoint& constraint)
    {
        Vector3 error = constraint.J * (p1->w - p2->w);        
        error += beta * constraint.stab;

        // solve the problem exactly as a block
        Vector3 imp = -constraint.Ainv * error;
        Matrix3 Jt = constraint.J.GetTranspose();
        Vector3 dw = Jt * imp; // TODO: transpose multiplication or store Jt

        // rigid 1
        p1->w += p1->Iinv * dw;

        // rigid 2
        p2->w -= p2->Iinv * dw;
    }

    void SolvePointToPointVTS(Body* p1, Body* p2, const RigidJoint& pair, float beta)
    {
        // world space anchor vectors
        Vector3 a1 = p1->R * pair.a1;
        Vector3 a2 = p2->R * pair.a2;

        // delta velocity
        Vector3 delta = p1->vel + cross(p1->w, a1) - p2->vel - cross(p2->w, a2);
        // add position stabilization term
        delta += beta * pair.delta;

        // construct system 3x3 matrix
        Matrix3 a1x = Skew(a1); // angular Jacobian 1
        Matrix3 a2x = Skew(a2); // angular Jacobian 2
        // we use the fact that the transpose of a skew-symmetric matrix flips sign
        Matrix3 A = (p1->invMass + p2->invMass) * Matrix3::Identity() - a1x * p1->Iinv * a1x - a2x * p2->Iinv * a2x;

        // solve the problem exactly as a block
        Vector3 imp = -A.GetInverse() * delta;
        Vector3 dw1 = cross(a1, imp);
        Vector3 dw2 = -cross(a2, imp);

        // rigid 1
        p1->vel += p1->invMass * imp;
        p1->w += p1->Iinv * dw1;

        // rigid 2
        p2->vel -= p2->invMass * imp;
        p2->w += p2->Iinv * dw2;
    }

    void SolveHingeVTS_ZeroAngularVel(Body* p1, Body* p2, const HingeJoint& pair, float beta)
    {
        // Compute Jacobian (they differ by sign)
        Vector3 zero;
        Vector3 n1 = p1->R * pair.axis1; // x axis
        Vector3 n2 = p2->R * pair.axis2;
        Vector3 u1 = n1.Perpendicular(); // y axis
        Vector3 v1 = cross(n1, u1); // z axis
        Matrix3 Jt(zero, u1, v1);
        Matrix3 J = Jt.GetTranspose();

        // Compute system matrix
        Vector3 wrel = p1->w - p2->w;
        Vector3 rotError = cross(n2, n1); // the (instantaneous) rotation needed to align n1 and n2
        Vector3 wError = wrel + beta * rotError;
        float dot1 = dot(u1, wError);
        float dot2 = dot(v1, wError);
        Matrix3 A = J * (p1->Iinv + p2->Iinv) * Jt;

        // only use y and z components
        Matrix2 A2(A[1][1], A[1][2], A[2][1], A[2][2]);
        Matrix2 Ainv = A2.GetInverse();
        Vector2 error(-dot1, -dot2);
        Vector2 lambda = Ainv * error;

        Vector3 imp(0, lambda.x, lambda.y);
        // rigid 1
        p1->w += p1->Iinv * Jt * imp;

        // rigid 2
        p2->w -= p2->Iinv * Jt * imp;
    }

    void SolveHingeVTS_ZeroQuaternionAxis(Body* p1, Body* p2, const HingeJoint& pair, float beta)
    {
        // TODO: optimize (cache data)
        Quaternion q1 = p1->q * pair.q1;
        Quaternion q2 = p2->q * pair.q2;
        Quaternion q1p = q1;
        Matrix3 eye;
        Matrix3 T1 = q1p.s * eye + Skew(q1p.v);
        Matrix3 T2 = q2.s * eye + Skew(q2.v);
        Matrix3 J = -0.5f * (Matrix3::TensorProduct(q1p.v, q2.v) + T1 * T2);
        Matrix3 Jt = J.GetTranspose();

        // Consider X the hinge axis
        Vector3 u = Jt(1);
        Vector3 v = Jt(2);

        // Stabilization term
        Quaternion q = q1.GetConjugate() * q2; // the current relative quaternion between constraint frames
        Vector3 rotError = beta * q.v;

        // solve the problem exactly as a block
#ifndef LOCKED_HINGE
        Matrix3 A = p1->Iinv + p2->Iinv;
        Vector3 Au = A * u;
        Vector3 Av = A * v;
        float symmDot = dot(u, Av);
        Matrix2 A2(dot(u, Au), symmDot, symmDot, dot(v, Av));

        Vector3 error = p1->w - p2->w;
        Vector2 error2(-dot(u, error) - rotError.y, -dot(v, error) - rotError.z);
#ifdef EXACT_BLOCK_SOLVE
        Matrix2 Ainv = A2.GetInverse();
        Vector2 lambda = Ainv * error2;
#else
        Vector2 lambda(error2.x / A2.a11, error2.y / A2.a22); // Gauss-Seidel style
#endif
        Vector3 imp(0, lambda.x, lambda.y);
#else
        Matrix3 A = J * (p1->Iinv + p2->Iinv) * Jt;
        Matrix3 Ainv = A.GetInverse();
        Vector3 error = J * (p1->w - p2->w) + rotError;
        Vector3 imp = -Ainv * error;
#endif

        // rigid 1
        p1->w += p1->Iinv * Jt * imp;

        // rigid 2
        p2->w -= p2->Iinv * Jt * imp;
    }

    void RigidJoints::SolveJointsVTS(float h, Bodies& bodies)
    {
        float beta = mBaumgarte / h;
        float betaRot = 0.5f / h;
        for (size_t i = 0; i < mJoints.size(); i++)
        {
            RigidJoint& pair = mJoints[i];
            Body* p1 = &bodies[pair.i];
            Body* p2 = &bodies[pair.j];

            SolvePointToPointVTS(p1, p2, pair, beta);
        }

        for (size_t i = 0; i < mLockedJoints.size(); i++)
        {
            LockedJoint& pair = mLockedJoints[i];
            Body* p1 = &bodies[pair.i];
            Body* p2 = &bodies[pair.j];

            // 1. Solve the point to point part (translation DOFs)
            SolvePointToPointVTS(p1, p2, pair, beta);

            // 2. Solve the rotation part
            SolveLockedVTS_ZeroQuaternion(betaRot, p1, p2, pair);
        }

        for (size_t i = 0; i < mHingeJoints.size(); i++)
        {
            HingeJoint& pair = mHingeJoints[i];
            Body* p1 = &bodies[pair.i];
            Body* p2 = &bodies[pair.j];

            // 1. Solve the point to point part (translation DOFs)
            SolvePointToPointVTS(p1, p2, pair, beta);

            // 2. Solve the rotation part
            SolveHingeVTS_ZeroQuaternionAxis(p1, p2, pair, betaRot);
        }
    }

}