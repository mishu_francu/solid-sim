
#include <Physics/Unified/Unified.h>
#include <Physics/Unified/ParticleContacts.h>
#include <Physics/Unified/TriangleContacts.h>
#include <Physics/Unified/Cloth.h>
#include <Physics/Unified/Rigids.h>
#include <Engine/Utils.h>

namespace Physics
{
	class ClothRigidCoupler : public ICoupler
	{
	public:
		void DetectCollision(Group* group1, Group* group2, Collidables& collidables, Bodies& bodies, Constraints& constraints) override
		{
			if (typeid(*group1) == typeid(Cloth) && typeid(*group2) == typeid(Rigids))
			{
				DetectCollision((Cloth*)group1, (Rigids*)group2, collidables, bodies, constraints);
			}
			else if (typeid(*group2) == typeid(Cloth) && typeid(*group1) == typeid(Rigids))
			{
				DetectCollision((Cloth*)group2, (Rigids*)group1, collidables, bodies, constraints);
			}
		}

		void DetectCollision(Cloth* cloth, Rigids* rigids, Collidables& collidables, Bodies& bodies, Constraints& constraints);

	private:
		void TriangleVsMesh(const std::vector<Vector3>& vertices, 
			Bodies& bodies, size_t ii, const Geometry::Aabb3& triBox, const Collidable* shape, const Cloth* cloth,
			uint32 i1, uint32 i2, uint32 i3, 
			Vector3 x1, Vector3 x2, Vector3 x3,
			Vector3 v1, Vector3 v2, Vector3 v3,
			Vector3 n1, Vector3 n2, Vector3 n3);

		void TriangleVsMeshTree(const Geometry::Mesh* mesh, const Geometry::AabbTree* tree,
			Bodies& bodies, size_t ii, const Geometry::Aabb3& triBox, const Collidable* shape, const Cloth* cloth,
			uint32 i1, uint32 i2, uint32 i3, 
			Vector3 x1, Vector3 x2, Vector3 x3,
			Vector3 v1, Vector3 v2, Vector3 v3,
			Vector3 n1, Vector3 n2, Vector3 n3);

	//private: // FIXME
	public:
		TriangleRigidContacts mTriangleContacts;
		RigidContacts mRigidContacts;
	};
}