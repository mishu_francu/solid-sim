#ifndef GRANULAR_H
#define GRANULAR_H

#include "Unified.h"
#include <Physics/GridBroadphase3D.h>
#include "StaticContacts.h"
#include "ParticleContacts.h"
#include "CollisionManager.h"

namespace Physics
{
	// homogeneous sphere shaped particles forming a granular material
	class Granular : public Group
	{
	public:
		enum GranularMode
		{
			GM_PBD,
			GM_VTS,
			GM_HYBRID,
		};

	public:
		Granular() : mRadius(1.5f), mTolerance(0.1f), mMode(GM_HYBRID) { }
		void SetRadius(float val) { mRadius = val; }
		float GetRadius() const { return mRadius; }
		float GetTolerance() const { return mTolerance; }

		void Init(float w, float h, float d)
		{
			mBroadphase.Init(Vector3(), w, h, d, 2 * mRadius); // TODO: use a reset flag
			mAabbs.resize(mCount);
			mStaticContacts.SetBaumgarte(0.5f);
			mParticleContacts.mBeta = 0.5f;
			mParticleContacts.mFriction = 0.3f;
			mStaticContacts.SetFriction(0.3f);
			//mStaticContacts.mStiffness = 0.6f;
			//mStaticContacts.mDamping = 0.1f;
			//mParticleContacts.mStiffness = 0.6f;
			//mParticleContacts.mDamping = 0.5f;
		}

		void UpdateCollidables(CollisionManager& collMgr, Bodies& bodies) override
		{
			for (size_t i = 0; i < mCount; i++)
			{
				Body& body = bodies[i + mStart];
				if (body.collIdx < 0)
					continue;
				collMgr.UpdateShape(body.collIdx, body.pos, body.q);
			}
		}

		void CreateContactConstraints(const CollisionManager& collMgr, Bodies& bodies, Constraints& constraints) override
		{
			mParticleContacts.mContacts.clear();
			mStaticContacts.Clear();
			for (size_t i = 0; i < collMgr.GetContacts().size(); i++)
			{
				const CollisionManager::Contact& pair = collMgr.GetContacts()[i];
				if ((pair.i1 == -1 || bodies[pair.i1].invMass == 0) && pair.i2 >= mStart && pair.i2 < mStart + mCount)
				{					
					AddContact(pair.i2, pair.a1, -pair.n);
				}
				else if ((pair.i2 == -1 || bodies[pair.i2].invMass == 0) && pair.i1 >= mStart && pair.i1 < mStart + mCount)
				{
					AddContact(pair.i1, pair.a2, pair.n);
				}
				else if (pair.i1 >= mStart && pair.i1 < mStart + mCount && pair.i2 >= mStart && pair.i2 < mStart + mCount)
				{
					ParticleContact contact;
					contact.i1 = pair.i1;
					contact.i2 = pair.i2;
					contact.len = 2 * mRadius;
					mParticleContacts.mContacts.push_back(contact);
				}
			}
			constraints.AddList(&mParticleContacts);
			constraints.AddList(&mStaticContacts);
		}

		void UpdateAabbs(Bodies& bodies)
		{
			float tol = mRadius + mTolerance;
			Vector3 rad(tol);
			for (size_t i = 0; i < mCount; i++)
			{
				mAabbs[i].min = bodies[mStart + i].pos - rad;
				mAabbs[i].max = bodies[mStart + i].pos + rad;
			}
		}

		virtual void DetectCollision(Bodies& bodies, Collidables& collidables, Constraints& constraints, bool beforePos) override
		{
			if ((beforePos && mMode == GM_PBD) || (!beforePos && mMode == GM_VTS))
				return;

			mStaticContacts.Clear();
			mParticleContacts.mContacts.clear();

			UpdateAabbs(bodies);
			mBroadphase.Sample(mAabbs);
			CollideParticles(bodies);

			for (size_t i = 0; i < collidables.size(); i++)
			{
				if ((collidables[i]->mType == CT_WALLS))// && (mCollFlags & CF_WALLS)) // TODO
				{
					const Walls* walls = (const Walls*)collidables[i].get();
					WallCollisions(bodies, walls->mBox);
				}
			}

			constraints.AddList(&mStaticContacts);
			constraints.AddList(&mParticleContacts);
		}

	private:
		void WallCollisions(Bodies& bodies, const Geometry::Aabb3& walls)
		{
			const float r = mRadius + mTolerance;
			for (size_t i = mStart; i < mStart + mCount; i++)
			{
				const Body& particle = bodies[i];
				if (particle.pos.Y() - r <= walls.min.Y())
					AddContact(i, Vector3(particle.pos.X(), walls.min.Y(), particle.pos.Z()), Vector3(0, 1, 0));
				//if (particle.pos.Y() + r >= WALL_TOP)
				//	AddContact((ParticleIdx)i, Vector3(particle.pos.X(), WALL_TOP, particle.pos.Z()), Vector3(0, -1, 0));
				if (particle.pos.X() - r <= walls.min.X())
					AddContact(i, Vector3(walls.min.X(), particle.pos.Y(), particle.pos.Z()), Vector3(1, 0, 0));
				if (particle.pos.X() + r >= walls.max.X())
					AddContact(i, Vector3(walls.max.X(), particle.pos.Y(), particle.pos.Z()), Vector3(-1, 0, 0));
				if (particle.pos.Z() - r <= walls.min.Z())
					AddContact(i, Vector3(particle.pos.X(), particle.pos.Y(), walls.min.Z()), Vector3(0, 0, 1));
				if (particle.pos.Z() + r >= walls.max.Z())
					AddContact(i, Vector3(particle.pos.X(), particle.pos.Y(), walls.max.Z()), Vector3(0, 0, -1));
			}
		}

		int AddContact(size_t idx, const Vector3& p, const Vector3& n)
		{
			//StaticContact contact;
			//contact.idx = idx;
			//contact.point = p;
			//contact.normal = n;
			//contact.vel.SetZero();
			//contact.len = mRadius;
			//mStaticContacts.mContacts.push_back(contact);
			//return mStaticContacts.mContacts.size() - 1;
			return mStaticContacts.AddContact(idx, p, n, mRadius);
		}			

		// collisions detection between particles
		void CollideParticles(Bodies& bodies)
		{
			std::vector<Constraint3D> candidates; // TODO: a more lightweight struct
			mBroadphase.Update(mAabbs, candidates);
			const float radTol = mRadius + mTolerance;
			const float radSqr = 4 * radTol * radTol;
			for (size_t i = 0; i < candidates.size(); i++)
			{
				if ((bodies[candidates[i].i1 + mStart].pos - bodies[candidates[i].i2 + mStart].pos).LengthSquared() <= radSqr)
				{
					ParticleContact contact;
					contact.i1 = candidates[i].i1 + mStart;
					contact.i2 = candidates[i].i2 + mStart;
					contact.len = 2 * mRadius;
					mParticleContacts.mContacts.push_back(contact);
				}
			}
		}

	private:
		float mRadius; // the radius of all particles
		float mTolerance; // collision tolerance
		ParticleContacts mParticleContacts;
		StaticContacts mStaticContacts;
		GridBroadphase3D mBroadphase;
		std::vector<Geometry::Aabb3> mAabbs;
		GranularMode mMode; // mostly for collision detection
	};
}

#endif // GRANULAR_H