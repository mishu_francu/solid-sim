#include "ClothIVP.h"

namespace Physics
{
	void ClothModelIVP::Step(float h)
	{
		// Symplectic Euler
		for (size_t i = 0; i < mParticles.size(); i++)
		{
			mParticles[i].prev = mParticles[i].pos;
			if (mParticles[i].invMass == 0)
				continue;
			mParticles[i].vel += h * gravity * mUnit;
			mParticles[i].pos += h * mParticles[i].vel;
		}

		// collision detection
		mContacts.clear();
		DetectCollisions();

		// reset constraints to zero
		for (size_t i = 0; i < mLinks.size(); i++)
		{
			mLinks[i].lambda = 0;
			mLinks[i].disp.SetZero();
		}
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			mContacts[i].lambda = 0;
			mContacts[i].lambdaF = 0;
		}

		if (mSolver == SOLVER_GAUSS_SEIDEL)
			SolveGS(h);
	}

	void ClothModelIVP::SolveGS(float h)
	{
		for (int k = 0; k < mNumIterations; ++k)
		{
			SolveLinks(h, 1.0f);
		}
	}

	void ClothModelIVP::SolveLinks(float h, float omega)
	{
		const float h2 = h * h;
		const float k = h2 * mArea * mUnit;
		const float beta = 0.6f / h;
		for (size_t i = 0; i < mLinks.size(); i++)
		{
			Link& link = mLinks[i];
			Particle& p1 = mParticles[link.i1];
			Particle& p2 = mParticles[link.i2];

			float mu = 1.f / (p1.invMass + p2.invMass);

			Vector3 delta = p1.pos - p2.pos;
			const float len0 = link.len;
			float len = delta.Length();
			delta.Scale(1 / len);
			float posErr = len - len0;

			Vector3 vrel = p1.vel - p2.vel;
			float velErr = vrel.Dot(delta);
			float err = velErr + beta * posErr;

			float lambda = err * mu;
			link.lambda += lambda;

			delta.Scale(omega * lambda);
			p1.vel -= delta * p1.invMass;
			p2.vel += delta * p2.invMass;
			p1.pos -= delta * (h * p1.invMass);
			p2.pos += delta * (h * p2.invMass);
		}
	}
}