#include "ClothOpen.h"
#include <vector>
#include <glm/glm.hpp>

using std::vector;

namespace Physics
{
	int numX = 20, numY=20;
	const size_t total_points = (numX+1)*(numY+1);
	int size = 4;
	float hsize = size/2.0f;

	vector<glm::vec3> X;
	vector<glm::vec3> V;
	vector<glm::vec3> F;
	vector<glm::vec3> dc_dp; //  dc/dp
	vector<glm::mat3> df_dx; //  df/dp
	vector<glm::mat3> df_dv; //  df/dv
	vector<float> C; //for implicit integration
	vector<float> C_Dot; //for implicit integration
	vector<glm::vec3> deltaP2;

	struct Spring {
		int p1, p2;
		float rest_length;
		float Ks, Kd;
		int type;
	};

	vector<uint16> indices;
	vector<Spring> springs;

	const int STRUCTURAL_SPRING = 0;
	const int SHEAR_SPRING = 1;
	const int BEND_SPRING = 2;

	int spring_count=0;

	const float mult = 1;
	const float mult1 = 1;
	const float DEFAULT_DAMPING =  -0.125f * mult1;
	float	KsStruct = 0.75f * mult,KdStruct = -0.25f * mult1;
	float	KsShear = 0.75f * mult,KdShear = -0.25f * mult1;
	float	KsBend = 0.95f * mult,KdBend = -0.25f * mult1;
	glm::vec3 gravity1=glm::vec3(0.0f,-0.0981f * mult,0.0f);
	float mass =1.f;

	template<class T>
	class LargeVector {
	private:
		vector<T> v;

	public:

		LargeVector() {

		}
		LargeVector(const LargeVector& other) {
			v.resize(other.v.size());
			memcpy(&v[0], &(other.v[0]), sizeof(other.v[0])*other.v.size());
		}
		void resize(const int size) {
			v.resize(size);
		}
		void clear(bool isIdentity=false) {
			memset(&v[0], 0, sizeof(T)*v.size());
			if(isIdentity) {
				for(size_t i=0;i<v.size();i++) {
					v[i] = T(1);
				}
			}
		}


		T& operator[](int index) {
			return v[index];
		}
		friend LargeVector<glm::vec3> operator*(const LargeVector<glm::mat3> other, const LargeVector<glm::vec3> f );
		friend LargeVector<glm::vec3> operator*(const float f, const LargeVector<glm::vec3> other);
		friend LargeVector<glm::vec3> operator-(const LargeVector<glm::vec3> Va, const LargeVector<glm::vec3> Vb );
		friend LargeVector<glm::vec3> operator*(const LargeVector<glm::vec3> Va, const LargeVector<glm::vec3> Vb );
		friend LargeVector<glm::vec3> operator+(const LargeVector<glm::vec3> Va, const LargeVector<glm::vec3> Vb );

		friend LargeVector<glm::mat3> operator*(const float f, const LargeVector<glm::mat3> other);
		friend LargeVector<glm::mat3> operator-(const LargeVector<glm::mat3> Va, const LargeVector<glm::mat3> Vb );


		friend LargeVector<glm::vec3> operator/(const float f, const LargeVector<glm::vec3> v );
		friend float dot(const LargeVector<glm::vec3> Va, const LargeVector<glm::vec3> Vb );
	};

	LargeVector<glm::vec3> operator*(const LargeVector<glm::mat3> other, const LargeVector<glm::vec3> v ) {
		LargeVector<glm::vec3> tmp(v);
		for(size_t i=0;i<v.v.size();i++) {
			tmp.v[i] = other.v[i] * v.v[i];
		}
		return tmp;
	}

	LargeVector<glm::vec3> operator*(const LargeVector<glm::vec3> other, const LargeVector<glm::vec3> v ) {
		LargeVector<glm::vec3> tmp(v);
		for(size_t i=0;i<v.v.size();i++) {
			tmp.v[i] = other.v[i] * v.v[i];
		}
		return tmp;
	}


	LargeVector<glm::vec3> operator*(const float f, const LargeVector<glm::vec3> other) {
		LargeVector<glm::vec3> tmp(other);
		for(size_t i=0;i<other.v.size();i++) {
			tmp.v[i] = other.v[i]*f;
		}
		return tmp;
	}
	LargeVector<glm::mat3> operator*(const float f, const LargeVector<glm::mat3> other) {
		LargeVector<glm::mat3> tmp(other);
		for(size_t i=0;i<other.v.size();i++) {
			tmp.v[i] = other.v[i]*f;
		}
		return tmp;
	}
	LargeVector<glm::vec3> operator-(const LargeVector<glm::vec3> Va, const LargeVector<glm::vec3> Vb ) {
		LargeVector<glm::vec3> tmp(Va);
		for(size_t i=0;i<Va.v.size();i++) {
			tmp.v[i] = Va.v[i] - Vb.v[i];
		}
		return tmp;
	}
	LargeVector<glm::mat3> operator-(const LargeVector<glm::mat3> Va, const LargeVector<glm::mat3> Vb ) {
		LargeVector<glm::mat3> tmp(Va);
		for(size_t i=0;i<Va.v.size();i++) {
			tmp.v[i] = Va.v[i] - Vb.v[i];
		}
		return tmp;
	}

	LargeVector<glm::vec3> operator+(const LargeVector<glm::vec3> Va, const LargeVector<glm::vec3> Vb ) {
		LargeVector<glm::vec3> tmp(Va);
		for(size_t i=0;i<Va.v.size();i++) {
			tmp.v[i] = Va.v[i] + Vb.v[i];
		}
		return tmp;
	}

	LargeVector<glm::vec3> operator/(const float f, const LargeVector<glm::vec3> v ) {
		LargeVector<glm::vec3> tmp(v);
		for(size_t i=0;i<v.v.size();i++) {
			tmp.v[i] = v.v[i] / f;
		}
		return tmp;
	}


	float dot(const LargeVector<glm::vec3> Va, const LargeVector<glm::vec3> Vb ) {
		float sum = 0;
		for(size_t i=0;i<Va.v.size();i++) {
			sum += glm::dot(Va.v[i],Vb.v[i]);
		}
		return sum;
	}

	const float EPS = 0.0001f;
	const float EPS2 = EPS*EPS;
	const int i_max = 20;

	void SolveConjugateGradientPreconditioned(LargeVector<glm::mat3> A, LargeVector<glm::vec3>& x, LargeVector<glm::vec3> b,LargeVector<glm::vec3> P, LargeVector<glm::vec3> P_inv) {
		float i =0;
		LargeVector<glm::vec3> r =  (b - A*x);
		LargeVector<glm::vec3> d = P_inv*r;
		LargeVector<glm::vec3> q;
		float alpha_new = 0;
		float alpha = 0;
		float beta  = 0;
		float delta_old = 0;
		float delta_new = dot(r,P*r);
		float delta0    = delta_new;
		while(i<i_max && delta_new> EPS2*delta0) {
			q = A*d;
			alpha = delta_new/dot(d,q);
			x = x + alpha*d;
			r = r - alpha*q;
			delta_old = delta_new;
			delta_new = dot(r,r);
			beta = delta_new/delta_old;
			d = r + beta*d;
			i++;
		}
	}

	LargeVector<glm::vec3> dV;
	LargeVector<glm::mat3> A;
	glm::mat3 M = glm::mat3(1.0f);
	LargeVector<glm::vec3> b;
	LargeVector<glm::vec3> P_;
	LargeVector<glm::vec3> P_inv;
	vector<float> inv_len;

	void ClothModelOpen::AddSpring(int a, int b, float ks, float kd, int type) {
		Spring spring;
		spring.p1=a;
		spring.p2=b;
		spring.Ks=ks;
		spring.Kd=kd;
		spring.type = type;
		glm::vec3 deltaP = X[a]-X[b];
		spring.rest_length = sqrt(glm::dot(deltaP, deltaP));
		springs.push_back(spring);

		Link link;
		link.i1 = a;
		link.i2 = b;
		//link.stiffness = stiffness;
		//link.len = (mParticles[i1].pos - mParticles[i2].pos).Length();
		//link.disp.SetZero();
		mLinks.push_back(link);
	}

	void ClothModelOpen::Init()
	{
		mParticles.resize(total_points);

		int i=0, j=0, count=0;
		int l1=0, l2=0;
		float ypos = 7.0f;
		int v = numY+1;
		int u = numX+1;

		indices.resize( numX*numY*2*3);
		X.resize(total_points);
		V.resize(total_points);
		F.resize(total_points);

		A.resize(total_points);
		b.resize(total_points);
		dV.resize(total_points);
		P_.resize(total_points);
		P_inv.resize(total_points);


		//fill in X
		for( j=0;j<=numY;j++) {
			for( i=0;i<=numX;i++) {
				glm::vec3 pos( ((float(i)/(u-1)) *2-1)* hsize, size+1, ((float(j)/(v-1) )* size));
				X[count++] = pos;
				mParticles[i].pos.Set(pos.x, pos.y, pos.z);
				mParticles[i].pos.Scale(100);
			}
		}

		//fill in V
		memset(&(V[0].x),0,total_points*sizeof(glm::vec3));

		//fill in indices
		uint16* id=&indices[0];
		for (i = 0; i < numY; i++) {
			for (j = 0; j < numX; j++) {
				int i0 = i * (numX+1) + j;
				int i1 = i0 + 1;
				int i2 = i0 + (numX+1);
				int i3 = i2 + 1;
				if ((j+i)%2) {
					*id++ = i0; *id++ = i2; *id++ = i1;
					*id++ = i1; *id++ = i2; *id++ = i3;
				} else {
					*id++ = i0; *id++ = i2; *id++ = i3;
					*id++ = i0; *id++ = i3; *id++ = i1;
				}
			}
		}
		//setup springs
		// Horizontal
		for (l1 = 0; l1 < v; l1++)	// v
			for (l2 = 0; l2 < (u - 1); l2++) {
				AddSpring((l1 * u) + l2,(l1 * u) + l2 + 1,KsStruct,KdStruct,STRUCTURAL_SPRING);
			}

		// Vertical
		for (l1 = 0; l1 < (u); l1++)
			for (l2 = 0; l2 < (v - 1); l2++) {
				AddSpring((l2 * u) + l1,((l2 + 1) * u) + l1,KsStruct,KdStruct,STRUCTURAL_SPRING);
			}


		// Shearing Springs
		for (l1 = 0; l1 < (v - 1); l1++)
			for (l2 = 0; l2 < (u - 1); l2++) {
				AddSpring((l1 * u) + l2,((l1 + 1) * u) + l2 + 1,KsShear,KdShear,SHEAR_SPRING);
				AddSpring(((l1 + 1) * u) + l2,(l1 * u) + l2 + 1,KsShear,KdShear,SHEAR_SPRING);
			}


		// Bend Springs
		for (l1 = 0; l1 < (v); l1++) {
			for (l2 = 0; l2 < (u - 2); l2++) {
				AddSpring((l1 * u) + l2,(l1 * u) + l2 + 2,KsBend,KdBend,BEND_SPRING);
			}
			AddSpring((l1 * u) + (u - 3),(l1 * u) + (u - 1),KsBend,KdBend,BEND_SPRING);
		}
		for (l1 = 0; l1 < (u); l1++) {
			for (l2 = 0; l2 < (v - 2); l2++) {
				AddSpring((l2 * u) + l1,((l2 + 2) * u) + l1,KsBend,KdBend,BEND_SPRING);
			}
			AddSpring(((v - 3) * u) + l1,((v - 1) * u) + l1,KsBend,KdBend,BEND_SPRING);
		}

		int total_springs = springs.size();
		C.resize(total_springs );
		inv_len.resize(total_springs );
		C_Dot.resize(total_springs );
		dc_dp.resize(total_springs );
		deltaP2.resize(total_springs );
		df_dx.resize(total_springs );
		df_dv.resize(total_springs );
		memset(&(C[0]),0,total_springs*sizeof(float));
		memset(&(C_Dot[0]),0,total_springs*sizeof(float));
		memset(&(deltaP2[0].x),0,total_springs*sizeof(glm::vec3));

		memset(&(P_[0].x),0,total_points*sizeof(glm::vec3));
		memset(&(P_inv[0].x),0,total_points*sizeof(glm::vec3));
	}

	void ClothModelOpen::Step(float dt)
	{
		ComputeForces();

		IntegrateImplicit(5 * dt);
		//EllipsoidCollision();
		//ApplyProvotDynamicInverse();

		// copy positions		
		for(size_t i=0;i<total_points;i++) {
			mParticles[i].pos.Set(X[i].x, X[i].y, X[i].z);
			mParticles[i].pos.Scale(100);
		}
	}

	void ClothModelOpen::ComputeForces() {
		size_t i=0;

		for(i=0;i<total_points;i++) {
			F[i] = glm::vec3(0);

			//add gravity force
			if(i!=0 && i!=( numX)	)
				F[i] += gravity1;
			//add force due to damping of velocity
			F[i] += DEFAULT_DAMPING*V[i];
		}

		for(i=0;i<springs.size();i++) {
			glm::vec3 p1 = X[springs[i].p1];
			glm::vec3 p2 = X[springs[i].p2];
			glm::vec3 v1 = V[springs[i].p1];
			glm::vec3 v2 = V[springs[i].p2];
			glm::vec3 deltaP = p1-p2;
			glm::vec3 deltaV = v1-v2;
			float dist = glm::length(deltaP);
			inv_len[i] = 1.0f/dist;
			C[i] = dist-springs[i].rest_length;
			dc_dp[i] = deltaP/dist;
			C_Dot[i] = glm::dot(v1, -dc_dp[i]) + glm::dot(v2, dc_dp[i]);
			deltaP2[i] = glm::vec3(deltaP.x*deltaP.x,deltaP.y*deltaP.y, deltaP.z*deltaP.z);

			float leftTerm = -springs[i].Ks * (dist-springs[i].rest_length);
			float rightTerm = springs[i].Kd * (glm::dot(deltaV, deltaP)/dist);
			glm::vec3 springForce = (leftTerm + rightTerm)*glm::normalize(deltaP);

			if(springs[i].p1 != 0 && springs[i].p1 != numX)
				F[springs[i].p1] += springForce;
			if(springs[i].p2 != 0 && springs[i].p2 != numX )
				F[springs[i].p2] -= springForce;
		}
	}

	void ClothModelOpen::IntegrateImplicit(float deltaTime) {

		float h = deltaTime;
		CalcForceDerivatives();
		float y = 0.0;//correction term
		size_t i=0;
		for(i=0;i<total_points;i++) {
			A[i] =  M - h * (df_dv[i] + h * df_dx[i]);
			b[i] = (h * (F[i] + h * df_dx[i] * ( V[i] + y)));
			P_[i] = glm::vec3(A[i][0][0], A[i][1][1], A[i][2][2]);
			P_inv[i] = 1.0f/P_[i];// glm::vec3(1.0f/A[0][0], 1.0f/A[1][1], 1.0f/A[2][2]);
		}

		SolveConjugateGradientPreconditioned(A, dV, b, P_, P_inv);

		for(i=0;i<total_points;i++) {
			V[i] += (dV[i]*deltaTime);
			X[i] += deltaTime*V[i];

			//if(X[i].y <0) {
			//	X[i].y = 0;
			//}
		}
	}

	void ClothModelOpen::CalcForceDerivatives() {
		//clear the derivatives
		memset(&(df_dx[0]),0,total_points*sizeof(glm::mat3));
		memset(&(df_dv[0]),0,total_points*sizeof(glm::mat3));

		size_t i=0;

		glm::mat3 d2C_dp2[2][2]={glm::mat3(1.0f),glm::mat3(1.0f),glm::mat3(1.0f),glm::mat3(1.0f)};


		for(i=0;i<springs.size();i++) {
			float c1 = C[i];
			d2C_dp2[0][0][0][0] = (-c1*deltaP2[i].x+c1);
			d2C_dp2[0][0][1][1] = (-c1*deltaP2[i].y+c1);
			d2C_dp2[0][0][2][2] = (-c1*deltaP2[i].z+c1);

			d2C_dp2[0][1][0][0] = (c1*deltaP2[i].x-c1);
			d2C_dp2[0][1][1][1] = (c1*deltaP2[i].y-c1);
			d2C_dp2[0][1][2][2] = (c1*deltaP2[i].z-c1);

			d2C_dp2[1][0]  = d2C_dp2[0][1];
			d2C_dp2[1][1]  = d2C_dp2[0][0];

			glm::mat3 dp1  = glm::outerProduct(dc_dp[i], dc_dp[i]);
			glm::mat3 dp2  = glm::outerProduct(dc_dp[i], -dc_dp[i]);
			glm::mat3 dp3  = glm::outerProduct(-dc_dp[i],-dc_dp[i]);

			df_dx[i] += -springs[i].Ks* (dp1 + (d2C_dp2[0][0]*C[i]) ) - springs[i].Kd * (d2C_dp2[0][0]*C_Dot[i]);
			df_dx[i] += -springs[i].Ks* (dp2 + (d2C_dp2[0][1]*C[i]) ) - springs[i].Kd * (d2C_dp2[0][1]*C_Dot[i]);
			df_dx[i] += -springs[i].Ks* (dp2 + (d2C_dp2[1][1]*C[i]) ) - springs[i].Kd * (d2C_dp2[1][1]*C_Dot[i]);

			df_dv[i] += -springs[i].Kd*dp1;
			df_dv[i] += -springs[i].Kd*dp2;
			df_dv[i] += -springs[i].Kd*dp3;
		}
	}
}