#include "ClothModel.h"

namespace Physics
{
	class ClothModelReg : public ClothModel
	{
	public:
		ClothModelReg(ClothPatch* owner) : ClothModel(owner) { }
		ClothModelReg(const ClothModel& model) : ClothModel(model) { }
		void Step(float h);
	private:
		void SolveRegGS(float h);
		void SolveRegCR(float h, float alpha0);
		void SolveTriangles(float h);
		void SolveContactsReg(float h);
		void SolveLinks(float h);
		void SolveBendsReg(float h);
	};
}