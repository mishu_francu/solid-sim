#include "ClothImplicit.h"
#include <Engine/Profiler.h>
#include <Math/Matrix.h>
#include <Math/Matrix3.h>
#include <Eigen/Dense>
#include <Eigen/IterativeLinearSolvers>

namespace Physics
{
	void ClothModelImplicit::AddElasticForces(bool updateNormal)
	{
		// TODO: add force reset in here
		for (size_t i = 0; i < mLinks.size(); i++)
		{
			Link& link = mLinks[i];
			Particle& p1 = mParticles[link.i1];
			Particle& p2 = mParticles[link.i2];			
			Vector3 delta = p2.pos - p1.pos;
			float len = delta.Length();
			delta.Normalize();
			if (updateNormal)
			{
				link.normal = delta;
				link.f = link.len / len;
			}
			float dLen = len - link.len;
			Vector3 force = link.stiffness * mUnit * dLen * link.normal; // TODO: damping
			p1.force += force;
			p2.force -= force;
		}
	}

	void ClothModelImplicit::StepImplicitNCG(float h)
	{
		PROFILE_SCOPE("IntegrateImplicitNCG");

		// unconstrained step
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (mParticles[i].invMass == 0)
				continue;
			mParticles[i].vel += h * gravity * mUnit;
			mParticles[i].pos += h * mParticles[i].vel;
			mParticles[i].prev = mParticles[i].vel;
			mParticles[i].force.SetZero();
		}

		// add penalty forces
		AddElasticForces(true);

		// build rhs
		std::vector<Vector3> r(GetNumParticles());
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			r[i] = h * mParticles[i].invMass * mParticles[i].force;
		}
		const float c = h * h;
		//const float c = mYoung * mArea * h * h;

		// conjugate gradient
		float delta = r * r;
		std::vector<Vector3> d(r); // conjugate gradient directions
		std::vector<float> y(mLinks.size()); // used for computing J * d
		std::vector<Vector3> z(GetNumParticles()); // used for computing J^T * y
		for (int iter = 0; iter < mNumIterations; iter++)
		{
			if (delta == 0.f)
				break;

			// TODO: damping
			// compute y = h^2 * k * J * d
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				y[i] = mLinks[i].stiffness * mUnit * mLinks[i].normal.Dot(d[mLinks[i].i1] - d[mLinks[i].i2]);
			}
			// compute z = W * J^T * y
			memset(&z[0], 0, sizeof(Vector3) * GetNumParticles());
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				Vector3 add = y[i] * mLinks[i].normal;
				//add = mLinks[i].f * add + (1 - mLinks[i].f) * (d[mLinks[i].i1] - d[mLinks[i].i2]);
				z[mLinks[i].i1] += mParticles[mLinks[i].i1].invMass * add;
				z[mLinks[i].i2] -= mParticles[mLinks[i].i2].invMass * add;
			}
			// compute alpha
			z = d + c * z; // z = S * d
			float den = d * z;
			float alpha = delta / den;

			for (size_t i = 0; i < GetNumParticles(); i++)
			{
				if (mParticles[i].invMass != 0)
				{
					mParticles[i].vel += alpha * d[i];
					mParticles[i].pos += h * alpha * d[i];
				}
				mParticles[i].force.SetZero();
			}
			AddElasticForces(true);
			for (size_t i = 0; i < GetNumParticles(); i++)
			{
				r[i] = h * mParticles[i].invMass * mParticles[i].force;
			}

			float delta1 = r * r;
			float beta = delta1 / delta;
			delta = delta1;
			d = r + beta * d;
		}
	}

	void ClothModelImplicit::StepImplicitCG(float h)
	{
		PROFILE_SCOPE("IntegrateImplicitCG");

		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			mParticles[i].force.SetZero();
		}

		// add penalty forces
		AddElasticForces(true);

		// build rhs
		std::vector<Vector3> r(GetNumParticles());
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			r[i] = mParticles[i].vel + h * (mParticles[i].invMass * mParticles[i].force + gravity * mUnit);
		}
		const float c = h * h;

		// conjugate gradient
		float delta = r * r;
		std::vector<Vector3> d(r); // conjugate gradient directions
		std::vector<float> y(mLinks.size()); // used for computing J * d
		std::vector<Vector3> z(GetNumParticles()); // used for computing J^T * y
		std::vector<Vector3> v(GetNumParticles());
		for (int iter = 0; iter < mNumIterations; iter++)
		{
			if (delta == 0.f)
				break;

			// compute y = h^2 * k * J * d
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				y[i] = mLinks[i].stiffness * mUnit * mLinks[i].normal.Dot(d[mLinks[i].i1] - d[mLinks[i].i2]);
			}
			// compute z = W * J^T * y
			memset(&z[0], 0, sizeof(Vector3) * GetNumParticles());
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				Vector3 add = y[i] * mLinks[i].normal;
				add = c * (mLinks[i].f * add + (1 - mLinks[i].f) * (d[mLinks[i].i1] - d[mLinks[i].i2]));
				z[mLinks[i].i1] += mParticles[mLinks[i].i1].invMass * add;
				z[mLinks[i].i2] -= mParticles[mLinks[i].i2].invMass * add;
			}
			// compute alpha
			z = d + z; // z = A * d
			float den = d * z;
			float alpha = delta / den;
			v = v + alpha * d;
			r = r - alpha * z;

			float delta1 = r * r;
			float beta = delta1 / delta;
			delta = delta1;
			d = r + beta * d;
		}

		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (mParticles[i].invMass != 0)
			{
				mParticles[i].vel = v[i];
				mParticles[i].pos += h * v[i];
			}
			mParticles[i].force.SetZero();
		}
	}

	void AddMatrix(Eigen::MatrixXf& dst, const Matrix3& src, size_t i0, size_t j0)
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				dst(i0 + i, j0 + j) += src.m[i][j];
			}
		}
	}

	void ClothModelImplicit::StepImplicit(float h) // broken
	{
		// Mueller style
		//PROFILE_SCOPE("IntegrateImplicit");

		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			mParticles[i].force.SetZero();
		}

		AddElasticForces(true);

		// build matrix and rhs
		Eigen::VectorXf b(GetNumParticles() * 3);
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			const int ii = int(i * 3);
			Vector3 v = mParticles[i].vel + h * (mParticles[i].invMass * mParticles[i].force + gravity * mUnit);
			b[ii] = v.X();
			b[ii + 1] = v.Y();
			b[ii + 2] = v.Z();
		}
		Eigen::MatrixXf mat = Eigen::MatrixXf::Identity(GetNumParticles() * 3, GetNumParticles() * 3);
		const float c = h * h;
		for (size_t k = 0; k < mLinks.size(); k++)
		{
			Link& link = mLinks[k];
			Particle& p1 = mParticles[link.i1];
			Particle& p2 = mParticles[link.i2];			
			Vector3 delta = p2.pos - p1.pos;
			float len = delta.Length();

			const int i = link.i1;
			const int j = link.i2;
			Matrix3 T = Matrix3::TensorProduct(delta, delta);
			const float f = link.len / len;
			float ci = c * link.stiffness * mUnit * mArea / link.len;
			Matrix3 K = ci * (f - 1) * Matrix3::Identity() - ci * f * T;
			//Matrix3 K = -ci * T;
			const int ii = i * 3;
			const int jj = j * 3;

			AddMatrix(mat, -K, ii, ii);
			AddMatrix(mat, K, ii, jj);
			AddMatrix(mat, -K, jj, jj);
			AddMatrix(mat, K, jj, ii);

			//Vector3 vi = K * (mParticles[i].vel - mParticles[j].vel);
			//b[ii] += vi.X();
			//b[ii + 1] += vi.Y();
			//b[ii + 2] += vi.Z();

			//Vector3 vj = K * (mParticles[j].vel - mParticles[i].vel);
			//b[jj] += vj.X();
			//b[jj + 1] += vj.Y();
			//b[jj + 2] += vj.Z();
		}

		Eigen::VectorXf V = mat.colPivHouseholderQr().solve(b);
		//Eigen::ConjugateGradient<Eigen::MatrixXf> solver;
		////solver.setMaxIterations(mNumIterations);
		//solver.compute(mat);
		//if (solver.info() != Eigen::Success) {
		//	// decomposition failed
		//	return;
		//}
		//Eigen::VectorXf V = solver.solve(b);
		////if (solver.info() != Eigen::Success) {
		////	// solving failed
		////	return;
		////}	
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (mParticles[i].invMass != 0)
			{
				const int ii = int(i * 3);
				mParticles[i].vel = Vector3(V[ii], V[ii + 1], V[ii + 2]);
				mParticles[i].pos += h * mParticles[i].vel;
			}
		}
	}

	void ClothModelImplicit::StepImplicitUV(float h)
	{
		// Mueller style
		//PROFILE_SCOPE("IntegrateImplicit");

		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			mParticles[i].force.SetZero();
		}

		// TODO: different stiffness per each link
		AddElasticForces(true);

		// build matrix and rhs
		Eigen::VectorXf b(GetNumParticles() * 3);
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			const int ii = int(i * 3);
			Vector3 v = mParticles[i].vel + h * (mParticles[i].invMass * mParticles[i].force + gravity * mUnit);
			b[ii] = v.X();
			b[ii + 1] = v.Y();
			b[ii + 2] = v.Z();
		}
		Eigen::MatrixXf mat = Eigen::MatrixXf::Identity(GetNumParticles() * 3, GetNumParticles() * 3); // TODO: mass matrix
		for (size_t k = 0; k < mTriangles.size(); k++)
		{
			Triangle& tri = mTriangles[k];
			Particle& p1 = mParticles[tri.i1];
			Particle& p2 = mParticles[tri.i2];
			Particle& p3 = mParticles[tri.i3];
			Vector3 dx1 = p2.pos - p1.pos;
			Vector3 dx2 = p3.pos - p1.pos;

		}

		Eigen::VectorXf V = mat.colPivHouseholderQr().solve(b);
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (mParticles[i].invMass != 0)
			{
				const int ii = int(i * 3);
				mParticles[i].vel = Vector3(V[ii], V[ii + 1], V[ii + 2]);
				mParticles[i].pos += h * mParticles[i].vel;
			}
		}
	}
}