#include "ClothNewmark.h"
#include <Engine/Profiler.h>

namespace Physics
{
	void ClothModelNewmark::Step(float h)
	{
		PROFILE_SCOPE("StepNewmark");
		// Newmark projection
		const float nmGamma = 0.55f;
		const float nmBeta = 0.29f;
		const float dampRatio = 0.f; // gamma / h
		const float h2 = h * h;
		const float invH = 1.f / h;
		const float stiff = h2 * mArea * mUnit;

		// unconstrained step
		for (size_t i = 0; i < GetNumParticles(); i++)
			mParticles[i].force.SetZero();
		for (size_t i = 0; i < mLinks.size(); i++)
		{
			const Link& link = mLinks[i];
			mParticles[link.i1].force -= link.disp;
			mParticles[link.i2].force += link.disp;
			mLinks[i].disp.SetZero();
			mLinks[i].lambda = 0;
		}
		for (size_t i = 0; i < mContacts.size(); i++)
		{
			mParticles[mContacts[i].idx].force -= mContacts[i].disp; 
		}
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (mParticles[i].invMass == 0.f)
				continue;
			mParticles[i].prev = mParticles[i].pos;
			mParticles[i].pos += h * mParticles[i].vel + 0.5f * h2 * mUnit * gravity + 0.5f * h2 * (1 - 2 * nmBeta) * mParticles[i].invMass * mParticles[i].force;
			mParticles[i].vel += h * mUnit * gravity + (1.f - nmGamma) * h * mParticles[i].invMass * mParticles[i].force;
		}

		HandleMouseSpring(h);

		// collision detection
		mContacts.clear();
		DetectCollisions();

		for (size_t i = 0; i < mContacts.size(); i++)
		{
			mContacts[i].lambda = 0;
			mContacts[i].lambdaF = 0;
			mContacts[i].disp.SetZero();
		}

		const float s = nmGamma / h;
		for (int k = 0; k < mNumIterations; ++k)
		{
			for (size_t i = 0; i < mLinks.size(); i++)
			{
				Particle& p1 = mParticles[mLinks[i].i1];
				Particle& p2 = mParticles[mLinks[i].i2];
				Vector3 delta = p1.pos - p2.pos;
				float len0 = mLinks[i].len;
				float len = delta.Length();
				delta.Scale(1.f / len);
				float epsilon = len0 / (mLinks[i].stiffness * mUnit * mArea);
				float err = len - len0;
				float vrel = delta.Dot(p1.vel - p2.vel);
				float res = ((err + h * dampRatio * vrel) /*- epsilon * mLinks[i].lambda*/) / nmBeta;
				float lambda = res / ((1.f + dampRatio) * (p1.invMass + p2.invMass) + epsilon / (nmBeta * h2));
				mLinks[i].lambda += lambda; // total force magnitude (approx)
				delta.Scale(lambda); // the constraint force
				mLinks[i].disp += (1.f / h2) * delta; // total constraint force
				p1.pos -= nmBeta * p1.invMass * delta;
				p2.pos += nmBeta * p2.invMass * delta;
				p1.vel -= s * p1.invMass * delta;
				p2.vel += s * p2.invMass * delta;
			}
			for (size_t i = 0; i < mContacts.size(); i++)
			{
				Contact& contact = mContacts[i];
				Particle& p1 = mParticles[contact.idx];
				Vector3 delta = p1.pos - contact.point;
				float len = delta.Dot(contact.normal);
				if (len > mThickness)
					continue;
				delta = contact.normal;
				const float depth = len - mThickness;
				delta.Scale(depth);
				mContacts[i].disp = (1.f / (nmBeta * h2)) * delta;
				p1.pos -= delta;
				p1.vel -= s * delta;

				// friction
				if (0/*mFriction > 0.f*/)
				{
					Vector3 v12 = p1.vel;
					float vnrel = contact.normal.Dot(v12);
					Vector3 vt = v12 - vnrel * contact.normal;
					float vtrel = vt.Length();

					float lambda = vnrel;// + invH * depth;
					contact.lambda += lambda;
					const float limit = mFriction * contact.lambda;

					if (vtrel > 0.001f)
					{
						float dLambda = vtrel;
						float lambda0 = contact.lambdaF;
						contact.lambdaF = lambda0 + dLambda;
						if (contact.lambdaF >= limit)
							contact.lambdaF = limit;
						dLambda = contact.lambdaF - lambda0;

						vt.Scale(1.f / vtrel); // normalize
						Vector3 p = dLambda * vt;
						p1.vel += /*nmGamma * */p;
						p1.pos += nmBeta * h * p;
						mContacts[i].disp += invH * p;
					}
				}
			}
		}
	}
}