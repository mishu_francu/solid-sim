#pragma once
#include "ClothModel.h"

namespace Physics
{
	class ClothModelIVP : public ClothModel
	{
	public:
		ClothModelIVP(ClothPatch* owner) : ClothModel(owner) { }
		ClothModelIVP(const ClothModel& model) : ClothModel(model) { }
		void Step(float h);

	private:
		void SolveGS(float h);
		void SolveLinks(float h, float omega);
	};
}