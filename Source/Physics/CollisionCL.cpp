#include "CollisionCL.h"
#include <Math/Vector4.h>
#include "Geometry/Mesh.h"

using namespace Geometry;

namespace Physics
{
	ALIGN16 struct PrimitivePairCL
	{
		Vector4 p, n;
		cl_uint idx1, idx2;
		int idx;
	};

	void NarrowPhaseCL::Init()
	{
		OpenCL* ocl = OpenCL::GetInstance();
		if (!ocl)
			return;
		mProgram = ocl->Build("../Shaders/Collision.cl");
		// Create kernel object
		if (mProgram)
		{
			mNarrowPhaseKernel.Create(mProgram, "PointVsTriangle");
		}
	}

	void NarrowPhaseCL::PrepareBuffers(const std::vector<Particle>& particles, const Mesh& mesh)
	{
		mPairsBuffer.Release();
		maxContacts = 500000;//max(particles.size(), mesh.indices.size() / 3) * 20;
		//Printf("max pairs: %d\n", maxContacts);
		mPairsBuffer.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(PrimitivePairCL) * maxContacts);
		mPrevBuffer.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * particles.size());
		mIndexBuffer.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(uint32) * mesh.indices.size());
		mVertexBuffer.Create(CL_MEM_ALLOC_HOST_PTR, sizeof(Vector4) * mesh.vertices.size());

		OpenCL* compute = OpenCL::GetInstance();
		if (!compute) return;

		// copy mesh index data
		uint32* idxPtr = (uint32*)compute->EnqueueMap(mIndexBuffer, true, CL_MAP_WRITE);
		for (size_t i = 0; i < mesh.indices.size(); i++)
		{
			idxPtr[i] = mesh.indices[i];
		}
		compute->EnqueueUnmap(mIndexBuffer, idxPtr);

		// copy mesh vertex data
		Vector4* vertPtr = (Vector4*)compute->EnqueueMap(mVertexBuffer, true, CL_MAP_WRITE);
		for (size_t i = 0; i < mesh.vertices.size(); i++)
		{
			vertPtr[i] = mesh.vertices[i];
		}
		compute->EnqueueUnmap(mVertexBuffer, vertPtr);
	}

	void NarrowPhaseCL::CopyBuffers(const std::vector<PrimitivePair>& pairs, const std::vector<Particle>& particles)
	{
		OpenCL* compute = OpenCL::GetInstance();
		if (!compute) return;

		// copy pairs
		nContacts = NextMultiple(pairs.size(), WAVE_SIZE);
		ASSERT(nContacts <= maxContacts);
		PrimitivePairCL* pairPtr = (PrimitivePairCL*)compute->EnqueueMap(mPairsBuffer, true, CL_MAP_WRITE);
		memset(pairPtr, 0, nContacts * sizeof(PrimitivePairCL));
		for (size_t i = 0; i < pairs.size(); i++)
		{
			pairPtr[i].idx1 = pairs[i].idx1;
			pairPtr[i].idx2 = pairs[i].idx2;
			pairPtr[i].idx = -1;
		}
		compute->EnqueueUnmap(mPairsBuffer, pairPtr);

		// copy prev positions
		Vector4* prevPtr = (Vector4*)compute->EnqueueMap(mPrevBuffer, true, CL_MAP_WRITE);
		for (size_t i = 0; i < particles.size(); i++)
		{
			prevPtr[i] = particles[i].prev;
		}
		compute->EnqueueUnmap(mPrevBuffer, prevPtr);
	}

	void NarrowPhaseCL::NarrowPhase(float radTol)
	{
		OpenCL* compute = OpenCL::GetInstance();
		if (!compute) return;

		mNarrowPhaseKernel.SetArgument(0, &mPairsBuffer);
		mNarrowPhaseKernel.SetArgument(1, &mPrevBuffer);
		mNarrowPhaseKernel.SetArgument(2, &mIndexBuffer);
		mNarrowPhaseKernel.SetArgument(3, &mVertexBuffer);
		mNarrowPhaseKernel.SetArgument(4, &radTol);

		size_t localSize = WAVE_SIZE;
		compute->EnqueueKernel(mNarrowPhaseKernel, &nContacts, &localSize);
		clFlush(compute->GetCommandQueue());
	}

	void NarrowPhaseCL::ReadBuffers(size_t nPairs, std::vector<Contact>& contacts)
	{
		OpenCL* compute = OpenCL::GetInstance();
		if (!compute) return;

		PrimitivePairCL* pairPtr = (PrimitivePairCL*)compute->EnqueueMap(mPairsBuffer, true, CL_MAP_READ);
		for (size_t i = 0; i < nPairs; i++)
		{
			if (pairPtr[i].idx >= 0)
			{
				Contact contact;
				contact.idx = pairPtr[i].idx;
				contact.point = pairPtr[i].p;
				contact.normal = pairPtr[i].n;
				contacts.push_back(contact);
			}
		}
		compute->EnqueueUnmap(mPairsBuffer, pairPtr);
	}
}