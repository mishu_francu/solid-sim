#include "RigidSystem.h"

namespace Physics
{
	bool Disjoint(const Rigid::Box& b1, const Rigid::Box& b2, Vector3 Axis, float& d)
	{
		float mina, maxa, minb, maxb;

		b1.GetSpan (Axis, mina, maxa);
		b2.GetSpan(Axis, minb, maxb);

		if (mina > maxb || minb > maxa)
			return true;

		d  = (maxa < maxb)? maxa : maxb;
		d -= (mina > minb)? mina : minb;

		return false;
	}

	bool BoxBox(const Rigid::Box& b1, const Rigid::Box& b2, RigidSystem::Contact& info)
	{
		Vector3 PA, PB, N;
		int i,j;
		Vector3 L[15]; //cele 15 axe de test
		for(i=0;i<3;i++)
			L[i] = b1.R(i);
		for(i=0;i<3;i++)
			L[3+i] = b2.R(i);
		for(i=0;i<3;i++)
			for(j=0;j<3;j++)
				L[6+3*i+j] = cross(L[i], L[3+j]);
		float d[15];
		for(i=0;i<15;i++)
			if(Disjoint(b1,b2,L[i],d[i]))
				return false;
		//cutiile se intrepatrund, cautam normala
		float min=1000000000.0f;
		int minaxis=-1;
		for(i=0; i < 15; i ++){
			//daca nu putem normaliza axa, sarim peste ea
			float l2 = L[i]*L[i];
			if (l2 < 0.000001f)
				continue;
			//normalizam axa separatoare si adancimea de pentrare
			float invl = 1.0f / sqrt(l2);
			L[i] *= invl;
			d[i] *= invl;
			//cautam axa cu pentrarea minima
			if (d[i] < min){
				min = d[i];
				minaxis = i;
			}
		}
		if (minaxis == -1)
			return 0;
		//ne asiguram ca normala indica dinspre A spre B
		Vector3 D=b2.X-b1.X;
		N = L[minaxis];
		float depth = d[minaxis];
		if (D * N > 0.0f)
			N.Flip();
		//informatia de coliziune
		switch(minaxis)
		{
			case 0: //cazul coliziune
			case 1: //plan triunghiular cu
			case 2:{//colt de cutie
				//punctul cel mai apropiat pe directia normalei la plan
				b2.GetSupportPoint(-N, PB);
				//calculeaza punctul din triunghi
				PA = PB - N * depth;

				info.pa = PA;
				info.pb = PB;
				info.n = N;

				return 1;
				   }
			case 3: //cazul coliziune
			case 4: //vertex din triunghi cu
			case 5:{ //fata a cutiei
				//afla care vertex din triunghi
				b1.GetSupportPoint(N, PA);
				//calculeaza punctul din cutie
				PB = PA + N * depth;

				info.pa = PA;
				info.pb = PB;
				info.n = N;

				return 1;
				   }
			case 6: //cazul coliziune
			case 7: //muchie - muchie
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			{	
				//afla care muchii
				int i = minaxis-6;
				int ia = i / 3;
				int ib = i - ia * 3;
				//gaseste 2 puncte pe cele 2 muchii
				Vector3 VB;
				b1.GetSupportPoint( N, PA);
				b2.GetSupportPoint(-N, VB);							
				//-----------------------------------------------------------------
				// Find the edge intersection. 
				// We first create a plane, aligned with the separation axis and the edge
				// on the box. 
				// Then we find the intersection between that plane (PB, N x F) and the line
				// (PA, E). That will give us the point on the triangle edge. 
				//-----------------------------------------------------------------

				//-----------------------------------------------------------------
				// plane along N and F, and passing through PB
				//-----------------------------------------------------------------
				Vector3 PlaneNormal = cross(N, b2.R(ib));
				float  PlaneD      = PlaneNormal * VB;

				//-----------------------------------------------------------------
				// find the intersection t, where Pintersection = PA + t*E
				//-----------------------------------------------------------------
				float div = b1.R(ia) * PlaneNormal;

				//-----------------------------------------------------------------
				// plane and ray colinear, skip the intersection.
				//-----------------------------------------------------------------
				if (fabs(div) < 0.000001f)
					return 0;

				float t = (PlaneD - PA * PlaneNormal) / div;

				//-----------------------------------------------------------------
				// point on edge of triangle
				//-----------------------------------------------------------------
				PA += b1.R(ia) * t;

				//-----------------------------------------------------------------
				// Calcualte the point on the box
				//-----------------------------------------------------------------
				PB = PA + N * depth;

				info.pa = PA;
				info.pb = PB;
				info.n = N;

				return 1;
			}
		}
		return 0;
	}

	bool BoxSphere(const Rigid::Box& b, const Rigid::Sphere& s, RigidSystem::Contact& info)
	{
		Vector3 C = !b.R * (s.X - b.X);
		float dmin = 0;
		Vector3 P;
		float delta;
		for (int i = 0; i<3; i++){
			if (C[i] > b.D[i]){
				delta = C[i] - b.D[i];
				dmin += delta * delta;
				P[i] = b.D[i];
			}
			else if (C[i] < -b.D[i]){
				delta = C[i] + b.D[i];
				dmin += delta * delta;
				P[i] = -b.D[i];
			}
			else
				P[i] = C[i];
		}
		if (dmin > 0 && dmin <= s.r * s.r){
			//trateaza coliziunea
			//corpul A - cutia, B - sfera
			Vector3 pa = b.X + b.R*P; // P in coord globale
			Vector3 n = (pa - s.X); // versorul dinspre centrul sferei spre P
			n.Normalize();
			Vector3 pb = s.X + s.r*n; // pct de contact al sferei
			info.pa = pa;
			info.pb = pb;
			info.n = n;
			info.a1 = P;
			info.a2 = !s.R * (s.r*n);
			return true;
		}
		return false;
	}

	bool SphereSphere(Rigid::Sphere* s1, Rigid::Sphere* s2, RigidSystem::Contact& info){
		Vector3 DX = s1->X - s2->X;
		if (DX.Length() < s1->r + s2->r){
			Vector3 normal = DX;
			normal.Normalize();
			info.n = normal;
			info.pa = s1->X - s1->r * normal;
			info.pb = s2->X + s2->r * normal;
			return true;
		}
		return false;
	}

	void RigidSystem::Step()
	{
		//if(pause) return;
		int n = rigids.size(); // number of bodies
		// update rotation and inertia tensor
		for (int i = 0; i < n; i++)
		{
			Rigid* b = &rigids[i];
			b->q = qNormalize(b->q);
			b->R = qToMatrix(b->q);
			b->Iinv = b->R * b->Icinv * !b->R;
		}
		DetectCollision();
		UpdateJoints();
		
		int s = map.size(); // number of constraints
		//init lambda
		float* lambda = new float[s]; // TODO: vector or pfloatlocated
		for(int i=0; i<s; i++)
			lambda[i] = 0;
		//init a = B * lambda
		float* a = new float[6*n];
		for(int i=0; i<6*n; i++)
			a[i] = 0;
		//compute d
		float* d = new float[s];
		for(int i=0; i<s; i++){
			d[i] = 0;
			for(int j=0; j<6; j++)
				d[i] += map[i].Jsp1[j] * map[i].Bsp1[j] + map[i].Jsp2[j] * map[i].Bsp2[j];
		}
		//compute eta
		float* eta = new float[s];
		for(int i=0; i<s; i++)
		{
			eta[i] = 0;
			// body 1
			if(map[i].i != -1)
			{
				Rigid* b1 = &rigids[map[i].i];
				float dot = 0;
				Vector3 aa1 = b1->Iinv * b1->M;
				for(int j=0; j<3; j++)
				{
					dot += map[i].Jsp1[j] * (b1->v[j] - h * b1->invmass * b1->F[j]);
					dot += map[i].Jsp1[j+3] * (b1->w[j] + h * aa1[j]);
				}
				eta[i] += map[i].beta * map[i].C / h - dot;
			}
			// body 2
			if(map[i].j != -1)
			{
				Rigid* b2 = &rigids[map[i].j];
				float dot = 0;
				Vector3 aa2 = b2->Iinv * b2->M;
				for(int j=0; j<3; j++)
				{
					dot += map[i].Jsp2[j] * (b2->v[j] - h * b2->invmass * b2->F[j]);
					dot += map[i].Jsp2[j+3] * (b2->w[j] + h * aa2[j]);
				}
				eta[i] += map[i].beta * map[i].C / h - dot;
			}
		}
		//Gauss-Seidel
		for(int l=0; l<numIterations; l++){
			for(int i=0; i<s; i++){
				int b1 = map[i].i;
				int b2 = map[i].j;
				float sum = 0;
				for(int j=0; j<6; j++){
					if(b1 != -1)
						sum += map[i].Jsp1[j] * a[b1*6+j];
					if(b2 != -1)
						sum += map[i].Jsp2[j] * a[b2*6+j];
				}
				float delta = (eta[i] - sum)/d[i];
				float l0 = lambda[i];
				lambda[i] += delta;
				if(lambda[i] > map[i].lmax)
					lambda[i] = map[i].lmax;
				else if(lambda[i] < map[i].lmin)
					lambda[i] = map[i].lmin;
				delta = lambda[i] - l0;
				for(int j=0; j<6; j++){
					if(b1 != -1)
						a[b1*6+j] += delta * map[i].Bsp1[j];
					if(b2 != -1)
						a[b2*6+j] += delta * map[i].Bsp2[j];
				}
			}
		}
		// compute V2 and update positions
		for (int i = 0; i < n; i++)
		{
			Rigid* b = &rigids[i];		
			if (b->invmass == 0)
				continue;
			Vector3 aa = b->Iinv * b->M;
			for(int j=0; j<3; j++)
			{
				b->f[j] = a[6*i + j];
				b->m[j] = a[6*i + j + 3];
				b->v[j] += b->f[j] + h * (b->invmass * b->F[j]);			
				b->w[j] += h * (b->m[j] + aa[j]);
			}		
			b->X += h * b->v;		
			b->q += 0.5f * h * (b->w * b->q);		
		}		
		// deallocate memory; TODO: drop
		delete[] eta;
		delete[] d;
		delete[] a;	
		delete[] lambda;
		// remove contacts
		std::vector<Joint>::iterator it = joints.begin();
		int c = 0;
		while(it != joints.end()){
			Joint jnt = *it;
			(*it).k -= c;
			if(jnt.type == CONTACT || jnt.type == FRICTION){
				it = joints.erase(it);			
				c++;
				continue;
			}
			else if(jnt.type == DFRICTION){
				it = joints.erase(it);
				continue;
			}
			it++;
		}
		std::vector<Constraint>::iterator  cit = map.begin();
		while(cit != map.end()){
			Constraint cstr = *cit;
			if(cstr.type == CONTACT || cstr.type == FRICTION){
				cit = map.erase(cit);			
				continue;
			}
			cit++;
		}
	}

	void RigidSystem::UpdateJoints()
	{
		int s = map.size(); //nr de constrangeri
		//compute C and Jsp
		for(int i=0; i<s; i++)
		{
			for(int j=0; j<6; j++){
				map[i].Jsp1[j] = 0;
				map[i].Jsp2[j] = 0;
				map[i].Bsp1[j] = 0;
				map[i].Bsp2[j] = 0;
				map[i].Jf1[j] = 0;
				map[i].Jf2[j] = 0;
			}
		}
		for(size_t i=0; i<joints.size(); i++)
		{
			Joint jnt = joints[i];
			int k = jnt.k;
			Vector3 a1, a2, p1, p2;
			if(jnt.i != -1){
				Rigid* b1 = &rigids[jnt.i];
				a1 = b1->R * jnt.a1;
				p1 = b1->X + a1;
			}
			else
				a1 = p1 = jnt.a1;
			if(jnt.j != -1){
				Rigid* b2 = &rigids[jnt.j];
				a2 = b2->R * jnt.a2;
				p2 = b2->X + a2;
			}
			else
				a2 = p2 = jnt.a2;
			if(joints[i].type == BALL_SOCKET || joints[i].type == HINGE){
				Vector3 x = p1 - p2;
				for(int j=0; j<3; j++)
					map[k+j].C = x[j];
				//corpul 1
				if(jnt.i != -1){
					map[k].Jsp1[0] = 1;
					map[k].Jsp1[4] = a1.Z();
					map[k].Jsp1[5] = -a1.Y();
					map[k + 1].Jsp1[1] = 1;
					map[k + 1].Jsp1[3] = -a1.Z();
					map[k + 1].Jsp1[5] = a1.X();
					map[k + 2].Jsp1[2] = 1;
					map[k + 2].Jsp1[3] = a1.Y();
					map[k + 2].Jsp1[4] = -a1.X();
				}
				//corpul 2
				if(jnt.j != -1){
					map[k].Jsp2[0] = -1;
					map[k].Jsp2[4] = -a2.Z();
					map[k].Jsp2[5] = a2.Y();
					map[k + 1].Jsp2[1] = -1;
					map[k + 1].Jsp2[3] = a2.Z();
					map[k + 1].Jsp2[5] = -a2.X();
					map[k + 2].Jsp2[2] = -1;
					map[k + 2].Jsp2[3] = -a2.Y();
					map[k + 2].Jsp2[4] = a2.X();
				}
				//if(jnt.type == HINGE){				
				//	Rigid* b1 = &rigids[jnt.i];
				//	Rigid* b2 = &rigids[jnt.j];
				//	Vector3 s1 = b1->R * jnt.s1;
				//	Vector3 s2 = b2->R * jnt.s2;
				//	Vector3 t1, t2;
				//	GetTangents(s1, t1, t2);
				//	Vector3 u = cross(s1, s2);
				//	map[k + 3].C = -t1 * u;
				//	map[k + 4].C = -t2 * u;
				//	if(jnt.i != -1)					
				//		for(int j=0; j<3; j++){
				//			map[k + 3].Jsp1[j] = -t1[j];
				//			map[k + 4].Jsp1[j] = -t2[j];
				//		}
				//		if(jnt.j != -1)
				//			for(int j=0; j<3; j++){
				//				map[k + 3].Jsp2[j] = t1[j];
				//				map[k + 4].Jsp2[j] = t2[j];
				//			}
				//}
			}
			else if(jnt.type == CONTACT)
			{
				Vector3 n = jnt.n;
				map[k].C = jnt.depth;			
				if(jnt.i != -1)
				{
					Vector3 a1xn = cross(a1,n);
					for(int j=0; j<3; j++)
					{
						map[k].Jsp1[j] = n[j];
						map[k].Jsp1[j + 3] = a1xn[j];
					}
				}
				if(jnt.j != -1)
				{
					Vector3 a2xn = cross(a2, n);
					for(int j=0; j<3; j++)
					{
						map[k].Jsp2[j] = -n[j];
						map[k].Jsp2[j + 3] = -a2xn[j];
					}
				}
			}
			else if(jnt.type == FRICTION){
				Vector3 t1 = jnt.t1;
				map[k].C = 0;			
				if(jnt.i != -1)
				{
					Vector3 a1xn = cross(a1, t1);
					for(int j=0; j<3; j++){				
						map[k].Jsp1[j] = t1[j];
						map[k].Jsp1[j + 3] = a1xn[j];
					}
				}
				if(jnt.j != -1)
				{
					Vector3 a2xn = cross(a2, t1);
					for(int j=0; j<3; j++){				
						map[k].Jsp2[j] = -t1[j];
						map[k].Jsp2[j + 3] = -a2xn[j];
					}
				}
				Vector3 t2 = jnt.t2;
				map[k + 1].C = 0;			
				if(jnt.i != -1)
				{
					Vector3 a1xn = cross(a1, t2);
					for(int j=0; j<3; j++){				
						map[k + 1].Jsp1[j] = t2[j];
						map[k + 1].Jsp1[j + 3] = a1xn[j];
					}
				}
				if(jnt.j != -1)
				{
					Vector3 a2xn = cross(a2, t2);
					for(int j=0; j<3; j++){				
						map[k + 1].Jsp2[j] = -t2[j];
						map[k + 1].Jsp2[j + 3] = -a2xn[j];
					}
				}
			}
			else if(jnt.type == DFRICTION){
				Vector3 t = jnt.n;			
				if(jnt.i != -1)
				{
					Vector3 a1xt = cross(a1, t);
					for(int j=0; j<3; j++){				
						map[k].Jf1[j] = t[j];
						map[k].Jf1[j + 3] = a1xt[j];
					}
				}
				if(jnt.j != -1)
				{
					Vector3 a2xt = cross(a2, t);
					for(int j=0; j<3; j++){				
						map[k].Jf2[j] = -t[j];
						map[k].Jf2[j + 3] = -a2xt[j];
					}
				}
			}
		}
		//compute B
		for(int i=0; i<s; i++)
		{
			//corpul 1
			if(map[i].i != -1)
			{
				Rigid* b1 = &rigids[map[i].i];
				Vector3 v1;
				for(int j=0; j<3; j++){
					map[i].Bsp1[j] = b1->invmass * (map[i].Jsp1[j] + mu * map[i].Jf1[j]);
					v1[j] = map[i].Jsp1[j+3] + mu * map[i].Jf1[j+3];
				}
				v1 = b1->Iinv * v1;
				for(int j=0; j<3; j++)
					map[i].Bsp1[j+3] = v1[j];
			}
			//corpul 2
			if(map[i].j != -1)
			{
				Rigid* b2 = &rigids[map[i].j];
				Vector3 v2;
				for(int j=0; j<3; j++){			
					map[i].Bsp2[j] = b2->invmass * (map[i].Jsp2[j] + mu * map[i].Jf2[j]);
					v2[j] = map[i].Jsp2[j+3] + mu * map[i].Jf2[j+3];
				}
				v2 = b2->Iinv * v2;
				for(int j=0; j<3; j++)
					map[i].Bsp2[j+3] = v2[j];			
			}
		}
	}

	int RigidSystem::AddRigid(Rigid& rigid)
	{
		if(rigid.invmass != 0)
			rigid.F.Y() = g / rigid.invmass;
		rigids.push_back(rigid);	
		return rigids.size() - 1;
	}

	int RigidSystem::AddContact(int i, int j, Vector3 a1, Vector3 a2, Vector3 n, float depth)
	{
		Joint jnt;
		jnt.type = CONTACT;
		jnt.i = i;
		jnt.j = j;
		jnt.k = map.size();
		jnt.a1 = a1;
		jnt.a2 = a2;
		jnt.depth = depth;
		jnt.n = n;
		joints.push_back(jnt);

		Constraint cstr;
		cstr.type = CONTACT;
		cstr.i = jnt.i;
		cstr.j = jnt.j;
		cstr.beta = beta;
		cstr.lmax = INFINITY;
		cstr.lmin = 0;
		map.push_back(cstr);

		return jnt.k;
	}

	bool RigidSystem::CollideRigids(int i1, int i2)
	{
		bool coll = false;
		Rigid* A = &rigids[i1];
		Rigid* B = &rigids[i2];
		for (size_t i = 0; i < A->shapes.size(); i++)
		{
			for (size_t j = 0; j < B->shapes.size(); j++)
			{
				auto shape1 = A->shapes[i].get();
				auto shape2 = B->shapes[j].get();
				shape1->X = A->X + A->R * shape1->T;
				shape1->R = A->R * shape1->O; //compunere de rotatii
				shape2->X = B->X + B->R * shape2->T;
				shape2->R = B->R * shape2->O;
				if (CollideShapes(shape1, shape2, i1, i2))
					coll = true;
			}
		}
		return coll;
	}

	void Project(Rigid& A, Rigid& B, Vector3 N, float depth, float h,
		const Vector3& a1, const Vector3& a2)
	{
		Vector3 disp = N * depth;
		if(A.invmass == 0)
		{
			B.X -= disp;
			B.v -= disp * h;
			B.w -= cross(a2, disp * h); 
		}
		else if(B.invmass == 0)
		{
			A.X += disp;
		}
		else
		{
			float t = B.invmass / (A.invmass + B.invmass);
			A.X += disp * (1.0f - t);
			B.X -= disp * t;
		}
	}

	void RigidSystem::DetectCollision()
	{
		contacts.clear();
		int n = rigids.size();
		for (int i=0;i<n-1;i++)
		{
			for (int j=i+1;j<n;j++)
			{
				CollideRigids(i, j);
			}
		}

		joints.clear();
		int num_contacts = contacts.size();
		for(int i=0; i<num_contacts; i++)
		{
			Contact info = contacts[i];
			Rigid& A = *info.A;
			Rigid& B = *info.B;
			Vector3 n = info.n;
			Vector3 a1 = info.pa - A.X;
			Vector3 a2 = info.pb - B.X;
			info.a1 = !(A.R) * a1;
			info.a2 = !(B.R) * a2;
			float d = fabs((info.pa - info.pb) * n);

			AddContact(info.i1, info.i2, info.a1, info.a2, info.n, d);
			
			//if (d < 0.0001)
			//	continue;
			//Vector3 vrel = (A.v + cross(A.w, a1) - B.v - cross(B.w, a2));
			//float vn = vrel * n;
			//if(vn > 0)
			//	continue;		
			//Vector3 vrt = vrel - vn * n;
			//float vt = vrt.Length();
			////if(d > 1/*0.01 || vn < -20*/)
			////{
			////	Project(A, B, n, d, h, a1, a2);
			////	//float restitution = 0;
			////	//float num = -(1 + restitution) * vn;
			////	//float t1, t2, t3, t4;
			////	//t1 = A.invmass;
			////	//t2 = B.invmass;
			////	//t3 = n * cross((A.Iinv * cross(a1, n)), a1);
			////	//t4 = n * cross((B.Iinv * cross(a2, n)), a2);
			////	//float j = num / (t1 + t2 + t3 + t4);			
			////	//Vector3 p = j * n;				
			////	//if(vt != 0){
			////	//	Vector3 t = -1/vt * vrt;
			////	//	p += MU_IMP * j * t;
			////	//}
			////	//if(A.invmass != 0){
			////	//	A.v += A.invmass * p;			
			////	//	A.w += A.Iinv * cross(a1, p);
			////	//}
			////	//if(B.invmass != 0){
			////	//	B.v -= B.invmass * p;
			////	//	B.w -= B.Iinv * cross(a2, p);
			////	//}
			////}
			////else
			//{
			//	int k = AddContact(info.i1, info.i2, info.a1, info.a2, info.n, d);
			//	// dynamic friction
			//	//if(vt != 0){
			//	//	Vector3 t = -1/vt * vrt;
			//	//	addDFriction(info.A, info.B, info.a1, info.a2, t, k);					
			//	//}
			//	////frecare Catto
			//	//Vector3 t1, t2;
			//	//if(GetTangents(info.n, t1, t2)){
			//	//	addFriction(info.A, info.B, info.a1, info.a2, t1, t2);
			//	//}			
			//}
		}		
	}

	bool RigidSystem::CollideShapes(Rigid::Shape* shape1, Rigid::Shape* shape2, int i1, int i2)
	{
		RigidSystem::Contact info;
		info.i1 = i1;
		info.i2 = i2;
		info.A = &rigids[i1];
		info.B = &rigids[i2];
		bool coll = false;
		if (shape1->type == Rigid::BOX && shape2->type == Rigid::BOX)
		{
			if (BoxBox(*(Rigid::Box*)shape1, *(Rigid::Box*)shape2, info))
			{
				contacts.push_back(info);					
				coll = true;
			}
		}
		else if (shape1->type == Rigid::SPHERE && shape2->type == Rigid::SPHERE)
		{
			Rigid::Sphere* s1 = (Rigid::Sphere*)shape1;
			Rigid::Sphere* s2 = (Rigid::Sphere*)shape2;
			if(SphereSphere(s1, s2, info)){
				coll = true;
				contacts.push_back(info);
			}
		}
		else if(shape1->type == Rigid::SPHERE && shape2->type == Rigid::BOX)
		{
			info.i1 = i2;
			info.i2 = i1;
			info.A = &rigids[i2];
			info.B = &rigids[i1];
			if (BoxSphere(*(Rigid::Box*)shape2, *(Rigid::Sphere*)shape1, info)){
				contacts.push_back(info);
				coll = true;
			}
		}
		else if(shape1->type == Rigid::BOX && shape2->type == Rigid::SPHERE)
		{
			if( BoxSphere(*(Rigid::Box*)shape1, *(Rigid::Sphere*)shape2, info) ){
				contacts.push_back(info);
				coll = true;
			}
		}
		return coll;
	}
}