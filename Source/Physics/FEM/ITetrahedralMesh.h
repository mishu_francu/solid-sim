#pragma once

#include <Engine/Types.h>
#include "StridedVector.h"

namespace FEM_SYSTEM
{
	class ITetrahedralMesh
	{
	public:
		virtual void InstanceWith(int order, int noPoints, StridedVector<uint32> connectivity, int noElements) = 0;
		virtual int GetNumNodes() const = 0;
		virtual int GetNumNodesPerElement() const = 0;
		virtual int GetNumElements() const = 0;
		virtual int GetNodesPerEdge(int) const = 0;
		virtual int GetNodesPerFace(int) const = 0;
		virtual int GetGlobalIndex(int eidx, int lidx) = 0;
		virtual int* GetIJKL(int lidx) = 0;
		virtual int GetOrder() const = 0;
	};
}

