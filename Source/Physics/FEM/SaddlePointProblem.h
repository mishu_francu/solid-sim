#pragma once

#include "MixedProblems.h"

namespace FEM_SYSTEM
{
	class SaddlePointProblem : public BaseProblem
	{
	public:
		enum SolverType
		{
			SPP_ST_DIRECT,
			SPP_ST_ITERATIVE,
			SPP_ST_SCHUR_COMPLIANCE,
			SPP_ST_SCHUR_MASS,
		};
	public:
		SaddlePointProblem(FemPhysicsMixed& source, int solverType) : BaseProblem(source), mSolverType(solverType)
		{
			if (mSolverType == SPP_ST_SCHUR_COMPLIANCE)
			{
				EigenMatrix C(mFPM.mVolComplianceMatrix);
				mCinv = C.inverse().sparseView();
			}
		}
		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, SparseMatrix& K);
		EigenVector SolveLinearSystem(EigenMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y);
		EigenVector SolveLinearSystem(SparseMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y);
		real MeritResidual(const EigenVector& rhs) const;

	private:
		SparseMatrix mCinv; // inverse compliance matrix
		int mSolverType = SPP_ST_DIRECT;
		EigenMatrix mKinv; // inverse stiffness matrix
	};
}
