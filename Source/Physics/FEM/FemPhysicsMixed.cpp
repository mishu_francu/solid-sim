#include "FemDataStructures.h"
#include "FemPhysicsMixed.h"
#include "ElasticEnergy.h"
#include "NewtonSolver.h"
#include "NonlinearCG.h"
#include "FemCollisions.h"
#ifdef USE_GSL
#include <gsl/gsl_multiroots.h>
#endif

#include "MixedProblems.h"
#include "SaddlePointProblem.h"

namespace FEM_SYSTEM
{

	FemPhysicsMixed::FemPhysicsMixed(std::vector<Tet>& tetrahedra, std::vector<Node>& allNodes, const FemConfig& config) 
		: FemPhysicsLinearIncompressible(tetrahedra, allNodes, config)
	{
		std::fill(mPressures.begin(), mPressures.end(), 0); // zero pressures
	}

	void FemPhysicsMixed::Step(real dt)
	{
		mPreviousPositions = mDeformedPositions;

		if (mSimType == ST_STATIC)
		{
			if (mForceFraction == 0)
			{
				mForceFraction = 1;
				if (mConfig.mSolver == NST_MIXED_NEWTON || mConfig.mSolver == NST_NEWTON)
					SolveSaddlePoint();
				else if (mConfig.mSolver == NST_NEWTON_LS)
					SolveSaddlePointLS();
			}
		}
		else if (mSimType == ST_QUASI_STATIC)
		{
			if (mForceFraction < 1)
			{
				mForceFraction = std::min(real(1), mForceFraction + mForceStep);
				//SolvePrincipalStretches();
				//SolveThreeField();
				//SolveCondensedNewton();
				if (mConfig.mSolver == NST_MIXED_NEWTON || mConfig.mSolver == NST_NEWTON)
					SolveSaddlePoint();
				else if (mConfig.mSolver == NST_NEWTON_LS)
					SolveSaddlePointLS();
#ifdef USE_GSL
				else if (mConfig.mSolver == NST_GSL)
					SolveSaddlePointGSL();
#endif
				else if (mConfig.mSolver == NST_MIXED_DUAL_ASCENT)
					SolveDualAscent();

				EigenVector errors(GetNumPressureNodes());
				GetCurrentVolumeErrors(errors, true);
			}
		}
		else if (mSimType == ST_IMPLICIT)
		{
			mForceFraction = 1;
			real h = dt / mNumSteps;
			for (int i = 0; i < mNumSteps; i++)
			{
				mTimeStep = h;
				SolveSaddlePoint();
				//SolveDualAscent();
				HandleCollisions(h);
			}
		}
	}

	void FemPhysicsMixed::SolveDualAscent()
	{
		MEASURE_TIME("Mixed FEM solve");

		AssembleDynamicContributions();

		uint32 numContacts = 0;
		if (!mCondenseContact)
			numContacts = GetNumContacts();
		uint32 numConstraints = GetNumPressureNodes() + numContacts;

		EigenVector sx, yx;
		EigenMatrix Kinv;
		//std::fill(mPressures.begin(), mPressures.end(), 0); // uncommnet to disable warm-starting
		mContactMultipliers.resize(numContacts);
		mContactMultipliers.setZero();

		//mLameLambda = 1e6; // ALM for standard materials
		mStepLength = 1e6;
		real alphaALM = 1e7;

		EigenVector errors(numConstraints);
		real totalErr = GetCurrentVolumeErrors(errors);
		//Printf("error norm %g\n", errors.lpNorm<1>());
		Printf("l1 err: %.2f%%\n", errors.lpNorm<1>() * 100 / mTotalInitialVol);

		for (uint32 iter = 0; iter < mOuterIterations; iter++)
		{
			uint32 size = GetNumFreeNodes() * NUM_POS_COMPONENTS;
			SolvePrimalNewton();

			EigenVector errors(numConstraints);
			real totalErr = GetCurrentVolumeErrors(errors);

			// add contact depth
			if (numContacts)
				errors.tail(numContacts) = mContactDepth; // TODO: compute only now


			real res = errors.lpNorm<1>() / mTotalInitialVol;
			//Printf("error norm %g\n", res);
			Printf("l1 err: %.2f%%\n", res * 100);
			if (abs(res) < 0.01) // less than 1%
			{
				Printf("Converged at iteration %d\n", iter + 1);
				break;
			}

			// compute dual Hessian
#ifndef ALM
			EigenMatrix K;
			ComputeStiffnessMatrix(K, false);
#else
			UnconstrainedProblem probl(*this);
			EigenMatrix K = probl.ComputeSystemMatrix(sx, yx);
#endif
			
			// assemble Jacobian
			EigenMatrix J(numConstraints, size);
			J.block(0, 0, GetNumPressureNodes(), size) = mVolJacobianMatrix;
			if (numContacts != 0)
				J.block(GetNumPressureNodes(), 0, numContacts, size) = mContactJacobian;

			// compute S * errors
			auto b = J.transpose() * errors;
			LinearSolver solver;
			solver.Init(K, LST_LU_PARDISO);
			auto z = solver.Solve(b);
			auto Serrors = J * z;

			// TODO: use MINRES formula
			// compute steepest descent alpha
			real Snorm = errors.transpose() * Serrors;
			real norm = errors.transpose() * errors;
			real alphaSD = norm / Snorm;
			//Printf("alpha SD: %g\n", alphaSD);

			real alpha = alphaSD; // 1e7;
			//alphaALM *= 1.5;
			for (uint32 i = 0; i < GetNumPressureNodes(); i++)
			{
				mPressures[i] += alpha * errors(i);
			}

			//mContactMultipliers += alpha * errors.tail(numContacts);
			for (uint32 i = 0; i < numContacts; i++)
			{
				real lambda = mContactMultipliers(i);
				real dLambda = alpha * errors(GetNumPressureNodes() + i);
				real newLambda = lambda + dLambda;
				if (newLambda > 0)
					newLambda = 0;
				dLambda = newLambda - lambda;
				mContactMultipliers(i) += dLambda;
			}
		}

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}
	}

	void FemPhysicsMixed::SolveSaddlePoint()
	{
		MEASURE_TIME("Mixed FEM solve");
		PROFILE_SCOPE("Mixed");

		uint32 numNodes = GetNumFreeNodes();
		uint32 numPNodes = GetNumPressureNodes();
		uint32 numContacts = GetNumContacts();
		uint32 numConstraints = mCondenseContact ? numPNodes : numPNodes + numContacts;
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numConstraints;

		AssembleDynamicContributions();

		NewtonSolver<SaddlePointProblem, SparseMatrix> solver;
		solver.mNumIterations = mOuterIterations;
		solver.mVerbose = mVerbose ? VL_DETAILED : VL_NONE;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mSolverType = LST_LDLT_PARDISO;
		SaddlePointProblem problem(*this, SaddlePointProblem::SPP_ST_DIRECT);

		// prepare the initial guess with the current configuration
		EigenVector solution(size);
		solution.setZero(); // initialize Lagrange multipliers with zero
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			const Vector3R& p = mDeformedPositions[i];
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
			solution(i * NUM_POS_COMPONENTS + 2) = p.z;
		}
		// warm-starting
		for (uint32 i = 0; i < numPNodes; i++)
		{
			solution(numDofs + i) = mPressures[i];
		}

		solver.Solve(problem, size, solution);

		// update deformed positions
		Vector3Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			mDeformedPositions[i] = u[i];
		}

		// update pressure
		auto p = solution.tail(numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			mPressures[i] = p[i];
		}

		CheckForInversion(true);

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}
	}

	void FemPhysicsMixed::SolveSaddlePointLS()
	{
		MEASURE_TIME("Mixed FEM solve");
		PROFILE_SCOPE("Mixed");

		uint32 numNodes = GetNumFreeNodes();
		uint32 numPNodes = GetNumPressureNodes();
		uint32 numContacts = GetNumContacts();
		uint32 numConstraints = mCondenseContact ? numPNodes : numPNodes + numContacts;
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numConstraints;

		AssembleDynamicContributions();

		NewtonSolverBackTrack<SaddlePointProblem, SparseMatrix> solver;
		solver.mNumIterations = mOuterIterations;
		solver.mVerbose = mVerbose ? VL_MINIMUM : VL_NONE;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mUseProblemSolver = true;
		SaddlePointProblem problem(*this, SaddlePointProblem::SPP_ST_DIRECT);

		// prepare the initial guess with the current configuration
		EigenVector solution(size);
		solution.setZero(); // initialize Lagrange multipliers with zero
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			const Vector3R& p = mDeformedPositions[i];
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
			solution(i * NUM_POS_COMPONENTS + 2) = p.z;
		}
		// warm-starting
		for (uint32 i = 0; i < numPNodes; i++)
		{
			solution(numDofs + i) = mPressures[i];
		}

		solver.Solve(problem, size, solution);

		// update deformed positions
		Vector3Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			mDeformedPositions[i] = u[i];
		}

		// update pressure
		auto p = solution.tail(numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			mPressures[i] = p[i];
		}

		CheckForInversion(true);

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}
	}

#ifdef USE_GSL
	int gslfunc(const gsl_vector* x, void* params, gsl_vector* f)
	{
		FemPhysicsMixed* FPM = (FemPhysicsMixed*)params;
		uint32 numNodes = FPM->GetNumFreeNodes();
		uint32 numPNodes = FPM->GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		// update deformed positions
		for (uint32 i = 0; i < numNodes; i++)
		{
			Vector3R u;
			u.x = gsl_vector_get(x, i * NUM_POS_COMPONENTS);
			u.y = gsl_vector_get(x, i * NUM_POS_COMPONENTS + 1);
			u.z = gsl_vector_get(x, i * NUM_POS_COMPONENTS + 2);
			FPM->mDeformedPositions[i] = u;
		}

		// update pressure
		EigenVector p(numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			real val = gsl_vector_get(x, numDofs + i);
			FPM->mPressures[i] = val;
			p(i) = val;
		}

		// assemble Jacobian matrix
		FPM->AssembleJacobianMatrix(FPM->mVolJacobianMatrix, false, true);

		// assemble right hand side
		auto rhs1 = FPM->ComputePosRhs();
		for (uint32 i = 0; i < numDofs; i++)
			gsl_vector_set(f, i, -rhs1(i));

		// start with the presssure components
		EigenVector errors(numPNodes); // vector used to accumulate volume errors
		real totalErr = FPM->GetCurrentVolumeErrors(errors);
		auto rhs2 = -errors + FPM->mVolComplianceMatrix * p;
		for (uint32 i = 0; i < numPNodes; i++)
			gsl_vector_set(f, numDofs + i, -rhs2(i));

		real res = sqrt(rhs1.squaredNorm() + rhs2.squaredNorm());
		Printf("%g\n", res);

		return GSL_SUCCESS;
	}

	int	gslfunc_df(const gsl_vector* x, void* p, gsl_matrix* J)
	{
		FemPhysicsMixed* FPM = (FemPhysicsMixed*)p;
		uint32 numNodes = FPM->GetNumFreeNodes();
		uint32 numPNodes = FPM->GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		// update deformed positions
		for (uint32 i = 0; i < numNodes; i++)
		{
			Vector3R u;
			u.x = gsl_vector_get(x, i * NUM_POS_COMPONENTS);
			u.y = gsl_vector_get(x, i * NUM_POS_COMPONENTS + 1);
			u.z = gsl_vector_get(x, i * NUM_POS_COMPONENTS + 2);
			FPM->mDeformedPositions[i] = u;
		}

		// update pressure
		for (uint32 i = 0; i < numPNodes; i++)
		{
			real val = gsl_vector_get(x, numDofs + i);
			FPM->mPressures[i] = val;
		}

		// assemble Jacobian matrix
		FPM->AssembleJacobianMatrix(FPM->mVolJacobianMatrix, false, true);

		// the system matrix
		EigenMatrix K(size, size);

		FPM->ComputeStiffnessMatrix(K.block(0, 0, numDofs, numDofs));
		K.block(0, numDofs, numDofs, numPNodes) = FPM->mVolJacobianMatrix.transpose();
		K.block(numDofs, 0, numPNodes, numDofs) = FPM->mVolJacobianMatrix;
		K.block(numDofs, numDofs, numPNodes, numPNodes) = -FPM->mVolComplianceMatrix;

		for (uint32 i = 0; i < size; i++)
			for (uint32 j = 0; j < size; j++)
				gsl_matrix_set(J, i, j, K(i, j));

		return GSL_SUCCESS;
	}

	int gslfunc_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J)
	{
		FemPhysicsMixed* FPM = (FemPhysicsMixed*)params;
		uint32 numNodes = FPM->GetNumFreeNodes();
		uint32 numPNodes = FPM->GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		// update deformed positions
		for (uint32 i = 0; i < numNodes; i++)
		{
			Vector3R u;
			u.x = gsl_vector_get(x, i * NUM_POS_COMPONENTS);
			u.y = gsl_vector_get(x, i * NUM_POS_COMPONENTS + 1);
			u.z = gsl_vector_get(x, i * NUM_POS_COMPONENTS + 2);
			FPM->mDeformedPositions[i] = u;
		}

		// update pressure
		EigenVector p(numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			real val = gsl_vector_get(x, numDofs + i);
			FPM->mPressures[i] = val;
			p(i) = val;
		}

		// assemble Jacobian matrix
		FPM->AssembleJacobianMatrix(FPM->mVolJacobianMatrix, false, true);

		// assemble right hand side
		auto rhs1 = FPM->ComputePosRhs();
		for (uint32 i = 0; i < numDofs; i++)
			gsl_vector_set(f, i, -rhs1(i));

		// start with the presssure components
		EigenVector errors(numPNodes); // vector used to accumulate volume errors
		real totalErr = FPM->GetCurrentVolumeErrors(errors);
		auto rhs2 = -errors + FPM->mVolComplianceMatrix * p;
		for (uint32 i = 0; i < numPNodes; i++)
			gsl_vector_set(f, numDofs + i, -rhs2(i));

		real res = sqrt(rhs1.squaredNorm() + rhs2.squaredNorm());
		Printf("%g\n", res);

		// the system matrix
		EigenMatrix K(size, size);

		FPM->ComputeStiffnessMatrix(K.block(0, 0, numDofs, numDofs));
		K.block(0, numDofs, numDofs, numPNodes) = FPM->mVolJacobianMatrix.transpose();
		K.block(numDofs, 0, numPNodes, numDofs) = FPM->mVolJacobianMatrix;
		K.block(numDofs, numDofs, numPNodes, numPNodes) = -FPM->mVolComplianceMatrix;

		for (uint32 i = 0; i < size; i++)
			for (uint32 j = 0; j < size; j++)
				gsl_matrix_set(J, i, j, K(i, j));

		return GSL_SUCCESS;
	}

	void FemPhysicsMixed::SolveSaddlePointGSL()
	{
		MEASURE_TIME("Mixed FEM solve");
		PROFILE_SCOPE("Mixed");

		uint32 numNodes = GetNumFreeNodes();
		uint32 numPNodes = GetNumPressureNodes();
		uint32 numContacts = (uint32)mCollIndices.size();
		uint32 numConstraints = mCondenseContact ? numPNodes : numPNodes + numContacts;
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numConstraints;

		AssembleDynamicContributions();

#ifndef GSL_NO_DERIVATIVE
		const gsl_multiroot_fdfsolver_type* T = gsl_multiroot_fdfsolver_gnewton;
		gsl_multiroot_fdfsolver* s = gsl_multiroot_fdfsolver_alloc(T, size);
#else
		const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_dnewton;
		gsl_multiroot_fsolver* s = gsl_multiroot_fsolver_alloc(T, size);
#endif

		// prepare the initial guess with the current configuration
		gsl_vector* x = gsl_vector_alloc(size);
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			const Vector3R& p = mDeformedPositions[i];
			gsl_vector_set(x, i * NUM_POS_COMPONENTS, p.x);
			gsl_vector_set(x, i * NUM_POS_COMPONENTS + 1, p.y);
			gsl_vector_set(x, i * NUM_POS_COMPONENTS + 2, p.z);
		}
		// warm-starting
		for (uint32 i = 0; i < numPNodes; i++)
		{
			gsl_vector_set(x, numDofs + i, mPressures[i]);
		}

#ifndef GSL_NO_DERIVATIVE
		gsl_multiroot_function_fdf f = { &gslfunc, &gslfunc_df, &gslfunc_fdf, size, this };
		gsl_multiroot_fdfsolver_set(s, &f, x);
#else
		gsl_multiroot_function f = { &gslfunc, size, this };
		gsl_multiroot_fsolver_set(s, &f, x);
#endif

		int iter = 0;
		int status;
		do
		{
			iter++;
#ifndef GSL_NO_DERIVATIVE
			status = gsl_multiroot_fdfsolver_iterate(s);
#else
			status = gsl_multiroot_fsolver_iterate(s);
#endif

			//print_state(iter, s);

			if (status)   /* check if solver is stuck */
				break;

			status = gsl_multiroot_test_residual(s->f, mAbsNewtonResidualThreshold);
		} while (status == GSL_CONTINUE && iter < (int)mOuterIterations);

		Printf("status = %s\n", gsl_strerror(status));
		Printf("iter = %d\n", iter + 1);

		// update deformed positions
		for (uint32 i = 0; i < numNodes; i++)
		{
			Vector3R u;
			u.x = gsl_vector_get(s->x, i * NUM_POS_COMPONENTS);
			u.y = gsl_vector_get(s->x, i * NUM_POS_COMPONENTS + 1);
			u.z = gsl_vector_get(s->x, i * NUM_POS_COMPONENTS + 2);
			mDeformedPositions[i] = u;
		}

		// update pressure
		for (uint32 i = 0; i < numPNodes; i++)
		{
			real val = gsl_vector_get(s->x, numDofs + i);
			mPressures[i] = val;
		}

#ifndef GSL_NO_DERIVATIVE
		gsl_multiroot_fdfsolver_free(s);
#else
		gsl_multiroot_fsolver_free(s);
#endif
		gsl_vector_free(x);

		CheckForInversion(true);

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}
	}
#endif // USE_GSL

	void FemPhysicsMixed::SolveThreeField()
	{
		//MEASURE_TIME("Mixed FEM solve");
		//PROFILE_SCOPE("Mixed");

		uint32 numNodes = GetNumFreeNodes();
		uint32 numPNodes = GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes * 2;

		AssembleDynamicContributions();

		NewtonSolverBackTrack<ThreeFieldProblem> solver;
		solver.mNumIterations = mOuterIterations;
		solver.mVerbose = VL_NONE;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mUseProblemSolver = true;
		solver.mUseBFGS = false;
		ThreeFieldProblem problem(*this);

		// prepare the initial guess with the current configuration
		EigenVector solution(size);
		solution.setZero(); // initialize Lagrange multipliers with zero
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			const Vector3R& p = mDeformedPositions[i];
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
			solution(i * NUM_POS_COMPONENTS + 2) = p.z;
		}
		// warm-starting
		for (uint32 i = 0; i < numPNodes; i++)
		{
			solution(numDofs + i) = mPressures[i];
		}
		// volumes
		real factor = mPressureOrder == 1 ? 0.25f : 1;
		EigenVector deformedVolumes(numPNodes);
		deformedVolumes.setZero();
		for (uint32 e = 0; e < GetNumElements(); e++)
		{
			const Vector3R& x0 = GetDeformedPosition(mTetMesh->GetGlobalIndex(e, 0));
			const Vector3R& x1 = GetDeformedPosition(mTetMesh->GetGlobalIndex(e, 1));
			const Vector3R& x2 = GetDeformedPosition(mTetMesh->GetGlobalIndex(e, 2));
			const Vector3R& x3 = GetDeformedPosition(mTetMesh->GetGlobalIndex(e, 3));
			Vector3R d1 = x1 - x0;
			Vector3R d2 = x2 - x0;
			Vector3R d3 = x3 - x0;
			Matrix3R mat(d1, d2, d3); // this is the spatial shape matrix Ds [Sifakis][Teran]
			real vol = (mat.Determinant()) / 6.f; // volume of the tet

			for (uint32 i = 0; i < GetNumLocalPressureNodes(); i++)
			{
				uint32 globalI = GetPressureGlobalIndex(e, i);
				deformedVolumes[globalI] += factor * vol;
			}
		}
		solution.tail(numPNodes) = deformedVolumes;

		solver.Solve(problem, size, solution);

		// update deformed positions
		Vector3Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			mDeformedPositions[i] = u[i];
		}

		// update pressure
		auto p = solution.segment(numDofs, numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			mPressures[i] = p[i];
		}

		// update volumes
		mVolStretches = solution.tail(numPNodes);

		CheckForInversion(true);

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}
	}

	void FemPhysicsMixed::SolvePrincipalStretches()
	{
		uint32 numNodes = GetNumFreeNodes();
		uint32 numPS = GetNumElements() * 3;
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPS * 2;

		AssembleDynamicContributions();

		NewtonSolver<PrincipalStretchesProblem> solver;
		solver.mNumIterations = mOuterIterations;
		solver.mVerbose = VL_DETAILED;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mSolverType = LST_LU_PARDISO;
		solver.mUseFiniteDiff = true;
		//solver.mUseProblemSolver = true;
		//solver.mUseBFGS = false;
		PrincipalStretchesProblem problem(*this);

		// prepare the initial guess with the current configuration
		EigenVector solution(size);
		solution.setZero(); // initialize Lagrange multipliers with zero
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			const Vector3R& p = mDeformedPositions[i];
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
			solution(i * NUM_POS_COMPONENTS + 2) = p.z;
		}
		// TODO: compute principal stretches
		solution.segment(numDofs, numPS).setOnes(); // set all principal stretches to 1
		solution.tail(numPS).setOnes();
		for (uint32 i = 0; i < numPS; i++)
		{
			solution(numDofs + i) = 0.9;
			solution(numDofs + numPS + i) = 0.1;
		}

		//// test the RHS
		//solution(9) = 0.099994;
		//solution(10) = -0.051281;
		//solution(11) = 0.05004;
		////solution(12) = 1.00566;
		////solution(13) = 0.99996;
		////solution(14) = 0.99452;
		//auto rhs = problem.ComputeRhs(solution);

		solver.Solve(problem, size, solution);

		// update deformed positions
		Vector3Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			mDeformedPositions[i] = u[i];
		}

		CheckForInversion(true);

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}
	}

	PrimalProblem FemPhysicsMixed::SolvePrimalNewton()
	{
		AssembleDynamicContributions();

		NewtonSolverBackTrack<PrimalProblem> solver;
		solver.mNumIterations = 100;
		solver.mVerbose = VL_NONE;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mUseProblemSolver = false;
		solver.mUseBFGS = false;

		// prepare the initial guess with the current configuration
		uint32 size = GetNumFreeNodes() * NUM_POS_COMPONENTS;
		EigenVector solution(size);
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			const Vector3R& p = mDeformedPositions[i];
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
			solution(i * NUM_POS_COMPONENTS + 2) = p.z;
		}
		
		PrimalProblem problem(*this);
		solver.Solve(problem, size, solution);

		// set node positions
		auto solVecs = GetStdVector(solution);
		ASSERT(solVecs.size() == GetNumFreeNodes());
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			mDeformedPositions[i] = solVecs[i];
		}

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}

		return problem;
	}

	void FemPhysicsMixed::SolveStandardNewton()
	{
		AssembleDynamicContributions();

		NewtonSolverBackTrack<StandardProblem> solver;
		solver.mNumIterations = mOuterIterations;
		solver.mVerbose = VL_NONE;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mUseProblemSolver = false;
		solver.mUseBFGS = false;

		// prepare the initial guess with the current configuration
		uint32 size = GetNumFreeNodes() * NUM_POS_COMPONENTS;
		EigenVector solution(size);
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			const Vector3R& p = mDeformedPositions[i];
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
			solution(i * NUM_POS_COMPONENTS + 2) = p.z;
		}

		StandardProblem problem(*this);
		solver.Solve(problem, size, solution);

		// set node positions
		auto solVecs = GetStdVector(solution);
		ASSERT(solVecs.size() == GetNumFreeNodes());
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			mDeformedPositions[i] = solVecs[i];
		}

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}
	}

	void FemPhysicsMixed::SolveCondensedNewton()
	{
		AssembleDynamicContributions();

		NewtonSolverBackTrack<CondensedProblem> solver;
		solver.mNumIterations = mOuterIterations;
		solver.mVerbose = VL_NONE;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mUseProblemSolver = false;
		solver.mUseBFGS = false;

		// prepare the initial guess with the current configuration
		uint32 size = GetNumFreeNodes() * NUM_POS_COMPONENTS;
		EigenVector solution(size);
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			const Vector3R& p = mDeformedPositions[i];
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
			solution(i * NUM_POS_COMPONENTS + 2) = p.z;
		}

		CondensedProblem problem(*this);
		solver.Solve(problem, size, solution);

		// set node positions
		auto solVecs = GetStdVector(solution);
		ASSERT(solVecs.size() == GetNumFreeNodes());
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			mDeformedPositions[i] = solVecs[i];
		}

		// compute new velocities
		if (mSimType == ST_IMPLICIT)
		{
			real invH = 1.f / mTimeStep;
			for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			{
				mVelocities[i] = invH * (mDeformedPositions[i] - mPreviousPositions[i]);
			}
		}
	}

	PrimalProblem FemPhysicsMixed::SolveUnconstrainedNCG()
	{
		EigenVector pos = GetEigenVector(mDeformedPositions);
		PrimalProblem problem(*this);
		NonlinearConjugateGradientMinimizer<PrimalProblem, EigenVector> minimizer;
		minimizer.mOuterIterations = 100;
		minimizer.mInnerIterations = 1;
		minimizer.mAbsResidualThreshold = mAbsNewtonResidualThreshold;
		minimizer.Solve(problem, GetNumFreeNodes() * 3, pos);

		return problem;
	}


	real FemPhysicsMixed::ComputeEnergy(int level)
	{
		// level 0 -> elastic
		// level 1 -> gravitational
		// level 2 -> vol
		// level 3 -> kinetic

		real val = 0;

		if (level == 0)
			val += ComputeElasticEnergy();


		// add the gravitational and kinetic part
		real invHSqr = mTimeStep != 0 ? 0.5f / mTimeStep / mTimeStep : 0;
		for (size_t i = 0; i < GetNumFreeNodes(); i++)
		{
			real mass = mMassMatrix.coeff(i, i);

			if (level == 1)
				val -= mass * mForceFraction * mGravity.y * mDeformedPositions[i].y; // gravitational

			//if (level == 3) // FIXME
			//	val += invHSqr * mass * mTotalDisplacements[i].LengthSquared(); // kinetic
		}

		for (int e = 0; e < (int)GetNumElements(); e++)
		{
			// + vol_0 * BulkModulus/2 * (J - 1)^2

			// compute nodal forces for this element only
			Matrix3R F;
			ComputeDeformationGradient(e, F);

			real J = F.Determinant();

			real vol_e = GetElementInitialVolume(e) * this->GetLameFirstParam() * 0.5f * ((J - 1)*(J - 1));

			if (level == 2)
				val += vol_e;
		}


		// add the pressure part (WIP)
		//for (size_t i = 0; i < mTractionSurface.size() / 3; i += 3)
		//{
		//	int base = i * 3;
		//	// global (shuffled) indices of nodes
		//	uint32 i1 = mTractionSurface[base];
		//	uint32 i2 = mTractionSurface[base + 1];
		//	uint32 i3 = mTractionSurface[base + 2];

		//	// compute triangle area and normal
		//	const Vector3R& p1 = GetDeformedPosition(i1);
		//	const Vector3R& p2 = GetDeformedPosition(i2);
		//	const Vector3R& p3 = GetDeformedPosition(i3);

		//	// compute volume formed with the origin
		//	real vol = (1.f / 6.f) * triple(p1, p2, p3);
		//	val += abs(vol) * mAppliedPressure;
		//}

		//Printf("energy: %f\n", val);
		return val;
	}


	real FemPhysicsMixed::GetCurrentVolumeErrors(EigenVector& errors, bool verbose)
	{
		real totalVol = 0;
		mTotalInitialVol = 0;
		real totalErr = 0;
		real factor = mPressureOrder == 1 ? 0.25f : 1;
		errors.setZero();
		for (uint32 e = 0; e < GetNumElements(); e++)
		{
			const Vector3R& x0 = GetDeformedPosition(mTetMesh->GetGlobalIndex(e, 0));
			const Vector3R& x1 = GetDeformedPosition(mTetMesh->GetGlobalIndex(e, 1));
			const Vector3R& x2 = GetDeformedPosition(mTetMesh->GetGlobalIndex(e, 2));
			const Vector3R& x3 = GetDeformedPosition(mTetMesh->GetGlobalIndex(e, 3));
			Vector3R d1 = x1 - x0;
			Vector3R d2 = x2 - x0;
			Vector3R d3 = x3 - x0;
			Matrix3R mat(d1, d2, d3); // this is the spatial shape matrix Ds [Sifakis][Teran]
			real vol = (mat.Determinant()) / 6.f; // volume of the tet
			mTotalInitialVol += mElementVolumes[e];
			real err;
			if (mLogConstraint)
			{
				real J = vol / mElementVolumes[e];
				err = mElementVolumes[e] * log(J);
			}
			else
				err = vol - mElementVolumes[e];
			totalVol += vol;
			totalErr += err;

			for (uint32 i = 0; i < GetNumLocalPressureNodes(); i++)
			{
				uint32 globalI = GetPressureGlobalIndex(e, i);
				errors[globalI] += factor * err;
			}
		}
		if (verbose)
		{
			Printf("vol err: %.4f%%\n", abs(totalErr) / mTotalInitialVol * 100);
		}
		return totalErr / mTotalInitialVol;
	}

	void FemPhysicsMixed::AssembleGeometricStiffnessMatrixFD(const EigenVector& p, EigenMatrix& Kgeom)
	{
		uint32 numNodes = GetNumFreeNodes();
		uint32 numDofs = numNodes * 3;
		Kgeom.resize(numDofs, numDofs);
		Kgeom.setZero();

		// current pressure forces
		auto fp0 = mVolJacobianMatrix.transpose() * p;

		auto J = mVolJacobianMatrix;
		real eps = 1e-6;
		for (uint32 j = 0; j < numDofs; j++)
		{
			mDeformedPositions[j / 3][j % 3] += eps;
			AssembleJacobianMatrix(J, false, true);
			auto fp = J.transpose() * p;
			auto dfp = (1.0 / eps) * (fp - fp0);
			for (uint32 i = 0; i < numDofs; i++)
			{
				Kgeom(i, j) = dfp(i);
			}
			mDeformedPositions[j / 3][j % 3] -= eps;
		}
	}

	void FemPhysicsMixed::AssembleGeometricStiffnessMatrix(const EigenVector& p, SparseMatrix& Kgeom) const
	{
		PROFILE_SCOPE("Geom stiff");

		size_t numNodes = GetNumFreeNodes();
		size_t numDofs = numNodes * 3;
		Kgeom.resize(numDofs, numDofs);
		Kgeom.setZero();
		std::vector<Eigen::Triplet<real>> triplets;
		
		uint32 numPressureLocalNodes = GetNumLocalPressureNodes();
		real factor = mPressureOrder == 1 ? 0.25f : 1.f;

		// go through all linear elements (tetrahedra)
		Matrix3R Z = Matrix3R::Zero();
		Eigen::Matrix<real, 12, 12> Klocal;
		for (int e = 0; e < (int)GetNumElements(); e++)
		{
			// compute local geometric stiffness matrix
			uint32 i0 = mTetMesh->GetGlobalIndex(e, 0);
			uint32 i1 = mTetMesh->GetGlobalIndex(e, 1);
			uint32 i2 = mTetMesh->GetGlobalIndex(e, 2);
			uint32 i3 = mTetMesh->GetGlobalIndex(e, 3);
			const Vector3R& x0 = GetDeformedPosition(i0);
			const Vector3R& x1 = GetDeformedPosition(i1);
			const Vector3R& x2 = GetDeformedPosition(i2);
			const Vector3R& x3 = GetDeformedPosition(i3);
			Vector3R d1 = x1 - x0;
			Vector3R d2 = x2 - x0;
			Vector3R d3 = x3 - x0;

			// prepare the building blocks
			Matrix3R K1 = Matrix3R::Skew(d1);
			Matrix3R K2 = Matrix3R::Skew(d2);
			Matrix3R K3 = Matrix3R::Skew(d3);

			// assemble the block matrix
			Matrix3R K[4][4];
			K[0][0] = Z; K[0][1] = K2 - K3; K[0][2] = K3 - K1; K[0][3] = K1 - K2;
			K[1][0] = K3 - K2; K[1][1] = Z; K[1][2] = -K3; K[1][3] = K2;
			K[2][0] = -K3 + K1; K[2][1] = K3; K[2][2] = Z; K[2][3] = -K1;
			K[3][0] = K2 - K1; K[3][1] = -K2; K[3][2] = K1; K[3][3] = Z;

			// convert it to an EigenMatrix
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					for (int x = 0; x < NUM_POS_COMPONENTS; x++)
					{
						for (int y = 0; y < NUM_POS_COMPONENTS; y++)
						{
							Klocal(i * NUM_POS_COMPONENTS + x, j * NUM_POS_COMPONENTS + y) = K[i][j](x, y);
						}
					}
				}
			}

			real factor1 = factor / 6;
			if (mLogConstraint)
			{
				Matrix3R mat(d1, d2, d3);
				real vol = mat.Determinant() / 6;
				real J = vol / mElementVolumes[e];
				factor1 = factor1 / J;

				Vector3R v[4];
				ComputeLocalJacobian(e, v, true);

				EigenMatrix Jlocal(1, 12);
				Jlocal.setZero();
				{
					// build Jacobian matrix
					for (uint32 k = 0; k < GetNumLocalNodes(); k++)
					{
						for (int l = 0; l < NUM_POS_COMPONENTS; l++)
						{
							Jlocal(0, k * NUM_POS_COMPONENTS + l) = v[k][l];
						}
					}
				}
				Klocal += -(1 / vol) * Jlocal.transpose() * Jlocal;
			}

			for (uint32 i = 0; i < numPressureLocalNodes; i++)
			{
				uint32 globalI = GetPressureGlobalIndex(e, i);
				// the local stiffness matrix Klocal is organized in 3x3 blocks for each pair of nodes
				for (uint32 j = 0; j < GetNumLocalNodes(); j++)
				{
					uint32 jGlobal = mReshuffleMap[mTetMesh->GetGlobalIndex(e, (int)j)];
					for (uint32 k = 0; k < GetNumLocalNodes(); k++)
					{
						uint32 kGlobal = mReshuffleMap[mTetMesh->GetGlobalIndex(e, (int)k)];
						if (jGlobal < mNumBCs || kGlobal < mNumBCs)
							continue;
						uint32 jOffset = (jGlobal - mNumBCs) * NUM_POS_COMPONENTS;
						uint32 kOffset = (kGlobal - mNumBCs) * NUM_POS_COMPONENTS;

						// add the the whole 3x3 block to the block matrix
						for (uint32 x = 0; x < NUM_POS_COMPONENTS; x++)
						{
							for (uint32 y = 0; y < NUM_POS_COMPONENTS; y++)
							{
								real val = factor1 * p[globalI] * Klocal(j * NUM_POS_COMPONENTS + x, k * NUM_POS_COMPONENTS + y);
								triplets.push_back(Eigen::Triplet<real>(jOffset + x, kOffset + y, val));
							}
						}
					}
				}
			}
		}
		Kgeom.setFromTriplets(triplets.begin(), triplets.end());
	}

	void FemPhysicsMixed::ComputeGradients(std::vector<Vector3R>& r, const EigenVector& dx, int material)
	{
		// compute negative gradient r = f(x) + M * ( v / h - dx / h^2 )
		for (size_t i = 0; i < r.size(); i++)
		{
			r[i].SetZero();
		}
		ElasticEnergy::ComputeForces(this, r, material);
		ComputeSpringForces(r);
		if (mSimType == ST_IMPLICIT)
		{
			const real invH = 1.f / mTimeStep;
			const real invHSqr = 1.f / (mTimeStep * mTimeStep);
			EigenVector inertia = mMassMatrix * (invH * GetEigenVector(mVelocities) - invHSqr * dx);
			auto stdInertia = GetStdVector(inertia);
			for (size_t i = 0; i < GetNumFreeNodes(); i++)
			{
				r[i + mNumBCs] += stdInertia[i];
			}
		}
	}

	template<class MATRIX>
	void FemPhysicsMixed::ComputeStiffnessMatrix(MATRIX& K, bool update)
	{
		real scale = mConfig.mConstraintScale;
		real invHSqr = 1.f / (mTimeStep * mTimeStep);
		
		if (update)
		{
			Eigen::Map<EigenVector> p((real*)&mPressures[0], mPressures.size(), 1);
			AssembleGeometricStiffnessMatrix(p, mGeometricStiffnessMatrix);
			// contacts and Dirichlet BCs have no geometric stiffness because they are linear constraints, i.e. the Hessian vanishes
			ElasticEnergy::AssembleStiffnessMatrix(this, mDeviatoricStiffnessMatrix);
		}

		K = mDeviatoricStiffnessMatrix + scale * mGeometricStiffnessMatrix;
		if (mSimType == ST_IMPLICIT)
			K += invHSqr * mMassMatrix;
		if (GetNumContacts() != 0 && mCondenseContact)
		{
			K += mContactStiffness * mContactJacobian.transpose() * mContactJacobian;
		}
		if (!mDirichletIndices.empty())
		{
			K += mDirichletStiffness * mDirichletJacobian.transpose() * mDirichletJacobian;
		}
		if (!mCables.empty())
		{
			SparseMatrix Ks;
			ComputeSpringStiffnessMatrix(Ks);
			K -= Ks;
		}
	}

	// explicit instantiations
	template void FemPhysicsMixed::ComputeStiffnessMatrix<EigenMatrix>(EigenMatrix& K, bool update);
	template void FemPhysicsMixed::ComputeStiffnessMatrix<SparseMatrix>(SparseMatrix& K, bool update);
	template void FemPhysicsMixed::ComputeStiffnessMatrix<Eigen::Block<EigenMatrix>>(Eigen::Block<EigenMatrix>& K, bool update);

	EigenMatrix FemPhysicsMixed::ComputeStdStiffnessMatrix(bool update)
	{
		real invHSqr = 1.f / (mTimeStep * mTimeStep);

		if (update)
		{
			// contacts and Dirichlet BCs have no geometric stiffness because they are linear constraints, i.e. the Hessian vanishes
			ElasticEnergy::AssembleStiffnessMatrix(this, mDeviatoricStiffnessMatrix);
		}

		EigenMatrix K = mDeviatoricStiffnessMatrix;
		if (mSimType == ST_IMPLICIT)
			K += invHSqr * mMassMatrix;
		if (GetNumContacts() != 0 && mCondenseContact)
		{
			K += mContactStiffness * mContactJacobian.transpose() * mContactJacobian;
		}
		//if (!mDirichletIndices.empty())
		//{
		//	K += dirichletStiffness * dirichletJacobian.transpose() * dirichletJacobian;
		//}
		if (!mCables.empty())
		{
			EigenMatrix Ks;
			ComputeSpringStiffnessMatrix(Ks);
			K -= Ks;
		}
		return K;
	}

	EigenVector FemPhysicsMixed::ComputeStdRhs(int material)
	{
		uint32 numDofs = GetNumFreeNodes() * NUM_POS_COMPONENTS;

		EigenVector rhs(numDofs);

		EigenVector f = mForceFraction * GetEigenVector(mBodyForces);
		rhs = f;

		ComputeTractionForces();
		if (mTractionForces.size() > 0)
			rhs += mForceFraction * GetEigenVector(mTractionForces);

		// elastic forces vector
		std::vector<Vector3R> fe(GetNumNodes());
		EigenVector disp = GetEigenVector(mDeformedPositions) - GetEigenVector(mPreviousPositions);
		ComputeGradients(fe, disp, material);
		rhs += GetEigenVector(fe, mNumBCs);

		return rhs;
	}

	EigenVector FemPhysicsMixed::ComputePosRhs()
	{
		uint32 numDofs = GetNumFreeNodes() * NUM_POS_COMPONENTS;
		uint32 numContacts = GetNumContacts();
		real scale = mConfig.mConstraintScale; // scaling of volume constraints

		EigenVector rhs = ComputeStdRhs();

		// add current pressure forces
		Eigen::Map<EigenVector> p((real*)&mPressures[0], mPressures.size(), 1);
		rhs.head(numDofs) -= scale * mVolJacobianMatrix.transpose() * p;

		// add current contact forces
		if (!mCondenseContact && numContacts != 0)
			rhs.head(numDofs) -= mContactJacobian.transpose() * mContactMultipliers;

		// construct the contact depth vector
		if (numContacts != 0)
		{
			mContactDepth.resize(numContacts);
			for (uint32 i = 0; i < numContacts; i++)
			{
				// the i'th contact
				const Vector3R& normal = mFemCollision->mCollNormals[i];
				uint32 idx = mFemCollision->mCollIndices[i] - mNumBCs; // affecting node idx
				// compute the penetration depth
				mContactDepth(i) = dot(normal, mDeformedPositions[idx] - mFemCollision->mCollPointsB[i]);
			}
		}

		// add penalty contact forces
		if (numContacts != 0 && mCondenseContact)
		{
			rhs.head(numDofs) -= mContactStiffness * mContactJacobian.transpose() * mContactDepth;
		}

		// add penalty Dirichlet BC forces
		if (!mDirichletIndices.empty())
		{
			// count the constraints
			int count = 0;
			for (size_t i = 0; i < mDirichletIndices.size(); i++)
			{
				if (mDirichletAxes[i] & AXIS_X) count++;
				if (mDirichletAxes[i] & AXIS_Y) count++;
				if (mDirichletAxes[i] & AXIS_Z) count++;
			}

			EigenVector dirichletErrors(count);
			count = 0;
			for (uint32 i = 0; i < mDirichletIndices.size(); i++)
			{
				// the i'th BC				
				uint32 flags = mDirichletAxes[i];
				uint32 idx0 = mDirichletIndices[i];
				uint32 idx = idx0 - mNumBCs; // affecting node idx
				// compute the BC error
				if (flags & AXIS_X)
				{
					dirichletErrors(count++) = mDeformedPositions[idx].x - mReferencePositions[idx0].x;
				}
				if (flags & AXIS_Y)
				{
					dirichletErrors(count++) = mDeformedPositions[idx].y - mReferencePositions[idx0].y;
				}
				if (flags & AXIS_Z)
				{
					dirichletErrors(count++) = mDeformedPositions[idx].z - mReferencePositions[idx0].z;
				}
			}
			rhs.head(numDofs) -= mDirichletStiffness * mDirichletJacobian.transpose() * dirichletErrors;
		}
		return rhs;
	}

	void FemPhysicsMixed::AssembleJacobianMatrixFD(EigenMatrix& J)
	{
		uint32 numNodes = GetNumFreeNodes();
		uint32 nDof = NUM_POS_COMPONENTS * numNodes; // position DOFs
		uint32 numLocalNodes = GetNumLocalNodes();
		uint32 nLocalDofs = numLocalNodes * NUM_POS_COMPONENTS;

		uint32 numPressureLocalNodes = GetNumLocalPressureNodes();
		uint32 numPNodes = GetNumFreePressureNodes();

		J.resize(numPNodes, nDof);
		J.setZero();

		// base constraint value
		EigenVector f0(numPNodes);
		GetCurrentVolumeErrors(f0);

		real eps = 1e-6;
		EigenVector f(numPNodes);
		for (uint32 j = 0; j < nDof; j++)
		{
			mDeformedPositions[j / 3][j % 3] += eps;
			GetCurrentVolumeErrors(f);
			auto df = (1.0 / eps) * (f - f0);
			for (uint32 i = 0; i < numPNodes; i++)
			{
				J(i, j) = df(i);
			}
			mDeformedPositions[j / 3][j % 3] -= eps;
		}
	}

	uint32 FemPhysicsMixed::GetNumContacts() const
	{
		if (mHasCollisions && mFemCollision)
			return (uint32)mFemCollision->mCollIndices.size();
		return 0;
	}
} // namespace FEM_SYSTEM