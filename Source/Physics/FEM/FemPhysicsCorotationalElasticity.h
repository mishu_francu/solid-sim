#ifndef FEM_PHYSICS_ANY_ORDER_COROTATIONAL_ELASTICITY_H
#define FEM_PHYSICS_ANY_ORDER_COROTATIONAL_ELASTICITY_H

#include "FemPhysicsLinear.h"
#include "TetrahedralMesh.h"
#include "LinearSolver.h"

namespace FEM_SYSTEM
{
	class FemPhysicsCorotationalElasticity : public FemPhysicsLinear
	{
	public:
		FemPhysicsCorotationalElasticity(std::vector<Tet>& tetrahedra,
			std::vector<Node>& allNodes, const FemConfig& config);

		void Step(real dt) override;
		void SolveEquilibrium(float);

	private:
		void StepExplicit(real h);
		void StepImplicit(real h);
		void AssembleStiffnessMatrix();
		void CacheLocalStiffnessMatrices();
		void ComputeRotations();
		void ComputeElasticForces(EigenVector& elasticForce) const;

	private:
		std::vector<EigenMatrix> mCachedLocalStiffnessMatrix;
		SparseMatrix mStiffnessMatrix; // stiffness matrix
		EigenMatrix mDenseK; // dense stiffness matrix
		std::vector<Matrix3R> mCachedRotationMatrix;
		EigenVector mElasticForce;
		LinearSolver mExplicitMassSolver;
	};


} // namespace FEM_SYSTEM

#endif // FEM_PHYSICS_ANY_ORDER_COROTATIONAL_ELASTICITY_H
