#pragma once

//#define BULLET_COLLISIONS

#ifdef BULLET_COLLISIONS
#include "BulletSoftBody/btDeformableMultiBodyDynamicsWorld.h"
#include "BulletSoftBody/btSoftBody.h"
#include "BulletSoftBody/btSoftBodyHelpers.h"
#include "BulletSoftBody/btSoftBodyRigidBodyCollisionConfiguration.h"
#include "BulletDynamics/Featherstone/btMultiBodyConstraintSolver.h"

#include "BulletUtils/b3BulletDefaultFileIO.h"
#endif

namespace FEM_SYSTEM
{
	class FemPhysicsBase;

	class FemCollision
	{
	public:
		void HandleCollisions(FemPhysicsBase* phys);

		void SetCollisionMesh(const Geometry::Mesh* mesh) { mCollisionMesh = mesh; }

		size_t GetNumCollidables() const { return mCollidables.size(); }
		const Physics::Collidable* GetCollidable(size_t i) const { return mCollidables[i].get(); }
		Physics::Collidable* GetCollidable(size_t i) { return mCollidables[i].get(); }
		void AddCollidable(std::shared_ptr<Physics::Collidable> coll) { mCollidables.push_back(coll); }

		size_t GetNumCollisionPoints() const { return mCollIndices.size(); }

		size_t GetNumCollisionPoints2() const { return mCollPointsB2.size(); }

		void ClearCollisions()
		{
			mCollIndices.clear();
			mCollNormals.clear();
			mCollPointsB.clear();
			mCollNormals2.clear();
			mCollPointsB2.clear();
		}
		void GetCollisionPoint(uint32 i, uint32& idx, Vector3& pt, Vector3& normal) const
		{
			idx = mCollIndices[i];
			pt = DoubleToFloat(mCollPointsB[i]);
			normal = DoubleToFloat(mCollNormals[i]);
		}
		void GetCollisionPoint2(uint32 i, Vector3& pt, Vector3T<int>& nodeTriple, Vector3& bary, Vector3& normal) const
		{
			pt = DoubleToFloat(mCollPointsB2[i]);
			nodeTriple = nodeTriples[i];
			bary = DoubleToFloat(baryCoords[i]);
			normal = DoubleToFloat(mCollNormals2[i]);
		}

		void AddCollisionPointNodeRigid(uint32 idx, Vector3 pt, Vector3 normal)
		{
			mCollIndices.push_back(idx);
			mCollPointsB.push_back(FloatToDouble(pt));
			mCollNormals.push_back(FloatToDouble(normal));
		}

		void AddCollisionPointFaceRigid(Vector3 pt, Math::Vector3T<int> nodeTriple, Vector3 bary, Vector3 normal)
		{
			mCollPointsB2.push_back(FloatToDouble(pt));
			nodeTriples.push_back(nodeTriple);
			baryCoords.push_back(FloatToDouble(bary));
			mCollNormals2.push_back(FloatToDouble(normal));
		}

#ifdef BULLET_COLLISIONS
		void InitBulletCollision();
		btDeformableMultiBodyDynamicsWorld* getDeformableDynamicsWorld()
		{
			return (btDeformableMultiBodyDynamicsWorld*)m_dynamicsWorld;
		}
#endif

	private:
		void DetectMeshCollision(const Geometry::Mesh* mesh);
		void NodeVsTriangle(const Geometry::Mesh* mesh, int vertexIndex, int triangleIndex);

	private:
		const Geometry::Mesh* mCollisionMesh;
		std::vector<std::shared_ptr<Physics::Collidable>> mCollidables;

		// collisions
		real mThickness = 0.02f;
		std::vector<bool> mCollFlags;
		std::vector<uint32> mCollIndices;
		std::vector<Vector3R> mCollNormals;
		std::vector<Vector3R> mCollPointsB;

		std::vector<Vector3T<int>> nodeTriples;
		std::vector<Vector3R> baryCoords;
		std::vector<Vector3R> mCollNormals2;
		std::vector<Vector3R> mCollPointsB2;

#ifdef BULLET_COLLISIONS
		btDiscreteDynamicsWorld* m_dynamicsWorld;
		btBroadphaseInterface* m_broadphase;
		btCollisionDispatcher* m_dispatcher;
		btDefaultCollisionConfiguration* m_collisionConfiguration;
		btConstraintSolver* m_solver;
		btAlignedObjectArray<btDeformableLagrangianForce*> m_forces;
#endif

		friend class FemPhysicsBase;
		friend class FemPhysicsMatrixFree;
		friend class FemPhysicsMixed;
	};
}
