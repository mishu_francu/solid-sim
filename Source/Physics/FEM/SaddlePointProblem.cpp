#include "FemDataStructures.h"
#include "SaddlePointProblem.h"

namespace FEM_SYSTEM
{
	EigenVector SaddlePointProblem::ComputeRhs(const EigenVector& solution)
	{
		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numPNodes = mFPM.GetNumPressureNodes();
		uint32 numContacts = mFPM.GetNumContacts();
		uint32 numConstraints = mFPM.mCondenseContact ? numPNodes : numPNodes + numContacts;
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numConstraints;

		real scale = mFPM.mConfig.mConstraintScale; // scaling of volume constraints

		// update deformed positions
		Vector3Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < mFPM.GetNumFreeNodes(); i++)
		{
			mFPM.mDeformedPositions[i] = u[i];
		}

		// update pressure
		auto p = solution.segment(numDofs, numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			mFPM.mPressures[i] = p[i];
		}

		if (!mFPM.mCondenseContact)
		{
			// update contact multipliers
			mFPM.mContactMultipliers = solution.tail(numContacts);
		}

		// assemble Jacobian matrix
		//mFPM.mJacobian.resize(numConstraints, numDofs);
		mFPM.AssembleJacobianMatrix(mFPM.mVolJacobianMatrix, false, true);
#ifdef CHECK_JACOBIAN
		std::cout << "J analytic" << std::endl << mFPM.mVolJacobianMatrix << std::endl;
		EigenMatrix J = mFPM.mVolJacobianMatrix;
		mFPM.AssembleJacobianMatrixFD(J);
		std::cout << "J approx" << std::endl << J << std::endl;
#endif
		//mFPM.mJacobian.block(0, 0, numPNodes, numDofs) = mFPM.mVolJacobianMatrix;
		//if (numContacts != 0 && !mFPM.mCondenseContact)
		//	mFPM.mJacobian.block(numPNodes, 0, numContacts, numDofs) = mFPM.mContactJacobian;

		// assemble right hand side
		EigenVector rhs(size);
		rhs.setZero();

		rhs.head(numDofs) = mFPM.ComputePosRhs();

		// start with the presssure components
		EigenVector errors(numPNodes); // vector used to accumulate volume errors
		real totalErr = mFPM.GetCurrentVolumeErrors(errors);
		rhs.segment(numDofs, numPNodes) = -scale * errors + scale * mFPM.mVolComplianceMatrix * p;
		//if (!mFPM.mCondenseContact && numContacts != 0)
		//	rhs.tail(numContacts) = (1.0 / mFPM.mContactStiffness) * mFPM.mContactMultipliers;

		// continue with contacts
		if (!mFPM.mCondenseContact && numContacts != 0)
			rhs.tail(numContacts) = -mFPM.mContactDepth;

		//real norm1 = rhs.tail(numPNodes).lpNorm<1>();
		//real penalty = p.lpNorm<Eigen::Infinity>();
		//real Eelastic = mFPM.ComputeEnergyForPlot(0);
		//real Egravity = mFPM.ComputeEnergyForPlot(1);
		//real merit = Eelastic + Egravity;// +penalty * norm1;
		//Printf("merit: %g, penalty: %g\n", merit, penalty);
		//Printf("constr L1: %g, elastic: %g, gravity: %g\n", norm1, Eelastic, Egravity);

		return rhs;
	}

	real SaddlePointProblem::MeritResidual(const EigenVector& rhs) const
	{
		//uint32 numNodes = mFPM.GetNumFreeNodes();
		//uint32 numPNodes = mFPM.GetNumPressureNodes();
		//uint32 numContacts = (uint32)mFPM.mCollIndices.size();
		//uint32 numConstraints = mFPM.mCondenseContact ? numPNodes : numPNodes + numContacts;
		//uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		//uint32 size = numDofs + numConstraints;

		////auto r = mFPM.ComputeStdRhs(MMT_NEO_HOOKEAN);
		////Printf("NH res: %g\n", r.norm());

		//real norm1 = rhs.tail(numPNodes).lpNorm<1>();
		//real norm2 = rhs.head(numDofs).norm();
		//EigenVector errors(numPNodes);
		//mFPM.GetCurrentVolumeErrors(errors);
		//Printf("vol err: %.2f%%\n", errors.lpNorm<1>() / mFPM.mTotalInitialVol * 100);
		//Printf("vol err: %.2f%%\n", norm1 / mFPM.mTotalInitialVol * 100);
		//Printf("force res: %g\n", norm2);
		//real merit = norm2 + norm1;
		//real merit = mFPM.ComputeEnergyForPlot(1) + 1e6 * norm1;
		//Printf("merit: %g\n", merit);

		//return norm1 / mFPM.mTotalInitialVol * 100;
		return 0.5 * rhs.squaredNorm();

		//real penalty = 1;
		//for (uint32 i = 0; i < mFPM.mPressures.size(); i++)
		//	if (abs(mFPM.mPressures[i]) > penalty)
		//		penalty = abs(mFPM.mPressures[i]);
		//real Eelastic = mFPM.ComputeEnergyForPlot(0);
		//real Egravity = mFPM.ComputeEnergyForPlot(1);
		//real merit = Eelastic + Egravity;// +penalty * norm1;
		//return merit;
	}

	void SaddlePointProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K)
	{
		if (mSolverType != SPP_ST_DIRECT)
			return;

		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numPNodes = mFPM.GetNumPressureNodes();
		uint32 numContacts = mFPM.GetNumContacts();
		uint32 numConstraints = mFPM.mCondenseContact ? numPNodes : numPNodes + numContacts;
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numConstraints;

		real scale = mFPM.mConfig.mConstraintScale; // scaling of volume constraints

		// the system matrix
		K.resize(size, size);

		auto Kxx = K.block(0, 0, numDofs, numDofs);
		mFPM.ComputeStiffnessMatrix(Kxx);
		K.block(0, numDofs, numDofs, numConstraints) = scale * mFPM.mVolJacobianMatrix.transpose();
		K.block(numDofs, 0, numConstraints, numDofs) = scale * mFPM.mVolJacobianMatrix;
		K.block(numDofs, numDofs, numPNodes, numPNodes) = -scale * mFPM.mVolComplianceMatrix;
		//if (!mFPM.mCondenseContact)
		//{
		//	// add contact compliance
		//	for (uint32 i = 0; i < numContacts; i++)
		//	{
		//		uint32 j = numDofs + numPNodes + i;
		//		mFPM.mSystemMatrix(j, j) = 1.0 / mFPM.mContactStiffness;
		//	}
		//}
	}

	void SaddlePointProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, SparseMatrix& K)
	{
		if (mSolverType != SPP_ST_DIRECT)
			return;

		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numPNodes = mFPM.GetNumPressureNodes();
		uint32 numContacts = mFPM.GetNumContacts();
		uint32 numConstraints = mFPM.mCondenseContact ? numPNodes : numPNodes + numContacts;
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numConstraints;

		real scale = mFPM.mConfig.mConstraintScale; // scaling of volume constraints

		// the system matrix
		K = SparseMatrix(size, size);

		std::vector<Eigen::Triplet<real>> triplets;
		SparseMatrix Kxx;
		mFPM.ComputeStiffnessMatrix(Kxx);
		for (int k = 0; k < Kxx.outerSize(); ++k)
		{
			for (SparseMatrix::InnerIterator it(Kxx, k); it; ++it)
			{
				triplets.push_back(Eigen::Triplet<real>((int)it.row(), (int)it.col(), it.value()));
			}
		}
		for (int k = 0; k < mFPM.mVolJacobianMatrix.outerSize(); ++k)
		{
			for (SparseMatrix::InnerIterator it(mFPM.mVolJacobianMatrix, k); it; ++it)
			{
				triplets.push_back(Eigen::Triplet<real>((int)(it.row() + numDofs), (int)it.col(), scale * it.value()));
				triplets.push_back(Eigen::Triplet<real>((int)it.col(), (int)(it.row() + numDofs), scale * it.value()));
			}
		}
		for (int k = 0; k < mFPM.mVolComplianceMatrix.outerSize(); ++k)
		{
			for (SparseMatrix::InnerIterator it(mFPM.mVolComplianceMatrix, k); it; ++it)
			{
				triplets.push_back(Eigen::Triplet<real>((int)(it.row() + numDofs), (int)(it.col() + numDofs), -scale * it.value()));
			}
		}
		K.setFromTriplets(triplets.begin(), triplets.end());
	}

	EigenVector SaddlePointProblem::SolveLinearSystem(SparseMatrix& A, const EigenVector& rhs, const EigenVector& s, const EigenVector& y)
	{
		if (mSolverType == SPP_ST_DIRECT)
		{
			LinearSolver solver;
			solver.Init(A, LST_LDLT_PARDISO);
			return solver.Solve(rhs);
		}
		return EigenVector();
	}

	EigenVector SaddlePointProblem::SolveLinearSystem(EigenMatrix& A, const EigenVector& rhs, const EigenVector& s, const EigenVector& y)
	{
		// TODO: no real need for K here

		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numPNodes = mFPM.GetNumPressureNodes();
		uint32 numContacts = mFPM.GetNumContacts();
		uint32 numConstraints = mFPM.mCondenseContact ? numPNodes : numPNodes + numContacts;
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numConstraints;
		real invHSqr = 1.f / (mFPM.mTimeStep * mFPM.mTimeStep);

		if (mSolverType == SPP_ST_DIRECT)
		{
			LinearSolver solver;
			solver.Init(A, LST_LDLT_PARDISO);
			return solver.Solve(rhs);
		}
		else if (mSolverType == SPP_ST_SCHUR_COMPLIANCE)
		{
			// static condensation
			//auto JtCinv = mFPM.mVolJacobianMatrix.transpose() * mCinv;
			EigenMatrix K;
			mFPM.ComputeStiffnessMatrix(K);
			K += mFPM.mVolJacobianMatrix.transpose() * mCinv * mFPM.mVolJacobianMatrix;
			//if (numContacts != 0)
			//	K += mFPM.mContactStiffness * mFPM.mContactJacobian.transpose() * mFPM.mContactJacobian;
			if (mFPM.mSimType == ST_IMPLICIT)
				K += invHSqr * mFPM.mMassMatrix;

			LinearSolver solver;
			//solver.SetTolerance(0.1);
			//solver.Init(K, LST_CG);
			solver.Init(K, LST_LU_PARDISO);

			EigenVector solution(size);
			solution.head(numDofs) = solver.Solve(rhs.head(numDofs) + mFPM.mVolJacobianMatrix.transpose() * mCinv * rhs.segment(numDofs, numPNodes));
			//if (numContacts != 0)
			//	solution.head(numDofs) += mFPM.mContactStiffness * mFPM.mContactJacobian.transpose() * rhs.tail(numContacts);
			solution.segment(numDofs, numPNodes) = mCinv * (mFPM.mVolJacobianMatrix * solution.head(numDofs) - rhs.segment(numDofs, numPNodes)); // back-solve
			//if (numContacts != 0)
			//	solution.tail(numContacts) = mFPM.mContactStiffness * (mFPM.mContactJacobian * solution.head(numDofs) - rhs.tail(numContacts));

			//Printf("Krylov iterations: %d\n", solver.GetIterations()); // TODO: if verbose

			return solution;
		}
		else if (mSolverType == SPP_ST_SCHUR_MASS)
		{
			//if (s.rows() == 0 && y.rows() == 0) // empty s and y vectors signal first iteration
			{
				EigenMatrix K = mFPM.mDeviatoricStiffnessMatrix + mFPM.mGeometricStiffnessMatrix;
				if (mFPM.mSimType == ST_IMPLICIT)
					K += invHSqr * mFPM.mMassMatrix;
				mKinv = K.inverse(); // compute inverse
			}
			//else
			//{
			//	auto sx = s.head(numDofs);
			//	auto yx = y.head(numDofs) - mFPM.mVolJacobianMatrix.transpose() * s.tail(numPNodes);
			//	
			//	mKinv = mKinv - (mKinv * yx * yx.transpose() * mKinv) / (yx.transpose() * mKinv * yx) + (sx * sx.transpose()) / (yx.transpose() * sx); // BFGS
			//	
			//	//auto r = (sx - mKinv * yx);
			//	//mKinv = mKinv + (r * r.transpose()) / (r.transpose() * yx); // SR1
			//}

			auto JKinv = mFPM.mVolJacobianMatrix * mKinv;
			EigenMatrix S = JKinv * mFPM.mVolJacobianMatrix.transpose() + mFPM.mVolComplianceMatrix;

			EigenVector solution(size);

			// solve reduced system
			LinearSolver solver;
			solver.Init(S, LST_LU_PARDISO);
			solution.tail(numPNodes) = solver.Solve(-rhs.tail(numPNodes) + JKinv * rhs.head(numDofs));

			// back-solve
			solution.head(numDofs) = mKinv * (rhs.head(numDofs) - mFPM.mVolJacobianMatrix.transpose() * solution.tail(numPNodes));
			//solution.head(numDofs) = decompK.solve(rhs.head(numDofs) - mFPM.mVolJacobianMatrix.transpose() * solution.tail(numPNodes));

			//Eigen::ConjugateGradient<SparseMatrix> CG;
			//CG.compute(K.sparseView());
			//CG.setMaxIterations(10);
			//sol = CG.solve(rhs.head(numDofs) - mVolJacobianMatrix.transpose() * dp);

			return solution;
		}
		else if (mSolverType == SPP_ST_ITERATIVE)
		{
			std::vector<real> stdFullSol(size);
			std::vector<real> stdRhs(rhs.data(), rhs.data() + size);
			int maxIterations = 100;
			real tolerance = 0.1; // this is actually the forcing term from inexact Newton (?)
#ifdef USE_CG_FOR_MATRIX_BASED
			//int innerIters = SolveConjugateGradientMF<real, real, FemPhysicsMixed>(mFPM, stdRhs, stdFullSol, maxIterations, tolerance, true);
			int innerIters = SolveConjugateResidualMF<real, real, FemPhysicsMixed>(mFPM, stdRhs, stdFullSol, maxIterations, tolerance, true);
			Eigen::Map<EigenVector> fullSol((real*)&stdFullSol[0], stdFullSol.size(), 1);
			return fullSol;
#else
			LinearSolver solver;
			//solver.SetTolerance(tolerance);
			solver.Init(A, LST_CG);
			auto solution = solver.Solve(rhs);
			Printf("Krylov iterations: %d\n", solver.GetIterations());
			return solution;
#endif
		}

		ASSERT(false);
		return EigenVector();
	}
}