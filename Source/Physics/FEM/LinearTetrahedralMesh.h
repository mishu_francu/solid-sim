#ifndef LINEAR_TETRAHEDRAL_MESH_H
#define LINEAR_TETRAHEDRAL_MESH_H

// C++ references
#include <vector>

// Custom references
#include <Physics/FEM/StridedVector.h>
#include <Physics/FEM/FemDataStructures.h>
#include <Physics/FEM/ITetrahedralMesh.h>
#include <Engine/Types.h>

namespace FEM_SYSTEM
{
	class LinearTetrahedralMesh : public ITetrahedralMesh
	{
	public:
		typedef uint32 mIndexType;
		typedef int IJKLType;

		void InstanceWith(int order, int noPoints, StridedVector<uint32> connectivity, int noElements)
		{
			mNumNodes = noPoints;
			mNumElems = noElements;
			mTets.resize(noElements);
			for (int i = 0; i < noElements; i++)
			{
				uint32* gidxs = connectivity.GetAt(i);
				for (int j = 0; j < 4; j++)
					mTets[i].idx[j] = gidxs[j];
			}
		}
		int GetNumNodes() const { return mNumNodes; }
		int GetNumNodesPerElement() const { return 4; }
		int GetNumElements() const { return mNumElems; }
		int GetNodesPerEdge(int) const { return 0; }
		int GetNodesPerFace(int) const { return 0; }
		int GetGlobalIndex(int eidx, int lidx) { return mTets[eidx].idx[lidx]; }
		int* GetIJKL(int lidx) { return mIJKL[lidx]; }
		int GetOrder() const { return 1; }

	private:
		uint32 mNumNodes;
		uint32 mNumElems;
		std::vector<Tet> mTets;
		int mIJKL[4][4] = { {1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1} };
	};
}

#endif // LINEAR_TETRAHEDRAL_MESH_H