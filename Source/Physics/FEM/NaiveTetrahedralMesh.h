#ifndef NAIVE_TETRAHEDRAL_MESH_H
#define NAIVE_TETRAHEDRAL_MESH_H

// C++ references
#include <tuple>

// Custom references
#include <Physics/FEM/StridedVector.h>
#include <Engine/Types.h>

namespace FEM_SYSTEM
{
	template<class IndexType>
	class NaiveTetrahedralMesh {
	public:

		// Typedefs
		typedef std::tuple<IndexType, int, int> InterpolationTuple;
		typedef std::tuple<IndexType, IndexType, IndexType> tuple3IndexType;
		typedef std::tuple<IndexType, IndexType, IndexType, IndexType> tuple4IndexType;
		typedef IndexType mIndexType;

		// unsigned _int8 ensure that we support a order up to 255
		// -> I,J,K,L are always \in [0, order]
		//typedef unsigned _int8 IJKLType;
		typedef int IJKLType; // TODO need to fix MultiIndex before anything but int can be used

		NaiveTetrahedralMesh();
		~NaiveTetrahedralMesh();

		void InstanceWith(int order, int noPoints, StridedVector<uint32> connectivity, int noElements);

		IndexType GetGlobalIndex(int eidx, int lidx);

		int GetNumNodes();
		int GetNumElements();
		int GetNumNodesPerElement();
		int GetNumLinearNodes();

		int GetOrder();
		IJKLType* GetIJKL(int lidx);

		// <gidx, eidx, lidx>
		std::vector<InterpolationTuple> mInterpolationscheme;
		IJKLType* mIJKL;

		// Returns 0 if order < 0
		static int GetNodesPerElement(int order)
		{
			int v = 0;
			for (int i = 0; i < order + 1; i++)
			{
				v += (i + 1) * (i + 2);
			}
			v = v / 2;
			return v;
		}

		static int GetNodesPerEdge(int order)
		{
			return std::max(order - 1, 0);
		}

		static int GetNodesPerFace(int order)
		{
			if (order > 0)
			{
				return std::max(((order - 2)*(order - 1)) / 2, 0);
			}
			return 0;
		}

		static int GetNodesPerBody(int order)
		{
			return GetNodesPerElement(order - 4);
		}

		bool GetIsInstanced() { return mIsInstanced; }

	private:

		IndexType* mConnectivityList;
		int mOrgNoPoints;
		int mNodesInMesh;
		int mNodesPrElement;
		int mElementsInMesh;
		int mOrder;
		bool mIsInstanced = false;

		void BuildHigherOrderMesh(int noPoints, StridedVector<uint32> connectivity);
		void RotateFaceIdxs(std::vector<IndexType> &idxs, std::vector<IndexType> &buffer, int len, int order, int rotations);
		void BucketflipFaceIdxs(std::vector<IndexType> &idxs, std::vector<IndexType> &buffer, int len, int order);
		int Sum(int from, int to);

	};
}


// C++ references
#include <vector>
#include <algorithm>
#include <string>

// Custom references
#include<Engine/Utils.h> // For Printf

// Macros
#define MATRIX_INDEX(i, j, width) i * width + j

namespace FEM_SYSTEM
{

	// Constructor
	template<class IndexType>
	NaiveTetrahedralMesh<IndexType>::NaiveTetrahedralMesh()
	{

	}

	// Destructor
	template<class IndexType>
	NaiveTetrahedralMesh<IndexType>::~NaiveTetrahedralMesh()
	{
		if (mIsInstanced)
		{
			delete[] mConnectivityList;
			delete[] mIJKL;
		}
	}

	template<class IndexType>
	void NaiveTetrahedralMesh<IndexType>::InstanceWith(int order, int noPoints, StridedVector<uint32> connectivity, int noElements)
	{
		// TODO assert that the selected IJKLType is big enough to handle the given order
		// TODO either assert that this mesh has already been instanced, or cleanup before each instance
		ASSERT(order > 0);
		ASSERT(noPoints >= 4);
		ASSERT(noElements > 0);
		ASSERT(mIsInstanced == false);
		// TODO assert that connectivity is not null, how to?

		this->mOrder = order;
		this->mElementsInMesh = noElements;
		this->mNodesPrElement = GetNodesPerElement(order);

		this->mConnectivityList = new IndexType[noElements * mNodesPrElement]{ 0 };
		this->mIJKL = new IJKLType[mNodesPrElement * 4]{ 0 };
		this->mIsInstanced = true;

		BuildHigherOrderMesh(noPoints, connectivity);
		this->mNodesInMesh = mInterpolationscheme.size();
		this->mOrgNoPoints = noPoints;
	}

	template<class IndexType>
	IndexType NaiveTetrahedralMesh<IndexType>::GetGlobalIndex(int eidx, int lidx)
	{
		ASSERT(eidx >= 0);
		ASSERT(eidx < mElementsInMesh);
		ASSERT(lidx >= 0);
		ASSERT(lidx < mNodesPrElement);

		return mConnectivityList[MATRIX_INDEX(eidx, lidx, mNodesPrElement)];
	}

	template<class IndexType>
	int NaiveTetrahedralMesh<IndexType>::GetNumNodes()
	{
		return this->mNodesInMesh;
	}

	template<class IndexType>
	int NaiveTetrahedralMesh<IndexType>::GetNumElements()
	{
		return this->mElementsInMesh;
	}

	template<class IndexType>
	int NaiveTetrahedralMesh<IndexType>::GetNumNodesPerElement()
	{
		return this->mNodesPrElement;
	}

	template<class IndexType>
	int NaiveTetrahedralMesh<IndexType>::GetNumLinearNodes()
	{
		return this->mOrgNoPoints;
	}

	template<class IndexType>
	int NaiveTetrahedralMesh<IndexType>::GetOrder()
	{
		return this->mOrder;
	}

	template<class IndexType>
	typename NaiveTetrahedralMesh<IndexType>::IJKLType * NaiveTetrahedralMesh<IndexType>::GetIJKL(int lidx)
	{
		ASSERT(lidx >= 0);
		ASSERT(lidx < mNodesPrElement);

		return &mIJKL[MATRIX_INDEX(lidx, 0, 4)];
	}

	template<class IndexType>
	void NaiveTetrahedralMesh<IndexType>::BuildHigherOrderMesh(int noPoints, StridedVector<uint32> connectivity)
	{
		int nextIJKLRow = 0;
		IndexType nextGlobalIdx = noPoints;

		// -- Build the corner nodes --
		if (mOrder > 0)
		{
			// The 4 IJKL entries
			mIJKL[MATRIX_INDEX(nextIJKLRow++, 0, 4)] = mOrder;
			mIJKL[MATRIX_INDEX(nextIJKLRow++, 1, 4)] = mOrder;
			mIJKL[MATRIX_INDEX(nextIJKLRow++, 2, 4)] = mOrder;
			mIJKL[MATRIX_INDEX(nextIJKLRow++, 3, 4)] = mOrder;

			// The ConnectivityList entries
			for (int eidx = 0; eidx < mElementsInMesh; eidx++)
			{
				uint32* gidxs = connectivity.GetAt(eidx);

				for (int i = 0; i < 4; i++)
				{
					IndexType gidx = (IndexType)(*(gidxs + i));
					mConnectivityList[MATRIX_INDEX(eidx, i, mNodesPrElement)] = gidx;

					auto it = std::find_if(mInterpolationscheme.begin(), mInterpolationscheme.end(),
						[gidx](const InterpolationTuple& e) { return std::get<0>(e) == gidx; });
					if (it == mInterpolationscheme.end())
					{
						mInterpolationscheme.push_back(std::make_tuple(gidx, eidx, i));
					}
				}
			}
		}

		// -- Build the edge nodes --
		if (mOrder > 1)
		{
			std::tuple<int, int> edge_pairs[]{
				std::make_tuple(0, 1),
				std::make_tuple(0, 2),
				std::make_tuple(0, 3),
				std::make_tuple(1, 2),
				std::make_tuple(1, 3),
				std::make_tuple(2, 3)
			};

			int no_nodes_pr_edge = GetNodesPerEdge(mOrder);


			// The IJKL entries
			for (int i = 0; i < 6; i++)
			{
				int e1, e2; std::tie(e1, e2) = edge_pairs[i];
				for (IJKLType k = mOrder - 1; k > 0; k--)
				{
					mIJKL[MATRIX_INDEX(nextIJKLRow, e1, 4)] = k;
					mIJKL[MATRIX_INDEX(nextIJKLRow++, e2, 4)] = mOrder - k;
				}
			}

			// The ConnectivityList entries

			// tuple<gidx of c1, gidx of c2, gidx of e1 of edge (c1,c2)>
			std::vector<tuple3IndexType> processed_edges;

			int lidx1, lidx2; IndexType gidx1, gidx2;
			for (int eidx = 0; eidx < mElementsInMesh; eidx++)
			{
				uint32* gidxs = connectivity.GetAt(eidx);

				for (int i = 0; i < 6; i++)
				{
					std::tie(lidx1, lidx2) = edge_pairs[i];
					gidx1 = (IndexType)(*(gidxs + lidx1));
					gidx2 = (IndexType)(*(gidxs + lidx2));

					// First check if the edge has already been processed
					auto it = std::find_if(processed_edges.begin(), processed_edges.end(),
						[gidx1, gidx2](const tuple3IndexType& e) { return std::get<0>(e) == gidx1 && std::get<1>(e) == gidx2; });
					if (it != processed_edges.end())
					{
						IndexType gidx = std::get<2>(*it);
						int j_offset = 4 + i * no_nodes_pr_edge;
						for (int j = 0; j < no_nodes_pr_edge; j++)
						{
							mConnectivityList[MATRIX_INDEX(eidx, j + j_offset, mNodesPrElement)] = gidx++;
						}
						continue;
					}
					it = std::find_if(processed_edges.begin(), processed_edges.end(),
						[gidx1, gidx2](const tuple3IndexType& e) { return std::get<0>(e) == gidx2 && std::get<1>(e) == gidx1; });
					if (it != processed_edges.end())
					{
						IndexType gidx = std::get<2>(*it);
						int j_offset = 4 + i * no_nodes_pr_edge;
						for (int j = 0; j < no_nodes_pr_edge; j++)
						{
							mConnectivityList[MATRIX_INDEX(eidx, j + j_offset, mNodesPrElement)] = gidx-- + (no_nodes_pr_edge - 1);
						}
						continue;
					}

					processed_edges.push_back(std::make_tuple(gidx1, gidx2, nextGlobalIdx));
					int j_offset = 4 + i * no_nodes_pr_edge;
					for (int j = 0; j < no_nodes_pr_edge; j++)
					{
						mInterpolationscheme.push_back(std::make_tuple(nextGlobalIdx, eidx, j + j_offset));
						mConnectivityList[MATRIX_INDEX(eidx, j + j_offset, mNodesPrElement)] = nextGlobalIdx++;
					}
				}
			}
		}


		// -- Build the face nodes --
		if (mOrder > 2)
		{
			std::tuple<int, int, int> face_pairs[]{
				std::make_tuple(0, 1, 2), std::make_tuple(0, 1, 3),
				std::make_tuple(0, 2, 3), std::make_tuple(1, 2, 3)
			};

			int no_nodes_pr_face = GetNodesPerFace(mOrder);


			// The IJKL entries
			for (int i = 0; i < 4; i++)
			{
				int f1, f2, f3; std::tie(f1, f2, f3) = face_pairs[i];

				int minor_order = mOrder - 1;
				for (IJKLType v1 = minor_order; v1 > 0; v1--)
				{
					for (IJKLType v2 = minor_order - v1; v2 > 0; v2--)
					{
						IJKLType v3 = (mOrder - v1) - v2;

						mIJKL[MATRIX_INDEX(nextIJKLRow, f1, 4)] = v1;
						mIJKL[MATRIX_INDEX(nextIJKLRow, f2, 4)] = v2;
						mIJKL[MATRIX_INDEX(nextIJKLRow++, f3, 4)] = v3;
					}
				}
			}

			// The ConnectivityList entries
			// tuple<gidx of fidx1, gidx of fidx2, gidx of fidx3, gidx of f1 of face (fidx1,fidx2,fidx3)>
			std::vector<tuple4IndexType> processed_faces;

			auto checkProcessedFace = [&processed_faces, no_nodes_pr_face, this]
			(int eidx, int fidx, IndexType gidx1, IndexType gidx2, IndexType gidx3, std::vector<IndexType> buffer1, std::vector<IndexType> buffer2, int rotations, bool bucketflip)->bool
			{
				auto it = std::find_if(processed_faces.begin(), processed_faces.end(),
					[gidx1, gidx2, gidx3](const tuple4IndexType& e) {
					return std::get<0>(e) == gidx1 && std::get<1>(e) == gidx2 && std::get<2>(e) == gidx3;
				});

				if (it != processed_faces.end())
				{
					IndexType gidx = std::get<3>(*it);
					std::generate(buffer1.begin(), buffer1.end(), [&gidx]() { return gidx++; });

					RotateFaceIdxs(buffer1, buffer2, no_nodes_pr_face, mOrder, rotations);

					if (bucketflip)
					{
						BucketflipFaceIdxs(buffer2, buffer1, no_nodes_pr_face, mOrder);
						buffer2 = buffer1;
					}

					int j_offset = 4 + 6 * GetNodesPerEdge(mOrder) + fidx * no_nodes_pr_face;
					for (int j = 0; j < no_nodes_pr_face; j++)
					{
						mConnectivityList[MATRIX_INDEX(eidx, j + j_offset, mNodesPrElement)] = buffer2[j];
					}
					return true;
				}
				return false;
			};

			std::vector<IndexType> gidxBuffer1(no_nodes_pr_face);
			std::vector<IndexType> gidxBuffer2(no_nodes_pr_face);

			int lidx1, lidx2, lidx3; IndexType gidx1, gidx2, gidx3;
			for (int eidx = 0; eidx < mElementsInMesh; eidx++)
			{
				uint32* gidxs = connectivity.GetAt(eidx);
				for (int i = 0; i < 4; i++)
				{
					std::tie(lidx1, lidx2, lidx3) = face_pairs[i];
					gidx1 = (IndexType)(*(gidxs + lidx1));
					gidx2 = (IndexType)(*(gidxs + lidx2));
					gidx3 = (IndexType)(*(gidxs + lidx3));

					// First check if the face has already been processed
					if (!(checkProcessedFace(eidx, i, gidx1, gidx2, gidx3, gidxBuffer1, gidxBuffer2, 0, false)
						|| checkProcessedFace(eidx, i, gidx3, gidx1, gidx2, gidxBuffer1, gidxBuffer2, 1, false)
						|| checkProcessedFace(eidx, i, gidx2, gidx3, gidx1, gidxBuffer1, gidxBuffer2, 2, false)
						|| checkProcessedFace(eidx, i, gidx1, gidx3, gidx2, gidxBuffer1, gidxBuffer2, 0, true)
						|| checkProcessedFace(eidx, i, gidx2, gidx1, gidx3, gidxBuffer1, gidxBuffer2, 1, true)
						|| checkProcessedFace(eidx, i, gidx3, gidx2, gidx1, gidxBuffer1, gidxBuffer2, 2, true)))
					{
						processed_faces.push_back(std::make_tuple(gidx1, gidx2, gidx3, nextGlobalIdx));
						int j_offset = 4 + 6 * GetNodesPerEdge(mOrder) + i * no_nodes_pr_face;
						for (int j = 0; j < no_nodes_pr_face; j++)
						{
							mInterpolationscheme.push_back(std::make_tuple(nextGlobalIdx, eidx, j + j_offset));
							mConnectivityList[MATRIX_INDEX(eidx, j + j_offset, mNodesPrElement)] = nextGlobalIdx++;
						}
					}
				}
			}
		}


		// -- Build the body nodes --
		if (mOrder > 3)
		{
			// The IJKL entries
			int body_order = mOrder - 4;

			for (IJKLType i = body_order + 1; i-- > 0;)
			{
				for (IJKLType j = std::max<IJKLType>(body_order - i, 0) + 1; j-- > 0;)
				{
					for (IJKLType k = std::max<IJKLType>(body_order - i - j, 0) + 1; k-- > 0;)
					{
						IJKLType l = body_order - i - j - k;
						mIJKL[MATRIX_INDEX(nextIJKLRow, 0, 4)] = i + 1;
						mIJKL[MATRIX_INDEX(nextIJKLRow, 1, 4)] = j + 1;
						mIJKL[MATRIX_INDEX(nextIJKLRow, 2, 4)] = k + 1;
						mIJKL[MATRIX_INDEX(nextIJKLRow++, 3, 4)] = l + 1;
					}
				}
			}

			// The ConnectivityList entries
			int no_nodes_pr_body = GetNodesPerBody(mOrder);
			int j_offset = 4 + 6 * GetNodesPerEdge(mOrder) + 4 * GetNodesPerFace(mOrder);
			for (int eidx = 0; eidx < mElementsInMesh; eidx++)
			{
				for (int j = 0; j < no_nodes_pr_body; j++)
				{
					mInterpolationscheme.push_back(std::make_tuple(nextGlobalIdx, eidx, j + j_offset));
					mConnectivityList[MATRIX_INDEX(eidx, j + j_offset, mNodesPrElement)] = nextGlobalIdx++;
				}
			}
		}


		//// -- Debug printing --

		//// Debug-print the IJKL matrix
		//Printf("[I,J,K,L]\n");
		////Printf((std::to_string(order)).c_str());
		//for (int i = 0; i < mNodesPrElement; i++)
		//{
		//	Printf("[");
		//	Printf((std::to_string(mIJKL[MATRIX_INDEX(i, 0, 4)])).c_str());
		//	Printf(",");
		//	Printf((std::to_string(mIJKL[MATRIX_INDEX(i, 1, 4)])).c_str());
		//	Printf(",");
		//	Printf((std::to_string(mIJKL[MATRIX_INDEX(i, 2, 4)])).c_str());
		//	Printf(",");
		//	Printf((std::to_string(mIJKL[MATRIX_INDEX(i, 3, 4)])).c_str());
		//	Printf("]\n");
		//}
		//// -

		// //-

		//// Debug-print the ConnectivityList
		//Printf("ConnectivityList:\n");
		//for (int i = 0; i < mElementsInMesh; i++)
		//{
		//	Printf("[");
		//	for (int j = 0; j < mNodesPrElement; j++)
		//	{
		//		Printf((std::to_string(mConnectivityList[MATRIX_INDEX(i, j, mNodesPrElement)])).c_str());
		//		if (j + 1 < mNodesPrElement)
		//			Printf(",");
		//	}
		//	Printf("]\n");
		//}

		//// Debug print the interpolation scheme
		//for (std::tuple<int, int, int> t : mInterpolationscheme)
		//{
		//	Printf("[gidx:");
		//	Printf((std::to_string(std::get<0>(t))).c_str());
		//	Printf(", eidx:");
		//	Printf((std::to_string(std::get<1>(t))).c_str());
		//	Printf(", lidx:");
		//	Printf((std::to_string(std::get<2>(t))).c_str());
		//	Printf("]\n");
		//}
	}

	template<class IndexType>
	void NaiveTetrahedralMesh<IndexType>::RotateFaceIdxs(std::vector<IndexType> &idxs, std::vector<IndexType> &buffer, int len, int order, int rotations)
	{
		std::vector<IndexType> temp; temp = idxs;
		buffer = idxs;

		while (rotations-- > 0)
		{
			int idx = -1;
			for (int i = order - 3; i > -1; i--)
			{
				int d = (order - 3) - i;
				for (int j = (order - 3) - d; j < order - 2; j++)
				{
					idx++;
					buffer[i + Sum(0, j)] = temp[idx];
				}
			}
			temp = buffer;
		}
	}

	template<class IndexType>
	void NaiveTetrahedralMesh<IndexType>::BucketflipFaceIdxs(std::vector<IndexType> &idxs, std::vector<IndexType> &buffer, int len, int order)
	{
		int no_buckets = order - 2;
		int current_idx = 0;
		for (int i = 0; i < no_buckets; i++)
		{
			int bucket_length = i + 1;
			int flip_from = current_idx;
			int flip_to = flip_from + bucket_length;
			for (int j = flip_from; j < flip_to; j++)
			{
				buffer[j] = idxs[flip_from + ((flip_to - 1) - j)];
			}
			current_idx = flip_to;
		}
	}

	template<class IndexType>
	int NaiveTetrahedralMesh<IndexType>::Sum(int from, int to)
	{
		int sum = 0;
		for (int i = from; i <= to; i++)
		{
			sum += i;
		}
		return sum;
	}
}

#endif // NAIVE_TETRAHEDRAL_MESH_H
