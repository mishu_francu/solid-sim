#include "FemDataStructures.h"
#include "FemPhysicsMatrixFree.h"
#include <Physics/FEM/PolarDecomposition.h>
#include <Engine/Profiler.h>

#pragma warning( disable : 4267) // for size_t-uint conversions

#define STVK
#define LOCAL_SOLVE

namespace FEM_SYSTEM
{
	void FemPhysicsMatrixFree::StepConstraint(real h)
	{
		PROFILE_SCOPE("Constraint FEM");

		Matrix3R id;
		Matrix3R Einv = mNormalElasticityMatrix.GetInverse();
		const real mu = 0.5f * mYoungsModulus / (1.f + mPoissonRatio);
		const real muInv = 1.f / mu;

		Eigen::Matrix<real, 6, 6> C;
		C.setZero();
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				C(i, j) = Einv[i][j];
			}
			C(i + 3, i + 3) = muInv;
		}
		for (size_t k = 0; k < tets.size(); k++)
		{
			lambdaAcc[k].setZero();
		}

		// candidate positions
		for (size_t i = 0; i < nodes.size(); i++)
		{
			nodes.at(i).force.SetZero();
			if (nodes.at(i).invMass == 0)
				continue;
			nodes.at(i).vel += h * mGravity;
			nodes.at(i).pos += h * nodes.at(i).vel;
		}

		// nonlinear Gauss-Seidel iterations
		for (int iter = 0; iter < 10; iter++)
		{
			//PROFILE_SCOPE("Iteration");
			for (size_t i = 0; i < tets.size(); i++)
			{
				//PROFILE_SCOPE("Tetrahedron");
				Tetrahedron& tet = tets.at(i);
				Eigen::Matrix<real, 6, 1> eps; // strain vector
				Eigen::Matrix<real, 6, 12> Jac; // Jacobian due to strain (de/dx)
				ComputeStrainAndJacobian<MMT_LINEAR>(i, eps, Jac); // TODO: Material template
				// compute local A matrix
				Eigen::Matrix<real, 12, 12> W;
				W.setZero();
				for (int k = 0; k < 12; k++)
				{
					W(k, k) = nodes.at(tet.i[k / 3]).invMass;
				}
				Eigen::Matrix<real, 6, 6> A = Jac * W * Jac.transpose();

				// compute constraint
				Eigen::Matrix<real, 6, 1> c = eps;
				real vol = abs(tet.vol);
				Eigen::Matrix<real, 6, 6> S = h * h * A + (1.0f / vol) * C;

				EigenVector f;
				{
					EigenVector lambda = S.llt().solve(c - (1.0f / vol) * C * lambdaAcc[i]);
					lambdaAcc[i] += lambda;
					f = Jac.transpose() * lambda;
				}

				// apply force
				for (int k = 0; k < 4; k++)
				{
					int nod = tet.i[k];
					if (nodes.at(nod).invMass == 0)
						continue;
					int base = k * 3;
					Vector3R fn(f[base], f[base + 1], f[base + 2]);
					nodes.at(nod).vel -= (h * nodes.at(nod).invMass) * fn;
					nodes.at(nod).pos -= (h * h * nodes.at(nod).invMass) * fn;
				}
			}
		}
	}

	template<int Material>
	void FemPhysicsMatrixFree::ComputeStrainAndJacobian(uint32 e, // tet index
		Eigen::Matrix<real, 6, 1>& eps, // strain vector
		Eigen::Matrix<real, 6, 12>& J) // Jacobian due to strain (de/dx)
	{
		const Tetrahedron& tet = tets[e];

		static Matrix3R id; // identity matrix
		Matrix3R F; // deformation gradient
		Matrix3R strain; // strain tensor
		Matrix3R Js[4]; // normal Jacobian (x4)
		Matrix3R Jn[4]; // shear Jacobian (x4)

		//real vol = abs(tet.vol);
		ComputeDeformationGradient(e, F);
		Matrix3R Ftr = !F;

		if (Material == MMT_STVK)
		{
			// Green strain
			for (int j = 0; j < 4; j++)
			{
				Jn[j] = tet.Hn[j] * Ftr;
				Js[j] = tet.Hs[j] * Ftr;
			}
			strain = 0.5f * (Ftr * F - id); // Green strain
		}
		else if (Material == MMT_LINEAR)
		{
			// Cauchy strain
			for (int j = 0; j < 4; j++)
			{
				Jn[j] = tet.Hn[j];
				Js[j] = tet.Hs[j];
			}
			strain = 0.5f * (F + !F) - id; // Cauchy strain
		}
		else if (Material == MMT_COROTATIONAL)
		{
			Matrix3R R, U;
			ComputePolarDecomposition(F, R, U);
			F = U;
			for (int j = 0; j < 4; j++)
			{
				Jn[j] = tet.Hn[j] * !R;
				Js[j] = tet.Hs[j] * !R;
			}
			strain = 0.5f * (F + !F) - id; // Cauchy strain
		}

		// convert strain tensor to 6-vector
		Vector3R epsN(strain[0][0], strain[1][1], strain[2][2]);
		Vector3R epsS(strain[1][2], strain[0][2], strain[0][1]);
		for (int j = 0; j < 3; j++)
		{
			eps[j] = epsN[j];
			eps[j + 3] = epsS[j];
		}

		// build Jacobian matrix
		for (int k = 0; k < 4; k++)
		{
			for (int l = 0; l < 3; l++)
			{
				for (int m = 0; m < 3; m++)
				{
					J(l + 3, m + k * 3) = Js[k][l][m];
					J(l, m + k * 3) = Jn[k][l][m];
				}
			}
		}
	}

	// velocity version of constraint-based FEM using block Gauss-Seidel
	template<int Material>
	void FemPhysicsMatrixFree::StepConstraintVel(real h)
	{
		PROFILE_SCOPE("Constraint FEM");

		// compute the inverse of the elasticity matrix
		Matrix3R Einv = mNormalElasticityMatrix.GetInverse(); // TODO: pre-compute
		const real mu = 0.5f * mYoungsModulus / (1.f + mPoissonRatio);
		const real muInv = 1.f / mu;

		// fill in the compliance matrix
		Eigen::Matrix<real, 6, 6> C;
		C.setZero();
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				C(i, j) = Einv[i][j];
			}
			C(i + 3, i + 3) = muInv;
		}

		// pre-compute the constraint function and its Jacobian
		std::vector<TetInfo, Eigen::aligned_allocator<TetInfo>> tetInfos(tets.size());
		for (size_t i = 0; i < tets.size(); i++)
		{
			Tetrahedron& tet = tets.at(i);
			Eigen::Matrix<real, 6, 1> eps; // strain vector
			Eigen::Matrix<real, 6, 12> J; // Jacobian due to strain (de/dx)
			ComputeStrainAndJacobian<Material>(i, eps, J);

			tetInfos[i].J = J;
			tetInfos[i].c = eps;

			auto Jtr = J.transpose();
			// compute local A matrix
			Eigen::Matrix<real, 12, 12> W;
			W.setZero();
			for (int k = 0; k < 12; k++)
			{
				W(k, k) = nodes[tet.i[k / 3]].invMass;
			}
			Eigen::Matrix<real, 6, 6> A = J * W * Jtr;
			tetInfos[i].S = h * h * A + (1.f / abs(tet.vol)) * C;

			lambdaAcc[i].setZero();
		}

		// candidate velocities
		for (size_t i = 0; i < nodes.size(); i++)
		{
			nodes.at(i).force.SetZero();
			if (nodes.at(i).invMass == 0)
				continue;
			nodes.at(i).vel += h * mGravity;
		}

		// block Gauss-Seidel iterations
		for (int iter = 0; iter < 20; iter++)
		{
			// compute velocity error and apply correction impulses
			for (size_t i = 0; i < tets.size(); i++)
			{
				Tetrahedron& tet = tets.at(i);
				Eigen::Matrix<real, 12, 1> v;
				for (int j = 0; j < 4; j++)
				{
					int base = j * 3;
					v[base] = nodes.at(tet.i[j]).vel.X();
					v[base + 1] = nodes.at(tet.i[j]).vel.Y();
					v[base + 2] = nodes.at(tet.i[j]).vel.Z();
				}
				auto err = 0.95f * tetInfos[i].c + h * (tetInfos[i].J * v);
				auto Jtr = tetInfos[i].J.transpose();

				EigenVector f, lambda;
				lambda = tetInfos[i].S.llt().solve(err - (1.0f / abs(tet.vol)) * C * lambdaAcc[i]);
				lambdaAcc[i] += lambda;
				f = Jtr * lambda;

				// apply force
				for (int k = 0; k < 4; k++)
				{
					int nod = tet.i[k];
					if (nodes.at(nod).invMass == 0)
						continue;
					int base = k * 3;
					Vector3R fn(f[base], f[base + 1], f[base + 2]);
					nodes.at(nod).force += fn;
					nodes.at(nod).vel -= (h * nodes.at(nod).invMass) * fn;
				}
			}
		}

		// integrate positions
		for (size_t i = 0; i < nodes.size(); i++)
		{
			if (nodes.at(i).invMass == 0)
				continue;
			nodes.at(i).pos += h * nodes.at(i).vel;
		}
	}

	// explicit function instantiations
	template void FemPhysicsMatrixFree::StepConstraintVel<MMT_LINEAR>(real h);
	template void FemPhysicsMatrixFree::StepConstraintVel<MMT_STVK>(real h);
	template void FemPhysicsMatrixFree::StepConstraintVel<MMT_COROTATIONAL>(real h);

	// specialized direct solver for the case of linear and corotational elasticity
	template<int Material>
	void FemPhysicsMatrixFree::StepConstraintLinear_DirectSolver(real h)
	{
		PROFILE_SCOPE("Constraint FEM");

		// compute the inverse of the elasticity matrix
		Matrix3R Einv = mNormalElasticityMatrix.GetInverse();
		const real mu = 0.5f * mYoungsModulus / (1.f + mPoissonRatio);
		const real muInv = 1.f / mu;

		// prepare global matrices and vectors
		size_t nDof = 3 * nodes.size();
		size_t nConstr = 6 * tets.size();
		EigenMatrix J(nConstr, nDof); // the Jacobian matrix
		J.setZero();

		// assemble the global inverse mass matrix
		EigenMatrix W(nDof, nDof); // the inverse mass matrix
		W.setZero();
		for (size_t k = 0; k < nodes.size(); k++)
		{
			int base = k * 3;
			for (int l = 0; l < 3; l++)
				W(base + l, base + l) = nodes[k].invMass;
		}

		// assemble the global compliance matrix
		EigenMatrix C(nConstr, nConstr);
		C.setZero();
		for (size_t k = 0; k < tets.size(); k++)
		{
			int base = k * 6;
			real vol = abs(tets[k].vol);
			real invVol = 1.0f / vol;
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					C(base + i, base + j) = invVol * Einv[i][j];
				}
				C(base + i + 3, base + i + 3) = invVol * muInv;
			}
		}
		EigenVector b(nConstr); // RHS
		b.setZero();

		// candidate positions
		for (size_t i = 0; i < nodes.size(); i++)
		{
			nodes.at(i).force.SetZero();
			if (nodes.at(i).invMass == 0)
				continue;
			nodes.at(i).vel += h * mGravity;
			nodes.at(i).pos += h * nodes.at(i).vel;
		}

		// assemble the global Jacobian and RHS
		for (size_t i = 0; i < tets.size(); i++)
		{
			Tetrahedron& tet = tets.at(i);
			Eigen::Matrix<real, 6, 1> eps; // strain vector
			Eigen::Matrix<real, 6, 12> Jac; // Jacobian due to strain (de/dx)
			ComputeStrainAndJacobian<Material>(i, eps, Jac);

			int baseRow = i * 6;
			for (int k = 0; k < 4; k++)
			{
				int nod = tet.i[k];
				int baseCol = nod * 3;
				for (int l = 0; l < 6; l++)
				{
					for (int m = 0; m < 3; m++)
					{
						J(baseRow + l, baseCol + m) += Jac(l, k * 3 + m);
					}
				}
			}
			for (int j = 0; j < 6; j++)
			{
				b[baseRow + j] = eps(j);
			}
		}

		// form the linear system and solve it
		EigenMatrix S = h * h * J * W * J.transpose() + C;
		EigenVector lambda = S.llt().solve(b);
		EigenVector f = J.transpose() * lambda;

		// integrate node velocities and positions
		for (size_t i = 0; i < nodes.size(); i++)
		{
			if (nodes.at(i).invMass == 0)
				continue;
			int base = i * 3;
			nodes.at(i).force = -Vector3R(f[base], f[base + 1], f[base + 2]);
			nodes.at(i).vel += (h * nodes.at(i).invMass) * nodes.at(i).force;
			nodes.at(i).pos += (h * h * nodes.at(i).invMass) * nodes.at(i).force;
			//nodes.at(i).pos += h * nodes.at(i).vel;
		}
	}

	// explicit function instantiations
	template void FemPhysicsMatrixFree::StepConstraintLinear_DirectSolver<MMT_LINEAR>(real h);
	template void FemPhysicsMatrixFree::StepConstraintLinear_DirectSolver<MMT_COROTATIONAL>(real h);

	// Newton solver using direct solve for linear systems
	template<int Material>
	void FemPhysicsMatrixFree::StepConstraintNonlinear_DirectSolver(real h)
	{
		PROFILE_SCOPE("Constraint FEM");

		// compute the inverse of the elasticity matrix
		Matrix3R Einv = mNormalElasticityMatrix.GetInverse();
		const real mu = 0.5f * mYoungsModulus / (1.f + mPoissonRatio);
		const real muInv = 1.f / mu;

		// prepare global matrices and vectors
		size_t nDof = 3 * nodes.size();
		size_t nConstr = 6 * tets.size();
		EigenMatrix J(nConstr, nDof); // the Jacobian matrix
		EigenMatrix W(nDof, nDof); // the inverse mass matrix
		EigenMatrix C(nConstr, nConstr);
		EigenVector b(nConstr); // RHS
		EigenVector lambdaAcc(nConstr);

		lambdaAcc.setZero();
		W.setZero();
		C.setZero();

		// assemble the global compliance matrix
		for (size_t k = 0; k < tets.size(); k++)
		{
			int base = k * 6;
			real vol = abs(tets[k].vol);
			real invVol = 1.0f / vol;
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					C(base + i, base + j) = invVol * Einv[i][j];
				}
				C(base + i + 3, base + i + 3) = invVol * muInv;
			}
		}

		// assemble the global inverse mass matrix
		for (size_t k = 0; k < nodes.size(); k++)
		{
			for (int l = 0; l < 3; l++)
				W(k + l, k + l) = nodes[k].invMass;
		}

		// candidate velocities and positions
		for (size_t i = 0; i < nodes.size(); i++)
		{
			nodes.at(i).force.SetZero();
			if (nodes.at(i).invMass == 0)
				continue;
			nodes.at(i).vel += h * mGravity;
			nodes.at(i).pos += h * nodes.at(i).vel;
		}

		for (int iter = 0; iter < 10; iter++)
		{
			J.setZero();
			b.setZero();

			// assemble the global Jacobian and RHS
			std::vector<TetInfo, Eigen::aligned_allocator<TetInfo>> tetInfos(tets.size());
			for (size_t i = 0; i < tets.size(); i++)
			{
				Tetrahedron& tet = tets.at(i);
				Eigen::Matrix<real, 6, 1> eps; // strain vector
				Eigen::Matrix<real, 6, 12> Jac; // Jacobian due to strain (de/dx)
				ComputeStrainAndJacobian<Material>(i, eps, Jac);

				int baseRow = i * 6;
				for (int k = 0; k < 4; k++)
				{
					int nod = tet.i[k];
					int baseCol = nod * 3;
					for (int l = 0; l < 6; l++)
					{
						for (int m = 0; m < 3; m++)
						{
							J(baseRow + l, baseCol + m) += Jac(l, k * 3 + m);
						}
					}
				}
				for (int j = 0; j < 6; j++)
				{
					b[baseRow + j] = eps(j);
				}
			}

			// form the linear system and solve it
			EigenMatrix S = h * h * J * W * J.transpose() + C;
			EigenVector lambda = S.llt().solve(b - C * lambdaAcc);
			lambdaAcc += lambda;
			EigenVector f = J.transpose() * lambda;

			// integrate node velocities and positions
			for (size_t i = 0; i < nodes.size(); i++)
			{
				if (nodes.at(i).invMass == 0)
					continue;
				int base = i * 3;
				nodes.at(i).force = -Vector3R(f[base], f[base + 1], f[base + 2]);
				nodes.at(i).vel += (h * nodes.at(i).invMass) * nodes.at(i).force;
				nodes.at(i).pos += (h * h * nodes.at(i).invMass) * nodes.at(i).force;
			}
		}
	}

	template void FemPhysicsMatrixFree::StepConstraintNonlinear_DirectSolver<MMT_LINEAR>(real h);
	template void FemPhysicsMatrixFree::StepConstraintNonlinear_DirectSolver<MMT_COROTATIONAL>(real h);
	template void FemPhysicsMatrixFree::StepConstraintNonlinear_DirectSolver<MMT_STVK>(real h);

} // namespace FEM_SYSTEM