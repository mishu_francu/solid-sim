#include "FemDataStructures.h"
#include "FemPhysicsAnalyticCantileverNonLinearElasticity.h"
#include <Physics/FEM/MeshFactory.h>

#pragma warning( disable : 4267) // for size_t-uint conversions

namespace FEM_SYSTEM
{
	FemPhysicsAnalyticCantileverNonLinearElasticity::FemPhysicsAnalyticCantileverNonLinearElasticity(std::vector<Tet>& tetrahedra,
		std::vector<Node>& nodes, const FemConfig& config)
		: FemPhysicsBase(config)
		, density(1250.f) // silicon rubber density
	{
		// 1. Build the higher order mesh/convert to a TetrahedralMesh instance
		// use the linear mesh stored as an array of Tetrahedron in the base class
		StridedVector<uint32> stridedVec(&(tetrahedra[0].idx[0]), tetrahedra.size(), sizeof(Tet));
		mTetMesh.reset(MeshFactory::MakeMesh<LinearTetrahedralMesh>(1, nodes.size(), stridedVec, tetrahedra.size()));

		// 2. Compute the interpolated positions
		// copy over the node positions of the linear mesh to a linear array
		// TODO: could use a strided vector instead
		std::vector<Vector3R> points(nodes.size());
		for (uint32 i = 0; i < nodes.size(); i++)
		{
			points[i] = nodes[i].pos;
		}

		// interpolate the mesh node positions (for the given order)
		// the first resulting nodes are the same as in the linear mesh
		std::vector<Vector3R> interpolatedPositions(GetNumNodes());
		//MeshFactory::Interpolate(*mTetMesh, &points[0], &interpolatedPositions[0]);
		interpolatedPositions = points;


		// 3. Mark all boundary nodes
		// initialize the fixed flags array
		std::vector<bool> fixed(GetNumNodes(), false);
		for (uint32 i = 0; i < nodes.size(); i++)
		{
			fixed[i] = nodes[i].invMass == 0;
		}

		// 4. Create a index-mapping from mesh-global-index to index into the mReferencePosition and mDeformedPositions vectors
		// - this is too allow all of the fixed nodes to be listed first in the two vectors.

		// create a mapping with the fixed nodes first
		mReshuffleMapInv.clear();
		for (uint32 i = 0; i < fixed.size(); i++)
		{
			if (fixed[i])
				mReshuffleMapInv.push_back(i);
		}
		mNumBCs = mReshuffleMapInv.size();
		for (uint32 i = 0; i < fixed.size(); i++)
		{
			if (!fixed[i])
				mReshuffleMapInv.push_back(i);
		}
		// create the reference positions - first ones are the fixed ones
		mReferencePositions.resize(GetNumNodes());
		mReshuffleMap.resize(GetNumNodes());
		for (uint32 i = 0; i < GetNumNodes(); i++)
		{
			uint32 idx = mReshuffleMapInv[i];
			mReferencePositions[i] = interpolatedPositions[idx];
			mReshuffleMap[idx] = i;
		}

		// 5. Copy the mReferencePositions into mDeformedPositions, to initialize it
		// - note that mDeformedPositions does not contain the boundary/fixed nodes.
		// create a vector of deformed positions (dofs)
		// the first mNumBCs nodes are fixed so we only consider the remaining ones
		mDeformedPositions.resize(GetNumFreeNodes());
		mDeformedPositions.assign(mReferencePositions.begin() + mNumBCs, mReferencePositions.end());
	}

	void FemPhysicsAnalyticCantileverNonLinearElasticity::Step(real dt)
	{
		// Do nothing
	}

	void FemPhysicsAnalyticCantileverNonLinearElasticity::SolveEquilibrium(float t)
	{
		// Work in progress!

		real min_x = INFINITY, min_y = INFINITY, max_x = -INFINITY, max_y = -INFINITY;

		for (uint32 i = 0; i < mReferencePositions.size(); i++)
		{
			if (mReferencePositions[i].x < min_x)
			{
				min_x = mReferencePositions[i].x;
			}
			if (mReferencePositions[i].x > max_x)
			{
				max_x = mReferencePositions[i].x;
			}
			if (mReferencePositions[i].y < min_y)
			{
				min_y = mReferencePositions[i].y;
			}
			if (mReferencePositions[i].y > max_y)
			{
				max_y = mReferencePositions[i].y;
			}
		}

		Printf("\nSOLVEEQUILIBRIUM\n");
		// TODO
		real a = std::abs(max_y - min_y);// 100.00;
		real L = std::abs(max_x - min_x);// 400.00;
		real thickness = a;
		real mid_y = min_y + a / 2.f;
		Printf(("a = " + std::to_string(a)).c_str());
		Printf((", L = " + std::to_string(L) + "\n").c_str());
		//real m = density * a * a * L;
		//Vector3R Q = mGravity;// *(m / L);
		Vector3R Q(0, -9.8f, 0);
		Q = Q * 100.f;
		//real I_x = std::pow(a, 4) / 12.f;
		real I_x = thickness * std::pow(a, 3) / 12.f;
		real h = -0.14f; // correct ?
		//Vector3R P(0, 0, 0);
		real E = 200;// this->mYoungsModulus;

		auto theta_f = [E, I_x, h, Q/*, P*/, L](real s)->real
		{
			return (1.f / (E*I_x))*(0.1667f * h * Q.y *s*s*s/* - 0.5f*P.y*s*/ - 0.5f*s*Q.y*L + 0.5f*Q.y*L*L /*+ L * P.y*/);
		};

		//auto integrate = [](real(*f)(real x), real a, real b, size_t n)->real
		auto integrate = [](std::function<real(real x)> f, real a, real b, size_t n)->real
		{
			real step = (b - a) / n;
			real area = 0.0f;
			for (size_t i = 0; i < n; i++)
			{
				area += f(a + (i + 0.5f) * step) * step;
			}
			return area;
		};

		auto x_int_f = [&theta_f](real s)->real
		{
			return std::cos(theta_f(s));
		};

		auto y_int_f = [&theta_f](real s)->real
		{
			return std::sin(theta_f(s));
		};

		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			Vector3R deflection(integrate(x_int_f, 0.f, mDeformedPositions[i].x, 100), integrate(y_int_f, 0.f, mDeformedPositions[i].x, 100), 0);
			//Vector3R x_deflection(deflection.x, 0, 0);
			//Vector3R y_deflection(0, deflection.y, 0);

			Printf(("i:" + std::to_string(i) + " ").c_str());
			Printf(("mDeformedPositions[i] = (" + std::to_string(mDeformedPositions[i].x) + "," + std::to_string(mDeformedPositions[i].y) + "," + std::to_string(mDeformedPositions[i].z) + ") ").c_str());
			//Printf(("mReferencePositions[i] = (" + std::to_string(mReferencePositions[i].x) + "," + std::to_string(mReferencePositions[i].y) + "," + std::to_string(mReferencePositions[i].z) + ") ").c_str());
			Printf(("deflection[i] = (" + std::to_string(deflection.x) + "," + std::to_string(deflection.y) + "," + std::to_string(deflection.z) + ") ").c_str());
			Printf(("theta=" + std::to_string(theta_f(mDeformedPositions[i].x))).c_str());
			Printf("\n");
			//real pre_y = mDeformedPositions[i].y;
			real x_deflection = mDeformedPositions[i].x - deflection.x;
			mDeformedPositions[i].x = deflection.x;
			mDeformedPositions[i].y -= deflection.y;
			continue;

			//mDeformedPositions[i] = Vector3R(x_deflection.x, y_deflection.y, mDeformedPositions[i].z);

			// TODO rotate the point on the y axis if needed instead of only moving it
			//Vector3R rotateAround(mDeformedPositions[i].x, mid_y, mDeformedPositions[i].z);
			real rad = theta_f(mDeformedPositions[i].x);
			real rotatedX = /*std::cos(rad) * (mDeformedPositions[i].x - rotateAround.x)*/ -std::sin(rad) * (mDeformedPositions[i].y - mid_y) + mDeformedPositions[i].x;
			real rotatedY = /*std::sin(rad) * (mDeformedPositions[i].x - rotateAround.x)*/ +std::cos(rad) * (mDeformedPositions[i].y - mid_y) + mid_y;

			mDeformedPositions[i].x = rotatedX + x_deflection;
			mDeformedPositions[i].y = rotatedY + deflection.y;
		}
	}
} // namespace FEM_SYSTEM
