#ifndef FEM_IO_H
#define FEM_IO_H

enum SwapCoordsType
{
	SCT_SWAP_NONE,
	SCT_SWAP_X_AND_Y,
	SCT_SWAP_X_AND_Z,
	SCT_SWAP_Y_AND_Z,
};

namespace FEM_SYSTEM
{
	class FemPhysicsBase;
	namespace IO
	{
		bool LoadFromFebFile(const char* path, std::vector<Node>& nodes, std::vector<Tet>& tets,
			std::vector<int>& fixedNodes, std::vector<uint32>& surfTris, std::set<uint32>& innerSurface, 
			real scale, FemConfig* config = nullptr, int* bcFlag = nullptr, std::vector<uint32>* bcIndices = nullptr);
		bool LoadFromXmlFile(const char* path, std::vector<Node>& nodes, std::vector<Tet>& tets,
			std::vector<int>& fixedNodes, std::vector<uint32>& surfTris, FemConfig& config, std::vector<CableDescriptor>& cables);
		bool LoadFromVolFile(const char* path, real scale, std::vector<Node>& nodes, std::vector<Tet>& tets);
		bool LoadFromTet1File(const char* path, real scale, const Vector3R& offset, std::vector<Node>& nodes, std::vector<Tet>& tets);
		void ExportToVTKFile(std::fstream& outfile, FemPhysicsBase* femPhysics);
		void ExportToVTKHeatMap(std::fstream& outfile, FemPhysicsBase* femPhysics);
	}
}

#endif // FEM_IO_H
