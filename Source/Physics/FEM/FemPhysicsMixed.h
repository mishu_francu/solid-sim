#ifndef FEM_PHYSICS_MIXED
#define FEM_PHYSICS_MIXED

#include "FemPhysicsLinearIncompressible.h"
#ifdef USE_GSL
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#endif

namespace FEM_SYSTEM
{
	class PrimalProblem;

	class FemPhysicsMixed : public FemPhysicsLinearIncompressible
	{
	public:
		FemPhysicsMixed(std::vector<Tet>& tetrahedra,
			std::vector<Node>& allNodes, const FemConfig& config);

		void Step(real dt) override;
		real ComputeEnergy(int level);

	private:
		void SolveStandardNewton();
		void SolveSaddlePoint();
		void SolveSaddlePointLS();
#ifdef USE_GSL
		void SolveSaddlePointGSL();
#endif
		void SolveThreeField();
		void SolvePrincipalStretches();
		PrimalProblem SolvePrimalNewton();
		PrimalProblem SolveUnconstrainedNCG();
		void SolveDualAscent();
		void SolveCondensedNewton();

		real GetCurrentVolumeErrors(EigenVector& errors, bool verbose = false);
		void AssembleGeometricStiffnessMatrix(const EigenVector& p, SparseMatrix& Kgeom) const;
		void AssembleGeometricStiffnessMatrixFD(const EigenVector& p, EigenMatrix& Kgeom);
		void ComputeGradients(std::vector<Vector3R>& r, const EigenVector& dx, int material = -1);

		template<class MATRIX>
		void ComputeStiffnessMatrix(MATRIX& K, bool update = true);
		EigenMatrix ComputeStdStiffnessMatrix(bool update = true);
		EigenVector ComputeStdRhs(int material = -1);
		EigenVector ComputePosRhs();

		void AssembleJacobianMatrixFD(EigenMatrix& J);

		uint32 GetNumContacts() const;

	private:
		real mTimeStep;
		EigenVector mContactMultipliers, mContactDepth;
		bool mCondenseContact = false;
		real mStepLength;
		EigenVector mVolStretches;

		friend class StandardProblem;
		friend class SaddlePointProblem;
		friend class ThreeFieldProblem;
		friend class PrimalProblem;
		friend class CondensedProblem;
		friend class PrincipalStretchesProblem;
#ifdef USE_GSL
		friend int gslfunc(const gsl_vector* x, void* params, gsl_vector* f);
		friend int gslfunc_df(const gsl_vector* x, void* p, gsl_matrix* J);
		friend int gslfunc_fdf(const gsl_vector* x, void* params, gsl_vector* f, gsl_matrix* J);
#endif
	};

} // namespace FEM_SYSTEM

#endif // FEM_PHYSICS_MIXED