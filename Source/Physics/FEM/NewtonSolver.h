#ifndef NEWTON_SOLVER_H
#define NEWTON_SOLVER_H

#include "LinearSolver.h"
#include <Engine/Types.h>
#include <Engine/Utils.h>

//namespace FEM_SYSTEM
//{
	enum VerboseLevel
	{
		VL_NONE = 0,
		VL_RESIDUAL,
		VL_MINIMUM,
		VL_DETAILED,
		VL_DEBUG,
	};

	enum LSCondition
	{
		LSC_ARMIJO,
		LSC_ENGINEERING,
	};

	// A simple Newton solver with no line search and no convergence criterion
	template<class PROBLEM, class MATRIX = EigenMatrix>
	class NewtonSolver
	{
	public:
		void Solve(PROBLEM& problem, uint32 size, EigenVector& solution);
		void ComputeJacobianFD(PROBLEM& problem, uint32 size, EigenVector& solution, MATRIX& K);

	public:
		int mNumIterations = 200;
		real mResidualThreshold = 1;
		bool mDebug = true;
		int mVerbose = 1;
		LinearSolverType mSolverType = LST_LU_PARDISO;
		bool mUseFiniteDiff = false;
	};

	// A naive back-tracking Newton solver of my own (halving the step)
	template<class PROBLEM, class MATRIX = EigenMatrix>
	class NewtonSolverBackTrack : public NewtonSolver<PROBLEM, MATRIX>
	{
	public:
		bool Solve(PROBLEM& problem, uint32 size, EigenVector& solution);

	public:
		int mLSCondition = LSC_ENGINEERING;
		real mAlpha = 1e-4;
		real mBackTrackFactor = 0.5;
		int mMaxBisection = 10;
		bool mUseProblemSolver = false;

		bool mUseBFGS = false;
		MATRIX mB; // B matrix in BFGS
		EigenMatrix mH; // H matrix in BFGS
	};

	// The Numerical Recipes version of the Newton solver
	//template<class PROBLEM>
	//class NewtonSolverNR : public NewtonSolverBackTrack<PROBLEM>
	//{
	//public:
	//	bool Solve(PROBLEM& problem, uint32 size, EigenVector& x);
	//	void LineSearch(EigenVector xold, real fold, EigenVector g, EigenVector p, 
	//		EigenVector& x, EigenVector& fvec, real& f, real stpmax, bool& check, PROBLEM& problem);
	//};

	template<class PROBLEM, class MATRIX>
	void NewtonSolver<PROBLEM, MATRIX>::ComputeJacobianFD(PROBLEM& problem, uint32 size, EigenVector& solution, MATRIX& K)
	{
		real eps = 1e-5; 
		K.resize(size, size);
		EigenVector rhs0 = problem.ComputeRhs(solution);
		for (uint32 j = 0; j < size; j++)
		{
			solution(j) += eps;
			EigenVector rhs = problem.ComputeRhs(solution);
			//K.block(0, j, size, 1) = -(rhs - rhs0) / eps;
			auto d = -(rhs - rhs0) / eps;
			for (uint32 i = 0; i < size; i++)
				K.coeffRef(i, j) = d(i);
			solution(j) -= eps;
		}
	}

	template<class PROBLEM, class MATRIX>
	void NewtonSolver<PROBLEM, MATRIX>::Solve(PROBLEM& problem, uint32 size, EigenVector& solution)
	{
		// working vars
		MATRIX K;
		EigenVector s(0), y(0); // dummy vars

		// assemble rhs
		EigenVector rhs = problem.ComputeRhs(solution);
		real res = rhs.norm();
		if (mVerbose)
		{
			Printf("%g\n", res);
		}

		// Newton steps (outer loop)
		bool converged = false;
		for (int iter = 0; iter < mNumIterations; iter++)
		{
			// assemble system matrix K
			if (mUseFiniteDiff)
				ComputeJacobianFD(problem, size, solution, K);
			else
				problem.ComputeSystemMatrix(s, y, K); // we assume ComputeRhs has already been called, so we don't need to send the current solution once more			
			if (mVerbose > VL_DETAILED)
				std::cout << "K" << std::endl << K << std::endl;

			// solve K * dx = b using Eigen
			LinearSolver solver;
			solver.Init(K, mSolverType);
			EigenVector delta = solver.Solve(rhs);

			solution = solution + delta;

			// assemble rhs
			rhs = problem.ComputeRhs(solution); // this also sends the final solution to the caller problem

			// compute the residual
			real res = rhs.norm();
			if (mVerbose)
			{
				Printf("%g\n", res);
			}
			if (mDebug && isnan(res))
			{
				if (mVerbose)
					Printf("nan rhs\n");
				break;
			}
			if (mDebug && problem.CheckForInversion())
			{
				if (mVerbose)
					Printf("Inverted elements\n");
			}
			if (res < mResidualThreshold)
			{
				if (mVerbose)
					Printf("Absolute residual convergence at iteration %d\n", iter + 1);
				converged = true;
				break;
			}
		}

		if (mVerbose && !converged)
			Printf("The Newton solver has not converged within the max number of iterations. Consider reducing the time step.\n");
	}

	template<class PROBLEM, class MATRIX>
	bool NewtonSolverBackTrack<PROBLEM, MATRIX>::Solve(PROBLEM& problem, uint32 size, EigenVector& solution)
	{
		// working vars
		EigenVector delta, solutionOld, rhs, newRhs;
		EigenVector s(0), y(0); // used by BFGS

		// assemble rhs
		rhs = problem.ComputeRhs(solution);
		real res = rhs.norm(); // residual
		if (this->mVerbose)
			Printf("%g\n", res);

		// Newton steps (outer loop)
		bool converged = false;
		for (int iter = 0; iter < this->mNumIterations; iter++)
		{

			// compute BFGS approximation
			if (iter > 0)
			{
				// compute the BFGS vectors anyway, in case the problem wants to use them
				s = solution - solutionOld; // or damping * delta
				y = newRhs - rhs;
				if (mUseBFGS)
				{
					mB = mB - (mB * s * s.transpose() * mB) / (s.transpose() * mB * s) + (y * y.transpose()) / (y.transpose() * s);
					//mH = mH - (mH * y * y.transpose() * mH) / (y.transpose() * mH * y) + (s * s.transpose()) / (y.transpose() * s);
				}
			}

			if (!mUseBFGS || iter == 0)
			{
				// assemble system matrix K
				problem.ComputeSystemMatrix(s, y, mB); // we assume ComputeRhs has already been called, so we don't need to send the current solution once more
				//mB = K;
				//mH = K.inverse(); // cheaper initial guess
			}

			if (!mUseProblemSolver)
			{
				// solve K * dx = b using Eigen
				LinearSolver solver;
				solver.Init(mB, this->mSolverType);
				//solver.SetTolerance(0.1);
				//solver.Init(K, LST_CG); // inexact Newton
				delta = solver.Solve(rhs);

				//delta = mH * rhs;
			}
			else
				delta = problem.SolveLinearSystem(mB, rhs, s, y);

			// save old solution
			solutionOld = solution;

			// try to find a suitable step length - "line search"
			real damping = 1;
			real Rold = -delta.transpose() * rhs;
			real fOld = problem.MeritResidual(rhs);
			//auto grad = -mB * rhs; // only for residual objective
			auto grad = -rhs; // when -rhs is the gradient of the objective
			//Printf("grad norm: %g\n", grad.norm());
			//Printf("det: %g\n", mB.determinant());
			real slope = delta.transpose() * grad;
			if (slope > 0 && this->mVerbose > VL_MINIMUM)
				Printf("positive slope %e\n", slope); // this is mostly for when minimizing the norm
			real minStep = 0;
			real maxStep = 1;
			int bisectionIterations = 0;
			bool giveup = false;
			while (true)
			{
				// update the solution
				solution = solutionOld + damping * delta;

				// compute new RHS (minus function)
				newRhs = problem.ComputeRhs(solution);

				// check if the solution is acceptable
				bool cond = true;
				if (mLSCondition == LSC_ARMIJO)
				{
					real fNew = problem.MeritResidual(newRhs);
					cond = fNew < fOld + mAlpha * damping * slope; // Armijo condition
				}
				else
				{
					real Rnew = -delta.transpose() * newRhs;
					cond = Rnew < 0.5 * Rold; // Bonet-Wood condition
				}
				if (!cond) // NR condition
				{
					maxStep = damping;
					bisectionIterations++;						
				}
				else
				{
					if (bisectionIterations > 10 // reached the bisection limit
						|| damping == 1) // or full step works
					{
						if (this->mVerbose > VL_MINIMUM)
							Printf("acceptable solution found\n");
						break;
					}
					else
					{
						minStep = damping;
						bisectionIterations++;
					}
				}

				damping = 0.5 * (minStep + maxStep); // TODO: use largest value from the roots
				if (this->mVerbose > VL_MINIMUM)
					Printf("step length %g\n", damping);
				if (damping < 1e-10)
				{
					if (this->mVerbose > VL_MINIMUM)
						Printf("step reduced too much - quitting\n");
					giveup = true;
					break;
				}
			} // end of back-track iteration
			if (this->mVerbose > VL_RESIDUAL)
			{
				if (damping < 1)
					Printf("step length: %.2g\n", damping);
			}

			// compute the new residual
			real newRes = newRhs.norm();
			// compute the relative change
			real rel = (res - newRes) / res;
			if (this->mVerbose)
			{
				if (this->mVerbose > VL_MINIMUM)
					Printf("residual %d: %g / %g (%e)\n", iter, newRes, res, rel);
				else
					Printf("%g\n", newRes);
			}

			// quit conditions
			if (this->mDebug && isnan(newRes))
			{
				if (this->mVerbose)
					Printf("nan rhs\n");
				solution = solutionOld;
				break;
			}
			if (giveup)
			{
				solution = solutionOld;
				break;
			}
			if (newRes < this->mResidualThreshold)
			{
				if (this->mVerbose > VL_RESIDUAL)
					Printf("Absolute residual convergence at iteration %d\n", iter + 1);
				converged = true;
				break;
			}
			if (abs(rel) < 1e-10)
			{
				if (this->mVerbose)
					Printf("The residual is decreasing too slow. Quitting at iteration %d.\n", iter + 1);
				break;
			}

			// update the RHS for the next step
			rhs = newRhs;
			res = newRes;
		} // end of Newton iteration

		if (this->mVerbose && !converged)
			Printf("The Newton solver has not converged within the max number of iterations. Consider reducing the time step.\n");

		return converged;
	}

	//template<class PROBLEM>
	//bool NewtonSolverNR<PROBLEM>::Solve(PROBLEM& problem, uint32 size, EigenVector& x) // TODO: drop size
	//{
	//	const real TOLF = 1e-8; // convergence tolerance on function value
	//	const real TOLX = std::numeric_limits<real>::epsilon();
	//	const real STPMX = 100;

	//	// working vars
	//	EigenMatrix K;
	//	EigenVector xold, g, p;
	//	real fold;

	//	bool check = false; // false on normal return, true if local minimum reached (spurious convergence)

	//	EigenVector fvec = -problem.ComputeRhs(x); // residual
	//	real f = 0.5 * fvec.squaredNorm(); // the minimization objective value

	//	// test for already being a solution
	//	real test = fvec.lpNorm<Eigen::Infinity>(); // max norm
	//	if (test < 0.01 * TOLF)
	//	{
	//		return true;
	//	}

	//	// compute max step
	//	real stpmax = STPMX * std::max<real>(x.norm(), size);

	//	// iteration loop
	//	for (int iter = 0; iter < mNumIterations; iter++)
	//	{
	//		K = problem.ComputeSystemMatrix();

	//		g = K * fvec; // grad f

	//		LinearSolver solver;
	//		solver.Init(K, LST_LU_PARDISO);
	//		p = solver.Solve(-fvec);

	//		// store old x and f (function value)
	//		xold = x;
	//		fold = f;

	//		LineSearch(xold, fold, g, p, x, fvec, f, stpmax, check, problem);

	//		// test for convergence on function value
	//		test = fvec.lpNorm<Eigen::Infinity>(); // max norm
	//		if (test < TOLF)
	//		{
	//			return true;
	//		}
	//		if (check)
	//		{
	//			// TODO
	//		}

	//		// TODO: test for convergence on delta
	//	}

	//	return true;
	//}

	//template<class PROBLEM>
	//void NewtonSolverNR<PROBLEM>::LineSearch(EigenVector xold, real fold, EigenVector g, EigenVector p, 
	//	EigenVector& x, EigenVector& fvec, real& f, real stpmax, bool& check, PROBLEM& problem)
	//{
	//	// full step for now
	//	//x = xold + p;

	//	const real TOLX = std::numeric_limits<real>::epsilon();
	//	const real ALF = 1e-4;

	//	uint32 size = xold.size();

	//	real len = p.norm();
	//	if (len > stpmax)
	//	{
	//		p *= stpmax / len; // scale if attempted step is too big
	//	}
	//	
	//	real slope = g.transpose() * p;
	//	if (slope >= 0)
	//	{
	//		// the slope should always be a descending one
	//		if (mVerbose)
	//			Printf("Roundoff problem in line search.\n");
	//		return;
	//	}

	//	real test = 0;
	//	for (uint32 i = 0; i < size; i++)
	//	{
	//		real temp = abs(p(i)) / std::max(abs(xold(i)), 1.0);
	//		if (temp > test)
	//			test = temp;
	//	}

	//	real alamin = TOLX / test;
	//	real alam = 1;
	//	real f2 = 0;
	//	real alam2 = 0;
	//	real tmplam;
	//	// start of Newton loop
	//	while (true)
	//	{
	//		// update solution
	//		x = xold + alam * p;

	//		// evaluate objective function
	//		fvec = -problem.ComputeRhs(x); // residual
	//		f = 0.5 * fvec.squaredNorm(); // the minimization objective value

	//		if (alam < alamin)
	//		{
	//			// convergence on dx
	//			x = xold;
	//			check = true;
	//			return;
	//		}
	//		else if (f <= fold + ALF * alam * slope)
	//		{
	//			return; // acceptable solution
	//		}
	//		else
	//		{
	//			// baktrack
	//			if (alam == 1) // first time
	//			{
	//				tmplam = -0.5 * slope / (f - fold - slope);
	//			}
	//			else // subsequent backtrack
	//			{
	//				real rhs1 = f - fold - alam * slope;
	//				real rhs2 = f2 - fold - alam2 * slope;
	//				real a = (rhs1 / (alam * alam) - rhs2 / (alam2 * alam2)) / (alam - alam2);
	//				real b = (-alam2 * rhs1 / (alam * alam) + alam * rhs2 / (alam2 * alam2)) / (alam - alam2);
	//				if (a == 0)
	//				{
	//					tmplam = -0.5 * slope / b;
	//				}
	//				else
	//				{
	//					real disc = b * b - 3 * a * slope;
	//					if (disc < 0)
	//						tmplam = 0.5 * alam;
	//					else if (b <= 0)
	//						tmplam = (-b + sqrt(disc)) / 3 / a;
	//					else
	//						tmplam = -slope / (b + sqrt(disc));
	//				}
	//				// enforce that the step is less than half
	//				if (tmplam > 0.5 * alam)
	//					tmplam = 0.5 * alam;
	//			}
	//		}

	//		alam2 = alam;
	//		f2 = f;
	//		alam = std::max(tmplam, 0.1 * alam);
	//		if (mVerbose)
	//			Printf("backtracking to %g\n", alam);
	//		problem.CheckForInversion(1);
	//	} // end of Newton loop
	//}

//} // namespace FEM_SYSTEM

#endif // NEWTON_SOLVER_H
