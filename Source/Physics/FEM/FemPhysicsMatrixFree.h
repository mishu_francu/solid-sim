#ifndef FEM_PHYSICS_H
#define FEM_PHYSICS_H
#include "FemPhysicsBase.h"
#include "PolarDecomposition.h"

//#define USE_MCL

#ifdef USE_MCL
#include <MCL/Problem.hpp>
#endif

namespace FEM_SYSTEM
{
	class FemPhysicsMatrixFree;

#ifdef USE_MCL
	class ElasticProblem : public mcl::optlib::Problem<real, Eigen::Dynamic>
	{
	public:
		ElasticProblem(FemPhysicsMatrixFree* femPhysics) : mFemPhysics(femPhysics), mCounter(0) { }
		bool converged(const VecX &x0, const VecX &x1, const VecX &grad) override;
		real value(const VecX &x) override;
		real gradient(const VecX &x, VecX &grad) override;
		void hessian(const VecX &x, MatX &hessian) override;

	private:
		uint32 mCounter;
		FemPhysicsMatrixFree* mFemPhysics;
	};
#endif

	class FemPhysicsMatrixFree : public FemPhysicsBase
	{
		public:
			enum NodalForcesType
			{
				NFT_DIRECT,
				NFT_FINITE_VOLUME,
			};

			struct Config
			{
				NodalForcesType mNodalForcesComputation = NFT_DIRECT; // not used
				NonlinearSolverType mSolver = NST_NEWTON;
				float mDescentRate = 1e-5f;
				bool mOptimizer = false;
			};

		public:
			FemPhysicsMatrixFree(std::vector<Tet>& tetrahedra,
				std::vector<Node>& allNodes, const FemConfig& config);

			void Step(real dt) override;
			
			const std::vector<Node>& GetNodes() const;
			std::vector<Node>& GetNodes(); // should we allow this?
			const std::vector<Tetrahedron>& GetTetrahedra() const;
			void UpdatePositions(std::vector<Node>& nodes) override;
			Vector3R GetDeformedPosition(uint32 idx, bool shuffle = true) const override { return nodes[shuffle ? mReshuffleMap[idx] : idx].pos; }
			Vector3R GetInitialPosition(uint32 idx) const override { return nodes[mReshuffleMap[idx]].pos0; } // Careful! These get over-written by implicit integrator
			void SetDeformedPosition(uint32 idx, Vector3R val, bool shuffle = true) override { nodes[shuffle ? mReshuffleMap[idx] : idx].pos = val; }
			Vector3R GetVelocity(uint32 idx) const override { return nodes[mReshuffleMap[idx]].vel; }
			uint32 GetNumNodes() const override { return (uint32)nodes.size(); }
			uint32 GetNumLocalNodes() const override { return 4; }
			uint32 GetNumFreeNodes() const override { ASSERT(mNumBCs < GetNumNodes()); return GetNumNodes() - mNumBCs; }
			uint32 GetNumElements() const override { return (uint32)tets.size(); }
			bool IsNodeFixed(uint32 i) const override { ASSERT(i < GetNumNodes()); return nodes[mReshuffleMap[i]].invMass == 0; }
			Matrix3R GetBarycentricJacobianMatrix(uint32 e) const override { return tets[e].Xtr; }
			real GetElementInitialVolume(uint32 e) const override { return tets[e].vol; }
			uint32 GetGlobalIndex(uint32 e, uint32 l) const override { return tets[e].i[l]; }
			uint32 GetGlobalOriginalIndex(uint32 e, uint32 l) const override { return originalTets[e].i[l]; }
			real GetTotalVolume() const;

			// Krylov solvers helpers
			void MatrixVectorMultiply(const std::vector<Vector3R>& in, std::vector<Vector3R>& out) const; // used for matrix free CG
			void ComputeGradients(std::vector<Vector3R>& r);
			void UpdatePosAndComputeGradients(const std::vector<Vector3R>& pos, std::vector<Vector3R>& r);
			real DotProduct(const std::vector<Vector3R>& a, const std::vector<Vector3R>& b) const { return InnerProduct<Vector3R, real>(a, b); }

			real ComputeEnergy(); // basically the minimization objective
			real ComputeEnergy(int level);

			// Newton solver helpers
			EigenVector ComputeRhs(const EigenVector& solution);			
			template<class MATRIX> void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, MATRIX& K);
			EigenVector SolveLinearSystem(EigenMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y);
			EigenVector SolveLinearSystem(SparseMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y);
			real MeritResidual(const EigenVector& rhs);

			Vector3R GetTotalDisplacement(uint32 i) const { return (nodes[i + mNumBCs].pos - nodes[i + mNumBCs].pos0); }
			void SetBoundaryConditionsSurface(const std::vector<uint32>& triangleList, real pressure) override;

			real ComputeSpringEnergy(Cable& cable);
			real ComputeSpringEnergy();

		private:
			// traditional FEM
			void SubStep(real h);
			bool Solve();

			// mixed FEM (constraint-based)
			void StepConstraint(real h);
			template<int Material> void StepConstraintVel(real h);
			template<int Material> void StepConstraintLinear_DirectSolver(real h);
			template<int Material> void StepConstraintNonlinear_DirectSolver(real h);

			// nonlinear static solvers
			void SolveNewtonCG();
			bool SolveNewton();
			bool SolveNewtonLS();
			void SolveNewtonMCL();
			void SolveGradientDescent(real alpha);
			void SolveNonlinearSteepestDescent();
			void SolveNonlinearConjugateGradient();
			void SolveNLopt();

			void ComputeDeformationGradient(uint32 e, Matrix3R& F) const override;
			template<int Material>
			void ComputeStrainAndJacobian(uint32 e, // tet index
				Eigen::Matrix<real, 6, 1>& eps, // strain vector
				Eigen::Matrix<real, 6, 12>& J); // Jacobian due to strain (de/dx)
			template<int Material>
			void ComputeStrain(const Tetrahedron& tet,
				const Matrix3R& F, // deformation gradient
				Eigen::Matrix<real, 6, 1>& eps); // strain vector
			template<int Material>
			void ComputeStrainJacobian(const Tetrahedron& tet,
				const Matrix3R& F, // deformation gradient
				Eigen::Matrix<real, 6, 12>& J); // Jacobian due to strain (de/dx)

			void ComputePressureForces(Vector3Array& fout, EigenMatrix& K) const;

			void ReshuffleFixedNodes();

			void AddCable(const Cable& cable) override;

		protected:
			void ComputeForceDifferential(const std::vector<Vector3R>& dx, std::vector<Vector3R>& df) const;
			void BuildMassMatrix();

		protected:
			std::vector<Node> nodes;
			std::vector<Tetrahedron> tets, originalTets;
			real ed, nud;
			Matrix3R Ed;
			bool hasCollisions;
			std::vector<Vector3R> disp; // displacements vector a.k.a u
			std::vector<Vector6, Eigen::aligned_allocator<Vector6>> lambdaAcc; // accumulated lambdas

			Config mConfig;

			real mTimeStep = 0;

			std::vector<Vector3R> mForces; // TODO: remove?
			SparseMatrix mMassMatrix;

			mutable std::vector<Matrix3R> dH;

			uint32 mNumSpringNodes = 0;

			friend class FemSystem;
			friend class ElasticProblem;
	};

	inline const std::vector<Node>& FemPhysicsMatrixFree::GetNodes() const
	{
		return nodes;
	}

	inline std::vector<Node>& FemPhysicsMatrixFree::GetNodes()
	{
		return nodes;
	}

	inline const std::vector<Tetrahedron>& FemPhysicsMatrixFree::GetTetrahedra() const
	{
		return tets;
	}

	// TODO: include FemPhysics.inl

	template<int Material>
	inline void FemPhysicsMatrixFree::ComputeStrainJacobian(const Tetrahedron& tet,
		const Matrix3R& F, // deformation gradient
		Eigen::Matrix<real, 6, 12>& J) // Jacobian due to strain (de/dx)
	{
		static Matrix3R id; // identity matrix
		Matrix3R Js[4]; // normal Jacobian (x4)
		Matrix3R Jn[4]; // shear Jacobian (x4)

		Matrix3R Ftr = !F;

		// TODO: we are wasting space, as all 4 matrices are the same
		if (Material == MMT_STVK)
		{
			// Green strain
			for (int j = 0; j < 4; j++)
			{
				Jn[j] = tet.Hn[j] * Ftr;
				Js[j] = tet.Hs[j] * Ftr;
			}
		}
		else if (Material == MMT_LINEAR)
		{
			// Cauchy strain
			for (int j = 0; j < 4; j++)
			{
				Jn[j] = tet.Hn[j];
				Js[j] = tet.Hs[j];
			}
		}
		else if (Material == MMT_COROTATIONAL)
		{
			Matrix3R R, U;
			ComputePolarDecomposition(F, R, U); // TODO: provide as input
			for (int j = 0; j < 4; j++)
			{
				Jn[j] = tet.Hn[j] * !R;
				Js[j] = tet.Hs[j] * !R;
			}
		}

		// build Jacobian matrix
		for (int k = 0; k < 4; k++)
		{
			for (int l = 0; l < 3; l++)
			{
				for (int m = 0; m < 3; m++)
				{
					J(l + 3, m + k * 3) = Js[k][l][m];
					J(l, m + k * 3) = Jn[k][l][m];
				}
			}
		}
	}

	template<int Material>
	void FemPhysicsMatrixFree::ComputeStrain(const Tetrahedron& tet,
		const Matrix3R& F, // deformation gradient
		Eigen::Matrix<real, 6, 1>& eps) // strain vector
	{
		static Matrix3R id; // identity matrix
		Matrix3R strain; // strain tensor

		Matrix3R Ftr = !F;

		if (Material == MMT_STVK)
		{
			// Green strain
			strain = 0.5f * (Ftr * F - id); // Green strain
		}
		else if (Material == MMT_LINEAR)
		{
			// Cauchy strain
			strain = 0.5f * (F + !F) - id; // Cauchy strain
		}
		else if (Material == MMT_COROTATIONAL)
		{
			Matrix3R R, U;
			ComputePolarDecomposition(F, R, U);
			strain = 0.5f * (U + !U) - id; // Cauchy strain
		}

		// convert strain tensor to 6-vector
		Vector3R epsN(strain[0][0], strain[1][1], strain[2][2]);
		Vector3R epsS(strain[1][2], strain[0][2], strain[0][1]);
		for (int j = 0; j < 3; j++)
		{
			eps[j] = epsN[j];
			eps[j + 3] = epsS[j];
		}
	}

}
#endif // FEM_PHYSICS_MATRIX_FREE_H