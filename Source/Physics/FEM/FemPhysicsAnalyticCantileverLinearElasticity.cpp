#include "FemDataStructures.h"
#include "FemPhysicsAnalyticCantileverLinearElasticity.h"
#include <Physics/FEM/MeshFactory.h>
#include <Math/Utils.h>

#pragma warning( disable : 4267) // for size_t-uint conversions

namespace FEM_SYSTEM
{
	FemPhysicsAnalyticCantileverLinearElasticity::FemPhysicsAnalyticCantileverLinearElasticity(std::vector<Tet>& tetrahedra,
		std::vector<Node>& nodes, const FemConfig& config)
		: FemPhysicsBase(config)
		, density(1250.f) // silicon rubber density
	{
		// 1. Build the higher order mesh/convert to a TetrahedralMesh instance
		// use the linear mesh stored as an array of Tetrahedron in the base class
		StridedVector<uint32> stridedVec(&(tetrahedra[0].idx[0]), tetrahedra.size(), sizeof(Tet));
		mTetMesh.reset(MeshFactory::MakeMesh<LinearTetrahedralMesh>(1, nodes.size(), stridedVec, tetrahedra.size()));

		// 2. Compute the interpolated positions
		// copy over the node positions of the linear mesh to a linear array
		// TODO: could use a strided vector instead
		std::vector<Vector3R> points(nodes.size());
		for (uint32 i = 0; i < nodes.size(); i++)
		{
			points[i] = nodes[i].pos;
		}

		// interpolate the mesh node positions (for the given order)
		// the first resulting nodes are the same as in the linear mesh
		std::vector<Vector3R> interpolatedPositions(GetNumNodes());
		//MeshFactory::Interpolate(*mTetMesh, &points[0], &interpolatedPositions[0]);
		interpolatedPositions = points;


		// 3. Mark all boundary nodes
		// initialize the fixed flags array
		std::vector<bool> fixed(GetNumNodes(), false);
		for (uint32 i = 0; i < nodes.size(); i++)
		{
			fixed[i] = nodes[i].invMass == 0;
		}

		// 4. Create a index-mapping from mesh-global-index to index into the mReferencePosition and mDeformedPositions vectors
		// - this is too allow all of the fixed nodes to be listed first in the two vectors.

		// create a mapping with the fixed nodes first
		mReshuffleMapInv.clear();
		for (uint32 i = 0; i < fixed.size(); i++)
		{
			if (fixed[i])
				mReshuffleMapInv.push_back(i);
		}
		mNumBCs = mReshuffleMapInv.size();
		for (uint32 i = 0; i < fixed.size(); i++)
		{
			if (!fixed[i])
				mReshuffleMapInv.push_back(i);
		}
		// create the reference positions - first ones are the fixed ones
		mReferencePositions.resize(GetNumNodes());
		mReshuffleMap.resize(GetNumNodes());
		for (uint32 i = 0; i < GetNumNodes(); i++)
		{
			uint32 idx = mReshuffleMapInv[i];
			mReferencePositions[i] = interpolatedPositions[idx];
			mReshuffleMap[idx] = i;
		}

		// 5. Copy the mReferencePositions into mDeformedPositions, to initialize it
		// - note that mDeformedPositions does not contain the boundary/fixed nodes.
		// create a vector of deformed positions (dofs)
		// the first mNumBCs nodes are fixed so we only consider the remaining ones
		mDeformedPositions.resize(GetNumFreeNodes());
		mDeformedPositions.assign(mReferencePositions.begin() + mNumBCs, mReferencePositions.end());
	}

	void FemPhysicsAnalyticCantileverLinearElasticity::Step(real dt)
	{
		// Do nothing
	}

	void FemPhysicsAnalyticCantileverLinearElasticity::SolveEquilibrium(float t)
	{
		real min_x = INFINITY, min_y = INFINITY, min_z = INFINITY, max_x = -INFINITY, max_y = -INFINITY, max_z = -INFINITY;

		for (uint32 i = 0; i < mReferencePositions.size(); i++)
		{
			if (mReferencePositions[i].x < min_x)
			{
				min_x = mReferencePositions[i].x;
			}
			if (mReferencePositions[i].x > max_x)
			{
				max_x = mReferencePositions[i].x;
			}
			if (mReferencePositions[i].y < min_y)
			{
				min_y = mReferencePositions[i].y;
			}
			if (mReferencePositions[i].y > max_y)
			{
				max_y = mReferencePositions[i].y;
			}
			if (mReferencePositions[i].z < min_z)
			{
				min_z = mReferencePositions[i].z;
			}
			if (mReferencePositions[i].z > max_z)
			{
				max_z = mReferencePositions[i].z;
			}
		}

		real width = std::abs(max_y - min_y);
		real length = std::abs(max_x - min_x);
		real thickness = std::abs(max_z - min_z);
		real mid_y = min_y + (width / 2.f);

		real m = density * width * thickness * length; // Correct?
		real q = mGravity.y * (m / length); // Correct?
		real I_x = thickness * std::pow(width, 3) / 12.f;
		real E = this->mYoungsModulus;

		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			real x = mDeformedPositions[i].x;
			x -= min_x;
			real y_deflection = (q * x*x * (6 * length*length - 4 * length*x + x * x)) / (24.f * E * I_x);

			mDeformedPositions[i].y += y_deflection;

			// rotate the point, to make it look good as well
			x = mDeformedPositions[i].x;
			Vector3R origin(x, mid_y + y_deflection, 0);
			Vector3R ref(min_x, mid_y, 0);
			Vector3R vec1 = Vector3R(x, mid_y, 0) - ref;
			Vector3R vec2 = origin - ref;

			if (vec1.Dot(vec2) != 0)
			{
				real rads = 2.f*PI * (vec1.Dot(vec2)) / (vec1.Length() * vec2.Length());

				mDeformedPositions[i].Subtract(origin);
				mDeformedPositions[i].x = mDeformedPositions[i].x * cos(rads) - mDeformedPositions[i].y * sin(rads);
				mDeformedPositions[i].y = mDeformedPositions[i].x * sin(rads) + mDeformedPositions[i].y * cos(rads);
				mDeformedPositions[i].Add(origin);
			}
		}
	}
} // namespace FEM_SYSTEM
