#ifndef POLAR_DECOMPOSITION_H
#define POLAR_DECOMPOSITION_H

namespace FEM_SYSTEM
{
	class PolarDecomposition
	{
	public:
		static bool EigenPolarDecomposition(Matrix3R const & A, Matrix3R & R, Matrix3R & S);
		static void QRPolarDecomposition(const Matrix3R& F, Matrix3R& R);
	};

	void ComputePolarDecomposition(const Matrix3R& F, Matrix3R& R, Matrix3R& U);
}

#endif // POLAR_DECOMPOSITION_H
