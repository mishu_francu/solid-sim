#ifndef FEM_PHYSICS_ANALYTIC_CANTILEVER_NONLINEAR_ELASTICITY_H
#define FEM_PHYSICS_ANALYTIC_CANTILEVER_NONLINEAR_ELASTICITY_H

#include <Physics/FEM/FemPhysicsBase.h>
#include <Physics/FEM/LinearTetrahedralMesh.h>

namespace FEM_SYSTEM
{
	class FemPhysicsAnalyticCantileverNonLinearElasticity : public FemPhysicsBase
	{
	public:
		FemPhysicsAnalyticCantileverNonLinearElasticity(std::vector<Tet>& tetrahedra,
			std::vector<Node>& allNodes, const FemConfig& config);

		uint32 GetNumNodes() const override { return mTetMesh->GetNumNodes(); }
		uint32 GetNumFreeNodes() const { ASSERT(mNumBCs < GetNumNodes()); return GetNumNodes() - mNumBCs; }
		bool IsNodeFixed(uint32 i) const { ASSERT(i < GetNumNodes()); return mReshuffleMap[i] < mNumBCs; }
		Vector3R GetDeformedPosition(uint32 i) const ;
		LinearTetrahedralMesh* GetTetMesh() { return mTetMesh.get(); }
		real GetTotalVolume() const { return 0; }

	protected:
		//uint32 mOrder;
		std::unique_ptr<LinearTetrahedralMesh> mTetMesh; // the topological data

		std::vector<Vector3R> mDeformedPositions; // nodal spatial positions
		std::vector<Vector3R> mReferencePositions; // nodal material positions

		uint32 mNumBCs;
		real density; // body density

		std::vector<uint32> mReshuffleMapInv;
		std::vector<uint32> mReshuffleMap;
	
	public:
		void Step(real dt) override;
		void SolveEquilibrium(float);
	};

	inline Vector3R FemPhysicsAnalyticCantileverNonLinearElasticity::GetDeformedPosition(uint32 i) const
	{
		uint32 idx = mReshuffleMap[i];
		return idx >= mNumBCs ? mDeformedPositions[idx - mNumBCs] : mReferencePositions[idx];
	}

} // namespace FEM_SYSTEM

#endif // FEM_PHYSICS_ANALYTIC_CANTILEVER_NONLINEAR_ELASTICITY_H
