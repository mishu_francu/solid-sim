// SolidFEM.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <Physics/FEM/FemDataStructures.h>
#include <Physics/FEM/FemIO.h>
#include <Physics/FEM/FemPhysicsMatrixFree.h>
#include <Physics/FEM/FemPhysicsMixed.h>
//#include <Engine/Profiler.h>

FILE* out;

using namespace FEM_SYSTEM;

void CreateCable(const CableDescriptor& descriptor, 
	const std::vector<Node>& nodes,
	const std::vector<Tet>& tets,
	FemPhysicsBase* femPhysics,
	real scale = 1)
{
	Cable cable;
	int numSprings = descriptor.divs;
	real div = descriptor.length / numSprings;
	cable.mCableNodes.resize(numSprings + 1);
	cable.mCablePositions.resize(numSprings + 1);
	// generate a sequence of points along x axis
	for (int i = 0; i <= numSprings; i++)
	{
		Vector3R pos = i * div * descriptor.dir;
		pos += descriptor.offset;
		// register the point to the tet mesh
		real minScore = 1e20;
		int elem = -1;
		real minScorePos = 1e20;
		int elemPos = -1;
		for (uint32 e = 0; e < tets.size(); e++)
		{
			real score = 0;
			const Tet& tet = tets[e];

			Vector3R x0 = scale * nodes[tet.idx[0]].pos;
			Vector3R x1 = scale * nodes[tet.idx[1]].pos;
			Vector3R x2 = scale * nodes[tet.idx[2]].pos;
			Vector3R x3 = scale * nodes[tet.idx[3]].pos;
			real w0, w1, w2, w3;
			ComputeBarycentric(pos, x0, x1, x2, x3, w0, w1, w2, w3);
			real eps = -0.001;
			bool positive = w0 > eps && w1 > eps && w2 > eps && w3 > eps;

			for (int j = 0; j < 4; j++)
			{
				Vector3R node = 100.0 * nodes[tet.idx[j]].pos;
				score += (node - pos).LengthSquared();
			}
			if (score < minScore)
			{
				minScore = score;
				elem = e;
			}
			if (positive && score < minScorePos)
			{
				minScorePos = score;
				elemPos = e;
			}
		}
		if (elemPos >= 0)
			elem = elemPos;
		const Tet& tet = tets[elem];
		Vector3R x0 = scale * nodes[tet.idx[0]].pos;
		Vector3R x1 = scale * nodes[tet.idx[1]].pos;
		Vector3R x2 = scale * nodes[tet.idx[2]].pos;
		Vector3R x3 = scale * nodes[tet.idx[3]].pos;
		real w0, w1, w2, w3;
		ComputeBarycentric(pos, x0, x1, x2, x3, w0, w1, w2, w3);
		Printf("%g, %g, %g, %g\n", w0, w1, w2, w3);
		real eps = 0.1;
		if (descriptor.useFreeCable && (w0 < -eps || w1 < -eps || w2 < -eps || w3 < -eps))
			cable.mCableNodes[i].elem = -elem;
		else
			cable.mCableNodes[i].elem = elem;
		cable.mCableNodes[i].bary.Set(w0, w1, w2);
		cable.mCablePositions[i] = (1 / scale) * pos;
	}

	cable.mActuation = 0.5;
	cable.mCableRestLength = div / scale;
	cable.mCableStiffness = descriptor.stiffness;

	femPhysics->AddCable(cable);
}

int main(int argc, char* argv[], char* envp[])
{
	if (argc < 2)
	{
		std::cout << "Please specify a file name" << std::endl;
		return -1;
	}

	// init a default config
	int numSteps = 10;
	bool noLineSearch = false;
	double scale = 1;
	bool optimizer = false;

	FemConfig femConfig;
	femConfig.mYoungsModulus = 100000;
	femConfig.mPoissonRatio = 0.499;
	femConfig.mGravity = 0;
	femConfig.mType = MT_NONLINEAR_ELASTICITY;
	femConfig.mOrder = 1;
	femConfig.mSimType = ST_QUASI_STATIC;
	femConfig.mNumSubsteps = 1;
	femConfig.mCustomConfig = nullptr;
	femConfig.mOuterIterations = 100;
	femConfig.mInnerIterations = 1;
	femConfig.mForceApplicationStep = 1.0 / numSteps;
	femConfig.mHasCollisions = false;
	femConfig.mAbsNewtonRsidualThreshold = 0.1;
	femConfig.mVerbose = true;
	femConfig.mAppliedPressure = 4000;
	femConfig.mZUpAxis = false;

	for (int i = 2; i < argc; i++)
	{
		const char* flag = argv[i];
		if (flag[0] == '-' && flag[1] == 'S')
		{
			flag += 2;
			scale = atof(flag);
		}
	}

	// load the mesh+BCs+config from a FEB file
	std::vector<Node> nodes;
    std::vector<Tet> tets;
    std::vector<int> fixedNodes;
    std::vector<uint32> surfTris;
    std::set<uint32> innerSurface;
	std::vector<uint32> bcIndices;
	std::vector<CableDescriptor> cables;
	int bcFlag;
	Printf("Loading %s...\n", argv[1]);
	int len = strlen(argv[1]);
	bool ret = false;
	if (argv[1][len - 3] == 'f' && argv[1][len - 2] == 'e' && argv[1][len - 1] == 'b')
		ret = IO::LoadFromFebFile(argv[1], nodes, tets, fixedNodes, surfTris, innerSurface, scale, &femConfig, &bcFlag, &bcIndices);
	else
		ret = IO::LoadFromXmlFile(argv[1], nodes, tets, fixedNodes, surfTris, femConfig, cables);
	if (ret)
    {
        Printf("Num nodes: %d\n", nodes.size());
        Printf("Num tets: %d\n", tets.size());
		Printf("Num fixed: %d\n", fixedNodes.size());
    }
    else
    {
        std::cout << "Load failed" << std::endl;
        return -1;
    }

	numSteps = (int)ceil(1.0 / femConfig.mForceApplicationStep);

	// FIXME
	for (size_t i = 0; i < nodes.size(); i++)
	{
		nodes[i].pos0 = nodes[i].pos;
	}
	for (size_t i = 0; i < tets.size(); i++)
	{
		Tet& tet = tets.at(i);
		const Vector3R& x0 = nodes.at(tet.idx[0]).pos0;
		const Vector3R& x1 = nodes.at(tet.idx[1]).pos0;
		const Vector3R& x2 = nodes.at(tet.idx[2]).pos0;
		const Vector3R& x3 = nodes.at(tet.idx[3]).pos0;
		Vector3R d1 = x1 - x0;
		Vector3R d2 = x2 - x0;
		Vector3R d3 = x3 - x0;
		Matrix3R mat(d1, d2, d3); // this is the reference shape matrix Dm [Sifakis][Teran03]
		real vol = (mat.Determinant()) / 6.f; // signed volume of the tet
		if (vol < 0)
		{
			// swap the first two indices so that the volume is positive next time
			std::swap(tet.idx[0], tet.idx[1]);
		}
	}

	// override or set properties from the command line
	std::string suffix;
	for (int i = 2; i < argc; i++)
	{
		const char* flag = argv[i];
		if (flag[0] == '-' && flag[1] == 'M')
		{
			flag += 2;
			if (strcmp(flag, "mixed") == 0)
			{
				femConfig.mType = MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY;
				suffix += "_mixed";
			}
			else if (strcmp(flag, "nonlinear") == 0)
			{
				femConfig.mType = MT_NONLINEAR_ELASTICITY;
				suffix += "_nonlin";
			}
		}
		else if (flag[0] == '-' && flag[1] == 'p')
		{
			flag += 2;
			real p = atof(flag);
			femConfig.mAppliedPressure = p;
			suffix = suffix + "_" + flag;
		}
		else if (flag[0] == '-' && flag[1] == 'P')
		{
			flag += 2;
			femConfig.mPoissonRatio = atof(flag);
		}
		else if (flag[0] == '-' && flag[1] == 'N')
		{
			flag += 2;
			numSteps = atoi(flag);
			femConfig.mForceApplicationStep = 1.0 / numSteps;
		}
		else if (flag[0] == '-' && flag[1] == 'I')
		{
			flag += 2;
			femConfig.mOuterIterations = atoi(flag);
		}
		else if (flag[0] == '-' && flag[1] == 'G')
		{
			flag += 2;
			femConfig.mGravity = atof(flag);
		}
		else if (flag[0] == '-' && flag[1] == 'R')
		{
			flag += 2;
			femConfig.mAbsNewtonRsidualThreshold = atof(flag);
		}
		else if (flag[0] == '-' && flag[1] == 'Z')
		{
			femConfig.mZUpAxis = true;
		}
		else if (flag[0] == '-' && flag[1] == 'O')
		{
			optimizer = true;
			suffix += "_opt";
		}
		else if (flag[0] == '-')
		{
			flag += 1;
			if (strcmp(flag, "nols") == 0)
			{
				noLineSearch = true;
				suffix += "_nols";
			}
		}
	}

	std::string path(argv[1]);
	std::string name = path.substr(0, path.size() - 4) + suffix;
	std::string outPath = name + ".txt";
	fopen_s(&out, outPath.c_str(), "wt");

	for (uint32 i = 0; i < fixedNodes.size(); i++)
    {
        int idx = fixedNodes[i];
        nodes[idx].invMass = 0.f;
    }

	Printf("Constructing simulator...\n");
	FemPhysicsBase* femPhysics = nullptr;
	FemPhysicsMatrixFree::Config nonlinConfig;
	FemPhysicsMixed::Config mixedConfig;
	if (femConfig.mType == MT_NONLINEAR_ELASTICITY)
	{
		femConfig.mMaterial = (MaterialModelType)MMT_NEO_HOOKEAN;

		nonlinConfig.mSolver = noLineSearch ? NST_NEWTON : NST_NEWTON_LS;
		nonlinConfig.mOptimizer = optimizer;
		femConfig.mCustomConfig = &nonlinConfig;

		femPhysics = new FemPhysicsMatrixFree(tets, nodes, femConfig);
	}
	else if (femConfig.mType == MT_INCOMPRESSIBLE_NONLINEAR_ELASTICITY)
	{
		femConfig.mMaterial = (MaterialModelType)MMT_DISTORTIONAL_OGDEN;
		//femConfig.mInvBulkModulus = 0;
		//femConfig.mPoissonRatio = 0.45;
		mixedConfig.mSolver = noLineSearch ? NST_NEWTON : NST_NEWTON_LS;
		mixedConfig.mConstraintScale = 1;
		//mixedConfig.mPressureOrder = 0;
		femConfig.mCustomConfig = &mixedConfig;
		femPhysics = new FemPhysicsMixed(tets, nodes, femConfig);
	}

	// add dynamic BCs
	for (size_t i = 0; i < bcIndices.size(); i++)
	{
		femPhysics->AddDirichletBC(bcIndices[i], bcFlag);
	}

	Printf("FEM Config:\n");
	Printf("-----------\n");
	Printf("\tE:\t%g\n", femConfig.mYoungsModulus);
	Printf("\tnu:\t%g\n", femConfig.mPoissonRatio);
	Printf("\tp:\t%g\n", femConfig.mAppliedPressure);
	Printf("\tg:\t%g\n", femConfig.mGravity);
	Printf("\tmethod:\t%d\n", femConfig.mType);
	Printf("\tmat:\t%d\n", femConfig.mMaterial);
	Printf("\tsolver:\t%s\n", noLineSearch ? "Newton" : "Newton + line search");
	Printf("\titers:\t%d\n", femConfig.mOuterIterations);
	Printf("\tsteps:\t%d\n", numSteps);
	Printf("\tres:\t%g\n", femConfig.mAbsNewtonRsidualThreshold);

	if (!surfTris.empty())
    {
        femPhysics->SetBoundaryConditionsSurface(surfTris, -femConfig.mAppliedPressure); // flip the sign for now
    }

	for (CableDescriptor& desc : cables)
	{
		CreateCable(desc, nodes, tets, femPhysics);
	}

	Printf("Solving...\n");
	//double start = Profiler::GetTimeMsStatic();
    for (int i = 0; i < numSteps; i++)
    {
		Printf("Step %d\n", i + 1);
        femPhysics->Step(0);
    }
	//double stop = Profiler::GetTimeMsStatic();
	//Printf("Elapsed time: %g s\n", (stop - start) / 1000);

	Printf("Saving VTK file\n");
	std::fstream vtkStream;
	std::string vtkPath = name + ".vtk";
	vtkStream.open(vtkPath, std::fstream::out);
	IO::ExportToVTKHeatMap(vtkStream, femPhysics);
	vtkStream.close();
	
	return 0;
}