#include "FemDataStructures.h"
#include "FemPhysicsLinearMixed.h"
#include <Physics/FEM/MeshFactory.h>
#include <iostream>

#pragma warning( disable : 4244) // for double-float conversions
#pragma warning( disable : 4267) // for size_t-uint conversions

//#define RIGID

namespace FEM_SYSTEM
{
	FemPhysicsLinearMixed::FemPhysicsLinearMixed(std::vector<Tet>& tetrahedra,
		std::vector<Node>& nodes, const FemConfig& config)
		: FemPhysicsLinear(tetrahedra, nodes, config)
		, mStressOrder(0)
	{
		mNumStressNodes = mStressOrder == 1 ? nodes.size() : GetNumElements();
		// build the stress tet mesh
		if (mStressOrder > 0)
		{
			// use the linear mesh stored as an array of Tetrahedron in the base class
			StridedVector<uint32> stridedVec(&(tetrahedra[0].idx[0]), tetrahedra.size(), sizeof(Tet));
			mStressMesh.reset(MeshFactory::MakeMesh<LinearTetrahedralMesh>(mStressOrder, nodes.size(), stridedVec, tetrahedra.size()));
		}

		AssembleComplianceMatrix();

		EigenMatrix denseM(mMassMatrix);
		mInverseMassMatrix = denseM.inverse();

		// assemble the global Jacobian
		AssembleJacobianMatrix(mJacobianMatrix);

		mStiffnessMatrix = mJacobianMatrix.transpose() * mComplianceMatrix.inverse() * mJacobianMatrix;
	}

	void FemPhysicsLinearMixed::Step(real dt)
	{
		const int numSteps = 1; // small sub-steps for stability
		real h = dt / numSteps;
		for (int i = 0; i < numSteps; i++)
		{
			StepConstraint(h);
		}
	}

	void FemPhysicsLinearMixed::SolveEquilibrium(float t)
	{
		auto decomp = mStiffnessMatrix.fullPivLu();
		EigenVector sol = decomp.solve(GetEigenVector(mBodyForces));
		Vector3Array u = GetStdVector(sol);
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			mDeformedPositions[i] += u[i];
	}

	void FemPhysicsLinearMixed::ComputeLocalJacobianBB2(uint32 elem, Matrix3R Bn[10], Matrix3R Bs[10])
	{
		for (int A = 0; A < 10; A++)
		{
			Vector3R v;
			for (int n = 0; n < 4; n++)
			{
				MultiIndex B = mTetMesh->GetIJKL(A);
				if (B.Decrement(n))
					v += mBarycentricJacobians[elem].y[n];
			}

			// the normal matrix for node A
			Bn[A] = Matrix3R(v); // diagonal matrix

			// the shear matrix for node A
			Bs[A] = Symm(v);
		}
	}

	// TODO: sparse matrices
	void FemPhysicsLinearMixed::StepConstraint(real h)
	{
		// prepare global matrices and vectors
		uint32 numNodes = GetNumFreeNodes();

		// compute external forces
		EigenVector sol = mInverseMassMatrix * GetEigenVector(mBodyForces);
		Vector3Array aext = GetStdVector(sol);

		// candidate positions - these are only needed for the strain (in the RHS)
		for (size_t i = 0; i < GetNumFreeNodes(); i++)
		{
			mVelocities[i] += h * aext[i];
			mDeformedPositions[i] += h * mVelocities[i];
		}

		// compute displacement vector
		Vector3Array u(numNodes);
		for (size_t i = 0; i < numNodes; i++)
		{
			u[i] = mDeformedPositions[i] - mReferencePositions[i + mNumBCs];
		}

		EigenVector b = mJacobianMatrix * GetEigenVector(u);

		// form the linear system and solve it
		if (mSystemMatrix.rows() == 0)
		{
			mSystemMatrix = h * h * mJacobianMatrix * mInverseMassMatrix * mJacobianMatrix.transpose();
#ifndef RIGID
			mSystemMatrix += mComplianceMatrix;
#endif
			//mFullPivLU = mSystemMatrix.fullPivLu();
			mLLT = mSystemMatrix.llt();
		}
		EigenVector lambda = mLLT.solve(b);
		EigenVector f = mJacobianMatrix.transpose() * lambda;

		// solve the linear system for the accelerations
		sol = mInverseMassMatrix * f;
		Vector3Array a = GetStdVector(sol);

		// integrate node velocities and positions
		for (size_t i = 0; i < numNodes; i++)
		{
			mVelocities[i] -= h * a[i];
			mDeformedPositions[i] -= h * h * a[i];
		}
	}

	void FemPhysicsLinearMixed::AssembleComplianceMatrix()
	{
		// compute the inverse of the elasticity matrix
		Matrix3R Einv = mNormalElasticityMatrix.GetInverse(); // TODO: cache
		const real mu = 0.5 * mYoungsModulus / (1.f + mPoissonRatio);
		const real muInv = 1.f / mu;

		// the 6x6 compliance matrix (inverse of elasticity matrix)
		Eigen::Matrix<real, NUM_STRESS_COMPONENTS, NUM_STRESS_COMPONENTS> Cconst;
		Cconst.setZero();
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				Cconst(i, j) = Einv[i][j]; // the normal part
			}
			Cconst(i + 3, i + 3) = muInv; // the shear part (diagonal)
		}

		// the local compliance matrix template
		const uint32 numLocalStressNodes = GetNumStressLocalNodes();
		const uint32 numLocalStressDofs = numLocalStressNodes * NUM_STRESS_COMPONENTS;
		EigenMatrix Clocal(numLocalStressDofs, numLocalStressDofs);
		Clocal.setZero();
		for (uint32 i = 0; i < numLocalStressNodes; i++)
		{
			for (uint32 j = 0; j < numLocalStressNodes; j++)
			{
				real factor = 1.f;
				if (mStressOrder == 1)
				{
					//factor = 0.1f * ComputeMultiIndexSumFactor(1, MultiIndex(mStressMesh->mIJKL + i * 4), MultiIndex(mStressMesh->mIJKL + j * 4));
					factor = i == j ? 0.1 : 0.05;
				}
				for (uint32 k = 0; k < NUM_STRESS_COMPONENTS; k++)
				{
					for (uint32 l = 0; l < NUM_STRESS_COMPONENTS; l++)
					{
						Clocal(i * NUM_STRESS_COMPONENTS + k, j * NUM_STRESS_COMPONENTS + l) = Cconst(k, l) * factor;
					}
				}
			}
		}

		// assemble the global compliance matrix 
		uint32 numStressNodes = GetNumStressNodes();
		uint32 numStressDofs = numStressNodes * NUM_STRESS_COMPONENTS;
		mComplianceMatrix = EigenMatrix(numStressDofs, numStressDofs);
		mComplianceMatrix.setZero();
		for (uint32 e = 0; e < GetNumElements(); e++)
		{
			real vol = mElementVolumes[e];
			for (uint32 i = 0; i < numLocalStressNodes; i++)
			{
				int globalI = mStressOrder == 0 ? e : mStressMesh->GetGlobalIndex(e, i);
				for (uint32 j = 0; j < numLocalStressNodes; j++)
				{
					int globalJ = mStressOrder == 0 ? e : mStressMesh->GetGlobalIndex(e, j);
					for (uint32 k = 0; k < NUM_STRESS_COMPONENTS; k++)
					{
						for (uint32 l = 0; l < NUM_STRESS_COMPONENTS; l++)
						{
							mComplianceMatrix(globalI * NUM_STRESS_COMPONENTS + k, globalJ * NUM_STRESS_COMPONENTS + l) += 
								vol * Clocal(i * NUM_STRESS_COMPONENTS + k, j * NUM_STRESS_COMPONENTS + l);
						}
					}
				}
			}
		}
	}

	void FemPhysicsLinearMixed::AssembleJacobianMatrix(EigenMatrix& J)
	{
		uint32 numNodes = GetNumFreeNodes();
		uint32 nDof = NUM_POS_COMPONENTS * numNodes; // position DOFs
		uint32 numLocalNodes = GetNumLocalNodes();
		uint32 nLocalDofs = numLocalNodes * NUM_POS_COMPONENTS;

		uint32 numStressLocalNodes = GetNumStressLocalNodes();
		uint32 numStressNodes = GetNumStressNodes();
		uint32 nConstr = NUM_STRESS_COMPONENTS * numStressNodes; // number of constraints (stress DOFs)

		J = EigenMatrix(nConstr, nDof); // the Jacobian matrix
		J.setZero();
		real factor = mStressOrder == 1 ? 0.25f : 1.f;
		for (uint32 e = 0; e < GetNumElements(); e++)
		{
			real vol = mElementVolumes[e];

			EigenMatrix Jlocal((uint32)NUM_STRESS_COMPONENTS, nLocalDofs);
			{
				Matrix3R Bs[10], Bn[10];
				if (mOrder == 1)
					ComputeStrainJacobian(e, Bn, Bs);
				else
					ComputeLocalJacobianBB2(e, Bn, Bs);
				// build Jacobian matrix
				for (uint32 k = 0; k < numLocalNodes; k++)
				{
					for (int l = 0; l < NUM_POS_COMPONENTS; l++)
					{
						for (int m = 0; m < NUM_POS_COMPONENTS; m++)
						{
							Jlocal(l + NUM_POS_COMPONENTS, m + k * NUM_POS_COMPONENTS) = Bs[k][l][m];
							Jlocal(l, m + k * NUM_POS_COMPONENTS) = Bn[k][l][m];
						}
					}
				}
			}

			Matrix3R BsLinear[4], BnLinear[4];
			ComputeStrainJacobian(e, BnLinear, BsLinear);

			// go through all local stress nodes
			for (uint32 j = 0; j < numStressLocalNodes; j++)
			{
				uint32 globalJ = mStressOrder == 0 ? e : mStressMesh->GetGlobalIndex(e, j);
				int baseRow = globalJ * NUM_STRESS_COMPONENTS;
				// go through all local position nodes
				for (uint32 k = 0; k < GetNumLocalNodes(); k++)
				{
					uint32 globalK = mReshuffleMap[mTetMesh->GetGlobalIndex(e, k)];
					if (globalK < mNumBCs)
						continue;
					int baseCol = (globalK - mNumBCs) * NUM_POS_COMPONENTS;
					if (mOrder == 2 && mStressOrder == 1)
					{
						Matrix3R Bs(0), Bn(0);
						MultiIndex C = mStressMesh->GetIJKL(j);
						Vector3R v;
						for (int n = 0; n < 4; n++)
						{
							MultiIndex A = mTetMesh->GetIJKL(k);
							if (A.Decrement(n))
							{
								real g = 0.1 * ComputeMultiIndexSumFactor(1, C, A);
								Bn = Bn + g * BnLinear[n];
								Bs = Bs + g * BsLinear[n];
							}
						}
						for (int l = 0; l < 3; l++)
						{
							for (int m = 0; m < NUM_POS_COMPONENTS; m++)
							{
								J(baseRow + l, baseCol + m) += vol * Bn(l, m);
								J(baseRow + 3 + l, baseCol + m) += vol * Bs(l, m);
							}
						}
					}
					else
					{
						for (int l = 0; l < NUM_STRESS_COMPONENTS; l++)
						{
							for (int m = 0; m < NUM_POS_COMPONENTS; m++)
							{
								J(baseRow + l, baseCol + m) += factor * vol * Jlocal(l, k * NUM_POS_COMPONENTS + m);
							}
						}
					}
				}
			}
		}
	}

} // namespace FEM_SYSTEM