#ifndef FEM_PHYSICS_LINEAR_MIXED_H
#define FEM_PHYSICS_LINEAR_MIXED_H

#include <Physics/FEM/FemPhysicsLinear.h>
#include <Physics/FEM/LinearTetrahedralMesh.h>

namespace FEM_SYSTEM
{
	class FemPhysicsLinearMixed : public FemPhysicsLinear
	{
	public:
		FemPhysicsLinearMixed(std::vector<Tet>& tetrahedra,
			std::vector<Node>& nodes, const FemConfig& config);
		void Step(real dt) override;
		void SolveEquilibrium(float);

		uint32 GetNumStressNodes() const { return mNumStressNodes; }
		uint32 GetNumStressLocalNodes() const { return mStressOrder == 1 ? 4 : 1; }

	private:
		// mixed LE
		void AssembleComplianceMatrix();
		void StepConstraint(real h);
		void AssembleJacobianMatrix(EigenMatrix& J);

		// BB2 position shape functions
		//void AssembleJacobianMatrixBB2(EigenMatrix& J);
		void ComputeLocalJacobianBB2(uint32 elem, Matrix3R Bn[10], Matrix3R Bs[10]);
		//void AssembleJacobianMatrix_LinearStressBB2(EigenMatrix& J);

	private:
		// mixed linear elasticity
		uint32 mStressOrder;
		uint32 mNumStressNodes;
		EigenMatrix mInverseMassMatrix; // inverse mass matrix (for Schur complement)
		EigenMatrix mComplianceMatrix; // global compliance matrix
		EigenMatrix mJacobianMatrix;
		EigenMatrix mStiffnessMatrix;
		EigenMatrix mSystemMatrix;
		Eigen::FullPivLU<EigenMatrix> mFullPivLU;
		Eigen::LLT<EigenMatrix> mLLT;

		std::unique_ptr<LinearTetrahedralMesh> mStressMesh; // the stress tet mesh
	};

} // namespace FEM_SYSTEM

#endif // !FEM_PHYSICS_LINEAR_MIXED_H
