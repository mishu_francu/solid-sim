#include "FemDataStructures.h"
#include "MixedProblems.h"
#include "FemDataStructures.h"
#include "ElasticEnergy.h"

namespace FEM_SYSTEM
{
	EigenVector PrimalProblem::ComputeRhs(const EigenVector& solution)
	{
		if (solution.rows() != 0)
		{
			// update deformed positions
			Vector3Array u = GetStdVector(solution);
			for (uint32 i = 0; i < mFPM.GetNumFreeNodes(); i++)
			{
				mFPM.mDeformedPositions[i] = u[i];
			}
		}

		// update the Jacobian, as no one else does it
		mFPM.AssembleJacobianMatrix(mFPM.mVolJacobianMatrix, false, true);

		EigenVector rhs = mFPM.ComputePosRhs();

#ifdef ALM
		// ALM part
		EigenVector errors(mFPM.GetNumPressureNodes());
		real totalErr = mFPM.GetCurrentVolumeErrors(errors);

		rhs += mFPM.mStepLength * mFPM.mVolJacobianMatrix.transpose() * errors;
#endif

		return rhs;
	}

	void PrimalProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K)
	{
		// the system matrix
		mFPM.ComputeStiffnessMatrix(mFPM.mSystemMatrix);

#ifdef ALM
		// ALM part
		mFPM.mSystemMatrix += mFPM.mStepLength * mFPM.mVolJacobianMatrix.transpose() * mFPM.mVolJacobianMatrix;

		// TODO: try to omit geometric stiffness matrix
		EigenMatrix G;
		EigenVector errors(mFPM.GetNumPressureNodes());
		real totalErr = mFPM.GetCurrentVolumeErrors(errors); // TODO: compute only once
		mFPM.AssembleGeometricStiffnessMatrix(mFPM.mStepLength * errors, G); // TODO: compute only once
		mFPM.mSystemMatrix += G;
#endif
	}

	void ThreeFieldProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K)
	{
		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numPNodes = mFPM.GetNumPressureNodes();
		uint32 numContacts = mFPM.GetNumContacts();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + 2 * numPNodes;

		real scale = mFPM.mConfig.mConstraintScale; // scaling of volume constraints

		// the system matrix
		K.resize(size, size);
		K.setZero();

		EigenMatrix id;
		id.setIdentity(numPNodes, numPNodes);

		EigenMatrix Kxx;
		mFPM.ComputeStiffnessMatrix(Kxx);
		K.block(0, 0, numDofs, numDofs) = Kxx;
		K.block(0, numDofs, numDofs, numPNodes) = scale * mFPM.mVolJacobianMatrix.transpose();
		K.block(numDofs, numDofs, numPNodes, numPNodes) = -scale * mFPM.mVolComplianceMatrix;
		K.block(numDofs, numDofs + numPNodes, numPNodes, numPNodes) = id;
		K.block(numDofs + numPNodes, 0, numPNodes, numDofs) = scale * mFPM.mVolJacobianMatrix;
		K.block(numDofs + numPNodes, numDofs + numPNodes, numPNodes, numPNodes) = -id;
	}

	EigenVector ThreeFieldProblem::ComputeRhs(const EigenVector& solution)
	{
		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numPNodes = mFPM.GetNumPressureNodes();
		uint32 numContacts = mFPM.GetNumContacts();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes * 2;

		//real scale = mFPM.mConfig.mConstraintScale; // scaling of volume constraints

		// update deformed positions
		Vector3Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < mFPM.GetNumFreeNodes(); i++)
		{
			mFPM.mDeformedPositions[i] = u[i];
		}

		// update pressure
		auto p = solution.segment(numDofs, numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			mFPM.mPressures[i] = p[i];
		}

		// vol stretches
		mFPM.mVolStretches = solution.segment(numDofs + numPNodes, numPNodes);

		if (!mFPM.mCondenseContact)
		{
			// update contact multipliers
			mFPM.mContactMultipliers = solution.tail(numContacts);
		}

		// assemble Jacobian matrix
		//mFPM.mJacobian.resize(numPNodes, numDofs);		
		//mFPM.mJacobian.block(0, 0, numPNodes, numDofs) = mFPM.mVolJacobianMatrix;
		//if (numContacts != 0 && !mFPM.mCondenseContact)
		//	mFPM.mJacobian.block(numPNodes, 0, numContacts, numDofs) = mFPM.mContactJacobian;

		// assemble right hand side
		EigenVector rhs(size);
		rhs.setZero();

		rhs.head(numDofs) = mFPM.ComputePosRhs();

		real factor = mFPM.mPressureOrder == 1 ? 0.25f : 1;
		EigenVector initialVolumes(numPNodes);
		initialVolumes.setZero();
		EigenVector deformedVolumes(numPNodes);
		deformedVolumes.setZero();
		for (uint32 e = 0; e < mFPM.GetNumElements(); e++)
		{
			const Vector3R& x0 = mFPM.GetDeformedPosition(mFPM.mTetMesh->GetGlobalIndex(e, 0));
			const Vector3R& x1 = mFPM.GetDeformedPosition(mFPM.mTetMesh->GetGlobalIndex(e, 1));
			const Vector3R& x2 = mFPM.GetDeformedPosition(mFPM.mTetMesh->GetGlobalIndex(e, 2));
			const Vector3R& x3 = mFPM.GetDeformedPosition(mFPM.mTetMesh->GetGlobalIndex(e, 3));
			Vector3R d1 = x1 - x0;
			Vector3R d2 = x2 - x0;
			Vector3R d3 = x3 - x0;
			Matrix3R mat(d1, d2, d3); // this is the spatial shape matrix Ds [Sifakis][Teran]
			real vol = (mat.Determinant()) / 6.f; // volume of the tet

			for (uint32 i = 0; i < mFPM.GetNumLocalPressureNodes(); i++)
			{
				uint32 globalI = mFPM.GetPressureGlobalIndex(e, i);
				initialVolumes[globalI] += factor * mFPM.mElementVolumes[e];
				deformedVolumes[globalI] += factor * vol;
			}
		}

		// presssure components
		rhs.segment(numDofs, numPNodes) = -mFPM.mVolStretches + initialVolumes + mFPM.mVolComplianceMatrix * p;

		// volume components
		rhs.segment(numDofs + numPNodes, numPNodes) = mFPM.mVolStretches - deformedVolumes;

		return rhs;
	}

	EigenVector BaseProblem::SolveLinearSystem(const EigenMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y)
	{
		LinearSolver solver;
		solver.Init(K, LST_LU_PARDISO);
		return solver.Solve(rhs);
	}

	EigenVector StandardProblem::ComputeRhs(const EigenVector& solution)
	{
		if (solution.rows() != 0)
		{
			// update deformed positions
			Vector3Array u = GetStdVector(solution);
			for (uint32 i = 0; i < mFPM.GetNumFreeNodes(); i++)
			{
				mFPM.mDeformedPositions[i] = u[i];
			}
		}

		EigenVector rhs = mFPM.ComputeStdRhs();

		return rhs;
	}

	void StandardProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K)
	{
		K = mFPM.ComputeStdStiffnessMatrix(); // FIXME
	}

	EigenVector CondensedProblem::ComputeRhs(const EigenVector& solution)
	{
		uint32 numPNodes = mFPM.GetNumPressureNodes();

		if (solution.rows() != 0)
		{
			// update deformed positions
			Vector3Array u = GetStdVector(solution);
			for (uint32 i = 0; i < mFPM.GetNumFreeNodes(); i++)
			{
				mFPM.mDeformedPositions[i] = u[i];
			}
		}

		EigenVector rhs = mFPM.ComputeStdRhs();

		EigenVector errors(numPNodes);
		mFPM.GetCurrentVolumeErrors(errors);

		auto p = mCinv * errors;

		mFPM.AssembleJacobianMatrix(mFPM.mVolJacobianMatrix, false, true);
		rhs -= mFPM.mVolJacobianMatrix.transpose() * p;

		return rhs;
	}

	void CondensedProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K)
	{
		uint32 numPNodes = mFPM.GetNumPressureNodes();

		// the system matrix
		real scale = mFPM.mConfig.mConstraintScale;
		real invHSqr = 1.f / (mFPM.mTimeStep * mFPM.mTimeStep);

		EigenVector errors(numPNodes);
		mFPM.GetCurrentVolumeErrors(errors);
		auto p = mCinv * errors;

		mFPM.AssembleGeometricStiffnessMatrix(p, mFPM.mGeometricStiffnessMatrix);
		// contacts and Dirichlet BCs have no geometric stiffness because they are linear constraints, i.e. the Hessian vanishes
		ElasticEnergy::AssembleStiffnessMatrix(&mFPM, mFPM.mDeviatoricStiffnessMatrix);

		K = mFPM.mDeviatoricStiffnessMatrix + scale * mFPM.mGeometricStiffnessMatrix
			+ mFPM.mVolJacobianMatrix.transpose() * mCinv * mFPM.mVolJacobianMatrix;
		if (mFPM.mSimType == ST_IMPLICIT)
			K += invHSqr * mFPM.mMassMatrix;
		//if (!mCollIndices.empty() && mCondenseContact)
		//{
		//	K += mContactStiffness * mContactJacobian.transpose() * mContactJacobian;
		//}
		//if (!mDirichletIndices.empty())
		//{
		//	K += dirichletStiffness * dirichletJacobian.transpose() * dirichletJacobian;
		//}
	}

	EigenVector PrincipalStretchesProblem::ComputeRhs(const EigenVector& solution)
	{
		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numElems = mFPM.GetNumElements();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 numPS = numElems * 3;
		uint32 size = numDofs + numPS * 2;

		// update deformed positions
		Vector3Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < mFPM.GetNumFreeNodes(); i++)
		{
			mFPM.mDeformedPositions[i] = u[i];
		}
		mPS = solution.segment(numDofs, numPS);
		mMultipliers = solution.tail(numPS);

		EigenVector rhs(size);
		// start with forcing terms
		rhs.head(numDofs) = mFPM.mForceFraction * GetEigenVector(mFPM.mBodyForces);
		mFPM.ComputeTractionForces();
		if (mFPM.mTractionForces.size() > 0)
			rhs.head(numDofs) += mFPM.mForceFraction * GetEigenVector(mFPM.mTractionForces);

		// add penalty Dirichlet BC forces
		if (!mFPM.mDirichletIndices.empty())
		{
			// count the constraints
			int count = 0;
			for (size_t i = 0; i < mFPM.mDirichletIndices.size(); i++)
			{
				if (mFPM.mDirichletAxes[i] & FemPhysicsBase::AXIS_X) count++;
				if (mFPM.mDirichletAxes[i] & FemPhysicsBase::AXIS_Y) count++;
				if (mFPM.mDirichletAxes[i] & FemPhysicsBase::AXIS_Z) count++;
			}

			EigenVector dirichletErrors(count);
			count = 0;
			for (uint32 i = 0; i < mFPM.mDirichletIndices.size(); i++)
			{
				// the i'th BC				
				uint32 flags = mFPM.mDirichletAxes[i];
				uint32 idx0 = mFPM.mDirichletIndices[i];
				uint32 idx = idx0 - mFPM.mNumBCs; // affecting node idx
				// compute the BC error
				if (flags & FemPhysicsBase::AXIS_X)
				{
					dirichletErrors(count++) = mFPM.mDeformedPositions[idx].x - mFPM.mReferencePositions[idx0].x;
				}
				if (flags & FemPhysicsBase::AXIS_Y)
				{
					dirichletErrors(count++) = mFPM.mDeformedPositions[idx].y - mFPM.mReferencePositions[idx0].y;
				}
				if (flags & FemPhysicsBase::AXIS_Z)
				{
					dirichletErrors(count++) = mFPM.mDeformedPositions[idx].z - mFPM.mReferencePositions[idx0].z;
				}
			}
			//Printf("Dirichlet BCs error: %g\n", dirichletErrors.norm());
			rhs.head(numDofs) -= mFPM.mDirichletStiffness * mFPM.mDirichletJacobian.transpose() * dirichletErrors;
			// TODO: we can build the rhs term directly without the Jacobian
		}

		// initialize the middle term
		rhs.segment(numDofs, numPS) = mMultipliers;

		// got through all the tets and compute principal stretches and their Jacobian
		Matrix3R F;
		mPSJacobian.resize(numDofs, numPS);
		mPSJacobian.setZero();
		for (uint32 i = 0; i < numElems; i++)
		{
			mFPM.ComputeDeformationGradient(i, F);
			EigenMatrix3 Aeig = Matrix3ToEigen<EigenMatrix3>(F);
			Eigen::JacobiSVD<EigenMatrix3> svd(Aeig, Eigen::ComputeFullU | Eigen::ComputeFullV);
			Eigen::Vector3d s = svd.singularValues();
			ASSERT(s(0) > 0 && s(1) > 0 && s(2) > 0);

			// hack:
			//mPS(i * 3) = s(0);
			//mPS(i * 3 + 1) = s(1);
			//mPS(i * 3 + 2) = s(2);

			Eigen::Vector3d ps = mPS.segment(i * 3, 3);
			rhs(numDofs + numPS + i * 3) = ps(0) - s(0);
			rhs(numDofs + numPS + i * 3 + 1) = ps(1) - s(1);
			rhs(numDofs + numPS + i * 3 + 2) = ps(2) - s(2);
			// now for the gradient
			auto U = svd.matrixU();
			auto V = svd.matrixV();
			// TODO: correct the sign
			for (uint32 j = 0; j < 4; j++) // num nodes
			{
				uint32 g = mFPM.GetGlobalIndex(i, j);
				if (g < mFPM.mNumBCs)
					continue;
				g -= mFPM.mNumBCs;
				const Vector3R& y = mFPM.mBarycentricJacobians[i].y[j];
				Eigen::Vector3d r(y.x, y.y, y.z);
				EigenMatrix3 R;
				R.setZero();
				R(0, 0) = (-r.transpose() * V.col(0));
				R(1, 1) = (-r.transpose() * V.col(1));
				R(2, 2) = (-r.transpose() * V.col(2));
				//auto D = R * U;
				//mPSJacobian.block(g * 3, i * 3, 3, 3) = D;
				mPSJacobian.block(g * 3, i * 3, 3, 1) = R(0, 0) * U.col(0);
				mPSJacobian.block(g * 3, i * 3 + 1, 3, 1) = R(1, 1) * U.col(1);
				mPSJacobian.block(g * 3, i * 3 + 2, 3, 1) = R(2, 2) * U.col(2);
				//EigenMatrix3 D;
				//D.block(0, 0, 3, 1) = R(0, 0) * U.col(0);
				//D.block(0, 1, 3, 1) = R(1, 1) * U.col(1);
				//D.block(0, 2, 3, 1) = R(2, 2) * U.col(2);
				//mPSJacobian.block(g * 3, i * 3, 3, 3) = D.transpose();
			}
			// gradient of energy - TODO: move to ElasticEnergy
			real J = ps(0) * ps(1) * ps(2);
			real psi0 = mFPM.GetShearModulus() * (ps(0) - 1.0 / ps(0)) + mFPM.GetLameFirstParam() * (J - 1) * ps(1) * ps(2);
			real psi1 = mFPM.GetShearModulus() * (ps(1) - 1.0 / ps(1)) + mFPM.GetLameFirstParam() * (J - 1) * ps(0) * ps(2);
			real psi2 = mFPM.GetShearModulus() * (ps(2) - 1.0 / ps(2)) + mFPM.GetLameFirstParam() * (J - 1) * ps(0) * ps(1);
			rhs(numDofs + i * 3) += mFPM.GetElementInitialVolume(i) * psi0;
			rhs(numDofs + i * 3 + 1) += mFPM.GetElementInitialVolume(i) * psi1;
			rhs(numDofs + i * 3 + 2) += mFPM.GetElementInitialVolume(i) * psi2;
		}
		std::cout << "J" << std::endl << mPSJacobian << std::endl;

		// finite diff approx of the Jacobian
		auto constr0 = rhs.tail(numPS);
		real eps = 1e-5;
		for (uint32 j = 0; j < numDofs; j++)
		{
			mFPM.mDeformedPositions[j / 3][j % 3] += eps;
			auto constr1 = ComputeConstraint();
			mPSJacobian.block(j, 0, 1, numPS) = (constr1 - constr0).transpose() / eps;
			mFPM.mDeformedPositions[j / 3][j % 3] -= eps;
		}
		std::cout << "Jfd" << std::endl << mPSJacobian << std::endl;

		//mMultipliers = -rhs.segment(numDofs, numPS); // hack
		EigenVector counter = mPSJacobian * mMultipliers;
		rhs.head(numDofs) = -rhs.head(numDofs) + counter;

		return -rhs;
	}

	void PrincipalStretchesProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K)
	{
		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numElems = mFPM.GetNumElements();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 numPS = numElems * 3;
		uint32 size = numDofs + numPS * 2;

		// compute the pos Hessian using finite differences
		EigenMatrix Hx(numDofs, numDofs);
		Hx.setZero();
		real eps = 1e-6;
		auto f0 = mPSJacobian * mMultipliers;
		EigenMatrix J;
		for (uint32 j = 0; j < numDofs; j++)
		{
			mFPM.mDeformedPositions[j / 3][j % 3] += eps;
			ComputePSJacobian(J);
			auto f1 = J * mMultipliers;
			Hx.block(0, j, numDofs, 1) = (f1 - f0) / eps;
			mFPM.mDeformedPositions[j / 3][j % 3] -= eps;
		}
		Hx += mFPM.mDirichletStiffness * mFPM.mDirichletJacobian.transpose() * mFPM.mDirichletJacobian;

		// compute the PS Hessian
		EigenMatrix Hs(numPS, numPS);
		Hs.setZero();
		real mu = mFPM.GetShearModulus();
		real lambda = mFPM.GetLameFirstParam();
		for (uint32 i = 0; i < numElems; i++)
		{
			Eigen::Vector3d s = mPS.segment(i * 3, 3);
			real J = s(0) * s(1) * s(2);
			uint32 b = i * 3;
			// TODO: move to ElasticEnergy
			Hs(b, b) = mu * (1 + 1 / s(0) / s(0)) + lambda * s(1) * s(1) * s(2) * s(2);
			Hs(b + 1, b + 1) = mu * (1 + 1 / s(1) / s(1)) + lambda * s(0) * s(0) * s(2) * s(2);
			Hs(b + 2, b + 2) = mu * (1 + 1 / s(2) / s(2)) + lambda * s(1) * s(1) * s(0) * s(0);
			Hs(b, b + 1) = Hs(b + 1, b) = lambda * s(2) * (2 * J - 1);
			Hs(b, b + 2) = Hs(b + 2, 0) = lambda * s(1) * (2 * J - 1);
			Hs(b + 1, b + 2) = Hs(b + 2, b + 1) = lambda * s(0) * (2 * J - 1);
		}

		// Hx  | 0  | J
		// 0   | Hs | I
		// J^T | I  | 0
		K.resize(size, size);
		K.setZero();
		K.block(numDofs + numPS, 0, numPS, numDofs) = mPSJacobian.transpose();
		K.block(0, numDofs + numPS, numDofs, numPS) = mPSJacobian;
		K.block(numDofs + numPS, numDofs, numPS, numPS).setIdentity();
		K.block(numDofs, numDofs + numPS, numPS, numPS).setIdentity();
		K.block(0, 0, numDofs, numDofs) = Hx;
		K.block(numDofs, numDofs, numPS, numPS) = Hs;
	}

	void PrincipalStretchesProblem::ComputePSJacobian(EigenMatrix& J)
	{
		uint32 numNodes = mFPM.GetNumFreeNodes();
		uint32 numElems = mFPM.GetNumElements();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 numPS = numElems * 3;
		uint32 size = numDofs + numPS * 2;

		Matrix3R F;
		J.resize(numDofs, numPS);
		J.setZero();
		for (uint32 i = 0; i < numElems; i++)
		{
			mFPM.ComputeDeformationGradient(i, F);
			EigenMatrix3 Aeig = Matrix3ToEigen<EigenMatrix3>(F);
			Eigen::JacobiSVD<EigenMatrix3> svd(Aeig, Eigen::ComputeFullU | Eigen::ComputeFullV);
			//Eigen::Vector3d s = svd.singularValues();
			//ASSERT(s(0) > 0 && s(1) > 0 && s(2) > 0);
			auto U = svd.matrixU();
			auto V = svd.matrixV();
			// TODO: correct the sign
			for (uint32 j = 0; j < 4; j++) // num nodes
			{
				uint32 g = mFPM.GetGlobalIndex(i, j);
				if (g < mFPM.mNumBCs)
					continue;
				const Vector3R& y = mFPM.mBarycentricJacobians[i].y[j];
				Eigen::Vector3d r(y.x, y.y, y.z);
				EigenMatrix3 R;
				R.setZero();
				R(0, 0) = (-r.transpose() * V.col(0));
				R(1, 1) = (-r.transpose() * V.col(1));
				R(2, 2) = (-r.transpose() * V.col(2));
				auto D = R * U;
				g -= mFPM.mNumBCs;
				J.block(g * 3, i * 3, 3, 3) = D;
			}
		}
	}

	EigenVector PrincipalStretchesProblem::ComputeConstraint()
	{
		uint32 numElems = mFPM.GetNumElements();
		uint32 numPS = numElems * 3;

		EigenVector constr(numPS);
		Matrix3R F;
		for (uint32 i = 0; i < numElems; i++)
		{
			mFPM.ComputeDeformationGradient(i, F);
			EigenMatrix3 Aeig = Matrix3ToEigen<EigenMatrix3>(F);
			Eigen::JacobiSVD<EigenMatrix3> svd(Aeig, Eigen::ComputeFullU | Eigen::ComputeFullV);
			Eigen::Vector3d s = svd.singularValues();
			ASSERT(s(0) > 0 && s(1) > 0 && s(2) > 0);
			Eigen::Vector3d ps = mPS.segment(i * 3, 3);
			constr(i * 3) = ps(0) - s(0);
			constr(i * 3 + 1) = ps(1) - s(1);
			constr(i * 3 + 2) = ps(2) - s(2);
		}
		return constr;
	}

} // namespace FEM_SYSTEM