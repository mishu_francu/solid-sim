#include "FemDataStructures.h"
#include "FemCollisions.h"
#include "FemPhysicsBase.h"
#include <Geometry/Collision3D.h>
#include <Geometry/Assets.h>

#ifdef BULLET_COLLISIONS
#	include <Physics/Unified/BulletWrapper.h>
#endif

using namespace Physics;

namespace FEM_SYSTEM
{
	void FemCollision::HandleCollisions(FemPhysicsBase* phys)
	{
		mCollIndices.clear();
		mCollNormals.clear();
		mCollPointsB.clear();

		if (mCollFlags.size() < phys->GetNumNodes())
		{
			mCollFlags.resize(phys->GetNumNodes());
		}
		std::fill(mCollFlags.begin(), mCollFlags.end(), false);

#ifndef MESH_COLLISIONS
		for (size_t i = 0; i < mCollidables.size(); i++)
		{
			const Physics::Collidable* collidable = mCollidables[i].get();

			uint32 numBCs = phys->GetNumNodes() - phys->GetNumFreeNodes();

			for (uint32 i = numBCs; i < phys->GetNumNodes(); i++)
			{
				const Vector3R& pos = phys->GetDeformedPosition(i);

				// floor collisions
				if (collidable->mType == Physics::CT_WALLS)
				{
					const Physics::Walls* walls = static_cast<const Physics::Walls*>(collidable);
					if (pos.x < walls->mBox.max.x + walls->center.x && pos.x > walls->mBox.min.x + walls->center.x &&
						pos.y < walls->mBox.max.y + walls->center.y && pos.y > walls->mBox.min.y + walls->center.y &&
						pos.z < walls->mBox.max.z + walls->center.z && pos.z > walls->mBox.min.z + walls->center.z)
					{
						Vector3R center = FloatToDouble(walls->mBox.GetCenter() + walls->center);
						Vector3R extents = FloatToDouble(walls->mBox.GetExtent());
						Vector3R delta = pos - center;
						Vector3R absDelta = delta.GetAbs();
						Vector3R penetration = extents - absDelta;
						int axis = penetration.GetMinComponent(); // the minimum penetration axis
						Vector3R normal;
						normal[axis] = 1;
						Vector3R point = pos;

						mCollIndices.push_back(i);
						if (delta[axis] > 0)
						{
							mCollNormals.push_back(normal);
							point[axis] = walls->mBox.max[axis] + walls->center[axis];
							mCollPointsB.push_back(point);
						}
						else
						{
							normal.Flip();
							mCollNormals.push_back(normal);
							point[axis] = walls->mBox.min[axis] + walls->center[axis];
							mCollPointsB.push_back(point);
						}
					}
				}

#ifdef SPHERE_COLLISIONS
				// sphere collisions
				real sphDist = (pos - sphPos).Length();
				if (sphDist <= sphRad)
				{
					Vector3R normal = (1.f / sphDist) * (pos - sphPos);
					Vector3R newPos = sphPos + normal * sphRad;
					//pos = newPos; // projection
					vel += -normal * dot(normal, vel) + normal * erp * (sphRad - sphDist); // optional

					mCollIndices.push_back(i);
					mCollNormals.push_back(normal);
					mCollPointsB.push_back(newPos);
				}
#endif

				//vel = (1.f / dt) * (pos - pos0);
			}
		}
#else // !MESH_COLLISIONS
		DetectMeshCollision(mCollisionMesh);
#endif
	}

#ifdef BULLET_COLLISIONS
	void FemPhysicsBase::InitBulletCollision() {
		m_collisionConfiguration = new btSoftBodyRigidBodyCollisionConfiguration();

		///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
		m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);

		m_broadphase = new btDbvtBroadphase();
		btDeformableBodySolver* deformableBodySolver = new btDeformableBodySolver();

		///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
		btDeformableMultiBodyConstraintSolver* sol = new btDeformableMultiBodyConstraintSolver();
		sol->setDeformableSolver(deformableBodySolver);
		m_solver = sol;

		m_dynamicsWorld = new btDeformableMultiBodyDynamicsWorld(m_dispatcher, m_broadphase, sol, m_collisionConfiguration, deformableBodySolver);
		//    deformableBodySolver->setWorld(getDeformableDynamicsWorld());
		//    m_dynamicsWorld->getSolverInfo().m_singleAxisDeformableThreshold = 0.f;//faster but lower quality
		btVector3 gravity = btVector3(0, /*-0.098*//*-10*//*-0.98*/0, 0);
		m_dynamicsWorld->setGravity(gravity);
		getDeformableDynamicsWorld()->getWorldInfo().m_gravity = gravity;
		//getDeformableDynamicsWorld()->getWorldInfo().m_sparsesdf.setDefaultVoxelsz(0.25);
		//getDeformableDynamicsWorld()->getWorldInfo().m_sparsesdf.Reset();

		{
			/*char absolute_path[1024];
			b3BulletDefaultFileIO fileio;
			fileio.findResourcePath("../Models/spine.vtk", absolute_path, 1024);
			btSoftBody* psb = btSoftBodyHelpers::CreateFromVtkFile(getDeformableDynamicsWorld()->getWorldInfo(), absolute_path);*/

			btAlignedObjectArray<btVector3> X;

			for (int i = 0; i < GetNumNodes(); i++) {
				Vector3R pos = GetDeformedPosition(i);
				X.push_back(btVector3(pos.x, pos.y, pos.z));
			}

			typedef btAlignedObjectArray<int> Index;
			btSoftBody* psb = new btSoftBody(&getDeformableDynamicsWorld()->getWorldInfo(), X.size(), &X[0], 0);

			TetrahedralMesh<uint32>* tetMesh = GetTetMesh();
			if (tetMesh != NULL) {
				for (int i = 0; i < GetNumElements(); ++i)
				{
					int ni0 = GetTetMesh()->GetGlobalIndex(i, 0);
					int ni1 = GetTetMesh()->GetGlobalIndex(i, 1);
					int ni2 = GetTetMesh()->GetGlobalIndex(i, 2);
					int ni3 = GetTetMesh()->GetGlobalIndex(i, 3);

					psb->appendTetra(ni0, ni1, ni2, ni3);
					{
						psb->appendLink(ni0, ni1, 0, true);
						psb->appendLink(ni1, ni2, 0, true);
						psb->appendLink(ni2, ni0, 0, true);
						psb->appendLink(ni0, ni3, 0, true);
						psb->appendLink(ni1, ni3, 0, true);
						psb->appendLink(ni2, ni3, 0, true);
					}
				}
			}
			else {
				for (int i = 0; i < GetNumElements(); ++i)
				{
					int ni0 = GetGlobalIndex(i, 0);
					int ni1 = GetGlobalIndex(i, 1);
					int ni2 = GetGlobalIndex(i, 2);
					int ni3 = GetGlobalIndex(i, 3);

					psb->appendTetra(ni0, ni1, ni2, ni3);
					{
						psb->appendLink(ni0, ni1, 0, true);
						psb->appendLink(ni1, ni2, 0, true);
						psb->appendLink(ni2, ni0, 0, true);
						psb->appendLink(ni0, ni3, 0, true);
						psb->appendLink(ni1, ni3, 0, true);
						psb->appendLink(ni2, ni3, 0, true);
					}
				}
			}

			btSoftBodyHelpers::generateBoundaryFaces(psb);
			psb->initializeDmInverse();
			psb->m_tetraScratches.resize(psb->m_tetras.size());
			psb->m_tetraScratchesTn.resize(psb->m_tetras.size());

			/*btSoftBody* psb = btSoftBodyHelpers::CreateFromTetGenData(getDeformableDynamicsWorld()->getWorldInfo(),
				TetraCube::getElements(),  //the elements are integer values (indices probably)
				0,
				TetraCube::getNodes(), //the nodes are floating points values, it's char* because it reads in a file, each line (except the first) is a 4 element value.
				false,
				true,
				true);*/
				//Alternatively: CreateFromVtkFile

				//psb->scale(btVector3(0.001, 0.001, 0.001));
				//psb->translate(btVector3(0, 0, 0.75)); //positioned right above the ground.
				//psb->scale(btVector3(1000, 1000, 1000));
			psb->getCollisionShape()->setMargin(/*0.000001*/ 1);
			psb->setTotalMass(1);
			psb->m_cfg.kKHR = 1; // collision hardness with kinematic objects
			psb->m_cfg.kCHR = 1; // collision hardness with rigid body
			psb->m_cfg.kDF = 0.2;
			//psb->m_cfg.kSHR = 1;
			psb->setMaxStress(50);
			psb->rotate(btQuaternion(0, -SIMD_PI / 2, 0));
			psb->m_cfg.collisions = btSoftBody::fCollision::SDF_RD;  //SDF_RDF
			psb->m_cfg.collisions |= btSoftBody::fCollision::VF_DD;
			//psb->m_cfg.collisions |= btSoftBody::fCollision::CL_SS;
			//btSoftBodyHelpers::generateBoundaryFaces(psb);
			//psb->setSelfCollision(true);

			getDeformableDynamicsWorld()->addSoftBody(psb);
		}

		{
			btTriangleIndexVertexArray* indexVertexArrays = new btTriangleIndexVertexArray();
			btIndexedMesh part;
			part.m_vertexBase = (const unsigned char*)(&mCollisionMesh->vertices[0]);
			part.m_vertexStride = sizeof(btScalar) * 3;
			part.m_numVertices = mCollisionMesh->vertices.size();
			part.m_triangleIndexBase = (const unsigned char*)(&mCollisionMesh->indices[0]);
			part.m_triangleIndexStride = sizeof(int) * 3;
			part.m_numTriangles = mCollisionMesh->indices.size() / 3;
			part.m_indexType = PHY_INTEGER;
			indexVertexArrays->addIndexedMesh(part, PHY_INTEGER);

			bool useQuantizedAabbCompression = true;

			btBvhTriangleMeshShape* groundShape = new btBvhTriangleMeshShape(indexVertexArrays, useQuantizedAabbCompression);
			groundShape->setLocalScaling(btVector3(10, 10, 10));
			//groundShape->setMargin(0.1);
			btVector3 localInertia(0, 0, 0);
			btTransform groundTransform;
			groundTransform.setIdentity();
			groundTransform.setOrigin(btVector3(0, 0, 0));
			groundTransform.setRotation(btQuaternion(btVector3(1, 0, 0), SIMD_PI * 0.));

			btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
			btRigidBody::btRigidBodyConstructionInfo rbInfo(1, 0, groundShape, localInertia);
			btRigidBody* body = new btRigidBody(rbInfo);

			getDeformableDynamicsWorld()->addRigidBody(body);
		}
		/*{
			btSoftBody* psb = btSoftBodyHelpers::CreateFromTriMesh(getDeformableDynamicsWorld()->getWorldInfo(), (const btScalar*)(&mCollisionMesh->vertices[0]), (const int*)(&mCollisionMesh->indices[0]), mCollisionMesh->indices.size() / 3, false);
			psb->scale(btVector3(10, 10, 10));
			//psb->rotate(btQuaternion(0, -SIMD_PI / 2, 0));
			//psb->translate(btVector3(0, -60, 0));
			psb->getCollisionShape()->setMargin(0.1);
			psb->setTotalMass(1);
			psb->m_cfg.kKHR = 1; // collision hardness with kinematic objects
			psb->m_cfg.kCHR = 1; // collision hardness with rigid body
			psb->m_cfg.kDF = 0.2;
			//psb->m_cfg.kSHR = 1;
			psb->setMaxStress(50);
			psb->m_cfg.collisions = btSoftBody::fCollision::SDF_RD;
			psb->m_cfg.collisions |= btSoftBody::fCollision::VF_DD;
			getDeformableDynamicsWorld()->addSoftBody(psb);
		}*/

		getDeformableDynamicsWorld()->setImplicit(false);
		getDeformableDynamicsWorld()->setLineSearch(false);
	}
#endif

	void FemCollision::DetectMeshCollision(const Geometry::Mesh* mesh)
	{
#ifdef BULLET_COLLISIONS
		//Do collision detection here.
		//First we update the bullet mesh, then do collision detection and output the contacts and contact normals.

		btSoftBodyArray psbarray = getDeformableDynamicsWorld()->getSoftBodyArray();
		btSoftBody* psb = psbarray.at(0);

		for (int i = 0; i < psb->m_nodes.size(); i++) {
			psb->m_nodes[i].m_q = psb->m_nodes[i].m_x;
		}

		for (int i = 0; i < psb->m_nodes.size(); i++) {
			psb->m_nodes[i].m_x = btVector3(GetDeformedPosition(i).x, GetDeformedPosition(i).y, GetDeformedPosition(i).z);
		}

		psb->scale(btVector3(1000, 1000, 1000));
		getDeformableDynamicsWorld()->reinitialize(0);

		///apply gravity and explicit force to velocity, predict motion
		getDeformableDynamicsWorld()->predictUnconstraintMotion(0);
		//psb->predictMotion(dt);

		///perform collision detection
		getDeformableDynamicsWorld()->performDiscreteCollisionDetection();
		getDeformableDynamicsWorld()->softBodySelfCollision();
		getDeformableDynamicsWorld()->setupConstraints();

		/*psb->scale(btVector3(0.001, 0.001, 0.001));

		for (int i = 0; i < psb->m_nodes.size(); i++) {
			femSys->GetNode(i).pos = FEM_SYSTEM::Vector3R(psb->m_nodes[i].m_x.x(), psb->m_nodes[i].m_x.y(), psb->m_nodes[i].m_x.z());
		}*/

		ClearCollisions();
		if (psb->m_nodeRigidContacts.size() > 0) {
			for (size_t i = 0; i < psb->m_nodeRigidContacts.size(); i++)
			{
				Vector3 normal = BV2V(psb->m_nodeRigidContacts[i].m_cti.m_normal);
				//normal = Vector3(normal.z, normal.y, normal.x);
				int idx = psb->m_nodeRigidContacts[i].m_node->index;
				/*btVector3 v1 = psb->m_nodeRigidContacts[i].m_face->m_n[0]->m_x;
				btVector3 v2 = psb->m_nodeRigidContacts[i].m_face->m_n[1]->m_x;
				btVector3 v3 = psb->m_nodeRigidContacts[i].m_face->m_n[2]->m_x;
				float w1 = psb->m_nodeRigidContacts[i].m_bary[0];
				float w2 = psb->m_nodeRigidContacts[i].m_bary[1];
				float w3 = psb->m_nodeRigidContacts[i].m_bary[2];
				Vector3 pt = 0.001 * BV2V(w1 * v1 + w2 * v2 + w3 * v3);*/

				//scale down here if the original mesh was scaled down (and only scale it as much as it was scaled down) - fx. scaling should not be applied on the cantilever.
				//When drawing the contacts in FEMDemo.cpp, scale up again if the scaling down was done here, and down by 0.1 if it was not.
				Vector3 pt = 0.001 * BV2V(psb->m_nodeRigidContacts[i].m_node->m_x + psb->m_nodeRigidContacts[i].m_cti.m_normal * (psb->m_nodeRigidContacts[i].m_cti.m_offset)); //offset is the penetration distance - margin.
				//Vector3 pt = BV2V(psb->m_nodeRigidContacts[i].m_node->m_x);
				AddCollisionPointNodeRigid(idx, pt, normal);
			}
		}

		/*if (psb->m_faceRigidContacts.size() > 0) {
			for (size_t i = 0; i < psb->m_faceRigidContacts.size(); i++)
			{
				Vector3 normal = BV2V(psb->m_faceRigidContacts[i].m_face->m_normal);
				//normal = Vector3(normal.z, normal.y, normal.x);
				//int idx = psb->m_faceRigidContacts[i].m_face->m_index;
				btSoftBody::Node* n0 = psb->m_faceRigidContacts[i].m_face->m_n[0];  //m_face is the softbody face.
				btSoftBody::Node* n1 = psb->m_faceRigidContacts[i].m_face->m_n[1];
				btSoftBody::Node* n2 = psb->m_faceRigidContacts[i].m_face->m_n[2];
				Math::Vector3T<int> nodeTriple(n0->index, n1->index, n2->index);

				float bary0 = psb->m_faceRigidContacts[i].m_bary[0];
				float bary1 = psb->m_faceRigidContacts[i].m_bary[1];
				float bary2 = psb->m_faceRigidContacts[i].m_bary[2];
				Vector3 bary = Vector3(bary0, bary1, bary2);
				//Vector3 pt = BV2V(bary0 * n0->m_x + bary1 * n1->m_x + bary2 * n2->m_x);  //equivalent to m_contactPoint it seems, bary is actually computed from the contact point and the three node positions.
				//Vector3 pt2 = BV2V(psb->m_faceRigidContacts[i].m_contactPoint);

				Vector3 rigid_contact_point = BV2V(psb->m_faceRigidContacts[i].m_rigid_contact_point);

				AddCollisionPointFaceRigid(rigid_contact_point, nodeTriple, bary, normal);
			}
		}*/

		/*if (psb->m_faceNodeContacts.size() > 0) {
			// @Mihail: contacts can be retrieved here!
			for (size_t i = 0; i < psb->m_faceNodeContacts.size(); i++)
			{
				Vector3 normal = BV2V(psb->m_faceNodeContacts[i].m_normal);
				//normal = Vector3(normal.z, normal.y, normal.x);
				int idx = psb->m_faceNodeContacts[i].m_node->index;
				btVector3 v1 = psb->m_faceNodeContacts[i].m_face->m_n[0]->m_x;
				btVector3 v2 = psb->m_faceNodeContacts[i].m_face->m_n[1]->m_x;
				btVector3 v3 = psb->m_faceNodeContacts[i].m_face->m_n[2]->m_x;
				float w1 = psb->m_faceNodeContacts[i].m_bary[0];
				float w2 = psb->m_faceNodeContacts[i].m_bary[1];
				float w3 = psb->m_faceNodeContacts[i].m_bary[2];
				Vector3 pt = 0.001 * BV2V(w1 * v1 + w2 * v2 + w3 * v3);
				AddCollisionPoint(idx, pt, normal);
			}
		}*/

#endif
	}

	/*void FemPhysicsBase::DetectMeshCollision(const Physics::Mesh* mesh)
	{
		// go through all nodes and test against all mesh triangles
		uint32 numBCs = GetNumNodes() - GetNumFreeNodes();
		for (uint32 i = numBCs; i < GetNumNodes(); i++)
		{
			// go through all mesh triangles
			for (uint32 j = 0; j < mesh->GetNumTriangles(); j++)
			{
				NodeVsTriangle(mesh, i, j * 3);
			}
		}
	}*/

	//void FemPhysicsBase::NodeVsTriangle(const Physics::Mesh* mesh, int vertexIndex, int triangleIndex)
	//{
	//	Vector3 v = DoubleToFloat(GetDeformedPosition(vertexIndex)); // use previous?
	//	const uint16 i1 = mesh->indices[triangleIndex];
	//	const uint16 i2 = mesh->indices[triangleIndex + 1];
	//	const uint16 i3 = mesh->indices[triangleIndex + 2];
	//	const Vector3& v1 = 0.01f * mesh->vertices[i1];
	//	const Vector3& v2 = 0.01f * mesh->vertices[i2];
	//	const Vector3& v3 = 0.01f * mesh->vertices[i3];
	//	//const Vector3& w = mParticles[vertexIndex].pos;

	//	//float tol = (w - v).Length();
	//	float radTol = (float)(mThickness * 1.5f);
	//	const Vector3 extrude(radTol);

	//	//Vector3 minV = vmin(vmin(v1, v2), v3) - extrude;
	//	//Vector3 maxV = vmax(vmax(v1, v2), v3) + extrude;
	//	//if (!PointInAabb3D(minV, maxV, v))
	//	//	return;

	//	Vector3 n1 = (v2 - v1).Cross(v3 - v1);
	//	n1.Normalize();
	//	// if coming from inside skip
	//	BarycentricCoords coords;
	//	Vector3 n, p;
	//	float d;
	//	bool intersect = false;
	//	//if (n1.Dot(v - v1) > 0)
	//		intersect = IntersectSphereTriangle(v, radTol, v1, v2, v3, n, p, d, coords);

	//	if (intersect)
	//	//#pragma omp critical
	//	{
	//		if (mCollFlags[vertexIndex] == false)
	//		{
	//			mCollIndices.push_back(vertexIndex);
	//			mCollNormals.push_back(FloatToDouble(n));
	//			mCollPointsB.push_back(FloatToDouble(p));
	//			mCollFlags[vertexIndex] = true;

	//			//Vector3R& pos = GetDeformedPosition(vertexIndex);
	//			//pos = FloatToDouble(p);
	//			//Vector3R& vel = GetVelocity(vertexIndex);
	//			//vel.SetZero();
	//		}
	//	}
	//}

}