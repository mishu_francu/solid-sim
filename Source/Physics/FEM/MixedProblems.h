#pragma once

#include "FemPhysicsMixed.h"

namespace FEM_SYSTEM
{
	class BaseProblem
	{
	public:
		BaseProblem(FemPhysicsMixed& source) : mFPM(source) {}
		EigenVector SolveLinearSystem(const EigenMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y);
		real MeritResidual(const EigenVector& rhs) const { return rhs.norm(); }
		bool CheckForInversion() const { return mFPM.CheckForInversion(); }

	protected:
		FemPhysicsMixed& mFPM;
	};

	class StandardProblem : public BaseProblem
	{
	public:
		StandardProblem(FemPhysicsMixed& source) : BaseProblem(source) {}
		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);
	};

	class ThreeFieldProblem : public BaseProblem
	{
	public:
		ThreeFieldProblem(FemPhysicsMixed& source) : BaseProblem(source) {}
		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);
	};

	class PrimalProblem : public BaseProblem
	{
	public:
		PrimalProblem(FemPhysicsMixed& source) : BaseProblem(source) {}
		// Newton solver helpers
		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);

		// Krylov solver helpers
		void ComputeGradients(EigenVector& r) { r = ComputeRhs(EigenVector()); }
		void UpdatePosAndComputeGradients(const EigenVector& solution, EigenVector& r) { r = ComputeRhs(solution); }
		real DotProduct(const EigenVector& a, const EigenVector& b) const { return a.transpose() * b; }
		void MatrixVectorMultiply(const EigenVector& in, EigenVector& out)
		{
			// dense operation for now
			EigenVector s, y;
			EigenMatrix K; // FIXME
			ComputeSystemMatrix(s, y, K);
			out = K * in;
		}
	};

	class CondensedProblem : public BaseProblem
	{
	public:
		CondensedProblem(FemPhysicsMixed& source) : BaseProblem(source)
		{
			EigenMatrix C(mFPM.mVolComplianceMatrix);
			mCinv = C.inverse();
		}
		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);

	private:
		EigenMatrix mCinv; // inverse compliance matrix
	};

	class PrincipalStretchesProblem : public BaseProblem
	{
	public:
		PrincipalStretchesProblem(FemPhysicsMixed& source) : BaseProblem(source) { }
		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);

	private:
		void ComputePSJacobian(EigenMatrix& J);
		EigenVector ComputeConstraint();

	private:
		EigenMatrix mPSJacobian;
		EigenVector mPS, mMultipliers;
	};

} //namespace FEM_SYSTEM