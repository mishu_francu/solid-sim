#include "ClothReg.h"
#include "ClothPatch.h"
#include <Engine/Profiler.h>
#include <Math/Matrix2.h>
#include <algorithm>

namespace Physics
{
	void ClothModelReg::Step(float h)
	{
		PROFILE_SCOPE("Step regularized");

		// unconstrained step
		for (size_t i = 0; i < GetNumParticles(); i++)
		{
			if (mParticles[i].invMass == 0.f)
				continue;
			mParticles[i].prev = mParticles[i].pos; 
			mParticles[i].vel += h * gravity * mUnit;
			mParticles[i].pos += h * mParticles[i].vel;
		}

		// collision detection
		mContacts.clear();
		DetectCollisions();

		HandleMouseSpring(h);

		if (mSolver == SOLVER_GAUSS_SEIDEL)
			SolveRegGS(h);
		else
			SolveRegCR(h, 1);
	}

	void ClothModelReg::SolveRegGS(float h)
	{
		ResetConstraints();

		// projection
		for (int k = 0; k < mNumIterations; ++k)
		{
			if (!mFEM)
				SolveLinks(h);
			else
				SolveTriangles(h);
			if (mDihedral && k == 0)
				SolveBendsReg(h);
			
			mOwnerPatch->GetCollisionHandler().SolveContactsPosition(h);
		}
	}

	void ClothModelReg::SolveLinks(float h)
	{
		//PROFILE_SCOPE("SolveLinksReg");
		const float h2 = h * h;
		const float omega = 1.f;
		for (size_t i = 0; i < mLinks.size(); i++)
		{
			Particle& p1 = mParticles[mLinks[i].i1];
			Particle& p2 = mParticles[mLinks[i].i2];
			Vector3 delta = p1.pos - p2.pos;
			float len0 = mLinks[i].len;
			float len = delta.Length();
			delta.Scale(1.f / len);
			float epsilon = len0 / (mLinks[i].stiffness * mUnit * mArea);
			float err = len - len0;
			float res = err;// - epsilon * mLinks[i].lambda;
			float lambda = omega * res / (h2 * (p1.invMass + p2.invMass) + epsilon);
			mLinks[i].lambda += lambda; // total force magnitude (approx)
			delta.Scale(lambda); // the constraint force
			mLinks[i].disp += delta; // total constraint force
			p1.pos -= h2 * p1.invMass * delta;
			p2.pos += h2 * p2.invMass * delta;
			p1.vel -= h * p1.invMass * delta;
			p2.vel += h * p2.invMass * delta;
		}
	}

	void ClothModelReg::SolveTriangles(float h)
	{
		const float h2 = h * h;
		const float nu = mPoisson; // Poisson ratio
		const float epsilon = 1.f / (max(mStretchStiff, 0.1f) * mUnit * mUnit); // 1 / Young's modulus
		const float epsilonUV = 2 * (1 + nu)  / (max(mShearStiff, 0.1f) * mUnit * mUnit); // 1 / shear modulus
		const Matrix2 E(1, nu, -nu, 1);
		for (size_t i = 0; i < mParticles.size(); i++)
			mParticles[i].force.SetZero();
		for (size_t k = 0; k < mTriangles.size(); k++)
		{
			Triangle& tri = mTriangles[k];
			Particle& p1 = mParticles[tri.i1];
			Particle& p2 = mParticles[tri.i2];
			Particle& p3 = mParticles[tri.i3];
			Vector3 dx1 = (p2.pos - p1.pos);
			Vector3 dx2 = (p3.pos - p1.pos);

			Vector3 wu = tri.invDet * (tri.dv2 * dx1 - tri.dv1 * dx2);
			Vector3 wv = tri.invDet * (-tri.du2 * dx1 + tri.du1 * dx2);

			float err, diag, lambda;
			Vector3 q1, q2, q3;

			// warp and weft
			Vector3 q2u = tri.invDet * tri.dv2 * wu;
			Vector3 q3u = -tri.invDet * tri.dv1 * wu;
			Vector3 q1u = -q2u - q3u;
			Vector3 q2v = -tri.invDet * tri.du2 * wv;
			Vector3 q3v = tri.invDet * tri.du1 * wv;
			Vector3 q1v = -q2v - q3v;
			float a = p1.invMass * q1u * q1u + p2.invMass * q2u * q2u + p3.invMass * q3u * q3u;
			float b = p1.invMass * q1v * q1v + p2.invMass * q2v * q2v + p3.invMass * q3v * q3v;
			float d = p1.invMass * q1u * q1v + p2.invMass * q2u * q2v + p3.invMass * q3u * q3v;
			Matrix2 A(a, d, d, b);
			Matrix2 E1 = (epsilon / tri.area) * E;
			Matrix2 E2(E1.a11 * tri.lu2, E1.a12 * tri.lu2, E1.a21 * tri.lv2, E1.a22 * tri.lv2);
			Matrix2 S = h2 * A + E2;
			//Vector2 cfm = E2 * tri.lambda2;
			tri.euu = 0.5f * ((wu * wu) - tri.lu2);
			tri.evv = 0.5f * ((wv * wv) - tri.lv2);
			Vector2 err2(-tri.euu, -tri.evv);
			Vector2 lambda2 = S.GetInverse() * (err2);// - cfm);
			//tri.lambda2 += lambda2;
			float lambdaU = lambda2.GetX();
			float lambdaV = lambda2.GetY();
			Vector3 imp1 = p1.invMass * (lambdaU * q1u + lambdaV * q1v);
			Vector3 imp2 = p2.invMass * (lambdaU * q2u + lambdaV * q2v);
			Vector3 imp3 = p3.invMass * (lambdaU * q3u + lambdaV * q3v);
			p1.pos += h2 * imp1;
			p2.pos += h2 * imp2;
			p3.pos += h2 * imp3;
			p1.vel += h * imp1;
			p2.vel += h * imp2;
			p3.vel += h * imp3;
			//p1.force += imp1;
			//p2.force += imp2;
			//p3.force += imp3;

			// warp
			//q2 = (tri.invDet * tri.dv2) * wu;
			//q3 = (-tri.invDet * tri.dv1) * wu;
			//q1 = -q2 - q3;
			//diag = (p1.invMass * q1 * q1 + p2.invMass * q2 * q2 + p3.invMass * q3 * q3);
			//err = 0.5f * (wu * wu - tri.lu2);
			//float epsU = tri.lu2 * epsilon / tri.area;
			//lambda = - (err + epsU * tri.lambdaU) / (h2 * diag + epsU);
			//tri.lambdaU += lambda;
			//p1.pos += h2 * p1.invMass * lambda * q1;
			//p2.pos += h2 * p2.invMass * lambda * q2;
			//p3.pos += h2 * p3.invMass * lambda * q3;
			//p1.vel += h * p1.invMass * lambda * q1;
			//p2.vel += h * p2.invMass * lambda * q2;
			//p3.vel += h * p3.invMass * lambda * q3;

			// weft
			//q2 = (-tri.invDet * tri.du2) * wv;
			//q3 = (tri.invDet * tri.du1) * wv;
			//q1 = -q2 - q3;
			//diag = (p1.invMass * q1 * q1 + p2.invMass * q2 * q2 + p3.invMass * q3 * q3);
			//err = 0.5f * (wv * wv - tri.lv2);
			//float epsV = tri.lv2 * epsilon / tri.area;
			//lambda = - (err + epsV * tri.lambdaV) / (h2 * diag + epsV);
			//tri.lambdaV += lambda;
			//p1.pos += h2 * p1.invMass * lambda * q1;
			//p2.pos += h2 * p2.invMass * lambda * q2;
			//p3.pos += h2 * p3.invMass * lambda * q3;
			//p1.vel += h * p1.invMass * lambda * q1;
			//p2.vel += h * p2.invMass * lambda * q2;
			//p3.vel += h * p3.invMass * lambda * q3;

			// shearing
			q2 = tri.invDet * (tri.dv2 * wv - tri.du2 * wu);
			q3 = tri.invDet * (-tri.dv1 * wv + tri.du1 * wu);
			q1 = -q2 - q3;
			diag = (p1.invMass * q1 * q1 + p2.invMass * q2 * q2 + p3.invMass * q3 * q3);
			tri.euv = wu * wv;
			err = tri.euv - tri.dot;
			float epsUV = tri.lu0 * tri.lv0 * epsilonUV / tri.area;
			lambda = -(err + epsUV * tri.lambdaUV) / (h2 * diag + epsUV);
			tri.lambdaUV += lambda;
			p1.pos += h2 * p1.invMass * lambda * q1;
			p2.pos += h2 * p2.invMass * lambda * q2;
			p3.pos += h2 * p3.invMass * lambda * q3;
			p1.vel += h * p1.invMass * lambda * q1;
			p2.vel += h * p2.invMass * lambda * q2;
			p3.vel += h * p3.invMass * lambda * q3;
		}
	}

	void ClothModelReg::SolveBendsReg(float h)
	{
		//PROFILE_SCOPE("SolveBendsReg");
		const float h2 = h * h;
		const float epsilon = 0;//mBendFactor * mStiffness;
		for (size_t i = 0; i < mBends.size(); i++)
		{
			BendConstraint& bc = mBends[i];

			const Vector3& x1 = mParticles[bc.i1].pos;
			const Vector3& x2 = mParticles[bc.i2].pos;
			const Vector3& x3 = mParticles[bc.i3].pos;
			const Vector3& x4 = mParticles[bc.i4].pos;

			Vector3 x12 = x2 - x1;
			Vector3 x13 = x3 - x1;
			Vector3 n1 = cross(x12, x13);
			float l1 = bc.l1;// n1.Length();
			Vector3 x14 = x4 - x1;
			Vector3 n2 = cross(x12, x14);
			float l2 = bc.l2; // n2.Length();

			float c1 = 1.f / (l1 * l2);
			float d = n1 * n2 * c1; // cosine of angle
			d = min(1.f, max(-1.f, d));
			ASSERT(!isnan(c1));
			Vector3 q3 = -c1 * cross(x12, n2);
			Vector3 q4 = -c1 * cross(x12, n1);
			Vector3 q2 = c1 * (cross(x13, n2) + cross(x14, n1));
			Vector3 q1 = -q2 - q3 - q4;

			float diag = mParticles[bc.i1].invMass * q1.LengthSquared() + 
				mParticles[bc.i2].invMass * q2.LengthSquared() + 
				mParticles[bc.i3].invMass * q3.LengthSquared() + 
				mParticles[bc.i4].invMass * q4.LengthSquared();
			float d2 = d * d;
			float sine = sqrt(1 - d * d);
			float arccos = sine; //PI - acos(d);
			float lambda = -(sine * arccos + epsilon * bc.lambda) / (h2 * diag + epsilon); // FIXME
			bc.lambda = lambda;
			mParticles[bc.i1].pos += h2 * lambda * mParticles[bc.i1].invMass * q1;
			mParticles[bc.i2].pos += h2 * lambda * mParticles[bc.i2].invMass * q2;
			mParticles[bc.i3].pos += h2 * lambda * mParticles[bc.i3].invMass * q3;
			mParticles[bc.i4].pos += h2 * lambda * mParticles[bc.i4].invMass * q4;
			mParticles[bc.i1].vel += h * lambda * mParticles[bc.i1].invMass * q1;
			mParticles[bc.i2].vel += h * lambda * mParticles[bc.i2].invMass * q2;
			mParticles[bc.i3].vel += h * lambda * mParticles[bc.i3].invMass * q3;
			mParticles[bc.i4].vel += h * lambda * mParticles[bc.i4].invMass * q4;
		}
	}

	void ClothModelReg::SolveRegCR(float h, float alpha0)
	{
		PROFILE_SCOPE("NCR Reg");

		const float h2 = h * h;
		const float invH = 1.f / h;
		std::vector<float> r, d, q;
		std::vector<Vector3> y;

		size_t nl = mLinks.size();
		if (nl == 0)
			return;
		if (r.size() != nl)
		{
			r.resize(nl);
			d.resize(nl);
			q.resize(nl);
		}

		float rSqrOld = 0;
		size_t np = mParticles.size();
		if (y.size() != np) y.resize(np);

		for (size_t i = 0; i < mLinks.size(); i++)
		{
			mLinks[i].disp.SetZero();
			mLinks[i].lambda = 0;
		}
		memset(&d[0], 0, nl * sizeof(float));
		for (int iter = 0; iter < mNumIterations; iter++)
		{
			// recompute error and normal
			for (size_t i = 0; i < nl; i++)
			{
				Vector3 delta;
				Link& link = mLinks[i];
				delta = mParticles[link.i2].pos - mParticles[link.i1].pos;
				float epsilon = 1.f / (mLinks[i].stiffness * mUnit * h2);
				float c = (delta.Length() - link.len) + epsilon * link.lambda;
				delta.Normalize();
				link.normal = delta;
				r[i] = -c;
			}

#ifndef SKIP_BETA
			// A-norm Fletcher-Reeves
			// y = J^T * r
			memset(&y[0], 0, np * sizeof(Vector3));
			float rSqrNew = 0;
			for (size_t i = 0; i < nl; i++)
			{
				Vector3 add = mLinks[i].normal * r[i];
				y[mLinks[i].i1] += add;
				y[mLinks[i].i2] -= add;
				float epsilon = 1.f / (mLinks[i].stiffness * h2 * mUnit);
				rSqrNew += epsilon * r[i] * r[i];
			}
			// r^2 = r A r
			for (size_t i = 0; i < np; i++)
			{
				rSqrNew += y[i].LengthSquared() * mParticles[i].invMass;
			}

			if (iter > 0)
			{
				// compute new search direction
				float beta = rSqrNew / rSqrOld; // FR
				//beta = min(1.f, beta);
				for (size_t i = 0; i < nl; i++)
				{
					d[i] = r[i] + beta * d[i];
				}
			}
			else
			{
				d = r;
			}
			rSqrOld = rSqrNew;
#else
			float beta = (float)iter / (float)(mNumIterations - 1);
			beta = pow(beta, 0.6f);
			for (size_t i = 0; i < nl; i++)
			{
				d[i] = r[i] + beta * d[i];
			}
#endif

#ifndef SKIP_ALPHA
			float den = 0;
			float dot = 0;
			// y = J^T * d
			memset(&y[0], 0, np * sizeof(Vector3));
			for (size_t i = 0; i < nl; i++)
			{
				Vector3 add = mLinks[i].normal * d[i];
				y[mLinks[i].i1] -= add;
				y[mLinks[i].i2] += add;
			}
			// q = J * W  * y = A * d
			// den = q^T * q
			// dot = r^T * q
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = mLinks[i].i1;
				const int i2 = mLinks[i].i2;
				q[i] = mLinks[i].normal.Dot(y[i2] * mParticles[i2].invMass - y[i1] * mParticles[i1].invMass);
				float epsilon = 1.f / (mLinks[i].stiffness * mUnit * h2);
				q[i] = q[i] + epsilon * d[i]; // actually q = (h^2 * A + eps * I) * d

				den += q[i] * q[i];
				dot += r[i] * q[i];
			}

			if (den == 0)
				break;
			float alpha = dot / den;
			//alpha = min(alpha, alpha0);
#else
			float alpha = alpha0;
#endif
			// apply dlambda = alpha * d
			for (size_t i = 0; i < nl; i++)
			{
				const int i1 = mLinks[i].i1;
				const int i2 = mLinks[i].i2;
				float dLambda = alpha * d[i];
				mLinks[i].lambda += dLambda;
				Vector3 disp = dLambda * mLinks[i].normal;
				mParticles[i1].pos -= mParticles[i1].invMass * disp;
				mParticles[i2].pos += mParticles[i2].invMass * disp;
				mParticles[i1].vel -= invH * mParticles[i1].invMass * disp;
				mParticles[i2].vel += invH * mParticles[i2].invMass * disp;
			}
		}
	}
}