#ifndef GRID_BROADPHASE3D_H
#define GRID_BROADPHASE3D_H

#include <Demo/Particles/Collision.h>
#include <Geometry/Collision3D.h>
#include <Math/Vector3.h>
#include <Demo/Particles/Constraint.h>

typedef ConstraintTemplate<Vector3> Constraint3D;

class GridBroadphase3D
{
private:
	// TODO: duplicate in 2D
	struct ParticleInCell
	{
		int particleId;
		int cellId;

		ParticleInCell() : particleId(0), cellId(0) { }
		ParticleInCell(int p, int c) : particleId(p), cellId(c) { }
		bool operator < (const ParticleInCell& other) const { return cellId < other.cellId; }
	};	

	int width, height, depth;
	float cellSize;
	int* cells;
	std::vector<ParticleInCell> particles;
	Vector3 offset;

public:
	GridBroadphase3D() : cells(NULL) { }
	
	~GridBroadphase3D()
	{
		if (cells != NULL)
			delete cells;
	}

	void Init(const Vector3& center, float w, float h, float d, float cellSize);
	virtual void Sample(const std::vector<Geometry::Aabb3>& handles);
	virtual void Update(const std::vector<Geometry::Aabb3>& handles, std::vector<Constraint3D>& pairs);
	void TestObject(const Geometry::Aabb3& aabb, std::vector<int>& particles);

private:
	void TestCell(const std::vector<Geometry::Aabb3>& handles, std::vector<Constraint3D>& pairs, int count, int start, int x, int y, int z);
};

#endif // GRID_BROADPHASE3D_H