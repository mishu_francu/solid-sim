#pragma once

#include <Math/Vector3.h>
#include <vector>

using Math::Vector3;

namespace Geometry
{
	struct BarycentricCoords;
	struct AabbTree;
}

namespace Physics
{
	struct Triangle;
	struct Particle;
	struct PrimitivePair;
	struct SelfContact;
	class ClothModel;

	class SelfCollisionsDetector
	{
	public:
		SelfCollisionsDetector() : mModel(nullptr) { }
		void Detect();
		void SetClothModel(ClothModel* model) { mModel = model; }

	private:
		void Midphase();
		void Narrowphase();
		void EdgeEdgeDetection();

		typedef float (SelfCollisionsDetector::*CCDFunction)(float t, const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4);

		void VertexTriangleTest(const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Math::Vector3& v2, const Vector3& v3, const Vector3& v4,
			int i1, int i2, int i3, int i4);
		bool VertexTriangleCCD(const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4,
			int i1, int i2, int i3, int i4, Vector3& normal, Geometry::BarycentricCoords& barycentric);

		float EvaluateFun_Coplanarity(float t, const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4);

		bool SolveCubic(const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4, float& toi);
		bool CheckDegenerateCase(float b, float c, float d, float& toi);
		int FindInflexionPoints(float a, float b, float c, float& tInfl1, float& tInfl2);
		bool CheckInterval(CCDFunction fun, float ta, float tb, const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4, float& tx);

		void EdgeEdgeTest(const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4,
			int i1, int i2, int i3, int i4);
		bool EdgeEdgeCCD(const Vector3& x1, const Vector3& x2, const Vector3& x3, const Vector3& x4,
			const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& v4,
			int i1, int i2, int i3, int i4, Vector3& normal, float& s, float& t);

	private:
		ClothModel* mModel;

		// keep these here so it doesn't keep reallocating
		std::vector<PrimitivePair> mVertexTriangleCandidates;
		std::vector<PrimitivePair> mEdgeEdgeCandidates;
	};
}

