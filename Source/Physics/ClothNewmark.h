#include "ClothModel.h"

namespace Physics
{
	class ClothModelNewmark : public ClothModel
	{
	public:
		ClothModelNewmark(ClothPatch* owner) : ClothModel(owner) { }
		ClothModelNewmark(const ClothModel& model) : ClothModel(model) { }
		void Step(float h) override;
	};
}