#include "OpenCL.h"
#include <string>
#include <fstream>

#include <Math/Utils.h>

#pragma comment(lib, "OpenCL.lib")

// convert the kernel file into a string
bool ConvertToString(const char *filename, std::string& s)
{
	size_t size;
	char*  str;
	std::fstream f(filename, (std::fstream::in | std::fstream::binary));

	if(f.is_open())
	{
		size_t fileSize;
		f.seekg(0, std::fstream::end);
		size = fileSize = (size_t)f.tellg();
		f.seekg(0, std::fstream::beg);
		str = new char[size+1];
		if(!str)
		{
			f.close();
			return false;
		}

		f.read(str, fileSize);
		f.close();
		str[size] = '\0';
		s = str;
		delete[] str;
		return true;
	}
	Printf("Error: failed to open file: %s\n", filename);
	return false;
}

OpenCL* OpenCL::GetInstance()
{
	static std::unique_ptr<OpenCL> instance;
	if (!instance)
	{
		instance.reset(new OpenCL());
		if (!instance->Init())
			instance.release();
	}
	return instance.get();
}

bool OpenCL::FindDevice(cl_platform_id platform, cl_device_type deviceType)
{
	// Get number of devices of the given type
	cl_int status = clGetDeviceIDs(platform, deviceType, 0, NULL, &mNumDevices);
	if (status != CL_SUCCESS || mNumDevices == 0)
	{
		return false; // no device found of the given type
	}

	// Enumerate all devices for this platform and choose one
	mDeviceNames.clear();
	mDevices = new cl_device_id[mNumDevices];
	CL_CALL(clGetDeviceIDs(platform, deviceType, mNumDevices, mDevices, NULL));
	for (cl_uint i = 0; i < mNumDevices; i++)
	{
		size_t size = 0;
		CL_CALL(clGetDeviceInfo(mDevices[i], CL_DEVICE_NAME, 0, NULL, &size));

		char* name = new char[size];
		status = clGetDeviceInfo(mDevices[i], CL_DEVICE_NAME, size, name, &size);
		if (status != CL_SUCCESS)
		{
			Printf("Failed getting device name.\n");
			continue;
		}
		Printf("Device %d name: %s\n", i, name);
		mDeviceNames.push_back(name);
	}

	// Create context
	mContext = clCreateContext(NULL, mNumDevices, mDevices, NULL, NULL, NULL);

	// Creating command queue associate with the context
	mDevice = mDevices[std::min(mDeviceIdx, mNumDevices - 1)];
	mCommandQueue = clCreateCommandQueue(mContext, mDevice, 0, NULL);

	return true;
}

bool OpenCL::Init()
{
	if (mInitialized)
	{
		Printf("OpenCL already initialized.\n");
		return true;
	}

	Printf("Initializing OpenCL.\n");
	// Get number of platforms
	cl_uint nPlatforms;
	if (clGetPlatformIDs(0, NULL, &nPlatforms) != CL_SUCCESS)
		return false;

	// Enumerate platforms
	if (nPlatforms == 0)
		return false;
	cl_platform_id *platforms = new cl_platform_id[nPlatforms];
	if (clGetPlatformIDs(nPlatforms, platforms, NULL) != CL_SUCCESS)
		return false;
	platfNames.clear();
	for (cl_uint i = 0; i < nPlatforms; i++)
	{
		size_t size = 0;
		cl_int status = clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, 0, NULL, &size);

		if (status != CL_SUCCESS)
		{
			Printf("Failed getting vendor name size.\n");
			continue;
		}
		char* vendor = new char[size];
		status = clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, size, vendor, &size);
		if (status != CL_SUCCESS)
		{
			Printf("Failed getting vendor name.\n");
			continue;
		}
		Printf("Vendor %d name: %s\n", i, vendor);
		platfNames.push_back(vendor);
	}

	cl_platform_id platform = platforms[std::min(mPlatfIdx, nPlatforms - 1)];

	// look for a device of the desired type for the desired platform
	bool found = FindDevice(platform, mDeviceType);

	if (!found)
	{
		// if not found, look for the first platform that has one
		for (size_t i = 0; i < nPlatforms; i++)
		{
			found = FindDevice(platforms[i], mDeviceType);
			if (found)
				break;
		}
	}

	if (!found)
	{
		// TODO: if still not found, pick any device
		return false;
	}

	mInitialized = true;
	delete[] platforms;
	return true;
}

void OpenCL::DeInit()
{
	if (!mInitialized)
	{
		Printf("OpenCL was not initialized.\n");
		return;
	}
	cl_int status;
	if (mCommandQueue)
		status = clReleaseCommandQueue(mCommandQueue);	//Release  Command queue.
	if (mContext)
		status = clReleaseContext(mContext);				//Release context.
	mInitialized = false;
}

cl_program OpenCL::Build(const char *filename, const char *params)
{
	// Create program object
	std::string sourceStr;
	if (!ConvertToString(filename, sourceStr))
		return false;
	const char *source = sourceStr.c_str();
	size_t sourceSize[] = {strlen(source)};
	cl_program program = clCreateProgramWithSource(mContext, 1, &source, sourceSize, NULL);
	if (program == NULL)
		return false;
	
	// Build program
	cl_int status = clBuildProgram(program, mNumDevices, mDevices, params, NULL, NULL);
	if (status == CL_INVALID_PROGRAM)
	{
		Printf("Invalid program\n");
		return false;
	}
	else if (status != CL_SUCCESS)
	{
		Printf("Program %s failed to build\n", filename);
		// Determine the size of the log
		size_t log_size;
		clGetProgramBuildInfo(program, mDevice, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		// Allocate memory for the log
		char *log = new char[log_size];
		// Get the log
		clGetProgramBuildInfo(program, mDevice, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
		// Print the log
		Printf("%s\n", log);
		delete[] log;
		return false;
	}
	return program;
}

bool OpenCL::GetKernelWorkGroupInfo(cl_kernel &kernel)
{
	cl_int status = CL_SUCCESS;

	//Get Kernel Work Group Info
	size_t kernelWorkGroupSize;
	status = clGetKernelWorkGroupInfo(kernel,
									  mDevice,
									  CL_KERNEL_WORK_GROUP_SIZE,
									  sizeof(size_t),
									  &kernelWorkGroupSize,
									  NULL);
	//if(checkVal(status, CL_SUCCESS, "clGetKernelWorkGroupInfo failed(CL_KERNEL_WORK_GROUP_SIZE)"))
		//	return SDK_FAILURE;
	Printf("work group size: %d\n", kernelWorkGroupSize);
	
	kernelWorkGroupSize;
	status = clGetKernelWorkGroupInfo(kernel,
									  mDevice,
									  CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
									  sizeof(size_t),
									  &kernelWorkGroupSize,
									  NULL);
	//if(checkVal(status, CL_SUCCESS, "clGetKernelWorkGroupInfo failed(CL_KERNEL_WORK_GROUP_SIZE)"))
		//	return SDK_FAILURE;
	Printf("preferred work group size multiple: %d\n", kernelWorkGroupSize);

	cl_ulong localMemoryUsed;
	status = clGetKernelWorkGroupInfo(kernel,
									  mDevice,
									  CL_KERNEL_LOCAL_MEM_SIZE,
									  sizeof(cl_ulong),
									  &localMemoryUsed,
									  NULL);
	//if(checkVal(status, CL_SUCCESS, "clGetKernelWorkGroupInfo failed(CL_KERNEL_LOCAL_MEM_SIZE)"))
		//	return SDK_FAILURE;
	Printf("local memory: %d\n", localMemoryUsed);

	size_t compileWorkGroupSize[3];
	status = clGetKernelWorkGroupInfo(kernel,
									  mDevice,
									  CL_KERNEL_COMPILE_WORK_GROUP_SIZE,
									  sizeof(size_t) * 3,
									  compileWorkGroupSize,
									  NULL);
	//if(checkVal(status, CL_SUCCESS, "clGetKernelWorkGroupInfo failed(CL_KERNEL_COMPILE_WORK_GROUP_SIZE)"))
		//	return SDK_FAILURE;
	Printf("compile work group size: %d %d %d\n", compileWorkGroupSize[0], compileWorkGroupSize[1], compileWorkGroupSize[2]);

	return true;
}
