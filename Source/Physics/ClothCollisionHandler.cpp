#include "ClothCollisionHandler.h"
#include "Engine/Profiler.h"
#include "ClothModel.h"

namespace Physics
{
	bool ClothCollisionHandler::HandleContactsVelocity(float h)
	{
		if (numVelocityIterations == 0)
			return false;

		bool allCollisionsSolved = false;
		int iter;
		for (iter = 0; iter < numVelocityIterations; iter++)
		{
			// static/unary contacts
			SolveContactsVelocity(h);

			// triangles vs vertex self-contacts
			float tris = SolveSelfTrisVelocity(h);

			// edge-edge self-contacts
			float edges = SolveSelfEdgesVelocity(h);

			if (tris + edges < threshold)
			{
				// stop if all self contacts have been solved
				allCollisionsSolved = true;
				break;
			}
		}

		// integrate the positions
		for (size_t i = 0; i < mModel->GetNumParticles(); i++)
		{
			auto& particle = mModel->GetParticle(i);
			if (particle.invMass == 0)
				continue;
			particle.pos = particle.prev + h * particle.vel;
		}
		return allCollisionsSolved;
	}

	void ClothCollisionHandler::HandleContactsPosition(float h)
	{
		if (numPositionIterations == 0)
			return;

		for (int iter = 0; iter < numPositionIterations; iter++)
		{
			// static/unary contacts
			SolveContactsPosition(h);

			// triangles vs vertex self-contacts
			float tris = SolveSelfTrisPosition(h);

			// edge-edge self-contacts
			float edges = SolveSelfEdgesPosition(h);
		}

		// update velocities
		const float invH = 1.f / h;
		for (size_t i = 0; i < mModel->GetNumParticles(); i++)
		{
			Particle& p = mModel->GetParticle(i);
			if (p.invMass == 0)
				continue;
			p.vel = invH * (p.pos - p.prev);
		}
	}

	void ClothCollisionHandler::DetectAndHandle(float h)
	{
		PROFILE_SCOPE("Decoupled collisions");

		InitRigidZones();

		// self-collision iteration
		uint32 numParticles = (uint32)mModel->GetNumParticles();
		int outerIter;
		for (outerIter = 0; outerIter < numSelfCollIterations; outerIter++)
		{
			// we detect the collisions, that will be put in specific arrays
			// we know/assume that the current positions are the ones after a cloth step and the previous positions are the ones before the cloth step
			mModel->DetectCollisions();
			if (outerIter > 0 && mModel->GetNumSelfTris() == 0 && mModel->GetNumSelfEdges() == 0)
			{
				// quit if there are no more self contacts
				return;
			}

			// now solve the contacts using a velocity approach
			// for now use, the the same number of iterations as for PBD
			mModel->ResetConstraints();
			HandleContactsVelocity(h);
			HandleContactsPosition(h);
		}
	}

	void ClothCollisionHandler::SolveContactsVelocity(float h)
	{
		const float invH = 1.0f / h;
		float mu = mModel->GetFriction();
		for (size_t i = 0; i < mModel->GetNumContacts(); i++)
		{
			Contact& contact = mModel->GetContact(i);
			Particle& p1 = mModel->GetParticle(contact.idx);
			Vector3 delta = p1.pos - contact.point;
#ifdef UPDATE_DEPTH
			float len = delta.Dot(contact.normal);
			if (len < mModel->GetThickness())
				contact.depth = mModel->GetThickness() - len;
#endif
			Vector3 vrel = p1.vel; // TODO: contact velocity
			delta = contact.normal;
			float vn = vrel.Dot(delta);
			float err = -vn + baumgarte * contact.depth * invH;

			float lambda0 = contact.lambda;
			contact.lambda += err;
			if (contact.lambda < 0)
				contact.lambda = 0;
			float dLambda = contact.lambda - lambda0;
			delta.Scale(dLambda);
			p1.vel += delta;

			// friction
			Vector3 vt = vrel - vn * contact.normal;
			float slip = vt.Length();
			if (fabs(slip) > 0.001f)
			{
				Vector3 tanDir = (1.f / slip) * vt;
				float limit = mu * contact.lambda;

				float lambdaF0 = contact.lambdaF;
				contact.lambdaF += slip;
				if (contact.lambdaF > limit)
					contact.lambdaF = limit;
				float dLambdaF = contact.lambdaF - lambdaF0;

				p1.vel -= dLambdaF * tanDir;
			}
		}
	}

	void ClothCollisionHandler::SolveContactsPosition(float h)
	{
		PROFILE_SCOPE("Solve Contacts PBD");
		const float invH = 1.f / h;
		const float mu = mModel->GetFriction();
		float thickness = mModel->GetThickness();
		for (size_t i = 0; i < mModel->GetNumContacts(); i++)
		{
			Contact& contact = mModel->GetContact(i);
			Particle& p1 = mModel->GetParticle(contact.idx);
			Vector3 delta = p1.pos - contact.point;
			float len = delta.Dot(contact.normal);
			Vector3 disp = contact.normal;
			float depth = len - thickness;
			if (len > thickness)
				continue; // TODO: clamp lambda instead?
			
			float dLambda = -depth;
			contact.lambda += dLambda;
			disp.Scale(dLambda);

			// friction
			Vector3 v12 = p1.vel;
			float vnrel = contact.normal.Dot(v12);
			Vector3 vt = v12 - vnrel * contact.normal;
			float vtrel = vt.Length();

			const float limit = mu * invH * contact.lambda;

			if (vtrel > 0.001f)
			{
				float dLambdaF = vtrel;
				float lambdaF0 = contact.lambdaF;
				contact.lambdaF = lambdaF0 + dLambdaF;
				if (contact.lambdaF >= limit)
					contact.lambdaF = limit;
				dLambdaF = contact.lambdaF - lambdaF0;

				vt.Scale(1.0f / vtrel); // normalize
				disp += -h * dLambdaF * vt;
			}

			// apply resulting displacement
			p1.pos += disp;
			p1.vel += invH * disp; // for solvers that do not update velocity at the end
		}
	}

	float ClothCollisionHandler::SolveSelfTrisVelocity(float h)
	{
		float totalError = 0;
		float mu = mModel->GetFriction();
		for (size_t i = 0; i < mModel->GetNumSelfTris(); i++)
		{
			SelfContact& contact = mModel->GetSelfTriangle(i);
			Particle& p1 = mModel->GetParticle(contact.i1);
			Particle& p2 = mModel->GetParticle(contact.i2);
			Particle& p3 = mModel->GetParticle(contact.i3);
			Particle& p4 = mModel->GetParticle(contact.i4);
			float invMass = contact.w1 * contact.w1 * p1.invMass + contact.w2 * contact.w2 * p2.invMass +
				contact.w3 * contact.w3 * p3.invMass + p4.invMass;
			float s = 1.f / invMass;

			Vector3 vel = contact.w1 * p1.vel + contact.w2 * p2.vel	+ contact.w3 * p3.vel;
			Vector3 deltaV = vel - p4.vel;
			float vrel = contact.normal.Dot(deltaV);
			float err = vrel + baumgarte * contact.depth / h;
			totalError += err * err;

			float lambda0 = contact.lambda;
			contact.lambda += s * err;
			if (contact.lambda < 0)
				contact.lambda = 0;
			float dLambda = contact.lambda - lambda0;

			// the normal impulse
			Vector3 imp = dLambda * contact.normal;

			// friction
			Vector3 vt = deltaV - vrel * contact.normal;
			float slip = vt.Length();
			if (fabs(slip) > 0.001f)
			{
				Vector3 tanDir = (1.f / slip) * vt;
				float limit = mu * contact.lambda;

				float lambdaF0 = contact.lambdaF;
				contact.lambdaF += slip * s;
				if (contact.lambdaF > limit)
					contact.lambdaF = limit;
				float dLambdaF = contact.lambdaF - lambdaF0;

				Vector3 impF = dLambdaF * tanDir;
				imp += impF;
			}

			// apply the total resulting impulse
			p1.vel -= imp * (contact.w1 * p1.invMass);
			p2.vel -= imp * (contact.w2 * p2.invMass);
			p3.vel -= imp * (contact.w3 * p3.invMass);
			p4.vel += imp * p4.invMass;
		}
		return totalError;
	}

	float ClothCollisionHandler::SolveSelfTrisPosition(float h)
	{
		float len0 = mModel->GetThickness();
		float mu = mModel->GetFriction();
		for (size_t i = 0; i < mModel->GetNumSelfTris(); i++)
		{
			SelfContact& contact = mModel->GetSelfTriangle(i);
			Particle& p1 = mModel->GetParticle(contact.i1);
			Particle& p2 = mModel->GetParticle(contact.i2);
			Particle& p3 = mModel->GetParticle(contact.i3);
			Particle& p4 = mModel->GetParticle(contact.i4);
			Vector3 p = contact.w1 * p1.pos + contact.w2 * p2.pos + contact.w3 * p3.pos;
			Vector3 delta = p4.pos - p;
			float len = delta.Length();
			Vector3 n = delta;
			n.Normalize();
			if (len > len0)
				continue;
			float invMass = contact.w1 * contact.w1 * p1.invMass + contact.w2 * contact.w2 * p2.invMass +
				contact.w3 * contact.w3 * p3.invMass + p4.invMass;
			float s = 1.f / invMass;
			float dLambda = s * (len - len0);

			// the normal displacement
			Vector3 disp = dLambda * n;
			contact.lambda += -dLambda / h;

			Vector3 vel = contact.w1 * p1.vel + contact.w2 * p2.vel + contact.w3 * p3.vel;
			Vector3 deltaV = vel - p4.vel;
			float vrel = contact.normal.Dot(deltaV);

			// friction
			Vector3 vt = deltaV - vrel * contact.normal;
			float slip = vt.Length();
			if (fabs(slip) > 0.001f)
			{
				Vector3 tanDir = (1.f / slip) * vt;
				float limit = mu * contact.lambda;

				float lambdaF0 = contact.lambdaF;
				contact.lambdaF += slip * s;
				if (contact.lambdaF > limit)
					contact.lambdaF = limit;
				float dLambdaF = contact.lambdaF - lambdaF0;

				Vector3 impF = dLambdaF * tanDir;
				disp += -h * impF;
			}

			p1.pos += disp * (contact.w1 * p1.invMass);
			p2.pos += disp * (contact.w2 * p2.invMass);
			p3.pos += disp * (contact.w3 * p3.invMass);
			p4.pos -= disp * (p4.invMass);
		}
		return 1;
	}

	float ClothCollisionHandler::SolveSelfEdgesVelocity(float h)
	{
		float totalError = 0;
		float mu = mModel->GetFriction();
		for (size_t i = 0; i < mModel->GetNumSelfEdges(); i++)
		{
			SelfContact& contact = mModel->GetSelfEdge(i);
			Particle& p1 = mModel->GetParticle(contact.i1);
			Particle& p2 = mModel->GetParticle(contact.i2);
			Particle& p3 = mModel->GetParticle(contact.i3);
			Particle& p4 = mModel->GetParticle(contact.i4);

			Vector3& v1 = p1.vel;
			Vector3& v2 = p2.vel;
			Vector3& v3 = p3.vel;
			Vector3& v4 = p4.vel;
			Vector3 vp = v1 + contact.w1 * (v2 - v1);
			Vector3 vq = v3 + contact.w2 * (v4 - v3);
			Vector3 deltaV = vp - vq;
			float vrel = contact.normal.Dot(deltaV);
			float err = vrel + baumgarte * contact.depth / h;
			totalError += err * err;

			float omw1 = 1.f - contact.w1;
			float omw2 = 1.f - contact.w2;
			float invMass = contact.w1 * contact.w1 * p1.invMass + omw1 * omw1 * p2.invMass
				+ contact.w2 * contact.w2 * p3.invMass + omw2 * omw2 * p4.invMass;
			float s = 1.f / invMass;

			float lambda0 = contact.lambda;
			contact.lambda += s * err;
			if (contact.lambda < 0)
				contact.lambda = 0;
			float dLambda = contact.lambda - lambda0;

			// the normal impulse
			Vector3 imp = dLambda * contact.normal;

			// friction
			Vector3 vt = deltaV - vrel * contact.normal;
			float slip = vt.Length();
			if (fabs(slip) > 0.001f)
			{
				Vector3 tanDir = (1.f / slip) * vt;
				float limit = mu * contact.lambda;

				float lambdaF0 = contact.lambdaF;
				contact.lambdaF += slip * s;
				if (contact.lambdaF > limit)
					contact.lambdaF = limit;
				float dLambdaF = contact.lambdaF - lambdaF0;

				// the friction impulse
				Vector3 impF = dLambdaF * tanDir;
				imp += impF;
			}

			// apply the total resulting impulse
			v1 -= imp * (omw1 * p1.invMass);
			v2 -= imp * (contact.w1 * p2.invMass);
			v3 += imp * (omw2 * p3.invMass);
			v4 += imp * (contact.w2 * p4.invMass);
		}
		return totalError;
	}

	float ClothCollisionHandler::SolveSelfEdgesPosition(float h)
	{
		float thickness = mModel->GetThickness();
		float mu = mModel->GetFriction();
		for (size_t i = 0; i < mModel->GetNumSelfEdges(); i++)
		{
			SelfContact& contact = mModel->GetSelfEdge(i);
			Particle& p1 = mModel->GetParticle(contact.i1);
			Particle& p2 = mModel->GetParticle(contact.i2);
			Particle& p3 = mModel->GetParticle(contact.i3);
			Particle& p4 = mModel->GetParticle(contact.i4);

			Vector3 p = p1.pos + contact.w1 * (p2.pos - p1.pos);
			Vector3 q = p3.pos + contact.w2 * (p4.pos - p3.pos);
			float len = (q - p).Length();
			Vector3 n = q - p;
			n.Normalize();
			if (len > thickness)
				continue;
			float omw1 = 1.f - contact.w1;
			float omw2 = 1.f - contact.w2;
			float invMass = contact.w1 * contact.w1 * p1.invMass + omw1 * omw1 * p2.invMass
				+ contact.w2 * contact.w2 * p3.invMass + omw2 * omw2 * p4.invMass;
			float s = 1.f / invMass;
			
			float dLambda = -s * (len - thickness);
			contact.lambda += dLambda / h;

			// the normal displacement
			Vector3 disp = dLambda * n;

			// friction
			Vector3 vp = p1.vel + contact.w1 * (p2.vel - p1.vel);
			Vector3 vq = p3.vel + contact.w2 * (p4.vel - p3.vel);
			Vector3 deltaV = vp - vq;
			float vrel = contact.normal.Dot(deltaV);
			Vector3 vt = deltaV - vrel * contact.normal;
			float slip = vt.Length();
			if (fabs(slip) > 0.001f)
			{
				Vector3 tanDir = (1.f / slip) * vt;
				float limit = mu * contact.lambda;

				float lambdaF0 = contact.lambdaF;
				contact.lambdaF += slip * s;
				if (contact.lambdaF > limit)
					contact.lambdaF = limit;
				float dLambdaF = contact.lambdaF - lambdaF0;

				// the friction impulse
				Vector3 impF = dLambdaF * tanDir;
				disp += impF * h;
			}

			p1.pos -= disp * (omw1 * p1.invMass);
			p2.pos -= disp * (contact.w1 * p2.invMass);
			p3.pos += disp * (omw2 * p3.invMass);
			p4.pos += disp * (contact.w2 * p4.invMass);
		}
		return 1;
	}

	// *****************************************************************
	// Rigid zones
	// *****************************************************************

	void ClothCollisionHandler::InitRigidZones()
	{
		// init association between particles and sets
		uint32 numParticles = (uint32)mModel->GetNumParticles();
		mSetAssociations.resize(numParticles);
		for (uint32 i = 0; i < mSetAssociations.size(); i++)
		{
			mSetAssociations[i] = i;
		}

		// init the list of sets
		mSetList.resize(numParticles);
		for (uint32 i = 0; i < mSetList.size(); i++)
		{
			mSetList[i].insert(i);
		}
	}

	void ClothCollisionHandler::BuildRigidZones()
	{
		// go through all self contacts and add their indices to the rigid zones
		std::vector<SelfContact> selfContacts(mModel->GetSelfTris());
		selfContacts.insert(selfContacts.end(), mModel->GetSelfEdges().begin(), mModel->GetSelfEdges().end());
		for (uint32 i = 0; i < selfContacts.size(); i++)
		{
			// choose by convention the first index to be the one indicating the set
			const SelfContact& sc = selfContacts[i];
			int setId1 = mSetAssociations[sc.i1];
			int setId2 = mSetAssociations[sc.i2];
			int setId3 = mSetAssociations[sc.i3];
			int setId4 = mSetAssociations[sc.i4];

			auto& set1 = mSetList[setId1];

			// add the contents of the other sets to the first one
			if (setId1 != setId2)
			{
				auto& set2 = mSetList[setId2];
				set1.insert(set2.begin(), set2.end());
				set2.clear();
			}
			if (setId1 != setId3)
			{
				auto& set3 = mSetList[setId3];
				set1.insert(set3.begin(), set3.end());
				set3.clear();
			}
			if (setId1 != setId4)
			{
				auto& set4 = mSetList[setId4];
				set1.insert(set4.begin(), set4.end());
				set4.clear();
			}

			// update the associations to set 1
			for (int elem : set1)
			{
				mSetAssociations[elem] = setId1;
			}
		}
	}

	void ClothCollisionHandler::HandleRigidZone(std::set<int> zone, float h)
	{
		// compute center of mass (COM) and velocity of COM
		Printf("Rigid zone\n");
		float mass = 0;
		Vector3 xcm, vcm;
		for (int i : zone)
		{
			const Particle& p = mModel->GetParticle(i);
			if (p.invMass == 0)
				continue;
			float m = 1.f / p.invMass;
			mass += m;
			xcm += m * p.pos;
			vcm += m * p.vel;
		}
		float invMass = 1.f / mass;
		xcm *= invMass;
		vcm *= invMass;

		// compute inertia tensor and angular momentum
		Matrix3 I;
		Vector3 L;
		for (int i : zone)
		{
			const Particle& p = mModel->GetParticle(i);
			if (p.invMass == 0)
				continue;
			float m = 1.f / p.invMass;
			Vector3 r = p.pos - xcm;
			Matrix3 inertia = m * (r.LengthSquared() * Matrix3::Identity() + Matrix3::TensorProduct(r, r));
			I = I + inertia;
			L += m * cross(r, p.vel - vcm);
		}

		// now we can compute the angular velocity
		Vector3 w = I.GetInverse() * L;
		float angle = h * w.Length();
		Vector3 axis = w; // axis of rotation
		axis.Normalize();

		// finally, modify the positions of the vertices according to the rigid motion
		//float invH = 1.f / h;
		for (int i : zone)
		{
			Particle& p = mModel->GetParticle(i);
			if (p.invMass == 0)
				continue;
			Vector3 r = p.pos - xcm;
			p.vel = vcm + cross(w, r);
			p.pos = p.prev + h * p.vel;

			// TODO: fix Bridson formula
			//Vector3 xf = axis.Dot(r) * axis; // fixed part
			//Vector3 xr = r - xf; // rotated part
			//mParticles[i].pos = xcm + h * vcm + xf + cosf(angle) * xr + sinf(angle) * cross(axis, xr);
			//mParticles[i].vel = invH * (mParticles[i].pos - mParticles[i].prev);
		}
	}

}