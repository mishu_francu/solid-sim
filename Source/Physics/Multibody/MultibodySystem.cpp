#include "MultibodySystem.h"
#include <Demo/Particles/PhysicsSystem.h>
#include <Demo/Particles/ParticleSystemPbd.h>
#include <Demo/Particles/ParticleSystemPenalty.h>
#include "FEMPhysics2D.h"
#include "FEMTests.h"

namespace MBD
{
	MultibodySystem::MultibodySystem(bool useFEM) : mUseFEM(useFEM), mMaterial(MMT_LINEAR)
	{
		FEMTests tests;
		tests.TestStrain();
		tests.TestForce();
		tests.TestForceDifferential();
		tests.TestVolJacobian();
	}

	MultibodySystem::~MultibodySystem()
	{
		// needed for pimpl using unique_ptr
	}

	void MultibodySystem::AddParticle(real mass, const Math::Vector2& pos, const Math::Vector2& vel)
	{
		Particle p;
		p.mass = mass;
		p.pos = pos;
		p.vel = vel;
		mParticles.push_back(p);
	}

	void MultibodySystem::AddDistanceConstraint(uint32 idx1, uint32 idx2)
	{
		if (mParticles[idx1].mass == 0 && mParticles[idx2].mass == 0)
			return;
		DistanceConstraint dc;
		dc.idx1 = idx1;
		dc.idx2 = idx2;
		mDistanceConstraints.push_back(dc);
	}

	void MultibodySystem::AddTriangleElement(uint32 idx1, uint32 idx2, uint32 idx3)
	{
		TriangleElement te;
		te.idx1 = idx1;
		te.idx2 = idx2;
		te.idx3 = idx3;
		mTriangleElements.push_back(te);
	}

	void MultibodySystem::Clear()
	{
		mParticleSys.reset(nullptr);
		mFEMPhysics2D.reset(nullptr);
		mParticles.clear();
		mDistanceConstraints.clear();
		mTriangleElements.clear();
	}

	void MultibodySystem::Compile()
	{
		if (!mUseFEM)
		{
			// realize using PhysicsSystem
			mParticleSys.reset(new ParticleSystemPbd<>());

			mParticleSys->Init(mWidth, mHeight, 5);
			mParticleSys->SetNumParticles(GetParticlesCount());
			mParticleSys->SetDetectCollision(false);

			for (uint32 i = 0; i < GetParticlesCount(); i++)
			{
				mParticleSys->GetParticle(i).invMass = mParticles[i].mass == 0 ? 0 : 1 / mParticles[i].mass;
				mParticleSys->GetParticle(i).pos = mParticles[i].pos;
				mParticleSys->SetParticleVelocity(i, mParticles[i].vel);
			}

			for (uint32 i = 0; i < GetDistanceConstraintsCount(); i++)
			{
				const DistanceConstraint& dc = mDistanceConstraints[i];
				mParticleSys->AddLink(dc.idx1, dc.idx2);
			}
		}
		else
		{
			// realize using FEMPhysics2D
			std::vector<FEMMesh::Node> nodes(GetParticlesCount());
			for (uint32 i = 0; i < GetParticlesCount(); i++)
			{
				nodes[i].invMass = mParticles[i].mass == 0 ? 0 : 1 / mParticles[i].mass;
				nodes[i].pos = mParticles[i].pos;
				nodes[i].vel = mParticles[i].vel;
			}
			std::vector<FEMMesh::Element> elems(GetTriangleElementsCount());
			for (uint32 i = 0; i < GetTriangleElementsCount(); i++)
			{
				elems[i].idx[0] = mTriangleElements[i].idx1;
				elems[i].idx[1] = mTriangleElements[i].idx2;
				elems[i].idx[2] = mTriangleElements[i].idx3;
			}
			
			FEMPhysics2D::Config config;
			config.mSimType = ST_STATIC;
			config.mMaterial = mMaterial;
			config.mYoungsModulus = 100000;
			config.mPoissonRatio = (real)0.4;
			config.mSolverType = IST_MIXED;
			config.mGravity = Vector2R(0, 0);

			FEMMesh mesh;
			mesh.mNodes = nodes;
			mesh.mElements = elems;

			mFEMPhysics2D.reset(new FEMPhysics2D(config));
			mFEMPhysics2D->Init(mesh);

			// add a displacement
			//mFEMPhysics2D->GetNodePos(2).y += 10;
			//mFEMPhysics2D->GetNodePos(3).y += 10;
		}
	}

	void MultibodySystem::Step()
	{
		if (!mUseFEM)
		{
			// step using PhysicsSystem
			if (mParticleSys)
			{
				mParticleSys->Step();

				// update data from PhysicsSystem
				for (uint32 i = 0; i < GetParticlesCount(); i++)
				{
					mParticles[i].pos = mParticleSys->GetParticle(i).pos;
					mParticles[i].vel = mParticleSys->GetParticleVelocity(i);
				}
			}
		}
		else
		{
			// update data from FEMPhysics2D
			mFEMPhysics2D->Step((real)0.016);
			for (uint32 i = 0; i < GetParticlesCount(); i++)
			{
				mParticles[i].pos = mFEMPhysics2D->GetNodePos(i);
				mParticles[i].vel = mFEMPhysics2D->GetNodeVel(i);
			}
		}
	}

} // namespace MBD