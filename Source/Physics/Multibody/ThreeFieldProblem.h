#ifndef THREE_FIELD_PROBLEM_H
#define THREE_FIELD_PROBLEM_H

#include "BaseMixedProblem.h"

namespace MBD
{
	class ThreeFieldProblem : public BaseProblem
	{
	public:
		ThreeFieldProblem(FEMMixedSolver& source) : BaseProblem(source) {}
		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);
	};
}

#endif // THREE_FIELD_PROBLEM_H
