#pragma once

#include "StandardProblem.h"

namespace MBD
{
	class FEMMixedSolver;

	class BaseProblem : public StandardProblem
	{
	public:
		BaseProblem(FEMMixedSolver& source);
		EigenVector SolveLinearSystem(const EigenMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y);
		real MeritResidual(const EigenVector& rhs) const { return rhs.norm(); }
		//bool CheckForInversion() const { return mFPM.CheckForInversion(); }

	protected:
		FEMMixedSolver& mMixed;
	};
}