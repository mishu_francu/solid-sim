#include "FEMLinearSolver.h"
#include "FEMPhysics2D.h"

namespace MBD
{
	FEMLinearSolver::FEMLinearSolver(FEMPhysics2D& fem) : mFEM(fem)
	{
		// elastic params
		real omn = 1.f - mFEM.GetPoissonRatio();
		real om2n = 1.f - 2 * mFEM.GetPoissonRatio();
		real s = mFEM.GetYoungsModulus() / (1.f + mFEM.GetPoissonRatio()); // this is actually 2 * mu, i.e. twice the shear modulus
		real f = s / om2n;
		// the constitutive relation matrix for normal stresses/strains [Mueller]
		mNormalElasticityMatrix = f * Matrix2R(omn, mFEM.GetPoissonRatio(),
			mFEM.GetPoissonRatio(), omn);
		// the constitutive relation matrix for shear components is just diag(2 * mu) [Mueller] - or is it?
	}

	void FEMLinearSolver::SolveEquilibrium()
	{
		AssembleStiffnessMatrix();
		EigenMatrix K(mStiffnessMatrix);
		auto decomp = K.lu();

		// add the gravity forces
		MVector loads(mFEM.GetNumFreeNodes());
		mFEM.ComputeBodyForces(loads);
		EigenVector f = GetEigenVector(loads);

		EigenVector sol = decomp.solve(mFEM.mForceFraction * f);
		Vector2Array u = GetStdVector(sol);
		for (uint32 i = 0; i < mFEM.GetNumFreeNodes(); i++)
		{
			mFEM.GetFreeNodePos(i) = mFEM.GetFreeNodePos0(i) + u[i];
		}
	}

	// assemble the global stiffness matrix for linear elasticity
	void FEMLinearSolver::AssembleStiffnessMatrix()
	{
		uint32 numNodes = mFEM.GetNumFreeNodes();
		uint32 numDofs = numNodes * NUM_DIMENSIONS;
		mStiffnessMatrix.resize(numDofs, numDofs);
		mStiffnessMatrix.setZero();
		//mBCStiffnessMatrix.resize(numDofs, mNumBCs * 3);
		//mBCStiffnessMatrix.setZero();
		// go through all linear elements (tetrahedra)
		for (uint32 i = 0; i < mFEM.GetNumElements(); i++)
		{
			EigenMatrix Klocal;
			ComputeLocalStiffnessMatrix(i, Klocal);
			// the local stiffness matrix Klocal is organized in 3x3 blocks for each pair of nodes
			for (uint32 j = 0; j < mFEM.GetNumLocalNodes(); j++)
			{
				uint32 jGlobal = mFEM.GetGlobalIndex(i, j);
				for (uint32 k = 0; k < mFEM.GetNumLocalNodes(); k++)
				{
					uint32 kGlobal = mFEM.GetGlobalIndex(i, k);
					if (jGlobal < mFEM.mNumBCs)
						continue;
					int jOffset = (jGlobal - mFEM.mNumBCs) * NUM_POS_COMPONENTS;
					int kOffset = (kGlobal - mFEM.mNumBCs) * NUM_POS_COMPONENTS;

					// add the the whole block to the block matrix
					for (uint32 x = 0; x < NUM_POS_COMPONENTS; x++)
					{
						for (uint32 y = 0; y < NUM_POS_COMPONENTS; y++)
						{
							real val = Klocal(j * NUM_POS_COMPONENTS + x, k * NUM_POS_COMPONENTS + y);
							if (kGlobal < mFEM.mNumBCs)
							{
								//mBCStiffnessMatrix.coeffRef(jOffset + x, kGlobal * NUM_POS_COMPONENTS + y) += val;
							}
							else
							{
								mStiffnessMatrix.coeffRef(jOffset + x, kOffset + y) += val;
							}
						}
					}
				}
			}
		}
	}

	void FEMLinearSolver::ComputeLocalStiffnessMatrix(uint32 i, EigenMatrix& Klocal)
	{
		// this code only works for linear elements
		const int size = NUM_ELEMENT_NODES * NUM_POS_COMPONENTS;
		Klocal = EigenMatrix(size, size);

		Matrix2R Bn[NUM_ELEMENT_NODES];
		Vector2R Bs[NUM_ELEMENT_NODES];
		ComputeStrainJacobian(i, Bn, Bs);

		// for linear FEM we can actually precompute the tangent stiffness matrix
		real s = mFEM.GetYoungsModulus() / (1.0 + mFEM.GetPoissonRatio());
		for (int j = 0; j < NUM_ELEMENT_NODES; j++)
		{
			for (int k = 0; k < NUM_ELEMENT_NODES; k++)
			{
				Matrix2R block = mFEM.GetElementInitialExtent(i) * (Bn[j] * mNormalElasticityMatrix * Bn[k] + (mFEM.GetShearModulus()) * Matrix2R::TensorProduct(Bs[j], Bs[k]));
				// copy the block into Klocal
				for (uint32 a = 0; a < NUM_POS_COMPONENTS; a++)
				{
					for (uint32 b = 0; b < NUM_POS_COMPONENTS; b++)
					{
						Klocal(j * NUM_POS_COMPONENTS + a, k * NUM_POS_COMPONENTS + b) = block(a, b);
					}
				}
			}
		}
		//std::cout << "Klocal linear" << std::endl << Klocal << std::endl;
	}

	void FEMLinearSolver::GetBarycentricJacobianVectors(uint32 e, Vector2R y[]) const
	{
		const Matrix2R& mat = mFEM.mElements[e].jacobian;
		y[1] = mat(0);
		y[2] = mat(1);
		y[0] = y[1] + y[2];
		y[0].Flip();
	}

	void FEMLinearSolver::ComputeStrainJacobian(uint32 e, Matrix2R Bn[], Vector2R Bs[])
	{
		Vector2R y[NUM_ELEMENT_NODES];
		GetBarycentricJacobianVectors(e, y);

		// compute the Cauchy strain Jacobian matrix according to [Mueller]: B = de/dx
		for (int j = 0; j < NUM_ELEMENT_NODES; j++)
		{
			Bn[j] = Matrix2R(y[j]); // diagonal matrix
			Bs[j] = Vector2R(y[j].y, y[j].x);
		}
	}

} // namespace MBD