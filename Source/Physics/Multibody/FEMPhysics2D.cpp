#include "FEMPhysics2D.h"
#include "ElasticEnergy2D.h"
#include "FEMOptimizationSolver.h"
#include "FEMLinearSolver.h"
#include "FEMMixedSolver.h"
#include <Engine/Utils.h>

#include <algorithm>
#include <iostream>

namespace MBD
{	
	FEMPhysics2D::FEMPhysics2D()
		: mNumSteps(1)
		, mTimeStep (0)
		, mNumBCs(0)
		, mForceFraction(0)
		, mForceStep((real)0.1)
		, mDensity(1)
		//, mBaseEnergy(0)
		, mUseLumpedMass(true)
	{
		// optionally override the config defaults
		mConfig.mSimType = ST_EXPLICIT;
		mConfig.mMaterial = MMT_NEO_HOOKEAN_OGDEN;
		mConfig.mYoungsModulus = 500000;
		mConfig.mPoissonRatio = (real)0.4;
		mConfig.mSolverType = IST_NONLINEAR;
		mConfig.mGravity = Vector2R(0, 100);
	}

	FEMPhysics2D::FEMPhysics2D(const Config& config) 
		: mConfig(config)
		, mNumSteps(1)
		, mTimeStep(0)
		, mNumBCs(0)
		, mForceFraction(0)
		, mForceStep((real)0.1)
		, mDensity(1)
		//, mBaseEnergy(0)
		, mUseLumpedMass(true)
	{
	}

	void FEMPhysics2D::Step(real dt)
	{
		// save positions before step
		mPreviousPositions = mDeformedPositions;

		FEMOptimizationSolver solver(*this, FEMOptimizationSolver::NST_NEWTON);

		if (GetSimType() == ST_IMPLICIT)
		{
			real h = dt / mNumSteps;
			for (uint32 i = 0; i < mNumSteps; i++)
			{
				mTimeStep = h;
				mForceFraction = 1;
				solver.Solve();
			}
		}
		else if (GetSimType() == ST_EXPLICIT)
		{
			real h = dt / mNumSteps;
			for (uint32 i = 0; i < mNumSteps; i++)
			{
				SubStepExplicit(h);
			}
		}
		else if (GetSimType() == ST_STATIC)
		{
			if (mForceFraction == 0)
			{
				mForceFraction = 1;
				if (GetSolverType() == IST_NONLINEAR)
					solver.Solve();
				else if (GetSolverType() == IST_LINEAR)
				{
					FEMLinearSolver linearSolver(*this);
					linearSolver.SolveEquilibrium();
				}
				else if (GetSolverType() == IST_MIXED)
				{
					FEMMixedSolver mixedSolver(*this);
					mixedSolver.Solve();
				}
			}
		}
		else if (GetSimType() == ST_QUASI_STATIC)
		{
			if (mForceFraction < 1)
			{
				mForceFraction = std::min(real(1), mForceFraction + mForceStep);
				solver.Solve();
			}
		}
	}

	// TODO: move to FEMExplicitIntegrator
	void FEMPhysics2D::SubStepExplicit(real h)
	{
		mForces.setZero();
		ElasticEnergy2D::ComputeForces(this, mForces);

		MVector loads(GetNumFreeNodes());
		ComputeBodyForces(loads);

		// integrate node velocities and positions using Symplectic Euler
		if (mUseLumpedMass)
		{
			for (uint32 i = 0; i < GetNumNodes(); i++)
			{
				if (mInvMasses[i] == 0)
					continue;
				GetNodeVel(i) += (h * GetNodeInvMass(i)) * (mForces[i] + loads[i - mNumBCs] + mExtForces[i - mNumBCs]);
				GetNodePos(i) += h * GetNodeVel(i);
			}
		}
		else
		{
			EigenVector f = mForces.tail(GetNumFreeNodes() * NUM_POS_COMPONENTS) + loads + mExtForces.ToEigen();
			// solve the linear system for the accelerations (invert the mass matrix) 
			mMassMatrix.makeCompressed();
			Eigen::SparseLU<SparseMatrix> sparseLU;
			sparseLU.compute(mMassMatrix);
			EigenVector sol = sparseLU.solve(f);
			RVector a(sol);

			for (uint32 i = 0; i < GetNumNodes(); i++)
			{
				if (mInvMasses[i] == 0)
					continue;
				GetNodeVel(i) += h * a[i - mNumBCs];
				GetNodePos(i) += h * GetNodeVel(i);
			}
		}
	}

	void FEMPhysics2D::Init(const FEMMesh& mesh)
	{
		uint32 numNodes = (uint32)mesh.mNodes.size();

		// count BCs
		mNumBCs = 0;
		for (uint32 i = 0; i < numNodes; i++)
		{
			if (mesh.mNodes[i].invMass == 0)
				mNumBCs++;
		}

		//uint32 numFreeNodes = numNodes - mNumBCs;
		//mReferencePositions.resize(numNodes);
		mDeformedPositions.resize(numNodes);
		mPreviousPositions.resize(numNodes);
		mVelocities.resize(numNodes);
		for (uint32 i = 0; i < numNodes; i++)
		{
			//mReferencePositions[i] = mesh.mNodes[i].pos;
			mDeformedPositions[i] = mesh.mNodes[i].pos;
			mPreviousPositions[i] = mesh.mNodes[i].pos;
			mVelocities[i] = mesh.mNodes[i].vel;
		}
		mElements = mesh.mElements;
		mForces.resize(numNodes);

		// prepare the vector of lumped masses
		std::vector<real> masses(numNodes, 0);

		// init the shape matrices
		mTotalInitialExtent = 0;
		for (uint32 i = 0; i < GetNumElements(); i++)
		{
			FEMMesh::Element& elem = mElements[i];
			Vector2R x[NUM_ELEMENT_NODES];
			for (uint32 j = 0; j < NUM_ELEMENT_NODES; j++)
				x[j] = GetNodePos(elem.idx[j]);
			Vector2R d[NUM_ELEMENT_NODES - 1];
			for (uint32 j = 0; j < NUM_ELEMENT_NODES - 1; j++)
				d[j] = x[j + 1] - x[0];
			Matrix2R mat(d[0], d[1]); // this is the reference shape matrix Dm [Sifakis][Teran03]
			Matrix2R X = mat.GetInverse(); // Dm^-1
			real extent = mat.Determinant() / 2;
			mTotalInitialExtent += extent;

			elem.extent = extent;
			elem.jacobian = !X; // Dm^-T pre-stored

			real lumpedMass = real(1.0 / NUM_ELEMENT_NODES) * extent * mDensity;
			for (uint32 j = 0; j < NUM_ELEMENT_NODES; j++)
				masses[elem.idx[j]] += lumpedMass;
		}

		// set masses
		mInvMasses.resize(numNodes);
		for (uint32 i = 0; i < GetNumNodes(); i++)
		{
			if (mesh.mNodes[i].invMass != 0)
			{
				if (masses[i] != 0)
					mInvMasses[i] = 1 / masses[i];
			}
			else
				mInvMasses[i] = 0;
		}

		AssembleMassMatrix();

		mForceFraction = 1;
		//mBaseEnergy = ComputeEnergy();
		mForceFraction = 0;

		mPressures.resize(numNodes); // nodal pressures

		mExtForces.resize(GetNumFreeNodes());
		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
			mExtForces[i].x = -1000000;
	}

	Matrix2R FEMPhysics2D::ComputeShapeMatrix(uint32 e) const
	{
		const FEMMesh::Element& elem = mElements[e];
		// compute deformed/spatial shape matrix Ds [Sifakis]
		Vector2R x[NUM_ELEMENT_NODES];
		for (uint32 j = 0; j < NUM_ELEMENT_NODES; j++)
			x[j] = GetNodePos(elem.idx[j]);
		Vector2R d[NUM_ELEMENT_NODES - 1];
		for (uint32 j = 0; j < NUM_ELEMENT_NODES - 1; j++)
			d[j] = x[j + 1] - x[0];
		return Matrix2R(d[0], d[1]);
	}

	real FEMPhysics2D::ComputeExtent(uint32 e) const
	{
		Matrix2R Ds = ComputeShapeMatrix(e);
		return 0.5 * Ds.Determinant();
	}

	void FEMPhysics2D::ComputeDeformationGradient(uint32 e, Matrix2R& F) const
	{
		const FEMMesh::Element& elem = mElements[e];
		// compute deformation gradient
		Matrix2R Ds = ComputeShapeMatrix(e);
		F = Ds * !elem.jacobian;
	}

	void FEMPhysics2D::ComputeExtents(SVector& extents) const
	{
		extents.resize(GetNumElements());
		for (uint32 e = 0; e < GetNumElements(); e++)
		{
			extents[e] = ComputeExtent(e);
		}
	}

	real FEMPhysics2D::ComputeEnergy() const
	{
		real val = ElasticEnergy2D::ComputeEnergy(this);
		// add the gravitational and kinetic part
		real invHSqr = mTimeStep != 0 ? 0.5f / mTimeStep / mTimeStep : 0;		
		MVector loads(GetNumFreeNodes());
		ComputeBodyForces(loads);

		for (uint32 i = 0; i < GetNumFreeNodes(); i++)
		{
			val -= mForceFraction * loads[i].y * GetFreeNodePos(i).y; // gravitational
			real mass = 1.f / GetFreeNodeInvMass(i);
			val -= invHSqr * mass * (GetFreeNodePos(i) - GetFreeNodePos0(i)).LengthSquared(); // kinetic
		}
		return val;// -mBaseEnergy;
	}

	// compute the contribution of gravity (as a force distribution) to the current forces (using BB shape functions)
	void FEMPhysics2D::ComputeBodyForces(MVector& f) const
	{
		// TODO: cache the body forces
		Vector2R flocal = mDensity * mConfig.mGravity * real(1.0 / NUM_ELEMENT_NODES);
		for (uint32 e = 0; e < GetNumElements(); e++)
		{
			// We note that the body force is constant across the element.
			Vector2R bforce = GetElementInitialExtent(e) * flocal;
			for (uint32 i = 0; i < GetNumLocalNodes(); i++)
			{
				uint32 globalI = GetGlobalIndex(e, i);
				if (globalI < mNumBCs)
					continue;
				f[globalI - mNumBCs] += bforce;
			}
		}
	}

	void ComputeLocalMassMatrixLumped(real density, uint32 numLocalNodes, EigenMatrix& Mlocal)
	{
		real massDiv = density / NUM_ELEMENT_NODES;
		for (uint32 i = 0; i < numLocalNodes * NUM_POS_COMPONENTS; i++)
		{
			Mlocal(i, i) = massDiv;
		}
	}

	// compute local mass matrix (using linear shape functions)
	void ComputeLocalMassMatrix(real density, uint32 numLocalNodes, EigenMatrix& Mlocal)
	{
		real massDiv = density / NUM_DIMENSIONS / (NUM_DIMENSIONS + 1);
		for (size_t i = 0; i < numLocalNodes; i++)
		{
			size_t offsetI = i * NUM_POS_COMPONENTS;
			for (size_t j = 0; j < numLocalNodes; j++)
			{
				size_t offsetJ = j * NUM_POS_COMPONENTS;
				// build a diagonal matrix
				for (size_t k = 0; k < NUM_POS_COMPONENTS; k++)
					Mlocal(offsetI + k, offsetJ + k) = (i == j) ? 2 * massDiv : massDiv;
			}
		}
	}

	// assemble the global mass matrix 
	void FEMPhysics2D::AssembleMassMatrix()
	{
		// compute the local mass matrix first
		uint32 numLocalNodes = GetNumLocalNodes();
		uint32 numLocalDofs = NUM_POS_COMPONENTS * numLocalNodes;

		EigenMatrix Mlocal(numLocalDofs, numLocalDofs);
		Mlocal.setZero();
		if (mUseLumpedMass)
			ComputeLocalMassMatrixLumped(mDensity, numLocalNodes, Mlocal);
		else
			ComputeLocalMassMatrix(mDensity, numLocalNodes, Mlocal);
			//ComputeLocalMassMatrixBB(mDensity, numLocalNodes, Mlocal);

		uint32 numNodes = GetNumFreeNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		EigenMatrix M(numDofs, numDofs);
		M.setZero();
		for (uint32 i = 0; i < GetNumElements(); i++)
		{
			real vol = GetElementInitialExtent(i);
			// for every local node there corresponds a 3x3 block matrix
			// we go through all of these blocks so that we know what the current pair is
			for (uint32 j = 0; j < numLocalNodes; j++)
			{
				for (uint32 k = 0; k < numLocalNodes; k++)
				{
					uint32 jGlobal = GetGlobalIndex(i, j);
					uint32 kGlobal = GetGlobalIndex(i, k);
					// do not add to the global matrix if at least one of the nodes is fixed
					if (jGlobal < mNumBCs || kGlobal < mNumBCs)
						continue;
					// add the local 3x3 matrix to the global matrix
					int jOffset = (jGlobal - mNumBCs) * NUM_POS_COMPONENTS;
					int kOffset = (kGlobal - mNumBCs) * NUM_POS_COMPONENTS;
					for (uint32 x = 0; x < NUM_POS_COMPONENTS; x++)
					{
						for (uint32 y = 0; y < NUM_POS_COMPONENTS; y++)
						{
							M.coeffRef(jOffset + x, kOffset + y) += vol * Mlocal(j * NUM_POS_COMPONENTS + x, k * NUM_POS_COMPONENTS + y);
						}
					}
				}
			}
		}

		mMassMatrix = M.sparseView();
	}

} // namespace MBD