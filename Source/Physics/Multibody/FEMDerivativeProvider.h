#ifndef FEM_DERIVATIVE_PROVIDER_H
#define FEM_DERIVATIVE_PROVIDER_H

#include "MultibodyCommon.h"

namespace MBD
{
	class FEMPhysics2D;

	class FEMDerivativeProvider
	{
	public:
		FEMDerivativeProvider(FEMPhysics2D& fem) : mFEM(fem) {}
		void ComputeGradients(MVector& grad);
		void ComputeFiniteDiffGradient(MVector& grad);
		void ComputeForceDifferential(const Vector2Array& dx, Vector2Array& df) const;

	private:
		FEMPhysics2D& mFEM;
	};
}

#endif //FEM_DERIVATIVE_PROVIDER_H
