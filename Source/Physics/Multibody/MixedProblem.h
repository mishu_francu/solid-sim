#ifndef MIXED_PROBLEM
#define MIXED_PROBLEM

#include "BaseMixedProblem.h"

namespace MBD
{
	class MixedProblem : public BaseProblem
	{
	public:
		enum SolverType
		{
			SPP_ST_DIRECT,
			SPP_ST_ITERATIVE,
			SPP_ST_SCHUR_COMPLIANCE,
			SPP_ST_SCHUR_MASS,
		};
	public:
		MixedProblem(FEMMixedSolver& source, int solverType);
		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);
		EigenVector SolveLinearSystem(const EigenMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y);
		real MeritResidual(const EigenVector& rhs) const;

	private:
		int mSolverType = SPP_ST_DIRECT;
		EigenMatrix mKinv; // inverse stiffness matrix
	};
}

#endif // MIXED_PROBLEM
