#pragma once

#include "BaseMixedProblem.h"

namespace MBD
{
	class TwoFieldProblem : public BaseProblem
	{
	public:
		TwoFieldProblem(FEMMixedSolver& source);
		uint32 GetSize() override;
		uint32 GetNumConstraints() override;
		real ComputeObjective() override;
		void ComputeGradients(const SVector& x, SVector& grad) override;

		EigenVector ComputeRhs(const EigenVector& solution);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);

		EigenVector ComputeExtents();
		EigenVector EvaluateConstraints();
		EigenMatrix ComputeConstraintGradient();

	private:
		EigenMatrix mCinv; // inverse compliance matrix
		EigenVector mPrevExtents;
	};

}