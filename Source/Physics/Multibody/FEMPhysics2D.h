#ifndef FEM_PHYSICS_2D_H
#define FEM_PHYSICS_2D_H

#include "MultibodyCommon.h"
#include <Engine/Types.h>

namespace MBD
{
	struct FEMMesh
	{
		struct Node
		{
			real invMass = 1;
			Vector2R pos;
			Vector2R pos0;
			Vector2R vel;
		};

		struct Element
		{
			uint32 idx[NUM_ELEMENT_NODES];
			real extent; // area or volume
			Matrix2R jacobian; // barycentric jacobian matrix
		};
	
	public:
		std::vector<Node> mNodes;
		std::vector<Element> mElements;
	};


	class FEMPhysics2D
	{
	public:
		struct Config
		{
			int mMaterial = MMT_STVK;
			int mSimType = ST_EXPLICIT;
			real mYoungsModulus = 100000;
			real mPoissonRatio = (real)0.4;
			int mSolverType = IST_NONLINEAR;
			Vector2R mGravity = Vector2R(0, 100);
		};

	public:
		FEMPhysics2D();
		FEMPhysics2D(const Config& config);
		void Init(const FEMMesh& mesh);
		void Step(real dt);
		
		real GetYoungsModulus() const { return mConfig.mYoungsModulus; }
		real GetPoissonRatio() const { return mConfig.mPoissonRatio; }
		real GetShearModulus() const { return real(0.5 * GetYoungsModulus() / (1.0 + GetPoissonRatio())); }
		real GetLameLambda() const { return real(GetYoungsModulus() * GetPoissonRatio() / (1.0 + GetPoissonRatio()) / (1.0 - 2.0 * GetPoissonRatio())); }
		real GetInvBulkModulus() const { return real((1.0 - 2.0 * GetPoissonRatio()) * 3.0 / GetYoungsModulus()); }
		int GetMaterial() const { return mConfig.mMaterial; }
		real GetTimeStep() const { return mTimeStep; }
		int GetSimType() const { return mConfig.mSimType; }
		int GetSolverType() const { return mConfig.mSolverType; }
		real GetForceFraction() const { return mForceFraction; }

		void SetMaterial(int mat) { mConfig.mMaterial = mat; }

		uint32 GetNumNodes() const { return (uint32)mDeformedPositions.size(); }
		uint32 GetNumFreeNodes() const { return GetNumNodes() - mNumBCs; }
		uint32 GetNumElements() const { return (uint32)mElements.size(); }
		uint32 GetGlobalIndex(uint32 e, uint32 i) const { return mElements[e].idx[i]; }
		uint32 GetNumLocalNodes() const { return NUM_ELEMENT_NODES; }
		uint32 GetNumBCs() const { return mNumBCs; }
		real GetElementInitialExtent(uint32 e) const { return mElements[e].extent; }
		const Matrix2R& GetBarycentricJacobianMatrix(uint32 e) const { return mElements[e].jacobian; }
		const SparseMatrix& GetMassMatrix() const { return mMassMatrix; }

		const Vector2R& GetNodePos(uint32 i) const { return mDeformedPositions[i]; }
		Vector2R& GetNodePos(uint32 i) { return mDeformedPositions[i]; }
		const Vector2R& GetNodeVel(uint32 i) const { return mVelocities[i]; }
		Vector2R& GetNodeVel(uint32 i) { return mVelocities[i]; }
		real GetNodeInvMass(uint32 i) const { return mInvMasses[i]; }
		EigenMap GetDeformedPositions() { return mDeformedPositions.ToEigen(); }
		EigenMap GetPreviousPositions() { return mPreviousPositions.ToEigen(); }

		const Vector2R& GetFreeNodePos(uint32 i) const { return mDeformedPositions[i + mNumBCs]; } // TODO: GetDeformedPos
		Vector2R& GetFreeNodePos(uint32 i) { return mDeformedPositions[i + mNumBCs]; } // TODO: GetDeformedPos
		const Vector2R& GetFreeNodePos0(uint32 i) const { return mPreviousPositions[i + mNumBCs]; } // TODO: GetPreviousPos
		Vector2R& GetFreeNodeVel(uint32 i) { return mVelocities[i + mNumBCs]; }
		real GetFreeNodeInvMass(uint32 i) const { return mInvMasses[i + mNumBCs]; }
		real GetFreeNodeMass(uint32 i) const { return 1 / mInvMasses[i + mNumBCs]; }

		Matrix2R ComputeShapeMatrix(uint32 e) const;
		real ComputeExtent(uint32 e) const;
		void ComputeExtents(SVector& extents) const;
		void ComputeDeformationGradient(uint32 e, Matrix2R& F) const;		
		void ComputeBodyForces(MVector& f) const;

		void SubStepExplicit(real dt);
		real ComputeEnergy() const;
		
		void AssembleMassMatrix();

		// mixed stuff
		real GetPressure(uint32 i) const { return mPressures[i]; }
		real& GetPressure(uint32 i) { return mPressures[i]; }
		SVector& GetPressures() { return mPressures; }
		real GetTotalInitialExtent() const { return mTotalInitialExtent; }
		SVector& GetExtents() { return mExtents; }
		real& GetExtent(uint32 i) { return mExtents[i]; }

	private:

		// config
		Config mConfig;

		uint32 mNumSteps;
		real mTimeStep;
		uint32 mNumBCs;
		real mForceFraction;
		real mForceStep;
		real mDensity;
		//real mBaseEnergy;
		bool mUseLumpedMass;

		// mesh
		//MVector mReferencePositions;
		std::vector<FEMMesh::Element> mElements;

		// state variables
		MVector mDeformedPositions;
		MVector mPreviousPositions;
		MVector mVelocities;

		// mixed state vars
		SVector mPressures;
		SVector mExtents; // used by 2/3-field
		real mTotalInitialExtent;

		// mass info
		SparseMatrix mMassMatrix;
		std::vector<real> mInvMasses;

		// forces
		MVector mForces;
		MVector mExtForces;

		friend class FEMDerivativeProvider; // TODO: remove
		friend class FEMLinearSolver; // TODO: remove
		friend class FEMTests;
	};
}

#endif // FEM_PHYSICS_2D_H
