#ifndef FEM_MIXED_SOLVER
#define FEM_MIXED_SOLVER

#include "MultibodyCommon.h"

namespace MBD
{
	class FEMPhysics2D;

	class FEMMixedSolver
	{
	public:
		FEMMixedSolver(FEMPhysics2D& fem);
		void Solve();

		void SolveSaddlePoint();
		void SolveThreeField();
		void SolveTwoField();
		void SolveTwoFieldADMM();
		void SolveTwoFieldNLopt();
		void SolveNLopt();

		// sub-solvers for 2 field ADMM
		void SolveDeviatoric(const EigenVector& u, real rho);
		void SolveVolumetric(const EigenVector& u, real rho);
		
		void AssembleComplianceMatrix();
		void AssembleJacobianMatrix();
		void ComputeLocalJacobian(uint32 e, Vector2R v[]) const;
		EigenVector ComputeStdRhs();
		EigenVector ComputePosRhs();
		real ComputeExtentErrors(EigenVector& errors);
		EigenMatrix ComputeStiffnessMatrix();
		void AssembleGeometricStiffnessMatrix(const EigenVector& p, EigenMatrix& Kgeom) const;
		
		uint32 GetNumPressureNodes() const;
		uint32 GetNumLocalPressureNodes() const { return NUM_ELEMENT_NODES; }
		uint32 GetPressureGlobalIndex(uint32 e, uint32 i) const;
		SparseMatrix& GetVolComplianceMatrix() { return mVolComplianceMatrix; }
		EigenMatrix& GetVolJacobianMatrix() { return mVolJacobianMatrix; }
		FEMPhysics2D& GetFEM() { return mFEM; }

	private:
		SparseMatrix mVolComplianceMatrix;
		EigenMatrix mVolJacobianMatrix;
		EigenMatrix mGeometricStiffnessMatrix;
		EigenMatrix mDeviatoricStiffnessMatrix;

		uint32 mOuterIterations = 10;
		real mAbsNewtonResidualThreshold = 1;

		FEMPhysics2D& mFEM;
	};
}

#endif // FEM_MIXED_SOLVER