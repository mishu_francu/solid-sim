#include "FEMTests.h"
#include "FEMPhysics2D.h"
#include "ElasticEnergy2D.h"
#include "FEMLinearSolver.h"
#include "FEMDerivativeProvider.h"
#include "FEMMixedSolver.h"
#include <Engine/Utils.h>

namespace MBD
{
	void FEMTests::TestStrain()
	{
		// initialized a single FEM triangle
		std::vector<FEMMesh::Node> nodes(3);
		std::vector<FEMMesh::Element> elems(1);

		nodes[0].pos.Set(0, 0); // 0
		nodes[1].pos.Set(100, 0); // 1
		nodes[2].pos.Set(0, 100); // 2

		nodes[0].invMass = 0;
		nodes[1].invMass = 0;

		elems[0].idx[0] = 0;
		elems[0].idx[1] = 1;
		elems[0].idx[2] = 2;

		FEMPhysics2D::Config config;
		config.mMaterial = MMT_LINEAR;

		FEMMesh mesh;
		mesh.mNodes = nodes;
		mesh.mElements = elems;

		FEMPhysics2D fem(config);
		fem.Init(mesh);

		// add some displacement
		real delta = 10;
		fem.GetNodePos(2).y += delta;

		// 1. using deformation gradient
		Matrix2R F, id;
		fem.ComputeDeformationGradient(0, F);
		Matrix2R strain = real(0.5) * (F + !F) - id; // Cauchy strain
		Printf("strain1: %g %g %g\n", strain.a11, strain.a22, strain.a12);

		// 2. using strain jacobian
		Matrix2R Bn[NUM_ELEMENT_NODES];
		Vector2R Bs[NUM_ELEMENT_NODES];
		FEMLinearSolver solver(fem);
		solver.ComputeStrainJacobian(0, Bn, Bs);
		Vector2R disp(0, delta);
		Vector2R epsN = Bn[2] * disp;
		real epsS = Bs[2].Dot(disp);
		Printf("strain2: %g %g %g\n", epsN.x, epsN.y, epsS);
	}

	void FEMTests::TestForce()
	{
		// initialized a single FEM triangle
		std::vector<FEMMesh::Node> nodes(3);
		std::vector<FEMMesh::Element> elems(1);

		nodes[0].pos.Set(0, 0); // 0
		nodes[1].pos.Set(100, 0); // 1
		nodes[2].pos.Set(0, 100); // 2

		nodes[0].invMass = 0;
		nodes[1].invMass = 0;

		elems[0].idx[0] = 0;
		elems[0].idx[1] = 1;
		elems[0].idx[2] = 2;

		FEMPhysics2D::Config config;
		config.mMaterial = MMT_LINEAR;

		FEMMesh mesh;
		mesh.mNodes = nodes;
		mesh.mElements = elems;

		FEMPhysics2D fem(config);
		fem.Init(mesh);

		// add some displacement
		real delta = 10;
		fem.GetNodePos(2).y += delta;

		// 1. using stresses
		MVector f1(3);
		ElasticEnergy2D::ComputeForces(&fem, f1);
		Printf("f1: %g, %g\n", f1[2].x, f1[2].y);

		// 2. using finite differences
		Vector2Array f2(1);
		ComputeFiniteDiffForces(fem, f2);
		Printf("f2: %g, %g\n", -f2[0].x, -f2[0].y);

		// 3. using stiffness matrix
		EigenMatrix K;
		ElasticEnergy2D::AssembleStiffnessMatrix(&fem, K);
		EigenVector disp(2);
		disp(0) = 0;
		disp(1) = delta;
		EigenVector f3 = K * disp;
		Printf("f3: %g, %g\n", -f3(0), -f3(1));

		// 4. using linear stiffness matrix
		FEMLinearSolver solver(fem);
		solver.AssembleStiffnessMatrix();
		EigenVector f4 = solver.mStiffnessMatrix * disp;
		Printf("f4: %g, %g\n", -f4(0), -f4(1));
	}

	void FEMTests::ComputeFiniteDiffForces(FEMPhysics2D& fem, std::vector<Vector2R>& f)
	{
		real E0 = ElasticEnergy2D::ComputeEnergy(&fem);
		const real eps = (real)1e-8;
		for (uint32 i = 0; i < fem.GetNumFreeNodes(); i++)
		{
			// perturb along x
			fem.GetFreeNodePos(i).x += eps;
			real E = ElasticEnergy2D::ComputeEnergy(&fem);
			fem.GetFreeNodePos(i).x -= eps;
			real dx = (E - E0) / eps;
			f[i].x = dx;

			// perturb along y
			fem.GetFreeNodePos(i).y += eps;
			E = ElasticEnergy2D::ComputeEnergy(&fem);
			fem.GetFreeNodePos(i).y -= eps;
			real dy = (E - E0) / eps;
			f[i].y = dy;
		}
	}

	void FEMTests::TestForceDifferential()
	{
		// initialize a single FEM triangle
		std::vector<FEMMesh::Node> nodes(3);
		std::vector<FEMMesh::Element> elems(1);

		nodes[0].pos.Set(0, 0); // 0
		nodes[1].pos.Set(0, 100); // 1
		nodes[2].pos.Set(100, 0); // 2

		elems[0].idx[0] = 0;
		elems[0].idx[1] = 2;
		elems[0].idx[2] = 1;

		FEMPhysics2D::Config config;
		config.mMaterial = MMT_LINEAR;

		FEMMesh mesh;
		mesh.mNodes = nodes;
		mesh.mElements = elems;

		FEMPhysics2D fem(config);
		fem.Init(mesh);

		// check force differential
		// 1. ComputeForceDifferential
		uint32 numNodes = fem.GetNumFreeNodes();
		Vector2Array df(numNodes);
		Vector2Array dx(numNodes);
		std::fill(dx.begin(), dx.end(), Vector2R(0, 0));
		dx[0].x = 1;
		//dx[0].y = 1;
		FEMDerivativeProvider fdp(fem);
		fdp.ComputeForceDifferential(dx, df);
		Printf("df1: %g %g\n", df[0].x, df[0].y);

		// 2. ElasticEnergy2D::ComputeLocalForceDifferential
		Vector2R dx2[NUM_ELEMENT_NODES];
		Vector2R df2[NUM_ELEMENT_NODES];
		dx2[0].x = 1;
		//dx2[0].y = 1;
		ElasticEnergy2D::ComputeLocalForceDifferential(&fem, 0, dx2, df2);
		Printf("df2: %g %g\n", df2[0].x, df2[0].y);

		// 3. stiffness matrix
		FEMLinearSolver solver(fem);
		solver.AssembleStiffnessMatrix();
		uint32 size = numNodes * NUM_POS_COMPONENTS;
		EigenVector dx3(size);
		dx3.setZero();
		dx3(0) = 1;
		//dx3(1) = 1;
		EigenVector df3 = -solver.mStiffnessMatrix * dx3;
		Printf("df3: %g %g\n", df3(0), df3(1));
	}

	void FEMTests::TestVolJacobian()
	{
		// initialize a single FEM triangle
		std::vector<FEMMesh::Node> nodes(3);
		std::vector<FEMMesh::Element> elems(1);

		nodes[0].pos.Set(0, 0); // 0
		nodes[1].pos.Set(100, 0); // 1
		nodes[2].pos.Set(0, 1000); // 2

		elems[0].idx[0] = 0;
		elems[0].idx[1] = 1;
		elems[0].idx[2] = 2;

		FEMPhysics2D::Config config;
		config.mMaterial = MMT_NEO_HOOKEAN_OGDEN;

		FEMMesh mesh;
		mesh.mNodes = nodes;
		mesh.mElements = elems;

		FEMPhysics2D fem(config);
		fem.Init(mesh);

		FEMMixedSolver solver(fem);

		// compute per element values
		int idx = 2; // the node for which we compute the gradient
		real de;
		real eps = 1e-8;

		// the v's are rows in the Jacobian matrix
		Vector2R v[3];
		solver.ComputeLocalJacobian(0, v);
		Printf("v[%d].x=%g\n", idx, v[idx].x);
		Printf("v[%d].y=%g\n", idx, v[idx].y);

		real a = fem.ComputeExtent(0);

		fem.GetFreeNodePos(idx).x += eps;
		real a1 = fem.ComputeExtent(0);
		fem.GetFreeNodePos(idx).x -= eps;
		de = (a1 - a) / eps;
		Printf("da/dx[%d]=%g\n", idx, de);

		fem.GetFreeNodePos(idx).y += eps;
		a1 = fem.ComputeExtent(0);
		fem.GetFreeNodePos(idx).y -= eps;
		de = (a1 - a) / eps;
		Printf("da/dy[%d]=%g\n", idx, de);

		// compute per node values
		EigenVector errors(3);
		solver.ComputeExtentErrors(errors);

		// compute first component of the gradient of errors(0)
		EigenVector errors1(3);

		fem.GetFreeNodePos(idx).x += eps;
		solver.ComputeExtentErrors(errors1);
		fem.GetFreeNodePos(idx).x -= eps;
		de = (errors1(0) - errors(0)) / eps;
		Printf("de0/dx[%d]=%g\n", idx, de);

		fem.GetFreeNodePos(idx).y += eps;
		solver.ComputeExtentErrors(errors1);
		fem.GetFreeNodePos(idx).y -= eps;
		de = (errors1(0) - errors(0)) / eps;
		Printf("de0/dy[%d]=%g\n", idx, de);

		solver.AssembleJacobianMatrix();
		auto J = solver.GetVolJacobianMatrix();
		Printf("J(0, %d)=%g\n", idx * NUM_POS_COMPONENTS, J(0, idx * NUM_POS_COMPONENTS));
		Printf("J(0, %d)=%g\n", idx * NUM_POS_COMPONENTS + 1, J(0, idx * NUM_POS_COMPONENTS + 1));
	}
}