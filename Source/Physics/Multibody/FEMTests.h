#ifndef FEM_TESTS_H
#define FEM_TESTS_H

#include "MultibodyCommon.h"

namespace MBD
{
	class FEMPhysics2D;

	class FEMTests
	{
	public:
		void TestStrain();
		void TestForce();
		void TestForceDifferential();
		void TestVolJacobian();

	private:
		void ComputeFiniteDiffForces(FEMPhysics2D& fem, std::vector<Vector2R>& f);
	};
}

#endif // FEM_TESTS_H
