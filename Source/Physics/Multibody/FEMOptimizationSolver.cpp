#include "FEMOptimizationSolver.h"
#include "FEMPhysics2D.h"
#include "StandardProblem.h"
#include "Optimizers.h"

namespace MBD
{
	FEMOptimizationSolver::FEMOptimizationSolver(FEMPhysics2D& fem, int solverType) 
		: mFEM(fem), 
		mSolverType(solverType), 
		mProblem(new StandardProblem(fem))
	{
		if (mSolverType == NST_NEWTON)
		{
			mOuterIterations = 100;
			mOptimizer = std::make_unique<NewtonOptimizer>(mProblem.get(), mOuterIterations);
		}
		else if (mSolverType == NST_NEWTON_CG)
		{
			mOuterIterations = 3;
			mInnerIterations = 200;
			mOptimizer = std::make_unique<NewtonCGOptimizer>(mProblem.get(), mOuterIterations, mInnerIterations);
		}
		else if (mSolverType == NST_GRADIENT_DESCENT)
		{
			mOuterIterations = 100000;
			mOptimizer = std::make_unique<GradientDescentOptimizer>(mProblem.get(), mOuterIterations, 2e-7);
		}
		else if (mSolverType == NST_STEEPEST_DESCENT)
		{
			mOuterIterations = 1000;
			mOptimizer = std::make_unique<SteepestDescentOptimizer>(mProblem.get(), mOuterIterations);
		}
		else if (mSolverType == NST_NLOPT)
		{
#ifdef USE_NLOPT
			mOptimizer = std::make_unique<NLoptOptimizer>(mProblem.get(), false);
#endif
		}
	}

	FEMOptimizationSolver::~FEMOptimizationSolver()
	{
		// needed for pimpl
	}
	
	void FEMOptimizationSolver::Solve()
	{
		mOptimizer->Solve();

		if (mFEM.GetTimeStep() != 0 && mFEM.GetSimType() == ST_IMPLICIT)
		{
			// compute new velocities
			const real invH = 1.f / mFEM.GetTimeStep();
			for (uint32 i = 0; i < mFEM.GetNumFreeNodes(); i++)
			{
				mFEM.GetFreeNodeVel(i) = invH * (mFEM.GetFreeNodePos(i) - mFEM.GetFreeNodePos0(i));
			}
		}
	}
	
} // namespace MBD