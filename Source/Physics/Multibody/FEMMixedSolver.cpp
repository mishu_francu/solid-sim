#include "FEMMixedSolver.h"
#include "FEMPhysics2D.h"
#include "ElasticEnergy2D.h"
#include "MixedProblem.h"
#include "TwoFieldProblem.h"
#include "ThreeFieldProblem.h"
#include "FEMDerivativeProvider.h"
#include "StandardProblem.h"
#include "Optimizers.h"
#ifdef USE_NLOPT
#include <NLopt/nlopt.hpp>
#endif
#include <Physics/FEM/NewtonSolver.h>
#include <Math/IterativeSolvers.h>

namespace MBD
{
	FEMMixedSolver::FEMMixedSolver(FEMPhysics2D& fem) : mFEM(fem)
	{
		AssembleComplianceMatrix();

		// force the material to distortional
		mFEM.SetMaterial(MMT_DISTORTIONAL_NH);
	}

	void FEMMixedSolver::Solve()
	{
		SolveSaddlePoint();
		//SolveNLopt();
		//SolveThreeField();
		//SolveTwoFieldNLopt();
	}

	uint32 FEMMixedSolver::GetNumPressureNodes() const
	{
		return mFEM.GetNumNodes();
	}

	void FEMMixedSolver::SolveSaddlePoint()
	{
		uint32 numNodes = mFEM.GetNumFreeNodes();
		uint32 numPNodes = GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		NewtonSolverBackTrack<MixedProblem> solver;
		solver.mNumIterations = mOuterIterations;
		solver.mVerbose = VL_NONE;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mUseProblemSolver = true;
		solver.mUseBFGS = false;
		MixedProblem problem(*this, MixedProblem::SPP_ST_DIRECT);

		// prepare the initial guess with the current configuration
		EigenVector solution(size);
		solution.setZero(); // initialize Lagrange multipliers with zero
		for (uint32 i = 0; i < numNodes; i++)
		{
			const Vector2R& p = mFEM.GetFreeNodePos(i);
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
		}
		// warm-starting
		for (uint32 i = 0; i < numPNodes; i++)
		{
			solution(numDofs + i) = mFEM.GetPressure(i);
		}

		solver.Solve(problem, size, solution);

		// update deformed positions
		Vector2Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < numNodes; i++)
		{
			mFEM.GetFreeNodePos(i) = u[i];
		}

		// update pressure
		auto p = solution.tail(numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			mFEM.GetPressure(i) = p[i];
		}

		// compute new velocities
		if (mFEM.GetSimType() == ST_IMPLICIT)
		{
			real invH = 1.f / mFEM.GetTimeStep();
			for (uint32 i = 0; i < numNodes; i++)
			{
				mFEM.GetFreeNodeVel(i) = invH * (mFEM.GetFreeNodePos(i) - mFEM.GetFreeNodePos0(i));
			}
		}
	}

	void FEMMixedSolver::SolveThreeField()
	{
		uint32 numNodes = mFEM.GetNumFreeNodes();
		uint32 numPNodes = GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes * 2;

		NewtonSolverBackTrack<ThreeFieldProblem> solver;
		solver.mNumIterations = mOuterIterations;
		solver.mVerbose = VL_NONE;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mUseProblemSolver = true;
		solver.mUseBFGS = false;
		ThreeFieldProblem problem(*this);

		// prepare the initial guess with the current configuration
		EigenVector solution(size);
		solution.setZero(); // initialize Lagrange multipliers with zero
		for (uint32 i = 0; i < mFEM.GetNumFreeNodes(); i++)
		{
			const Vector2R& p = mFEM.GetFreeNodePos(i);
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
		}
		// warm-starting
		for (uint32 i = 0; i < numPNodes; i++)
		{
			solution(numDofs + i) = mFEM.GetPressure(i);
		}
		// volumes
		real factor = 0.25;
		EigenVector deformedVolumes(numPNodes);
		deformedVolumes.setZero();
		for (uint32 e = 0; e < mFEM.GetNumElements(); e++)
		{
			Matrix2R mat = mFEM.ComputeShapeMatrix(e);
			real ext = mat.Determinant() / 2;

			for (uint32 i = 0; i < GetNumLocalPressureNodes(); i++)
			{
				uint32 globalI = GetPressureGlobalIndex(e, i);
				deformedVolumes[globalI] += factor * ext;
			}
		}
		solution.tail(numPNodes) = deformedVolumes;

		solver.Solve(problem, size, solution);

		// update deformed positions
		Vector2Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < numNodes; i++)
		{
			mFEM.GetFreeNodePos(i) = u[i];
		}

		// update pressure
		auto p = solution.segment(numDofs, numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			mFEM.GetPressure(i) = p[i];
		}

		// update volumes
		mFEM.GetExtents() = SVector(solution.tail(numPNodes));

		// compute new velocities
		if (mFEM.GetSimType() == ST_IMPLICIT)
		{
			real invH = 1.f / mFEM.GetTimeStep();
			for (uint32 i = 0; i < numNodes; i++)
			{
				mFEM.GetFreeNodeVel(i) = invH * (mFEM.GetFreeNodePos(i) - mFEM.GetFreeNodePos0(i));
			}
		}
	}

	void FEMMixedSolver::SolveTwoField()
	{
		uint32 numNodes = mFEM.GetNumFreeNodes();
		uint32 numPNodes = GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		NewtonSolverBackTrack<TwoFieldProblem> solver;
		solver.mNumIterations = 100;// mOuterIterations;
		solver.mVerbose = VL_DETAILED;
		solver.mResidualThreshold = mAbsNewtonResidualThreshold;
		solver.mUseProblemSolver = false;
		solver.mUseBFGS = false;
		TwoFieldProblem problem(*this);

		// prepare the initial guess with the current configuration
		EigenVector solution(size);
		solution.setZero();
		for (uint32 i = 0; i < mFEM.GetNumFreeNodes(); i++)
		{
			const Vector2R& p = mFEM.GetFreeNodePos(i);
			solution(i * NUM_POS_COMPONENTS) = p.x;
			solution(i * NUM_POS_COMPONENTS + 1) = p.y;
		}
		// extents
		//real factor = 0.25;
		//EigenVector deformedVolumes(numPNodes);
		//deformedVolumes.setZero();
		//for (uint32 e = 0; e < mFEM.GetNumElements(); e++)
		//{
		//	Matrix2R mat = mFEM.ComputeShapeMatrix(e);
		//	real ext = mat.Determinant() / 2;

		//	for (uint32 i = 0; i < GetNumLocalPressureNodes(); i++)
		//	{
		//		uint32 globalI = GetPressureGlobalIndex(e, i);
		//		deformedVolumes[globalI] += factor * ext;
		//	}
		//}		
		solution.tail(numPNodes) = problem.ComputeExtents();

		solver.Solve(problem, size, solution);

		//// update deformed positions
		//Vector2Array u = GetStdVector(solution.head(numDofs));
		//for (uint32 i = 0; i < numNodes; i++)
		//{
		//	mFEM.GetFreeNodePos(i) = u[i];
		//}

		//// update volumes
		//mFEM.GetExtents() = SVector(solution.tail(numPNodes));

		// compute new velocities
		if (mFEM.GetSimType() == ST_IMPLICIT)
		{
			real invH = 1.f / mFEM.GetTimeStep();
			for (uint32 i = 0; i < numNodes; i++)
			{
				mFEM.GetFreeNodeVel(i) = invH * (mFEM.GetFreeNodePos(i) - mFEM.GetFreeNodePos0(i));
			}
		}
	}

	void FEMMixedSolver::AssembleJacobianMatrix()
	{
		uint32 numNodes = mFEM.GetNumFreeNodes();
		uint32 nDof = NUM_POS_COMPONENTS * numNodes; // position DOFs
		uint32 numLocalNodes = mFEM.GetNumLocalNodes();
		uint32 nLocalDofs = numLocalNodes * NUM_POS_COMPONENTS;

		uint32 numPressureLocalNodes = GetNumLocalPressureNodes();
		uint32 numPNodes = GetNumPressureNodes();

		EigenMatrix& J = mVolJacobianMatrix;
		J.resize(numPNodes, nDof);
		J.setZero();
		real factor = (real)0.25;
		int fixedCounter = 0;
		for (uint32 e = 0; e < mFEM.GetNumElements(); e++)
		{
			Vector2R v[NUM_ELEMENT_NODES];
			ComputeLocalJacobian(e, v);

			EigenMatrix Jlocal(1, nLocalDofs);
			Jlocal.setZero();
			{
				// build Jacobian matrix
				for (uint32 k = 0; k < numLocalNodes; k++)
				{
					for (int l = 0; l < NUM_POS_COMPONENTS; l++)
					{
						Jlocal(0, k * NUM_POS_COMPONENTS + l) = v[k][l];
					}
				}
			}

			// go through all local stress nodes
			for (uint32 j = 0; j < numPressureLocalNodes; j++)
			{
				uint32 globalJ = GetPressureGlobalIndex(e, j);
				// go through all local position nodes
				for (uint32 k = 0; k < mFEM.GetNumLocalNodes(); k++)
				{
					uint32 globalK = mFEM.GetGlobalIndex(e, k);
					int baseCol = (globalK - mFEM.GetNumBCs()) * NUM_POS_COMPONENTS;
					{
						for (int l = 0; l < NUM_POS_COMPONENTS; l++)
						{
							real val = factor * Jlocal(0, k * NUM_POS_COMPONENTS + l);
							if (globalK >= mFEM.GetNumBCs())
								J.coeffRef(globalJ, baseCol + l) += val;
						}
					}
				}
			}
		}
	}

	EigenVector FEMMixedSolver::ComputePosRhs()
	{
		uint32 numDofs = mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS;

		EigenVector rhs = ComputeStdRhs();

		// add current pressure forces
		rhs.head(numDofs) -= mVolJacobianMatrix.transpose() * mFEM.GetPressures().ToEigen();

		return rhs;
	}

	EigenVector FEMMixedSolver::ComputeStdRhs()
	{
		FEMDerivativeProvider fdp(mFEM);
		MVector grad;
		fdp.ComputeGradients(grad);
		return grad.ToEigen();
	}

	uint32 FEMMixedSolver::GetPressureGlobalIndex(uint32 e, uint32 i) const
	{
		return mFEM.GetGlobalIndex(e, i);
	}

	real FEMMixedSolver::ComputeExtentErrors(EigenVector& errors)
	{
		real totalVol = 0;
		//mTotalInitialExtent = 0;
		real totalErr = 0;
		real factor = (real)0.25;
		errors.setZero();
		for (uint32 e = 0; e < mFEM.GetNumElements(); e++)
		{
			Matrix2R Ds = mFEM.ComputeShapeMatrix(e);
			real extent = Ds.Determinant() / 2;
			real extent0 = mFEM.GetElementInitialExtent(e);
			//mTotalInitialExtent += extent0;
			real err = extent - extent0;
			totalVol += extent;
			totalErr += err;

			for (uint32 i = 0; i < GetNumLocalPressureNodes(); i++)
			{
				uint32 globalI = GetPressureGlobalIndex(e, i);
				errors[globalI] += factor * err;
			}
		}
		return totalErr / mFEM.GetTotalInitialExtent();
	}

	EigenMatrix FEMMixedSolver::ComputeStiffnessMatrix()
	{
		real invHSqr = 1.f / (mFEM.GetTimeStep() * mFEM.GetTimeStep());

		AssembleGeometricStiffnessMatrix(mFEM.GetPressures().ToEigen(), mGeometricStiffnessMatrix); // TODO: in 2D it's constant
		ElasticEnergy2D::AssembleStiffnessMatrix(&mFEM, mDeviatoricStiffnessMatrix);

		EigenMatrix K = mDeviatoricStiffnessMatrix + mGeometricStiffnessMatrix;
		if (mFEM.GetSimType() == ST_IMPLICIT)
			K += invHSqr * mFEM.GetMassMatrix();
		return K;
	}

	void FEMMixedSolver::AssembleGeometricStiffnessMatrix(const EigenVector& p, EigenMatrix& Kgeom) const
	{
		size_t numNodes = mFEM.GetNumFreeNodes();
		size_t numDofs = numNodes * NUM_POS_COMPONENTS;
		Kgeom.resize(numDofs, numDofs);
		Kgeom.setZero();

		uint32 numPressureLocalNodes = GetNumLocalPressureNodes();
		real factor = (real)0.25;
		uint32 numBCs = mFEM.GetNumBCs();

		Matrix2R K12(0, 1, -1, 1);
		Matrix2R K13(0, -1, 1, 1);
		Matrix2R K23(0, 1, -1, 0);

		// go through all linear elements (tetrahedra)
		Matrix2R Z = Matrix2R::Zero();
		const int localSize = NUM_ELEMENT_NODES * NUM_POS_COMPONENTS;
		Eigen::Matrix<real, localSize, localSize> Klocal;
		for (uint32 e = 0; e < mFEM.GetNumElements(); e++)
		{
			// assemble the block matrix
			Matrix2R K[NUM_ELEMENT_NODES][NUM_ELEMENT_NODES];
			K[0][0] = Z; K[0][1] = K12; K[0][2] = K13;
			K[1][0] = K12; K[1][1] = Z; K[1][2] = K23;
			K[2][0] = K13; K[2][1] = K23; K[2][2] = Z;

			// convert it to an EigenMatrix
			for (int i = 0; i < NUM_ELEMENT_NODES; i++)
			{
				for (int j = 0; j < NUM_ELEMENT_NODES; j++)
				{
					for (int x = 0; x < NUM_POS_COMPONENTS; x++)
					{
						for (int y = 0; y < NUM_POS_COMPONENTS; y++)
						{
							Klocal(i * NUM_POS_COMPONENTS + x, j * NUM_POS_COMPONENTS + y) = 0.5 * K[i][j](x, y);
						}
					}
				}
			}

			for (uint32 i = 0; i < numPressureLocalNodes; i++)
			{
				uint32 globalI = GetPressureGlobalIndex(e, i);
				// the local stiffness matrix Klocal is organized in blocks for each pair of nodes
				for (uint32 j = 0; j < mFEM.GetNumLocalNodes(); j++)
				{
					uint32 jGlobal = mFEM.GetGlobalIndex(e, j);
					for (uint32 k = 0; k < mFEM.GetNumLocalNodes(); k++)
					{
						size_t kGlobal = mFEM.GetGlobalIndex(e, k);
						if (jGlobal < numBCs || kGlobal < numBCs)
							continue;
						size_t jOffset = (jGlobal - numBCs) * NUM_POS_COMPONENTS;
						size_t kOffset = (kGlobal - numBCs) * NUM_POS_COMPONENTS;

						// add the the whole block to the block matrix
						for (size_t x = 0; x < NUM_POS_COMPONENTS; x++)
						{
							for (size_t y = 0; y < NUM_POS_COMPONENTS; y++)
							{
								real val = (factor / 2) * p[globalI] * Klocal(j * NUM_POS_COMPONENTS + x, k * NUM_POS_COMPONENTS + y);
								Kgeom.coeffRef(jOffset + x, kOffset + y) += val;
							}
						}
					}
				}
			}
		}
	}

	void FEMMixedSolver::ComputeLocalJacobian(uint32 e, Vector2R v[]) const
	{
		Matrix2R Ds = mFEM.ComputeShapeMatrix(e);

		// a(x) = 0.5 * det(Ds)
		// v[1] = (da/dx1, da/dy1)
		//v[1].Set(0.5 * Ds.a22, -0.5 * Ds.a12);
		v[1] = -0.5 * Ds(1).GetPerpendicular();

		// v[2] = (da/dx2, da/dy2)
		//v[2].Set(-0.5 * Ds.a21, 0.5 * Ds.a11);
		v[2] = 0.5 * Ds(0).GetPerpendicular();

		v[0] = -v[1] - v[2];
	}

	void FEMMixedSolver::AssembleComplianceMatrix()
	{
		real invBulkModulus = mFEM.GetInvBulkModulus();

		// the local compliance matrix template
		const uint32 numLocalPressureNodes = GetNumLocalPressureNodes();
		const uint32 numLocalPressureDofs = numLocalPressureNodes;
		EigenMatrix Clocal(numLocalPressureDofs, numLocalPressureDofs);
		Clocal.setZero();
		for (uint32 i = 0; i < numLocalPressureNodes; i++)
		{
			for (uint32 j = 0; j < numLocalPressureNodes; j++)
			{
				real factor = i == j ? 0.1 : 0.05;
				Clocal(i, j) = factor * invBulkModulus;
			}
		}

		// assemble the global compliance matrix 
		uint32 numPressureNodes = GetNumPressureNodes();
		mVolComplianceMatrix = SparseMatrix(numPressureNodes, numPressureNodes);
		mVolComplianceMatrix.setZero();
		for (uint32 e = 0; e < mFEM.GetNumElements(); e++)
		{
			real vol = mFEM.GetElementInitialExtent(e);
			for (uint32 i = 0; i < numLocalPressureNodes; i++)
			{
				uint32 globalI = GetPressureGlobalIndex(e, i);
				for (uint32 j = 0; j < numLocalPressureNodes; j++)
				{
					uint32 globalJ = GetPressureGlobalIndex(e, j);
					mVolComplianceMatrix.coeffRef(globalI, globalJ) += vol * Clocal(i, j);
				}
			}
		}
	}

	static double myfunc(const std::vector<double> &x, std::vector<double> &grad, void *my_func_data)
	{
		FEMPhysics2D* fem = (FEMPhysics2D*)my_func_data; // this is also casting away const-ness

		// update internal node positions so that the deformation gradient gets updated too
		uint32 numBCs = fem->GetNumNodes() - fem->GetNumFreeNodes();
		for (uint32 i = 0; i < fem->GetNumFreeNodes(); i++)
		{
			Vector2R delta(x[i * NUM_POS_COMPONENTS], x[i * NUM_POS_COMPONENTS + 1]);
			fem->GetFreeNodePos(i) = fem->GetFreeNodePos0(i) + delta;
		}

		if (!grad.empty())
		{
			MVector r(fem->GetNumFreeNodes());
			FEMDerivativeProvider fdp(*fem);
			fdp.ComputeGradients(r);
			for (size_t i = 0; i < r.size(); i++)
			{
				grad[i * NUM_POS_COMPONENTS] = -r[i].x;
				grad[i * NUM_POS_COMPONENTS + 1] = -r[i].y;
			}
		}
		return fem->ComputeEnergy();
	}

	struct ConstraintData
	{
		uint32 idx;
		FEMMixedSolver* mixed;
	};

	static double myconstr(const std::vector<double> &x, std::vector<double> &grad, void *data)
	{
		ConstraintData* cdata = (ConstraintData*)data;
		FEMMixedSolver* mixed = cdata->mixed;
		FEMPhysics2D* fem = &mixed->GetFEM();
		uint32 idx = cdata->idx;

		// update internal node positions
		uint32 numBCs = fem->GetNumNodes() - fem->GetNumFreeNodes();
		for (uint32 i = 0; i < fem->GetNumFreeNodes(); i++)
		{
			Vector2R delta(x[i * NUM_POS_COMPONENTS], x[i * NUM_POS_COMPONENTS + 1]);
			fem->GetFreeNodePos(i) = fem->GetFreeNodePos0(i) + delta;
		}

		EigenVector errors(fem->GetNumNodes());
		mixed->ComputeExtentErrors(errors);

		return errors(idx); // TODO: optimize
	}

	void FEMMixedSolver::SolveNLopt()
	{
#ifdef USE_NLOPT
		// create solver object
		uint32 size = mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS;
		nlopt::opt opt(nlopt::LN_COBYLA, size);
		opt.set_min_objective(myfunc, &mFEM);

		// add extent constraints
		std::vector<ConstraintData> data(mFEM.GetNumNodes());
		for (uint32 i = 0; i < mFEM.GetNumNodes(); i++)
		{
			data[i].idx = i;
			data[i].mixed = this;
			opt.add_equality_constraint(myconstr, &data[i]);
		}

		// run the solver
		opt.set_xtol_rel(1e-1);
		std::vector<real> x(size); // initialized to zero ?!
		real minf;
		try {
			nlopt::result result = opt.optimize(x, minf);
		}
		catch (std::exception &e) {
			Printf("nlopt failed: %s\n", e.what());
		}

		// add the result
		for (uint32 i = 0; i < mFEM.GetNumFreeNodes(); i++)
		{
			Vector2R delta(x[i * NUM_POS_COMPONENTS], x[i * NUM_POS_COMPONENTS + 1]);
			mFEM.GetFreeNodePos(i) = mFEM.GetFreeNodePos0(i) + delta;
		}
#endif
	}

#ifdef USE_NLOPT
	class NLoptConstrainedOptimizer : public NLoptOptimizer
	{
	public:
		struct ConstraintData
		{
			uint32 idx;
			NLoptConstrainedOptimizer* obj;
		};
	public:
		NLoptConstrainedOptimizer(IOptimizationProblem* problem) : NLoptOptimizer(problem, true) 
		{
			// add a constraint for every node
			uint32 numConstr = problem->GetNumConstraints();
			mData.resize(numConstr);
			for (uint32 i = 0; i < numConstr; i++)
			{				
				mData[i].idx = i;
				mData[i].obj = this;
				mOpt->add_equality_constraint(myconstr, &mData[i]);
			}
		}
	private:
		static double myconstr(const std::vector<double>& x, std::vector<double>& grad, void* data)
		{
			ConstraintData* cdata = (ConstraintData*)data;
			NLoptConstrainedOptimizer* obj = cdata->obj;
			TwoFieldProblem* problem = (TwoFieldProblem*)obj->mProblem;

			EigenVector errors = problem->EvaluateConstraints(); // TODO: optimize

			if (!grad.empty())
			{
				EigenMatrix J = problem->ComputeConstraintGradient(); // TODO: optimize
				auto row = J.row(cdata->idx);
				for (size_t i = 0; i < grad.size(); i++)
					grad[i] = row(i);
			}

			return errors(cdata->idx);
		}

	private:
		std::vector<ConstraintData> mData;
	};

	void FEMMixedSolver::SolveTwoFieldNLopt()
	{
		TwoFieldProblem problem(*this);
		NLoptConstrainedOptimizer solver(&problem);
		solver.Solve();
	}
#endif

	void FEMMixedSolver::SolveTwoFieldADMM()
	{
		real rho = 1;
		SVector extents;
		EigenVector u; // scaled Lagrange multipliers
		for (uint32 iter = 0; iter < mOuterIterations; iter++)
		{
			SolveDeviatoric(u, rho);
			//SolveVolumetric(u, rho);
			mFEM.ComputeExtents(extents);
			u = u + extents.ToEigen() - mFEM.GetExtents().ToEigen();
		}
	}

	class DeviatoricADMMProblem : public BaseProblem
	{
	public:
		DeviatoricADMMProblem(FEMMixedSolver& mixed, const EigenVector& u, real rho) 
			: BaseProblem(mixed)
			, mMultiplier(u)
			, mRho(rho) 
		{
		}

		void ComputeGradients(const SVector& delta, SVector& grad) override
		{
			// apply the current solution and compute the standard gradients
			StandardProblem::ComputeGradients(delta, grad);
			
			//SVector extents;
			//mFEM.ComputeExtents(extents);

			//EigenVector constr = extents.ToEigen() - mFEM.GetExtents().ToEigen() + u;

			//mMixed.AssembleJacobianMatrix();
			//r.ToEigen() += mRho * mMixed.GetVolJacobianMatrix() * constr;
		}

		real ComputeObjective() override
		{
			// compute the total energy bar the volumetric part
			//mFEM.SetMaterial(MMT_DISTORTIONAL_NH);
			real energy = mFEM.ComputeEnergy();

			// augment the Lagrangian
			SVector extents;
			mFEM.ComputeExtents(extents);

			EigenVector constr = extents.ToEigen() - mFEM.GetExtents().ToEigen() + mMultiplier;

			return energy + 0.5 * mRho * constr.squaredNorm();
		}

	private:
		const EigenVector& mMultiplier;
		real mRho;
	};

	void FEMMixedSolver::SolveDeviatoric(const EigenVector& u, real rho)
	{
#ifdef USE_NLOPT
		DeviatoricADMMProblem problem(*this, u, rho);
		NLoptOptimizer solver(&problem, true);
		solver.Solve();
#endif
	}

	class VolumetricADMMProblem : public StandardProblem
	{
	public:
		VolumetricADMMProblem(FEMPhysics2D& fem, real rho) : StandardProblem(fem), mRho(rho) {}

		void ComputeGradients(const SVector& delta, SVector& grad) override
		{
			// apply the current solution - delta is made up of delta nodal extents
			for (uint32 i = 0; i < mFEM.GetNumNodes(); i++)
			{
				mFEM.GetExtent(i) = delta[i];
			}

			// TODO: volumetric gradient

			//SVector extents;
			//mFEM.ComputeExtents(extents);

			//EigenVector constr = extents.ToEigen() - mFEM.GetExtents().ToEigen() + u;

			//r = -mRho * constr;
		}

	private:
		real mRho;
	};

	//void FEMMixedSolver::SolveVolumetric(const EigenVector& u, real rho)
	//{
	//	real alpha = 1e-7;

	//	VolumetricADMMProblem problem(mFEM, rho);
	//	EigenVector b(mFEM.GetNumNodes()); // allocation!
	//	for (uint32 iter = 0; iter < mOuterIterations; iter++)
	//	{
	//		problem.ComputeGradients(b, u);

	//		// perform the gradient descent step (very similar to explicit integration)
	//		for (uint32 i = 0; i < mFEM.GetNumNodes(); i++) // TODO: vectorize, omit the for
	//		{
	//			real delta = alpha * b[i];
	//			mFEM.GetExtent(i) += delta;
	//		}
	//	}
	//}

} // namespace MBD