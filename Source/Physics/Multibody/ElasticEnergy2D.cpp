#include "ElasticEnergy2D.h"
#include "FEMPhysics2D.h"
#include <Math/Matrix.h>

#include <iostream>

namespace MBD
{
	real ElasticEnergy2D::ComputeEnergy(const FEMPhysics2D* femPhysics, int material)
	{
		real totalEnergy = 0;
		if (material == -1)
			material = femPhysics->GetMaterial();

		// helper constants
		const Matrix2R id;
		const real mu = femPhysics->GetShearModulus();
		const real lambda = femPhysics->GetLameLambda();

		// go through all elements and add up nodal forces
		// TODO: OpenMP acceleration
		for (int e = 0; e < (int)femPhysics->GetNumElements(); e++)
		{
			// compute nodal forces for this element only
			Matrix2R F;
			femPhysics->ComputeDeformationGradient(e, F);

			//Matrix3R R, U;
			//if (material == MMT_COROTATIONAL)
			//{
			//	ComputePolarDecomposition(F, R, U);
			//	F = U; // this seems to be broken for the torus!
			//}

			// compute strain and strain rate as tensors
			Matrix2R strain;
			if (material == MMT_LINEAR ||
				material == MMT_COROTATIONAL)
			{
				strain = real(0.5) * (F + !F) - id; // Cauchy strain
			}
			else
			{
				strain = real(0.5) * (!F * F - id);	// Green strain	
			}

			real energy = 0;
			if (material == MMT_LINEAR ||
				material == MMT_COROTATIONAL ||
				material == MMT_STVK)
			{
				real trace = strain.Trace();
				real contraction = (strain * strain).Trace();
				energy = mu * contraction + real(0.5) * lambda * trace * trace;
			}
			else if (material == MMT_NEO_HOOKEAN)
			{
				real J = F.Determinant();
				Matrix2R C = !F * F;
				real I1 = C.Trace();
				real logJ = log(J);
				energy = 0.5f * mu * (I1 - NUM_DIMENSIONS) - mu * logJ + 0.5f * lambda * logJ * logJ;
			}
			else if (material == MMT_NEO_HOOKEAN_OGDEN)
			{
				real J = F.Determinant();
				Matrix2R C = !F * F;
				real I1 = C.Trace();
				real logJ = log(J);
				energy = 0.5f * mu * (I1 - NUM_DIMENSIONS) - mu * logJ + 0.5f * lambda * (J - 1) * (J - 1);
			}
			else if (material == MMT_DISTORTIONAL_NH)
			{
				real J = F.Determinant();
				Matrix2R C = !F * F;
				real I1 = C.Trace();
				real logJ = log(J);
				energy = 0.5f * mu * (I1 - 3) - mu * logJ;
			}

			totalEnergy += femPhysics->GetElementInitialExtent(e) * energy;
		}
		return totalEnergy;
	}

	void ElasticEnergy2D::ComputeForces(const FEMPhysics2D* femPhysics, MVector& fout, int material)
	{
		// go through all elements and add up nodal forces
		for (int e = 0; e < (int)femPhysics->GetNumElements(); e++)
		{
			Matrix2R piolae = ComputeElementStress(femPhysics, e, material);
			Matrix2R forces = -femPhysics->GetElementInitialExtent(e) * piolae * femPhysics->GetBarycentricJacobianMatrix(e); // material description as the volume is the undeformed one
			uint32 i[NUM_ELEMENT_NODES];
			for (int j = 0; j < NUM_ELEMENT_NODES; j++)
				i[j] = femPhysics->GetGlobalIndex(e, j);
			for (int j = 1; j < NUM_ELEMENT_NODES; j++)
			{
				Vector2R f = forces(j - 1); // the j-1 column of 'forces'
				fout[i[j]] += f;
				fout[i[0]] -= f;
			}
		}
	}

	Matrix2R ElasticEnergy2D::ComputeElementStress(const FEMPhysics2D* femPhysics, uint32 e, int material)
	{
		Matrix2R id;
		const real mu = femPhysics->GetShearModulus();
		const real lambda = femPhysics->GetLameLambda();
		if (material == -1)
			material = femPhysics->GetMaterial();

		// compute nodal forces for this element only
		Matrix2R F;
		femPhysics->ComputeDeformationGradient(e, F);

		//Matrix3R R, U;
		//if (material == MMT_COROTATIONAL)
		//{
		//	ComputePolarDecomposition(F, R, U);
		//	F = U; // this seems to be broken for the torus!
		//}

		// compute strain and strain rate as tensors
		Matrix2R strain;
		if (material == MMT_LINEAR ||
			material == MMT_COROTATIONAL)
		{
			strain = real(0.5) * (F + !F) - id; // Cauchy strain
		}
		else
		{
			strain = real(0.5) * (!F * F - id);	// Green strain	
		}

		// compute PK1 stress tensors [Sifakis]
		Matrix2R piolae;
		if (material == MMT_STVK)
		{
			// Green strain - StVK model
			piolae = F * (2 * mu * strain + lambda * strain.Trace() * id);
		}
		else if (material == MMT_LINEAR)
		{
			// Cauchy strain - linear elasticity
			piolae = 2 * mu * strain + lambda * strain.Trace() * id;
		}
		//else if (material == MMT_COROTATIONAL)
		//{
		//	piolae = R * (2 * mu * strain + lambda * strain.Trace() * id);
		//}
		else if (material == MMT_NEO_HOOKEAN)
		{
			// Neo-Hookean [Sifakis]
			Matrix2R Finv = F.GetInverse();
			Matrix2R Finvtr = !Finv;
			real J = F.Determinant();
			piolae = mu * (F - Finvtr) + lambda * log(J) * Finvtr;
		}
		else if (material == MMT_NEO_HOOKEAN_OGDEN)
		{
			// Neo-Hookean [Smith]->[Ogden]
			Matrix2R Finv = F.GetInverse();
			Matrix2R Finvtr = !Finv;
			real J = F.Determinant();
			piolae = mu * (F - Finvtr) + lambda * (J - 1) * J * Finvtr;
		}
		else if (material == MMT_DISTORTIONAL_NH)
		{
			// only the mu terms [Smith]->[Ogden]
			Matrix2R Finv = F.GetInverse();
			Matrix2R Finvtr = !Finv;
			piolae = mu * (F - Finvtr);
		}

		return piolae;
	}

	void ElasticEnergy2D::AssembleStiffnessMatrix(const FEMPhysics2D* femPhysics, EigenMatrix& stiffnessMatrix, EigenMatrix* bcStiffnessMatrix)
	{
		size_t numNodes = femPhysics->GetNumFreeNodes();
		size_t numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 numBCs = (uint32)(femPhysics->GetNumNodes() - numNodes);
		stiffnessMatrix.resize(numDofs, numDofs);
		stiffnessMatrix.setZero();
		if (bcStiffnessMatrix)
		{
			bcStiffnessMatrix->resize(numDofs, numBCs * NUM_POS_COMPONENTS);
			bcStiffnessMatrix->setZero();
		}
		// go through all linear elements (tetrahedra)
		for (uint32 i = 0; i < femPhysics->GetNumElements(); i++)
		{
			EigenMatrix Klocal;
			ComputeLocalStiffnessMatrixFromDifferential(femPhysics, i, Klocal);
			// the local stiffness matrix Klocal is organized in blocks for each pair of nodes
			for (uint32 j = 0; j < femPhysics->GetNumLocalNodes(); j++)
			{
				uint32 jGlobal = femPhysics->GetGlobalIndex(i, j);
				for (uint32 k = 0; k < femPhysics->GetNumLocalNodes(); k++)
				{
					uint32 kGlobal = femPhysics->GetGlobalIndex(i, k);
					if (jGlobal < numBCs)
						continue;
					int jOffset = (jGlobal - numBCs) * NUM_POS_COMPONENTS;
					int kOffset = (kGlobal - numBCs) * NUM_POS_COMPONENTS;

					// add the the whole block to the block matrix
					for (size_t x = 0; x < NUM_POS_COMPONENTS; x++)
					{
						for (size_t y = 0; y < NUM_POS_COMPONENTS; y++)
						{
							real val = Klocal(j * NUM_POS_COMPONENTS + x, k * NUM_POS_COMPONENTS + y);
							if (kGlobal < numBCs)
							{
								if (bcStiffnessMatrix)
									bcStiffnessMatrix->coeffRef(jOffset + x, kGlobal * NUM_POS_COMPONENTS + y) += val;
							}
							else
							{
								stiffnessMatrix.coeffRef(jOffset + x, kOffset + y) += val;
							}
						}
					}
				}
			}
		}
	}

	void ElasticEnergy2D::ComputeLocalStiffnessMatrixFromDifferential(const FEMPhysics2D* femPhysics, uint32 e, EigenMatrix& Klocal)
	{
		uint32 numNodes = NUM_ELEMENT_NODES; // local nodes
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;

		std::vector<Vector2R> df(numNodes);
		std::vector<Vector2R> basisI(numNodes);
		std::vector<real> basisJ(numDofs);
		Klocal.resize(numDofs, numDofs);

		// pre-compute inverse diagonal values
		for (size_t j = 0; j < numDofs; j++)
		{
			// prepare unit vector
			std::fill(basisJ.begin(), basisJ.end(), 0);
			basisJ[j] = 1;
			// compute A-dot product
			Vector2R* dx2 = (Vector2R*)basisJ.data();
			ComputeLocalForceDifferential(femPhysics, e, dx2, &df[0]);
			for (size_t i = 0; i < numDofs; i++)
			{
				// prepare unit vector
				std::fill(basisI.begin(), basisI.end(), Vector2R(0));
				basisI[i / NUM_POS_COMPONENTS][i % NUM_POS_COMPONENTS] = 1;
				real val = InnerProduct<Vector2R, real>(basisI, df);
				Klocal(i, j) = -val;
			}
		}
		std::cout << "Klocal nonlinear" << std::endl << Klocal << std::endl;
	}

	void ElasticEnergy2D::ComputeLocalForceDifferential(const FEMPhysics2D* femPhysics, uint32 e, const Vector2R dx[], Vector2R df[])
	{
		// compute deformation gradient increment dF
		Vector2R x[NUM_ELEMENT_NODES];
		for (uint32 j = 0; j < NUM_ELEMENT_NODES; j++)
			x[j] = dx[j];
		Vector2R d[NUM_ELEMENT_NODES - 1];
		for (uint32 j = 0; j < NUM_ELEMENT_NODES - 1; j++)
			d[j] = x[j + 1] - x[0];
		Matrix2R mat(d[0], d[1]);
		Matrix2R X = femPhysics->GetBarycentricJacobianMatrix(e);
		Matrix2R dF = mat * !X;

		Matrix2R F;
		femPhysics->ComputeDeformationGradient(e, F); // we don't need it for linear, but never mind

		Matrix2R dP, id;

		// Lame coefficients
		const real mu = femPhysics->GetShearModulus();
		real lambda = femPhysics->GetLameLambda();

		if (femPhysics->GetMaterial() == MMT_LINEAR)
		{
			// stress differential - linear elasticity
			dP = mu * (dF + !dF) + lambda * dF.Trace() * id;
		}
		//else if (femPhysics->GetMaterial() == MMT_COROTATIONAL)
		//{
		//	// corotational
		//	Matrix3R R, S;
		//	ComputePolarDecomposition(F, R, S);
		//	Matrix3R dS = !R * dF;
		//	dP = 2 * mu * dF + lambda * dS.Trace() * R;
		//	// nonlinear correction term - comment below to obtain the implicit corotational method in [Mueller]
		//	real trS = S.Trace();
		//	Matrix3R A = S - trS * id;
		//	Vector3R w(dS(1, 2) - dS(2, 1), dS(0, 2) - dS(2, 0), dS(0, 1) - dS(1, 0));
		//	Vector3R r = A.GetInverse() * w;
		//	Matrix3R X = Matrix3R::Skew(r);
		//	Matrix3R dR = R * X;
		//	dP = dP + (lambda * (trS - 3) - 2 * mu) * dR;
		//}
		else if (femPhysics->GetMaterial() == MMT_STVK)
		{
			Matrix2R E = real(0.5) * (!F * F - id);
			Matrix2R dE = real(0.5) * (!dF * F + !F * dF);
			dP = dF * (2 * mu * E + lambda * E.Trace() * id)
				+ F * (2 * mu * dE + lambda * dE.Trace() * id);
		}
		else if (femPhysics->GetMaterial() == MMT_NEO_HOOKEAN)
		{
			Matrix2R Finv = F.GetInverse();
			Matrix2R Finvtr = !Finv;
			real J = F.Determinant();
			dP = mu * dF + (mu - lambda * log(J)) * Finvtr * !dF * Finvtr + lambda * (Finv * dF).Trace() * Finvtr;
		}
		else if (femPhysics->GetMaterial() == MMT_NEO_HOOKEAN_OGDEN)
		{
			Matrix2R Finv = F.GetInverse();
			Matrix2R Finvtr = !Finv;
			real J = F.Determinant();
			dP = mu * dF + (mu - lambda * (J - 1)) * Finvtr * !dF * Finvtr + lambda * J * (Finv * dF).Trace() * Finvtr;
		}
		else if (femPhysics->GetMaterial() == MMT_DISTORTIONAL_NH)
		{
			// only the mu terms
			Matrix2R Finv = F.GetInverse();
			Matrix2R Finvtr = !Finv;
			dP = mu * dF + mu * Finvtr * !dF * Finvtr;
		}

		Matrix2R dH = -femPhysics->GetElementInitialExtent(e) * dP * X;

		Vector2R f3;
		for (int j = 1; j < NUM_ELEMENT_NODES; j++)
		{
			Vector2R f = dH(j - 1);
			df[j] = f;
			f3 -= f;
		}
		df[0] = f3;
	}
} // namespace MBD