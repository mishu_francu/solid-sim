#pragma once

#include "MultibodyCommon.h"

namespace nlopt
{
	class opt;
}

namespace MBD
{
	class IOptimizationProblem;

	class IOptimizer
	{
	public:
		IOptimizer(IOptimizationProblem* problem, uint32 numIterations) : mProblem(problem), mNumIterations(numIterations) {}
		virtual ~IOptimizer() {}
		virtual void Solve() = 0;

	protected:
		IOptimizationProblem* mProblem;
		uint32 mNumIterations;
	};

	class NewtonOptimizer : public IOptimizer
	{
	public:
		NewtonOptimizer(IOptimizationProblem* problem, uint32 numIterations) : IOptimizer(problem, numIterations) {}
		void Solve() override;
		real MeritResidual(const EigenVector& rhs) const { return rhs.norm(); }
		bool CheckForInversion() { return false; }
		EigenVector ComputeRhs(const EigenVector& sol);
		void ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K);
		EigenVector SolveLinearSystem(const EigenMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y) { return EigenVector(); }
	};

	class GradientDescentOptimizer : public IOptimizer
	{
	public:
		GradientDescentOptimizer(IOptimizationProblem* problem, uint32 numIterations, real alpha)
			: IOptimizer(problem, numIterations)
			, mAlpha(alpha)
		{
		}
		void Solve() override;
	private:
		real mAlpha;
	};

	class SteepestDescentOptimizer : public IOptimizer
	{
	public:
		SteepestDescentOptimizer(IOptimizationProblem* problem, uint32 numIterations) : IOptimizer(problem, numIterations) {}
		void Solve() override;
	};

	class NewtonCGOptimizer : public IOptimizer
	{
	public:
		NewtonCGOptimizer(IOptimizationProblem* problem, uint32 numIterations, uint32 numSubIterations) :
			IOptimizer(problem, numIterations), mSubIterations(numSubIterations) {}
		void Solve() override;
	private:
		uint32 mSubIterations;
	};

#ifdef USE_NLOPT
	class NLoptOptimizer : public IOptimizer
	{
	public:
		NLoptOptimizer(IOptimizationProblem* problem, bool derivativeFree);
		void Solve() override;
	private:
		static double myfunc(const std::vector<double>& x, std::vector<double>& grad, void* my_func_data);
	protected:
		std::unique_ptr<nlopt::opt> mOpt;
	};
#endif
} // namespace MBD
