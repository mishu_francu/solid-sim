#include "FEMDerivativeProvider.h"
#include "FEMPhysics2D.h"
#include "ElasticEnergy2D.h"

namespace MBD
{
	void FEMDerivativeProvider::ComputeGradients(MVector& r)
	{
		// make sure r has the right size
		r.resize(mFEM.GetNumFreeNodes());

		// compute negative gradient r = f(x, v) + M * g
		mFEM.mForces.setZero();
		ElasticEnergy2D::ComputeForces(&mFEM, mFEM.mForces);

		MVector loads(mFEM.GetNumFreeNodes());
		mFEM.ComputeBodyForces(loads);

		const real invH = mFEM.GetTimeStep() == 0 ? 0 : 1.f / mFEM.GetTimeStep();
		const real invHSqr = mFEM.mTimeStep == 0 ? 0 : 1.f / (mFEM.mTimeStep * mFEM.mTimeStep);
		for (uint32 i = 0; i < mFEM.GetNumFreeNodes(); i++)
		{
			real mass = mFEM.GetFreeNodeMass(i);
			r[i] = mFEM.mForces[i + mFEM.mNumBCs] + loads[i] + mFEM.mExtForces[i];
			if (mFEM.GetSimType() == ST_IMPLICIT || mFEM.GetSimType() == ST_EXPLICIT)
			{
				r[i] += mass * invH * mFEM.GetFreeNodeVel(i);
				r[i] -= mass * invHSqr * (mFEM.GetFreeNodePos(i) - mFEM.GetFreeNodePos0(i)); // the inertial term
			}
		}
	}

	void FEMDerivativeProvider::ComputeFiniteDiffGradient(MVector& grad)
	{
		grad.resize(mFEM.GetNumFreeNodes());
		real E0 = mFEM.ComputeEnergy();
		const real eps = (real)1e-8;
		for (uint32 i = 0; i < mFEM.GetNumFreeNodes(); i++)
		{
			// perturb along x
			mFEM.GetFreeNodePos(i).x += eps;
			real E = mFEM.ComputeEnergy();
			mFEM.GetFreeNodePos(i).x -= eps;
			real dx = (E - E0) / eps;
			grad[i].x = dx;

			// perturb along y
			mFEM.GetFreeNodePos(i).y += eps;
			E = mFEM.ComputeEnergy();
			mFEM.GetFreeNodePos(i).y -= eps;
			real dy = (E - E0) / eps;
			grad[i].y = dy;
		}
	}

	void FEMDerivativeProvider::ComputeForceDifferential(const Vector2Array& dx, Vector2Array& df) const
	{
		memset(&df[0], 0, df.size() * sizeof(Vector2R));

		// Lame coefficients
		const real mu = mFEM.GetShearModulus();
		const real lambda = mFEM.GetLameLambda();

		Matrix2R id;
		Vector2R zero;
		for (uint32 i = 0; i < mFEM.GetNumElements(); i++)
		{
			const FEMMesh::Element& elem = mFEM.mElements[i];
			Vector2R dxLocal[NUM_ELEMENT_NODES];
			Vector2R dfLocal[NUM_ELEMENT_NODES];
			for (uint32 j = 0; j < NUM_ELEMENT_NODES; j++)
				dxLocal[j] = elem.idx[j] < mFEM.mNumBCs ? zero : dx[elem.idx[j] - mFEM.mNumBCs]; // TODO: double check this!
			ElasticEnergy2D::ComputeLocalForceDifferential(&mFEM, i, dxLocal, dfLocal);

			for (int j = 0; j < NUM_ELEMENT_NODES; j++)
			{
				Vector2R f = dfLocal[j];
				if (elem.idx[j] >= mFEM.mNumBCs)
					df[elem.idx[j] - mFEM.mNumBCs] += f;
			}
		}
	}

} // namespace MBD