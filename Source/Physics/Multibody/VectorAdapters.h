#pragma once

namespace MBD
{
	inline Eigen::Map<EigenVector> GetEigenVector(Vector2Array& arr, uint32 start = 0)
	{
		return Eigen::Map<EigenVector>((real*)&arr[start], (arr.size() - start) * NUM_DIMENSIONS, 1);
	}

	inline Vector2Array GetStdVector(const EigenVector& vec)
	{
		size_t size = vec.size() / NUM_DIMENSIONS;
		Vector2R* ptr = (Vector2R*)vec.data();
		return Vector2Array(ptr, ptr + size);
	}

	// a wrapper class to treat a Vector2Array as an EigenVector
	class MVector : public Vector2Array
	{
	public:
		MVector() : Vector2Array() {}

		explicit MVector(size_type count) : Vector2Array(count) {}

		explicit MVector(const EigenVector& vec) : Vector2Array(GetStdVector(vec)) {} // this involves data copying - avoid!

		EigenMap ToEigen(uint32 start = 0)
		{
			return EigenMap((real*)&(*this)[start], (size() - start) * NUM_DIMENSIONS, 1);
		}

		EigenMapConst ToEigen(uint32 start = 0) const
		{
			return EigenMapConst((real*)&(*this)[start], (size() - start) * NUM_DIMENSIONS, 1);
		}

		operator EigenVector() { return ToEigen(); }

		Eigen::VectorBlock<EigenMap> tail(uint32 n)
		{
			return ToEigen().tail(n);
		}

		void setZero()
		{
			std::fill(begin(), end(), Vector2R(0, 0));
		}

		real& operator()(uint32 i) { return ToEigen()(i); } // TODO: use the data buffer directly
	};

	// TODO: replace return type by Eigen::CwiseBinaryOp<...>
	inline EigenVector operator +(Eigen::VectorBlock<EigenMap> a, MVector b)
	{
		return a + b.ToEigen();
	}

	inline EigenVector operator *(real s, const MVector& v)
	{
		return s * v.ToEigen();
	}

	inline std::vector<real> GetStdVectorS(const EigenVector& vec)
	{
		size_t size = vec.size();
		real* ptr = (real*)vec.data();
		return std::vector<real>(ptr, ptr + size);
	}

	inline Eigen::Map<EigenVector> GetEigenVectorS(std::vector<real>& arr, uint32 start = 0)
	{
		return Eigen::Map<EigenVector>((real*)&arr[start], (arr.size() - start), 1);
	}

	// a wrapper class to treat a std::vector<real> as an EigenVector
	class SVector : public std::vector<real>
	{
	public:
		SVector() : std::vector<real>() {}

		explicit SVector(const std::vector<real>& src) : std::vector<real>(src) {}

		explicit SVector(size_type count) : std::vector<real>(count) {}

		explicit SVector(const EigenVector& vec) : std::vector<real>(GetStdVectorS(vec)) {} // this involves data copying - avoid!

		explicit SVector(const MVector& src) : std::vector<real>((real*)(&src[0]), (real*)(&src[0] + src.size())) {}

		EigenMap ToEigen(uint32 start = 0)
		{
			return EigenMap((real*)&(*this)[start], (size() - start), 1);
		}

		EigenMapConst ToEigen(uint32 start = 0) const
		{
			return EigenMapConst((real*)&(*this)[start], (size() - start), 1);
		}

		operator EigenVector() { return ToEigen(); }

		Eigen::VectorBlock<EigenMap> tail(uint32 n)
		{
			return ToEigen().tail(n);
		}

		void setZero()
		{
			std::fill(begin(), end(), 0);
		}

		real& operator()(uint32 i) { return ToEigen()(i); } // TODO: use the data buffer directly

		EigenVector operator +=(const EigenVector& v)
		{
			return ToEigen() += v;
		}

	};

	inline EigenVector operator +(Eigen::VectorBlock<EigenMap> a, const SVector& b)
	{
		return a + b.ToEigen();
	}

	inline EigenVector operator *(real s, const SVector& v)
	{
		return s * v.ToEigen();
	}

	// a simple wrapper class to treat an EigenVector as a Vector2Array
	class RVector
	{
	public:
		RVector(const EigenVector& vec) : mVec(vec) {}
		Vector2R operator[](uint32 i) const { return Vector2R(mVec(i * NUM_POS_COMPONENTS), mVec(i * NUM_POS_COMPONENTS + 1)); }

	private:
		const EigenVector& mVec;
	};

} // namespace MBD