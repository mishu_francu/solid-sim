#include "ThreeFieldProblem.h"
#include "FEMPhysics2D.h"
#include "FEMMixedSolver.h"

namespace MBD
{
	void ThreeFieldProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& A)
	{
		FEMPhysics2D& fem = mMixed.GetFEM();
		uint32 numNodes = fem.GetNumFreeNodes();
		uint32 numPNodes = mMixed.GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + 2 * numPNodes;

		// the system matrix
		A.resize(size, size);
		A.setZero();

		EigenMatrix id;
		id.setIdentity(numPNodes, numPNodes);

		A.block(0, 0, numDofs, numDofs) = mMixed.ComputeStiffnessMatrix();
		A.block(0, numDofs, numDofs, numPNodes) = mMixed.GetVolJacobianMatrix().transpose();
		A.block(numDofs, numDofs, numPNodes, numPNodes) = -mMixed.GetVolComplianceMatrix();
		A.block(numDofs, numDofs + numPNodes, numPNodes, numPNodes) = id;
		A.block(numDofs + numPNodes, 0, numPNodes, numDofs) = mMixed.GetVolJacobianMatrix();
		A.block(numDofs + numPNodes, numDofs, numPNodes, numPNodes) = -id;
	}

	EigenVector ThreeFieldProblem::ComputeRhs(const EigenVector & solution)
	{
		FEMPhysics2D& fem = mMixed.GetFEM();
		uint32 numNodes = fem.GetNumFreeNodes();
		uint32 numPNodes = mMixed.GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes * 2;

		// update deformed positions
		MVector u(solution.head(numDofs));
		for (uint32 i = 0; i < fem.GetNumFreeNodes(); i++)
		{
			fem.GetFreeNodePos(i) = u[i];
		}

		// update pressure
		fem.GetPressures() = SVector(solution.segment(numDofs, numPNodes));

		// update extents
		fem.GetExtents() = SVector(solution.segment(numDofs + numPNodes, numPNodes));

		// assemble Jacobian matrix
		mMixed.AssembleJacobianMatrix();

		// assemble right hand side
		EigenVector rhs(size);
		rhs.setZero();

		rhs.head(numDofs) = mMixed.ComputePosRhs();

		real factor = 0.25;
		EigenVector initialVolumes(numPNodes);
		initialVolumes.setZero();
		EigenVector deformedVolumes(numPNodes);
		deformedVolumes.setZero();
		for (uint32 e = 0; e < fem.GetNumElements(); e++)
		{
			Matrix2R mat = fem.ComputeShapeMatrix(e);
			real ext = mat.Determinant() / 2;

			for (uint32 i = 0; i < mMixed.GetNumLocalPressureNodes(); i++)
			{
				uint32 globalI = mMixed.GetPressureGlobalIndex(e, i);
				initialVolumes[globalI] += factor * fem.GetElementInitialExtent(e);
				deformedVolumes[globalI] += factor * ext;
			}
		}

		// presssure components
		auto extents = fem.GetExtents().ToEigen();
		rhs.segment(numDofs, numPNodes) = -extents + initialVolumes + mMixed.GetVolComplianceMatrix() * fem.GetPressures().ToEigen();

		// volume components
		rhs.segment(numDofs + numPNodes, numPNodes) = -extents + deformedVolumes;

		return rhs;
	}

} // namespace MBD