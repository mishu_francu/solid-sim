#include "MixedProblem.h"
#include "FEMMixedSolver.h"
#include "FEMPhysics2D.h"
#include <Physics/FEM/LinearSolver.h>
#include <Engine/Utils.h>

namespace MBD
{
	MixedProblem::MixedProblem(FEMMixedSolver& source, int solverType) : BaseProblem(source), mSolverType(solverType)
	{
	}

	EigenVector MixedProblem::ComputeRhs(const EigenVector & solution)
	{
		FEMPhysics2D& fem = mFEM;
		uint32 numNodes = fem.GetNumFreeNodes();
		uint32 numPNodes = mMixed.GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		// update deformed positions
		Vector2Array u = GetStdVector(solution.head(numDofs));
		for (uint32 i = 0; i < numNodes; i++)
		{
			fem.GetFreeNodePos(i) = u[i];
		}

		// update pressure
		auto p = solution.segment(numDofs, numPNodes);
		for (uint32 i = 0; i < numPNodes; i++)
		{
			fem.GetPressure(i) = p[i];
		}

		// assemble Jacobian matrix
		mMixed.AssembleJacobianMatrix();

		// assemble right hand side
		EigenVector rhs(size);
		rhs.setZero();

		rhs.head(numDofs) = mMixed.ComputePosRhs();

		// pressure components
		EigenVector errors(numPNodes); // vector used to accumulate volume errors
		real totalErr = mMixed.ComputeExtentErrors(errors);
		rhs.segment(numDofs, numPNodes) = -errors + mMixed.GetVolComplianceMatrix() * p;

		return rhs;
	}

	real MixedProblem::MeritResidual(const EigenVector& rhs) const
	{
		uint32 numPNodes = mMixed.GetNumPressureNodes();
		real norm1 = rhs.tail(numPNodes).lpNorm<1>();
		return norm1 / mFEM.GetTotalInitialExtent() * 100;
	}

	void MixedProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& A)
	{
		FEMPhysics2D& fem = mFEM;
		uint32 numNodes = fem.GetNumFreeNodes();
		uint32 numPNodes = mMixed.GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		// the system matrix
		A.resize(size, size);
		A.setZero();

		A.block(0, 0, numDofs, numDofs) = mMixed.ComputeStiffnessMatrix();
		A.block(0, numDofs, numDofs, numPNodes) = mMixed.GetVolJacobianMatrix().transpose();
		A.block(numDofs, 0, numPNodes, numDofs) = mMixed.GetVolJacobianMatrix();
		A.block(numDofs, numDofs, numPNodes, numPNodes) = -mMixed.GetVolComplianceMatrix();
	}

	EigenVector MixedProblem::SolveLinearSystem(const EigenMatrix& A, const EigenVector& rhs, const EigenVector& s, const EigenVector& y)
	{
		if (mSolverType == SPP_ST_DIRECT)
		{
			LinearSolver solver;
			solver.Init(A, LST_LDLT_PARDISO);
			return solver.Solve(rhs);
		}

		ASSERT(false);
		return EigenVector();
	}

	BaseProblem::BaseProblem(FEMMixedSolver& source) : StandardProblem(source.GetFEM()), mMixed(source) {}

	EigenVector BaseProblem::SolveLinearSystem(const EigenMatrix& K, const EigenVector& rhs, const EigenVector& s, const EigenVector& y)
	{
		LinearSolver solver;
		solver.Init(K, LST_LU_PARDISO);
		return solver.Solve(rhs);
	}
} // namespace MBD