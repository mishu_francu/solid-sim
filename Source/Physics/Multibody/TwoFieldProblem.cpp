#include "TwoFieldProblem.h"
#include "FEMMixedSolver.h"
#include "FEMPhysics2D.h"

namespace MBD
{
	TwoFieldProblem::TwoFieldProblem(FEMMixedSolver& source) : BaseProblem(source)
	{
		EigenMatrix C(mMixed.GetVolComplianceMatrix());
		mCinv = C.inverse();

		mPrevExtents = ComputeExtents();
		mFEM.GetExtents() = SVector(mPrevExtents); // just allocate the size
	}

	void TwoFieldProblem::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& A)
	{
		FEMPhysics2D& fem = mMixed.GetFEM();
		uint32 numNodes = fem.GetNumFreeNodes();
		uint32 numPNodes = mMixed.GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		// the system matrix
		A.resize(size, size);
		A.setZero();

		EigenMatrix id;
		id.setIdentity(numPNodes, numPNodes);

		A.block(0, 0, numDofs, numDofs) = mMixed.ComputeStiffnessMatrix();
		A.block(0, numDofs, numDofs, numPNodes) = mMixed.GetVolJacobianMatrix().transpose() * mCinv;
		A.block(numDofs, 0, numPNodes, numDofs) = mMixed.GetVolJacobianMatrix();
		A.block(numDofs, numDofs, numPNodes, numPNodes) = -id;
	}

	EigenVector TwoFieldProblem::ComputeRhs(const EigenVector& solution)
	{
		FEMPhysics2D& fem = mMixed.GetFEM();
		uint32 numNodes = fem.GetNumFreeNodes();
		uint32 numPNodes = mMixed.GetNumPressureNodes();
		uint32 numDofs = numNodes * NUM_POS_COMPONENTS;
		uint32 size = numDofs + numPNodes;

		// update deformed positions
		MVector u(solution.head(numDofs));
		for (uint32 i = 0; i < fem.GetNumFreeNodes(); i++)
		{
			fem.GetFreeNodePos(i) = u[i];
		}

		// update extents
		//fem.GetExtents() = SVector(solution.tail(numPNodes));
		for (uint32 i = 0; i < fem.GetNumNodes(); i++)
		{
			fem.GetExtent(i) = solution.tail(numPNodes)(i);
		}

		// assemble Jacobian matrix
		mMixed.AssembleJacobianMatrix();

		// assemble right hand side
		EigenVector rhs(size);
		rhs.setZero();

		auto extents = fem.GetExtents().ToEigen();
		auto errors = ComputeExtents() - extents;
		rhs.head(numDofs) = mMixed.ComputeStdRhs() - mMixed.GetVolJacobianMatrix().transpose() * mCinv * errors;
		rhs.tail(numPNodes) = -errors;

		return rhs;
	}

	EigenVector TwoFieldProblem::ComputeExtents()
	{
		FEMPhysics2D& fem = mMixed.GetFEM();
		uint32 numPNodes = mMixed.GetNumPressureNodes();
		real factor = 0.25;
		EigenVector initialVolumes(numPNodes);
		initialVolumes.setZero();
		EigenVector deformedVolumes(numPNodes);
		deformedVolumes.setZero();
		for (uint32 e = 0; e < fem.GetNumElements(); e++)
		{
			Matrix2R mat = fem.ComputeShapeMatrix(e);
			real ext = mat.Determinant() / 2;

			for (uint32 i = 0; i < mMixed.GetNumLocalPressureNodes(); i++)
			{
				uint32 globalI = mMixed.GetPressureGlobalIndex(e, i);
				initialVolumes[globalI] += factor * fem.GetElementInitialExtent(e);
				deformedVolumes[globalI] += factor * ext;
			}
		}
		return deformedVolumes;
	}

	void TwoFieldProblem::ComputeGradients(const SVector& x, SVector& grad)
	{
		// apply the current solution
		EigenMapConst sol = x.ToEigen();
		//StandardProblem::ApplyState(sol.head(mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS));
		uint32 numDofs = mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS;
		SVector g;
		SVector dx(sol.head(numDofs));
		StandardProblem::ComputeGradients(dx, g);
		auto delta = sol.tail(mFEM.GetNumNodes());
		for (uint32 i = 0; i < mFEM.GetNumNodes(); i++)
		{
			mFEM.GetExtent(i) = mPrevExtents(i) + delta(i);
		}

		EigenVector r(GetSize());
		r.head(numDofs) = g.ToEigen();
		EigenVector errors = EvaluateConstraints();
		r.tail(mFEM.GetNumNodes()) = -mCinv * errors;
		grad = SVector(r);
	}

	real TwoFieldProblem::ComputeObjective()
	{
		real energyDist = mFEM.ComputeEnergy();

		EigenVector errors = EvaluateConstraints();
		real energyVol = 0.5 * errors.transpose() * mCinv * errors;
		return energyVol + energyDist;
	}

	EigenVector TwoFieldProblem::EvaluateConstraints()
	{
		FEMPhysics2D& fem = mMixed.GetFEM();
		auto extents = fem.GetExtents().ToEigen();
		return ComputeExtents() - extents;
	}

	EigenMatrix TwoFieldProblem::ComputeConstraintGradient()
	{
		mMixed.AssembleJacobianMatrix();
		uint32 numNodes = mFEM.GetNumNodes();
		uint32 numDofs = mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS;
		EigenMatrix J(numNodes, numDofs + numNodes);
		J.block(0, 0, numNodes, numDofs) = mMixed.GetVolJacobianMatrix();
		EigenMatrix eye;
		eye.setIdentity(numNodes, numNodes);
		J.block(0, numDofs, numNodes, numNodes) = -eye;
		return J;
	}

	uint32 TwoFieldProblem::GetNumConstraints()
	{
		return mFEM.GetNumNodes();
	}

	uint32 TwoFieldProblem::GetSize()
	{
		return mFEM.GetNumNodes() + mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS;
	}
} // namespace MBD