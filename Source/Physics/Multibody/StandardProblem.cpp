#include "StandardProblem.h"
#include "FEMDerivativeProvider.h"
#include "FEMPhysics2D.h"
#include "ElasticEnergy2D.h"
#include <Physics/FEM/LinearSolver.h>

namespace MBD
{
	uint32 StandardProblem::GetSize()
	{
		return mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS;
	}

	void StandardProblem::ApplyState(const EigenVector& delta)
	{
		uint32 size = mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS;
		mFEM.GetDeformedPositions().tail(size) = mFEM.GetPreviousPositions().tail(size) + delta;
	}

	real StandardProblem::ComputeObjective()
	{
		return mFEM.ComputeEnergy();
	}

	void StandardProblem::ComputeGradients(const SVector& delta, SVector& grad)
	{
		if (delta.size() != 0)
		{
			uint32 size = mFEM.GetNumFreeNodes() * NUM_POS_COMPONENTS;
			mFEM.GetDeformedPositions().tail(size) = mFEM.GetPreviousPositions().tail(size) + delta;
		}

		FEMDerivativeProvider fdp(mFEM);
		MVector r(mFEM.GetNumFreeNodes());
		fdp.ComputeGradients(r);
		grad = SVector(r);
	}

	void StandardProblem::ComputeHessian(EigenMatrix& H)
	{
		ElasticEnergy2D::AssembleStiffnessMatrix(&mFEM, H);
		real h = mFEM.GetTimeStep();
		if (h != 0 && mFEM.GetSimType() == ST_IMPLICIT)
			H += (1 / h / h) * mFEM.GetMassMatrix();
	}

	void StandardProblem::MatrixVectorMultiply(const Vector2Array& d, Vector2Array& df) const
	{
		// compute M * d / h^2 - K * d (only the second term for quasi-static, identified by mTimeStep = 0)
		FEMDerivativeProvider fdp(mFEM);
		fdp.ComputeForceDifferential(d, df);
		real timeStep = mFEM.GetTimeStep();
		const real invHSqr = timeStep == 0 ? 0 : 1.f / (timeStep * timeStep);
		for (uint32 i = 0; i < mFEM.GetNumFreeNodes(); i++)
		{
			df[i] = invHSqr * mFEM.GetFreeNodeMass(i) * d[i] - df[i];
		}
	}
} // namespace MBD