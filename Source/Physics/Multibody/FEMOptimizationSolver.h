#ifndef FEM_OPTIMIZATION_SOLVER
#define FEM_OPTIMIZATION_SOLVER

#include "MultibodyCommon.h"

namespace MBD
{
	class FEMPhysics2D;
	class IOptimizationProblem;
	class IOptimizer;
	class StandardProblem;

	class FEMOptimizationSolver
	{
	public:
		enum NonlinearSolverType
		{
			NST_NEWTON,
			NST_NEWTON_CG,
			NST_NONLINEAR_CG,
			NST_GRADIENT_DESCENT,
			NST_STEEPEST_DESCENT,
			NST_NLOPT,
		};

	public:
		FEMOptimizationSolver(FEMPhysics2D& fem, int solverType);
		~FEMOptimizationSolver();
		void Solve();

	private:
		FEMPhysics2D& mFEM;
		uint32 mOuterIterations = 10;
		uint32 mInnerIterations = 10;
		int mSolverType = NST_NEWTON;
		std::unique_ptr<IOptimizer> mOptimizer;
		std::unique_ptr<StandardProblem> mProblem;
	};
}

#endif // FEM_OPTIMIZATION_SOLVER
