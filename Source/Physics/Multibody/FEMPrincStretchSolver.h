#ifndef FEM_PRINC_STRETCH_SOLVER
#define FEM_PRINC_STRETCH_SOLVER

namespace MBD
{
	class FEMPrincStretchSolver
	{
	public:
		void Solve();
	};
}

#endif // FEM_PRINC_STRETCH_SOLVER
