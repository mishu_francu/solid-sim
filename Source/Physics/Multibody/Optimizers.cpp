#include "Optimizers.h"
#include "StandardProblem.h"
#include <Physics/FEM/NewtonSolver.h>
#include <Math/IterativeSolvers.h>
#ifdef USE_NLOPT
#include <NLopt/nlopt.hpp>
#endif

namespace MBD
{
	void NewtonOptimizer::Solve()
	{
		NewtonSolverBackTrack<NewtonOptimizer> solver;
		solver.mNumIterations = mNumIterations;
		solver.mVerbose = VL_NONE;
		solver.mResidualThreshold = 1;
		solver.mUseProblemSolver = false;
		solver.mUseBFGS = false;

		// prepare the initial guess with the current configuration
		uint32 size = mProblem->GetSize();
		EigenVector solution(size);
		solution.setZero();

		solver.Solve(*this, size, solution);

		SVector grad, dx(solution);
		mProblem->ComputeGradients(dx, grad);
	}

	EigenVector NewtonOptimizer::ComputeRhs(const EigenVector& sol)
	{
		SVector b;
		SVector dx(sol);
		mProblem->ComputeGradients(dx, b);
		return b;
	}

	void NewtonOptimizer::ComputeSystemMatrix(const EigenVector& s, const EigenVector& y, EigenMatrix& K)
	{
		mProblem->ComputeHessian(K);
	}

	void GradientDescentOptimizer::Solve()
	{
		uint32 size = mProblem->GetSize();
		SVector delta(size);
		delta.setZero();
		SVector grad(size);
		mProblem->ComputeGradients(delta, grad);
		for (uint32 iter = 0; iter < mNumIterations; iter++)
		{
			delta += mAlpha * grad;
			mProblem->ComputeGradients(delta, grad);
		}
	}

	void SteepestDescentOptimizer::Solve()
	{
		// the differential vector
		uint32 size = mProblem->GetSize();
		MVector df(size / NUM_POS_COMPONENTS); // allocation!
		SVector grad;
		mProblem->ComputeGradients(SVector(), grad);

		SVector dx(size);
		dx.setZero();

		for (uint32 iter = 0; iter < mNumIterations; iter++)
		{
			MVector r(grad);
			mProblem->MatrixVectorMultiply(r, df);
			real delta = InnerProduct<Vector2R, real>(r, r);
			if (delta == 0)
				break;
			real alpha = delta / InnerProduct<Vector2R, real>(r, df);
			if (alpha == 0)
				break;
			dx += alpha * r;
			mProblem->ComputeGradients(dx, grad);
		}
	}

	void NewtonCGOptimizer::Solve()
	{
		uint32 size = mProblem->GetSize();
		MVector delta(size / NUM_POS_COMPONENTS); // allocation!
		SVector grad;
		mProblem->ComputeGradients(SVector(), grad);

		SVector dx(size);
		dx.setZero();

		// Newton steps (outer loop)
		for (uint32 iter = 0; iter < mNumIterations; iter++)
		{
			// solve K * dx = b by Conjugate Gradient
			MVector b(grad);
			std::fill(delta.begin(), delta.end(), Vector2R()); // reset the guess to zero
			SolveConjugateGradientMF<real, Vector2R, IOptimizationProblem>(*mProblem, b, delta, mSubIterations);

			dx += delta;
			mProblem->ComputeGradients(dx, grad);
		}
	}

#ifdef USE_NLOPT
	NLoptOptimizer::NLoptOptimizer(IOptimizationProblem* problem, bool derivativeFree) : IOptimizer(problem, 0)
	{
		// create solver object
		uint32 size = mProblem->GetSize();
		nlopt::algorithm alg = derivativeFree ? nlopt::LN_COBYLA : nlopt::LD_SLSQP;
		mOpt.reset(new nlopt::opt(alg, size));
		mOpt->set_min_objective(myfunc, mProblem);
	}

	void NLoptOptimizer::Solve()
	{
		// run the solver
		mOpt->set_xtol_rel(1e-4);
		std::vector<real> x(mProblem->GetSize()); // initialized to zero ?!
		real minf;
		try {
			nlopt::result result = mOpt->optimize(x, minf);
		}
		catch (std::exception & e) {
			Printf("nlopt failed: %s\n", e.what());
		}
	}

	double NLoptOptimizer::myfunc(const std::vector<double>& x, std::vector<double>& grad, void* my_func_data)
	{
		IOptimizationProblem* problem = (IOptimizationProblem*)my_func_data;

		SVector g;
		SVector delta(x);
		problem->ComputeGradients(delta, g);
		if (!grad.empty())
		{
			uint32 size = problem->GetSize();
			for (uint32 i = 0; i < size; i++)
				grad[i] = -g(i);
		}
		return problem->ComputeObjective();
	}
#endif

} // namespace MBD