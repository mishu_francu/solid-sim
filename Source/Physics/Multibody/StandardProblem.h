#ifndef STANDARD_PROBLEM_H
#define STANDARD_PROBLEM_H

#include "MultibodyCommon.h"
#include <Engine\Utils.h>

namespace MBD
{
	// Interface used by an optimizer to solve a certain problem.
	// We assume that the optimizer is computing a delta solution starting from zero.
	// The minimum requirement is to provide a gradient implementation.
	// The gradient function also receives the current solution and is the only one that does.
	// Currently we require the negative gradient to be supplied (may change).
	// The objective function is optional and does not require the current solution, 
	// as it assumes it was already passed to the gradient function.
	// The matrix vector multiply operation is a the gradient differential,
	// i.e. the product between the Hessian and a given vector (direction);
	// alternatively, one can provide the Hessian matrix directly.
	class IOptimizationProblem
	{
	public:
		// TODO: make them data members
		virtual uint32 GetSize() = 0;
		virtual uint32 GetNumConstraints() { ASSERT(false); return 0; }

		virtual real ComputeObjective() { ASSERT(false); return 0; }
		virtual void ComputeGradients(const SVector& x, SVector& grad) = 0;
		virtual void ComputeHessian(EigenMatrix& H) { ASSERT(false); }
		virtual void MatrixVectorMultiply(const Vector2Array& in, Vector2Array& out) const { ASSERT(false); }
	};

	class FEMPhysics2D;

	class StandardProblem : public IOptimizationProblem
	{
	public:
		StandardProblem(FEMPhysics2D& fem) : mFEM(fem) {}
		uint32 GetSize() override;
		real ComputeObjective() override;
		void ComputeGradients(const SVector& delta, SVector& grad) override;
		void ComputeHessian(EigenMatrix& H) override;
		void MatrixVectorMultiply(const Vector2Array& in, Vector2Array& out) const override;

	protected:
		void ApplyState(const EigenVector& delta);

	protected:
		FEMPhysics2D& mFEM;
	};

}


#endif // STANDARD_PROBLEM_H
