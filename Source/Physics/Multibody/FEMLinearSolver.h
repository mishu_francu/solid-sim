#ifndef FEM_LINEAR_SOLVER_H
#define FEM_LINEAR_SOLVER_H

#include <Engine/Types.h>
#include "MultibodyCommon.h"

namespace MBD
{
	class FEMPhysics2D;

	class FEMLinearSolver
	{
	public:
		FEMLinearSolver(FEMPhysics2D& fem);
		void SolveEquilibrium();

	private:
		void AssembleStiffnessMatrix();
		void ComputeLocalStiffnessMatrix(uint32 i, EigenMatrix& Klocal);
		void GetBarycentricJacobianVectors(uint32 e, Vector2R y[]) const;
		void ComputeStrainJacobian(uint32 i, Matrix2R Bn[], Vector2R Bs[]);

	private:
		FEMPhysics2D& mFEM;
		EigenMatrix mStiffnessMatrix;
		Matrix2R mNormalElasticityMatrix;

		friend class FEMTests;
	};

}

#endif // FEM_LINEAR_SOLVER_H
