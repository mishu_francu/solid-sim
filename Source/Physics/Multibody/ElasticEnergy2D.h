#ifndef ELASTIC_ENERGY_2D_H
#define ELASTIC_ENERGY_2D_H

#include "MultibodyCommon.h"
#include <Math/Matrix2.h>
#include <Engine/Types.h>

namespace MBD
{
	class FEMPhysics2D;

	class ElasticEnergy2D
	{
	public:
		static real ComputeEnergy(const FEMPhysics2D* femPhysics, int material = -1);
		static void ComputeForces(const FEMPhysics2D* femPhysics, MVector& fout, int material = -1);
		static Matrix2R ComputeElementStress(const FEMPhysics2D* femPhysics, uint32 e, int material = -1);
		static void ComputeLocalForceDifferential(const FEMPhysics2D* femPhysics, uint32 e, const Vector2R dx[], Vector2R df[]);
		static void ComputeLocalStiffnessMatrixFromDifferential(const FEMPhysics2D* femPhysics, uint32 e, EigenMatrix& Klocal);
		static void AssembleStiffnessMatrix(const FEMPhysics2D* femPhysics, EigenMatrix& stiffnessMatrix, EigenMatrix* bcStiffnessMatrix = nullptr);
	};
}

#endif // ELASTIC_ENERGY_2D_H
