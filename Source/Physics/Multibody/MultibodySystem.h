#ifndef MULTIBODY_SYSTEM_H
#define MULTIBODY_SYSTEM_H

#include "MultibodyCommon.h"
#include <Math/Vector2.h>
#include <Engine/Types.h>

#include <memory>

class PhysicsSystem;

namespace MBD
{
	class FEMPhysics2D;

	class MultibodySystem
	{
	public:
		struct Particle
		{
			float mass;
			Math::Vector2 pos, vel;
		};

		struct DistanceConstraint
		{
			uint32 idx1, idx2;
		};

		struct TriangleElement
		{
			uint32 idx1, idx2, idx3;
		};

	public:
		MultibodySystem(bool useFEM);
		~MultibodySystem();

		void SetMaterial(int val) { mMaterial = val; }

		void AddParticle(real mass, const Math::Vector2& pos, const Math::Vector2& vel);
		void AddDistanceConstraint(uint32 idx1, uint32 idx2);
		void AddTriangleElement(uint32 idx1, uint32 idx2, uint32 idx3);

		uint32 GetParticlesCount() const { return (uint32)mParticles.size(); }
		uint32 GetDistanceConstraintsCount() const { return (uint32)mDistanceConstraints.size(); }
		uint32 GetTriangleElementsCount() const { return (uint32)mTriangleElements.size(); }

		const Particle& GetParticle(uint32 i) const { return mParticles[i]; }
		const DistanceConstraint& GetDistanceConstraint(uint32 i) const { return mDistanceConstraints[i]; }
		const TriangleElement& GetTriangleElement(uint32 i) const { return mTriangleElements[i]; }

		FEMPhysics2D* GetFEMPhysics() { return mFEMPhysics2D.get(); }

		void SetWidthAndHeight(float width, float height)
		{
			mWidth = width;
			mHeight = height;
		}

		void Clear();
		void Compile();
		void Step();

	private:
		bool mUseFEM;

		std::vector<Particle> mParticles;
		std::vector<DistanceConstraint> mDistanceConstraints;
		std::vector<TriangleElement> mTriangleElements;

		std::unique_ptr<PhysicsSystem> mParticleSys;
		float mWidth, mHeight;

		std::unique_ptr<FEMPhysics2D> mFEMPhysics2D;

		int mMaterial;
	};

} // namespace MBD

#endif // MULTIBODY_SYSTEM_H