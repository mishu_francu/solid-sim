#ifndef MULTIBODY_COMMON_H
#define MULTIBODY_COMMON_H

#include <Math/Vector2.h>
#include <Math/Matrix2.h>
#include <Engine/Types.h>

// Eigen library includes
#if !defined(_DEBUG) && defined(USE_MKL)
	#define EIGEN_USE_MKL_ALL
	#include <Eigen/PardisoSupport>
#endif
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/IterativeSolvers>

#include <vector>

typedef double real;
typedef Eigen::Matrix<real, Eigen::Dynamic, Eigen::Dynamic> EigenMatrix;
typedef Eigen::Matrix<real, Eigen::Dynamic, 1> EigenVector;

typedef Eigen::SparseMatrix<real> SparseMatrix;

namespace MBD
{
	typedef Math::Vector2Tpl<real> Vector2R;
	typedef Matrix2T<real> Matrix2R;

	typedef std::vector<Vector2R> Vector2Array;

	typedef Eigen::Map<EigenVector> EigenMap;
	typedef Eigen::Map<const EigenVector> EigenMapConst;

	enum
	{
		NUM_DIMENSIONS = 2,
		NUM_POS_COMPONENTS = NUM_DIMENSIONS,
		NUM_ELEMENT_NODES = 3,
	};

	enum SimulationType
	{
		ST_STATIC,
		ST_QUASI_STATIC,
		ST_EXPLICIT,
		ST_IMPLICIT,
	};

	enum MaterialType
	{
		MMT_LINEAR,
		MMT_STVK,
		MMT_COROTATIONAL,
		MMT_NEO_HOOKEAN,
		MMT_NEO_HOOKEAN_OGDEN,
		MMT_DISTORTIONAL_NH,
	};

	enum ImplicitSolverType
	{
		IST_LINEAR,
		IST_NONLINEAR,
		IST_MIXED,
		IST_PRINCIPAL_STRETCHES,
	};
}

#include "VectorAdapters.h"

#endif // MULTIBODY_COMMON_H