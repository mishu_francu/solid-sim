#include <Engine/Base.h>

#	include <Graphics3D/Graphics3D.h>
#if (RENDERER3D == OPENGL) && defined(FIXED_PIPELINE)

#ifndef _WIN64
// TODO: static linkage (#define FREEIMAGE_LIB)
#	include <FreeImage/FreeImage.h>
#endif
#	include <Graphics2D/Texture.h>
#	include <Engine/Profiler.h>

// TODO: move all magic numbers

bool Graphics3D::Init(HWND window)
{
//	// some constructor stuff
//	mousePressed = false;
//
//	hWnd = window;
//	hDC = GetDC(hWnd);
//	InitPF();
//	// if we can create a rendering context ...  
//	if (hGLRC = wglCreateContext(hDC))
//	{
//		// try to make it the thread's current rendering context 
//		BOOL bHaveCurrentRC = wglMakeCurrent(hDC, hGLRC);  
//	}
//	
//	// TODO: unify with Graphics2DGL
//#if !defined(USE_GDIPLUS)
//	FreeImage_Initialise();
//#endif

	int argc = 1;
	char* argv[1] = { (char*)"Something" };
	glutInit(&argc, argv);

	sphList = glGenLists(1);
	glNewList(sphList, GL_COMPILE);
	glutSolidSphere(20, 10, 10);
	glEndList();

	return true;
}

void Graphics3D::SetContext()
{
	// we need the Z-buffer for 3D
	glEnable(GL_DEPTH_TEST);

	// solid/wireframe
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	//culling	
	glEnable(GL_CULL_FACE);

	// transparency
	glAlphaFunc(GL_ALWAYS, 0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// lights
    glEnable(GL_LIGHTING);
	
	// light 1 (positional)    
	//glEnable(GL_LIGHT0);
	GLfloat amb1[] = {0.2f, 0.2f, 0.2f, 1};	
	GLfloat dif[] = {1, 1, 1, 1};	
    glLightfv(GL_LIGHT0, GL_AMBIENT, amb1);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, dif);    
	
	// light 2 (directional)
    glEnable(GL_LIGHT1);
	GLfloat pos[]= {1, 1, 1, 0};	
	GLfloat amb2[] = {0.1f, 0.1f, 0.1f, 1};	
	GLfloat dif2[] = {0.5, 0.5, 0.5, 1};	
    glLightfv(GL_LIGHT1, GL_POSITION, pos);	
    glLightfv(GL_LIGHT1, GL_AMBIENT, amb2);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, dif2);
	
	// ambiental light
	GLfloat amb[] = {0.4f, 0.4f, 0.4f, 0};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb);
	
	// materials
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
}

//void Graphics3D::InitPF()
//{
//	PIXELFORMATDESCRIPTOR pfd = { 
//		sizeof(PIXELFORMATDESCRIPTOR),    // size of this pfd 
//		1,                                // version number 
//		PFD_DRAW_TO_WINDOW |              // support window 
//		PFD_SUPPORT_OPENGL |              // support Graphics2D 
//		PFD_DOUBLEBUFFER,                 // double buffered
//		PFD_TYPE_RGBA,                    // RGBA type 
//		32,                               // 32-bit color depth 
//		0, 0, 0, 0, 0, 0,                 // color bits ignored 
//		0,                                // no alpha buffer 
//		0,                                // shift bit ignored 
//		0,                                // no accumulation buffer 
//		0, 0, 0, 0,                       // accum bits ignored 
//		32,                               // 32-bit z-buffer     
//		0,                                // no stencil buffer 
//		0,                                // no auxiliary buffer 
//		PFD_MAIN_PLANE,                   // main layer 
//		0,                                // reserved 
//		0, 0, 0                           // layer masks ignored 
//	}; 
//	int  iPixelFormat; 
//	// get the device context's best, available pixel format match 
//	iPixelFormat = ChoosePixelFormat(hDC, &pfd); 
//	// make that match the device context's current pixel format 
//	SetPixelFormat(hDC, iPixelFormat, &pfd); 
//}

void Graphics3D::Resize(int width, int height)
{
	// viewport
	w = width;
	h = height;

	// light position - why?
	GLfloat pos[]= {0, 0, 0, 1};	
    glLightfv(GL_LIGHT0, GL_POSITION, pos);	

	//float mat[16];
	//glGetFloatv(GL_PROJECTION_MATRIX, mat);
	//for (int i = 0; i < 4; i++)
		//Printf("%.2f %.2f %.2f %.2f\n", mat[i], mat[i + 4], mat[i + 8], mat[i + 12]);
}

void Graphics3D::DeInit()
{
	wglMakeCurrent(0, 0);
	ReleaseDC(hWnd, hDC) ;
	wglDeleteContext(hGLRC);

#ifndef USE_GDIPLUS
	FreeImage_DeInitialise();
#endif
}

void Graphics3D::Viewport()
{
	glViewport(0, 0, w, h);

	// perspective projection matrix
	float aspect = float(w) / float(h); 
	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();
	//float near_height = 5;
	// TODO: choose wisely and make them global
	//float zNear = 10;
	//float zFar = 10000;
	//glFrustum(-near_height * aspect, near_height * aspect, -near_height, near_height, zNear, zFar);
	gluPerspective(fov, aspect, n, f);

	// view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	Matrix4 view = Matrix4::Transposed(camera.View());
	glLoadMatrixf(view.GetData());
}

void Graphics3D::BeginDraw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//Viewport();
	glPushMatrix();
}

void Graphics3D::EndDraw()
{
	glPopMatrix();
	SwapBuffers(hDC);
}

void Graphics3D::DrawLine(const Vector3& a, const Vector3& b)
{
	glColor3f(color.X(), color.Y(), color.Z());
	const float l = 20.f;
	glBegin(GL_LINES);
		glVertex3f(a.X(), a.Y(), a.Z());
		glVertex3f(b.X(), b.Y(), b.Z());
	glEnd();
}

void Graphics3D::DrawTriangle(const Vector3& a, const Vector3& b, const Vector3& c, bool fill)
{
	glColor3f(color.X(), color.Y(), color.Z());
	const float l = 20.f;
	glBegin(fill ? GL_TRIANGLES : GL_LINE_LOOP);
		glVertex3f(a.X(), a.Y(), a.Z());
		glVertex3f(b.X(), b.Y(), b.Z());
		glVertex3f(c.X(), c.Y(), c.Z());
	glEnd();
}

void Graphics3D::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<uint32>& indices)
{
	glColor3f(color.X(), color.Y(), color.Z());
	const float l = 20.f;
	glBegin(GL_TRIANGLES);
	for (size_t i = 0; i < indices.size(); i += 3)
	{
		const Vector3& a = vertices[indices[i]];
		const Vector3& b = vertices[indices[i + 1]];
		const Vector3& c = vertices[indices[i + 2]];
		
		glVertex3f(a.X(), a.Y(), a.Z());
		glVertex3f(b.X(), b.Y(), b.Z());
		glVertex3f(c.X(), c.Y(), c.Z());
	}
	glEnd();
}

void Graphics3D::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, const Matrix4* model)
{
	glColor3f(color.X(), color.Y(), color.Z());
	const float l = 20.f;
	glBegin(GL_TRIANGLES);
	for (size_t i = 0; i < indices.size(); i += 3)
	{
		const Vector3& a = vertices[indices[i]];
		const Vector3& b = vertices[indices[i + 1]];
		const Vector3& c = vertices[indices[i + 2]];
		
		const Vector3& n1 = normals[indices[i]];
		const Vector3& n2 = normals[indices[i + 1]];
		const Vector3& n3 = normals[indices[i + 2]];
		
		glVertex3f(a.X(), a.Y(), a.Z());
		glNormal3f(n1.X(), n1.Y(), n1.Z());
		glVertex3f(b.X(), b.Y(), b.Z());
		glNormal3f(n2.X(), n2.Y(), n2.Z());
		glVertex3f(c.X(), c.Y(), c.Z());
		glNormal3f(n3.X(), n3.Y(), n3.Z());
	}
	glEnd();
}

void Graphics3D::DrawAxes()
{
	glColor3f(1, 1, 1);
	const float l = 20.f;
	glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(l, 0, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(0, l, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(0, 0, l);
	glEnd();

	//glRasterPos3f(1.1f * l, 0, 0);
	//glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'x');
	//glRasterPos3f(0, 1.1f * l, 0);
	//glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'y');
	//glRasterPos3f(0, 0, 1.1f * l);
	//glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'z');
}

void Graphics3D::MouseDown(int x, int y)
{
	mousePressed = true;
	mouseX = x;
	mouseY = y;
}

void Graphics3D::MouseUp(int x, int y)
{
	mousePressed = false;
}

void Graphics3D::MouseMove(int x, int y)
{
	if (!mousePressed)
		return;
	int dx = x - mouseX;
	int dy = y - mouseY;
	mouseX = x;
	mouseY = y;
	camera.Rotate(dx, dy);
}

void Graphics3D::MouseWheel(float delta)
{
	camera.Translate(delta, 0);
}

void Graphics3D::DrawSphere(const Vector3& pos, float radius)
{
	//MEASURE_TIME("DrawSphere");
	glPushMatrix();
	glTranslatef(pos.X(), pos.Y(), pos.Z());
	float r = 20;
	float s = radius / r;
	glScalef(s, s, s); // scales the normal too
	//glutSolidSphere(r, 10, 10);
	glCallList(sphList);
	glPopMatrix();
}

void Graphics3D::DrawCube(const Vector3& pos, const Vector3& ext, const Matrix3& rot)
{
	// TODO: rotation
	glColor3f(color.X(), color.Y(), color.Z());
	glScalef(ext.X(), ext.Y(), ext.Z());
	glutSolidCube(2);
}

void Graphics3D::DrawTetrahedron(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& v3)
{
	glBegin(GL_LINES);
	glVertex3fv((float*)&v0);
	glVertex3fv((float*)&v1);
	glVertex3fv((float*)&v1);
	glVertex3fv((float*)&v2);
	glVertex3fv((float*)&v2);
	glVertex3fv((float*)&v0);
	glVertex3fv((float*)&v3);
	glVertex3fv((float*)&v0);
	glVertex3fv((float*)&v3);
	glVertex3fv((float*)&v1);
	glVertex3fv((float*)&v3);
	glVertex3fv((float*)&v2);
	glEnd();
}

void Graphics3D::DrawWireCube(const Vector3& pos, const Vector3& s)
{
	glColor3f(color.X(), color.Y(), color.Z());
	glPushMatrix();
	glTranslatef(pos.X(), pos.Y(), pos.Z());
	glScalef(s.X(), s.Y(), s.Z());
	glutWireCube(1);
	glPopMatrix();
}

void Graphics3D::SetFlipNormals(bool val) {}
void Graphics3D::SetFlags(int val) {}
int Graphics3D::GetRenderFlags() { return 0; }
void Graphics3D::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<Vector3>& colors, const std::vector<uint32>& indices, float scale) {}
void Graphics3D::DrawSphereTex(const Vector3& pos, float radius, const Matrix3& rot, Texture* tex) {}
void Graphics3D::DrawMesh(const SimpleMesh& mesh) {}
void Graphics3D::DrawCapsule(const Vector3& pos, float radius, float height, const Matrix3& rot) {}
void Graphics3D::SetLightPos(const Vector3& v) {}
void Graphics3D::GetScreenCoords(const Vector3& p, float& sx, float& sy) {}

#endif
