#ifndef BODIES_H
#define BODIES_H

#include <Math/Quaternion.h>
#include <vector>
#include <memory>

class Graphics3D;

struct Body
{
	enum PrimitiveType
	{
		NONE,
		SPHERE,
		BOX,
		RECTANGLE,
		LINE
	};

	struct Shape
	{
		Vector3 pos;
		Quaternion rot;
		Matrix3 mat; // redundant
		// TODO: shape instance

		virtual ~Shape() { }
		virtual PrimitiveType GetType() = 0; // or member
	};

	struct Box : public Shape
	{
		float a, b, c;

		void SetDimensions(float x, float y, float z)
		{
			a = x;
			b = y;
			c = z;
		}

		virtual PrimitiveType GetType() { return BOX; }
	};

	struct Sphere : public Shape
	{
		float radius;

		virtual PrimitiveType GetType() { return SPHERE; }
	};

	int id;
	Vector3 pos;
	Quaternion rot;
	Matrix3 mat; // this is redundant
	float red, green, blue; // TODO: Color struct
	std::vector<std::unique_ptr<Shape> > shapes;

	void Draw(Graphics3D* g3D) const;
};

class Bodies
{
private:
	std::vector<std::unique_ptr<Body> > bodies; // TODO: smart pointers

public:
	void Load();

	void Draw(Graphics3D* g3D) const;
};

#endif // BODIES_H