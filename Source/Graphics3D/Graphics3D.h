#ifndef GRAPHICS3D_H
#define GRAPHICS3D_H

#include <Graphics2D/Graphics.h>
#include <Engine/Types.h>
#include <Graphics3D/Bodies.h>
#include <Math/Matrix4.h>
#include <Math/Vector2.h>
#include <Graphics3D/Camera.h>
#include <Graphics2D/Texture.h>

#if (RENDERER == GDIPLUS)
#define RENDERER3D OPENGL
#else
#define RENDERER3D RENDERER
#endif

struct SimpleMesh
{
	std::vector<Vector3> vertices;
	std::vector<uint32> indices;
	std::vector<Vector3> normals;

	Vector3 pos;
	Matrix3 rot;
};

enum ShaderFlags
{
	NONE = 0,
	VERTEX_COLORS = 1,
	OREN_NAYAR = 2,
	TEXTURED = 4,
	MONOCHROME = 8,
	WIREFRAME = 16,
};

enum RenderMode
{
	RM_SHADED,
	RM_WIREFRAME_ON_SHADED,
	RM_WIREFRAME
};

struct NormalProgram
{
	GLuint programID;
	GLint MatrixID, ModelMatrixID, eyePosID;
	GLint LightDirID; // rename to position
	GLint diffColorID, specColorID;
	GLint glossID;
	GLint flipID;
	GLint flagsID;
	GLint winScaleId;
	GLint shadowVPMatrixID;
};

class OpenGLSData
{
public:
	bool Init();
	void PrepareForDraw(const Vector3& lightPos, int w, int h);
	void PrepareForShadow();

	void DrawAxes();
	void DrawSphere(const Vector3& pos, float radius);
	void DrawSphereTex(const Vector3& pos, float radius, const Matrix3& rot, Texture* tex);
	void DrawTriangle(const Vector3& a, const Vector3& b, const Vector3& c, bool fill);
	void DrawCube(const Vector3& pos, const Vector3& ext, const Matrix3& rot);
	void DrawCapsule(const Vector3& pos, float radius, float height, const Matrix3& rot);
	void DrawLine(const Vector3& a, const Vector3& b);
	void DrawLines(std::vector<Vector3> points);
	void DrawMesh(const SimpleMesh& mesh);
	void DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, const Matrix4* model);
	void DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<Vector3>& colors, const std::vector<uint32>& indices, float scale);
	void DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, const std::vector<Vector2>& uvs, Texture* tex);
	void DrawTetrahedron(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& v3);
	void DrawTetrahedronEdgeAsBBCurve(const Vector3& c1, const Vector3& c2, const size_t nodesperedge, const Vector3* edge);

	void SetColor(float r, float g, float b);
	void SetFlipNormals(bool val);
	void SetProgram(bool wireframe);
	
	void SetFlags(int val);
	int GetFlags() const { return flags; }

	const Matrix4& GetProjection() const { return Projection; }
	void SetProjection(const Matrix4& mat) { Projection = mat; }

	const Matrix4& GetView() const { return View; }
	void SetView(const Matrix4& mat) { View = mat; }

	const Matrix4& GetViewShadow() const { return viewShadow; }
	void SetViewShadow(const Matrix4& mat) { viewShadow = mat; }

private:
	void CreateSphere();
	bool InitShadowMap();

	bool LoadNormalProgram();
	bool LoadWireframeProgram();

private:
	Matrix4 Projection, View;
	GLuint vertexbuffer; // TODO: remove
	GLuint normalbuffer; // TODO: remove
	GLuint elementbuffer;
	GLuint colorbuffer;
	GLuint uvbuffer;

	NormalProgram program, wireframeProgram;
	NormalProgram* currProgram = nullptr;

	GLuint linesBuffer;


	GLuint sphereVAO;
	//uint32 sphereVBO;
	GLuint vao;

	int flags = 0;

	// sphere
	struct PosNormal // vertex format
	{
		Vector3 v, n;
	};
	GLsizei nSphereElements;

	// shadow
	GLuint shadowProgramID;
	GLuint shadowFramebuffer;
	GLuint shadowModelID;
	bool isShadowPass = false;
	Texture shadowMap;
	Matrix4 viewShadow;
	Matrix4 projShadow;
};

class Graphics3D
{
public:
	enum CullingMode
	{
		CULL_FRONT = GL_FRONT,
		CULL_BACK = GL_BACK
	};

public:
#ifdef _WIN32
	HWND hWnd;
#endif
	int w, h;

	Camera camera;
	int mouseX, mouseY;
	bool mousePressed;

	Bodies bodies;

	float fov, n, f;

#if defined(_WIN32) && (RENDERER3D == OPENGL)
	// TODO: rename
	HGLRC hGLRC;
	HDC hDC;
	
	GLuint sphList;
#endif

#if !defined(FIXED_PIPELINE) && (RENDERER3D == OPENGL)
	OpenGLSData mGLS;
#endif
	Vector3 color;
	Vector3 lightPos;

	int mRenderMode;

public:
	Graphics3D() : mousePressed(false), fov(60), n(0.1f), f(10000), w(10), h(10) { }
	bool Init(HWND window);
	void DeInit();
	void Resize(int width, int height);
	void BeginDraw();		
	void EndDraw();
	
	void DrawAxes();
	void MouseDown(int x, int y);
	void MouseMove(int x, int y);
	void MouseUp(int x, int y);
	void MouseWheel(float delta);

	Bodies& GetBodies() { return bodies; }

	void DrawSphere(const Vector3& pos, float radius);
	void DrawSphereTex(const Vector3& pos, float radius, const Matrix3& rot, Texture* tex);
	void DrawTriangle(const Vector3& a, const Vector3& b, const Vector3& c, bool fill);
	void DrawCube(const Vector3& pos, const Vector3& scale, const Matrix3& rot); // TODO: rename to box
	void DrawWireCube(const Vector3& pos, const Vector3& scale);
	void DrawLine(const Vector3& a, const Vector3& b);
	void DrawLines(const std::vector<Vector3>& points);
	void DrawMesh(const std::vector<Vector3>& vertices, const std::vector<uint32>& indices);
	void DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, const Matrix4* model = nullptr);
	void DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<Vector3>& colors, const std::vector<uint32>& indices, float scale = 1.f);
	void DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, 
		const std::vector<Vector2>& uvs, Texture* tex);
	void DrawTetrahedronEdgeAsBBCurve(const Vector3& c1, const Vector3& c2, const size_t nodesperedge, const Vector3* edge);
	void DrawTetrahedron(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& v3);
	void DrawCapsule(const Vector3& pos, float radius, float height, const Matrix3& rot);

	void DrawMesh(const SimpleMesh& mesh);

	void SetColor(float r, float g, float b);

	void Viewport();
	void SetContext();

	void ViewportShadow();

	const Vector3& GetLightPos() const { return lightPos; }
	void SetLightPos(const Vector3& v);
	void SetFlipNormals(bool val);
	void SetFlags(int val);
	int GetRenderFlags();
	void SetCulling(int val);

	void GetScreenCoords(const Vector3& p, float& sx, float& sy);

	void SetRenderMode(int val);
};

#ifdef FIXED_PIPELINE
inline void Graphics3D::SetColor(float r, float g, float b)
{
	color.Set(r, g, b);
	glColor4f(r, g, b, 1.f);
}
#endif

#endif // GRAPHICS3D_H
