#include <Engine/Base.h>
#include <Engine/Engine.h>
#include <Graphics3D/Graphics3D.h>
#include <Graphics3D/Bodies.h>
#include <Engine/xml.h>

void ReadCoord(XMLElement* xbody, Vector3& v, Quaternion& q)
{
	char str[100];
	XMLElement* xpos = xbody->FindElementZ("Position");
	if(xpos != NULL)
	{
		xpos->FindVariableZ("x")->GetValue(str);			
		v.X() = (float)atof(str);
		xpos->FindVariableZ("y")->GetValue(str);
		v.Y() = (float)atof(str);
		xpos->FindVariableZ("z")->GetValue(str);
		v.Z() = (float)atof(str);
	}

	XMLElement* xrot = xbody->FindElementZ("Rotation");
	if(xrot != NULL)
	{
		xrot->FindVariableZ("x")->GetValue(str);			
		q.v.X() = (float)atof(str);
		xrot->FindVariableZ("y")->GetValue(str);
		q.v.Y() = (float)atof(str);
		xrot->FindVariableZ("z")->GetValue(str);
		q.v.Z() = (float)atof(str);
		xrot->FindVariableZ("w")->GetValue(str);
		q.s = (float)atof(str);		
	}
}

void Bodies::Load()
{
	XML* scene = new XML("scene.xml");
	if(scene->IntegrityTest() || scene->ParseStatus() == 0)
	{
		XMLElement* root = scene->GetRootElement();
		char str[100];		
		XMLElement* xbodies = root->FindElementZ("Bodies");
		if (!xbodies)
			return;
		int n = xbodies->GetChildrenNum();
		for (int i = 0; i < n; i++)
		{
			XMLElement* xbody = xbodies->GetChildren()[i];
			Body* body = new Body;
			xbody->FindVariableZ("id")->GetValue(str);
			body->id = atoi(str);
			
			XMLElement* xshapes = xbody->FindElementZ("Shape");
			int numShape = xshapes->GetChildrenNum();
			for (int j = 0; j < numShape; j++)
			{
				XMLElement* xshape = xshapes->GetChildren()[j];
				xshape->GetElementName(str);
				if(strcmp(str, "Box") == 0)
				{
					Body::Box* box = new Body::Box();
					xshape->FindVariableZ("a")->GetValue(str);
					float a = (float)atof(str);
					xshape->FindVariableZ("b")->GetValue(str);
					float b = (float)atof(str);
					xshape->FindVariableZ("c")->GetValue(str);
					float c = (float)atof(str);
					box->SetDimensions(a, b, c);
					ReadCoord(xshape, box->pos, box->rot);
					box->mat = box->rot.ToMatrix();
					body->shapes.push_back(std::auto_ptr<Body::Shape>(box));
				}
				else if(strcmp(str, "Sphere") == 0)
				{
					Body::Sphere* sphere = new Body::Sphere();
					xshape->FindVariableZ("Radius")->GetValue(str);
					sphere->radius = (float)atof(str);			
					ReadCoord(xshape, sphere->pos, sphere->rot);
					sphere->mat = sphere->rot.ToMatrix();
					body->shapes.push_back(std::auto_ptr<Body::Shape>(sphere));
				}				
			}			
			
			ReadCoord(xbody, body->pos, body->rot);
			body->mat = qToMatrix(body->rot);

			XMLElement* xcol = xbody->FindElementZ("Color");
			if(xcol != NULL)
			{
				xcol->FindVariableZ("r")->GetValue(str);			
				body->red = (float)atof(str);
				xcol->FindVariableZ("g")->GetValue(str);
				body->green = (float)atof(str);
				xcol->FindVariableZ("b")->GetValue(str);
				body->blue = (float)atof(str);
			}

			//XMLElement* xmesh = xbody->FindElementZ("Mesh");
			//if(xmesh != NULL)
			//{
			//	xmesh->FindElementZ("Path")->GetContents()[0]->GetValue(str);
			//	Mesh* mesh = new Mesh();
			//	//mesh->LoadFrom3DS(str);
			//	mesh->LoadFromFile(str);
			//	XMLElement* xtexid = xmesh->FindElementZ("TexID");
			//	if(xtexid != NULL){
			//		xtexid->GetContents()[0]->GetValue(str);
			//		mesh->texID = atoi(str);
			//	}
			//	else
			//		mesh->texID = 1;
			//	XMLElement* xpos = xmesh->FindElementZ("Position");
			//	if(xpos != NULL){
			//		xpos->FindVariableZ("x")->GetValue(str);
			//		mesh->pos.x = atof(str);
			//		xpos->FindVariableZ("y")->GetValue(str);
			//		mesh->pos.y = atof(str);
			//		xpos->FindVariableZ("z")->GetValue(str);
			//		mesh->pos.z = atof(str);
			//	}
			//	else
			//		mesh->pos = -body->rigid->X;
			//	XMLElement* xrot = xmesh->FindElementZ("Rotation");
			//	if(xrot){
			//		xrot->FindVariableZ("x")->GetValue(str);
			//		mesh->rot.v.x = atof(str);
			//		xrot->FindVariableZ("y")->GetValue(str);
			//		mesh->rot.v.y = atof(str);
			//		xrot->FindVariableZ("z")->GetValue(str);
			//		mesh->rot.v.z = atof(str);
			//		xrot->FindVariableZ("w")->GetValue(str);
			//		mesh->rot.s = atof(str);
			//	}
			//	body->mesh = mesh;
			//}

			//world.AddRigid(body->rigid);
			bodies.push_back(std::auto_ptr<Body>(body));
		}
	}
}

void Body::Draw(Graphics3D* g3D) const
{
#if (RENDERER == OPENGL)
	// TODO: remove all GL calls
	glPushMatrix();
	float T[16];
	GetTransform(T, mat, pos);
	glMultMatrixf(T);

	g3D->SetColor(red, green, blue);
	for (size_t i = 0; i < shapes.size(); i++)
	{
		Shape* shape = shapes[i].get();
		glPushMatrix();
		float M[16];
		GetTransform(M, shape->mat, shape->pos);
		glMultMatrixf(M);
		switch(shape->GetType())
		{
			case Body::BOX:
			{
				Box* box = (Box*)shape;
				float a = box->a;
				float b = box->b;
				float c = box->c;
				g3D->DrawCube(Vector3(), Vector3(a, b, c), Matrix3());
			}
			break;
			case Body::SPHERE:
			{
				Sphere* s = (Sphere*)shape;
				g3D->DrawSphere(Vector3(0, 0, 0), s->radius);
			}
			break;
		}
		glPopMatrix();
	}
	glPopMatrix();
#endif
}

void Bodies::Draw(Graphics3D* g3D) const
{
	for (size_t i = 0; i < bodies.size(); i++)
	{
		bodies[i]->Draw(g3D);
	}
}


