#include <Engine/Base.h>

#include <Graphics3D/Graphics3D.h>
#if (RENDERER3D == OPENGL) && !defined(FIXED_PIPELINE)

//#include <Engine/Profiler.h>
#include <Graphics2D/Texture.h> // TODO: not really 2D only

#ifdef USE_IMGUI
#include <imgui.h>
#include <imgui_impl_win32.h>
#include <imgui_impl_opengl3.h>
#endif

#include <fstream>
#include <string>
#include <vector>

#define USE_VAO

bool LoadShader(const char* path, GLenum type, GLuint& ShaderID)
{
	ShaderID = glCreateShader(type);
	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(path, std::ios::in);
	if (VertexShaderStream.is_open())
	{
		std::string Line = "";
		while (std::getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}
	else
	{
		Printf("Could not open file %s\n", path);
		return false;
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	Printf("Compiling shader : %s\n", path);
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(ShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(ShaderID);

	// Check Vertex Shader
	glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(ShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (!Result && InfoLogLength)
	{
		std::vector<char> VertexShaderErrorMessage(InfoLogLength);
		glGetShaderInfoLog(ShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		Printf("%s\n", &VertexShaderErrorMessage[0]);
		return false;
	}

	return true;
}

bool LoadShaders(const char* vertex_file_path, const char* fragment_file_path, GLuint& ProgramID)
{
	bool ret = true;
	GLuint VertexShaderID, FragmentShaderID;

	if (!LoadShader(vertex_file_path, GL_VERTEX_SHADER, VertexShaderID))
		return false;

	if (!LoadShader(fragment_file_path, GL_FRAGMENT_SHADER, FragmentShaderID))
		return false;

	// Link the program
	Printf("Linking program\n");
	ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	GLint Result = GL_FALSE;
	int InfoLogLength;
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (!Result && InfoLogLength)
	{
		std::vector<char> ProgramErrorMessage(max(InfoLogLength, int(1)));
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		Printf("%s\n", &ProgramErrorMessage[0]);
		ret = false;
	}

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ret;
}

bool LoadShadersEx(const char* vertex_file_path, const char* geometry_file_path, const char* fragment_file_path, GLuint& ProgramID)
{
	bool ret = true;
	GLuint VertexShaderID, GeometryShaderID, FragmentShaderID;

	if (!LoadShader(vertex_file_path, GL_VERTEX_SHADER, VertexShaderID))
		return false;

	if (!LoadShader(geometry_file_path, GL_GEOMETRY_SHADER, GeometryShaderID))
		return false;

	if (!LoadShader(fragment_file_path, GL_FRAGMENT_SHADER, FragmentShaderID))
		return false;

	// Link the program
	Printf("Linking program\n");
	ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, GeometryShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	GLint Result = GL_FALSE;
	int InfoLogLength;
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (!Result && InfoLogLength)
	{
		std::vector<char> ProgramErrorMessage(max(InfoLogLength, int(1)));
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		Printf("%s\n", &ProgramErrorMessage[0]);
		ret = false;
	}

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ret;
}


bool LoadShadersEx(const char* vertex_file_path, const char* ctrl_file_path, const char* eval_file_path, const char* fragment_file_path, GLuint& ProgramID)
{
	bool ret = true;
	GLuint VertexShaderID, CtrlShaderID, EvalShaderID, FragmentShaderID;

	if (!LoadShader(vertex_file_path, GL_VERTEX_SHADER, VertexShaderID))
		return false;

	if (!LoadShader(ctrl_file_path, GL_TESS_CONTROL_SHADER, CtrlShaderID))
		return false;

	if (!LoadShader(eval_file_path, GL_TESS_EVALUATION_SHADER, EvalShaderID))
		return false;

	if (!LoadShader(fragment_file_path, GL_FRAGMENT_SHADER, FragmentShaderID))
		return false;

	// Link the program
	Printf("Linking program\n");
	ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, CtrlShaderID);
	glAttachShader(ProgramID, EvalShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	GLint Result = GL_FALSE;
	int InfoLogLength;
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (!Result && InfoLogLength)
	{
		std::vector<char> ProgramErrorMessage(max(InfoLogLength, int(1)));
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		Printf("%s\n", &ProgramErrorMessage[0]);
		ret = false;
	}

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ret;
}

bool Graphics3D::Init(HWND window)
{
	mousePressed = false;
	lightPos.Set(0, 400, 800);
	if (!mGLS.Init())
		return false;

#ifdef USE_IMGUI
	// ********** ImGui ********************

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsClassic();

	// Setup Platform/Renderer bindings
	ImGui_ImplWin32_Init(window);
	ImGui_ImplOpenGL3_Init("#version 130");
#endif

	return true;
}

bool OpenGLSData::LoadNormalProgram()
{
	if (!LoadShaders("../Shaders/shader_phong_vertex.glsl", "../Shaders/shader_phong_fragment.glsl", program.programID))
		return false;

	// Get a handle for our uniforms; TODO: check each of them
	program.MatrixID = glGetUniformLocation(program.programID, "view_proj_matrix");
	program.shadowVPMatrixID = glGetUniformLocation(program.programID, "vp_shadow_matrix");
	program.ModelMatrixID = glGetUniformLocation(program.programID, "model_matrix");
	program.eyePosID = glGetUniformLocation(program.programID, "eye_position");
	program.LightDirID = glGetUniformLocation(program.programID, "light_position");
	program.diffColorID = glGetUniformLocation(program.programID, "material_kd");
	program.specColorID = glGetUniformLocation(program.programID, "material_ks");
	program.glossID = glGetUniformLocation(program.programID, "material_shininess");
	program.flipID = glGetUniformLocation(program.programID, "flip");
	program.flagsID = glGetUniformLocation(program.programID, "flags");

	return true;
}

bool OpenGLSData::LoadWireframeProgram()
{
	if (!LoadShadersEx("../Shaders/shader_phong_vertex.glsl", "../Shaders/shader_phong_geometry.glsl", "../Shaders/shader_phong_fragment.glsl", wireframeProgram.programID))
		return false;

	// Get a handle for our uniforms; TODO: check each of them
	wireframeProgram.MatrixID = glGetUniformLocation(wireframeProgram.programID, "view_proj_matrix");
	wireframeProgram.shadowVPMatrixID = glGetUniformLocation(wireframeProgram.programID, "vp_shadow_matrix");
	wireframeProgram.ModelMatrixID = glGetUniformLocation(wireframeProgram.programID, "model_matrix");
	wireframeProgram.eyePosID = glGetUniformLocation(wireframeProgram.programID, "eye_position");
	wireframeProgram.LightDirID = glGetUniformLocation(wireframeProgram.programID, "light_position");
	wireframeProgram.diffColorID = glGetUniformLocation(wireframeProgram.programID, "material_kd");
	wireframeProgram.specColorID = glGetUniformLocation(wireframeProgram.programID, "material_ks");
	wireframeProgram.glossID = glGetUniformLocation(wireframeProgram.programID, "material_shininess");
	wireframeProgram.flipID = glGetUniformLocation(wireframeProgram.programID, "flip");
	wireframeProgram.flagsID = glGetUniformLocation(wireframeProgram.programID, "flags");
	wireframeProgram.winScaleId = glGetUniformLocation(wireframeProgram.programID, "WIN_SCALE");

	return true;
}

bool OpenGLSData::Init()
{
	glGenVertexArrays(1, &vao);

	glGenBuffers(1, &vertexbuffer);  // TODO: remove
	glGenBuffers(1, &normalbuffer);
	glGenBuffers(1, &colorbuffer);
	glGenBuffers(1, &uvbuffer);
	glGenBuffers(1, &linesBuffer);
	glGenBuffers(1, &elementbuffer);

	if (!LoadNormalProgram())
		return false;

	if (!LoadWireframeProgram())
		return false;

	currProgram = nullptr;

	CreateSphere();

	// Set up shadows
	InitShadowMap();

	// Load shadow shaders
	if (!LoadShaders("../Shaders/shader_shadow_vertex.glsl", "../Shaders/shader_shadow_fragment.glsl", shadowProgramID))
		return false;

	shadowModelID = glGetUniformLocation(shadowProgramID, "model");

	float ext = 400.f;
	projShadow = Matrix4::Ortographic(-ext, ext, ext, -ext, -1000.f, 1000.f);

	CheckGlError();

	return true;
}

void Graphics3D::SetLightPos(const Vector3& v)
{
	lightPos = v;
	mGLS.SetViewShadow(Matrix4::LookAt(lightPos, Vector3(0, 0, 0), Vector3(0, 0, 1)));
}

void Graphics3D::SetContext()
{
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);

	CheckGlError();
}

void Graphics3D::DeInit()
{
	//wglMakeCurrent(NULL, NULL);
	//ReleaseDC(hWnd, hDC) ;
	//wglDeleteContext(hGLRC);

#ifdef USE_IMGUI
	// ImGui Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplWin32_Shutdown();
	if (ImGui::GetCurrentContext())
		ImGui::DestroyContext();
#endif
}

void Graphics3D::Resize(int width, int height)
{
	w = width;
	h = height;
	mGLS.SetProjection(Matrix4::Perspective(fov, (float)w / (float)h, n, f)); // TODO: pay attention to z-buffer accuracy
}

void Graphics3D::SetFlipNormals(bool val)
{
	mGLS.SetFlipNormals(val);
}

void OpenGLSData::SetFlipNormals(bool val)
{
	if (!isShadowPass)
		glUniform1i(currProgram->flipID, val ? -1 : 1);
}


void Graphics3D::SetFlags(int val)
{
	mGLS.SetFlags(val);
}

void OpenGLSData::SetFlags(int val)
{
	if (!isShadowPass) 
	{
		glUniform1i(currProgram->flagsID, val);
		flags = val;
	}
}

int Graphics3D::GetRenderFlags()
{
	return mGLS.GetFlags();
}

void Graphics3D::SetCulling(int val)
{
	glCullFace(val);
}

void Graphics3D::Viewport()
{
	glViewport(0, 0, w, h);
	mGLS.SetView(camera.View());
	mGLS.PrepareForDraw(lightPos, w, h);
}

void OpenGLSData::PrepareForDraw(const Vector3& lightPos, int w, int h)
{
	isShadowPass = false;
	const Matrix4 VP = Projection * View;

	glUseProgram(program.programID);

	// set uniform values
	glUniformMatrix4fv(program.MatrixID, 1, GL_FALSE, VP.GetData());
	glUniform3f(program.LightDirID, lightPos.X(), lightPos.Y(), lightPos.Z());
	glUniform1i(program.glossID, 15);
	glUniform1i(program.flipID, 1);
	glUniform1i(program.flagsID, 0);
	glUniform3f(program.diffColorID, 1, 1, 1);
	glUniform3f(program.specColorID, 0, 0, 0);

	glUseProgram(wireframeProgram.programID);

	glUniformMatrix4fv(wireframeProgram.MatrixID, 1, GL_FALSE, VP.GetData());
	glUniform3f(wireframeProgram.LightDirID, lightPos.X(), lightPos.Y(), lightPos.Z());
	glUniform1i(wireframeProgram.glossID, 15);
	glUniform1i(wireframeProgram.flipID, 1);
	glUniform1i(wireframeProgram.flagsID, 0);
	glUniform2f(wireframeProgram.winScaleId, w * 0.5f, h * 0.5f);
	glUniform3f(wireframeProgram.diffColorID, 1, 1, 1);
	glUniform3f(wireframeProgram.specColorID, 0, 0, 0);

	SetProgram(false);

	// shadow matrix
	// TODO: use bias matrix	
	const Matrix4 shadowVP = projShadow * viewShadow;
	glUniformMatrix4fv(currProgram->shadowVPMatrixID, 1, GL_FALSE, shadowVP.GetData());

	// shadow map
#if (RENDERER == OPENGL)
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, shadowMap.ID);
	GLint shadowMapID = glGetUniformLocation(currProgram->programID, "shadowMap");
	//glUniform1i(shadowMapID, 1); // FIXME: weird bug when changing to wireframe program

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
#endif

	glCullFace(GL_BACK);

	CheckGlError();

#ifdef USE_IMGUI
	// ******** ImGui *****************
	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
#endif
}

void Graphics3D::ViewportShadow()
{
	mGLS.PrepareForShadow();
}

void OpenGLSData::PrepareForShadow()
{
	isShadowPass = true;
	// set the shadow program
	glUseProgram(shadowProgramID);

	// TODO: store uniform locations
	glUniformMatrix4fv(glGetUniformLocation(shadowProgramID, "projShadow"), 1, GL_FALSE, projShadow.GetData());
	glUniformMatrix4fv(glGetUniformLocation(shadowProgramID, "viewShadow"), 1, GL_FALSE, viewShadow.GetData());

	// set the render target 
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFramebuffer);
	glViewport(0, 0, 1024, 1024);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glCullFace(GL_FRONT);

	CheckGlError();
}

void Graphics3D::BeginDraw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Graphics3D::EndDraw()
{
	SwapBuffers(hDC);
}

void Graphics3D::DrawAxes()
{
	mGLS.DrawAxes();
}

void OpenGLSData::DrawAxes()
{
	Matrix4 Model = Matrix4::Identity();
	glUniformMatrix4fv(currProgram->ModelMatrixID, 1, GL_FALSE, Model.GetData());
	Matrix4 MVP = Projection * View * Model;
	glUniformMatrix4fv(currProgram->MatrixID, 1, GL_FALSE, MVP.GetData());

	float l = 20.f;
	float lines[] = { 0, 0, 0, l, 0, 0, 0, 0, 0, 0, l, 0, 0, 0, 0, 0, 0, l };

	glBindBuffer(GL_ARRAY_BUFFER, linesBuffer);
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(Vector3), &lines[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Draw the triangles !
	glDrawArrays(GL_LINES, 0, 6);

	glDisableVertexAttribArray(0);
	CheckGlError();
}

void Graphics3D::MouseDown(int x, int y)
{
	mousePressed = true;
	mouseX = x;
	mouseY = y;
}

void Graphics3D::MouseUp(int x, int y)
{
	mousePressed = false;
}

void Graphics3D::MouseMove(int x, int y)
{
	if (!mousePressed)
		return;
	int dx = x - mouseX;
	int dy = y - mouseY;
	mouseX = x;
	mouseY = y;
	camera.Rotate(dx, dy);
}

void Graphics3D::MouseWheel(float delta)
{
	camera.Translate(delta, 0);
}

void OpenGLSData::CreateSphere()
{
	std::vector<PosNormal> vertices;
	std::vector<unsigned int> indices; // TODO: uint16 or less

	const int divsTheta = 10;
	const int divsPhi = 10;
	float theta = 0;
	const float dt = 2 * PI / divsTheta;
	const float dp = PI / (divsPhi - 1);
	std::vector<Vector2> uvs;

	for (int i = 0; i < divsTheta; i++)
	{
		float phi = -PI / 2;
		for (int j = 0; j < divsPhi; j++)
		{
			Vector3 v1(cos(theta) * cos(phi), sin(phi), sin(theta) * cos(phi)); // TODO: sin/cos of sum?
			uvs.push_back(Vector2(theta / (2 * PI), (phi + PI / 2) / PI));
			phi += dp;
			PosNormal pn;
			pn.v = v1;
			v1.Normalize();
			pn.n = v1;
			vertices.push_back(pn);
		}
		theta += dt;
	}

	for (int i = 0; i < divsTheta; i++)
	{
		for (int j = 0; j < divsPhi - 1; j++)
		{
			int ni = (i + 1) % divsTheta;
			int nj = (j + 1) % divsPhi;
			int i1 = (i * divsPhi + j);
			int i2 = (ni * divsPhi + j);
			int i3 = (i * divsPhi + nj);
			int i4 = (ni * divsPhi + nj);
			indices.push_back(i1);
			indices.push_back(i3);
			indices.push_back(i2);
			indices.push_back(i2);
			indices.push_back(i3);
			indices.push_back(i4);
		}
	}
	nSphereElements = (GLsizei)indices.size();

#ifdef USE_VAO
	// create the vertex array object (VAO)
	glGenVertexArrays(1, &sphereVAO);
	glBindVertexArray(sphereVAO);

	uint32 sphereVBO;
	glGenBuffers(1, &sphereVBO);
	glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(PosNormal), &vertices[0], GL_STATIC_DRAW);

	// vertex attribute array 0 - positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(PosNormal), (void*)0);
	// vertex attribute array 1 - normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(PosNormal), (void*)sizeof(Vector3));

	// uvs buffer
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(Vector2), &uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vector2), 0);

	// create index buffer object (IBO)
	uint32 sphereIBO;
	glGenBuffers(1, &sphereIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphereIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32), &indices[0], GL_STATIC_DRAW);

	glBindVertexArray(0);
#endif
}

void Graphics3D::DrawSphere(const Vector3& pos, float radius)
{
	mGLS.DrawSphere(pos, radius);
}

void OpenGLSData::DrawSphere(const Vector3& pos, float radius)
{
	Matrix4 Model = Matrix4::Translation(pos.X(), pos.Y(), pos.Z()) * Matrix4::Scale(radius, radius, radius);
	GL_CALL(glUniformMatrix4fv(isShadowPass ? shadowModelID : currProgram->ModelMatrixID, 1, GL_FALSE, Model.GetData()));

#ifdef USE_VAO
	GL_CALL(glBindVertexArray(sphereVAO));
#else
	uint32 sphereVBO;
	glGenBuffers(1, &sphereVBO);
	glBindBuffer(GL_ARRAY_BUFFER, sphereVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(PosNormal), &vertices[0], GL_STATIC_DRAW);

	// create index buffer object (IBO)
	uint32 sphereIBO;
	glGenBuffers(1, &sphereIBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sphereIBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32), &indices[0], GL_STATIC_DRAW);

	// vertex attribute array 0 - positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(PosNormal), (void*)0);
	// vertex attribute array 1 - normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(PosNormal), (void*)sizeof(Vector3));
#endif

	GL_CALL(glDrawElements(GL_TRIANGLES, nSphereElements, GL_UNSIGNED_INT, (void*)0));

#ifdef USE_VAO
	GL_CALL(glBindVertexArray(0));
#endif
}

void Graphics3D::DrawSphereTex(const Vector3& pos, float radius, const Matrix3& rot, Texture* tex)
{

}

void OpenGLSData::DrawSphereTex(const Vector3& pos, float radius, const Matrix3& rot, Texture* tex)
{
	Matrix4 Model = Matrix4(rot, pos) * Matrix4::Scale(radius, radius, radius);
	GL_CALL(glUniformMatrix4fv(isShadowPass ? shadowModelID : currProgram->ModelMatrixID, 1, GL_FALSE, Model.GetData()));

	glBindVertexArray(sphereVAO);

#if (RENDERER == OPENGL)
	if (!isShadowPass)
	{
		// no texture needed for shadow pass
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, tex->ID);
		glUniform1i(glGetUniformLocation(currProgram->programID, "map_color"), 0);
	}
#endif

	SetFlags(ShaderFlags::TEXTURED);
	SetColor(1, 1, 1);

	glDrawElements(GL_TRIANGLES, nSphereElements, GL_UNSIGNED_INT, (void*)0);

	SetFlags(ShaderFlags::NONE);

#ifdef USE_VAO
	glBindVertexArray(0);
#endif

	CheckGlError();
}

void Graphics3D::DrawTriangle(const Vector3& a, const Vector3& b, const Vector3& c, bool fill)
{
	mGLS.DrawTriangle(a, b, c, fill);
}

void OpenGLSData::DrawTriangle(const Vector3& a, const Vector3& b, const Vector3& c, bool fill)
{
	if (!fill)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); // for wireframe
	Matrix4 Model = Matrix4::Identity();
	glUniformMatrix4fv(currProgram->ModelMatrixID, 1, GL_FALSE, Model.GetData());
	//glUniformMatrix4fv(MatrixID, 1, GL_FALSE, Projection.GetData());

	static GLfloat buffer[9];
	for (int i = 0; i < 3; i++)
	{
		buffer[i] = a[i];
		buffer[i + 3] = b[i];
		buffer[i + 6] = c[i];
	}
	Vector3 n = cross(b - a, c - a);
	n.Normalize();
	static const GLfloat colors[] = {
		n.X(), n.Y(), n.Z(),
		n.X(), n.Y(), n.Z(),
		n.X(), n.Y(), n.Z(),
	};

	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(buffer), buffer, GL_STATIC_DRAW);
	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);
	// 2nd attribute buffer : colors
	glEnableVertexAttribArray(1);
	//glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, 3); // Starting from vertex 0; 3 vertices total -> 1 triangle

	//glDisableVertexAttribArray(0);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	CheckGlError();
}

void Graphics3D::DrawCube(const Vector3& pos, const Vector3& ext, const Matrix3& rot)
{
	mGLS.DrawCube(pos, ext, rot);
}

void OpenGLSData::DrawCube(const Vector3& pos, const Vector3& ext, const Matrix3& rot)
{
	Matrix4 Model = Matrix4(rot, pos) * Matrix4::Scale(0.5f * ext.X(), 0.5f * ext.Y(), 0.5f * ext.Z());
	GL_CALL(glUniformMatrix4fv(isShadowPass ? shadowModelID : currProgram->ModelMatrixID, 1, GL_FALSE, Model.GetData()));

	// TODO: do as for the sphere
	static float buffer[36 * 3];
	static float normals[36 * 3];
	float* ptr = buffer;
	float* ptrN = normals;
	for (int i = 0; i < 3; i++) // the current axis
	{
		for (int j = 0; j < 2; j++) // the direction of the axis
		{
			for (int k = 0; k < 2; k++) // the current triangle
			{
				Vector3 a, b, c;
				const int sgnDir = -1 + 2 * j;
				const int sgnTr = -1 + 2 * k;
				Vector3 n;
				a[i] = sgnDir; b[i] = sgnDir; c[i] = sgnDir;
				n[i] = sgnDir;
				int axis1 = (i + 1) % 3;
				int axis2 = (i + 2) % 3;
				if (sgnDir < 0)
				{
					a[axis1] = -1; a[axis2] = -1;
					b[axis1] = -sgnTr; b[axis2] = 1;
					c[axis1] = 1; c[axis2] = sgnTr;
				}
				else
				{
					a[axis1] = -1; a[axis2] = -1;
					b[axis1] = 1; b[axis2] = sgnTr;
					c[axis1] = -sgnTr; c[axis2] = 1;
				}

				ptr[0] = a[0]; ptr[1] = a[1]; ptr[2] = a[2]; ptr += 3;
				ptr[0] = b[0]; ptr[1] = b[1]; ptr[2] = b[2]; ptr += 3;
				ptr[0] = c[0]; ptr[1] = c[1]; ptr[2] = c[2]; ptr += 3;

				ptrN[0] = n[0]; ptrN[1] = n[1]; ptrN[2] = n[2]; ptrN += 3;
				ptrN[0] = n[0]; ptrN[1] = n[1]; ptrN[2] = n[2]; ptrN += 3;
				ptrN[0] = n[0]; ptrN[1] = n[1]; ptrN[2] = n[2]; ptrN += 3;
			}
		}
	}

	//glBindVertexArray(vao); // for GL 3.3 core

	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(buffer), buffer, GL_STATIC_DRAW);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normals), normals, GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
		1,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3); // Starting from vertex 0; 3 vertices total -> 1 triangle

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	CheckGlError();
}

void Graphics3D::DrawCapsule(const Vector3& pos, float radius, float height, const Matrix3& rot)
{
	mGLS.DrawCapsule(pos, radius, height, rot);
}

void OpenGLSData::DrawCapsule(const Vector3& pos, float radius, float height, const Matrix3& rot)
{
	Matrix4 Model = Matrix4(rot, pos);// * Matrix4::Scale(0.5f * ext.X(), 0.5f * ext.Y(), 0.5f * ext.Z());
	glUniformMatrix4fv(currProgram->ModelMatrixID, 1, GL_FALSE, Model.GetData());

	const int divsTheta = 10;
	const int divsPhi = 5;
	float theta = 0;
	const float dt = 2 * PI / divsTheta;
	const float dp = PI / (divsPhi - 1) / 2;
	std::vector<Vector2> uvs;
	std::vector<Vector3> vertices;
	std::vector<Vector3> normals;
	std::vector<uint32> indices;

	for (int i = 0; i < divsTheta; i++)
	{
		float phi = 0;//PI / 2;
		for (int j = 0; j < divsPhi; j++)
		{
			Vector3 v1(cos(theta) * cos(phi), sin(phi), sin(theta) * cos(phi)); // TODO: sin/cos of sum?
			uvs.push_back(Vector2(theta / (2 * PI), (phi + PI / 2) / PI));
			phi += dp;
			vertices.push_back(v1 * radius + Vector3(0, height, 0));
			v1.Normalize();
			normals.push_back(v1);
		}
		theta += dt;
	}
	size_t half = vertices.size();

	theta = 0;
	for (int i = 0; i < divsTheta; i++)
	{
		float phi = 0;//-PI / 2;
		for (int j = 0; j < divsPhi; j++)
		{
			Vector3 v1(cos(theta) * cos(phi), sin(phi), sin(theta) * cos(phi)); // TODO: sin/cos of sum?
			uvs.push_back(Vector2(theta / (2 * PI), (phi + PI / 2) / PI));
			phi -= dp;
			vertices.push_back(v1 * radius - Vector3(0, height, 0));
			v1.Normalize();
			normals.push_back(v1);
		}
		theta += dt;
	}

	for (int i = 0; i < divsTheta; i++)
	{
		for (int j = 0; j < divsPhi - 1; j++)
		{
			int ni = (i + 1) % divsTheta;
			int nj = (j + 1) % divsPhi;
			int i1 = (i * divsPhi + j);
			int i2 = (ni * divsPhi + j);
			int i3 = (i * divsPhi + nj);
			int i4 = (ni * divsPhi + nj);
			indices.push_back(i1);
			indices.push_back(i3);
			indices.push_back(i2);
			indices.push_back(i2);
			indices.push_back(i3);
			indices.push_back(i4);

			indices.push_back((uint32)(i1 + half));
			indices.push_back((uint32)(i2 + half));
			indices.push_back((uint32)(i3 + half));

			indices.push_back((uint32)(i2 + half));
			indices.push_back((uint32)(i4 + half));
			indices.push_back((uint32)(i3 + half));
		}
	}

	for (int i = 0; i < divsTheta; i++)
	{
		int ni = (i + 1) % divsTheta;
		indices.push_back((uint32)(i * divsPhi));
		indices.push_back((uint32)(ni * divsPhi));
		indices.push_back((uint32)(i * divsPhi + half));

		indices.push_back((uint32)(ni * divsPhi + half));
		indices.push_back((uint32)(i * divsPhi + half));
		indices.push_back((uint32)(ni * divsPhi));
	}

	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vector3), &vertices[0], GL_STATIC_DRAW);

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	//glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(Vector3), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(
		1,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32), &indices[0], GL_STATIC_DRAW);

	glDrawElements(GL_TRIANGLES, (GLsizei)indices.size(), GL_UNSIGNED_INT, (void*)0);

	//glDisableVertexAttribArray(0);
	//glDisableVertexAttribArray(1);

	CheckGlError();
}

void Graphics3D::DrawLine(const Vector3& a, const Vector3& b)
{
	mGLS.DrawLine(a, b);
}

void Graphics3D::DrawLines(const std::vector<Vector3>& points)
{
	mGLS.DrawLines(points);
}

void OpenGLSData::DrawLine(const Vector3& a, const Vector3& b)
{
	Matrix4 Model = Matrix4::Identity();
	glUniformMatrix4fv(currProgram->ModelMatrixID, 1, GL_FALSE, Model.GetData());

	float l = 20.f;
	float lines[] = { a.X(), a.Y(), a.Z(), b.X(), b.Y(), b.Z() };

	glBindBuffer(GL_ARRAY_BUFFER, linesBuffer);
	glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(Vector3), &lines[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	CheckGlError();

	glDrawArrays(GL_LINES, 0, 2);
	CheckGlError();

	glDisableVertexAttribArray(0);
	CheckGlError();
}

void OpenGLSData::DrawLines(std::vector<Vector3> points)
{
	Matrix4 Model = Matrix4::Identity();
	glUniformMatrix4fv(currProgram->ModelMatrixID, 1, GL_FALSE, Model.GetData());

	glBindBuffer(GL_ARRAY_BUFFER, linesBuffer);
	glBufferData(GL_ARRAY_BUFFER, points.size() * sizeof(Vector3), points.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	CheckGlError();

	glDrawArrays(GL_LINES, 0, (GLsizei)points.size());
	CheckGlError();

	glDisableVertexAttribArray(0);
	CheckGlError();
}

void Graphics3D::DrawWireCube(const Vector3& pos, const Vector3& scale)
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDisable(GL_CULL_FACE);
	DrawCube(pos, scale, Matrix3());
	glEnable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	CheckGlError();
}

void Graphics3D::DrawMesh(const SimpleMesh& mesh)
{
	mGLS.DrawMesh(mesh);
}

void OpenGLSData::DrawMesh(const SimpleMesh& mesh)
{
	if (mesh.vertices.empty())
		return;

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, mesh.vertices.size() * sizeof(Vector3), &mesh.vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, mesh.normals.size() * sizeof(Vector3), &mesh.normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh.indices.size() * sizeof(uint32), &mesh.indices[0], GL_STATIC_DRAW);

	Matrix4 Model = Matrix4(mesh.rot, mesh.pos); //Matrix4::Translation(mesh.pos.X(), mesh.pos.Y(), mesh.pos.Z()); // TODO: Vector3 version
	glUniformMatrix4fv(!isShadowPass ? currProgram->ModelMatrixID : shadowModelID, 1, GL_FALSE, Model.GetData());

	glDrawElements(GL_TRIANGLES, (GLsizei)mesh.indices.size(), GL_UNSIGNED_INT, (void*)0);

	CheckGlError();
}

void Graphics3D::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, const Matrix4* model)
{
	mGLS.DrawMesh(vertices, normals, indices, model);
}

void OpenGLSData::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, const Matrix4* model)
{
	if (vertices.empty())
		return;

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vector3), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(Vector3), &normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32), &indices[0], GL_STATIC_DRAW);

	Matrix4 Model;
	if (model != nullptr)
		Model = *model;
	else
		Model = Matrix4::Identity();
	glUniformMatrix4fv(!isShadowPass ? currProgram->ModelMatrixID : shadowModelID, 1, GL_FALSE, Model.GetData());

#ifndef PN_TRIANGLES
	glDrawElements(GL_TRIANGLES, (GLsizei)indices.size(), GL_UNSIGNED_INT, (void*)0);
#else
	GLint location = glGetUniformLocation(programID, "tess_factor");
	GL_CALL(glUniform1i(location, 1));
	glDrawElements(GL_PATCHES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);
#endif

	CheckGlError();
}

void Graphics3D::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<Vector3>& colors, const std::vector<uint32>& indices, float scale)
{
	mGLS.DrawMesh(vertices, normals, colors, indices, scale);
}

void OpenGLSData::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<Vector3>& colors, const std::vector<uint32>& indices, float scale)
{
	if (vertices.empty())
		return;

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vector3), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(Vector3), &normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), 0);

	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, colors.size() * sizeof(Vector3), &colors[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32), &indices[0], GL_STATIC_DRAW);

	CheckGlError();

	Matrix4 Model = Matrix4::Scale(scale, scale, scale);
	glUniformMatrix4fv(!isShadowPass ? currProgram->ModelMatrixID : shadowModelID, 1, GL_FALSE, Model.GetData());
#ifndef PN_TRIANGLES
	glDrawElements(GL_TRIANGLES, (GLsizei)indices.size(), GL_UNSIGNED_INT, (void*)0);
#else
	GLint location = glGetUniformLocation(programID, "tess_factor");
	GL_CALL(glUniform1i(location, 1));
	glDrawElements(GL_PATCHES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);
#endif

	CheckGlError();
}

void Graphics3D::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, const std::vector<Vector2>& uvs, Texture* tex)
{
	mGLS.DrawMesh(vertices, normals, indices, uvs, tex);
}

void OpenGLSData::DrawMesh(const std::vector<Vector3>& vertices, const std::vector<Vector3>& normals, const std::vector<uint32>& indices, const std::vector<Vector2>& uvs, Texture* tex)
{
	if (vertices.empty())
		return;

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vector3), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(Vector3), &normals[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vector3), 0);

	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(Vector2), &uvs[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vector2), 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32), &indices[0], GL_STATIC_DRAW);

	Matrix4 Model = Matrix4::Identity();
	glUniformMatrix4fv(!isShadowPass ? currProgram->ModelMatrixID : shadowModelID, 1, GL_FALSE, Model.GetData());
	
	// FIXME
	//Vector3 eye = camera.GetPosition();
	//glUniform3f(eyePosID, eye.X(), eye.Y(), eye.Z());

#if (RENDERER == OPENGL)
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex->ID);
	glUniform1i(glGetUniformLocation(currProgram->programID, "map_color"), 0);
#endif

#ifndef PN_TRIANGLES
	glDrawElements(GL_TRIANGLES, (GLsizei)indices.size(), GL_UNSIGNED_INT, (void*)0);
#else
	GLint location = glGetUniformLocation(programID, "tess_factor");
	GL_CALL(glUniform1i(location, 1));
	glDrawElements(GL_PATCHES, indices.size(), GL_UNSIGNED_SHORT, (void*)0);
#endif

	CheckGlError();
}

void Graphics3D::DrawTetrahedronEdgeAsBBCurve(const Vector3& c1, const Vector3& c2, const size_t nodesperedge, const Vector3* edge)
{
	mGLS.DrawTetrahedronEdgeAsBBCurve(c1, c2, nodesperedge, edge);
}

void OpenGLSData::DrawTetrahedronEdgeAsBBCurve(const Vector3& c1, const Vector3& c2, const size_t nodesperedge, const Vector3* edge)
{
	std::vector<Vector3> nodes; nodes.resize(2 + nodesperedge);
	nodes[0] = c1;
	for (size_t i = 0; i < nodesperedge; i++)
	{
		nodes[1 + i] = edge[i];
	}
	nodes[nodes.size() - 1] = c2;

	auto binom = [](int n, int k) -> int
	{
		int val = 1;

		// Utilize that C(n, k) = C(n, n-k) 
		if (k > n - k)
			k = n - k;

		for (int i = 0; i < k; ++i)
		{
			val *= (n - i);
			val /= (i + 1);
		}

		return val;
	};

	auto evalBB = [&binom](float t, size_t noNodes, const Vector3* nodes) -> Vector3
	{
		// t \in [0,1]
		// order = noNodes - 1
		int order = (int)(noNodes - 1);
		Vector3 val(0, 0, 0);
		for (size_t i = 0; i < noNodes; i++)
		{
			val += nodes[i] * float(binom(order, (int)i)) * std::pow((1.f - t), order - i) * std::pow(t, i);
		}
		return val;
	};

	const size_t subdivisions = 10; // ie how many line segments, min(1)
	std::vector<Vector3> vtx;
	vtx.resize(subdivisions * 2);
	for (size_t s = 0; s < subdivisions; s++)
	{
		vtx[s * 2] = evalBB(float(s) / float(subdivisions), nodes.size(), &nodes[0]);
		if (s > 0) vtx[(s * 2) - 1] = vtx[s * 2];

		if ((s + 1) == subdivisions)
		{
			vtx[(s*2) + 1] = evalBB(1.f, nodes.size(), &nodes[0]);
		}
	}

	// Draw the line buffer
	Matrix4 Model = Matrix4::Identity();
	glUniformMatrix4fv(!isShadowPass ? currProgram->ModelMatrixID : shadowModelID, 1, GL_FALSE, Model.GetData());

	glBindBuffer(GL_ARRAY_BUFFER, linesBuffer);
	glBufferData(GL_ARRAY_BUFFER, vtx.size() * sizeof(Vector3), &vtx[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Draw the triangles !
	glDrawArrays(GL_LINES, 0, (GLsizei)vtx.size());

	glDisableVertexAttribArray(0);
	CheckGlError();
}

void Graphics3D::DrawTetrahedron(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& v3)
{
	mGLS.DrawTetrahedron(v0, v1, v2, v3);
}

void OpenGLSData::DrawTetrahedron(const Vector3& v0, const Vector3& v1, const Vector3& v2, const Vector3& v3)
{
	Vector3 vtx[12];
	vtx[0] = v0;
	vtx[1] = v1;
	vtx[2] = v1;
	vtx[3] = v2;
	vtx[4] = v2;
	vtx[5] = v0;
	vtx[6] = v3;
	vtx[7] = v0;
	vtx[8] = v3;
	vtx[9] = v1;
	vtx[10] = v3;
	vtx[11] = v2;

	Matrix4 Model = Matrix4::Identity();
	glUniformMatrix4fv(!isShadowPass ? currProgram->ModelMatrixID : shadowModelID, 1, GL_FALSE, Model.GetData());

	glBindBuffer(GL_ARRAY_BUFFER, linesBuffer);
	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(Vector3), &vtx[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	// Draw the triangles !
	glDrawArrays(GL_LINES, 0, 12);

	glDisableVertexAttribArray(0);
	CheckGlError();
}

bool OpenGLSData::InitShadowMap()
{
	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.	
	glGenFramebuffers(1, &shadowFramebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFramebuffer);

	// Depth texture. Slower than a depth buffer, but you can sample it later in your shader
	GLuint depthTexture;
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

#if (RENDERER == OPENGL)
	shadowMap.ID = depthTexture;
	shadowMap.width = 1024;
	shadowMap.height = 1024;
#endif

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	glDrawBuffer(GL_NONE); // No color buffer is drawn to.

	// Always check that our framebuffer is ok
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		return false;

	CheckGlError();
	return true;
}

void Graphics3D::SetColor(float r, float g, float b)
{
	color.Set(r, g, b);
	mGLS.SetColor(r, g, b);
}

void OpenGLSData::SetColor(float r, float g, float b)
{
	if (isShadowPass)
		return;
	glUniform3f(currProgram->diffColorID, r, g, b);
}

void Graphics3D::GetScreenCoords(const Vector3& point, float& sx, float& sy)
{
	Matrix4 VP = mGLS.GetProjection() * mGLS.GetView();
	Vector4 p(point);
	Vector4 proj = VP * p;
	float px = proj.x / proj.w;
	float py = proj.y / proj.w;
	float pz = proj.z / proj.w;
	if (pz < 0)
		return;
	sx = 0.5f * (1.0f + px) * w;
	sy = 0.5f * (1.0f - py) * h;
}

void Graphics3D::SetRenderMode(int val)
{ 
	mRenderMode = val; 
	if (val == RM_WIREFRAME)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		mGLS.SetProgram(false);
		SetFlags(ShaderFlags::MONOCHROME);
	}
	else if (val == RM_WIREFRAME_ON_SHADED)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		mGLS.SetProgram(true);
		SetFlags(ShaderFlags::WIREFRAME);
	}
	else if (val == RM_SHADED)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		mGLS.SetProgram(false);
		SetFlags(ShaderFlags::NONE);
	}
}

void OpenGLSData::SetProgram(bool wireframe)
{
	currProgram = wireframe ? &wireframeProgram : &program;
	glUseProgram(currProgram->programID);
}

#endif