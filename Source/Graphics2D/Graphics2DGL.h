#ifndef GRAPHICS2DGL_H
#define GRAPHICS2DGL_H

#include <Graphics2D/Graphics2D.h>

class Graphics2DGL : public Graphics2D
{
	public:

#if defined(WIN32) && (RENDERER == OPENGL)
		// TODO: rename
		HGLRC hGLRC;

		HDC hDC;

#elif (RENDERER == OPENGLES) && !defined(ANDROID_NDK)
		EGLDisplay display;

		EGLSurface window;
#endif
	
	protected:

		void InitPF(void);

};

#	include "Graphics2DGL.inl"

#endif // GRAPHICS2DGL_H