inline void Graphics2D::Translate(float x, float y) 
{ 
}

inline void Graphics2D::Rotate(float angle) 
{ 
}

inline void Graphics2D::Scale(float s, float t) 
{
	rescale = rescale || (sx != s && sy != t);
	sx = s; sy = t;
}

inline void Graphics2D::PushMatrix() 
{ 
}

inline void Graphics2D::PopMatrix() 
{ 
}

inline void Graphics2D::SetColor(uint32 color)
{
	pen->SetColor(Gdiplus::Color(color));
	brush->SetColor(Gdiplus::Color(color));
}

inline void Graphics2D::SetLineWidth(float w)
{
	pen->SetWidth(w);
}

inline void Graphics2D::SetLineStyle(int val)
{
	if (val > 0)
	{
		pen->SetEndCap(Gdiplus::LineCapArrowAnchor);
	}
	else
	{
		pen->SetEndCap(Gdiplus::LineCapNoAnchor);
	}
}


inline void Graphics2D::DrawLine(float x1, float y1, float x2, float y2)
{	
	frameGdi->DrawLine(pen, x1, y1, x2, y2);
}

inline void Graphics2D::DrawCircle(float x, float y, float radius, float arcLen)
{
	frameGdi->DrawEllipse(pen, x - radius, y - radius, 2 * radius, 2 * radius);
}

inline void Graphics2D::SetFiltering(Texture::FilteringType val)
{
	Texture::SetFiltering(val);
	rescale = true;
}

inline void Graphics2D::ResetTransform() {}

inline void Graphics2D::FillCircle(float x, float y, float radius, float arcLen)
{
	frameGdi->FillEllipse(brush, x - radius, y - radius, 2 * radius, 2 * radius);
}

inline void Graphics2D::SetContext() {}

inline void Graphics2D::Viewport() {}

inline void Graphics2D::SetBgColor(uint32 c)
{
	bgColor[0] = ((c >> 16) & 0xff);
	bgColor[1] = ((c >> 8) & 0xff);
	bgColor[2] = (c & 0xff);
}

inline void Graphics2D::DrawRect(float x, float y, float width, float height)
{
	frameGdi->DrawRectangle(pen, x, y, width, height);
}

