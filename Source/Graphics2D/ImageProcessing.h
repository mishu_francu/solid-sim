#ifndef IMAGE_PROCESSING_H
#define IMAGE_PROCESSING_H

#include <Math/Matrix.h>
#include <Engine/Profiler.h>
#include "Color.h"

// TODO: namespace

inline int floor(int s)
{
	int x = (int)s;
	if (s < 0) x--;
	return x;
}

struct NearestNeighbourResampler : Resampler
{
	NearestNeighbourResampler(const uint8* ptr, int w, int h, int p) : Resampler(ptr, w, h, p) { }
	GRB operator()(float s, float t);
};

struct BilinearResampler : Resampler
{
	BilinearResampler(const uint8* ptr, int w, int h, int p) : Resampler(ptr, w, h, p) { }
	GRB operator()(float s, float t);
};

struct CatmullRom
{
	GRBf operator()(float s, GRBf p[]);
};

struct Cubic1D
{
	GRBf operator()(float s, GRBf p[]);
};

template<class Interpolator = CatmullRom>
struct BicubicResampler1D : Resampler
{
	Interpolator interp;
	BicubicResampler1D(const uint8* ptr, int w, int h, int p) : Resampler(ptr, w, h, p) { }
	GRB operator()(float s, float t);
};

static float b[] = {
		1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
		0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	   -3,  3,  0,  0, -2, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
		2, -2,  0,  0,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
		0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  0,
		0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,
		0,  0,  0,  0,  0,  0,  0,  0, -3,  3,  0,  0, -2, -1,  0,  0,
		0,  0,  0,  0,  0,  0,  0,  0,  2, -2,  0,  0,  1,  1,  0,  0,
	   -3,  0,  3,  0,  0,  0,  0,  0, -2,  0, -1,  0,  0,  0,  0,  0,
		0,  0,  0,  0, -3,  0,  3,  0,  0,  0,  0,  0, -2,  0, -1,  0,
		9, -9, -9,  9,  6,  3, -6, -3,  6, -6,  3, -3,  4,  2,  2,  1,
	   -6,  6,  6, -6, -3, -3,  3,  3, -4,  4, -2,  2, -2, -2, -1, -1,
		2,  0, -2,  0,  0,  0,  0,  0,  1,  0,  1,  0,  0,  0,  0,  0,
		0,  0,  0,  0,  2,  0, -2,  0,  0,  0,  0,  0,  1,  0,  1,  0,
	   -6,  6,  6, -6, -4, -2,  4,  2, -3,  3, -3,  3, -2, -1, -2, -1,
		4, -4, -4,  4,  2,  2, -2, -2,  2, -2,  2, -2,  1,  1,  1,  1
};

struct BicubicResampler : Resampler
{
	// TODO: MatrixN
	MatrixSqr<GRBf, COLUMN> p;
	std::vector<GRBf> beta;
	const MatrixSqr<float> B;
	MatrixSqr<GRBf> A;
	std::vector<float> sBar, tBar; // TODO: vector4

	BicubicResampler(const uint8* ptr, int w, int h, int p) 
		: Resampler(ptr, w, h, p)
		, p(4)
		, beta(16)
		, B(16, b)
		, A(4)
		, sBar(4)
		, tBar(4)
	{ 
	}

	GRB operator()(float s, float t);
};

float* Cubic(int& size, float scale, float a);

void ProcessImage(const uint8* in, uint8* out, int width, int height, int components);
void ProcessImageSeparable(const uint8* in, uint8* out, int width, int height, float scale);

#ifdef WIN32
void ConvertToSSE(const void* in, __m128* out, int width, int height);
void ProcessImageSeparable_SSE(const __m128* in, uint8* out, int width, int height, float scale);
#endif

// TODO: componets as template param?
template<class Resampler>
uint8* ResizeImage(const uint8* in, int& width, int& height, float scale, int pitch)
{
	MEASURE_TIME("ResizeImage");
	// width must be multiple of 4 for OpenGL/GPU
	int w = (int)(scale * width);// + 0.5f);
	int k = w / 4;
	int n = width / 4;
	scale = (float)k / (float)n;
	w = 4 * k;
	int h = (int)(scale * height + 0.5f);
	GRB* out = new GRB[w * h];

	Resampler resample(in, width, height, pitch);
	const uint8* inPtr = in;
	GRB* outPtr = out;
	float invS = 1.f / scale;
	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			float s = x * invS;
			float t = y * invS;
			*outPtr = resample(s, t);
			outPtr++;
		}
	}

	width = w;
	height = h;
	return (uint8*)out;
}
#endif