#include <Engine/Base.h>

#if (RENDERER == GDIPLUS)

#include <Graphics2D/Graphics2D.h>

bool Graphics2D::Init(HWND window, HINSTANCE instance)
{
	// Initialize GDI+.
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;	
	if (GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL) != Gdiplus::Status::Ok)
		return false;
	hWnd = window;
	
	pen = new Gdiplus::Pen(Gdiplus::Color(255, 255, 255, 255));
	Gdiplus::FontFamily fontFamily(L"Lucida Console");
	font = new Gdiplus::Font(&fontFamily, 12, Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);
	brush = new Gdiplus::SolidBrush(Gdiplus::Color(255, 255, 255, 255));
	
	return true;
}

void Graphics2D::Resize(int width, int height)
{
	w = width;
	h = height;

	SAFE_DELETE(gdi);
	SAFE_DELETE(frameGdi);
	SAFE_DELETE(frameBmp);

	hDC = GetDC(hWnd);
	gdi = new Gdiplus::Graphics(hDC); // TODO: other constructors
	Texture::graphics = gdi;
	ReleaseDC(hWnd, hDC);

	frameBmp = new Gdiplus::Bitmap(w, h);
	frameGdi = new Gdiplus::Graphics(frameBmp);

	// enable anti-aliasing
	frameGdi->SetSmoothingMode(Gdiplus::SmoothingModeAntiAlias);
}

void Graphics2D::DeInit()
{
	delete font;
	delete pen;
	SAFE_DELETE(frameGdi);
	SAFE_DELETE(frameBmp);
	SAFE_DELETE(gdi);
}

void Graphics2D::BeginDraw()
{
	frameGdi->Clear(Gdiplus::Color(255, bgColor[0], bgColor[1], bgColor[2]));
}

void Graphics2D::EndDraw()
{
	gdi->DrawImage(frameBmp, 0, 0);
	rescale = false;
}

void Graphics2D::DrawImage(Texture* image, float x_dest, float y_dest, float scale, int anchor)
{
	if (image->GetAlpha() == 0)
		return;
	if (image->GetAlpha() == 255)
	{
		if (rescale)
		{
			image->Rescale(sx, sy);
		}
		frameGdi->DrawCachedBitmap(image->cache, x_dest, y_dest);
	}
	else
	{
		Gdiplus::ColorMatrix ClrMatrix = { 
					1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 0.0f, image->GetAlpha() / 255.f, 0.0f,
					0.0f, 0.0f, 0.0f, 0.0f, 1.0f
		};
		Gdiplus::ImageAttributes ImgAttr;
		ImgAttr.SetColorMatrix(&ClrMatrix, Gdiplus::ColorMatrixFlagsDefault, Gdiplus::ColorAdjustTypeBitmap);
		frameGdi->DrawImage(image->bmp, Gdiplus::RectF(x_dest, y_dest, sx * image->Width(), sy * image->Height()), 0, 0, 
                  image->Width(), image->Height(), Gdiplus::UnitPixel, &ImgAttr);
	}
}

void Graphics2D::DrawRegion(Texture* image, float x_dest, float y_dest, int x_src, int y_src, 
	int width, int height, float scale, int anchor)
{
}

void Graphics2D::FillRect(float x, float y, float width, float height)
{
	frameGdi->FillRectangle(brush, x, y, width, height);
}

void Graphics2D::DrawString(float x, float y, const char* text)
{
	Gdiplus::PointF pointF(x, y - 10);
	wchar_t ucText[256];
	const int len = strlen(text) + 1;
	MultiByteToWideChar(CP_ACP, 0, text, len, ucText, len);
	frameGdi->DrawString(ucText, -1, font, pointF, brush);
}

// TODO: move to common
void Graphics2D::DrawFormattedString(float x, float y, const char* text, ...)
{
	va_list arg_list;
	char str[256];

	va_start(arg_list, text);
	vsprintf_s(str, 256, text, arg_list);
	va_end(arg_list);

	DrawString(x, y, str);
}

void Graphics2D::DrawLineMesh(const LineMesh& mesh)
{
	ASSERT((mesh.indices.size() & 1) == 0)
	for (int i = 0; i < mesh.indices.size(); i += 2)
	{
		int i0 = mesh.indices[i];
		int i1 = mesh.indices[i + 1];
		const Vector2* v0 = (const Vector2 * )(mesh.vertices + i0 * mesh.stride);
		const Vector2* v1 = (const Vector2*)(mesh.vertices + i1 * mesh.stride);
		DrawLine(*v0, *v1);
	}
}

#endif
