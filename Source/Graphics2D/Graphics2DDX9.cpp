#include <Engine/Base.h>

#if (RENDERER == DIRECTX)

#include "Graphics2D.h"
#include "Texture.h"

struct HUD_VERTEX
{
	float x, y, z;
	uint32 color;
	float tu, tv;
public:
	HUD_VERTEX(){}

	HUD_VERTEX(float _x, float _y, float _z, uint32 _color, float _tu, float _tv)
	{
		x = _x;
		y = _y;
		z = _z;
		color = _color;
		tu = _tu;
		tv = _tv;
	}
};

#define HUD_VERTEX_FVF (D3DFVF_XYZ| D3DFVF_DIFFUSE | D3DFVF_TEX1)

struct VertexColor
{
	float x, y, z;
	uint32 color;
public:
	VertexColor(){}

	VertexColor(float _x, float _y, float _z, uint32 _color)
	{
		x = _x;
		y = _y;
		z = _z;
		color = _color;
	}
};

#define FVF_VERTEX_COLOR (D3DFVF_XYZ| D3DFVF_DIFFUSE )


LPDIRECT3DDEVICE9 Graphics2D::device = NULL;

D3DXMATRIX Graphics2D::Identity;

bool Graphics2D::Init(HWND window)
{
	hWnd = window;

	direct3D9 = Direct3DCreate9(D3D_SDK_VERSION);
	ASSERT(direct3D9 != NULL);

	D3DPRESENT_PARAMETERS d3dpp; 
	ZeroMemory( &d3dpp, sizeof(d3dpp) );
	d3dpp.Windowed = TRUE;
	d3dpp.hDeviceWindow = window;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;

	HRESULT hr = direct3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                  D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                                  &d3dpp, &device);
	ASSERT(SUCCEEDED(hr));

#ifndef USE_D3DXSPRITE
	// create the vertex buffer for displaying sprites
	m_pVB = NULL;
    if( FAILED( device->CreateVertexBuffer( 4 * sizeof( HUD_VERTEX ),
                                                  D3DUSAGE_WRITEONLY, HUD_VERTEX_FVF,
                                                  D3DPOOL_MANAGED, &m_pVB, NULL ) ) )
	{
		MessageBox( NULL, L"Could not create vertexbuffer.bmp:\nSDKGraphics2D::InitDeviceObjects", L"Error", MB_OK );
		return false;
	}
#endif

	hr = D3DXCreateFont(device, 15, 0, FW_BOLD, 1, FALSE, DEFAULT_CHARSET,
		OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE,
		L"Arial", &font);

	ASSERT(SUCCEEDED(hr));

	D3DXCreateSprite(device, &d3dxSprite);

	ASSERT(SUCCEEDED(hr));

	fillColor = 0xff007f7f;
	return true;
}

void Graphics2D::BeginDraw()
{
	device->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0,128,128), 1.0f, 0);	
	device->BeginScene();
	d3dxSprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE);
}

void Graphics2D::EndDraw()
{
	d3dxSprite->End();
	device->EndScene();
	device->Present( NULL, NULL, NULL, NULL );
}

void Graphics2D::DeInit()
{
	if (d3dxSprite != NULL)
		d3dxSprite->Release();
	if (font != NULL)
		font->Release();
	if (device != NULL)
		device->Release();
	if (direct3D9 != NULL)
		direct3D9->Release();
}

void Graphics2D::Resize(int width, int height)
{
	w = width;
	h = height;
#ifndef XUSE_D3DXSPRITE
	D3DXMatrixIdentity(&Identity);
	
	D3DXMatrixIdentity(&transformMatrix);
	Translate(-1, 1);
	Scale(2.0f / width, - 2.0f / height);

	//D3DXMatrixTranslation(&tempMatrix, center.x, center.y, 0);

	device->SetTransform(D3DTS_VIEW, &Identity);
	device->SetTransform(D3DTS_PROJECTION, &transformMatrix);
	device->SetTransform(D3DTS_WORLD, &Identity);

	D3DXMatrixIdentity(&transformMatrix);
	D3DXMatrixIdentity(&tempMatrix);

	// Turn off culling
    device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
    // Turn off D3D lighting
    device->SetRenderState(D3DRS_LIGHTING, FALSE);
	device->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	device->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	device->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	device->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

	device->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
#endif
}

void Graphics2D::DrawRegion(const Texture* image, float x_dest, float y_dest, int x_src, int y_src, 
	int width, int height, int transform, int anchor)
{
	PushMatrix();
	SetColor(0xffffffff);

	ASSERT( x_src < image->Width());
	ASSERT( y_src < image->Height());

	//remember original xsrc & ysrc
	int16 xsrc = x_src;
	int16 ysrc = y_src;
	
	//if we're before the image move to its begining
	if(xsrc < 0)
		xsrc = 0;
	if(ysrc < 0)
		ysrc = 0;
	
	//if we moved xsrc or ysrc we need to update x_dst or y_dst & width or height
	x_dest += ( xsrc - x_src );
	y_dest += ( ysrc - y_src );
	width  -= ( xsrc - x_src );
	height -= ( ysrc - y_src );
	
	//adjust x_dest & y_dest based on align request
	if( anchor & Align::Right )
		x_dest -= width;
	if( anchor & Align::Bottom )
		y_dest -= height;
	if( anchor & Align::HCenter )
		x_dest -= width>>1;
	if( anchor & Align::VCenter )
		y_dest -= height>>1;

	//if ((x_dest + width < 0 || x_dest > SCREEN_WIDTH || y_dest + height < 0 || y_dest > SCREEN_HEIGHT))
	//	return;

	Translate(x_dest, y_dest);

	if( (transform & Transform::Rot90)!=0 )
	{
		Translate((float)height, 0.0f);
		Rotate(ANGLE(90));
	}
	if( (transform & Transform::FlipX)!=0 )
	{
		Scale(-1, 1);				
		Translate(-(float)width, 0.0f);
	}
	if( (transform & Transform::FlipY)!=0 )
	{
		Scale(1, -1);
		Translate(0, -(float)height);
	}

#ifndef USE_D3DXSPRITE
	HUD_VERTEX scrv2[4];
	const float imgW = (float)image->Width();
	const float imgH = (float)image->Height();
	float minX  = (x_src + 0.5f) / imgW;
	float minY  = (y_src + 0.5f) / imgH;
	float maxX  = (x_src + width - 0.5f) / imgW;
	float maxY  = (y_src + height - 0.5f) / imgH;

	const float w = (float)width;
	const float h = (float)height;
	scrv2[0] = HUD_VERTEX( 0.0f, 0.0f, 1.0f, color, minX, minY);
	scrv2[1] = HUD_VERTEX( w, 0.0f, 1.0f, color, maxX, minY);
	scrv2[2] = HUD_VERTEX( 0.0f, h, 1.0f, color, minX, maxY);
	scrv2[3] = HUD_VERTEX( w, h, 1.0f, color, maxX, maxY);
	
	device->SetFVF(HUD_VERTEX_FVF);
	device->SetTexture(0, image->D3DTexture);

	device->SetTransform(D3DTS_WORLD, &transformMatrix);
	device->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, &scrv2, sizeof(HUD_VERTEX));
#else
	RECT srcRect;
	SetRect(&srcRect, x_src, y_src, x_src + width, y_src + height);
	D3DXVECTOR3 pos(x_dest, y_dest, 0);
	d3dxSprite->SetTransform(&transformMatrix);
	d3dxSprite->Draw(image->D3DTexture, &srcRect, NULL, &pos, 0xffffffff);
#endif
	PopMatrix();
}

void Graphics2D::DrawImage(const Texture* image, float x_dest, float y_dest, int transform, int anchor)
{
	DrawRegion(image, x_dest, y_dest, 0, 0, image->Width(), image->Height(), transform, anchor);
}

void Graphics2D::DrawRect(float x, float y, float width, float height)
{
	VertexColor scrv2[5];
	scrv2[0] = VertexColor(x, y, 1, color);
	scrv2[1] = VertexColor(x + width, y, 1, color);
	scrv2[2] = VertexColor(x + width, y + height, 1, color);
	scrv2[3] = VertexColor(x, y + height, 1, color);
	scrv2[4] = VertexColor(x, y, 1, color);

	device->SetTexture(0, NULL);
	device->SetFVF(FVF_VERTEX_COLOR);

	device->SetTransform(D3DTS_WORLD, &transformMatrix);
	device->DrawPrimitiveUP(D3DPT_LINESTRIP, 4, &scrv2, sizeof(VertexColor));
}

void Graphics2D::FillRect(float x, float y, float width, float height)
{
	VertexColor scrv2[4];
	scrv2[0] = VertexColor(x, y, 1, color);
	scrv2[1] = VertexColor(x + width, y, 1, color);
	scrv2[2] = VertexColor(x + width, y + height, 1, color);
	scrv2[3] = VertexColor(x, y + height, 1, color);

	device->SetTexture(0, NULL);
	device->SetFVF(FVF_VERTEX_COLOR);

	device->SetTransform(D3DTS_WORLD, &transformMatrix);
	device->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, &scrv2, sizeof(VertexColor));
}

void Graphics2D::DrawLine(float x1, float y1, float x2, float y2)
{
	VertexColor scrv2[2];
	scrv2[0] = VertexColor(x1, y1, 1, color);
	scrv2[1] = VertexColor(x2, y2, 1, color);

	device->SetTexture(0, NULL);
	device->SetFVF(FVF_VERTEX_COLOR);

	device->SetTransform(D3DTS_WORLD, &transformMatrix);
	device->DrawPrimitiveUP(D3DPT_LINELIST, 1, &scrv2, sizeof(VertexColor));
}

void Graphics2D::DrawCircle(float x, float y, float radius, float arcLen)
{
	int n = (int)(2.0f * PI * radius / arcLen + 0.5f);
	const int minSegments = 7;
	if (n < minSegments)
		n = minSegments;
	const float delta = 360.0f / n;
	float angle = 0;
	VertexColor* vertices = new VertexColor[n + 1]; // TODO: stack or buffer allocation
	for (int i = 0; i <= n; i++)
	{
		// TODO: cos of sum optimization
		float x1 = x + radius * cos(RADIAN(angle));
		float y1 = y + radius * sin(RADIAN(angle));
		vertices[i] = VertexColor(x1, y1, 1, color);
		angle += delta;
	}
	
	device->SetTexture(0, NULL);
	device->SetFVF(FVF_VERTEX_COLOR);

	device->SetTransform(D3DTS_WORLD, &transformMatrix);
	device->DrawPrimitiveUP(D3DPT_LINESTRIP, n, vertices, sizeof(VertexColor));
}

void Graphics2D::DrawString(int x, int y, const char* text)
{
	HRESULT hr;
	RECT rc;
	SetRect( &rc, x, y, 0, 0 );
	hr = font->DrawTextA( d3dxSprite, text, -1, &rc, DT_NOCLIP, color );
	ASSERT(SUCCEEDED(hr));
}

void Graphics2D::DrawFormattedString(int x, int y, const char* text, ...)
{
	char strBuffer[512];

	va_list args;
	va_start( args, text );
	vsprintf_s( strBuffer, 512, text, args );
	strBuffer[511] = L'\0';
	va_end( args );

	DrawString(x, y, strBuffer);
}

#endif // USE_DIRECTX