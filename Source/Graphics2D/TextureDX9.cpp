#include <Engine/Base.h>
#if (RENDERER == DIRECTX)
#include <Graphics2D/Texture.h>
#include <Graphics2D/Graphics2D.h>

inline char16* CharToWChar(const char* string)
{
	int len = strlen(string) + 1;
	char16* wstring = (char16*) MemoryFramework::Alloc(2 * len, "Global", "CharToWChar");

	mbstowcs_s(NULL, wstring, len, string, _TRUNCATE);
	return wstring;
}

bool Texture::LoadTexture(const char* filename)
{
	LPCWSTR name = CharToWChar( filename );

	// check if the device supports non power of 2 textures
	// this is not very safe, as it excludes cards that do not support this
	// we may need different sets of resources
	D3DCAPS9 caps;
	Graphics2D::device->GetDeviceCaps(&caps);
	int pow2 = 0;
	if ((caps.TextureCaps & D3DPTEXTURECAPS_POW2) == 0)
	{
		pow2 = D3DX_DEFAULT_NONPOW2;
	}

	if( FAILED( D3DXCreateTextureFromFileEx(Graphics2D::device, 
											name,
											pow2,//width
											pow2,//height
											1,//MipLevels
											0,//usage
											D3DFMT_UNKNOWN,//format
											D3DPOOL_MANAGED,//pool
											D3DX_FILTER_NONE,//filter
											D3DX_DEFAULT,//mipfilter
											0,//ColorKey 
											&D3DTextureInfo,//texture info
											NULL,//Palette 
											&D3DTexture ) ) )
    {
		WCHAR buff[128];
		swprintf_s(buff, L"Could not find %ls", name);
        MessageBox( NULL, buff, L"Error", MB_OK );
		MemoryFramework::Free((char16*)name);
        return false;
    }
	MemoryFramework::Free((char16*)name);

	// check if width and height are powers of 2
	if (pow2 == 0)
	{
		ASSERT((D3DTextureInfo.Width & (D3DTextureInfo.Width - 1)) == 0);
		ASSERT((D3DTextureInfo.Height & (D3DTextureInfo.Height - 1)) == 0);
	}
	// TODO: else warning

	width = D3DTextureInfo.Width;
	height = D3DTextureInfo.Height;

	return true;
}

#endif // DIRECTX