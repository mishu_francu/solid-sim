#ifndef AABB_H
#define AABB_H

#include <Math/Vector2.h>
#include <vector>

using Math::Vector2;

struct Aabb
{
	// TODO: validity assert
	Vector2 min, max;

	Aabb() { }
	Aabb(float x, float y, float w, float h) : min(x, y), max(x + w, y + h) { }

	float GetWidth() const { return max.GetX() - min.GetX(); }
	float GetHeight() const { return max.GetY() - min.GetY(); }
	Vector2 GetCenter() const { return 0.5f * (min + max); }

	void Expand(float s)
	{
		Vector2 exp(s, s);
		min.Subtract(exp);
		max.Add(exp);
	}
};

typedef std::vector<Aabb> AabbVector;

inline Vector2 vmin(const Vector2& a, const Vector2& b)
{
	return Vector2(std::min(a.GetX(), b.GetX()), std::min(a.GetY(), b.GetY()));
}

inline Vector2 vmax(const Vector2& a, const Vector2& b)
{
	return Vector2(std::max(a.GetX(), b.GetX()), std::max(a.GetY(), b.GetY()));
}

inline bool AabbOverlap(const Aabb& bounds1, const Aabb& bounds2)
{
	if ( bounds1.max.GetX() < bounds2.min.GetX() )
		return false;
	if ( bounds1.max.GetY() < bounds2.min.GetY() )
		return false;
	if ( bounds2.max.GetX() < bounds1.min.GetX() )
		return false;
	if ( bounds2.max.GetY() < bounds1.min.GetY() )
		return false;
	return true;
}

inline bool PointInAabb(const Vector2& min, const Vector2& max, const Vector2& point)
{
	return !(point.GetX() < min.GetX() || point.GetX() > max.GetX()
		|| point.GetY() < min.GetY() || point.GetY() > max.GetY());
}

inline bool PointInAabb(const Aabb& bounds, const Vector2& point)
{
	return PointInAabb(bounds.min, bounds.max, point);
}

#endif // AABB_H