#include <Engine/Base.h>
#include <Engine/Profiler.h>
#include <Engine/Utils.h>
#include <Graphics2D/ImageProcessing.h>

// binomial
float blur2D3[] = {
	0.0625f, 0.125f, 0.0625f,
	0.125f,  0.25f,  0.125f,
	0.0625f, 0.125f, 0.0625f
};

// gaussian
float blur2D5[] = {
	0.003f, 0.0133f, 0.0219f, 0.0133f, 0.003f,
	0.0133f, 0.0596f, 0.0983f, 0.0596f, 0.0133f,
	0.0219f, 0.0983f, 0.1621f, 0.0983f, 0.0219f,
	0.0133f, 0.0596f, 0.0983f, 0.0596f, 0.0133f,
	0.003f, 0.0133f, 0.0219f, 0.0133f, 0.003f
};

// binomial
float blur1D3[] = { 0.25f, 0.5f, 0.25f };
float blur1D5[] = { 0.0625f, 0.25f, 0.375f, 0.25f, 0.0625f };

float blur1DGauss5[] = { 0.0545f, 0.2442f, 0.4026f, 0.2442f, 0.0545f };

// blur
GRB Kernel(const uint8* ptr, int x, int y, int width, int height, float* blur2D, int size)
{
	GRBf val = 0;
	GRB* inPtr = (GRB*)ptr;
	int stride = 0;
	int half = size / 2;
	for (int j = -half; j <= half; j++) // TODO: invert loops?
	{
		int y1 = Math::clamp(y + j, 0, height - 1);
		int dy = y1 - y;
		int base = dy * width;
		for (int i = -half; i <= half; i++)
		{
			int x1 = Math::clamp(x + i, 0, width - 1);
			int dx = x1 - x;
			int offset = dx + base;
			GRB src = *(inPtr + offset);
			val += blur2D[i + half + stride] * src;
		}
		stride += size;
	}
	val.Clamp();
	return val;
}

void ProcessImage(const uint8* in, uint8* out, int width, int height, int components) // TODO: drop components?
{
	MEASURE_TIME("ProcessImage");
	const uint8* inPtr = in;
	GRB* outPtr = (GRB*)out;
	for (int j = 0; j < height; j++)
	{
		for (int i = 0; i < width; i++)
		{
			*outPtr = Kernel(inPtr, i, j, width, height, blur2D5, 5);
			outPtr++;
			inPtr += components;
		}
	}
}

GRB KernelX(const GRB* ptr, int x, int width, const float blur1D[], int size)
{
	GRBf val = 0;
	int half = size / 2;
	for (int i = -half; i <= half; i++)
	{
		int x1 = Math::clamp(x + i, 0, width - 1); // TODO: GetColor
		int offset = x1 - x;
		GRB src = *(ptr + offset);
		val += blur1D[i + half] * src;
	}
	val.Clamp();
	return val;
}

GRB KernelY(const GRB* ptr, int y, int width, int height, const float blur1D[], int size) // TODO: pitch
{
	GRBf val = 0;
	int half = size / 2;
	for (int i = -half; i <= half; i++)
	{
		int y1 = Math::clamp(y + i, 0, height - 1); // TODO: GetColor
		int offset = (y1 - y) * width;
		GRB src = *(ptr + offset);
		val += blur1D[i + half] * src;
	}
	val.Clamp();
	return val;
}

void Gaussian(int size, float scale, float sigma, float* out)
{
	int half = size / 2;
	const float sigma2 = sigma * sigma;
	const float den = 0.5f / sigma2;
	const float norm = 1.f / (sigma2 * sqrt(2 * PI));
	float sum = 0;
	for (int i = 0; i <= half; i++)
	{
		float x = scale * i;
		float val = exp(-x * x * den) * norm;
		sum += val;
		out[i + half] = val;
		if (i) // TODO: iszero(i)
		{
			out[half - i] = val;
			sum += val;
		}
	}
	// normalize
	const float inv = 1.f / sum;
	for (int i = 0; i < size; i++)	
		out[i] *= inv;
}

void Binomial(int size, float* out)
{
	int half = size / 2;
	float sum = 0;
	float val = 1;
	for (int i = half; i >= 0; i--)
	{
		int k = half - i;
		sum += val;
		out[i + half] = val;
		if (i) // TODO: iszero(i)
		{
			out[half - i] = val;
			sum += val;
		}
		val *= (float)(size - k) / (float)(k + 1);
	}
	// normalize
	const float inv = 1.f / sum;
	for (int i = 0; i < size; i++)	
		out[i] *= inv;
}

void Sinc(int size, float scale, float* out)
{
	int half = size / 2;
	float sum = 0;
	for (int i = 0; i <= half; i++)
	{
		float val = 1;
		if (i) // TODO: iszero(i)
		{
			float x = scale * i * PI;
			val = sin(x) / x;
			out[half - i] = val;
			sum += val;
		}
		sum += val;
		out[i + half] = val;
	}
	// normalize
	const float inv = 1.f / sum;
	for (int i = 0; i < size; i++)	
		out[i] *= inv;
}

float* Tent(int& size, float scale)
{
	int half = (int)(1.f / scale);
	size = 1 + 2 * half;
	float* out = new float[size];
	float sum = 0;
	for (int i = 0; i <= half; i++)
	{
		float val = 1;
		if (i) // TODO: iszero(i)
		{
			float x = fabs(scale * i);
			val = x < 1 ? 1 - x : 0;
			out[half - i] = val;
			sum += val;
		}
		sum += val;
		out[i + half] = val;
	}
	// normalize
	const float inv = 1.f / sum;
	for (int i = 0; i < size; i++)
	{
		out[i] *= inv;
		//Printf("%.3f ", out[i]);
	}
	//Printf("\n");
	return out;
}

// a should be between -1 and -0.5
float* Cubic(int& size, float scale, float a)
{
	int half = (int)(1.9999f / scale);
	size = 1 + 2 * half;
	float* out = new float[size];
	float sum = 0;
	for (int i = 0; i <= half; i++)
	{
		float val = 1;
		if (i) // TODO: iszero(i)
		{
			float x = fabs(scale * i);
			float x2 = x * x;
			float x3 = x2 * x;
			if (x < 1)
				val = (a + 2) * x3 - (a + 3) * x2 + 1;
			else if (x < 2)
				val = a * x3 - 5 * a * x2 + 8* a * x - 4 * a;
			else
				val = 0;
			out[half - i] = val;
			sum += val;
		}
		sum += val;
		out[i + half] = val;
	}
	// normalize
	const float inv = 1.f / sum;
	for (int i = 0; i < size; i++)
	{
		out[i] *= inv;
		//Printf("%.3f ", out[i]);
	}
	//Printf("\n");
	return out;
}

// TODO: Mitchell, B-spline

// TODO: gamma correction
void ProcessImageSeparable(const uint8* in, uint8* out, int width, int height, float scale)
{
	MEASURE_TIME("ProcessImage Separable");
	//const int size = scale < 0.5f ? 5 : 3;
	//const float* blur1D = blur1D5;
	//float* blur1D = new float[size];
	//Gaussian(size, scale, 5.f, blur1D);
	//Binomial(size, blur1D);
	//Sinc(size, scale, blur1D);
	int size;
	//float* blur1D = Tent(size, scale);
	float* blur1D = Cubic(size, scale, -0.75f); // TODO: Fourier transform filtering
	//Printf("kernel: %.4f %.4f %.4f, scale: %.2f\n", blur1D[0], blur1D[1], blur1D[2], scale);
	//Printf("size=%d\n", size);

	const GRB* inPtr = (GRB*)in;
	GRB* outX = new GRB[width * height * 3];
	GRB* outPtrX = outX;
	for (int j = 0; j < height; j++)
	{
		for (int i = 0; i < width; i++)
		{
			*outPtrX = KernelX(inPtr, i, width, blur1D, size);
			outPtrX++;
			inPtr++;
		}
	}
	// second pass
	inPtr = outX;
	GRB* outPtr = (GRB*)out;
	// TODO: reuse code?
	for (int j = 0; j < height; j++)
	{
		for (int i = 0; i < width; i++)
		{
			*outPtr = KernelY(inPtr, j, width, height, blur1D, size);
			outPtr++;
			inPtr++;
		}
	}
	
	delete[] outX;
	delete[] blur1D;
}

//struct GRBf_SSE
//{
//	__m128 quad;
//
//	GRBf_SSE() { quad = _mm_setzero_ps(); }
//	GRBf_SSE(const GRB& c) { quad = _mm_set_ps(c.r, c.g, c.b, 0); }
//	GRBf_SSE operator +=(const GRBf_SSE& c) 
//	{
//		quad = _mm_add_ps(quad, c.quad);
//		return *this;
//	}
//};
//
//GRBf_SSE operator *(float s, const GRBf_SSE& c)
//{
//	GRBf_SSE r;
//	r.quad = _mm_set1_ps(s)
//}

#ifdef WIN32
static const __m128 upper = _mm_set1_ps(255);
static const __m128 point5 = _mm_set1_ps(0.5f);
//static const __m128 lower = _mm_setzero_ps();

__m128 KernelX_SSE(const __m128* ptr, int x, int width, const __m128 blur1D[], int size)
{
	__m128 val = _mm_setzero_ps();
	int half = size / 2;
	for (int i = -half; i <= half; i++)
	{
		int x1 = Math::clamp(x + i, 0, width - 1); // TODO: GetColor
		int offset = x1 - x;
		__m128 src = *(ptr + offset);
		//__m128 add = _mm_setr_ps(src.g, src.r, src.b, 0); // TODO: load integers or at least cast from __m128i to __m128
		val = _mm_add_ps(val, _mm_mul_ps(blur1D[i + half], src));
	}
	val = _mm_min_ps(upper, _mm_max_ps(_mm_setzero_ps(), val));
	return val;
}

GRB KernelY_SSE(const __m128* ptr, int y, int width, int height, const __m128 blur1D[], int size) // TODO: pitch
{
	__m128 val = _mm_setzero_ps();
	int half = size / 2;
	for (int i = -half; i <= half; i++)
	{
		int y1 = Math::clamp(y + i, 0, height - 1); // TODO: GetColor
		int offset = (y1 - y) * width;
		__m128 src = *(ptr + offset);
		val = _mm_add_ps(val, _mm_mul_ps(blur1D[i + half], src));
	}
	val = _mm_add_ps(point5, val); // for rounding
	val = _mm_min_ps(upper, _mm_max_ps(_mm_setzero_ps(), val));
	ALIGN16 float temp[4];
	_mm_store_ps(temp, val);
	GRB ret;
	ret.g = (uint8)temp[0];
	ret.r = (uint8)temp[1];
	ret.b = (uint8)temp[2];
	return ret;
}

void ProcessImageSeparable_SSE(const __m128* in, uint8* out, int width, int height, float scale)
{
	MEASURE_TIME("ProcessImage Separable SSE");
	int size;
	float* blur1D = Cubic(size, scale, -0.75f); // TODO: Fourier transform filtering
	//Printf("size=%d\n", size);
	//__m128* blurSSE = new __m128[size];
	__m128* blurSSE = (__m128*)_aligned_malloc(size * sizeof(__m128), 16);
	for (int i = 0; i < size; i++)
		blurSSE[i] = _mm_set1_ps(blur1D[i]);

	const __m128* inPtr = in;
	//__m128* outX = new __m128[width * height];
	__m128* outX = (__m128*)_aligned_malloc(width * height * sizeof(__m128), 16);
	__m128* outPtrX = outX;
	for (int j = 0; j < height; j++)
	{
		for (int i = 0; i < width; i++)
		{
			*outPtrX = KernelX_SSE(inPtr, i, width, blurSSE, size);
			outPtrX++;
			inPtr++;
		}
	}
	// second pass
	GRB* outPtr = (GRB*)out;
	outPtrX = outX;
	// TODO: reuse code?
	for (int j = 0; j < height; j++)
	{
		for (int i = 0; i < width; i++)
		{
			*outPtr = KernelY_SSE(outPtrX, j, width, height, blurSSE, size);
			outPtr++;
			outPtrX++;
		}
	}
	
	//delete[] outX;
	_aligned_free(outX);
	delete[] blur1D;
	//delete[] blurSSE;
	_aligned_free(blurSSE);
}

void ConvertToSSE(const void* in, __m128* out, int width, int height)
{
	MEASURE_TIME("ConvertToSSE");
	const GRB* inPtr = (GRB*)in;
	__m128* outPtr = out;
	for (int j = 0; j < height; j++)
	{
		for (int i = 0; i < width; i++)
		{
			const GRB& src = *inPtr;
			__m128 val = _mm_setr_ps(src.g, src.r, src.b, 0); // TODO: load integers or at least cast from __m128i to __m128
			*outPtr = val;
			outPtr++;
			inPtr++;
		}
	}
}
#endif

GRB NearestNeighbourResampler::operator()(float s, float t)
{
	int x = (int)s;
	int y = (int)t;
	return GetColor(x, y);
}

GRB BilinearResampler::operator()(float s, float t)
{
	int x = (int)floor(s);
	int y = (int)floor(t);

	float alpha = s - x;
	float beta = t - y;

	float omAlpha = 1.f - alpha;
	GRBf c1 = omAlpha * GetColorF(x, y) + alpha * GetColorF(x + 1, y);
	GRBf c2 = omAlpha * GetColorF(x, y + 1) + alpha * GetColorF(x + 1, y + 1);
	GRBf c = (1.f - beta) * c1 + beta * c2;
	c.Clamp();
	return c;
}

GRB BicubicResampler::operator()(float x, float y)
{
	//MEASURE_TIME("bicubic (per pixel)");
	// ref point (0, 0)
	int x0 = (int)floor(x);
	int y0 = (int)floor(y);

	// init 4x4 samples
	const GRB* inPtr = (const GRB*)in;
	for (int j = 0; j < 4; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			int x1 = Math::clamp(x0 - 1 + i, 0, width - 1);		
			p[i][j] = GetColor(x0 - 1 + i, y0 - 1 + j); // TODO: make image a matrix too
		}
	}

	// TODO: premultiply stuff and get rid of allocations
	for (int k = 0; k < 4; k++)
	{
		int i = k & 1;
		int j = k >> 1;
		beta[k] = p[i + 1][j + 1];
		beta[k + 4] = 0.5f * (p[i + 2][j + 1] - p[i][j + 1]);
		beta[k + 8] = 0.5f * (p[i + 1][j + 2] - p[i + 1][j]);
		beta[k + 12] = 0.25f * (p[i + 2][j + 2] - p[i + 2][j] - p[i][j + 2] + p[i][j]);
	}

	// TODO: use buffer of A directly -> make it column major
	std::vector<GRBf> alpha = B.Transform<GRBf, GRBf>(beta); // hidden allocation

	// copy alpha to matrix A
	for (int j = 0; j < 4; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			A[i][j] = alpha[i + j * 4]; // TODO: increment cursor
		}
	}

	float s = x - x0;
	float t = y - y0;
	sBar[0] = tBar[0] = 1;
	for (int i = 0; i < 3; i++)
	{
		sBar[i + 1] = sBar[i] * s;
		tBar[i + 1] = tBar[i] * t;
	}
	std::vector<GRBf> temp = A.Transform<float, GRBf>(tBar); // hidden allocation
	GRBf ret = InnerProduct<GRBf, float, GRBf>(temp, sBar); // hidden allocation
	ret.Clamp();
	return ret;
}

GRBf CatmullRom::operator()(float s, GRBf p[])
{
	GRBf a = -0.5f * p[0] + 1.5f * p[1] - 1.5f * p[2] + 0.5f * p[3];
	GRBf b =         p[0] - 2.5f * p[1] +  2.f * p[2] - 0.5f * p[3];
	GRBf c = -0.5f * p[0] +               0.5f * p[2];
	GRBf d = p[1];
	float s2 = s * s;
	float s3 = s2 * s;
	return a * s3 + b * s2 + c * s + d;
}

GRBf Cubic1D::operator()(float s, GRBf p[])
{
	GRBf a = -0.167f * p[0] + 0.5f * p[1] - 0.5f * p[2] + 0.167f * p[3];
	GRBf b =    0.5f * p[0] -        p[1] + 0.5f * p[2];
	GRBf c = -0.333f * p[0] - 0.5f * p[1] +        p[2] - 0.167f * p[3];
	GRBf d = p[1];
	float s2 = s * s;
	float s3 = s2 * s;
	return a * s3 + b * s2 + c * s + d;
}

template<class Interpolator>
GRB BicubicResampler1D<Interpolator>::operator()(float x, float y)
{
	//MEASURE_TIME("bicubic 1d (per pixel)");
	// ref point (0, 0)
	int x0 = (int)floor(x);
	int y0 = (int)floor(y);
	float s = x - x0;
	float t = y - y0;

	const GRB* inPtr = (const GRB*)in;
	GRBf p[4], c[4];
	for (int j = 0; j < 4; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			p[i] = GetColorF(x0 - 1 + i, y0 - 1 +j); // TODO: make image a matrix too
		}
		c[j] = interp(s, p);
	}
	GRBf ret = interp(t, c);
	ret.Clamp();
	return ret;
}

template struct BicubicResampler1D<CatmullRom>;
template struct BicubicResampler1D<Cubic1D>;

//template uint8* ResizeImage<NearestNeighbourResampler>(const uint8* in, int& width, int& height, float scale);
//template uint8* ResizeImage<BilinearResampler>(const uint8* in, int& width, int& height, float scale);
//template uint8* ResizeImage<BicubicResampler1D<>>(const uint8* in, int& width, int& height, float scale);
//template uint8* ResizeImage<BicubicResampler1D<Cubic1D>>(const uint8* in, int& width, int& height, float scale);
//template uint8* ResizeImage<BicubicResampler>(const uint8* in, int& width, int& height, float scale);