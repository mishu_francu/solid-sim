#include <Engine/Base.h>

#if (RENDERER == OPENGL) && defined(FIXED_PIPELINE)

#include <Graphics2D/Graphics2D.h>
#include <Graphics2D/Texture.h>

bool Graphics2D::Init(HWND window, HINSTANCE instance)
{
	hWnd = window;
	hDC = GetDC(hWnd);
	InitPF();
	// if we can create a rendering context ...  
	if (hGLRC = wglCreateContext(hDC))
	{
		// try to make it the thread's current rendering context 
		BOOL bHaveCurrentRC = wglMakeCurrent(hDC, hGLRC);  
	}

#ifdef USE_GDIPLUS
	Gdiplus::GdiplusStartupInput input;    
	Gdiplus::GdiplusStartup(&gdiplusToken, &input, NULL);
#elif !defined(_WIN64)
	FreeImage_Initialise();
#endif

	// Setup GLEW which loads OGL function pointers
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		// Problem: glewInit failed, something is seriously wrong.
		Printf("Error: %s\n", glewGetErrorString(err));
		return false;
	}

	glClearColor(0.5f, 0.3f, 0.4f, 0.f);

	return true;
}

void Graphics2D::SetContext()
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	// solid/wireframe
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	// transparency
	glAlphaFunc(GL_ALWAYS,0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// line antialiasing
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
}

void Graphics2D::InitPF()
{
	PIXELFORMATDESCRIPTOR pfd = { 
		sizeof(PIXELFORMATDESCRIPTOR),    // size of this pfd 
		1,                                // version number 
		PFD_DRAW_TO_WINDOW |              // support window 
		PFD_SUPPORT_OPENGL |              // support Graphics2D 
		PFD_DOUBLEBUFFER,                 // double buffered
		PFD_TYPE_RGBA,                    // RGBA type 
		32,                               // 32-bit color depth 
		0, 0, 0, 0, 0, 0,                 // color bits ignored 
		0,                                // no alpha buffer 
		0,                                // shift bit ignored 
		0,                                // no accumulation buffer 
		0, 0, 0, 0,                       // accum bits ignored 
		32,                               // 32-bit z-buffer     
		8,                                // stencil buffer 
		0,                                // no auxiliary buffer 
		PFD_MAIN_PLANE,                   // main layer 
		0,                                // reserved 
		0, 0, 0                           // layer masks ignored 
	}; 
	int  iPixelFormat; 
	// get the device context's best, available pixel format match 
	iPixelFormat = ChoosePixelFormat(hDC, &pfd); 
	// make that match the device context's current pixel format 
	SetPixelFormat(hDC, iPixelFormat, &pfd); 
}

void Graphics2D::Resize(int width, int height)
{
	w = width;
	h = height;
}

void Graphics2D::DeInit()
{
	wglMakeCurrent(0,0);
	ReleaseDC(hWnd,hDC) ;
	wglDeleteContext(hGLRC);

#ifdef USE_GDIPLUS
	Gdiplus::GdiplusShutdown(gdiplusToken);
#elif !defined(_WIN64)
	FreeImage_DeInitialise();
#endif
}

void Graphics2D::Viewport()
{
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();	
	gluOrtho2D(0.0, (GLfloat) w, 0.0, (GLfloat) h);

	glMatrixMode(GL_MODELVIEW);
	ResetTransform();
}

void Graphics2D::BeginDraw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
}

void Graphics2D::EndDraw()
{
	glPopMatrix();
	SwapBuffers(hDC);
}

void Graphics2D::DrawRegion(Texture* image, float x_dest, float y_dest, int x_src, int y_src, 
	int width, int height, float scale, int anchor)
{
	if (image->GetAlpha() == 0)
		return;
	if (x_src >= image->Width() || y_src >= image->Height())
		return;

	y_src = image->Height() - y_src;

	//remember original xsrc & ysrc
	int xsrc = x_src;
	int ysrc = y_src;
	
	//if we're before the image move to its begining
	if(xsrc < 0)
		xsrc = 0;
	if(ysrc < 0)
		ysrc = 0;
	
	//if we moved xsrc or ysrc we need to update x_dst or y_dst & width or height
	x_dest += ( xsrc - x_src );
	y_dest += ( ysrc - y_src );
	width  -= ( xsrc - x_src );
	height -= ( ysrc - y_src );
	
	//adjust x_dest & y_dest based on align request
	//if( anchor & Align::Right )
	//	x_dest -= width;
	//if( anchor & Align::Bottom )
	//	y_dest -= height;
	if( anchor & Align::HCenter )
		x_dest -= 0.5f * scale * width;
	if( anchor & Align::VCenter )
		y_dest -= 0.5f * scale * height;

	// translate to destination
	glPushMatrix();
	glTranslatef(x_dest, y_dest, 0);
	glScalef(scale, scale, scale);

	//if( (transform & Transform::Rot90)!=0 )
	//{
	//	Translate(height, 0);
	//	Rotate(90);
	//}
	//if( (transform & Transform::FlipX)!=0 )
	//{
	//	Scale(-1, 1);				
	//	Translate(-width, 0);
	//}
	//if( (transform & Transform::FlipY)!=0 )
	//{
	//	Scale(1, -1);
	//	Translate(0, -height);
	//}

	// compute texture coordinates
#if TEXTURE_TYPE == GL_TEXTURE_RECTANGLE
	float minX  = (x_src);
	float minY  = (y_src);
	float maxX  = (x_src + width);
	float maxY  = (y_src - height);
#else
	const float imageW = image->Width();
	const float imageH = image->Height();
#ifndef USE_GDIPLUS
	float minX  = (x_src) / imageW;
	float minY  = (y_src) / imageH;
	float maxX  = (x_src + width ) / imageW;
	float maxY  = (y_src - height ) / imageH;
#else
	float minX  = (x_src) / imageW;
	float maxY  = (y_src) / imageH;
	float maxX  = (x_src + width ) / imageW;
	float minY  = (y_src - height ) / imageH;
#endif
#endif

	glEnable(GL_TEXTURE_2D);

	glColor4ub(255, 255, 255, image->GetAlpha());
	glBindTexture(GL_TEXTURE_2D, image->ID);
	glBegin(GL_QUADS);
		glTexCoord2f(minX, minY);
		glVertex3f(0, 0, 0);
		
		glTexCoord2f(minX, maxY);
		glVertex3f(0, height, 0);
		
		glTexCoord2f(maxX, maxY);
		glVertex3f(width, height, 0);
		
		glTexCoord2f(maxX, minY);
		glVertex3f(width, 0, 0);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
}

void Graphics2D::DrawRect(float x, float y, float width, float height)
{
	glBegin(GL_LINE_LOOP);
		glVertex3f(x, y, 0);
		glVertex3f(x + width, y, 0);
		glVertex3f(x + width, y + height, 0);
		glVertex3f(x, y + height, 0);
	glEnd();
}

void Graphics2D::FillRect(float x, float y, float width, float height)
{
	glBegin(GL_QUADS);
		glVertex3f(x, y, 0);
		glVertex3f(x, y + height, 0);
		glVertex3f(x + width, y + height, 0);
		glVertex3f(x + width, y, 0);
	glEnd();
}

void Graphics2D::DrawLine(float x1, float y1, float x2, float y2)
{
	glBegin(GL_LINES);
		glVertex3f(x1, y1, 0);
		glVertex3f(x2, y2, 0);
	glEnd();
}

void Graphics2D::DrawCircle(float x, float y, float radius, float arcLen)
{
	int n = (int)(2.0f * PI * radius / arcLen + 0.5f);
	const int minSegments = 7;
	if (n < minSegments)
		n = minSegments;
	const float delta = 360.0f / n;
	float angle = 0;
	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < n; i++)
	{
		float x1 = x + radius * cos(RADIAN(angle));
		float y1 = y + radius * sin(RADIAN(angle));
		glVertex3f(x1, y1, 0);
		angle += delta;
	}
	glEnd();
}

void Graphics2D::DrawPolyLine(float v[], int n, bool fill)
{
	glBegin(fill ? GL_TRIANGLE_FAN : GL_LINE_LOOP);
	for (int i = 0; i < n; i ++)
	{
		glVertex3f(v[i * 2], v[i * 2 + 1], 0);
	}
	glEnd();
}

void Graphics2D::FillCircle(float x, float y, float radius, float arcLen)
{
	int n = (int)(2.0f * PI * radius / arcLen + 0.5f);
	const int minSegments = 7;
	if (n < minSegments)
		n = minSegments;
	const float delta = 360.0f / n;
	float angle = 0;
	glBegin(GL_TRIANGLE_FAN);
	glVertex3f(x, y, 0);
	for (int i = 0; i <= n; i++)
	{
		float x1 = x + radius * cos(RADIAN(angle));
		float y1 = y + radius * sin(RADIAN(angle));
		glVertex3f(x1, y1, 0);
		angle += delta;
	}
	glEnd();
}

void Graphics2D::DrawLineMesh(const LineMesh& mesh)
{
	// TODO: implement
}

void Graphics2D::DrawString(float x, float y, const char* text)
{
	glRasterPos2i(x, y + 12);
	for (int i = 0; text[i] != '\0'; i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, text[i]);
	}
}

void Graphics2D::DrawFormattedString(float x, float y, const char* text, ...)
{
	va_list arg_list;
	char str[256];

	va_start(arg_list, text);
	vsprintf_s(str, 256, text, arg_list);
	va_end(arg_list);

	DrawString(x, y, str);
}

#endif // USE_OPENGL
