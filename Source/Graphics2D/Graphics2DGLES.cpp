#include <Engine/Base.h>

#if (RENDERER == OPENGLES)

#if !defined(_WIN64) && !defined(ANDROID_NDK)
// TODO: static linkage (#define FREEIMAGE_LIB)
#	include <FreeImage/FreeImage.h>
#endif
#	include <Graphics2D/Graphics2D.h>
#	include <Graphics2D/Texture.h>

bool Graphics2D::Init(HWND wnd)
{	
#ifndef ANDROID_NDK
	hWnd = wnd;
	display = eglGetDisplay(GetDC(hWnd));
	if (display == EGL_NO_DISPLAY)
	{
		Printf("EGL: no display\n");
		return false;
	}

	EGLint major, minor;
	if (!eglInitialize(display, &major, &minor))
	{
		Printf("EGL: intitialize failed\n");
		return false;
	}

	int numConfig;
	if (!eglGetConfigs(display, NULL, 0, &numConfig))
	{
		return false;
	}

	EGLConfig* configs = new EGLConfig[numConfig];
	if (!eglGetConfigs(display, configs, numConfig, &numConfig))
	{
		delete[] configs;
		return false;
	}
	for (int i = 0; i < numConfig; i++)
	{
		int val;
		Printf("Config %d\n", i);
		if (eglGetConfigAttrib(display, configs[i], EGL_BUFFER_SIZE, &val))
		{
			Printf("buffer size: %d\n", val);
		}
		if (eglGetConfigAttrib(display, configs[i], EGL_COLOR_BUFFER_TYPE, &val))
		{
			if (val == EGL_RGB_BUFFER)
			{
				Printf("color buffer type: RGB\n");
			}
			else
			{
				Printf("color buffer type: %d\n", val);
			}
		}
		if (eglGetConfigAttrib(display, configs[i], EGL_DEPTH_SIZE, &val))
		{
			Printf("depth size: %d\n", val);
		}
		if (eglGetConfigAttrib(display, configs[i], EGL_CONFIG_CAVEAT, &val))
		{
			if (val == EGL_NONE)
			{
				Printf("caveat: none\n");
			}
			else if (val == EGL_SLOW_CONFIG)
			{
				Printf("caveat: slow\n");
			}
			else if (val == EGL_NON_CONFORMANT_CONFIG)
			{
				Printf("caveat: non conformant\n");
			}
			else
			{
				Printf("caveat: %d\n", val);
			}
		}
	}
	EGLConfig config = configs[0];
	delete[] configs;

	EGLint attribList[] =
	{
		EGL_RENDER_BUFFER, EGL_BACK_BUFFER,
		EGL_NONE
	};
	window = eglCreateWindowSurface(display, config, hWnd, NULL);
	if(window == EGL_NO_SURFACE)
	{
		EGLint error = eglGetError();
		switch(error)
		{
		case EGL_BAD_MATCH:
			Printf("Check window and EGLConfig attributes to determine compatibility, or verify that the EGLConfig supports rendering to a window\n");
			break;
		case EGL_BAD_CONFIG:
			Printf("Verify that provided EGLConfig is valid\n");
			break;
		case EGL_BAD_NATIVE_WINDOW:
			Printf("Verify that provided EGLNativeWindow is valid\n");
			break;
		case EGL_BAD_ALLOC:
			Printf("Not enough resources available. Handle and recover\n");
			break;
		default:
			Printf("EGL: no surface (0x%x)\n", error);
			break;
		}
		return false;
	}

	EGLContext context = eglCreateContext(display, config, EGL_NO_CONTEXT, NULL);
	if (context == EGL_NO_CONTEXT)
	{
		return false;
	}

	if (!eglMakeCurrent(display, window, window, context))
	{
		return false;
	}

	Printf("Success!\n");
#endif // !ANDROID_NDK

	const GLubyte* extensions = glGetString(GL_EXTENSIONS);
	Printf("Extensions: %s\n", extensions);
	int max;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max);
	Printf("Max texture size: %d\n", max);
	
	glClearColor( 0.0f, 0.7f, 0.7f, 0.0f );

	glDisable(GL_DEPTH_TEST);
	//glDisable(GL_ALPHA_TEST);
	
	// transparency
	glAlphaFunc(GL_ALWAYS, 0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//glEnable(GL_ALPHA_BITS);

#ifndef ANDROID_NDK
	FreeImage_Initialise();
#endif

#ifdef LINE_BUFFER
	lineIdx = 0;
#endif

	return true;
}

void Graphics2D::SetContext()
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	// solid/wireframe
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	// transparency
	glAlphaFunc(GL_ALWAYS,0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// line antialiasing - not available in GL ES 1.1
	//glEnable(GL_LINE_SMOOTH);
	//glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
}

void Graphics2D::Viewport()
{
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();	
	glOrthof(0.0, (GLfloat) w, 0.0, (GLfloat) h, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0, (GLfloat)h, 0);
	glScalef(1, -1, 1);
}

void Graphics2D::Resize(int width, int height)
{
	w = width;
	h = height;
}

void Graphics2D::DeInit()
{
#ifndef ANDROID_NDK
	FreeImage_DeInitialise();
#endif
}

void Graphics2D::BeginDraw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);	
	//glPushMatrix();
}

void Graphics2D::EndDraw()
{
	//glPopMatrix();

#ifdef LINE_BUFFER
	// flush
	ASSERT(lineIdx < BUFFER_SIZE);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, 0, lines);
	glDrawArrays(GL_LINES, 0, lineIdx / 2);
	lineIdx = 0;
#endif
#ifndef ANDROID_NDK
	eglSwapBuffers(display, window);
#endif
	CheckGlError();
}

//void Graphics2D::DrawImage(Texture* image, float x_dest, float y_dest, float scale, int anchor)
//{
//	DrawRegion(image, x_dest, x_dest, 0, 0, image->Width(), image->Height(), scale, anchor);
//}

void Graphics2D::DrawRegion(Texture* image, float x_dest, float y_dest, int x_src, int y_src, 
	int width, int height, float scale, int anchor)
{
	ASSERT(x_src < image->Width());
	ASSERT(y_src < image->Height());

	y_src = image->Height() - y_src;

	//remember original xsrc & ysrc
	int xsrc = x_src;
	int ysrc = y_src;
	
	//if we're before the image move to its begining
	if(xsrc < 0)
		xsrc = 0;
	if(ysrc < 0)
		ysrc = 0;
	
	//if we moved xsrc or ysrc we need to update x_dst or y_dst & width or height
	x_dest += ( xsrc - x_src );
	y_dest += ( ysrc - y_src );
	width  -= ( xsrc - x_src );
	height -= ( ysrc - y_src );
	
	//adjust x_dest & y_dest based on align request
	//if( anchor & Align::Right )
	//	x_dest -= width;
	//if( anchor & Align::Bottom )
	//	y_dest -= height;
	if( anchor & Align::HCenter )
		x_dest -= 0.5f * scale * width;
	if( anchor & Align::VCenter )
		y_dest -= 0.5f * scale * height;

	// compute texture coordinates
	const float imageW = (float)image->Width();
	const float imageH = (float)image->Height();
	float minX  = (x_src) / imageW;
	float minY  = (y_src) / imageH;
	float maxX  = (x_src + width ) / imageW;
	float maxY  = (y_src - height ) / imageH;

	glEnable(GL_TEXTURE_2D);

	// translate to destination
	glPushMatrix();
	glTranslatef((float)x_dest, (float)y_dest, 0); // TODO: change the arguments type
	glScalef(scale, scale, scale);

	glColor4ub(255, 255, 255, image->GetAlpha());
	glBindTexture(GL_TEXTURE_2D, image->ID);
	glEnableClientState(GL_VERTEX_ARRAY);
	float vertices[] = { 
		(float)width, 0, 0,
		0, 0, 0,
		(float)width, (float)height, 0,
		0, (float)height, 0};
#ifdef ANDROID_NDK
	float uvs[] = { 
		maxX, maxY,
		minX, maxY,
		maxX, minY,
		minX, minY};
#else
	float uvs[] = { 
		maxX, minY,
		minX, minY,
		maxX, maxY,
		minX, maxY};
#endif
	glVertexPointer(3, GL_FLOAT, 0, vertices);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, uvs);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
	
	CheckGlError();
}

void Graphics2D::DrawRect(float x, float y, float width, float height)
{
	float vertices[] = { 
		x, y,
		x + width, y,
		x + width, y,
		x + width, y + height,
		x + width, y + height,
		x, y + height,
		x, y + height,
		x, y
	};
#ifndef LINE_BUFFER
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glDrawArrays(GL_LINES, 0, 8);
#else
	memcpy(lines + lineIdx, vertices, 16 * sizeof(float));
	lineIdx += 16;
#endif
}

void Graphics2D::FillRect(float x, float y, float width, float height)
{
	glEnableClientState(GL_VERTEX_ARRAY);
	float vertices[] = { 
		x + width, y,
		x, y,
		x + width, y + height,
		x, y + height};
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

void Graphics2D::DrawLine(float x1, float y1, float x2, float y2)
{
#ifdef LINE_BUFFER
	lines[lineIdx++] = x1;
	lines[lineIdx++] = y1;
	lines[lineIdx++] = x2;
	lines[lineIdx++] = y2;
#else
	float vertices[] = { 
		x1, y1,
		x2, y2 };
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glDrawArrays(GL_LINES, 0, 2);

#endif
}

void Graphics2D::DrawCircle(float x, float y, float radius, float arcLen)
{
	//const float arcLen = 30;
	int n = (int)(2 * PI * radius / arcLen);
	const int minSegments = 7;
	if (n < minSegments)
	{
		n = minSegments;
	}
	const float delta = 360.0f / n;
	float angle = 0;
#ifdef LINE_BUFFER
	Vector3 center = View.Transform(Vector3(x, y, 0));
	float x0 = center.X() + radius;
	float y0 = center.Y();
	float x1 = x0;
	float y1 = y0;
	for (int i = 0; i < n; i++)
	{
		lines[lineIdx++] = x1;
		lines[lineIdx++] = y1;
		angle += delta;
		float x2 = center.X() + radius * cos(RADIAN(angle));
		float y2 = center.Y() + radius * sin(RADIAN(angle));
		lines[lineIdx++] = x2;
		lines[lineIdx++] = y2;
		x1 = x2;
		y1 = y2;
	}
#else
	float* buffer = new float[(n + 1) * 2]; // TODO: fast allocator or max preallocated block
	buffer[0] = x + radius;
	buffer[1] = y;
	for (int i = 0, idx = 2; i < n; i++, idx += 2)
	{
		angle += delta;
		buffer[idx] = x + radius * cos(RADIAN(angle));
		buffer[idx + 1] = y + radius * sin(RADIAN(angle));
	}
	glEnableClientState(GL_VERTEX_ARRAY); // is this needed?
	glVertexPointer(2, GL_FLOAT, 0, buffer);
	glDrawArrays(GL_LINE_LOOP, 0, n + 1);
	delete[] buffer;
#endif
}

void Graphics2D::FillCircle(float x, float y, float radius, float arcLen)
{
	int n = (int)(2.0f * PI * radius / arcLen + 0.5f);
	const int minSegments = 7;
	if (n < minSegments)
		n = minSegments;
	Vector2* circle = new Vector2[n + 2]; // stack alloc
	const float delta = 360.0f / n;
	float angle = 0;	
	circle[0].Set(x, y);
	for (int i = 1; i <= n; i++)
	{
		float x1 = x + radius * cos(RADIAN(angle));
		float y1 = y + radius * sin(RADIAN(angle));
		circle[i].Set(x1, y1);
		angle += delta;
	}
	circle[n + 1].Set(x + radius, y);
	
	glVertexPointer(2, GL_FLOAT, 0, circle);
	glDrawArrays(GL_TRIANGLE_FAN, 0, n + 2);

	delete[] circle; // TODO: better way (static allocation)
}

void Graphics2D::DrawPolyLine(float v[], int n, bool fill)
{
	glVertexPointer(2, GL_FLOAT, 0, v);
	glDrawArrays(fill ? GL_TRIANGLE_FAN : GL_LINE_LOOP, 0, n);
}

void Graphics2D::DrawString(float x, float y, const char* text)
{
	//glRasterPos3i(x, y + 12, 0);

	//for (int i = 0; text[i] != '\0'; i++)
	//{
	//	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, text[i]);
	//}
}

void Graphics2D::DrawFormattedString(float x, float y, const char* text, ...)
{
	//va_list arg_list;
	//va_start(arg_list, text);
	//Printf(text, arg_list);
	//va_end(arg_list);
}

#endif // USE_OPENGL
