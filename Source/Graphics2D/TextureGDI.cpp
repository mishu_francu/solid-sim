#include <Engine/Base.h>
#include <Graphics2D/Texture.h>

#if (RENDERER == GDIPLUS)

Gdiplus::Graphics* Texture::graphics = NULL;
Texture::FilteringType Texture::filtering = Texture::DEFAULT;

bool Texture::LoadTexture(const char* filename)
{
	wchar_t ucFilename[100];
	const int len = strlen(filename) + 1;
	MultiByteToWideChar(CP_ACP, 0, filename, len, ucFilename, len);
	//img = new Gdiplus::Image(ucFilename);
	bmp = new Gdiplus::Bitmap(ucFilename);
	width = bmp->GetWidth();
	height = bmp->GetHeight();
	cache = new Gdiplus::CachedBitmap(bmp, graphics);
	return true;
}

void Texture::Rescale(float sx, float sy)
{
	int sw = (int)(sx * width + 0.5f);
	int sh = (int)(sy * height + 0.5f);
	if (cache)
		delete cache;
	Gdiplus::Bitmap temp(sw, sh);
	Gdiplus::Graphics tempGdi(&temp);
	if (filtering == HQ_BILINEAR)
		tempGdi.SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeHighQualityBilinear);
	else if (filtering == BICUBIC)
		tempGdi.SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeHighQualityBicubic);
	else
		tempGdi.SetInterpolationMode(Gdiplus::InterpolationMode::InterpolationModeDefault);
	tempGdi.DrawImage(bmp, 0, 0, sw, sh);
	cache = new Gdiplus::CachedBitmap(&temp, graphics);
}


#endif