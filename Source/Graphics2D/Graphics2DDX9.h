#ifndef GRAPHICS2DDX9_H
#define GRAPHICS2DDX9_H

#include <Graphics2D/Graphics2D.h>

class Graphics2DDX9 : public Graphics2D 
{
	public:

		LPDIRECT3D9 direct3D9;

		static LPDIRECT3DDEVICE9 device;

		LPDIRECT3DVERTEXBUFFER9 m_pVB;

		D3DXMATRIX transformMatrix, tempMatrix;

		static D3DXMATRIX Identity;

		uint32 color;

		ID3DXFont* font;

		ID3DXSprite* d3dxSprite;

	protected:

		D3DXMATRIX matrixStack[MATRIX_STACK_SIZE];

		int stackCursor;

	public:

		Graphics2DDX9(): stackCursor(0) { }
	
};

#include "Graphics2DDX9.inl"

#endif // GRAPHICS2DDX9_H