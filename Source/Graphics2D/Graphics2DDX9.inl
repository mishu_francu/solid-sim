
inline void Graphics2D::Translate(float x, float y)
{
	D3DXMatrixMultiply(&transformMatrix, D3DXMatrixTranslation(&tempMatrix, (float)x, (float)y, 0.0f), &transformMatrix );
}

inline void Graphics2D::Scale(float u, float v)
{
	D3DXMatrixMultiply(&transformMatrix, D3DXMatrixScaling(&tempMatrix, u, v, 1.0f), &transformMatrix );
}

inline void Graphics2D::Rotate(float angle)
{
	D3DXMatrixMultiply(&transformMatrix, D3DXMatrixRotationZ(&tempMatrix, angle), &transformMatrix );
}

inline void Graphics2D::PushMatrix() 
{
	ASSERT(stackCursor + 1 < MATRIX_STACK_SIZE);
	matrixStack[stackCursor++] = transformMatrix;
}

inline void Graphics2D::PopMatrix() 
{
	ASSERT(stackCursor - 1 >= 0);
	transformMatrix = matrixStack[--stackCursor];
	device->SetTransform(D3DTS_WORLD, &transformMatrix);
}

inline void Graphics2D::SetColor(uint32 color)
{
	this->color = color;
}

inline void Graphics2D::Flush()
{
}
