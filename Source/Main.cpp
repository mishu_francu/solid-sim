#include "../Engine/Base.h"

#include "SpriteTest.h"

// application entry point
int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	RUN_ENGINE(SpriteTest, hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}

