#include "Grid.h"
#include <Graphics2D/Graphics2D.h>

void Grid::SetSize(float width, float height)
{
	m_numCellsX = (int)width / m_cellSize;
	m_numCellsY = (int)height / m_cellSize;

	m_width = m_numCellsX * m_cellSize;
	m_height = m_numCellsY * m_cellSize;
		
	// init food grid
	m_codes.resize(m_numCellsX * m_numCellsY);
	for (int i = 0; i < m_numCellsY; i++)
	{
		for (int j = 0; j < m_numCellsX; j++)
		{
			int idx = i * m_numCellsX + j;
			m_codes[idx] = eEmpty;
			if (GetChance(70))
				m_codes[idx] = eFood;
		}
	}
}

void Grid::Draw(Graphics2D* graphics)
{
	graphics->SetColor(0xffffffff);
	graphics->FillRect(0, 0, m_width, m_height);
	// draw lines
	graphics->SetColor(0xff0000ff);
	for (int i = 0; i <= m_numCellsX; i++)
	{
		float x = i * m_cellSize;
		graphics->DrawLine(x, 0, x, m_height);
	}
	for (int i = 0; i <= m_numCellsY; i++)
	{
		float y = i * m_cellSize;
		graphics->DrawLine(0, y, m_width, y);
	}
	// draw food
	const float margin = 0.2f * m_cellSize;
	for (int i = 0; i < m_numCellsY; i++)
	{
		for (int j = 0; j < m_numCellsX; j++)
		{
			int idx = i * m_numCellsX + j;
			if (m_codes[idx] != eInvalid && m_codes[idx] != eEmpty)
			{
				if (m_codes[idx] == eFood)
					graphics->SetColor(0xff007f00);
				else if (m_codes[idx] == eBacteria)
					graphics->SetColor(0xff9f0000);
				else if (m_codes[idx] == eDead)
					graphics->SetColor(0xff000000);
				graphics->FillRect(j * m_cellSize + margin, i * m_cellSize + margin, m_cellSize - 2 * margin, m_cellSize - 2 * margin);
			}
		}
	}
}

int Grid::RequestPosition(CellType type)
{
	if (m_numOccupied >= m_numCellsX * m_numCellsY)
		return -1;
	while (true)
	{
		int x = rand() % m_numCellsX;
		int y = rand() % m_numCellsY;
		int idx = x + y * m_numCellsX;
		if (m_codes[idx] == eEmpty)
		{
			m_codes[idx] = type;
			m_numOccupied++;
			Handle handle;
			handle.x = x;
			handle.y = y;
			m_handles.push_back(handle);
			return (int)m_handles.size() - 1;
		}
	}
}

int Grid::RequestMove(int handle, int move)
{
	int x = m_handles[handle].x;
	int y = m_handles[handle].y;
	int idx0 = x + y * m_numCellsX;
	int type0 = m_codes[idx0];
	bool ret = true;
	switch(move)
	{
	case 0: // North
		ret = Decrement(y);
		break;
	case 1: // North east
		ret = Decrement(y) && Increment(x, m_numCellsX);
		break;
	case 2: // East
		ret = Increment(x, m_numCellsX);
		break;
	case 3: // South east
		ret = Increment(x, m_numCellsX) && Increment(y, m_numCellsY);
		break;
	case 4: // South
		ret = Increment(y, m_numCellsY);
		break;
	case 5: // South west
		ret = Increment(y, m_numCellsY) && Decrement(x);
		break;
	case 6: // West
		ret = Decrement(x);
		break;
	case 7: // North west
		ret = Decrement(x) && Decrement(y);
		break;
	default: // No move
		return type0;
	}
	if (!ret)
		return eInvalid;

	int idx = x + y * m_numCellsX;
	int type = m_codes[idx];
	if (type == type0)
		return type0;
	m_codes[idx0] = eEmpty;
	m_codes[idx] = type0;
	m_handles[handle].x = x;
	m_handles[handle].y = y;
	return type;
}

void Grid::AnnounceDeath(int handle)
{
	int x = m_handles[handle].x;
	int y = m_handles[handle].y;
	int idx0 = x + y * m_numCellsX;
	m_codes[idx0] = eDead;
	// TODO: erase from handles
}
