#ifndef GRID_H
#define GRID_H

#include <vector>

class Graphics2D;

inline bool GetChance(int percent)
{
	return (rand() % 100) < percent;
}

class Grid
{
public:
	enum CellType
	{
		eInvalid = -1,
		eEmpty,
		eFood,
		eBacteria,
		eDead
	};

	struct Handle
	{
		int x, y;
	};

private:
	int m_cellSize;
	int m_width, m_height;
	int m_numCellsX, m_numCellsY;
	std::vector<int> m_codes;
	int m_numOccupied;
	std::vector<Handle> m_handles;

public:
	Grid(int cellSize) 
		: m_cellSize(cellSize)
		, m_numOccupied(0)
	{
	}

	void SetSize(float width, float height);

	void Draw(Graphics2D* graphics);

	int RequestPosition(CellType type);

	int RequestMove(int handle, int move);

	void AnnounceDeath(int handle);

private:
	bool Decrement(int& y)
	{
		if (y > 0)
		{
			y--;
			return true;
		}
		return false;
	}

	bool Increment(int& val, int max)
	{
		if (val < max - 1)
		{
			val++;
			return true;
		}
		return false;
	}
};

class Bacteria
{
public:
	enum
	{
		kInitialEnergy = 10,
		kFoodValue = 4,
	};

private:
	Grid& m_grid;
	int m_handle;
	int m_energy;

public:
	Bacteria(Grid& grid) 
		: m_grid(grid)
		, m_handle(-1)
		, m_energy(kInitialEnergy)
	{
	}

	void operator =(const Bacteria& other)
	{
		m_grid = other.m_grid;
		m_handle = other.m_handle;
		m_energy = other.m_energy;
	}

	int GetEnergy() { return m_energy; }

	void Init()
	{
		m_handle = m_grid.RequestPosition(Grid::eBacteria);
	}

	void Move()
	{
		if (m_energy < 0)
			return;
		m_energy--;
		if (m_energy < 0)
		{
			m_grid.AnnounceDeath(m_handle);
			return;
		}
		int move = rand() % 9;
		if (m_grid.RequestMove(m_handle, move) == Grid::eFood)
			m_energy += kFoodValue;
	}

	Bacteria Replicate()
	{
		Bacteria ret(m_grid);
		m_energy -= kInitialEnergy;
		ret.Init();
		return ret;
	}
};

#endif // GRID_H