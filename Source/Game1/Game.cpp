#ifndef USE_GDIPLUS
#	define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#endif
// Windows Header Files:
#include <windows.h>
#include <tchar.h>

#include "Game.h"
#include <Graphics2D/Graphics2D.h>

void Game::OnDraw()
{
	Update();

	// draw
	m_grid.Draw(graphics);
}

void Game::Update()
{
	m_frames++;
	if (m_frames < 20)
		return;

	m_frames = 0;
	std::vector<Bacteria> duplicates;
	std::vector<Bacteria>::iterator it = m_bacteria.begin();
	while (it != m_bacteria.end())
	{
		if (it->GetEnergy() < 0)
		{
			it = m_bacteria.erase(it);
			continue;
		}
		if (it->GetEnergy() >= 2 * Bacteria::kInitialEnergy)
		{
			duplicates.push_back(it->Replicate());
		}
		it->Move();
		++it;
	}
	for (size_t i = 0; i < duplicates.size(); ++i)
	{
		m_bacteria.push_back(duplicates[i]);
	}
}

// application entry point
int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	RUN_ENGINE(Game, hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}

