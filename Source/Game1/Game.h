#ifndef GAME_H
#define GAME_H

#include <Engine/Engine.h>
#include <vector>
#include "Grid.h"

class Game : public Engine // TODO: don't inherit
{
private:
	Grid m_grid;
	std::vector<Bacteria> m_bacteria;
	int m_frames;

public:
	Game() : m_grid(20), m_frames(0) { }
	
	virtual void OnDraw();

	virtual void OnResize()
	{
		m_grid.SetSize(getScreenWidth(), getScreenHeight());
		m_bacteria.resize(10, Bacteria(m_grid));
		for (size_t i = 0; i < m_bacteria.size(); i++)
			m_bacteria[i].Init();
	}

private:
	void Update();
};

#endif // GAME_H