#include "pch.h"
#include "Skeleton.h"
#include <Engine/xml.h>
#include <Engine/Utils.h>
#include <Math/Utils.h>
#include <Geometry/Mesh.h>

#include <stack>

uint32 crc32(uint32 crc, const void *buf, uint32 size);

namespace Geometry
{
	// I think this was an OGRE XML format
	void LoadSkeletonFromXml(const char* path, Skeleton& skel)
	{
		XML* config = new XML(path);
		if (config->IntegrityTest() || config->ParseStatus() == 0)
		{
			char str[128];
			XMLElement* root = config->GetRootElement();

			XMLElement* xBones = root->FindElementZ("bones");
			if (xBones)
			{
				for (size_t i = 0; i < xBones->GetChildrenNum(); i++)
				{
					XMLElement* xBone = xBones->GetChildren()[i];
					Skeleton::Bone bone;
					XMLVariable* xId = xBone->FindVariableZ("id");
					if (xId)
					{
						bone.id = xId->GetValueInt();
					}
					XMLVariable* xName = xBone->FindVariableZ("name");
					if (xName)
					{
						xName->GetValue(str);
						bone.name = str;
						bone.nameCrc = crc32(0, str, (uint32)strlen(str));
					}
					XMLElement* xPos = xBone->FindElementZ("position");
					if (xPos)
					{
						bone.pos = Math::ReadVector3(xPos);
						bone.pos0 = bone.pos;
					}
					XMLElement* xScale = xBone->FindElementZ("scale");
					if (xScale)
					{
						bone.scale = Math::ReadVector3(xScale);
					}
					XMLElement* xRot = xBone->FindElementZ("rotation");
					if (xRot)
					{
						XMLVariable* xAngle = xRot->FindVariableZ("angle");
						xAngle->GetValue(str);
						float angle = (float)atof(str);
						XMLElement* xAxis = xRot->FindElementZ("axis");
						if (xAxis)
						{
							xAxis->FindVariableZ("x")->GetValue(str);
							float x = (float)atof(str);
							xAxis->FindVariableZ("y")->GetValue(str);
							float y = (float)atof(str);
							xAxis->FindVariableZ("z")->GetValue(str);
							float z = (float)atof(str);
							Vector3 axis(x, y, z);
							bone.rot.s = cosf(angle * 0.5f);
							bone.rot.v = axis * sinf(angle * 0.5f);
							bone.rot = qNormalize(bone.rot);
							bone.rot0 = bone.rot;
						}
					}
					skel.AddBone(bone);
				}
			}

			XMLElement* xBoneTree = root->FindElementZ("bonehierarchy");
			if (xBoneTree)
			{
				for (size_t i = 0; i < xBoneTree->GetChildrenNum(); i++)
				{
					XMLElement* xBoneParent = xBoneTree->GetChildren()[i];
					XMLVariable* xBone = xBoneParent->FindVariableZ("bone");
					if (xBone)
					{
						xBone->GetValue(str);
						Skeleton::Bone* bone = skel.GetBoneByName(str);
						XMLVariable* xParent = xBoneParent->FindVariableZ("parent");
						if (bone && xParent)
						{
							xParent->GetValue(str);
							Skeleton::Bone* parent = skel.GetBoneByName(str);
							bone->parent = parent;
							parent->children.push_back(bone);
						}
					}
				}
				skel.FindRoot();
			}

			XMLElement* xAnimations = root->FindElementZ("animations");
			if (xAnimations)
			{
				for (size_t i = 0; i < xAnimations->GetChildrenNum(); i++)
				{
					XMLElement* xAnimation = xAnimations->GetChildren()[i];
					Skeleton::Animation anim;
					xAnimation->FindVariableZ("name")->GetValue(str);
					anim.name = str;
					xAnimation->FindVariableZ("length")->GetValue(str);
					anim.length = (float)atof(str);

					// read bone tracks
					XMLElement* xTracks = xAnimation->FindElementZ("tracks");
					if (xTracks)
					{
						for (size_t j = 0; j < xTracks->GetChildrenNum(); j++)
						{
							XMLElement* xTrack = xTracks->GetChildren()[j];
							xTrack->FindVariableZ("bone")->GetValue(str);
							Skeleton::Track track;
							track.bone = skel.GetBoneByName(str);

							// read track keyframes
							XMLElement* xKeyframes = xTrack->FindElementZ("keyframes");
							if (xKeyframes)
							{
								for (size_t k = 0; k < xKeyframes->GetChildrenNum(); k++)
								{
									XMLElement* xKeyframe = xKeyframes->GetChildren()[k];
									Skeleton::Keyframe kf;
									xKeyframe->FindVariableZ("time")->GetValue(str);
									kf.time = (float)atof(str);
									XMLElement* xTranslate = xKeyframe->FindElementZ("translate");
									if (xTranslate)
									{
										xTranslate->FindVariableZ("x")->GetValue(str);
										float x = (float)atof(str);
										xTranslate->FindVariableZ("y")->GetValue(str);
										float y = (float)atof(str);
										xTranslate->FindVariableZ("z")->GetValue(str);
										float z = (float)atof(str);
										kf.offset.Set(x, y, z);
									}
									XMLElement* xRot = xKeyframe->FindElementZ("rotate");
									if (xRot)
									{
										XMLVariable* xAngle = xRot->FindVariableZ("angle");
										xAngle->GetValue(str);
										float angle = (float)atof(str);
										XMLElement* xAxis = xRot->FindElementZ("axis");
										if (xAxis)
										{
											xAxis->FindVariableZ("x")->GetValue(str);
											float x = (float)atof(str);
											xAxis->FindVariableZ("y")->GetValue(str);
											float y = (float)atof(str);
											xAxis->FindVariableZ("z")->GetValue(str);
											float z = (float)atof(str);
											Vector3 axis(x, y, z);
											kf.rot.s = cosf(angle * 0.5f);
											kf.rot.v = axis * sinf(angle * 0.5f);
											kf.rot = qNormalize(kf.rot);
										}
									}
									track.keyframes.push_back(kf);
								}
							}

							anim.tracks.push_back(track);
						}
					}

					skel.AddAnimation(anim);
				}
			}
		}
	}

	Skeleton::Bone* Skeleton::GetBoneByName(const char* name)
	{
		uint32 crc = crc32(0, name, (uint32)strlen(name));
		// linear search !!!
		for (size_t i = 0; i < mBones.size(); i++)
		{
			if (mBones[i].nameCrc == crc)
				return &mBones[i];
		}
		return NULL;
	}

	void Skeleton::FindRoot()
	{
		for (size_t i = 0; i < mBones.size(); i++)
		{
			if (mBones[i].parent == NULL)
			{
				mRoot = &mBones[i];
				return;
			}
		}
		mRoot = NULL;
	}

	void Skeleton::ComputeWorldTransforms()
	{
		std::stack<Skeleton::Bone*> stack;
		Skeleton::Bone* root = mRoot;
		root->worldRot = root->rot;
		root->worldPos = root->pos;
		stack.push(root);
		while (!stack.empty())
		{
			Skeleton::Bone* parent = stack.top();
			stack.pop();

			for (size_t i = 0; i < parent->children.size(); i++)
			{
				Skeleton::Bone* child = parent->children[i];
				stack.push(child);

				child->worldRot = parent->worldRot * child->rot;
				child->worldPos = parent->worldPos + qRotate(parent->worldRot, child->pos);
			}
		}
	}

	void Skeleton::SetCurrentAnimation(int i)
	{
		mCurrAnim = i;
		Animation& anim = mAnimations[mCurrAnim];
		for (size_t i = 0; i < anim.tracks.size(); i++)
			anim.tracks[i].currFrame = 0;
		mTime = 0;
	}

	void Skeleton::UpdateAnimation(float dt)
	{
		Animation& anim = mAnimations[mCurrAnim];
		mTime += dt;
		for (size_t i = 0; i < anim.tracks.size(); i++)
		{
			Track& track = anim.tracks[i];
			if (track.currFrame >= track.keyframes.size() - 1)
				continue;
			Bone* bone = track.bone;
			if (mTime >= track.keyframes[track.currFrame].time)
			{
				track.currFrame++;
			}
			const Keyframe& frame1 = track.keyframes[track.currFrame];
			float time0 = 0;
			Quaternion rot0 = bone->rot0;
			if (track.currFrame > 0)
			{
				const Keyframe& frame0 = track.keyframes[track.currFrame - 1];
				time0 = frame0.time;
				rot0 = frame0.rot;
			}
			float t = (mTime - time0) / (frame1.time - time0);
			//bone->translate = frame0.offset + t * (frame1.offset - frame0.offset);
			bone->pos = bone->pos0;// + bone->translate;
			float sigma = 1;
			if (qDot(rot0, frame1.rot) < 0)
				sigma = -1;
			Quaternion q = (1.f - t) * rot0 + sigma * t * frame1.rot; // lerp between rotations
			bone->rot = qNormalize(q);
		}
		// TODO: detect stop
	}

	void Skeleton::SkinMesh(const Geometry::Mesh& mesh, Geometry::Mesh& skin)
	{
		if (mesh.weights.empty())
			return;

		// compute world transformations; TODO: once
		std::stack<Skeleton::Bone*> stack;
		stack.push(mRoot);
		mRoot->transform0 = Matrix4(mRoot->rot0.ToMatrix(), mRoot->pos0);
		while (!stack.empty())
		{
			Skeleton::Bone* parent = stack.top();
			stack.pop();

			for (size_t i = 0; i < parent->children.size(); i++)
			{
				Skeleton::Bone* child = parent->children[i];
				stack.push(child);

				Matrix4 T(child->rot0.ToMatrix(), child->pos0);
				child->transform0 = parent->transform0 * T;
			}
		}

		// compute skinned position
		skin.vertices.resize(mesh.vertices.size());
		skin.velocities.resize(mesh.vertices.size());
		for (size_t i = 0; i < mesh.vertices.size(); i++)
		{
			Vector3 v;
			//float sum = 0;
			for (int j = 0; j < Geometry::Mesh::Weights::MAX_WEIGHTS; j++)
			{
				if (mesh.weights[i].bw[j].id < 0)
					break;
				Bone* bone = GetBoneById(mesh.weights[i].bw[j].id);
				float w = mesh.weights[i].bw[j].weight;
				Matrix4 T = bone->transform;
				Matrix4 T0 = bone->transform0;
				Matrix4 T0inv = T0.GetInverseTransform(); // TODO: store
				Matrix4 M = T * T0inv;
				v += w * M.Transform(mesh.vertices[i]);
				//sum += w;
			}
			skin.velocities[i] = v - skin.vertices[i];
			skin.vertices[i] = v;
		}
	}

} // namespace Geometry