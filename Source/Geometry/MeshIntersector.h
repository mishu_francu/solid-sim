#pragma once

#include <Geometry/Mesh.h>
#include <Geometry/AabbTree.h>

namespace Geometry
{
	class MeshIntersector
	{
	public:
		// ideally, back to private
		enum class SideType : uint8
		{
			INVALID = 0,
			EXTERIOR = 1,
			INTERIOR = 2,
		};

		enum IntersectionType : int
		{
			FACE_PT = 0, // this is the default
			END_PT = 1,
			EDGE_PT = 2,
			VERTEX_PT = 4,
		};

		struct ProxyInfo
		{
			int intersectionTri = -1;
			Vector3 intersectionPt;
			float distanceToIntersection = 1e10f; // TODO: max float
			SideType side = SideType::INVALID; // or color
			bool visited = false;
		};

		struct IntersectionPoint
		{
			Vector3 pt;
			uint32 edge, tri;
			const Mesh* meshEdge; // the mesh containing the edge
			const Mesh* meshTri; // the mesh containing the triangle
			// additional data
			int edgeOnTri = -1;
			uint32 type;
			uint64 key;

			void Classify(BarycentricCoords coords, float t, float epsilon = 0.02f);
			uint64 MakeKey(uint32 edge, uint32 tri) const { return (((uint64)edge) << 32) | tri; }
			bool operator <(const IntersectionPoint& other) { return key < other.key; }
		};

		struct TrianglePair
		{
			int tri1, tri2; // the 2 triangle indices
			uint32 ptIdx; // the index of the intersection point
		};

	public:
		void InitMeshes(Mesh* mesh1, Mesh* mesh2)
		{
			mMeshSrc1 = mesh1;
			mMeshSrc2 = mesh2;
		}

		void SetTransforms(const Matrix4& mat1, const Matrix4& mat2);

		void ClearIntersection();
		void IntersectFast();
		void IntersectSlow();

		void UpdateMesh1()
		{
			mMesh1 = *mMeshSrc1;
			mMesh1.Transform(mTransform1, false);
			delete mTree1; // FIXME
			mTree1 = ComputeMeshTree(mMesh1, ATF_TRIANGLES | ATF_EDGES, 10, 0.1f);
		}

		void UpdateMesh2()
		{
			mMesh2 = *mMeshSrc2;
			mMesh2.Transform(mTransform2, false);
			delete mTree2; // FIXME
			mTree2 = ComputeMeshTree(mMesh2, ATF_TRIANGLES | ATF_EDGES, 10, 0.1f);
		}

		const Mesh& GetMesh1() const { return mMesh1; }
		const Mesh& GetMesh2() const { return mMesh2; }
		const std::vector<ProxyInfo>& GetVertexInfos1() const { return mVertexInfos1; }
		std::vector<ProxyInfo>& GetVertexInfos1() { return mVertexInfos1; }
		const std::vector<ProxyInfo>& GetVertexInfos2() const { return mVertexInfos2; }
		const std::vector<IntersectionPoint>& GetIntersections() const { return mIntersections; }
		const std::vector<int>& GetSelection1() const { return mSelection1; }
		const std::vector<int>& GetSelection2() const { return mSelection2; }
		const Mesh& GetIntersectingMesh1() const { return mIntMesh1; }
		const Mesh& GetIntersectingMesh2() const { return mIntMesh2; }
		const std::vector<Vector3>& GetPolyLine() const { return mPolyLine; }

	private:
		struct CandidatePair
		{
			const Mesh* meshTri;
			const Mesh* meshEdge;
			uint32 tri, edge;
		};

	private:
		void TestTrees(const Mesh& mesh1, const Mesh& mesh2, AabbTree* node1, AabbTree* node2, std::vector<CandidatePair>& pairs);
		bool IntersectEdgeTriangle(const Mesh& mesh1, const Mesh& mesh2, uint32 e, uint32 tri, std::vector<int>& intTris, std::vector<ProxyInfo>& vertexInfos);
		void FloodFill(int start, const Mesh& mesh1, std::vector<ProxyInfo>& vertexInfos1);

		void ExtractInteriorMesh(const Mesh& meshIn, const std::vector<ProxyInfo>& vertexInfos, Mesh& meshOut);

		void IntersectEdgesAndFaces(const Mesh& mesh1, std::vector<ProxyInfo>& vertexInfos1, const Mesh& mesh2, std::vector<int>& intTris2, Mesh& meshOut);

		void AddTriTriPair(size_t i, int e, int tri, const Mesh* meshEdge);

	private:
		Mesh* mMeshSrc1 = nullptr;
		Mesh* mMeshSrc2 = nullptr;
		Mesh mMesh1;
		Mesh mMesh2;
		Matrix4 mTransform1;
		Matrix4 mTransform2;
		std::vector<ProxyInfo> mVertexInfos1;
		std::vector<ProxyInfo> mVertexInfos2;
		std::vector<int> mSelection1;
		std::vector<int> mSelection2;
		std::vector<IntersectionPoint> mIntersections;
		std::vector<TrianglePair> mTriTriList;
		std::vector<int> mIntTriangles1;
		std::vector<int> mIntTriangles2;
		Mesh mIntMesh1;
		Mesh mIntMesh2;
		AabbTree* mTree1 = nullptr;
		AabbTree* mTree2 = nullptr;
		std::vector<Vector3> mPolyLine;
	};
}
