#include "pch.h"
#include <Geometry/Collision3D.h>
#include <Engine/Utils.h>
#include <Engine/xml.h>
#include "Mesh.h"
#include <Geometry/Skeleton.h>

#ifndef ANDROID_NDK

#include <assimp/cimport.h>        // Plain-C interface
#include <assimp/scene.h>          // Output data structure
#include <assimp/postprocess.h>    // Post processing flags

#endif

#include <fstream>

uint32 crc32(uint32 crc, const void* buf, uint32 size);

namespace Geometry
{
	void CreateMesh(const struct aiScene* sc, const struct aiNode* nd, Mesh& collMesh,
		const Vector3& offset, float scale, bool flipYZ)
	{
#ifndef ANDROID_NDK
		ASSERT(sc != NULL);

		for (size_t n = 0; n < nd->mNumMeshes; ++n)
		{
			// found one
			const struct aiMesh* mesh = sc->mMeshes[nd->mMeshes[n]];

			size_t baseIdx = collMesh.vertices.size();
			for (size_t i = 0; i < mesh->mNumVertices; i++)
			{
				const aiVector3D& src = mesh->mVertices[i];
				aiVector3D t = /*nd->mTransformation * */src;
				Vector3 dst(t.x * scale, t.y * scale, t.z * scale);
				if (flipYZ)
					dst.Set(t.x * scale, t.z * scale, t.y * scale);
				collMesh.vertices.push_back(dst + offset);

				if (mesh->mNormals)
				{
					const aiVector3D& srcN = mesh->mNormals[i];
					Vector3 n(srcN.x, srcN.y, srcN.z);
					n.Normalize();
					collMesh.normals.push_back(n);
				}

				if (mesh->GetNumColorChannels() > 0 && mesh->HasVertexColors(0))
				{
					Vector3 col(mesh->mColors[0][i].r, mesh->mColors[0][i].g, mesh->mColors[0][i].b);
					collMesh.colors.push_back(col);
				}

				if (mesh->GetNumUVChannels() > 0 && mesh->HasTextureCoords(0))
				{
					Vector2 uv(mesh->mTextureCoords[0][i].x, mesh->mTextureCoords[0][i].y);
					collMesh.uvs.push_back(uv);
				}
			}

			for (size_t t = 0; t < mesh->mNumFaces; ++t)
			{
				const struct aiFace* face = &mesh->mFaces[t];
				for (size_t i = 0; i < face->mNumIndices; i += 3)
				{
					size_t index0 = face->mIndices[i] + baseIdx;
					size_t index1 = face->mIndices[i + 1] + baseIdx;
					size_t index2 = face->mIndices[i + 2] + baseIdx;

					//Need this to avoid back face culling issue.
					if (flipYZ) {
						collMesh.indices.push_back((uint32)index0);
						collMesh.indices.push_back((uint32)index2);
						collMesh.indices.push_back((uint32)index1);
					}
					else {
						collMesh.indices.push_back((uint32)index0);
						collMesh.indices.push_back((uint32)index1);
						collMesh.indices.push_back((uint32)index2);
					}
				}
			}

			//return;
		}

		// recurse if no mesh found
		for (size_t n = 0; n < nd->mNumChildren; ++n)
		{
			CreateMesh(sc, nd->mChildren[n], collMesh, offset, scale, flipYZ);
		}
#endif
	}

	bool LoadMesh(const char* path, Mesh& mesh, const Vector3& offset, float scale, bool flipYZ)
	{
		const aiScene* scene = aiImportFile(path, aiProcess_JoinIdenticalVertices | aiProcess_Triangulate);
		if (scene == NULL)
			return false;
		mesh.indices.clear();
		mesh.vertices.clear();
		CreateMesh(scene, scene->mRootNode, mesh, offset, scale, flipYZ);
		return true;
	}

	void CreatePlane(Mesh& mesh)
	{
		const float size = 200;
		mesh.vertices.push_back(Vector3(size, 0, -size));
		mesh.vertices.push_back(Vector3(-size, 0, -size));
		mesh.vertices.push_back(Vector3(-size, 0, size));
		mesh.vertices.push_back(Vector3(size, 0, size));

		mesh.indices.push_back(0);
		mesh.indices.push_back(1);
		mesh.indices.push_back(2);
		mesh.indices.push_back(0);
		mesh.indices.push_back(2);
		mesh.indices.push_back(3);
	}

	void CreateBox(Mesh& mesh)
	{
		const float size = 200;
		mesh.vertices.push_back(Vector3(size, size, -size));
		mesh.vertices.push_back(Vector3(-size, size, -size));
		mesh.vertices.push_back(Vector3(-size, size, size));
		mesh.vertices.push_back(Vector3(size, size, size));

		mesh.vertices.push_back(Vector3(size, -size, -size));
		mesh.vertices.push_back(Vector3(-size, -size, -size));
		mesh.vertices.push_back(Vector3(-size, -size, size));
		mesh.vertices.push_back(Vector3(size, -size, size));

		mesh.indices.push_back(0);
		mesh.indices.push_back(1);
		mesh.indices.push_back(2);
		mesh.indices.push_back(0);
		mesh.indices.push_back(2);
		mesh.indices.push_back(3);

		mesh.indices.push_back(4);
		mesh.indices.push_back(6);
		mesh.indices.push_back(5);
		mesh.indices.push_back(4);
		mesh.indices.push_back(7);
		mesh.indices.push_back(6);

		mesh.indices.push_back(0);
		mesh.indices.push_back(4);
		mesh.indices.push_back(5);
		mesh.indices.push_back(0);
		mesh.indices.push_back(5);
		mesh.indices.push_back(1);

		mesh.indices.push_back(3);
		mesh.indices.push_back(6);
		mesh.indices.push_back(7);
		mesh.indices.push_back(3);
		mesh.indices.push_back(2);
		mesh.indices.push_back(6);

		mesh.indices.push_back(1);
		mesh.indices.push_back(5);
		mesh.indices.push_back(6);
		mesh.indices.push_back(1);
		mesh.indices.push_back(6);
		mesh.indices.push_back(2);

		mesh.indices.push_back(0);
		mesh.indices.push_back(7);
		mesh.indices.push_back(4);
		mesh.indices.push_back(0);
		mesh.indices.push_back(3);
		mesh.indices.push_back(7);
	}

	void CreateTetrahedron(Mesh& mesh, float r)
	{
		const float h = r / 3;
		const float d = 2 * sqrtf(2.f) * r / 3;
		const float hl = sqrtf(2.f / 3.f) * r;
		const float k = d / 2;

		mesh.vertices.push_back(Vector3(0, r, 0)); // A 0
		mesh.vertices.push_back(Vector3(-hl, -h, k)); // B 1
		mesh.vertices.push_back(Vector3(hl, -h, k)); // C 2
		mesh.vertices.push_back(Vector3(0, -h, -d)); // D 3

		mesh.indices.push_back(0);
		mesh.indices.push_back(1);
		mesh.indices.push_back(2);

		mesh.indices.push_back(0);
		mesh.indices.push_back(2);
		mesh.indices.push_back(3);

		mesh.indices.push_back(0);
		mesh.indices.push_back(3);
		mesh.indices.push_back(1);

		mesh.indices.push_back(1);
		mesh.indices.push_back(3);
		mesh.indices.push_back(2);
	}

	void Subdivide(Mesh& mesh, uint16 i1, uint16 i2, uint16 i3, float r)
	{
		const Vector3& a = mesh.vertices[i1];
		const Vector3& b = mesh.vertices[i2];
		const Vector3& c = mesh.vertices[i3];

		Vector3 m = 0.5f * (a + b);
		Vector3 n = 0.5f * (b + c);
		Vector3 p = 0.5f * (a + c);

		m.Scale(r / m.Length());
		n.Scale(r / n.Length());
		p.Scale(r / p.Length());

		// TODO: don't duplicate edge half vertices
		uint16 i4 = mesh.AddVertex(m);
		uint16 i5 = mesh.AddVertex(n);
		uint16 i6 = mesh.AddVertex(p);

		mesh.AddTriangle(i1, i4, i6);
		mesh.AddTriangle(i4, i2, i5);
		mesh.AddTriangle(i4, i5, i6);
		mesh.AddTriangle(i6, i5, i3);
	}

	void CreateSphere(Mesh& mesh, float r, int level)
	{
		Mesh tet;
		CreateTetrahedron(tet, r);

		mesh.vertices.insert(mesh.vertices.begin(), tet.vertices.begin(), tet.vertices.end());

		for (int iter = 0; iter < level; iter++)
		{
			mesh.indices.clear();
			for (size_t i = 0; i < tet.indices.size(); i += 3)
			{
				Subdivide(mesh, tet.indices[i], tet.indices[i + 1], tet.indices[i + 2], r);
			}
			tet = mesh;
		}
	}

	void LoadMeshFromOgreXml(const char* path, Mesh& mesh)
	{
		XML* config = new XML(path);
		if (config->IntegrityTest() || config->ParseStatus() == 0)
		{
			char str[128];
			XMLElement* root = config->GetRootElement();

			XMLElement* xSubs = root->FindElementZ("submeshes");
			if (xSubs)
			{
				for (size_t i = 0; i < xSubs->GetChildrenNum(); i++)
				{
					size_t base = mesh.vertices.size();
					XMLElement* xSub = xSubs->GetChildren()[i];
					XMLElement* xFaces = xSub->FindElementZ("faces");
					if (xFaces)
					{
						for (size_t j = 0; j < xFaces->GetChildrenNum(); j++)
						{
							XMLElement* xFace = xFaces->GetChildren()[j];
							int i1 = xFace->FindVariableZ("v1")->GetValueInt();
							int i2 = xFace->FindVariableZ("v2")->GetValueInt();
							int i3 = xFace->FindVariableZ("v3")->GetValueInt();
							mesh.AddTriangle((uint16)(i1 + base), (uint16)(i2 + base), uint16(i3 + base));
						}
					}
					XMLElement* xGeom = xSub->FindElementZ("geometry");
					if (xGeom)
					{
						for (size_t j = 0; j < xGeom->GetChildrenNum(); j++)
						{
							XMLElement* xVB = xGeom->GetChildren()[j];
							bool hasPos = false;
							bool hasNormals = false;
							bool hasUV = false;
							XMLVariable* xFlag = xVB->FindVariableZ("positions");
							if (xFlag)
							{
								xFlag->GetValue(str);
								if (strncmp(str, "true", 4) == 0) hasPos = true;
							}
							xFlag = xVB->FindVariableZ("normals");
							if (xFlag)
							{
								xFlag->GetValue(str);
								if (strncmp(str, "true", 4) == 0) hasNormals = true;
							}
							xFlag = xVB->FindVariableZ("texture_coords");
							if (xFlag)
							{
								int nChannels = xFlag->GetValueInt();
								hasUV = nChannels > 0;
							}
							for (size_t k = 0; k < xVB->GetChildrenNum(); k++)
							{
								XMLElement* xVertex = xVB->GetChildren()[k];
								if (hasPos)
								{
									Vector3 pos = Math::ReadVector3(xVertex->FindElementZ("position"));
									mesh.vertices.push_back(pos);
								}
								if (hasNormals)
								{
									Vector3 normal = Math::ReadVector3(xVertex->FindElementZ("normal"));
									mesh.normals.push_back(normal);
								}
								if (hasUV)
								{
									XMLElement* xUV = xVertex->FindElementZ("texcoord");
									xUV->FindVariableZ("u")->GetValue(str);
									float u = (float)atof(str);
									xUV->FindVariableZ("v")->GetValue(str);
									float v = (float)atof(str);
									Vector2 uv(u, v);
									mesh.uvs.push_back(uv);
								}
							}
						}
					}

					XMLElement* xWeights = xSub->FindElementZ("boneassignments");
					if (xWeights)
					{
						std::vector<int> count(mesh.vertices.size(), 0);
						mesh.weights.resize(mesh.vertices.size());
						for (size_t j = 0; j < xWeights->GetChildrenNum(); j++)
						{
							XMLElement* xWeight = xWeights->GetChildren()[j];
							int vId = (int)base + xWeight->FindVariableZ("vertexindex")->GetValueInt();
							int bId = xWeight->FindVariableZ("boneindex")->GetValueInt();
							xWeight->FindVariableZ("weight")->GetValue(str);
							float w = (float)atof(str);

							int wId = count[vId]++;
							ASSERT(wId < Mesh::Weights::MAX_WEIGHTS); // TODO: reduce to 4 or 8 influences
							mesh.weights[vId].bw[wId].id = bId;
							mesh.weights[vId].bw[wId].weight = w;
						}
					}
				}
			}
		}
	}

	void CreateSkeleton(const aiScene* scene, aiNode* node, Skeleton& skeleton, Skeleton::Bone* rootBone)
	{
		//Printf("node %s\n", );
		// assume the bone names are already loaded
		Skeleton::Bone* bone = skeleton.GetBoneByName(node->mName.C_Str());
		if (bone != nullptr)
		{
			if (rootBone == nullptr)
			{
				// it means this is the root bone
				skeleton.SetRoot(bone);
			}
			else
			{
				// add the double reference between child and parent
				bone->parent = rootBone;
				rootBone->children.push_back(bone);
			}

			// position
			aiMatrix4x4 mat = node->mTransformation;
			bone->pos.Set(mat.a4, mat.b4, mat.c4);
			bone->pos0 = bone->pos;
			// rotation
			Matrix3 rot(mat.a1, mat.a2, mat.a3,
				mat.b1, mat.b2, mat.b3,
				mat.c1, mat.c2, mat.c3);
			bone->rot.SetFromMatrix(rot);
			bone->rot0 = bone->rot;
		}

		// look for  meshes
		for (size_t n = 0; n < node->mNumMeshes; ++n)
		{
			// found one
			const struct aiMesh* mesh = scene->mMeshes[node->mMeshes[n]];

			//Printf("num bones: %d\n", mesh->mNumBones);
			for (uint32 i = 0; i < mesh->mNumBones; i++)
			{
				// name
				const char* name = mesh->mBones[i]->mName.C_Str();
				//Printf("%d: %s\n", i, name);
				Skeleton::Bone newBone;
				newBone.name = name;
				newBone.nameCrc = crc32(0, name, (uint32)strlen(name));
				newBone.id = i;

				skeleton.AddBone(newBone);
			}
		}

		// recurse if no mesh found
		for (size_t n = 0; n < node->mNumChildren; ++n)
		{
			CreateSkeleton(scene, node->mChildren[n], skeleton, bone);
		}
	}

	bool LoadSkeleton(const char* path, Skeleton& skeleton)
	{
		const aiScene* scene = aiImportFile(path, aiProcessPreset_TargetRealtime_MaxQuality);
		if (scene == NULL)
			return false;

		CreateSkeleton(scene, scene->mRootNode, skeleton, nullptr);

		if (scene->HasAnimations())
		{
			//Printf("num anims %d\n", scene->mNumAnimations);
			for (uint32 i = 0; i < scene->mNumAnimations; i++)
			{
				aiAnimation* aiAnim = scene->mAnimations[i];
				//Printf("anim %d: %s (%d channels)\n", i, aiAnim->mName.C_Str(), aiAnim->mNumChannels);

				Skeleton::Animation anim;
				anim.name = aiAnim->mName.C_Str();
				anim.length = (float)aiAnim->mDuration; // in ticks

				for (uint32 j = 0; j < aiAnim->mNumChannels; j++)
				{
					aiNodeAnim* aiChannel = aiAnim->mChannels[j];

					Skeleton::Track track;
					track.bone = skeleton.GetBoneByName(aiChannel->mNodeName.C_Str());
					if (track.bone != nullptr)
					{
						for (uint32 k = 0; k < aiChannel->mNumRotationKeys; k++)
						{
							Skeleton::Keyframe key;
							key.time = (float)aiChannel->mRotationKeys[k].mTime;
							aiQuaternion quat = aiChannel->mRotationKeys[k].mValue;
							key.rot = Quaternion(quat.w, Vector3(quat.x, quat.y, quat.z));

							track.keyframes.push_back(key);
						}

						anim.tracks.push_back(track);
					}
					else
						Printf("Bone %s not found\n", aiChannel->mNodeName.C_Str());
				}

				skeleton.AddAnimation(anim);
			}
		}

		return true;
	}

	bool LoadMeshFromObj(const char* path, Mesh& mesh)
	{
		mesh.Clear();

		FILE* ptr_file;
		char buf[1000];

		errno_t ret = fopen_s(&ptr_file, path, "r");
		if (ret != 0)
		{
			printf("Couldn't open file %s\n", path);
			return false;
		}

		int nod = 0;
		
		while (fgets(buf, 1000, ptr_file) != NULL)
		{
			char* ptr = buf;
			char* word = buf + 2;

			if (buf[0] == 'v' && buf[1] != 't')
			{
				// read vertex
				Vector3 vec;
				int counter = 0;
				while (ptr = strchr(word, ' '))
				{
					if (ptr == word)
					{
						word++;
						continue;
					}
					*ptr = '\0';
					float x = (float)atof(word);
					vec[counter] = x;
					counter++;
					word = ptr + 1;
				}
				vec[2] = (float)atof(word);
				if (buf[1] == 'n')
					mesh.normals.push_back(vec);
				else
					mesh.vertices.push_back(vec);
			}

			if (buf[0] == 'f')
			{
				// read face
				int indx[3];
				int counter = 0;
				while (ptr = strchr(word, ' '))
				{
					*ptr = '\0';

					int x = atoi(word);
					indx[counter] = x;
					counter++;
					word = ptr + 1;
				}
				if (counter < 3)
					indx[2] = atoi(word);

				mesh.indices.push_back(indx[0] - 1);
				mesh.indices.push_back(indx[1] - 1);
				mesh.indices.push_back(indx[2] - 1);
			}
		}

		fclose(ptr_file);
		return true;
	}

	bool SaveMeshToObj(const char* path, const Mesh& mesh)
	{
		std::ofstream file;
		file.open(path, std::ios::out);
		file << "# File created by SolidSim" << std::endl;
		for (size_t i = 0; i < mesh.vertices.size(); i++)
		{
			const Vector3& v = mesh.vertices[i];
			file << "v " << v.x << " " << v.y << " " << v.z << std::endl;
		}
		for (size_t i = 0; i < mesh.normals.size(); i++)
		{
			const Vector3& v = mesh.normals[i];
			file << "vn " << v.x << " " << v.y << " " << v.z << std::endl;
		}
		for (size_t i = 0; i < mesh.GetNumTriangles(); i++)
		{
			int i0 = mesh.indices[i * 3] + 1;
			int i1 = mesh.indices[i * 3 + 1] + 1;
			int i2 = mesh.indices[i * 3 + 2] + 1;
			file << "f " << i0 << "//" << i0 << " " << i1 << "//" << i1 << " " << i2 << "//" << i2 << std::endl;
		}
		return true;
	}

}