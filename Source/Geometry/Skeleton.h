#ifndef SKELETON_H
#define SKELETON_H

#include <Math/Vector3.h>
#include <Math/Quaternion.h>
#include <Engine/Types.h>
#include <Geometry/Collision3D.h> // FIXME
#include <Math/Matrix4.h>

#include <vector>
#include <string>

namespace Geometry
{
	struct Mesh;

	class Skeleton
	{
	public:
		struct Bone
		{
			int id;
			std::string name;
			uint32 nameCrc;
			Vector3 pos, pos0; // local position
			Quaternion rot, rot0; // local orientation quaternion
			Vector3 scale;
			Bone* parent;
			std::vector<Bone*> children;
			Vector3 worldPos;
			Quaternion worldRot;
			Matrix3 worldMat;

			Matrix4 transform, transform0;
			Vector3 translate; // incremental translation of the bone due to animation (obsolete)
			Quaternion rotate; // incremental rotation of the bone due to animation (obsolete)

			Bone() : scale(1.f), parent(NULL) { }
		};

		struct Keyframe
		{
			float time;
			Vector3 offset;
			Quaternion rot;
		};

		struct Track
		{
			Bone* bone;
			std::vector<Keyframe> keyframes;
			size_t currFrame;
		};

		struct Animation
		{
			std::string name;
			float length;
			std::vector<Track> tracks;
		};

	public:
		Skeleton() : mRoot(NULL), mCurrAnim(-1) { }
		void AddBone(const Bone& bone) { mBones.push_back(bone); }
		Bone* GetBoneByName(const char* name); // const?
		Bone* GetBoneById(int id) { return &mBones[id]; } // FIXME
		size_t GetNumBones() const { return mBones.size(); }
		void FindRoot();
		Bone* GetRoot() { return mRoot; } // const?
		void SetRoot(Bone* bone) { mRoot = bone; }

		void ComputeWorldTransforms();
		void AddAnimation(const Animation& anim) { mAnimations.push_back(anim); }
		void SetCurrentAnimation(int i);
		void UpdateAnimation(float dt);
		void SkinMesh(const Geometry::Mesh& mesh, Geometry::Mesh& skin);

	private:
		std::vector<Bone> mBones;
		Bone* mRoot;
		std::vector<Animation> mAnimations;
		int mCurrAnim;
		float mTime;
	};

	void LoadSkeletonFromXml(const char* path, Skeleton& skel);

} // namespace Geometry

#endif