#ifndef GEOMETRY_MESH_H
#define GEOMETRY_MESH_H

#include "Collision3D.h"
#include <Engine/Types.h>
#include <Math/Matrix4.h>
#include <Math/Quaternion.h>

#include <vector>

namespace Geometry
{
	struct Mesh
	{
		struct Edge
		{
			uint32 i1, i2; // vertex indices
			int t1, t2; // triangles indices (winged edge)
			//Aabb3 box; // hack for AABB tree based self collision
			int count; // multiplicity
			int origIdx;
			Edge() : i1(0), i2(0), t1(-1), t2(-1), count(0) { }
			Edge(int a, int b) : i1(a), i2(b), t1(-1), t2(-1), count(1) { }
		};

		struct Triangle
		{
			uint32 e[3];
		};

		struct BoneWeight
		{
			int id;
			float weight;
			BoneWeight() : id(-1), weight(0) { }
		};
		struct Weights
		{
			enum { MAX_WEIGHTS = 10 };
			BoneWeight bw[MAX_WEIGHTS];
			// TODO: operator[]
		};

		std::vector<Vector3> vertices;
		std::vector<uint32> indices;
		std::vector<Vector3> normals;
		std::vector<Vector3> tangents;
		std::vector<Vector3> bitangents;
		std::vector<Vector2> uvs;
		std::vector<Vector3> colors; // TODO: Vector4
		std::vector<Edge> edges;
		std::vector<Weights> weights;
		std::vector<Vector3> velocities; // remove?
		std::vector<double> pressures; //TODO: to be removed; should be "real" not double
		std::vector<std::vector<int>> oneRings;
		std::vector<Triangle> triangles;

		size_t GetNumTriangles() const { return indices.size() / 3; }

		void Clear()
		{
			vertices.clear();
			indices.clear();
			normals.clear();
			tangents.clear();
			bitangents.clear();
			uvs.clear();
			colors.clear();
			edges.clear();
			weights.clear();
			velocities.clear();
			pressures.clear();
			oneRings.clear();
		}

		uint32 AddVertex(const Vector3& v, bool check = false)
		{
			if (check)
			{
				std::vector<Vector3>::iterator it = std::find_if(vertices.begin(), vertices.end(), [&v](const Vector3& t) { return std::abs(t.x - v.x) < 0.00001f && std::abs(t.y - v.y) < 0.00001f && std::abs(t.z - v.z) < 0.00001f; });
				if (it != vertices.end())
				{
					return uint32(std::distance(vertices.begin(), it));
				}
			}
			vertices.push_back(v);
			return (uint32)(vertices.size() - 1);
		}

		void AddTriangle(uint32 a, uint32 b, uint32 c, bool flip = false)
		{
			indices.push_back(a);
			if (!flip)
			{
				indices.push_back(b);
				indices.push_back(c);
			}
			else
			{
				indices.push_back(c);
				indices.push_back(b);
			}
		}

		void ComputeNormals(bool flip = false)
		{
			normals.resize(vertices.size());
			for (size_t i = 0; i < vertices.size(); i++)
				normals[i].SetZero();
			for (size_t i = 0; i < indices.size(); i +=3)
			{
				const int i1 = indices[i];
				const int i2 = indices[i + 1];
				const int i3 = indices[i + 2];
				const Vector3& v1 = vertices[i1];
				const Vector3& v2 = vertices[i2];
				const Vector3& v3 = vertices[i3];
				Vector3 n = (v2 - v1).Cross(v3 - v1);
	#ifdef ANGLE_WEIGHTED
				n.Normalize();
				Vector3 e1 = v2 - v1;
				Vector3 e2 = v3 - v2;
				Vector3 e3 = v1 - v3;
				e1.Normalize();
				e2.Normalize();
				e3.Normalize();
				float t1 = acosf(-(e1 * e3));
				float t2 = acosf(-(e1 * e2));
				float t3 = acosf(-(e2 * e3));
				normals[i1] += (t1 / PI) * n;
				normals[i2] += (t2 / PI) * n;
				normals[i3] += (t3 / PI) * n;
	#else
				normals[i1] += n;
				normals[i2] += n;
				normals[i3] += n;
	#endif
			}
			for (size_t i = 0; i < vertices.size(); i++)
			{
				normals[i].Normalize();
				if (flip)
					normals[i].Flip();
			}
		}

		void Transform(const Matrix4& mat, bool calcVel)
		{
			velocities.resize(vertices.size());
			for (size_t i = 0; i < vertices.size(); i++)
			{
				Vector3 v = mat.Transform(vertices[i]);
				if (calcVel)
					velocities[i] = v - vertices[i];
				vertices[i] = v;
			}
		}

		Vector3 GetCentroid() const
		{
			Vector3 c;
			for (size_t i = 0; i < vertices.size(); i++)
			{
				c += vertices[i];
			}
			c.Scale(1.f / vertices.size());
			return c;
		}

		void GetBoundingSphere(Vector3& c, float& r)
		{
			c = GetCentroid();
			r = 0;
			for (size_t i = 0; i < vertices.size(); i++)
			{
				float len = (vertices[i] - c).Length();
				r = max(r, len);
			}
		}
		
		void ConstructEdges();

		void ConstructOneRings();

		Aabb3 GetAabb() const
		{
			Vector3 v = vertices[0];
			Vector3 v1 = v;
			Vector3 v2 = v;
			for (size_t i = 1; i < vertices.size(); i++)
			{
				v = vertices[i];
				v1 = vmin(v1, v);
				v2 = vmax(v2, v);
			}
			return Aabb3(v1, v2);
		}

		Aabb3 GetAabb(const Quaternion& q, const Vector3& t) const
		{
			Vector3 v = qRotate(q, vertices[0]) + t;
			Vector3 v1 = v;
			Vector3 v2 = v;
			for (size_t i = 1; i < vertices.size(); i++)
			{
				v = qRotate(q, vertices[i]) + t;
				v1 = vmin(v1, v);
				v2 = vmax(v2, v);
			}
			return Aabb3(v1, v2);
		}

		Aabb3 GetAabb(const Matrix3& R, const Vector3& t) const
		{
			Vector3 v = R * vertices[0] + t;
			Vector3 v1 = v;
			Vector3 v2 = v;
			for (size_t i = 1; i < vertices.size(); i++)
			{
				v = R * vertices[i] + t;
				v1 = vmin(v1, v);
				v2 = vmax(v2, v);
			}
			return Aabb3(v1, v2);
		}

		void Extrude(float val)
		{
			if (normals.empty())
				ComputeNormals();
			for (size_t i = 0; i < vertices.size(); i++)
			{
				vertices[i] += val * normals[i]; 
			}
		}

		size_t RemoveDuplicatedVertices();

		void Transform(float scale, const Vector3& offset)
		{
			// first scale, then translate
			for (size_t i = 0; i < vertices.size(); i++)
				vertices[i] = scale * vertices[i] + offset;
		}
	};

	static inline bool CompareEdges(const Mesh::Edge& e1, const Mesh::Edge& e2)
	{
		return e1.i1 < e2.i1 || (e1.i1 == e2.i1 && e1.i2 < e2.i2);
	}

}
#endif // GEOMETRY_MESH_H