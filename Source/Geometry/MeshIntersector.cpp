#include "pch.h"
#include "MeshIntersector.h"
#include <stack>

namespace Geometry
{
	void MeshIntersector::SetTransforms(const Matrix4& mat1, const Matrix4& mat2)
	{
		mTransform1 = mat1;
		mTransform2 = mat2;
		mMesh1 = *mMeshSrc1;
		mMesh2 = *mMeshSrc2;
		mMesh1.Transform(mat1, false);
		mMesh2.Transform(mat2, false);
		mTree1 = ComputeMeshTree(mMesh1, ATF_TRIANGLES | ATF_EDGES, 10, 0.1f);
		mTree2 = ComputeMeshTree(mMesh2, ATF_TRIANGLES | ATF_EDGES, 10, 0.1f);
	}

	void MeshIntersector::IntersectionPoint::Classify(BarycentricCoords coords, float t, float epsilon)
	{
		type = FACE_PT;
		if (fabs(t) < epsilon || fabs(t - 1) < epsilon)
		{
			// TODO: identify vertex index on mesh1 (simple)
			type |= END_PT;
		}
		if (fabs(coords.u) < epsilon || fabs(coords.u) < epsilon || fabs(coords.u) < epsilon)
		{
			type |= EDGE_PT;
			// find the edge on the triangle
			int side = -1;
			if (fabs(coords.u) < epsilon)
				side = 1;
			if (fabs(coords.v) < epsilon)
				side = 2;
			if (fabs(coords.w) < epsilon)
				side = 0;
			edgeOnTri = meshTri->triangles[tri].e[side];
			// TODO: vertex-vertex intersection
		}

		if (type & EDGE_PT)
		{
			int i1 = edge;
			int i2 = edgeOnTri;
			if (i1 > i2)
				std::swap(i1, i2);
			key = MakeKey(i1, i2);
		}
		else
			key = MakeKey(edge, tri);
	}

	void MeshIntersector::ClearIntersection()
	{
		mVertexInfos1.clear();
		mVertexInfos2.clear();
		mIntersections.clear();
		mIntTriangles1.clear();
		mIntTriangles2.clear();
		mIntMesh1.Clear();
		mIntMesh2.Clear();

		mTriTriList.clear();
		mPolyLine.clear();
	}

	void MeshIntersector::AddTriTriPair(size_t i, int e, int tri, const Mesh* meshEdge)
	{
		const Mesh::Edge& edge = meshEdge->edges[e];

		TrianglePair triTri;
		triTri.ptIdx = (uint32)i;
		bool meshEdgeIs1 = meshEdge == &mMesh1;
		if (meshEdgeIs1)
			triTri.tri2 = tri;
		else
			triTri.tri1 = tri;
		if (edge.t1 >= 0)
		{
			if (meshEdgeIs1)
				triTri.tri1 = edge.t1;
			else
				triTri.tri2 = edge.t1;
			mTriTriList.push_back(triTri);
		}
		if (edge.t2 >= 0)
		{
			if (meshEdgeIs1)
				triTri.tri1 = edge.t2;
			else
				triTri.tri2 = edge.t2;
			mTriTriList.push_back(triTri);
		}
	}

	void MeshIntersector::IntersectFast()
	{
		ClearIntersection();

		mVertexInfos1.resize(mMesh1.vertices.size());
		mVertexInfos2.resize(mMesh2.vertices.size());

		// do edge-triangle intersections
		std::vector<CandidatePair> pairs;
		TestTrees(mMesh1, mMesh2, mTree1, mTree2, pairs);

		for (int k = 0; k < pairs.size(); k++)
		{
			const CandidatePair& pair = pairs[k];
			IntersectEdgeTriangle(*pair.meshEdge, *pair.meshTri, pair.edge, pair.tri,
				pair.meshTri == &mMesh1 ? mIntTriangles1 : mIntTriangles2,
				pair.meshEdge == &mMesh1 ? mVertexInfos1 : mVertexInfos2);
		}

		// find orphan edge-edge intersections, i.e. that were not correctly classified
		for (size_t i = 0; i < mIntersections.size(); i++)
		{
			const IntersectionPoint& intPt = mIntersections[i];
			if (intPt.type & EDGE_PT)
			{
				// look to see if there is an unmatched pair
				const Mesh::Edge& edge = intPt.meshEdge->edges[intPt.edge];
				// do a linear search until I figure this out
				for (int j = 0; j < mIntersections.size(); j++)
				{
					if (i == j)
						continue;
					if (mIntersections[j].edge == intPt.edgeOnTri &&
						(mIntersections[j].tri == edge.t1 || mIntersections[j].tri == edge.t2))
					{
						float len = (mIntersections[j].pt - intPt.pt).Length();
						mIntersections[j].key = intPt.key;
						mIntersections[j].type = intPt.type;
						mIntersections[j].edgeOnTri = intPt.edge;
					}
				}
			}
		}

		// remove duplicates
		std::sort(mIntersections.begin(), mIntersections.end());

		std::vector<IntersectionPoint> newIntersections;
		for (size_t i = 0; i < mIntersections.size(); i++)
		{
			if (i == 0 || mIntersections[i].key != mIntersections[i - 1].key)
			{
				newIntersections.push_back(mIntersections[i]);
			}
		}
		mIntersections = newIntersections;

		// parse the intersection points list for tri-tri pairs
		for (size_t i = 0; i < mIntersections.size(); i++)
		{
			const IntersectionPoint& intPt = mIntersections[i];

			// register the triangle pairs
			if (intPt.type & EDGE_PT)
			{
				const Mesh::Edge edgeOnTri = intPt.meshTri->edges[intPt.edgeOnTri];
				if (edgeOnTri.t1 >= 0)
					AddTriTriPair(i, intPt.edge, edgeOnTri.t1, intPt.meshEdge);
				if (edgeOnTri.t2 >= 0)
					AddTriTriPair(i, intPt.edge, edgeOnTri.t2, intPt.meshEdge);
			}
			else // FACE_PT
			{
				AddTriTriPair(i, intPt.edge, intPt.tri, intPt.meshEdge);
			}
		}

		// sort the tri-tri list by the triangle indices (lexicographic order)
		std::sort(mTriTriList.begin(), mTriTriList.end(), [](const TrianglePair& a, const TrianglePair& b) {
			if (a.tri1 == b.tri1)
				return a.tri2 < b.tri2;
			return a.tri1 < b.tri1; 
		});

		// convert tri-tri list to poly-line
		bool firstOfPair = true;
		for (size_t i = 0; i < mTriTriList.size(); i++)
		{
			if (firstOfPair)
			{
				if (i < mTriTriList.size() - 1 && mTriTriList[i + 1].tri1 == mTriTriList[i].tri1 && mTriTriList[i + 1].tri2 == mTriTriList[i].tri2)
				{
					mPolyLine.push_back(mIntersections[mTriTriList[i].ptIdx].pt);
					mPolyLine.push_back(mIntersections[mTriTriList[i + 1].ptIdx].pt);
					firstOfPair = false;
				}
			}
			else
			{
				firstOfPair = true;
			}
		}

		// flood fill interior
		int startInterior1 = -1;
		for (int i = 0; i < mVertexInfos1.size(); i++)
		{
			if (mVertexInfos1[i].side == SideType::INTERIOR)
			{
				startInterior1 = i;
				break;
			}
		}
		if (startInterior1 >= 0 && mVertexInfos1[startInterior1].side == SideType::INTERIOR)
			FloodFill(startInterior1, mMesh1, mVertexInfos1);

		int startInterior2 = -1;
		for (int i = 0; i < mVertexInfos2.size(); i++)
		{
			if (mVertexInfos2[i].side == SideType::INTERIOR)
			{
				startInterior2 = i;
				break;
			}
		}
		if (startInterior2 >= 0 && mVertexInfos2[startInterior2].side == SideType::INTERIOR)
			FloodFill(startInterior2, mMesh2, mVertexInfos2);

		ExtractInteriorMesh(mMesh1, mVertexInfos1, mIntMesh1);
		ExtractInteriorMesh(mMesh2, mVertexInfos2, mIntMesh2);

		mSelection1.clear();
		for (int i = 0; i < mVertexInfos1.size(); i++)
		{
			if (mVertexInfos1[i].side != SideType::INVALID)
			{
				mSelection1.push_back(i);
			}
		}

		mSelection2.clear();
		for (int i = 0; i < mVertexInfos2.size(); i++)
		{
			if (mVertexInfos2[i].side != SideType::INVALID)
			{
				mSelection2.push_back(i);
			}
		}
	}

	void MeshIntersector::TestTrees(const Mesh& mesh1, const Mesh& mesh2, AabbTree* node1, AabbTree* node2, std::vector<CandidatePair>& pairs)
	{
		if (node1 == NULL || node2 == NULL)
			return;

		Aabb3 box1 = node1->box;
		Aabb3 box2 = node2->box;
		if (!AabbOverlap3D(box1, box2))
			return;

		if (node1->triangles.empty() && node1->edges.empty())
		{
			if (node1->left)
				TestTrees(mesh1, mesh2, node1->left, node2, pairs);
			if (node1->right)
				TestTrees(mesh1, mesh2, node1->right, node2, pairs);
		}
		else if (node2->triangles.empty() && node2->edges.empty())
		{
			if (node2->left)
				TestTrees(mesh1, mesh2, node1, node2->left, pairs);
			if (node2->right)
				TestTrees(mesh1, mesh2, node1, node2->right, pairs);
		}
		else
		{
			for (int i = 0; i < node1->triangles.size(); i++)
			{
				for (int j = 0; j < node2->edges.size(); j++)
				{
					uint32 tri1 = node1->triangles[i].idx;
					uint32 edge2 = node2->edges[j].idx;
					pairs.push_back({ &mesh1, &mesh2, tri1, edge2 });
				}
			}

			for (int i = 0; i < node2->triangles.size(); i++)
			{
				for (int j = 0; j < node1->edges.size(); j++)
				{
					uint32 tri2 = node2->triangles[i].idx;
					uint32 edge1 = node1->edges[j].idx;
					pairs.push_back({ &mesh2, &mesh1, tri2, edge1 });
				}
			}
		}
	}

	bool MeshIntersector::IntersectEdgeTriangle(const Mesh& meshEdge, const Mesh& meshTri, uint32 e, uint32 tri, std::vector<int>& intTris, std::vector<ProxyInfo>& vertexInfos)
	{
		const Mesh::Edge& edge = meshEdge.edges[e];
		const Vector3& p = meshEdge.vertices[edge.i1];
		const Vector3& q = meshEdge.vertices[edge.i2];

		Aabb3 boxEdge;
		boxEdge.Add(p);
		boxEdge.Add(q);
		boxEdge.Extrude(0.1f);

		uint32 i0 = meshTri.indices[tri * 3];
		uint32 i1 = meshTri.indices[tri * 3 + 1];
		uint32 i2 = meshTri.indices[tri * 3 + 2];

		const Vector3& a = meshTri.vertices[i0];
		const Vector3& b = meshTri.vertices[i1];
		const Vector3& c = meshTri.vertices[i2];

		Aabb3 boxTri;
		boxTri.Add(a);
		boxTri.Add(b);
		boxTri.Add(c);
		boxTri.Extrude(0.1f);

		if (!AabbOverlap3D(boxEdge, boxTri))
			return false;

		// compute triangle normal
		Vector3 ab = b - a;
		Vector3 ac = c - a;
		Vector3 n = ab.Cross(ac);
		n.Normalize();
		// TODO: use interpolated normal instead

		BarycentricCoords coords;
		float t;
		Vector3 pt;
		if (IntersectSegmentTriangle(p, q, a, b, c, coords, t, pt))
		{
			intTris.push_back(tri);
			IntersectionPoint intPt = { pt, (uint32)e, (uint32)tri, &meshEdge, &meshTri };
			intPt.Classify(coords, t);
			uint32 id = (uint32)mIntersections.size();
			mIntersections.push_back(intPt);

			// check the sides of each endpoint
			{
				ProxyInfo& info = vertexInfos[edge.i1];
				float dist = (p - pt).Length();
				if (dist < info.distanceToIntersection)
				{
					info.distanceToIntersection = dist;
					info.intersectionTri = tri;
					info.intersectionPt = pt;
					if (n.Dot(p - pt) > 0)
					{
						info.side = SideType::EXTERIOR;
					}
					else
					{
						info.side = SideType::INTERIOR;
					}
				}
			}

			{
				ProxyInfo& info = vertexInfos[edge.i2];
				float dist = (q - pt).Length();
				if (dist < info.distanceToIntersection)
				{
					info.distanceToIntersection = dist;
					info.intersectionTri = tri;
					info.intersectionPt = pt;
					if (n.Dot(q - pt) > 0)
					{
						info.side = SideType::EXTERIOR;
					}
					else
					{
						info.side = SideType::INTERIOR;
					}
				}
			}

			return true;
		}

		return false;
	}

	void MeshIntersector::FloodFill(int start, const Mesh& mesh, std::vector<ProxyInfo>& vertexInfos1)
	{
		std::stack<int> stackInt;
		stackInt.push(start);
		while (!stackInt.empty())
		{
			int vi = stackInt.top();
			stackInt.pop();
			for (int i = 0; i < mesh.oneRings[vi].size(); i++)
			{
				int vj = mesh.oneRings[vi][i];
				if (vertexInfos1[vj].side != SideType::EXTERIOR && !vertexInfos1[vj].visited)
				{
					vertexInfos1[vj].side = SideType::INTERIOR;
					vertexInfos1[vj].visited = true;
					stackInt.push(vj);
				}
			}
		}
	}

	void MeshIntersector::ExtractInteriorMesh(const Mesh& meshIn, const std::vector<ProxyInfo>& vertexInfos, Mesh& meshOut)
	{
		meshOut.Clear();

		// extract interior mesh
		std::vector<int> map(meshIn.vertices.size(), -1);
		for (int i = 0; i < vertexInfos.size(); i++)
		{
			if (vertexInfos[i].side != SideType::INVALID)
			{
				map[i] = (int)meshOut.vertices.size();
				meshOut.vertices.push_back(meshIn.vertices[i]);
			}
		}

		for (int i = 0; i < meshIn.GetNumTriangles(); i++)
		{
			uint32 i0 = meshIn.indices[i * 3];
			uint32 i1 = meshIn.indices[i * 3 + 1];
			uint32 i2 = meshIn.indices[i * 3 + 2];
			if (map[i0] >= 0 && map[i1] >= 0 && map[i2] >= 0)
			{
				meshOut.AddTriangle(map[i0], map[i1], map[i2], false);
			}
		}

		meshOut.ComputeNormals();
	}

	void MeshIntersector::IntersectSlow()
	{
		ClearIntersection();
	
		mVertexInfos1.resize(mMesh1.vertices.size());
		mVertexInfos2.resize(mMesh2.vertices.size());
	
		// do edge-triangle intersections
		IntersectEdgesAndFaces(mMesh1, mVertexInfos1, mMesh2, mIntTriangles2, mIntMesh1);
		IntersectEdgesAndFaces(mMesh2, mVertexInfos2, mMesh1, mIntTriangles1, mIntMesh2);
	}

	// brute force intersection code
	void MeshIntersector::IntersectEdgesAndFaces(const Mesh& mesh1, std::vector<ProxyInfo>& vertexInfos1, const Mesh& mesh2, std::vector<int>& intTris2, Mesh& meshOut)
	{
		int startInterior = -1;
		int startExterior = -1;
		meshOut.Clear();
		intTris2.clear();
	
		// find all edge-triangle intersections
		for (int i = 0; i < mesh1.edges.size(); i++)
		{
			for (int j = 0; j < mesh2.GetNumTriangles(); j++)
			{
				const Mesh::Edge& edge = mesh1.edges[i];
				IntersectEdgeTriangle(mesh1, mesh2, i, j, intTris2, vertexInfos1);
			}
		}
	
		int startInterior1 = -1;
		for (int i = 0; i < vertexInfos1.size(); i++)
		{
			if (vertexInfos1[i].side == SideType::INTERIOR)
			{
				startInterior1 = i;
				break;
			}
		}

		// flood fill interior
		FloodFill(startInterior1, mesh1, vertexInfos1);
	
		// extract interior mesh
		std::vector<int> map(mesh1.vertices.size(), -1);
		for (int i = 0; i < vertexInfos1.size(); i++)
		{
			if (vertexInfos1[i].side != SideType::INVALID)
			{
				map[i] = (int)meshOut.vertices.size();
				meshOut.vertices.push_back(mesh1.vertices[i]);
			}
		}
	
		for (int i = 0; i < mesh1.GetNumTriangles(); i++)
		{
			uint32 i0 = mesh1.indices[i * 3];
			uint32 i1 = mesh1.indices[i * 3 + 1];
			uint32 i2 = mesh1.indices[i * 3 + 2];
			if (map[i0] >= 0 && map[i1] >= 0 && map[i2] >= 0)
			{
				meshOut.AddTriangle(map[i0], map[i1], map[i2], true);
			}
		}
	
		meshOut.ComputeNormals();
	}
	
	//void MeshIntersector::IntersectAndFollowContour()
	//{
	//	// find a first intersection point
	//	// check all the edges of mesh 1 against triangles of mesh 2
	//	mIntersections.clear();
	//	mIntHash.clear();
	//	for (int i = 0; i < mesh1.edges.size(); i++)
	//	{
	//		const Mesh::Edge& edge = mesh1.edges[i];
	//		const Vector3& p = mesh1.vertices[edge.i1];
	//		const Vector3& q = mesh1.vertices[edge.i2];
	//		for (int j = 0; j < mesh2.GetNumTriangles(); j++)
	//		{
	//			const Vector3& a = mesh2.vertices[mesh2.indices[j * 3]];
	//			const Vector3& b = mesh2.vertices[mesh2.indices[j * 3 + 1]];
	//			const Vector3& c = mesh2.vertices[mesh2.indices[j * 3 + 2]];
	//
	//			BarycentricCoords coords;
	//			float t;
	//			if (IntersectSegmentTriangle(p, q, a, b, c, coords, t))
	//			{
	//				Vector3 pt = (1.0f - t) * p + t * q;
	//				IntersectionPoint intPt = { pt, (uint32)i, (uint32)j, &mesh1, &mesh2 };
	//				intPt.Classify(coords, t);
	//				mIntersections.push_back(intPt);
	//				mIntHash.insert({ MakeKey(i, j), intPt });
	//				break;
	//			}
	//		}
	//		if (mIntersections.size() > 0)
	//			break;
	//	}
	//
	//	// we have the first intersection point, now check its neighbors
	//	std::stack<uint32> stack;
	//	stack.push(0);
	//	while (!stack.empty())
	//	{
	//		uint32 index = stack.top();
	//		stack.pop();
	//		const IntersectionPoint& intPt0 = mIntersections[index];
	//		uint32 e = mIntersections[index].edge;
	//		uint32 tri = mIntersections[index].tri;
	//		FollowIntersectionPoint(e, tri, index, stack);
	//	}
	//}
	
	//void CollisionDemo::FollowIntersectionPoint(uint32 e, uint32 tri, uint32 index, std::stack<uint32>& stack)
	//{
	//	const IntersectionPoint& intPt0 = mIntersections[index];
	//	const Mesh& mesh1 = *intPt0.mesh1;
	//	const Mesh& mesh2 = *intPt0.mesh2;
	//	const Vector3& a = mesh2.vertices[mesh2.indices[tri * 3]];
	//	const Vector3& b = mesh2.vertices[mesh2.indices[tri * 3 + 1]];
	//	const Vector3& c = mesh2.vertices[mesh2.indices[tri * 3 + 2]];
	//
	//	bool found = false;
	//	for (int j = 0; j < 2 && !found; j++)
	//	{
	//		uint32 ti = (j == 0) ? mesh1.edges[e].t1 : mesh1.edges[e].t2;
	//
	//		// there are 2 cases: a. an edge of Ti intersects T or b. an edge of T intersects Ti
	//		// first, go through the edges of Ti besides E
	//		for (int i = 0; i < 3 && !found; i++)
	//		{
	//			uint32 ei = mesh1.triangles[ti].e[i];
	//			if (ei == e)
	//				continue;
	//
	//			// check if the pair already exists
	//			uint64 key = MakeKey(ei, tri);
	//			if (mIntHash.find(key) != mIntHash.end())
	//				continue;
	//
	//			const Mesh::Edge& edge = mesh1.edges[ei];
	//			const Vector3& p = mesh1.vertices[edge.i1];
	//			const Vector3& q = mesh1.vertices[edge.i2];
	//
	//			BarycentricCoords coords;
	//			float t;
	//			if (IntersectSegmentTriangle(p, q, a, b, c, coords, t))
	//			{
	//				Vector3 pt = (1.0f - t) * p + t * q;
	//				IntersectionPoint intPt = { pt, ei, tri, &mesh1, &mesh2 };
	//				intPt.Classify(coords, t);
	//				mIntersections.push_back(intPt);
	//				mIntHash.insert({ key, intPt });
	//				uint32 newIndex = (uint32)(mIntersections.size() - 1);
	//				mIntLines.push_back({ index, newIndex });
	//				stack.push(newIndex);
	//			}
	//		}
	//
	//		const Vector3& ai = mesh1.vertices[mesh1.indices[ti * 3]];
	//		const Vector3& bi = mesh1.vertices[mesh1.indices[ti * 3 + 1]];
	//		const Vector3& ci = mesh1.vertices[mesh1.indices[ti * 3 + 2]];
	//
	//		// if no intersection is found, then go through all the edges of T and check them against Ti
	//		for (int i = 0; i < 3 && !found; i++)
	//		{
	//			uint32 ei = mesh2.triangles[tri].e[i];
	//
	//			const Mesh::Edge& edge = mesh2.edges[ei];
	//			const Vector3& p = mesh2.vertices[edge.i1];
	//			const Vector3& q = mesh2.vertices[edge.i2];
	//
	//			// check if the pair already exists
	//			uint64 key = MakeKey(ei, ti);
	//			if (mIntHash.find(key) != mIntHash.end())
	//				continue;
	//
	//			BarycentricCoords coords;
	//			float t;
	//			if (IntersectSegmentTriangle(p, q, ai, bi, ci, coords, t))
	//			{
	//				Vector3 pt = (1.0f - t) * p + t * q;
	//				IntersectionPoint intPt = { pt, ei, ti, &mesh2, &mesh1 };
	//				intPt.Classify(coords, t);
	//				mIntersections.push_back(intPt);
	//				mIntHash.insert({ key, intPt });
	//				uint32 newIndex = (uint32)(mIntersections.size() - 1);
	//				mIntLines.push_back({ index, newIndex });
	//				stack.push(newIndex);
	//			}
	//		}
	//	}
	//}


}