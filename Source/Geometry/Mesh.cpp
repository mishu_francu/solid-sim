#include "pch.h"
#include "Mesh.h"
#include "Engine/Utils.h"

namespace Geometry
{
	void Mesh::ConstructEdges()
	{
		if (indices.empty())
			return;
		// build unique edges array
		std::vector<Mesh::Edge> duplicates(indices.size());
		for (size_t i = 0; i < indices.size(); i++)
		{
			// construct edge
			int i1 = indices[i];
			int i2 = indices[i + 1];
			if ((i + 1) % 3 == 0)
			{
				i2 = indices[i - 2];
				Triangle tri;
				tri.e[2] = (uint32)i;
				tri.e[1] = (uint32)(i - 1);
				tri.e[0] = (uint32)(i - 2);
				triangles.push_back(tri);
			}
			if (i1 > i2)
				std::swap(i1, i2);
			Mesh::Edge edge(i1, i2);
			edge.t1 = (int)(i / 3);
			edge.origIdx = (int)i;
			duplicates[i] = edge;
		}
		// sort it so we can find duplicates
		std::sort(duplicates.begin(), duplicates.end(), CompareEdges);

		// add only unique edges
		Mesh::Edge currEdge = duplicates[0];
		std::vector<int> map(duplicates.size(), -1);
		map[currEdge.origIdx] = 0;
		for (size_t i = 1; i < duplicates.size(); i++)
		{
			if (duplicates[i].i1 == duplicates[i - 1].i1 && duplicates[i].i2 == duplicates[i - 1].i2)
			{
				map[duplicates[i].origIdx] = (int)edges.size();
				currEdge.t2 = duplicates[i].t1;
				continue;
			}
			edges.push_back(currEdge);
			currEdge = duplicates[i];
			map[currEdge.origIdx] = (int)edges.size();
		}
		edges.push_back(currEdge);

		// remap triangle edges
		for (size_t i = 0; i < triangles.size(); i++)
		{
			for (int j = 0; j < 3; j++)
				triangles[i].e[j] = map[triangles[i].e[j]];
		}
	}

	void Mesh::ConstructOneRings()
	{
		if (edges.size() != 0)
		{
			oneRings.resize(vertices.size());
			for (size_t i = 0; i < edges.size(); i++)
			{
				int i1 = edges[i].i1;
				int i2 = edges[i].i2;
				oneRings[i1].push_back(i2);
				oneRings[i2].push_back(i1);
			}
		}
	}

	size_t Mesh::RemoveDuplicatedVertices()
	{
		std::vector<int> map(vertices.size(), -1);
		std::vector<Vector3> newVertices;
		// brute force
		for (size_t i = 0; i < vertices.size(); i++)
		{
			if (map[i] >= 0)
				continue;

			for (size_t j = i + 1; j < vertices.size(); j++)
			{
				Vector3 delta = vertices[i] - vertices[j];
				if (delta.Length() < 1e-10f)
				{
					map[j] = (int)newVertices.size();
				}
			}

			map[i] = (int)newVertices.size();
			newVertices.push_back(vertices[i]);
		}

		// replace vertices
		size_t oldCount = vertices.size();
		vertices = newVertices;

		// remap indices
		for (size_t i = 0; i < indices.size(); i++)
		{
			ASSERT(map[indices[i]] >= 0);
			indices[i] = map[indices[i]];
		}

		return oldCount - newVertices.size();
	}
}