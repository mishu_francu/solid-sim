#include "pch.h"
#include "Mesh.h"

namespace Geometry
{
	void SmoothVertices(Mesh& mesh, const std::vector<int>* selection, float lambda, bool smoothNormals)
	{
		if (mesh.oneRings.size() != mesh.vertices.size())
			mesh.ConstructOneRings();
		std::vector<Vector3> avgPos(mesh.vertices.size());
		std::vector<Vector3>& vectors = smoothNormals ? mesh.normals : mesh.vertices;
		// TODO: only for selection
		for (int i = 0; i < avgPos.size(); i++)
		{
			Vector3 avg;
			size_t n = mesh.oneRings[i].size();
			for (int j = 0; j < n; j++)
			{
				int idx = mesh.oneRings[i][j];
				avg += vectors[idx];
			}
			if (n > 0)
				avg *= 1.0f / n;
			avgPos[i] = avg;
		}
		if (selection != nullptr && selection->size() > 0)
		{
			for (int k = 0; k < selection->size(); k++)
			{
				int i = selection->at(k);
				vectors[i] += lambda * (avgPos[i] - vectors[i]);
			}
		}
		else
		{
			for (int i = 0; i < avgPos.size(); i++)
				vectors[i] += lambda * (avgPos[i] - vectors[i]);
		}
	}

	void ProjectVerticesOnMesh(Mesh& meshToDeform, const Matrix4& transform, const std::vector<int>& selection, const Mesh& meshTarget, std::vector<Vector3>& projections, float step)
	{
		projections.resize(selection.size(), Vector3(0, 100, 0));
		// for each interior vertex on mesh 1, raycast onto mesh 2
		for (size_t k = 0; k < selection.size(); k++)
		{
			int i = selection[k];
			{
				// build a ray
				Vector3 p = meshToDeform.vertices[i];
				p = transform.Transform(p);
				Vector3 dir = -meshToDeform.normals[i]; // unit length direction
				dir = transform.TransformRay(dir);
				Vector3 q = p + dir;

				// prepare output
				Vector3 pt;
				BarycentricCoords coords;
				coords.u = -1;
				coords.v = -1;
				coords.w = -1;
				float t;

				// go through all potential triangles
				for (int j = 0; j < meshTarget.GetNumTriangles(); j++)
				{
					uint32 i0 = meshTarget.indices[j * 3];
					uint32 i1 = meshTarget.indices[j * 3 + 1];
					uint32 i2 = meshTarget.indices[j * 3 + 2];
					const Vector3& a = meshTarget.vertices[i0];
					const Vector3& b = meshTarget.vertices[i1];
					const Vector3& c = meshTarget.vertices[i2];

					if (IntersectSegmentTriangle(p, q, a, b, c, coords, t, pt, true))
					{
						// store the point
						// we should be able to stop here (we presume only one intersection for now)
						break;
					}
				}

				const float epsilon = 1e-4f;
				if (coords.u >= -epsilon && coords.v >= -epsilon && coords.w >= -epsilon)
				{
					Matrix4 T = transform.GetInverseTransform();
					projections[k] = pt;
					pt = T.Transform(pt);
					// do the actual projection
					Vector3 delta = pt - meshToDeform.vertices[i];
					meshToDeform.vertices[i] += step * delta;
				}
			}
		}
	}
}
