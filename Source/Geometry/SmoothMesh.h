#pragma once

#include <vector>

struct Matrix4;

namespace Geometry
{
	struct Mesh;

	void SmoothVertices(Mesh& mesh, const std::vector<int>* selection = nullptr, float lambda = 0.5f, bool smoothNormals = false);

	void ProjectVerticesOnMesh(Mesh& meshToDeform, const Matrix4& transform, const std::vector<int>& selection, const Mesh& meshTarget, std::vector<Vector3>& projections, float step = 1.0f);
}