#ifndef COLLISION3D_H
#define COLLISION3D_H

#include <Math/Vector3.h>
#include <Math/Vector2.h>
#include <Engine/Types.h>
#include <vector>
#include <Math/Utils.h>

using Math::Vector3;

#define ANGLE_WEIGHTED

namespace Geometry
{
	// TODO: templated
	struct Aabb3
	{
		Vector3 min, max;
		Aabb3() : min(1e7f), max(-1e7f) { } // init to an invalid box that can be grown through min/max operations
		Aabb3(const Vector3& a, const Vector3& b) : min(a), max(b) { }
		Vector3 GetExtent() const { return max - min; }
		Vector3 GetHalfExtent() const { return 0.5f * (max - min); }
		Vector3 GetCenter() const { return 0.5f * (max + min); }
		void Add(const Aabb3& box)
		{
			min = vmin(min, box.min);
			max = vmax(max, box.max);
		}
		void Add(const Vector3& v)
		{
			min = vmin(min, v);
			max = vmax(max, v);
		}
		void Extrude(float e)
		{
			Vector3 ext(e);
			min -= ext;
			max += ext;
		}
	};

	inline bool AabbOverlap3D(const Aabb3& bounds1, const Aabb3& bounds2)
	{
		if (bounds1.max.X() < bounds2.min.X())
			return false;
		if (bounds1.max.Y() < bounds2.min.Y())
			return false;
		if (bounds1.max.Z() < bounds2.min.Z())
			return false;
		if (bounds2.max.X() < bounds1.min.X())
			return false;
		if (bounds2.max.Y() < bounds1.min.Y())
			return false;
		if (bounds2.max.Z() < bounds1.min.Z())
			return false;
		return true;
	}

	// TODO: templated version
	inline bool PointInAabb3D(const Vector3& min, const Vector3& max, const Vector3& point)
	{
		return !(point.X() < min.X() || point.X() > max.X()
			|| point.Y() < min.Y() || point.Y() > max.Y()
			|| point.Z() < min.Z() || point.Z() > max.Z());
	}

	inline bool TestSegmentAABB(const Vector3& p0, const Vector3& p1, const Aabb3& b)
	{
		Vector3 c = (b.min + b.max) * 0.5f;
		Vector3 e = b.max - c;
		Vector3 m = (p0 + p1) * 0.5f;
		Vector3 d = p1 - m;
		m = m - c;
		float adx = fabsf(d.X());
		if (fabs(m.X()) > e.X() + adx) return false;
		float ady = fabsf(d.Y());
		if (fabs(m.Y()) > e.Y() + ady) return false;
		float adz = fabsf(d.Z());
		if (fabs(m.Z()) > e.Z() + adz) return false;

		const float eps = 1e-6f;
		adx += eps;
		ady += eps;
		adz += eps;
		if (fabs(m.Y() * d.Z() - m.Z() * d.Y()) > e.Y() * adz + e.Z() * ady) return false;
		if (fabs(m.Z() * d.X() - m.X() * d.Z()) > e.X() * adz + e.Z() * adx) return false;
		if (fabs(m.X() * d.Y() - m.Y() * d.X()) > e.X() * ady + e.Y() * adx) return false;

		return true;
	}

	// TODO: use vector3 instead
	struct BarycentricCoords
	{
		float u, v, w;
	};

	// I think this does not do projection, so it requires that p is in the (a, b, c) plane
	inline BarycentricCoords Barycentric(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& p)
	{
		Vector3 v0 = b - a;
		Vector3 v1 = c - a;
		Vector3 v2 = p - a;
		float d00 = v0.LengthSquared();
		float d01 = v0.Dot(v1);
		float d11 = v1.LengthSquared();
		float d20 = v2.Dot(v0);
		float d21 = v2.Dot(v1);
		float denom = d00 * d11 - d01 * d01;
		BarycentricCoords ret;
		ret.v = (d11 * d20 - d01 * d21) / denom;
		ret.w = (d00 * d21 - d01 * d20) / denom;
		ret.u = 1.f - ret.v - ret.w;
		return ret;
	}

	inline bool IntersectSphereSphere(const Vector3& p1, float r1, const Vector3& p2, float r2, Vector3& normal, Vector3& pos, float& dSqr)
	{
		const float dist = r1 + r2;
		const float radSqr = dist * dist;
		Vector3 delta = p2 - p1;
		dSqr = delta.LengthSquared();
		if (dSqr >= radSqr)
			return false;
		delta.Normalize();
		normal = delta; // pointing from 1 to 2
		pos = p1 + delta * r1;
		return true;
	}

	// from Ericson
	inline bool IntersectSegmentPlane(const Vector3& a, const Vector3& b, // the segment
		const Vector3& n, float d, // the plane
		float& t, Vector3& q)
	{
		Vector3 ab = b - a;
		t = (d - n.Dot(a)) / n.Dot(ab);
		if (t >= 0.f && t < 1.f)
		{
			q = a + t * ab;
			return true;
		}
		return false;
	}

	inline bool IntersectRayPlane(const Vector3& a, const Vector3& ab, // the segment
		const Vector3& n, float d, // the plane
		float& t, Vector3& q)
	{
		t = (d - n.Dot(a)) / n.Dot(ab);
		if (t >= 0.f)
		{
			q = a + t * ab;
			return true;
		}
		return false;
	}

	inline bool IntersectRaySphere(const Vector3& p, const Vector3& d, // the ray
		const Vector3& center, float radius, // the sphere,
		float& t, Vector3& q)
	{
		Vector3 m = p - center;
		float b = m.Dot(d);
		float c = m.LengthSquared() - radius * radius;
		if (c > 0.f && b > 0.f) return false;
		float discr = b * b - c;
		if (discr < 0.f) return false;
		t = -b - (float)sqrt(discr);
		// if t is negative, ray started inside sphere so clamp it to zero
		if (t < 0.f)
			t = 0.f;
		q = p + t * d;
		return true;
	}

	inline Vector3 ClosestPtSegm(const Vector3& p, const Vector3& a, const Vector3& b, float& t)
	{
		Vector3 u = b - a;
		Vector3 v = p - a;
		t = u.Dot(v) / u.LengthSquared();
		if (t <= 0.f)
		{
			t = 0.f;
			return a;
		}
		if (t >= 1.f)
		{
			t = 1.f;
			return b;
		}
		return a + t * u;
	}


	// closest points between segments (p1, q1) and (p2, q2)
	// returns squared distance
	float ClosestPtSegmSegm(const Vector3& p1, const Vector3& q1, const Vector3& p2, const Vector3& q2, float& s, float& t, Vector3& c1, Vector3& c2);

	// closest point on triangle (a, b, c) to vertex p
	// outputs the closest point and its barycentric coordinates in bar
	Vector3 ClosestPtPointTriangle(const Vector3& p, const Vector3& a, const Vector3& b, const Vector3& c, BarycentricCoords& bar);

	bool IntersectSphereTriangle(const Vector3& v, float radTol, const Vector3& v1, const Vector3& v2, const Vector3& v3,
		Vector3& normal, Vector3& pos, float& dist, BarycentricCoords& coords);

	bool IntersectSphereTriangle1(const Vector3& v, float radTol, const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& ref,
		Vector3& normal, Vector3& pos, float& dist, BarycentricCoords& coords);

	bool IntersectSegmentTriangle(const Vector3& p, const Vector3& q, // the segment
		const Vector3& a, const Vector3& b, const Vector3& c, // the triangle
		BarycentricCoords& coords, float& t, Vector3& pt, bool isRay = false);

	bool IntersectSweptSphereTriangle(const Vector3& c, float radius, const Vector3& v, // the swept sphere
		const Vector3& v1, const Vector3& v2, const Vector3& v3, // the triangle
		Vector3& pos, Vector3& normal, float& dist, BarycentricCoords& coords);

} // namespace Geometry

#endif // COLLISION3D_H