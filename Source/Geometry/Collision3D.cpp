#include "pch.h"
#include "Collision3D.h"


// from Ericson

namespace Geometry
{
	// closest points between segments (p1, q1) and (p2, q2)
	// returns squared distance
	float ClosestPtSegmSegm(const Vector3& p1, const Vector3& q1, const Vector3& p2, const Vector3& q2,
		float& s, float& t, Vector3& c1, Vector3& c2)
	{
		Vector3 d1 = q1 - p1;
		Vector3 d2 = q2 - p2;
		Vector3 r = p1 - p2;
		float a = d1.LengthSquared();
		float e = d2.LengthSquared();
		float f = d2.Dot(r);

		// TODO: check if degenerate

		float c = d1.Dot(r);
		float b = d1.Dot(d2);
		float denom = a * e - b * b;

		if (denom != 0.f)
			s = Math::clamp((b * f - c * e) / denom, 0.f, 1.f);
		else
			s = 0.f;

		t = (b * s + f) / e;

		if (t < 0.f)
		{
			t = 0.f;
			s = Math::clamp(-c / a, 0.f, 1.f);
		}
		else if (t > 1.f)
		{
			t = 1.f;
			s = Math::clamp((b - c) / a, 0.f, 1.f);
		}

		// epilogue
		c1 = p1 + s * d1;
		c2 = p2 + t * d2;
		return (c1 - c2).LengthSquared();
	}

	// closest point on triangle (a, b, c) to vertex p
	// outputs the closest point and its barycentric coordinates in bar
	inline Vector3 ClosestPtPointTriangle(const Vector3& p, const Vector3& a, const Vector3& b, const Vector3& c, BarycentricCoords& bar)
	{
		bar.u = bar.v = bar.w = 0.f;

		// Check if P in vertex region outside A
		Vector3 ab = b - a;
		Vector3 ac = c - a;
		Vector3 ap = p - a;
		float d1 = ab.Dot(ap);
		float d2 = ac.Dot(ap);
		if (d1 <= 0.f && d2 <= 0.f)
		{
			bar.u = 1.f;
			return a;
		}

		// Check if P in vertex region outside B
		Vector3 bp = p - b;
		float d3 = ab.Dot(bp);
		float d4 = ac.Dot(bp);
		if (d3 >= 0.f && d4 <= d3)
		{
			bar.v = 1.f;
			return b;
		}

		// Check if P in edge region of AB, if so return projection
		float vc = d1 * d4 - d3 * d2;
		if (vc <= 0.f && d1 >= 0.f && d3 <= 0.f)
		{
			bar.v = d1 / (d1 - d3);
			bar.u = 1 - bar.v;
			return a + bar.v * ab;
		}

		// Check if P in vertex region outside C
		Vector3 cp = p - c;
		float d5 = ab.Dot(cp);
		float d6 = ac.Dot(cp);
		if (d6 >= 0.f && d5 <= d6)
		{
			bar.w = 1.f;
			return c;
		}

		// Check if P in edge region of AC
		float vb = d5 * d2 - d1 * d6;
		if (vb <= 0.f && d2 >= 0.f && d6 <= 0.f)
		{
			bar.w = d2 / (d2 - d6);
			bar.u = 1 - bar.w;
			return a + bar.w * ac;
		}

		// Check if P in edge region of BC
		float va = d3 * d6 - d5 * d4;
		float d43 = d4 - d3;
		float d56 = d5 - d6;
		if (va <= 0.f && d43 >= 0.f && d56 >= 0.f)
		{
			bar.w = d43 / (d43 + d56);
			bar.v = 1 - bar.w;
			return b + bar.w * (c - b);
		}

		// P inside face region
		float denom = 1.f / (va + vb + vc);
		bar.v = vb * denom;
		bar.w = vc * denom;
		bar.u = 1.f - bar.v - bar.w;
		return a + ab * bar.v + ac * bar.w;
	}

	bool IntersectSphereTriangle(const Vector3& v, float radTol, const Vector3& v1, const Vector3& v2, const Vector3& v3,
		Vector3& normal, Vector3& pos, float& dist, BarycentricCoords& coords)
	{
		Vector3 p = ClosestPtPointTriangle(v, v1, v2, v3, coords);
		Vector3 delta = v - p;
		float dSqr = delta.LengthSquared();
		dist = dSqr;
		if (dSqr > radTol * radTol)
			return false;
		if (dSqr == 0)
			return false; // TODO: choose a normal
		pos = p;
		delta.Normalize();
		normal = delta;
		return true;
	}

	bool IntersectSphereTriangle1(const Vector3& v, float radTol, const Vector3& v1, const Vector3& v2, const Vector3& v3, const Vector3& ref,
		Vector3& normal, Vector3& pos, float& dist, BarycentricCoords& coords)
	{
		Vector3 p = ClosestPtPointTriangle(v, v1, v2, v3, coords);
		Vector3 delta = v - p;

		Vector3 dir = ref - p;
		dir.Normalize();
		//dir = dir.Dot(normal) > 0 ? normal : -1.f * normal;
		float d = delta.Dot(dir);
		//if (d > radTol)
		//return false;

		float dSqr = delta.LengthSquared();
		if (dSqr > radTol * radTol && d > 0)
			return false;

		//Vector3 delta1 = v - v1;
		//float d1 = normal.Dot(delta1);
		//Vector3 p1 = v - d1 * normal;
		//coords = Barycentric(v1, v2, v3, p1);
		//if (coords.u >= -0.1f && coords.u <= 1.1f
		//	&& coords.v >= -0.1f && coords.v <= 1.1f
		//	&& coords.w >= -0.1f && coords.w <= 1.1f)
		//{
		//	dSqr = min(dSqr, d1 * d1);
		//}

		pos = p;
		delta.Normalize();
		normal = delta;
		dist = sqrtf(dSqr);
		//dist = fabs(d);
		return true;
	}

	// broken?
	bool IntersectSegmentTriangleOld(const Vector3& p, const Vector3& q, // the segment
		const Vector3& a, const Vector3& b, const Vector3& c, // the triangle
		BarycentricCoords& coords, float& t, Vector3* tn)
	{
		Vector3 ab = b - a;
		Vector3 ac = c - a;
		Vector3 qp = p - q;

		Vector3 n = tn != NULL ? *tn : ab.Cross(ac);

		float d = n.Dot(qp);
		if (d <= 0.f) return false;

		Vector3 ap = p - a;
		t = n.Dot(ap);
		if (t < 0.f || t > d) return false;

		Vector3 e = qp.Cross(ap);
		float v = ac.Dot(e);
		if (v < 0.f || v > d) return false;
		float w = -ab.Dot(e);
		if (w < 0.f || v + w > d) return false;

		float ood = 1.f / d;
		t *= ood;
		coords.v = v * ood;
		coords.w = w * ood;
		coords.u = 1.f - coords.v - coords.w;
		return true;
	}

	float SignedVolume(const Vector3& a0, const Vector3& a1, const Vector3& a2, const Vector3& a3)
	{
		return triple(a1 - a0, a2 - a0, a3 - a0);
	}

	bool SameSign(float a, float b)
	{
		// if one of them is zero, it doesn't matter the sign of the other
		if (a == 0 || b == 0)
			return true;
		// TODO: use an epsilon
		if (a > 0 && b > 0)
			return true;
		if (a < 0 && b < 0)
			return true;
		return false;
	}

	bool IntersectSegmentTriangle(const Vector3& p, const Vector3& q, const Vector3& a, const Vector3& b, const Vector3& c, BarycentricCoords& coords, float& t, Vector3& r, bool isRay)
	{
		// this is the double sided version of the test
		// TODO: reinstate the counter-clockwise triangle test

		const float epsilon = 1e-7f;

		// TODO: ray case

		coords.u = SignedVolume(b, c, p, q);
		//if (coords.u < -epsilon)
		//	return false;
		coords.v = SignedVolume(c, a, p, q);
		//if (coords.v < -epsilon)
		//	return false;
		if (!SameSign(coords.u, coords.v))
			return false;
		coords.w = SignedVolume(a, b, p, q);
		//if (coords.w < -epsilon)
		//	return false;
		if (!SameSign(coords.u, coords.w) || !SameSign(coords.v, coords.w))
			return false;

		float sumTri = coords.u + coords.v + coords.w;

		// this is the case when PQ lies in the plane of the triangle
		// TODO: handle separately
		if (fabs(sumTri) < epsilon)
			return false;

		float invSumTri = 1.f / sumTri;
		coords.u *= invSumTri;
		coords.v *= invSumTri;
		coords.w *= invSumTri;

		// TODO: check that the barycentric coords are really inside

		r = coords.u * a + coords.v * b + coords.w * c;
		Vector3 pq = q - p;
		Vector3 pr = r - p;
		float len = pq.Length();
		t = pr.Dot(pq) / len / len;

		if (t < -epsilon || (t > (1.0f + epsilon) && !isRay))
			return false;
		// TODO: check if points correspond

		return true;
	}

	bool IntersectSweptSphereTriangle(const Vector3& c, float radius, const Vector3& v, // the swept sphere
		const Vector3& v1, const Vector3& v2, const Vector3& v3, // the triangle
		Vector3& pos, Vector3& normal, float& dist, BarycentricCoords& coords)
	{
		Vector3 n = (v2 - v1).Cross(v3 - v1);
		n.Normalize();
		Vector3 d = c - radius * n;

		float t;
		Vector3 p;
		if (!IntersectSegmentPlane(d, d + v, n, v1.Dot(n), t, p))
		{
			return false;
		}

		coords = Barycentric(v1, v2, v3, p);
		if (coords.v >= 0.f && coords.w >= 0.f && coords.u >= 0.f)
		{
			// intersection is inside triangle
			pos = d + t * v;
			normal = n;
			return true;
		}

		// TODO: use the fact that it's in the same plane and/or its barycentric coordinates
		Vector3 q = ClosestPtPointTriangle(p, v1, v2, v3, coords);
		if (IntersectRaySphere(q, -v, c, radius, t, p) && t <= 1.f)
			//Vector3 r = ClosestPtSegm(q, c, c + v, t);
			//if ((q - r).LengthSquared() < radius * radius)
		{
			pos = q;
			normal = n;
			return true;
		}

		return false;
	}

} // namespace Geometry