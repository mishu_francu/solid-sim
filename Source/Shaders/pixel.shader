#version 140
// Interpolated values from the vertex shaders
//in vec2 UV;
in vec3 fragmentColor;
in vec3 normal;
in vec3 halfVec;

// TODO: varying is deprecated
//varying vec3 normal;
in vec3 light;
in vec3 EyeDirection_cameraspace;

// Ouput data
out vec4 color;
 
// Values that stay constant for the whole mesh.
//uniform sampler2D myTextureSampler;
uniform vec3 l; // TODO: rename
uniform mat4 V;
uniform vec3 diffColor;
uniform vec3 specColor;
uniform vec3 ambientColor;
uniform vec3 lightColor;
uniform float gloss;

vec3 Shade()
{
	// Cosine of the angle between the normal and the light direction,
	// clamped above 0
	//  - light is at the vertical of the triangle -> 1
	//  - light is perpendicular to the triangle -> 0
	//  - light is behind the triangle -> 0
	float cosTheta = clamp( dot( normal,light ), 0,1 );
	 
	//color = 0.5 * (normal + vec3(1, 1, 1));
	//color = vec3(0.1, 0.1, 0.5) + fragmentColor * cosTheta;

	// Eye vector (towards the camera)
	vec3 E = normalize(EyeDirection_cameraspace);
	// Direction in which the triangle reflects the light
	vec3 R = reflect(-light, normal);
	// Cosine of the angle between the Eye vector and the Reflect vector,
	// clamped to 0
	//  - Looking into the reflection -> 1
	//  - Looking elsewhere -> < 1
	float cosAlpha = clamp( dot( E,R ), 0,1 );
	
	// TODO: pass ambient light as uniform
	return vec3(0.2, 0.2, 0.2) + fragmentColor * cosTheta;// + fragmentColor * pow(cosAlpha, 5);
}

#define PI 3.1415926535897932384626433832795
//#define VERTEX_SHADING

// diffuse only
//vec3 ShadeLambert(vec3 color)
//{
//	vec3 diffColor = color; // TODO: diffuse color
//	vec3 lightColor = vec3(30); // TODO: light color; so big?
//	vec3 lightDir = normalize(l);
//	vec3 n = normal; // TODO: rename to normal if possible; check if normalized?
//	float cosTheta = clamp(dot(lightDir, n), 0, 1);
//	vec3 ambientColor = vec3(0.2); // TODO: ambient color
//	return ambientColor + diffColor * lightColor * cosTheta / PI;
//}

vec3 ShadeLambert()
{
	vec3 lightDirVS = normalize((V * vec4(l, 0)).xyz); // TODO: light dir vs?
	vec3 n = normalize(normal);
	vec3 h = normalize(halfVec);
	
	//vec3 lightColor = vec3(1); // TODO: light color
	float cosi = clamp(dot(lightDirVS, n), 0, 1);
	float cosh = clamp(dot(h, n), 0, 1);
	
	return ambientColor + (diffColor + (gloss + 8) * pow(cosh, gloss) * specColor / 8) * lightColor * cosi / PI;		
}

void main()
{ 
    // Output color = color of the texture at the specified UV
    //color = texture( myTextureSampler, UV ).rgb;

#ifdef VERTEX_SHADING
	// per vertex shading
	color = vec4(fragmentColor, 1);
#else
	// per pixel shading
	color = vec4(ShadeLambert(), 1);
#endif
	//color = vec4(1, 1, 1, 1);
}
