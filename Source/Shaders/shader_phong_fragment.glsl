#version 400
layout(location = 0) out vec4 out_color;

uniform vec3 light_position;
uniform vec3 eye_position;
uniform int material_shininess;
uniform vec3 material_kd;
uniform vec3 material_ks;
uniform int flip;
uniform int flags;

uniform sampler2D map_color;
uniform sampler2DShadow shadowMap;
//uniform sampler2D shadowMap; // normal texture for shadow map

#define VERTEX_COLORS 1
#define OREN_NAYAR 2
#define TEXTURED 4
#define MONOCHROME 8

in vec3 position;
in vec3 normal;
in vec3 color;
in vec2 texcoord;
in vec4 ShadowCoord;

float orennayar(float roughness){
    vec3 L = normalize(light_position - position);
    vec3 N = normalize(normal);
    vec3 V = normalize(eye_position - position);

    float acosVN = acos(dot(V, N));
    float acosLN = acos(dot(L, N));
    float alpha = max(acosVN, acosLN);
    float beta = min(acosVN, acosLN);
    float gamma = dot(V - N * dot(V, N), L - N * dot(L, N));
    float rough_sq = roughness * roughness;

    float C1 = 1.0f - 0.5f * (rough_sq / (rough_sq + 0.33f));
    float C2 = 0.45f * (rough_sq / (rough_sq + 0.09));
    if (gamma >= 0)
    {
        C2 *= sin(alpha);
    }
    else
    {
        C2 *= (sin(alpha) - pow((2 * beta) / 3.14167, 3));
    }

    float C3 = (1.0f / 8.0f);
    C3 *= (rough_sq / (rough_sq + 0.09f));
    C3 *= pow((4.0f * alpha * beta) / (3.14167 * 3.14167), 2);

    float A = gamma * C2 * tan(beta);
    float B = (1 - abs(gamma)) * C3 * tan((alpha + beta) / 2.0f);

    return min(max(0.0f, dot(N, L)) * (C1 + A + B),0.99f);
}

void main(){
	vec3 N = flip * normalize ( normal);
	vec3 L = normalize ( light_position - position); // the light direction with respect to this point
	vec3 V = normalize ( eye_position - position);

	float ambient_light = 0.2;
	float diffuse_light = max(dot(L,N), 0);
	float specular_light = 0;

	// Compute specular component
	if(diffuse_light > 0 && length(material_ks) > 0){
		specular_light = pow(max(dot(V,reflect(L,N)),0), material_shininess);	//Phong
		//specular_light = pow(max(dot(N,normalize(L+V)),0), material_shininess);		//Blinn - Phong
	}

	//float shade = (ambient_light + diffuse_light);
	float shade = ((flags & OREN_NAYAR) != 0) ? orennayar(1.4) : (ambient_light + diffuse_light);

	vec3 albedo = ((flags & TEXTURED) == 0) ? material_kd : 2 * material_kd * texture(map_color, texcoord).xyz;

	vec3 mat_col = ((flags & VERTEX_COLORS) != 0) ? albedo * (1 - color.r) + color : albedo;
	// TODO: clamp mat_col
	//vec3 mat_col = albedo * (1 - color.r) + color;
	vec3 light = mat_col * shade + material_ks * specular_light;
	
	// Biased shadow coordinates; TODO: use a bias matrix
	vec4 shadowCoords = vec4(ShadowCoord.xyz * 0.5 + vec3(0.5), 1);
	
	// Shadows using a traditional texture
	//float depth = texture( shadowMap, shadowCoords.xy).r;
	float visibility = 1.0f;
	//if ( depth  <  shadowCoords.z) {
		//visibility = 0.5;
	//}

	// Compute the shadow visibility factor
	float bias = 0.005;
	visibility = max(texture( shadowMap, vec3(shadowCoords.xy, (shadowCoords.z - bias)/shadowCoords.w) ), 0.5f);
	
	out_color = ((flags & MONOCHROME) != 0) ? vec4(material_kd, 1) : vec4(visibility * light, 1);
}