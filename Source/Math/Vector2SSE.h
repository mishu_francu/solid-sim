#include <intrin.h>

typedef __m128 vector4;

// TODO: remove unions for performance?
union Quad
{
	vector4 q;
	float f[4];
};

//#define USE_SCALAR

#ifdef USE_SCALAR
// Format: <val, ?, ?, ?>, although usually initialized to <val, 0, 0, 0>
ALIGN16 struct Scalar
{
	ALIGN16 Quad quad;

	Scalar() { }

	// TODO: make it implicit but with warning?
	explicit Scalar(float val)
	{
#		pragma message("converting float to scalar")
		quad.q = _mm_set_ss(val);
	}

	float GetFloat() const
	{
		// TODO: warning
		return _mm_cvtss_f32(quad.q);
	}

	operator float() const
	{
#		pragma message("converting scalar to float")
		return quad.f[0];
	}

	Scalar GetReciprocal() const
	{
		Scalar ret;
		ret.quad.q = _mm_rcp_ss(quad.q);
		return ret;
	}

	Scalar GetSqrt() const
	{
		Scalar ret;
		ret.quad.q = _mm_sqrt_ss(quad.q);
		return ret;
	}

	void operator +=(const Scalar& s)
	{
		quad.q = _mm_add_ss(quad.q, s.quad.q);
	}

	void operator *=(const Scalar& s)
	{
		quad.q = _mm_mul_ss(quad.q, s.quad.q);
	}

	int IsZero() const
	{
		static const vector4 zero = _mm_setzero_ps();
		vector4 mask = _mm_cmpeq_ss(quad.q, zero);
		return _mm_cvttss_si32(mask);
	}

	vector4 CompareGeq(const Scalar& s) const
	{
		return _mm_cmpge_ss(quad.q, s.quad.q);
	}

	static Scalar Select(vector4 mask, const Scalar& a, const Scalar& b)
	{
		Scalar ret;
		ret.quad.q = _mm_or_ps(_mm_and_ps(mask, a.quad.q), _mm_andnot_ps(mask, b.quad.q));
		return ret;
	}
};

inline Scalar sqrt(const Scalar& s)
{
	return s.GetSqrt();
}

inline Scalar operator -(const Scalar& s)
{
	static const vector4 SIGNMASK = _mm_castsi128_ps(_mm_set1_epi32(0x80000000));
	Scalar ret;
	ret.quad.q = _mm_xor_ps(s.quad.q, SIGNMASK);
	return ret;
}

inline Scalar operator +(const Scalar& a, const Scalar& b)
{
	Scalar ret;
	ret.quad.q = _mm_add_ss(a.quad.q, b.quad.q);
	return ret;
}

inline Scalar operator -(const Scalar& a, const Scalar& b)
{
	Scalar ret;
	ret.quad.q = _mm_sub_ss(a.quad.q, b.quad.q);
	return ret;
}

inline Scalar operator *(const Scalar& a, const Scalar& b)
{
	Scalar ret;
	ret.quad.q = _mm_mul_ss(a.quad.q, b.quad.q);
	return ret;
}

inline Scalar operator /(const Scalar& a, const Scalar& b)
{
	Scalar ret;
	// TODO: roll back to div is precision is too low
	//ret.quad.q = _mm_div_ss(a.quad.q, b.quad.q);
	ret.quad.q = _mm_mul_ss(a.quad.q, _mm_rcp_ss(b.quad.q));
	return ret;
}

inline int operator <(const Scalar& a, const Scalar& b)
{
	vector4 mask = _mm_cmplt_ss(a.quad.q, b.quad.q);
	return _mm_cvttss_si32(mask); // TODO: _mm_movemask_ps?
}

inline int operator <=(const Scalar& a, const Scalar& b)
{
	vector4 mask = _mm_cmple_ss(a.quad.q, b.quad.q);
	return _mm_cvttss_si32(mask);
}

inline int operator >=(const Scalar& a, const Scalar& b)
{
	vector4 mask = _mm_cmpge_ss(a.quad.q, b.quad.q);
	return _mm_cvttss_si32(mask);
}

inline int operator ==(const Scalar& a, const Scalar& b)
{
	vector4 mask = _mm_cmpeq_ss(a.quad.q, b.quad.q);
	return _mm_cvttss_si32(mask);
}

inline int operator >(const Scalar& a, const Scalar& b)
{
	vector4 mask = _mm_cmpgt_ss(a.quad.q, b.quad.q);
	return _mm_cvttss_si32(mask); // TODO: _mm_movemask_ps?
}
#else
typedef float Scalar;
#endif

// Format <x, y, ?, ?>, although usually initialized to <x, y, 0, 0>
template<>
struct Vector2Tpl<Scalar>
{
	ALIGN16 Quad quad;

	INLINE Vector2Tpl()
	{
		SetZero();
	}
	
	INLINE explicit Vector2Tpl(float a, float b)
	{ 
		Set(a, b);
	}

	INLINE explicit Vector2Tpl(float a)
	{ 
		Set(a, a);
	}

	INLINE explicit Vector2Tpl(const vector4& q)
	{
		quad.q = q;
	}

	template <typename Real>
	INLINE explicit Vector2Tpl(const Vector2Tpl<Real>& v)
	{
		Set(v.GetX(), v.GetY()); // TODO: load instruction
	}

	INLINE vector4& GetNative()
	{
		return quad.q;
	}
	
	INLINE void Set(float a, float b)
	{
		quad.q = _mm_setr_ps(a, b, 0, 0);
	}

	INLINE void SetZero()
	{
		quad.q = _mm_setzero_ps();
	}

	// TODO: convert to Scalar
	INLINE float GetX() const
	{
		return quad.f[0]; // TODO: use cast
	}

	INLINE float GetY() const
	{
		return quad.f[1];
	}

	INLINE void Subtract(const Vector2Tpl& v)
	{
		quad.q = _mm_sub_ps(quad.q, v.quad.q);
	}

	INLINE void Add(const Vector2Tpl& v)
	{
		quad.q = _mm_add_ps(quad.q, v.quad.q);
	}

	INLINE void Multiply(const Vector2Tpl& v)
	{
		quad.q = _mm_mul_ps(quad.q, v.quad.q);
	}

	INLINE void Divide(const Vector2Tpl& v)
	{
		quad.q = _mm_div_ps(quad.q, v.quad.q);
	}

	INLINE void Flip()
	{
		static vector4 minOne = _mm_set1_ps(-1.f); // TODO: is this really initialized statically?
		quad.q = _mm_mul_ps(quad.q, minOne);
	}

#if defined(FLOAT_SCALING_OPERATORS) || !defined(USE_SCALAR)
	INLINE void Scale(float s)
	{
#		pragma message("scaling vector by float")
		quad.q = _mm_mul_ps(quad.q, _mm_set1_ps(s));
	}
#endif

#ifdef USE_SCALAR
	INLINE void Scale(const Scalar& s)
	{
		vector4 qs = _mm_unpacklo_ps(s.quad.q, s.quad.q); // broadcast
		quad.q = _mm_mul_ps(quad.q, qs);
	}

	INLINE Scalar Dot(const Vector2Tpl& v) const
	{
		vector4 prods = _mm_mul_ps(quad.q, v.quad.q);
		Scalar ret;
		ret.quad.q = _mm_hadd_ps(prods, prods); // this is SSE3 only!!!
		return ret;
	}

	INLINE Scalar LengthSquared() const
	{
		return Dot(*this);
	}

	INLINE Scalar Length() const
	{
		Scalar ret;
		ret.quad.q = _mm_sqrt_ss(LengthSquared().quad.q);
		return ret;
	}

	INLINE void Normalize()
	{
		Scalar invLen;
		invLen.quad.q = _mm_rsqrt_ss(LengthSquared().quad.q);
		Scale(invLen);
	}

	INLINE bool NormalizeCheck()
	{
		Scalar lenSqr = LengthSquared();
		if (lenSqr.IsZero())
			return false;
		Scalar invLen;
		invLen.quad.q = _mm_rsqrt_ss(lenSqr.quad.q);
		Scale(invLen);
		return true;
	}
#else
	void Scale4(vector4 s)
	{
		quad.q = _mm_mul_ps(quad.q, _mm_shuffle_ps(s, s, 0));
	}

	vector4 Dot4(const Vector2Tpl& v) const
	{

		vector4 prods = _mm_mul_ps(quad.q, v.quad.q);				
		vector4 dot = _mm_hadd_ps(prods, prods); // this is SSE3 only!!!
		// TODO: SSE4 _mm_dp_ps
		return dot;
	}

	float Dot(const Vector2Tpl& v) const
	{
		return _mm_cvtss_f32(Dot4(v));
	}

	vector4 LengthSquared4() const
	{
		return Dot4(*this);
	}

	Scalar LengthSquared() const
	{
		return Dot(*this);
	}

	Scalar Length() const
	{
		vector4 ret = _mm_sqrt_ss(LengthSquared4());
		return _mm_cvtss_f32(ret);
	}

	void Normalize()
	{
		vector4 invLen = _mm_rsqrt_ss(LengthSquared4());
		Scale4(invLen);
	}

	bool NormalizeCheck()
	{
		vector4 lenSqr = LengthSquared4();
		if (_mm_cvtss_f32(lenSqr) == 0.f)
			return false;
		vector4 invLen = _mm_rsqrt_ss(lenSqr);
		Scale4(invLen);
		return true;
	}
#endif

	Scalar Cross(const Vector2Tpl& v) const
	{
		Vector2Tpl temp(v.GetY(), -v.GetX()); // TODO: optimize (shuffle)
		return Dot(temp);
	}

	INLINE Vector2Tpl operator +=(const Vector2Tpl& v)
	{
		Add(v);
		return *this;
	}

	INLINE Vector2Tpl operator -=(const Vector2Tpl& v)
	{
		Subtract(v);
		return *this;
	}
	
	void Rotate(float theta)
	{
		// TODO: optimize with SSE
		const float c = cos(theta);
		const float s = sin(theta);
		const float x0 = quad.f[0];
		quad.f[0] = x0 * c - quad.f[1] * s;
		quad.f[1] = x0 * s + quad.f[1] * c;
	}
};

typedef Vector2Tpl<Scalar> Vector2;
