#if USE_SSE && defined(USE_SCALAR)
// Format: <val1, val2, ?, ?>
struct ScalarPair
{
	Quad quad;

	ScalarPair() { }

	explicit ScalarPair(float val)
	{
		quad.q = _mm_set1_ps(val);
	}

	explicit ScalarPair(const Scalar& a, const Scalar& b)
	{
		quad.q = _mm_unpacklo_ps(a.quad.q, b.quad.q);
	}

	// TODO: conversions to Scalar
	float GetX() const
	{
		return quad.f[0];
	}

	float GetY() const
	{
		return quad.f[1];
	}

	ScalarPair Sqrt() const
	{
		ScalarPair ret;
		ret.quad.q = _mm_sqrt_ps(quad.q);
		return ret;
	}

	ScalarPair RcpSqrt() const
	{
		ScalarPair ret;
		ret.quad.q = _mm_rsqrt_ps(quad.q);
		return ret;
	}
};

inline ScalarPair operator +(const ScalarPair& a, const ScalarPair& b)
{
	ScalarPair ret;
	ret.quad.q = _mm_add_ps(a.quad.q, b.quad.q);
	return ret;
}

inline ScalarPair operator -(const ScalarPair& a, const ScalarPair& b)
{
	ScalarPair ret;
	ret.quad.q = _mm_sub_ps(a.quad.q, b.quad.q);
	return ret;
}

inline ScalarPair operator *(const ScalarPair& a, const ScalarPair& b)
{
	ScalarPair ret;
	ret.quad.q = _mm_mul_ps(a.quad.q, b.quad.q);
	return ret;
}

inline ScalarPair operator /(const ScalarPair& a, const ScalarPair& b)
{
	ScalarPair ret;
	ret.quad.q = _mm_mul_ps(a.quad.q, _mm_rcp_ps(b.quad.q));
	return ret;
}

// Format: <x1, y1, x2, y2>
struct Vector2Pair
{
	Quad quad;

	Vector2Pair()
	{
		SetZero();
	}

	explicit Vector2Pair(const Vector2& a, const Vector2& b)
	{
		quad.q = _mm_movelh_ps(a.quad.q, b.quad.q);
	}

	void Set(const Vector2& a, const Vector2& b)
	{
		quad.q = _mm_movelh_ps(a.quad.q, b.quad.q);
	}

	void SetZero()
	{
		quad.q = _mm_setzero_ps();
	}

	Vector2 GetVector1() const
	{
		return Vector2(quad.q);
	}

	Vector2 GetVector2() const
	{
		return Vector2(_mm_movehl_ps(quad.q, quad.q));
	}

	void Subtract(const Vector2Pair& v)
	{
		quad.q = _mm_sub_ps(quad.q, v.quad.q);
	}

	void Add(const Vector2Pair& v)
	{
		quad.q = _mm_add_ps(quad.q, v.quad.q);
	}

	void Scale(const ScalarPair& sp)
	{
		vector4 qs = _mm_shuffle_ps(sp.quad.q, sp.quad.q, _MM_SHUFFLE(1, 1, 0, 0));
		quad.q = _mm_mul_ps(quad.q, qs);
	}
	
	ScalarPair Dot(const Vector2Pair& v) const
	{
		vector4 prods = _mm_mul_ps(quad.q, v.quad.q);
		ScalarPair ret;
		ret.quad.q = _mm_hadd_ps(prods, prods);
		return ret;
	}

	ScalarPair LengthSqr() const
	{
		return Dot(*this);
	}

	ScalarPair Length() const
	{
		return LengthSqr().Sqrt();
	}

	void Normalize()
	{
		ScalarPair invLen = LengthSqr().RcpSqrt();
		Scale(invLen);
	}

	Vector2Pair operator +=(const Vector2Pair& vp)
	{
		Add(vp);
		return *this;
	}
};
#else
// TODO: templated pairs
typedef Vector2 ScalarPair;

struct Vector2Pair
{
	Vector2 u, v;

	Vector2Pair()
	{
		SetZero();
	}

	explicit Vector2Pair(Vector2& a, Vector2& b) : u(a), v(b)
	{
	}

	void Set(Vector2& a, Vector2& b)
	{
		u = a;
		v = b;
	}

	void Set(float x, float y, float z, float w)
	{
		u.Set(x, y);
		v.Set(z, w);
	}

	void SetZero()
	{
		u.SetZero();
		v.SetZero();
	}

	Vector2 GetVector1() const
	{
		return u;
	}

	Vector2 GetVector2() const
	{
		return v;
	}

	void Add(const Vector2Pair& v2p)
	{
		u.Add(v2p.u);
		v.Add(v2p.v);
	}

	void Subtract(const Vector2Pair& v2p)
	{
		u.Subtract(v2p.u);
		v.Subtract(v2p.v);
	}

	void Scale(const ScalarPair& sp)
	{
		u.Scale(sp.GetX());
		v.Scale(sp.GetY());
	}

	ScalarPair LengthSqr() const
	{
		return ScalarPair(u.LengthSquared(), v.LengthSquared());
	}
	
	ScalarPair Dot(const Vector2Pair& w) const
	{
		return ScalarPair(u.Dot(w.u), v.Dot(w.v));
	}

	ScalarPair Length() const
	{
		return ScalarPair(u.Length(), v.Length());
	}

	void Normalize()
	{
		ScalarPair len = Length();
		Scale(ScalarPair(1.f / len.GetX(), 1.f / len.GetY()));
	}

	Vector2Pair operator +=(const Vector2Pair& vp)
	{
		u.Add(vp.u);
		v.Add(vp.v);
		return *this;
	}

};

inline Vector2Pair operator *(float r, const Vector2Pair& vp)
{
	Vector2Pair ret;
	ret.u = r * vp.u;
	ret.v = r * vp.v;
	return ret;
}

inline Vector2Pair operator *(const Vector2Pair& vp, float r)
{
	Vector2Pair ret;
	ret.u = r * vp.u;
	ret.v = r * vp.v;
	return ret;
}

inline Vector2Pair operator +(const Vector2Pair& vp1, const Vector2Pair& vp2)
{
	Vector2Pair ret;
	ret.u = vp1.u + vp2.u;
	ret.v = vp1.v + vp2.v;
	return ret;
}

inline Vector2Pair operator -(const Vector2Pair& vp1, const Vector2Pair& vp2)
{
	Vector2Pair ret;
	ret.u = vp1.u - vp2.u;
	ret.v = vp1.v - vp2.v;
	return ret;
}

#endif