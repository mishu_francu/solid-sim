#ifndef MATRIX2_H
#define MATRIX2_H

template <typename Real>
struct Matrix2T
{
	Real a11, a12, 
		  a21, a22;

	Matrix2T() : a11(1), a12(0), a21(0), a22(1) { }

	Matrix2T(Real a, Real b, Real c, Real d) : a11(a), a12(b), a21(c), a22(d) { }

	Matrix2T(Math::Vector2Tpl<Real> col1, Math::Vector2Tpl<Real> col2)
	{
		a11 = col1.x;
		a21 = col1.y;
		a12 = col2.x;
		a22 = col2.y;
	}

	Matrix2T(Math::Vector2Tpl<Real> diag)
	{
		a11 = diag.x;
		a22 = diag.y;
		a12 = a21 = 0;
	}

	Math::Vector2Tpl<Real> operator ()(int i) const
	{
		if (i == 0)
			return Math::Vector2Tpl<Real>(a11, a21);
		else
			return Math::Vector2Tpl<Real>(a12, a22);
	}

	Real operator ()(int i, int j) const
	{
		if (i == 0 && j == 0)
			return a11;
		if (i == 0 && j == 1)
			return a12;
		if (i == 1 && j == 0)
			return a21;
		if (i == 1 && j == 1)
			return a22;
		return 0;
	}

	static Matrix2T TensorProduct(const Math::Vector2Tpl<Real>& u, const Math::Vector2Tpl<Real>& v)
	{
		Matrix2T m;
		m.a11 = u.GetX() * v.GetX();
		m.a12 = u.GetX() * v.GetY();
		m.a21 = u.GetY() * v.GetX();
		m.a22 = u.GetY() * v.GetY();
		return m;
	}

	static Matrix2T Identity()
	{
		Matrix2T m;
		m.a11 = 1;
		m.a12 = 0;
		m.a21 = 0;
		m.a22 = 1;
		return m;
	}

	static Matrix2T Zero()
	{
		Matrix2T m;
		m.a11 = 0;
		m.a12 = 0;
		m.a21 = 0;
		m.a22 = 0;
		return m;
	}

	Real Trace() const
	{
		return a11 + a22;
	}

	Real Determinant() const
	{
		return a11 * a22 - a12 * a21;
	}

	Matrix2T GetInverse() const
	{
		Real det = Determinant();
		if (det == 0)
			return Matrix2T();
		Matrix2T m;
		m.a11 = a22 / det;
		m.a12 = -a12 / det;
		m.a21 = -a21 / det;
		m.a22 = a11 / det;
		return m;
	}
};

typedef Matrix2T<float> Matrix2;

template <typename Real>
inline Math::Vector2Tpl<Real> operator *(const Matrix2T<Real>& m, const Math::Vector2Tpl<Real>& v)
{
	Math::Vector2Tpl<Real> w;
	w.x = m.a11 * v.x + m.a12 * v.y;
	w.y = m.a21 * v.x + m.a22 * v.y;
	return w;
}

template <typename Real>
inline Matrix2T<Real> operator *(Real s, const Matrix2T<Real>& m)
{
	Matrix2T<Real> r;
	r.a11 = s * m.a11;
	r.a12 = s * m.a12;
	r.a21 = s * m.a21;
	r.a22 = s * m.a22;
	return r;
}

template <typename Real>
inline Matrix2T<Real> operator +(const Matrix2T<Real>& a, const Matrix2T<Real>& b)
{
	Matrix2T<Real> r;
	r.a11 = a.a11 + b.a11;
	r.a12 = a.a12 + b.a12;
	r.a21 = a.a21 + b.a21;
	r.a22 = a.a22 + b.a22;
	return r;
}

template <typename Real>
inline Matrix2T<Real> operator -(const Matrix2T<Real>& a, const Matrix2T<Real>& b)
{
	Matrix2T<Real> r;
	r.a11 = a.a11 - b.a11;
	r.a12 = a.a12 - b.a12;
	r.a21 = a.a21 - b.a21;
	r.a22 = a.a22 - b.a22;
	return r;
}

template <typename Real>
inline Matrix2T<Real> operator *(const Matrix2T<Real>& a, const Matrix2T<Real>& b)
{
	Matrix2T<Real> r;
	r.a11 = a.a11 * b.a11 + a.a12 * b.a21;
	r.a12 = a.a11 * b.a12 + a.a12 * b.a22;
	r.a21 = a.a21 * b.a11 + a.a22 * b.a21;
	r.a22 = a.a21 * b.a12 + a.a22 * b.a22;
	return r;
}

template <typename Real>
inline Matrix2T<Real> operator !(const Matrix2T<Real>& m)
{
	Matrix2T<Real> r;
	r.a11 = m.a11;
	r.a12 = m.a21;
	r.a21 = m.a12;
	r.a22 = m.a22;
	return r;
}


#endif // MATRIX2_H