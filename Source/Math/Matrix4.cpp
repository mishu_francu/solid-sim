#include <Engine/Base.h>
#include <Math/Matrix4.h>

Matrix4 Matrix4::LookAt(const Vector3& eye, const Vector3& center, const Vector3& up)
{
	Vector3 z = eye - center;
	z.Normalize();
	Vector3 x = z.Cross(up);
	x.Normalize();
	Vector3 y = x.Cross(z);
	Matrix4 lookAt;// (x, y, z, Vector3());
	lookAt.col[0][0] = x.x;
	lookAt.col[0][1] = y.x;
	lookAt.col[0][2] = z.x;
	lookAt.col[1][0] = x.y;
	lookAt.col[1][1] = y.y;
	lookAt.col[1][2] = z.y;
	lookAt.col[2][0] = x.z;
	lookAt.col[2][1] = y.z;
	lookAt.col[2][2] = z.z;
	lookAt.col[3][0] = -x.Dot(eye);
	lookAt.col[3][1] = -y.Dot(eye);
	lookAt.col[3][2] = -z.Dot(eye);
	lookAt.col[3][3] = 1.f;
	return lookAt;
}
