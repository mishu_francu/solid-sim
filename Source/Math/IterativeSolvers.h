#ifndef ITERATIVE_SOLVERS_H
#define ITERATIVE_SOLVERS_H

#include "Matrix.h"

// References:
// [Shewchuck] Shewchuk, J.R., "An Introduction to the Conjugate Gradient Method Without the Agonizing Pain"

// Matrix free methods
template <typename Real, typename Vector, class MatrixMultiplier>
int SolveConjugateGradientMF(const MatrixMultiplier& A, const std::vector<Vector>& b, std::vector<Vector>& x, int numIters, float eps = 1e-3f, bool usePrecond = false)
{
	// algorithm taken from [Shewchuk]

	std::vector<Vector> r = b; // allocation!
	// compute the full residual only if x != 0
	std::vector<Vector> df(b.size()); // allocation!
	if (InnerProduct<Vector, Real>(x, x) != 0)
	{
		A.MatrixVectorMultiply(x, df);
		r = b - df;
	}
	std::vector<Vector> d = r; // allocation!

	// initialize vector used for preconditioning
	std::vector<Vector> pcv;
	std::vector<Real> diagInv;
	if (usePrecond)
	{
		// allocate only if needed
		pcv.resize(b.size());
		diagInv.resize(b.size());

		// pre-compute inverse diagonal values
		for (size_t i = 0; i < diagInv.size(); i++)
		{
			// prepare unit vector
			std::fill(pcv.begin(), pcv.end(), Vector(0));
			pcv[i] = Vector(1);
			// compute A-dot product
			A.MatrixVectorMultiply(pcv, df);
			Real val = InnerProduct<Vector, Real>(pcv, df);
			diagInv[i] = Real(1) / val;

			d[i] = r[i] * diagInv[i];
		}
	}
	std::vector<Vector>& s = usePrecond ? pcv : r; // just a shell for r if not preconditioned

	Real delta = InnerProduct<Vector, Real>(r, d);
	if (delta == 0)
		return 0;
	Real delta0 = delta;

	for (int iter = 0; iter < numIters; iter++)
	{
		A.MatrixVectorMultiply(d, df);
		Real alpha = delta / InnerProduct<Vector, Real>(d, df);
		x = x + alpha * d;
		// TODO: move the calculations below to the top of the loop
#ifdef EXACT_RESIDUAL
		if (iter % 50 == 0)
		{
			A.MatrixVectorMultiply(x, df);
			r = b - df;
		}
		else
#endif
		r = r - alpha * df;

		if (usePrecond)
		{
			for (size_t i = 0; i < diagInv.size(); i++)
				s[i] = r[i] * diagInv[i];
		}

		Real delta1 = InnerProduct<Vector, Real>(r, s);
		Real beta = delta1 / delta;
		delta = delta1;

		// the convergence criterion from [Shewchuk]
		if (abs(delta) <= eps * eps * abs(delta0))
		{
			// convergence criterion met
			return iter + 1;
		}
		d = s + beta * d;
	}
	return numIters;
}

template <typename Real, typename Vector, class MatrixMultiplier>
int SolveConjugateResidualMF(const MatrixMultiplier& A, const std::vector<Vector>& b, std::vector<Vector>& x, int numIters, float eps = 1e-3f, bool usePrecond = false)
{
	// algorithm taken from [Shewchuk]

	std::vector<Vector> r = b; // allocation!
	// compute the full residual only if x != 0
	std::vector<Vector> q(b.size()); // allocation!
	if (InnerProduct<Vector, Real>(x, x) != 0)
	{
		A.MatrixVectorMultiply(x, q); // q = Ax
		r = b - q;
	}

	// initialize vector used for preconditioning
	std::vector<Vector> pcv;
	std::vector<Real> diagInv;
	if (usePrecond)
	{
		// allocate only if needed
		pcv.resize(b.size());
		diagInv.resize(b.size());

		// pre-compute inverse diagonal values
		for (size_t i = 0; i < diagInv.size(); i++)
		{
			// prepare unit vector
			std::fill(pcv.begin(), pcv.end(), Vector(0));
			pcv[i] = Vector(1);
			// compute A-dot product
			A.MatrixVectorMultiply(pcv, q);
			Real val = InnerProduct<Vector, Real>(pcv, q);
			diagInv[i] = Real(1) / val;

			r[i] = r[i] * diagInv[i];
		}
	}

	std::vector<Vector> d = r; // allocation!	
	A.MatrixVectorMultiply(r, q); // q = Ar

	std::vector<Vector> p(b.size()); // allocation!
	A.MatrixVectorMultiply(d, p); // p = Ad
	std::vector<Vector>& s = usePrecond ? pcv : p; // just a shell for p if not preconditioned

	Real delta = InnerProduct<Vector, Real>(r, q);
	if (delta == 0)
		return 0;
	Real delta0 = InnerProduct<Vector, Real>(r, r);

	for (int iter = 0; iter < numIters; iter++)
	{
		if (usePrecond)
		{
			for (size_t i = 0; i < diagInv.size(); i++)
				s[i] = p[i] * diagInv[i];
		}

		Real alpha = delta / InnerProduct<Vector, Real>(p, s);
		x = x + alpha * d;
		r = r - alpha * s;

		A.MatrixVectorMultiply(r, q); // q = Ar

		Real delta1 = InnerProduct<Vector, Real>(r, q);
		Real beta = delta1 / delta;
		delta = delta1;

		// the convergence criterion from [Shewchuk]
		if (abs(InnerProduct<Vector, Real>(r, r)) <= eps * eps * abs(delta0))
		{
			// convergence criterion met
			return iter + 1;
		}
		
		d = r + beta * d;
		p = q + beta * p;
	}
	return numIters;
}

#endif // ITERATIVE_SOLVERS_H