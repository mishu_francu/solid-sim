#ifndef MATRIX_H
#define MATRIX_H

#include <Engine/Utils.h>
#include <vector>
#include <algorithm>

// TODO: namespace
enum MajorType
{
	ROW,
	COLUMN
};

// TODO: rename Real to something neutral like T
template <typename Real = float>
struct MatrixSqrBase
{
	size_t size;
	Real* data;
	bool own;

	MatrixSqrBase() : data(NULL), size(0), own(true) { }

	MatrixSqrBase(size_t n) : size(n), own(true)
	{
		data = new Real[n * n];
	}

	MatrixSqrBase(size_t n, Real* mem) : size(n), own(false), data(mem) { }

	~MatrixSqrBase()
	{
		if (own && data)
		{
			delete[] data;
		}
	}

	size_t GetSize() const { return size; }
	void SetSize(size_t n)
	{
		if (own && data)
		{
			delete[] data;
		}
		data = new Real[n * n];
		size = n;
	}

	void SetZero()
	{
		memset(data, 0, size * size * sizeof(Real));
	}
};

template <typename Real = float, int Major = ROW>
struct MatrixSqr : MatrixSqrBase<Real>
{
	MatrixSqr() : MatrixSqrBase<Real>() { }
	MatrixSqr(size_t n) : MatrixSqrBase<Real>(n) { }
	MatrixSqr(size_t n, Real* mem) : MatrixSqrBase<Real>(n, mem) { }

	// TODO: asserts
	Real& Get(size_t i, size_t j)
	{
		return this->data[i * this->size + j];
	}

	Real Get(size_t i, size_t j) const
	{
		return this->data[i * this->size + j];
	}

	Real& operator()(size_t i, size_t j) { return Get(i, j); }

	// return row i
	Real* operator[](size_t i)
	{
		return this->data + i * this->size;
	}

	void SetIdentity()
	{
		for (size_t i = 0; i < this->size; i++)
		{
			for (size_t j = 0; j < this->size; j++)
			{
				Get(i, j) = i == j ? 1 : 0;
			}
		}
	}

	template<typename VT, typename RT>
	std::vector<RT> Transform(const std::vector<VT>& v) const
	{
		ASSERT(this->size == v.size());
		std::vector<RT> r(this->size);
		for (size_t i = 0; i < this->size; i++)
		{
			RT sum = 0;
			for (size_t j = 0; j < this->size; j++)
			{
				sum += Get(i, j) * v[j];
			}
			r[i] = sum;
		}
		return r;
	}

	// linear system solving utils

	void GetTridiagonal(std::vector<Real>& a, std::vector<Real>& b, std::vector<Real>& c)
	{
		// a[0] and c[n - 1] are not used
		for(size_t i = 0; i < this->size; i++)
		{
			b[i] = Get(i, i);
			if (i > 0)
				a[i] = Get(i, i - 1);
			if (i < this->size - 1)
				c[i] = Get(i, i + 1);
		}
	}

	void PivotColumns(int i, int j)
	{
		for (size_t k = 0, base = 0; k < this->size; ++k, base += this->size)
		{
			std::swap(this->data[base + i], this->data[base + j]);
		}
	}

	void PivotRows(int i, int j)
	{
		for (size_t k = 0; k < this->size; ++k)
		{
			std::swap(this->data[i * this->size + k], this->data[j * this->size + k]);
		}
	}

	// Crout's algorithm
	void DecomposeLU(MatrixSqr<Real>& l, MatrixSqr<Real>& u) const
	{
		int n = (int)this->size;
		u.SetZero();
		l.SetZero(); // TODO: set identity
		for (int i = 0; i < n; ++i)
		{
			l.Get(i, i) = 1.0f;
		}
		for (int i = 0; i < n; ++i)
		{
			for (int j = i; j < n; ++j)
			{
				u.Get(i, j) = Get(i, j);
				for (int k = 0; k < i; ++k)
				{
					u.Get(i, j) -= l.Get(i, k) * u.Get(k, j);
				}
			}
			for (int j = i + 1; j < n; ++j)
			{
				l.Get(j, i) = Get(j, i);
				for (int k = 0; k < i; ++k)
				{
					l.Get(j, i) -= l.Get(j, k) * u.Get(k, i);
				}
				l.Get(j, i) /= u.Get(i, i);
			}
		}
	}

	bool DecomposeCholesky(MatrixSqr<Real>& el)
	{
		for (int i = 0; i < this->size; i++)
		{
			for (int j = i; j < this->size; j++)
			{
				Real sum = el.Get(i, j);
				for (int k = i - 1; k >= 0; k--)
					sum -= el.Get(i, k) * el.Get(j, k);
				if (i == j)
				{
					if (sum < 0.)
						return false;
					el.Get(i, i) = sqrt(sum);
				}
				else
				{
					el.Get(j, i) = sum / el.Get(i, i);
				}
			}
			for (int i = 0; i < this->size; i++)
			{
				for (int j = 0; j < i; j++)
					el.Get(j, i) = 0.;
			}
		}
		return true;
	}
};

template <typename Real>
bool SolveGauss(MatrixSqr<Real>& a, std::vector<Real>& b, std::vector<Real>& x)
{
	const int n = (int)a.GetSize();
	for (int i = 0; i < n - 1; ++i)
	{
		// find the maximum (absolute value) leading coefficient
		Real max = a.Get(i, i);
		int maxIndex = i;
		for (int j = i + 1; j < n; ++j)
		{
			Real aa = fabs(a.Get(j, i));
			if (maxIndex == -1 || aa >  max)
			{
				max = aa;
				maxIndex = j;
			}
		}
		if (maxIndex != i)
		{
			a.PivotRows(i, maxIndex);
			std::swap(b[i], b[maxIndex]);
		}

		// forward substitution (ith step)
		for (int j = i + 1; j < n; ++j)
		{
			// check if the denominator is non zero, otherwise pivot columns
			if (a.Get(i, i) == 0.0f)
			{
				bool nonZeroFound = false;
				for (int k = i + 1; k < n; ++k)
				{
					if (a.Get(i, k) != 0)
					{
						a.PivotColumns(i, k); // this shuffles the solution
						nonZeroFound = true;
						break;
					}
				}
				if (!nonZeroFound)
				{
					return false;
				}
			}
			
			// do the elimination step
			float f = a.Get(j, i) / a.Get(i, i);
			for (int k = 0; k < n; ++k)
			{
				a.Get(j, k) -= f * a.Get(i, k);
			}
			b[j] -= f * b[i];
		}
	}

	// back substitution
	for (int i = n - 1; i >= 0; --i)
	{
		float s = 0;
		for (int j = i + 1; j < n; ++j)
		{
			s += a.Get(i, j) * x[j];
		}
		x[i] = (b[i] - s) / a.Get(i, i);
	}

	return true;
}

template <typename Real>
void SolveLU(const MatrixSqr<Real>& l, const MatrixSqr<Real>& u, const std::vector<Real>& b, std::vector<Real>& x)
{
	size_t n = b.size();
	std::vector<Real> p(n);

	for (size_t i = 0; i < n; ++i)
	{
		p[i] = b[i];
		for (size_t j = 0; j < i; ++j)
		{
			p[i] -= l.Get(i, j) * p[j];
		}
	}

	for (int i = (int)(n - 1); i >= 0; --i)
	{
		float s = 0;
		for (size_t j = i + 1; j < n; ++j)
		{
			s += u.Get(i, j) * x[j];
		}
		x[i] = (p[i] - s) / u.Get(i, i);
	}
}

template <typename Real>
bool SolveTridiagonal(const std::vector<Real>& a, const std::vector<Real>& b, const std::vector<Real>& c, const std::vector<Real>& r, std::vector<Real>& u)
{
	size_t n = a.size();
	if (b[0] == 0.0)
		return false; // system should be rewritten
	Real bet = b[0];
	u[0] = r[0] / bet;
	std::vector<Real> gam(n);
	for (size_t j = 1; j < n; j++)
	{
		gam[j] = c[j - 1] / bet;
		bet = b[j] - a[j] * gam[j];
		if (bet == 0.0)
			return false; // algorithm fails
		u[j] = (r[j] - a[j] * u[j - 1]) / bet;
	}
	for (int j = (int)(n - 2); j >= 0; j--)
		u[j] -= gam[j + 1] * u[j + 1];
	return true;
}

template <typename Real>
std::vector<Real> operator *(const MatrixSqr<Real>& m, const std::vector<Real>& v)
{
	return m.template Transform<Real, Real>(v);
}

template <typename Real>
std::vector<Real> operator -(const std::vector<Real>& a, const std::vector<Real>& b)
{
	size_t n = a.size();
	std::vector<Real> r(n);
	for (size_t i = 0; i < n; i++)
		r[i] = a[i] - b[i];
	return r;
}

template <typename Real>
std::vector<Real> operator +(const std::vector<Real>& a, const std::vector<Real>& b)
{
	size_t n = a.size();
	std::vector<Real> r(n);
	for (size_t i = 0; i < n; i++)
		r[i] = a[i] + b[i];
	return r;
}

template <typename Elem>
std::vector<Elem> operator *(float s, const std::vector<Elem>& v)
{
	size_t n = v.size();
	std::vector<Elem> r(n);
	for (size_t i = 0; i < n; i++)
		r[i] = s * v[i];
	return r;
}

template <typename Elem>
std::vector<Elem> operator *(double s, const std::vector<Elem>& v)
{
	size_t n = v.size();
	std::vector<Elem> r(n);
	for (size_t i = 0; i < n; i++)
		r[i] = s * v[i];
	return r;
}

// inner product
template <typename Elem>
float operator *(const std::vector<Elem>& a, const std::vector<Elem>& b)
{
	ASSERT(a.size() == b.size());
	float sum = 0;
	for(size_t i = 0; i < a.size(); i++)
	{
		float dot = a[i] * b[i];
		sum += dot;
	}
	return sum;
}

template <typename TE, typename TR>
TR InnerProduct(const std::vector<TE>& a, const std::vector<TE>& b)
{
	ASSERT(a.size() == b.size());
	TR sum = 0;
	for (size_t i = 0; i < a.size(); i++)
		sum += a[i] * b[i];
	return sum;
}

template <typename T1, typename T2, typename TR>
TR InnerProduct(const std::vector<T1>& a, const std::vector<T2>& b)
{
	ASSERT(a.size() == b.size());
	TR sum = 0;
	for(size_t i = 0; i < a.size(); i++)
		sum += a[i] * b[i];
	return sum;
}

template <typename Real>
void SolveSteepestDescent(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, int numIters)
{
	std::vector<Real> r = b - a * x;
	Real delta = r * r;
	std::vector<Real> q(b.size());
	const float tolerance = 0.001f;
	for (int iter = 0; iter < numIters; iter++)
	{
		q = a * r;
		Real den = r * q;
		Real alpha = delta / den;
		x = x + alpha * r;
		r = r - alpha * q;
		delta = r * r;
		if (delta < tolerance)
			return;
#ifdef COMPUTE_RESIDUAL
		Printf("residual %d = %f\n", iter, sqrt(delta1));
#endif
	}
}

template <typename Real>
void SolveConjugateGradient(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, int numIters)
{
	std::vector<Real> r = b - a * x;
	std::vector<Real> d = r;
	Real delta1 = r * r;
	Real delta0 = delta1;
	std::vector<Real> q(b.size());
	for (int iter = 0; iter < numIters; iter++)
	{
		q = a * d;
		Real alpha = delta1 / (d * q);
		//Printf("alpha%d=%f\n", iter, alpha);
		x = x + alpha * d;
		r = r - alpha * q;
		delta0 = delta1;
		delta1 = r * r;
		if (delta1 == 0.f)
			return;
		Real beta = delta1 / delta0;
		d = r + beta * d;
#ifdef COMPUTE_RESIDUAL
		Printf("residual %d = %f\n", iter, sqrt(delta1));
#endif
	}
}

template <typename Real>
void SolveBiconjugateGradient(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, int numIters)
{
	std::vector<Real> r = b - a * x;
	std::vector<Real> rBar(r), p(r), pBar(r);
	std::vector<Real> q = a * p;
	std::vector<Real> qBar(q);
	Real rSqr = rBar * r;
	for (int iter = 0; iter < numIters; iter++)
	{
		Real alpha = rSqr / (pBar * q);
		x = x + alpha * p;
		r = r - alpha * q;
		rBar = rBar - alpha * qBar;
		Real rSqr1 = rBar * r;
		Real beta = rSqr1 / rSqr;
		rSqr = rSqr1;
		p = r + beta * p;
		pBar = rBar + beta * pBar;
		q = a * p;
		qBar = a * pBar;
#ifdef COMPUTE_RESIDUAL
		Printf("residual %d = %f\n", iter, sqrt(r * r));
#endif
	}
}

template <typename Real>
void SolveMinimalResidual(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, int numIters)
{
	// minimum residual method
	std::vector<Real> r = b - a * x;
	std::vector<Real> p(r);
	std::vector<Real> y = a * r;
	std::vector<Real> z(y);
	Real rSqr = r * y;
	for (int iter = 0; iter < numIters; iter++)
	{
		Real alpha = rSqr / (z * z);
		x = x + alpha * p;
		r = r - alpha * z;
		y = a * r;
		Real rSqr1 = r * y; // TODO: tolerance on rSqr1
		Real beta = rSqr1 / rSqr;
		rSqr = rSqr1;
		p = r + beta * p;
		//z = a * p;
		z = y + beta * z;
#ifdef COMPUTE_RESIDUAL
		Printf("residual %d = %f\n", iter, sqrt(r * r));
#endif
	}
}

namespace math {

	template <typename Real>
	struct Preconditioner
	{
		typedef void(*Function)(const MatrixSqr<Real>& a, const std::vector<Real>& x, std::vector<Real>& y);

		static void Diagonal(const MatrixSqr<Real>& a, const std::vector<Real>& x, std::vector<Real>& y)
		{
			for (size_t i = 0; i < x.size(); i++)
			{
				y[i] = x[i] * 1.f / a.Get(i, i);
			}
		}

		static void LuGaussSeidel(const MatrixSqr<Real>& a, const std::vector<Real>& x, std::vector<Real>& y)
		{
			// forward substitution
			for (size_t i = 0; i < x.size(); i++)
			{
				y[i] = x[i];
				for (size_t j = 0; j < i; j++)
				{
					y[i] -= a.Get(i, j) * y[j] / a.Get(j, j);
				}
			}
			// backward substitution
			int n = x.size();
			for (int i = n - 1; i >= 0; i--)
			{
				for (int j = i + 1; j < n; j++)
				{
					y[i] -= a.Get(i, j) * y[j];
				}
				y[i] *= 1.f / a.Get(i, i);
			}
		}

		template <int n>
		static void GaussSeidel(const MatrixSqr<Real>& a, const std::vector<Real>& x, std::vector<Real>& y)
		{
			for (size_t i = 0; i < y.size(); i++)
				y[i] = 0.f;
			SolveGaussSeidel(a, x, y, n);
		}
	};

}

template <typename Real>
void SolvePreconditionedConjugateGradient(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, 
							typename math::Preconditioner<Real>::Function precond, int numIters)
{
	std::vector<Real> r = b - a * x;
	std::vector<Real> d = r;
	std::vector<Real> p(r.size());
	precond(a, r, p);
	Real delta1 = r * p;
	Real delta0 = delta1;
	std::vector<Real> q(b.size());
	for (int iter = 0; iter < numIters; iter++)
	{
		q = a * d;
		Real alpha = delta1 / (d * q);
		x = x + alpha * d;
		r = r - alpha * q;
		precond(a, r, p);
		delta0 = delta1;
		delta1 = r * p;
		Real beta = delta1 / delta0;
		d = p + beta * d;
#ifdef COMPUTE_RESIDUAL
		Printf("residual %d = %f\n", iter, sqrt(r * r));
#endif
	}
}

template <typename Real>
void SolvePreconditionedMinimalResidual(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, 
										typename math::Preconditioner<Real>::Function precond, int numIters)
{
	// minimum residual method
	std::vector<Real> r = b - a * x;
	std::vector<Real> d(r);
	std::vector<Real> y = a * r;
	std::vector<Real> z(y);
	std::vector<Real> p(r.size());
	precond(a, r, p);
	Real rSqr = p * y;
	for (int iter = 0; iter < numIters; iter++)
	{
		Real alpha = rSqr / (z * z);
		x = x + alpha * d;
		r = r - alpha * z;
		y = a * r;
		precond(a, r, p);
		Real rSqr1 = p * y;
		Real beta = rSqr1 / rSqr;
		rSqr = rSqr1;
		d = p + beta * d;
		z = a * d;
#ifdef COMPUTE_RESIDUAL
		Printf("residual %d = %f\n", iter, sqrt(r * r));
#endif
	}
}

// TODO: nonlinear CG

template <typename Real>
Real snrm(const std::vector<Real>& sx, int itol)
{
	if (itol <= 3)
	{
		return sqrt(sx * sx); // L2 norm
	}
	else
	{
		Real max = 0.;
		for (size_t i = 0; i < sx.size(); i++)
		{
			if (abs(sx[i]) > max)
				max = abs(sx[i]);
		}
		return max; // Linf norm
	}
}

template <typename Real>
void asolve(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x)
{
	for (size_t i = 0; i < x.size(); i++)
		x[i] = b[i] / a.Get(i, i);
}

template <typename Real>
void SolveConjugateGradient(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, int itol, int itmax, Real tol)
{
	size_t n = b.size();
	std::vector<Real> r = b - a * x;
	//std::vector<Real> rr = r;
	std::vector<Real> rr = a * r;
	std::vector<Real> z(n);
	Real bnrm, znrm;
	if (itol == 1)
	{
		bnrm = snrm(b, itol);
		asolve(a, r, z);
	}
	else if (itol == 2)
	{
		asolve(a, b, z);
		bnrm = snrm(z, itol);
		asolve(a, r, z);
	}
	else if (itol == 3 || itol == 4)
	{
		asolve(a, b, z);
		bnrm = snrm(z, itol);
		asolve(a, r, z);
		znrm = snrm(z, itol);
	}

	std::vector<Real> zz(n), p(n), pp(n);
	int iter = 0;
	Real bkden = 1.;
	while (iter < itmax)
	{
		++iter;
		asolve(a, rr, zz);
		Real bknum = z * rr;
		if (iter == 1)
		{
			p = z; // careful with std::vector assignments
			pp = zz;
		}
		else
		{
			Real bk = bknum / bkden;
			p = bk * p + z;
			pp = bk * pp + zz;
		}
		bkden = bknum;
		z = a * p;
		Real akden = z * pp;
		Real ak = bknum / akden;
		pp = a * zz;
		x = x + ak * p;
		r = r - ak * z;
		rr = r - ak * zz;
		asolve(a, r, z);

		Real err;
		if (itol == 1)
			err = snrm(r, itol) / bnrm;
		else if (itol == 2)
			err = snrm(z, itol) / bnrm;
		else if (itol == 3 || itol == 4)
		{
			Real zm1nrm = znrm;
			znrm = snrm(z, itol);
			const Real EPS = 1e-14f;
			if (abs(zm1nrm - znrm) > EPS * znrm)
			{
				Real dxnrm = abs(ak) * snrm(p, itol);
				err = znrm / abs(zm1nrm - znrm) * dxnrm;
			}
			else
			{
				err = znrm / bnrm;
				continue;
			}
			Real xnrm = snrm(x, itol);
			if (err <= 0.5 * xnrm)
			{
				err /= xnrm;
			}
			else
			{
				err = znrm / bnrm;
				continue;
			}
		}
		if (err <= tol)
			break;
	}
}

template <typename Real>
void SolveGaussSeidel(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, int numIters)
{
	size_t n = b.size();
#ifdef COMPUTE_RESIDUAL_GS
	std::vector<Real> r(n);
#endif
	for (int iter = 0; iter < numIters; iter++)
	{
		for (size_t i = 0; i < n; i++)
		{
			Real sum = 0;
			for (size_t j = 0; j < n; j++)
			{
				if (i != j)
					sum += a.Get(i, j) * x[j];
			}
			x[i] = (-sum + b[i]) / a.Get(i, i);
		}
#ifdef COMPUTE_RESIDUAL_GS
		r = b - a * x;
		Printf("residual %d = %f\n", iter, sqrt(r * r));
#endif
	}
}

template <typename Real>
void SolveJacobi(const MatrixSqr<Real>& a, const std::vector<Real>& b, std::vector<Real>& x, int numIters)
{
	// TODO: double check
	size_t n = b.size();
	std::vector<Real> xx(n);
	for (int iter = 0; iter < numIters; iter++)
	{
		for (size_t i = 0; i < n; i++)
		{
			Real sum = 0;
			for (size_t j = 0; j < n; j++)
			{
				if (i != j)
					sum += a.Get(i, j) * x[j];
			}
			xx[i] = (-sum + b[i]) / a.Get(i, i);
		}
		// TODO: memcpy
		for (size_t i = 0; i < n; i++)
		{
			x[i] = xx[i]; 
		}
	}
}

template <typename Real>
struct MatrixSqr<Real, COLUMN> : MatrixSqrBase<Real>
{
	MatrixSqr(size_t n) : MatrixSqrBase<Real>(n) {	}

	// TODO: asserts
	Real& Get(size_t i, size_t j)
	{
		return this->data[i + j * this->size];
	}

	Real Get(size_t i, size_t j) const
	{
		return this->data[i + j * this->size];
	}

	// returns column i
	Real* operator[](size_t i)
	{
		return this->data + i * this->size;
	}
};

#endif // MATRIX_H