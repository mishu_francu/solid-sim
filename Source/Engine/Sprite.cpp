#include <Engine/Base.h>
#include "Sprite.h"

#include <Graphics2D/Graphics2D.h>
#include <Graphics2D/Texture.h>
#include <Streams/FileStream.h>

Graphics2D*	Sprite::m_pGraphics = NULL;
Rect Sprite::m_cRect;

Sprite::Sprite()
{
	m_pTexture = NULL;
	m_iOp = OperationMode::Draw;
	
	//////////////////////////////////////////////////
	// MODULES
	this->m_iModules = 0;
	this->m_pModulesX = NULL;
	this->m_pModulesY = NULL;
	this->m_pModulesW = NULL;
	this->m_pModulesH = NULL;

	// ////////////////////////////////////////////////
	// FRAMES
	this->m_iFrames = 0;
	this->m_pFMOffset = NULL;
	this->m_iFModules = 0;
	this->m_pFModulesOX = NULL;
	this->m_pFModulesOY = NULL;
	this->m_pFModulesID = NULL;
	this->m_pFModulesFlags = NULL;

	// ////////////////////////////////////////////////
	// ANIMATIONS
	this->m_iAnimations = 0;
	this->m_pAFOffset = NULL;
	this->m_iAFrames = 0;
	this->m_pAFramesOX = NULL;
	this->m_pAFramesOY = NULL;
	this->m_pAFramesID = NULL;
	this->m_pAFramesTime = NULL;
	this->m_pAFramesFlags = NULL;
}

Sprite::~Sprite()
{
	this->ReleaseSprite();
}

void Sprite::SetGraphics(Graphics2D* g)
{
	m_pGraphics = g;
}

void Sprite::ReleaseSprite()
{
	this->m_pTexture = NULL;
	
	//////////////////////////////////////////////////
	// MODULES
	MemoryFramework::Free(this->m_pModulesX);
	MemoryFramework::Free(this->m_pModulesY);
	MemoryFramework::Free(this->m_pModulesW);
	MemoryFramework::Free(this->m_pModulesH);

	// ////////////////////////////////////////////////
	// FRAMES
	MemoryFramework::Free(this->m_pFMOffset);
	MemoryFramework::Free(this->m_pFModulesOX);
	MemoryFramework::Free(this->m_pFModulesOY);
	MemoryFramework::Free(this->m_pFModulesID);
	MemoryFramework::Free(this->m_pFModulesFlags);

	// ////////////////////////////////////////////////
	// ANIMATIONS
	MemoryFramework::Free(this->m_pAFOffset);
	MemoryFramework::Free(this->m_pAFramesOX);
	MemoryFramework::Free(this->m_pAFramesOY);
	MemoryFramework::Free(this->m_pAFramesID);
	MemoryFramework::Free(this->m_pAFramesTime);
	MemoryFramework::Free(this->m_pAFramesFlags);
}

//void Sprite::SetTexture(Texture* texture)
//{
//	if(this->m_pTexture != NULL && this->m_pTexture != texture)
//		TEXMAN->DeleteTexture(this->m_pTexture);
//	this->m_pTexture = texture;
//}

Texture* Sprite::GetTexture() const
{
	return this->m_pTexture;
}

void Sprite::LoadSprite(const char* filename, const char* textureName)
{
	//SetTexture(TEXMAN->GetTexture(textureName));
	m_pTexture = new Texture();
	if (!m_pTexture->LoadTexture(textureName))
	{
		Printf("Could not load texture %s\n", textureName);
		return;
	}

	int ret = 0;
	FileStream stream;
	if (!stream.Open(filename, Stream::STREAM_MODE_READ))
		return;
	/*int8 type = */stream.ReadByte();
	
	// Modules
	this->m_iModules = stream.ReadShort() & 0xFFFF;
	this->m_pModulesX = (uint16*)MemoryFramework::Alloc(this->m_iModules * sizeof(uint16),"Global", "Sprite::LoadSprite::1");
	this->m_pModulesY = (uint16*)MemoryFramework::Alloc(this->m_iModules * sizeof(uint16),"Global", "Sprite::LoadSprite::2");
	this->m_pModulesW = (uint16*)MemoryFramework::Alloc(this->m_iModules * sizeof(uint16),"Global", "Sprite::LoadSprite::3");
	this->m_pModulesH = (uint16*)MemoryFramework::Alloc(this->m_iModules * sizeof(uint16),"Global", "Sprite::LoadSprite::4");
	ret = stream.ReadArray(this->m_pModulesX, this->m_iModules * sizeof(int16));
	ret = stream.ReadArray(this->m_pModulesY, this->m_iModules * sizeof(int16));
	ret = stream.ReadArray(this->m_pModulesW, this->m_iModules * sizeof(int16));
	ret = stream.ReadArray(this->m_pModulesH, this->m_iModules * sizeof(int16));
	//////////////////////////////////////////////////
	// Frames
	m_iFrames = stream.ReadShort() & 0xFFFF;
	this->m_pFMOffset = (int16*)MemoryFramework::Alloc((this->m_iFrames + 1) * sizeof(int16),"Global", "Sprite::LoadSprite::5");
	m_pFMOffset[0] = 0;

	int16 fm_offset = 0;
	for (int i = 1; i <= m_iFrames; i++)
	{
		fm_offset += (stream.ReadByte() & 0xFF);
		m_pFMOffset[i] = (int16)fm_offset;
	}

	m_iFModules = stream.ReadShort() & 0xFFFF;

	this->m_pFModulesOX = (int16*)MemoryFramework::Alloc(m_iFModules * sizeof(int16),"Global", "Sprite::LoadSprite::6");
	this->m_pFModulesOY = (int16*)MemoryFramework::Alloc(m_iFModules * sizeof(int16),"Global", "Sprite::LoadSprite::7");
	this->m_pFModulesID = (int8*)MemoryFramework::Alloc(m_iFModules * sizeof(int8),"Global", "Sprite::LoadSprite::8");
	this->m_pFModulesFlags = (int8*)MemoryFramework::Alloc(m_iFModules * sizeof(int8),"Global", "Sprite::LoadSprite::9");
	ret = stream.ReadArray(this->m_pFModulesOX, this->m_iFModules * sizeof(int16));
	ret = stream.ReadArray(this->m_pFModulesOY, this->m_iFModules * sizeof(int16));
	ret = stream.ReadArray(this->m_pFModulesID, this->m_iFModules * sizeof(int8));
	ret = stream.ReadArray(this->m_pFModulesFlags, this->m_iFModules * sizeof(int8));

	//////////////////////////////////////////////////
	// Animations
	m_iAnimations = stream.ReadShort() & 0xFFFF;
	this->m_pAFOffset = (int16*)MemoryFramework::Alloc((this->m_iAnimations + 1) * sizeof(int16),"Global", "Sprite::LoadSprite::10");
	m_pAFOffset[0] = 0;
	int af_offset = 0;
	for (int i = 1; i <= m_iAnimations; i++)
	{
		af_offset += (stream.ReadByte() & 0xFF);
		m_pAFOffset[i] = (int16)af_offset;
	}

	m_iAFrames = stream.ReadShort() & 0xFFFF;
	this->m_pAFramesOX = (int8*)MemoryFramework::Alloc(this->m_iAFrames  * sizeof(int8),"Global", "Sprite::LoadSprite::11");
	this->m_pAFramesOY = (int8*)MemoryFramework::Alloc(this->m_iAFrames  * sizeof(int8),"Global", "Sprite::LoadSprite::12");
	this->m_pAFramesID = (int8*)MemoryFramework::Alloc(this->m_iAFrames  * sizeof(int8),"Global", "Sprite::LoadSprite::13");
	this->m_pAFramesTime = (int8*)MemoryFramework::Alloc(this->m_iAFrames  * sizeof(int8),"Global", "Sprite::LoadSprite::14");
	this->m_pAFramesFlags = (int8*)MemoryFramework::Alloc(this->m_iAFrames  * sizeof(int8),"Global", "Sprite::LoadSprite::15");

	ret = stream.ReadArray(this->m_pAFramesOX, this->m_iAFrames * sizeof(int8));
	ret = stream.ReadArray(this->m_pAFramesOY, this->m_iAFrames * sizeof(int8));
	ret = stream.ReadArray(this->m_pAFramesID, this->m_iAFrames * sizeof(int8));
	ret = stream.ReadArray(this->m_pAFramesTime, this->m_iAFrames * sizeof(int8));
	ret = stream.ReadArray(this->m_pAFramesFlags, this->m_iAFrames * sizeof(int8));
}

int Sprite::CombineTransforms(int transform1, int transform2)
{
	if ((transform1 & Transform::Rot90) == 0)
		return (transform1 ^ transform2);
	if ((transform2 & Transform::FlipX) != 0)
		transform1 ^= Transform::FlipY;
	if ((transform2 & Transform::FlipY) != 0)
		transform1 ^= Transform::FlipX;
	if ((transform2 & Transform::Rot90) != 0)
		transform1 ^= (Transform::Rot90 | Transform::FlipY | Transform::FlipX);
	return transform1;
}

Rect Sprite::RectFrame(int frame, float x, float y, int flags)
{
	m_cRect = Rect(MAX_POSITION_VALUE, MAX_POSITION_VALUE, -2 * MAX_POSITION_VALUE, -2 * MAX_POSITION_VALUE);
	int16 old_op = m_iOp;
	m_iOp = OperationMode::ComputeRect;
	DrawFrame(frame, x, y, flags);
	m_iOp = old_op;
	return m_cRect;
}

int Sprite::GetAnimFrameTime(int anim, int aframe)
{
	return m_pAFramesTime[m_pAFOffset[anim] + aframe] & 0xFF;
}

int Sprite::GetAnimTime(int anim)
{
	int16 len = GetAnimLength(anim);
	int16 time = 0;
	for (int16 i = 0; i < len; i++)
		time += GetAnimFrameTime(anim, i);

	return time;
}

int Sprite::GetAnimLength(int anim)
{
	return m_pAFOffset[anim + 1] - m_pAFOffset[anim];
}

void Sprite::DrawModule(int module, float x, float y, int transform)
{
	uint16 srcW = this->m_pModulesW[module];
	uint16 srcH = this->m_pModulesH[module];
	if (m_iOp == OperationMode::ComputeRect)
	{
		if ((transform & Transform::Rot90) != 0)
			swap(srcW, srcH);
		m_cRect.Union(Rect( x, y, x + srcW, y + srcH));
	}
	else
	{
		const uint16 srcX = this->m_pModulesX[module];
		const uint16 srcY = this->m_pModulesY[module];
		m_pGraphics->DrawRegion(m_pTexture, x, y, srcX, srcY, srcW, srcH); // transform, Align::TopLeft); // FIXME
	}
}

void Sprite::DrawModule(int module, float x, float y, int transform, int align)
{
	const uint16 srcX = this->m_pModulesX[module];
	const uint16 srcY = this->m_pModulesY[module];
	const uint16 srcW = this->m_pModulesW[module];
	const uint16 srcH = this->m_pModulesH[module];
	m_pGraphics->DrawRegion(m_pTexture, x, y, srcX, srcY, srcW, srcH); // transform, align); // FIXME
}

void Sprite::DrawFrameModule(int frame, int fmodule, float x, float y, int transform)
{
	int16 off = this->m_pFMOffset[frame] + fmodule;

	int16 ox = this->m_pFModulesOX[off];
	int16 oy = this->m_pFModulesOY[off];
	int16 target = (this->m_pFModulesID[off] & 0xFF);
	int16 fm_transform = (this->m_pFModulesFlags[off] & 0xFF);

	if ((transform & (Transform::Rot90 | Transform::FlipY | Transform::FlipX)) != 0)
	{
		int16 w = m_pModulesW[target];
		int16 h = m_pModulesH[target];

		if ((transform & Transform::FlipX) != 0)
			ox = - ox - w;
		if ((transform & Transform::FlipY) != 0)
			oy = - oy - h;

		if ((transform & Transform::Rot90) != 0)
		{
			int16 t = ox;
			ox = - oy - h;
			oy = t;
		}

		fm_transform = CombineTransforms(fm_transform, transform);
	}

	if (m_iOp == OperationMode::ComputeRect)
	{
		DrawModule(target, x + ox, y + oy, fm_transform);
	}
	else
	{
		m_pGraphics->PushMatrix();
		m_pGraphics->Translate(ox, oy);
		DrawModule(target, x, y, fm_transform);
		m_pGraphics->PopMatrix();
	}
}


void Sprite::DrawFrame(int frame, float x, float y, int transform)
{
	int num_fmodules = this->m_pFMOffset[frame + 1] - this->m_pFMOffset[frame];
	for (int16 i = 0; i < num_fmodules; i++)
	{
		DrawFrameModule(frame, i, x, y, transform);
	}
}


void Sprite::DrawFrameRotated(int frame, float x, float y, int transform, float angle)
{
	m_pGraphics->PushMatrix();
	
	m_pGraphics->Translate(x, y);
	m_pGraphics->Rotate(ANGLE(angle));
	int num_fmodules = this->m_pFMOffset[frame + 1] - this->m_pFMOffset[frame];
	for (int16 i = 0; i < num_fmodules; i++)
	{
		DrawFrameModule(frame, i, 0, 0, transform);
	}

	m_pGraphics->PopMatrix();
}

void Sprite::DrawFrame(int frame, float x, float y, int transform, int align)
{

	if(align == 0)
	{
		DrawFrame(frame, x, y, transform);
		return;
	}

	RectFrame(frame, 0, 0, transform);
	
	float offsetX = 0;
	float offsetY = 0;

	if ((align & Align::Right) != 0)
		offsetX = m_cRect.right;
	else
		if ((align & Align::HCenter) != 0)
			offsetX = (m_cRect.left + m_cRect.right) * 0.5f;
		else if ((align & Align::Left) != 0)
			offsetX = m_cRect.left;

	if ((align & Align::Bottom) != 0)
		offsetY = m_cRect.bottom;
	else
		if ((align & Align::VCenter) != 0)
			offsetY = (m_cRect.top + m_cRect.bottom) * 0.5f;
		else if ((align & Align::Top) != 0)
			offsetY = m_cRect.top;

	DrawFrame(frame, x - offsetX, y - offsetY, transform);
}

void Sprite::DrawAnimFrame(int anim, int aframe, float x, float y, int transform)
{
	int16 off = m_pAFOffset[anim] + aframe;
	DrawFrame(m_pAFramesID[off] & 0xFF, x + m_pAFramesOX[off], y + m_pAFramesOY[off], CombineTransforms(m_pAFramesFlags[off], transform));
}


