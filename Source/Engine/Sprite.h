#ifndef SPRITE_H
#define SPRITE_H

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))

struct Rect
{
	float left;
	float top;
	float right;
	float bottom;

	Rect(): left(0), right(0), top(0), bottom(0) {}

	Rect(float left, float top, float right, float bottom) 
	{
		this->left = left;
		this->right = right;
		this->top = top;
		this->bottom = bottom;
	}

	Rect(int iLeft, int iTop, int iRight, int iBottom) 
	{
		left = (float)iLeft;
		right = (float)iRight;
		top = (float)iTop;
		bottom = (float)iBottom;
	}

	float Width() { return right - left; }

	float Height() { return bottom - top; }

	// TODO: out of line version
	void Union(const Rect& r)
	{		
		left = MIN(left, r.left);
		right = MAX(right, r.right);
		top = MIN(top, r.top);
		bottom = MAX(bottom, r.bottom);
	}

	// not sure if operators are a good idea
};

// to be moved (if used further more)
template<typename T>
inline void swap(T& v0, T& v1)
{
	T aux(v0);
	v0 = v1;
	v1 = aux;
}

struct OperationMode 
{
	static const int8 Draw = 0;
	static const int8 ComputeRect = 1;
};

class Texture;
class Graphics2D;

class Sprite
{
public:
	Sprite();
	
	~Sprite();

public:
	
	void LoadSprite(const char* filename, const char* textureName);
	
	void ReleaseSprite();
	
	//void SetTexture(Texture* texture);
	
	Texture* GetTexture() const;
	
	// Note: changed all the arguments and return values to int (better to use native word size)
	// might only increases stack memory usage, but the code should be faster (hopefuly) or at least the same
	static int CombineTransforms(int transform1, int transform2);
	
	Rect RectFrame(int frame, float x, float y, int flags);

	int GetAnimFrameTime(int anim, int aframe);

	int GetAnimTime(int anim);

	int GetAnimLength(int anim);

	int GetNumModules() const { return m_iModules; }

	int GetModuleWidth(int i) const { return m_pModulesW[i]; }

	int GetModuleHeight(int i) const { return m_pModulesH[i]; }

	int GetNumFrames() const { return m_iFrames; }

	void DrawModule(int module, float x, float y, int transform);

	void DrawModule(int module, float x, float y, int transform, int align);

private: 

	void DrawFrameModule(int frame, int fmodule, float x, float y, int transform);

public: 

	void DrawFrame(int frame, float x, float y, int transform);

	void DrawFrame(int frame, float x, float y, int transform, int align);

	void DrawFrameRotated(int frame, float x, float y, int transform, float angle);

	void DrawAnimFrame(int anim, int aframe, float x, float y, int transform);

public:

	static void SetGraphics(Graphics2D* g);

private:

	static Graphics2D* m_pGraphics;

private:

	Texture* m_pTexture;

	static Rect m_cRect;
	
	int	m_iOp;
	
	// ////////////////////////////////////////////////
	// MODULES
	int m_iModules; // number of modules
	uint16 *m_pModulesX; // x position inside texture for each module
	uint16 *m_pModulesY; // y position inside texture for each module
	uint16 *m_pModulesW; // width for each module
	uint16 *m_pModulesH; // height for each module

	// ////////////////////////////////////////////////
	// FRAMES
	int	 m_iFrames; // number of frames
	int16* m_pFMOffset; // = new int[_frames+1] number of fmodules for a frame i = _frame_fm_offset[i+1] - _frame_fm_offset[i]
	int  m_iFModules; // total number of fmodules
	int16* m_pFModulesOX;// ox for each fmodule
	int16* m_pFModulesOY;// oy for each fmodule
	int8*  m_pFModulesID; // module/frame id for each fmodule
	int8*  m_pFModulesFlags; // transform for each fmodule

	// ////////////////////////////////////////////////
	// ANIMATIONS
	int m_iAnimations; // number of animations
	int16* m_pAFOffset; // = new int[_animations+1] number of aframes for an animation i = _animation_af_offset[i+1] - _animation_af_offset[i]
	int m_iAFrames; // total number of aframes
	int8* m_pAFramesOX; // ox for each aframe
	int8* m_pAFramesOY; // oy for each aframe
	int8* m_pAFramesID; // frame id for each aframe
	int8* m_pAFramesTime; // time for each aframe
	int8* m_pAFramesFlags; // transform for each aframe

private:
	
	static const int MAX_POSITION_VALUE = int16_MAX; 

};

#endif // SPRITE_H
