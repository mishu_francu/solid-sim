#include "Base.h"
#include <Demo/Particles/Particles.h>
#include <Demo/Particles/Rigid.h>
#include <Demo/Particles/3D/Collision3D.h>
#include <Demo/Particles/3D/FemSystem.h>

#include <jni.h>
#include <android/log.h>
#include <stdint.h>
#include <time.h>

ParticlesDemo engine;

extern "C"
{

//static long
//_getTime(void)
//{
//    struct timeval  now;
//
//    gettimeofday(&now, NULL);
//    return (long)(now.tv_sec*1000 + now.tv_usec/1000);
//}

/* Call to initialize the graphics state */
JNIEXPORT void JNICALL
Java_com_demo_particles_DemoRenderer_nativeInit( JNIEnv*  env )
{
	engine.Create();
}

JNIEXPORT void JNICALL
Java_com_demo_particles_DemoRenderer_nativeResize( JNIEnv*  env, jobject  thiz, jint w, jint h )
{
	engine.Resize(w, h);
}

/* Call to finalize the graphics state */
JNIEXPORT void JNICALL
Java_com_demo_particles_DemoRenderer_nativeDone( JNIEnv*  env )
{
	//engine.Destroy();
}

/* This is called to indicate to the render loop that it should
 * stop as soon as possible.
 */
JNIEXPORT void JNICALL
Java_com_demo_particles_DemoGLSurfaceView_nativePause( JNIEnv*  env )
{
	engine.PausePhysics();
}

long prev = -1, curr;

/* Call to render the next GL frame */
JNIEXPORT void JNICALL
Java_com_demo_particles_DemoRenderer_nativeRender( JNIEnv*  env )
{
	engine.Draw();
}

JNIEXPORT void JNICALL
Java_com_demo_particles_ParticlesDemo_setGravity(JNIEnv*  env, jobject  thiz, jfloat x, jfloat y)
{
	engine.SetGravity(x, y);
}

};
