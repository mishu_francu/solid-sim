#ifndef __FILESTREAM__
#define __FILESTREAM__

#pragma once

#include "Stream.h"

class FileStream: public Stream
{
public:
	FileStream();
	~FileStream();
private:
	int8*		BuildModeString(int32 mode);
public:
	bool		Open(const char *filename, int32 mode);
	void		Close();
	int32		Seek(int32 bytes);
	size_t		GetLength();
	int32		Available();
	bool		IsEOF();
	void		Reset();

public:
	// TODO: define these inline and using a macro
	int8		ReadByte();
	int16		ReadShort();
	int32		ReadInt();
	int64		ReadLong();
	uint8		ReadUByte();
	uint16		ReadUShort();
	uint32		ReadUInt();
	uint64		ReadULong();
	float		ReadFloat();
	int32		ReadArray(void* buff, int32 len);

	void		WriteByte(uint8 val);
	void		WriteUInt(uint32 val);
	void		WriteFloat(float val);
	void		Write(void* buffer, int size);
private:
	FILE*		m_pFile;
	int8*		m_pModeStr;
};

#endif