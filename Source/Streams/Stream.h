#ifndef STREAM_H
#define STREAM_H

class Stream
{
public:
	Stream()
	{
		m_iPosition = 0;
		m_iLenght = 0;
		m_iMode = 0;
	}

public:
	virtual bool	Open(const char *filename, int32 mode) = 0;
	virtual void	Close() = 0;
	virtual int32	Seek(int32 bytes) = 0;
	virtual size_t	GetLength() = 0;
	virtual int32	Available() = 0;
	virtual bool	IsEOF() = 0;
	virtual void	Reset() = 0;

public:
	virtual int8	ReadByte() = 0;
	virtual int16	ReadShort() = 0;
	virtual int32	ReadInt() = 0;
	virtual int64	ReadLong() = 0;
	virtual uint8	ReadUByte() = 0;
	virtual uint16	ReadUShort() = 0;
	virtual uint32  ReadUInt() = 0;
	virtual uint64	ReadULong() = 0;
	virtual float	ReadFloat() = 0;
	virtual int32	ReadArray(void* buff, int32 len) = 0;

	virtual void	WriteByte(uint8 val) = 0;
	virtual void	WriteUInt(uint32 val) = 0;
	virtual void	WriteFloat(float val) = 0;
	virtual void	Write(void* buffer, int size) = 0;
protected:
	size_t		m_iPosition;
	size_t		m_iLenght;
	int32		m_iMode;
public:
	static const int32 STREAM_MODE_READ = 1<<0;
	static const int32 STREAM_MODE_WRITE = 1<<1;
	static const int32 STREAM_MODE_APPEND = 1<<2;
};

#endif // STREAM_H