#include <Engine/Base.h>
#include "FileStream.h"


FileStream::FileStream()
{
	m_pModeStr = NULL;
	m_pFile = NULL;
}


FileStream::~FileStream()
{
	this->Close();
	MemoryFramework::Free(m_pModeStr);
}

int8* FileStream::BuildModeString(int mode)
{
	if(m_pModeStr == NULL)
		m_pModeStr = (int8*)MemoryFramework::Alloc(16, "Global", "FileStream::BuildModeString::m_pMode");

	if( (mode & STREAM_MODE_READ) != 0)
	{
		sprintf_s((char*)m_pModeStr, 16, "rb");
	}
	if( (mode & STREAM_MODE_WRITE) != 0)
	{
		sprintf_s((char*)m_pModeStr, 16, "wb");
	}
	return m_pModeStr;
}



bool FileStream::Open(const char *filename, int mode)
{
	if (m_pFile != NULL)
		Close();
#ifdef ANDROID_NDK
	m_pFile = fopen((const char*)filename, (const char*)BuildModeString(mode));
	if (m_pFile == NULL)
	{
		Printf("Could not open file %s\n", filename);
		return false;
	}
#else
	int ret = fopen_s(&m_pFile, (const char*)filename, (const char*)BuildModeString(mode));
	if (ret != 0)
	{
		m_pFile = NULL;
		Printf("Could not open file %s\n", filename);
		return false;
	}
#endif	

	m_iMode = mode;
	Reset();
	return true;
}


void FileStream::Close()
{
	if (m_pFile == NULL)
		return ;
	fclose(m_pFile);
	m_pFile = NULL;
}


int FileStream::Seek(int bytes)
{
	if(bytes + m_iPosition > m_iLenght)
		bytes = int(m_iLenght - m_iPosition);
	if(bytes + m_iPosition < 0)
		bytes = -(int)m_iPosition;
	fseek(m_pFile, bytes, SEEK_CUR);

	return bytes;
}


size_t FileStream::GetLength()
{
	return m_iLenght;
}


int FileStream::Available()
{
	return int(m_iLenght - m_iPosition);
}


bool FileStream::IsEOF()
{
	return (m_iLenght <= m_iPosition);
}


void FileStream::Reset()
{
	// Read the file length
	fseek(m_pFile, 0, SEEK_END);
	m_iLenght = ftell(m_pFile);
	fseek(m_pFile, 0, SEEK_SET);

	m_iPosition = 0;
}



int8 FileStream::ReadByte()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	int8 ret;
	m_iPosition += fread(&ret, sizeof(ret), 1, m_pFile);
	return ret;
}


int16 FileStream::ReadShort()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	int16 ret;
	m_iPosition += fread(&ret, 1, sizeof(ret), m_pFile);
	return ret;
}


int FileStream::ReadInt()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	int ret;
	m_iPosition += fread(&ret, 1, sizeof(ret), m_pFile);
	return ret;
}


int64 FileStream::ReadLong()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	int64 ret;
	m_iPosition += fread(&ret, 1, sizeof(ret), m_pFile);
	return ret;
}


uint8 FileStream::ReadUByte()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	uint8 ret;
	m_iPosition += fread(&ret, 1, sizeof(ret), m_pFile);
	return ret;
}


uint16 FileStream::ReadUShort()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	uint16 ret;
	m_iPosition += fread(&ret, 1, sizeof(ret), m_pFile);
	return ret;
}


uint32  FileStream::ReadUInt()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	uint32  ret;
	m_iPosition += fread(&ret, 1, sizeof(ret), m_pFile);
	return ret;
}

float FileStream::ReadFloat()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	float ret;
	m_iPosition += fread(&ret, 1, sizeof(ret), m_pFile);
	return ret;
}

uint64 FileStream::ReadULong()
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	
	uint64 ret;
	m_iPosition += fread(&ret, 1, sizeof(ret), m_pFile);
	return ret;
}


int FileStream::ReadArray(void *buff, int len)
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_READ) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	size_t ret = fread(buff, 1, len, m_pFile);
	m_iPosition += ret;
	return (int)ret;
}

void FileStream::WriteByte(uint8 val)
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_WRITE) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif	
	m_iPosition += fwrite(&val, 1, sizeof(val), m_pFile);
}

void FileStream::WriteUInt(uint32 val)
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_WRITE) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif	
	m_iPosition += fwrite(&val, 1, sizeof(val), m_pFile);
}

void FileStream::WriteFloat(float val)
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_WRITE) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif	
	m_iPosition += fwrite(&val, 1, sizeof(val), m_pFile);
}

void FileStream::Write(void* buffer, int size)
{
#if DEBUG
	if (m_pFile == NULL || IsEOF() || (m_iMode & STREAM_MODE_WRITE) == 0)
	{
		ASSERT(true);
		return 0;
	}
#endif
	m_iPosition += fwrite(buffer, 1, size, m_pFile);
}